<?PHP

require_once('Widget.class.php');
require_once ('PresetFormat.class.php');
require_once ('Helper.class.php');
require_once('QueryProvider.php');

class Blog extends Widget
{
    const TAG_COMMUNITY = 'society';
    const TAG_ECONOMY = 'economy';
    const TAG_ECOLOGY = 'ecology';
    const TAG_MAIN = 'main';
    const TAG_PLATFORMA = 'platform';

    private $postMatrix = array();

    private $postsMatrix;

    private $numItemsOnPage;

    private $tags = array();

    private $isSubscribe = false;

    public $itemPerPage = 12;

    public $minGroupItemCount = 4;

    /**
     *
     * Конструктор
     *
     */
    function Blog(&$parent = null)
    {
        Widget::Widget($parent);
        Helper::$db = $this->db;
        $this->postMatrix = array(
            array('1/1' => array('settings' => array('block' => '1/1', 'start' => 0, 'limit' => 1), 'items' => null)),
            array('1/2' => array('settings' => array('block' => '1/2', 'start' => 0, 'limit' => 2), 'items' => null)),
            array('1/3' => array('settings' => array('block' => '1/3', 'start' => 0, 'limit' => 3), 'items' => null)),
            array('1/1' => array('settings' => array('block' => '1/1', 'start' => 0, 'limit' => 1), 'items' => null)),
            array('1/4' => array('settings' => array('block' => '1/4', 'start' => 0, 'limit' => 4), 'items' => null)),
            array('1/2' => array('settings' => array('block' => '1/2', 'start' => 0, 'limit' => 2), 'items' => null)),
            array('1/1' => array('settings' => array('block' => '1/1', 'start' => 0, 'limit' => 1), 'items' => null)),
            array('1/4' => array('settings' => array('block' => '1/4', 'start' => 0, 'limit' => 4), 'items' => null)),
            array('1/3' => array('settings' => array('block' => '1/3', 'start' => 0, 'limit' => 3), 'items' => null)),
        );
        $this->postsMatrix = new stdClass();
        $this->numItemsOnPage = 21;
        $this->numAuthorsOnPage = 12;

        $this->tags = array(
            self::TAG_ECOLOGY => "Экология",
            self::TAG_ECONOMY => "Экономика",
            self::TAG_COMMUNITY => "Общество",
            self::TAG_PLATFORMA => "Платформа"
        );
//        $this->fetch();
        $blogTags = new stdClass();
        $blogTags->community = Blog::TAG_COMMUNITY;
        $blogTags->ecology = Blog::TAG_ECOLOGY;
        $blogTags->economy = Blog::TAG_ECONOMY;
        $blogTags->main = Blog::TAG_MAIN;
        $blogTags->platforma = Blog::TAG_PLATFORMA;

        Helper::$db = $this->db;

        $this->smarty->assign('blogTags', $blogTags);
    }

    /**
     *
     * Отображение
     *
     */
    function fetch()
    {
        // чтобы работала пагинация в бесконечном скролле
        // нам необходимы ID записей, которые уже вывелись раньше
        // за неимением "гоничной" (не может jScroll работать с POST запросами
        // информацию об уже выведенных ID храним в сессии
        // и если пользователь обновил страницу - иы сбрасываем сохраненные в сессии значения
        if (intval(@$_GET['page']) == 0 || is_null(@$_GET['page'])){
            unset($_SESSION['usedIdsPosts']);
            unset($_SESSION['useElasticBlock']);
        }

        // Оформлял ли пользователь подписку
        $this->isSubscribe = isset($_COOKIE['issubscribe']) ? $_COOKIE['issubscribe'] : false;

        // Какую статью нужно вывести?
        $article_url = $this->url_filtered_param('article_url');
        $rubrika_url = $this->url_filtered_param('rubrika_url');
        $tagUrl = $this->url_filtered_param('tag_url');
        $writerId = $this->url_filtered_param('writer_id');
        $isRobot = $this->url_filtered_param('robot');
        $specProjects = $this->url_filtered_param('specprojects');
        $isInfiniteScroll = $this->url_filtered_param('infinitescroll');
        $page = $this->url_filtered_param('page');
        $mode = $this->url_filtered_param('mode');

        $this->rubrikaUrl = $rubrika_url;

        if (!empty($article_url) && empty($isRobot) && $rubrika_url == 'news') {
            // Если передан id статьи, выводим ее
            return $this->fetchNewsItem($article_url, $rubrika_url);
        }
        elseif (!empty($article_url) && empty($isRobot)) {
            // Если передан id статьи, выводим ее
            return $this->fetch_item($article_url, $rubrika_url);
        }
        elseif (!empty($article_url) && !empty($isRobot)) {
            return $this->fetchItemForRobot($article_url, $rubrika_url);
        }
        elseif (!empty($specProjects)){
            return $this->fetchListSpecProjects();
        }
        elseif ($mode == 'getaboutpage'){
            return $this->getAboutPage();
        }
        elseif ($mode == 'autorslist' && empty($writerId)){
            return $this->getAllAuthors();
        }
        elseif ($mode == 'autorslist' && !empty($writerId)){
            $usedids = unserialize($_SESSION['usedIdsPosts']);
            $useelastic = 0;
            if (!is_null($_SESSION['useElasticBlock'])){
                $useelastic = $_SESSION['useElasticBlock'];
            }
            return $this->getAuthorsPosts($writerId, $page, $usedids, $useelastic, false);
        }
        elseif ($mode == 'getleaders' && !empty($rubrika_url)){
            $this->typeUrl = "leaders";
            return $this->getLeaders($rubrika_url);
        }
        elseif(!empty($rubrika_url) && empty($mode)){
            $usedids = unserialize($_SESSION['usedIdsPosts']);
            $useelastic = 0;
            if (!is_null($_SESSION['useElasticBlock'])){
                $useelastic = $_SESSION['useElasticBlock'];
            }
            $this->typeUrl = "blogs";

            return $this->getPosts($rubrika_url, $page, $usedids, $useelastic, false);
        }
        elseif(!empty($rubrika_url) && $mode == 'posttags' && empty($tagUrl)){
            $this->typeUrl = "tag";
            return $this->getPostTags($rubrika_url);
        }
        elseif(!empty($rubrika_url) && $mode == 'getsimpleblogs' && empty($tagUrl)){
            // Если нет, выводим список всех новостей
            $usedids = unserialize($_SESSION['usedIdsPosts']);
            $useelastic = 0;
            if (!is_null($_SESSION['useElasticBlock'])){
                $useelastic = $_SESSION['useElasticBlock'];
            }

            if (empty($page)){
                $page = 0;
            }

            $this->typeUrl = "getsimpleblogs";

            return $this->getPosts('main', $page, $usedids, $useelastic, false, 1);
        }
        elseif($rubrika_url == 'news' && $mode == 'main' && empty($tagUrl)){
            $useelastic = 0;
            if (!is_null($_SESSION['useElasticBlock'])){
                $useelastic = $_SESSION['useElasticBlock'];
            }

            if (empty($page)){
                $page = 0;
            }

            $this->typeUrl = "getsimpleblogs";

            return $this->getNews($rubrika_url, $page, $useelastic, false);
        }
        elseif(!empty($rubrika_url) && $mode == 'main' && empty($tagUrl)){
            // Если нет, выводим список всех новостей
            $usedids = unserialize($_SESSION['usedIdsPosts']);
            $useelastic = 0;
            if (!is_null($_SESSION['useElasticBlock'])){
                $useelastic = $_SESSION['useElasticBlock'];
            }

            if (empty($page)){
                $page = 0;
            }

            $this->typeUrl = "getsimpleblogs";

            return $this->getPosts($rubrika_url, $page, $usedids, $useelastic, false);
        }
        elseif(!empty($rubrika_url) && $mode == 'getpostsfromtag' && !empty($tagUrl)){
            $usedids = isset($_SESSION['usedIdsPosts']) ? unserialize($_SESSION['usedIdsPosts']): array();
            $useelastic = 0;
            if (isset($_SESSION['useElasticBlock']) && !is_null($_SESSION['useElasticBlock'])){
                $useelastic = $_SESSION['useElasticBlock'];
            }
            return $this->fetchListByTag($rubrika_url, $tagUrl, $page, $usedids, $useelastic, false);
        }
        elseif($mode == "search"){
            return $this->getSearch($_GET['query'], $_GET['page']);
        }
        elseif ($mode == "APIconfirmSubscribe") {

            $hash = $_GET['hash'];
            $confirmSubscribe = $this->confirmSubscribe($hash);
            echo json_encode(array('ok' => true, 'confirmSubscribe' => $confirmSubscribe));
            exit(0);;
        }
        elseif ($mode == "confirmSubscribe"){
            //By default, we assume that it is not
            //an AJAX request.
            $isAjaxRequest = false;

            //IF HTTP_X_REQUESTED_WITH is equal to xmlhttprequest
            if(
                isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
                strcasecmp($_SERVER['HTTP_X_REQUESTED_WITH'], 'xmlhttprequest') == 0
            ){
                //Set our $isAjaxRequest to true.
                $isAjaxRequest = true;
            }
            $hash = $_GET['hash'];
            $confirmSubscribe = $this->confirmSubscribe($hash);

            // Если нет, выводим список всех новостей
            $usedids = unserialize($_SESSION['usedIdsPosts']);
            $useelastic = 0;
            if (!is_null($_SESSION['useElasticBlock'])){
                $useelastic = $_SESSION['useElasticBlock'];
            }

            if (empty($page)){
                $page = 0;
            }
            if ($isAjaxRequest) {
                echo json_encode(array('ok' => true, 'confirmSubscribe' => $confirmSubscribe));
                exit(0);;
            }

            $this->smarty->assign('confirmSubscribe', $confirmSubscribe);
            return $this->getMainPage();
        }
        elseif ($mode == 'getPostInfinityScroll' && $rubrika_url != 'news'){
            return $this->getPostListToInfinityScroll($rubrika_url);
        }
        elseif ($mode == 'getPostInfinityScroll' && $rubrika_url == 'news'){
            return $this->getNewsListToInfinityScroll($rubrika_url);
        }
        elseif($mode == "sitemap"){
            $this->getSitemap();
//            return true;
        }
        else {
            // Если нет, выводим список всех постов
            return $this->getMainPage();
        }
    }

    private function getHost()
    {
        $host = $this->config->protocol . $this->config->host;

        return $host;
    }

    function getPostById($id)
    {
        $query = sql_placeholder("SELECT bp.id, bp.header, bp.name, bp.lead, bp.url, bp.format, bp.font, bp.is_partner_material,
                    bp.image_1_1, bp.image_1_2, bp.image_1_3, bp.image_rss, bp.type_post, bp.partner_url,
                    DATE_FORMAT(bp.created, '%d.%m.%Y') AS date_created, bp.citate_author_name,
                    bp.amount_to_displaying, bp.signature_to_sum, bp.text_body, bp.spec_project, bp.body_color,
                    bt.name AS tagName, bt.url AS tagUrl,
                    p.name AS partnerName
                    ,bp.type_announce_1_1, bp.type_announce_1_2, bp.type_announce_1_3
                    ,sp.name AS sp_name, sp.header AS sp_header, sp.external_url AS sp_url
                    ,sp.image_1_1 AS sp_image_1_1, sp.image_1_2 AS sp_image_1_2, sp.image_1_3 AS sp_image_1_3
                    ,sp.type_announce_1_1 AS sp_type_announce_1_1
                    ,sp.type_announce_1_2 AS sp_type_announce_1_2
                    ,sp.type_announce_1_3 AS sp_type_announce_1_3,
                    bw.bloger, bw.name as writerName, bw.announcement_img as blogImg, bw.id as writerId, bw.tag_url as writerTag
                  FROM blogposts AS bp
                  LEFT JOIN blogwriters AS bw ON bw.id = bp.writers
                  LEFT JOIN blogtags AS bt ON bt.id = bp.tags
                  LEFT JOIN partners AS p ON p.id = bp.partner
                  LEFT JOIN spec_projects AS sp ON sp.id = bp.spec_project
                  WHERE bp.id = ? AND bp.enabled = 1 AND (bw.enabled = 1 OR bp.spec_project OR bp.type_post = 12) ",
            $id);

        $this->db->query($query);
        $post = $this->db->result();
        if ($post) {
            $post->typeRow = '1_3';
            $postArray = Helper::formatPostList([$post], $post->typeRow);
            $post = $postArray[0];
        }
        return $post;
    }

    function getNewsById($id) {
        $query = sql_placeholder("SELECT n.id, NULL as blocks, n.tags, n.header as name, n.text_content as header, n.url, n.is_partner_material,
                            'news' as type_post, 
                            bw.name AS writer, bw.id AS writer_id, bw.private, bw.bloger, bw.name_genitive,
                            bw.image AS writerImage, bw.stub_image, bw.stub_link,
                            image_1_1, image_1_2, image_1_3, image_rss,
                            n.type_announce_1_1, n.type_announce_1_2, n.type_announce_1_3,
                            DATE_FORMAT(n.created, '%d.%m.%Y') AS date_created,
                            bt.url AS tagUrl, bt.name AS tagName, 
                            CONCAT ('news/', DATE_FORMAT(n.created, '%Y/%m/%d')) AS postUrl,
                            bw.bloger, bw.name as writerName, bw.announcement_img as blogImg, bw.id as writerId, bw.tag_url as writerTag,
                            n.format
                            
                            FROM news AS n
                          LEFT JOIN blogwriters AS bw ON bw.id=n.writers
                          LEFT JOIN blogtags AS bt ON bt.id = n.tags
                          WHERE n.id = ?", $id);
        if (!empty($query)){
            $this->db->query($query);
            $news = $this->db->result();
        }
        else{
            $news = null;
        }

        if ($news) {
            $news->typeRow = '1_3';
            $newsArray = Helper::formatPostList([$news], $news->typeRow);
            $news = $newsArray[0];
        }

        return $news;
    }

    function getStoryList()
    {
        $result = [];
        $storiesQuery = sql_placeholder("SELECT *
                              FROM groups AS g
                              WHERE g.enabled = 1
                              ORDER BY g.created DESC");

        $this->db->query($storiesQuery);
        $stories = $this->db->results();

        foreach ($stories AS $k => $story){
            $items[] = [
                "id" => $story->id,
                "name" => $story->name,
                "url" => $story->url,
                "date" => date("d.m.Y", strtotime($story->created)),
                "enabled" => $story->enabled,
                "postList" => $this->getGroupContent($story->id),
            ];
        }

        $this->db->query("SELECT * FROM seo_content WHERE section = 'plots'");
        $stories = $this->db->result();

        $seoTitle = $stories->seo_title ? $stories->seo_title : "Сюжеты / +1";
        $seoDescription = $stories->seo_description ? $stories->seo_description :  "Все, что вы хотели знать об изменениях в экономике, обществе и экосистеме, но боялись спросить.";
        $meta = [
            "title" => $seoTitle,
            "description" => $seoDescription,
            "keywords" => $stories->seo_keywords ,
            "ogTitle" => $seoTitle,
            "ogType" => "article",
            "ogDescription" => $seoDescription,
            "ogImage" => "/og-plus-one.ru.png",
            "ogSiteName" => "",
            "ogUrl" => "",
            "twitterCard" => "summary_large_image",
            "twitterTitle" => $seoTitle,
            "twitterDescription" =>	$seoDescription,
            "twitterUrl" => "",
            "twitterImage" => "/og-plus-one.ru.png",
            "twitterImageAlt" => $seoTitle,
            "twitterSite" => ""
        ];

        $result = [
            'header' => $stories->seo_title,
            'description' => $stories->header,
            'imageDesktop' => $stories->large_image ? "/files/seo_content/" . $stories->large_image : null,
            'imageMobile' => $stories->stub_image ? "/files/seo_content/" . $stories->stub_image : null,
            "items" => $items,
            "meta" => $meta

        ];

        return $result;
    }

    function getStoryItem($url)
    {
        $query = sql_placeholder("SELECT g.*
                              FROM groups AS g
                              WHERE url = ?
                              LIMIT 1", $url);
        $this->db->query($query);
        $group = $this->db->result();


        $groupInfo =  $this->getGroup($group->id);

        $writerInfo = [
            "id" => $group->id,
            "header" => $group->name,
            "imageDesktop" => $group->image ? "/files/plots/" . $group->image : null,
            "imageMobile" =>  $group->stub_image ? "/files/plots/" . $group->stub_image : null,
            "description" =>  $group->lead,
        ];

        $posts = $groupInfo->postList;

        $meta = [
            "title" => $group->meta_title,
            "description" => $group->meta_description,
            "keywords" => $group->meta_keywords,
            "ogTitle" => $group->meta_title,
            "ogType" => "article",
            "ogDescription" => $group->meta_description,
            "ogImage" => "/og-plus-one.ru.png",
            "ogSiteName" => "",
            "ogUrl" => "",
            "twitterCard" => "summary_large_image",
            "twitterTitle" => $group->meta_title,
            "twitterDescription" =>	$group->meta_description,
            "twitterUrl" => "",
            "twitterImage" => "/og-plus-one.ru.png",
            "twitterImageAlt" => "+1 — Проект об устойчивом развитии",
            "twitterSite" => ""
        ];

        $result = [
            "writerInfo" => $writerInfo,
            "items" => $posts,
            "meta" => $meta
        ];

        return $result;
    }

    function getGroupContent($id)
    {
        $groupItemIdListQuery = sql_placeholder("SELECT g.id, g.content_type, g.content_id
                              FROM group_items AS g
                              WHERE group_id = ?
                              ORDER BY g.order_num ASC", $id);

        $this->db->query($groupItemIdListQuery);
        $groupItemIdList = $this->db->results();
        $postList = [];
        foreach ($groupItemIdList as $groupItemId) {
            if ($groupItemId->content_type == 'blog') {
                $groupItem = $this->getPostById($groupItemId->content_id);
            } elseif ($groupItemId->content_type == 'news') {
                $groupItem = $this->getNewsById($groupItemId->content_id);
            }
            if (!empty($groupItem)) {
                $postList[] = $groupItem;
            }
        }
        return $postList;
    }

    function formatGroup($group)
    {
        $postList = $this->getGroupContent($group->id);

        $group->date = date('d.m.Y', strtotime($group->created));
        $group->postList = $postList;

        return $group;
    }

    function getGroup($id)
    {
        $query = sql_placeholder("SELECT g.id, g.name, g.created, g.enabled
                              FROM groups AS g
                              WHERE g.enabled = 1
                                AND g.id = ?
                              ORDER BY g.created DESC", $id);
        $this->db->query($query);
        $group = $this->db->result();

        return $this->formatGroup($group);

    }

    function getMainPageNews()
    {
        $sliderScheme = [[1, 3, 3], [3, 3, 3], [3, 3, 3]];
        $limit = 0;
        array_walk_recursive($sliderScheme, function($val) use (&$limit) { $limit += $val; });

        $nowSTR = (new DateTime('now'))->format('Y-m-d H:i:s');
        $andEnabled = " AND n.enabled = 1 AND n.created <= '{$nowSTR}' ";

        $newsQuery = sql_placeholder("SELECT header as name, header, url, CONCAT ('news/', DATE_FORMAT(created, '%Y/%m/%d')) AS postUrl
                        , DATE_FORMAT(created, '%d.%m.%Y') as date_created, image_rss as image_1_2, image_rss as image_1_3, NULL as font, NULL as format, 'news' as type_post
                        , '' as type_announce_1_2
                  FROM news AS n
                  LEFT JOIN blogwriters AS bw ON bw.id = n.writers
                  WHERE 1=1 {$andEnabled} AND bw.enabled = 1
                  ORDER BY created DESC LIMIT ?", $limit);
        $this->db->query($newsQuery);
        $news = $this->db->results();

        // Очистка знака переноса для новостей для главной
        foreach ($news as $index => $n) {
            $news[$index]->name = str_replace("&shy;", "", $n->name);
        }

        $rawNewsList = Helper::formatPostList($news);

        // DESKTOP
        //Дополняем количество элементов до полного слайда
        $newsCount = 0;
        $countRawNews = count($rawNewsList);
        foreach ($sliderScheme as $slideKey => $slide) {
            if ($countRawNews > 0) {
                foreach ($slide as $columnKey => $column) {
                    $newsCount += $column;
                    $countRawNews -= $column;
                }
            }
        }
        $missingNewsCount = $newsCount - count($rawNewsList);
        $emptyNews = $missingNewsCount > 0 ? array_fill(0, $missingNewsCount, null) : [];
        $newsListWithEmpty = array_merge($rawNewsList, $emptyNews);

        //Строим структуру массива по 7 новостей на один слайд
        $newsList = [];
        foreach ($sliderScheme as $slideKey => $slide) {
            if (!empty($newsListWithEmpty)) {
                $newsList[$slideKey] = [];
                foreach ($slide as $columnKey => $column) {
                    $newsList[$slideKey][$columnKey] = [];
                    while (count($newsList[$slideKey][$columnKey]) < $column) {
                        $newsList[$slideKey][$columnKey][] = array_shift($newsListWithEmpty);
                    }
                }
            }
        }

        // MOBILE
        // Отдельный набор новстей для мобильной версии
        $mobileNewsList = $rawNewsList;
        array_splice($mobileNewsList, 12);
        $mobileNewsCountOnPage = 4;
        $mobilePageCount = ceil(count($mobileNewsList) / $mobileNewsCountOnPage);
        $mobileMissingNewsCount = $mobileNewsCountOnPage * $mobilePageCount - count($mobileNewsList);
        $mobileEmptyNews = $mobileMissingNewsCount > 0 ? array_fill(0, $mobileMissingNewsCount, null) : [];
        $mobileNewsList = array_merge($mobileNewsList, $mobileEmptyNews);
        $mobileNewsList = array_chunk($mobileNewsList, $mobileNewsCountOnPage);

        $this->smarty->assign('newsList', $newsList);
        $this->smarty->assign('mobileNewsList', $mobileNewsList);

        return array(
            'newsList' => $newsList,
            'mobileNewsList' => $mobileNewsList
        );
    }


    public function getMainPage()
    {
        $query = sql_placeholder("SELECT * FROM main_page ORDER BY row_number ASC");
        $this->db->query($query);
        $mainPageRows = $this->db->results();
        foreach ($mainPageRows AS $k=>$row){

            if ($row->row_type === 'subscribe') {
                $row->typeRow = 'subscribe';
                $mainPageRows[$k] = array($row);
                continue;
            } elseif ($row->row_type === 'group') {
                $rowContentArray = json_decode($row->row_content);
                $group = $this->getGroup($rowContentArray[0]);
                if (count($group->postList) >= $this->minGroupItemCount) {
                    $group->typeRow = $row->row_type;
                    $mainPageRows[$k] = [$group];
                }
                continue;
            }

            $rowContentArray = json_decode($row->row_content);
            $postList = array();

            if (is_array($rowContentArray)) {

                $query = sql_placeholder("SELECT bp.id, bp.header, bp.name, bp.lead, bp.url, bp.url_target,
                    bp.format, bp.font, bp.is_partner_material,
                    bp.image_1_1, bp.image_1_2, bp.image_1_3, bp.image_rss, bp.type_post, bp.partner_url,
                    DATE_FORMAT(bp.created, '%d.%m.%Y') AS date_created, bp.citate_author_name,
                    bp.amount_to_displaying, bp.signature_to_sum, bp.text_body, bp.spec_project, bp.body_color,
                    sp.font_color as font_color, sp.color as marker_color,
                    bt.name AS tagName, bt.url AS tagUrl,
                    p.name AS partnerName
                    ,bp.type_announce_1_1, bp.type_announce_1_2, bp.type_announce_1_3
                    ,sp.name AS sp_name, sp.header AS sp_header, sp.external_url AS sp_url
                    ,sp.image_1_1 AS sp_image_1_1, sp.image_1_2 AS sp_image_1_2, sp.image_1_3 AS sp_image_1_3
                    ,sp.type_announce_1_1 AS sp_type_announce_1_1
                    ,sp.type_announce_1_2 AS sp_type_announce_1_2
                    ,sp.type_announce_1_3 AS sp_type_announce_1_3,
                    bw.bloger, bw.name as writerName, bw.announcement_img as blogImg, bw.id as writerId, bw.tag_url as writerTag
                  FROM blogposts AS bp
                  LEFT JOIN blogwriters AS bw ON bw.id = bp.writers
                  LEFT JOIN blogtags AS bt ON bt.id = bp.tags
                  LEFT JOIN partners AS p ON p.id = bp.partner
                  LEFT JOIN spec_projects AS sp ON sp.id = bp.spec_project
                  WHERE bp.id IN (?@) AND bp.enabled = 1 AND (bw.enabled = 1 OR bp.spec_project OR bp.type_post = 12) ",
                    $rowContentArray);

                $this->db->query($query);
                $posts = $this->db->results();

                foreach ($rowContentArray AS $key => $v) {
                    if (is_array($posts)) {
                        foreach ($posts AS $post) {
                            if ($post->id == $v) {
                                $post->typeRow = $row->row_type;
                                $postList[$key] = $post;
                            }
                        }
                    }
                }
                $mainPageRows[$k] = Helper::formatPostList($postList, $row->row_type);
            } else {

                if ($rowContentArray->type === 'banner') {
                    $post = new stdClass();
                    $post->row_type = 'banner';

                    $post->params = $rowContentArray->params;
                    $post->typeRow = $row->row_type;

                    $postList[$rowContentArray->type] = $post;
                }
                $mainPageRows[$k] = $postList;
            }
        }
        return $mainPageRows;
        $this->smarty->assign('posts', $mainPageRows);

        // video
        // state 1 = record
        // state 2 = live
        $query = sql_placeholder("SELECT id, image_1_1_c AS imageDesktop, image_1_4 AS imageMobile, state
                      FROM blogposts
                      WHERE type_post = 11
                      AND enabled = 1
                      AND (state = 2 OR state = 1)
                      ORDER BY id DESC LIMIT 1");
        $this->db->query($query);
        $mainVideo = $this->db->result();
        $typeVideo = 'live';
        $videoList = array();
        if ($mainVideo) {
            switch ($mainVideo->state) {
                case 1:
                    $typeVideo = 'record';
                    break;
                case 2:
                    $typeVideo = 'live';
                    break;
            }
            $query = sql_placeholder("SELECT * FROM videos WHERE parent_video_post = ? AND type_video = ?", $mainVideo->id, $typeVideo);
            $this->db->query($query);
            $videoList = $this->db->results();
        }


        $this->smarty->assign('firstVideo', count($videoList)> 0 ? $videoList[0]->video_url : null);
        $this->smarty->assign('videoImageDesktop', $mainVideo ? $mainVideo->imageDesktop : null);
        $this->smarty->assign('videoImageMobile', $mainVideo ? $mainVideo->imageMobile : null);
        $this->smarty->assign('videoList', $videoList);
        $this->smarty->assign('typeVideo', $typeVideo);

        $this->smarty->assign('hasCookieSbscr', @$_COOKIE['sbscr']);

        $sliderScheme = [[1, 3, 3], [3, 3, 3], [3, 3, 3]];
        $limit = 0;
        array_walk_recursive($sliderScheme, function($val) use (&$limit) { $limit += $val; });
        $newsQuery = sql_placeholder("SELECT header as name, header, url, CONCAT ('news/', DATE_FORMAT(created, '%Y/%m/%d')) AS tagUrl
                        , DATE_FORMAT(created, '%d.%m.%Y') as date_created, image_rss as image_1_2, image_rss as image_1_3, NULL as font, NULL as format, 'news' as type_post
                        , '' as type_announce_1_2
                  FROM news AS n
                  LEFT JOIN blogwriters AS bw ON bw.id = n.writers
                  WHERE n.enabled = 1 AND bw.enabled = 1
                  ORDER BY created DESC LIMIT ?", $limit);
        $this->db->query($newsQuery);
        $news = $this->db->results();
        $rawNewsList = Helper::formatPostList($news);

        // DESKTOP
        //Дополняем количество элементов до полного слайда
        $newsCount = 0;
        $countRawNews = count($rawNewsList);
        foreach ($sliderScheme as $slideKey => $slide) {
            if ($countRawNews > 0) {
                foreach ($slide as $columnKey => $column) {
                    $newsCount += $column;
                    $countRawNews -= $column;
                }
            }
        }
        $missingNewsCount = $newsCount - count($rawNewsList);
        $emptyNews = $missingNewsCount > 0 ? array_fill(0, $missingNewsCount, null) : [];
        $newsListWithEmpty = array_merge($rawNewsList, $emptyNews);

        //Строим структуру массива по 7 новостей на один слайд
        $newsList = [];
        foreach ($sliderScheme as $slideKey => $slide) {
            if (!empty($newsListWithEmpty)) {
                $newsList[$slideKey] = [];
                foreach ($slide as $columnKey => $column) {
                    $newsList[$slideKey][$columnKey] = [];
                    while (count($newsList[$slideKey][$columnKey]) < $column) {
                        $newsList[$slideKey][$columnKey][] = array_shift($newsListWithEmpty);
                    }
                }
            }
        }

        // MOBILE
        // Отдельный набор новстей для мобильной версии
        $mobileNewsList = $rawNewsList;
        array_splice($mobileNewsList, 12);
        $mobileNewsCountOnPage = 4;
        $mobilePageCount = ceil(count($mobileNewsList) / $mobileNewsCountOnPage);
        $mobileMissingNewsCount = $mobileNewsCountOnPage * $mobilePageCount - count($mobileNewsList);
        $mobileEmptyNews = $mobileMissingNewsCount > 0 ? array_fill(0, $mobileMissingNewsCount, null) : [];
        $mobileNewsList = array_merge($mobileNewsList, $mobileEmptyNews);
        $mobileNewsList = array_chunk($mobileNewsList, $mobileNewsCountOnPage);

        $this->smarty->assign('newsList', $newsList);
        $this->smarty->assign('mobileNewsList', $mobileNewsList);
        $this->smarty->assign('showModules', $this->config->showModules);
        $headerMenu = $this->getHeaderMenu();
        $this->smarty->assign('headerMenu', $headerMenu['menu']);
        $this->smarty->assign('rubrikaUrl', $headerMenu['currentRubrika']);
        $this->smarty->assign('typeUrl', $headerMenu['currentType']);

        $this->body = $this->smarty->fetch('mainPage.tpl');
        return $this->body;
    }

    function getSitemap()
    {
        $query = sql_placeholder("(SELECT 'blog' as type, b.url as url, DATE_FORMAT(b.created, '%Y-%m-%d') AS post_modified, t.url AS tag_url, b.created as created
                                     , b.type_post
                                     FROM blogposts AS b
                               INNER JOIN blogtags AS t on t.id=b.tags
                                LEFT JOIN blogwriters bw ON bw.id = b.writers
                                    WHERE b.enabled = 1 AND b.url != '' AND bw.enabled = 1)
                           UNION ALL
                          (        SELECT 'news' as type, n.url as url, DATE_FORMAT(n.created, '%Y-%m-%d') AS post_modified, CONCAT('news/', DATE_FORMAT(n.created, '%Y/%m/%d')) AS tag_url, n.created as created
                                     , NULL as type_post
                                     FROM news AS n
                                    WHERE n.enabled=1 AND n.url != '' )
                                    ORDER BY created DESC
                          ");
        $this->db->query($query);
        $result = $this->db->results();


        $strHead = <<<END
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
END;
        $strItem = "";

        $strFoot = <<<END
</urlset>
END;

        $newLinks = array(
            'platform' => 'platform',
            'manual' => 'manual',
            'author' => 'author',
            'plots' => 'story',
            'stable-development' => 'устойчивое-развитие',
        );
        foreach ($newLinks as $tag => $url) {


            $url = $this->root_url . "/" . $url;
            $modDate = (new DateTime())->format('Y-m-d');

            $strItem .= <<<END
    <url>
      <loc>$url</loc>
      <lastmod>$modDate</lastmod>
    </url>

END;
        }

        $query = sql_placeholder("
              SELECT pt.name, pt.url, pt.id, pt.enabled
                FROM post_tags AS pt
               WHERE pt.enabled = 1 AND TRIM(body) <> ''
            GROUP BY pt.id
            ORDER BY LOWER(name) ASC
        ");

        $this->db->query($query);
        $tags = $this->db->results();

        foreach ($tags as $tag) {

            $url = $this->root_url . "/устойчивое-развитие/" . $tag->url;
            $modDate = (new DateTime())->format('Y-m-d');

            $strItem .= <<<END
    <url>
      <loc>$url</loc>
      <lastmod>$modDate</lastmod>
    </url>

END;
        }

        foreach($result AS $post){
            if ($post->type == 'blog') {
                if (preg_match('#^(http|https)://#is', $post->url) === 1 ) {
                    continue;
                }
            }
            if ($post->type_post == Helper::TYPE_MANUAL ) {
                $url = $this->root_url . "/manual/" . $post->url;
            } else {
                $url = $this->root_url . "/" . $post->tag_url . "/" . $post->url;
            }
            $modDate = $post->post_modified;


            $strItem .= <<<END
   <url>
      <loc>$url</loc>
      <lastmod>$modDate</lastmod>
   </url>

END;

        }



        $sitemap = $strHead . $strItem . $strFoot;

        header("Content-type: text/xml; charset=UTF-8");
        header("Cache-Control: must-revalidate");
        header("Pragma: no-cache");
        header("Expires: -1");

        echo $sitemap;
        die();

    }

    function confirmSubscribe($hash)
    {
        $query = sql_placeholder("SELECT * FROM subscribe WHERE check_hash = ? LIMIT 1", $hash);
        $this->db->query($query);

        if ($this->db->num_rows() == 0) {
            return false;
        }

        $item = $this->db->result();

        if ($item){
            if (!$item->enabled) {
                $query = sql_placeholder("UPDATE subscribe SET enabled = 1 WHERE id = ? ", $item->id);
                $this->db->query($query);
            }
            $this->sendEmailToMailChimp($item->email);
        }

        setcookie("issubscribe", 1, 7842552499, "/");

        return true;
    }

    function sendEmailToMailChimp($emailAddress)
    {
        $mailChimpDc = "us15";

        $mailChimpApiKey = "d8837a0da0847891e0b0181f3fec534e-" . $mailChimpDc;

        $listId = "cf6f0a7d24";

        $url = "https://{$mailChimpDc}.api.mailchimp.com/3.0/lists/{$listId}/members";

        $ch = curl_init($url);

        $postFields = array(
            "email_address" => $emailAddress,
            "status" => "subscribed",
        );

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);   // возвращает веб-страницу
        curl_setopt($ch, CURLOPT_USERPWD, "user:" . $mailChimpApiKey);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postFields));

        $content = curl_exec($ch);
        curl_close($ch);
        return $content;
    }

    function fetchListSpecProjects($ajax = true)
    {
        $query = sql_placeholder(" SELECT *, DATE_FORMAT(created, '%d.%m.%Y %H:%i:%s') AS date_created FROM spec_projects WHERE enabled = 1 ORDER BY created DESC");
        $this->db->query($query);
        $countSpecProjects = $this->db->num_rows();
        $specProjects = $this->db->results();

        // Если не существует спецпроектов - ошибка 404
        if ($countSpecProjects == 0) {
            return false;
        }

        $items = array();
        foreach ($specProjects AS $k => $item) {
            $relatedPostName = $item->name;
            $relatedPostName = str_replace("<p>", "", $relatedPostName);
            $relatedPostName = str_replace("</p>", "", $relatedPostName);
            $item->name = $relatedPostName;

            $relatedPostHeader = $item->header;
            $relatedPostHeader = str_replace("<p>", "", $relatedPostHeader);
            $relatedPostHeader = str_replace("</p>", "", $relatedPostHeader);
            $item->header = $relatedPostHeader;

            $item->dateStr = Helper::dateTimeWord($item->date_created);
            $item->image_1_1 = $item->image_1_1 ? '/files/spec_project/' . $item->image_1_1 : null;
            $item->image_1_2 = $item->image_1_2 ? '/files/spec_project/' . $item->image_1_2 : null;
            $item->image_1_3 = $item->image_1_3 ? '/files/spec_project/' . $item->image_1_3 : null;
            $item->url = $item->external_url;
            $item->iconName  = "СПЕЦПРОЕКТ";
            $items[] = $item;
        }

        $this->title = "Спецпроекты +1";

        uasort($items, function ($a, $b) {
            $aDate = new DateTime($a->date_created);
            $bDate = new DateTime($b->date_created);
            if ($aDate < $bDate ) {
                return 1;
            } elseif ($aDate > $bDate) {
                return -1;
            }
            return 0;
        });

        if ($ajax == true){
            $item = new stdClass();
            $item->meta_title = "+1 — Спецпроекты";

            $item->meta_description = "Наглядно и информативно об устойчивом развитии, социальной ответственности и современном мире";

            $item->meta_keywords = "";

            $item->ogImage = "https://plus-one.ru/og-plus-one.ru.png?";

            $item->ogUrl = "https://plus-one.ru/special-projects";
            $item->ogSiteName = $this->settings->site_name;

            $meta = [
                'title' => $item->meta_title,
                'description' => $item->meta_description,
                'keywords' => $item->meta_keywords,
                'ogTitle' => $item->meta_title,
                'ogType' => 'article',
                'ogDescription' => $item->meta_description,
                'ogImage' => $item->ogImage,
                'ogSiteName' => $item->ogSiteName,
                'ogUrl' => $item->ogUrl,
                'twitterCard' => 'summary_large_image',
                'twitterTitle' => $item->meta_title,
                'twitterDescription' => $item->meta_description,
                'twitterUrl' => $item->ogUrl,
                'twitterImage' => $item->ogImage,
                'twitterImageAlt' => $item->meta_title,
                'twitterSite' => $item->ogSiteName
            ];

            return array('items' => $items, 'meta' => $meta);
        }

        // Передаем в шаблон
        $this->smarty->assign('specialProjects', $items);

        $this->body = $this->smarty->fetch('specialprojects.tpl');
        return $this->body;
    }

    function getSearch($queryString, $page = 1, $limit = 15, $ajax = false)
    {
        $limit = intval($limit);
        $page = intval($page);
        $offset = ($page - 1) * $limit;
        $results = array();
        $countAll = new stdClass();
        $countAll->count_all = 0;
        $nowDate  = new DateTime('now');
        if (isset($this->config->indexing) && $this->config->indexing) {
            require_once './Sphinx.class.php';
            $indexDBConfig = $this->config->sphinx;
            $indexDB = new Sphinx($indexDBConfig['name'], $indexDBConfig['host'], $indexDBConfig['user'], $indexDBConfig['password'], $indexDBConfig['port']);
            $tablePrefix = @$indexDBConfig['table_prefix'];
            $indexDB->connect();
            $indexDB->query(sql_placeholder("SELECT id, model FROM {$tablePrefix}posts
                            WHERE MATCH(?) AND date_created <= ?  AND enabled=1 
                         ORDER BY WEIGHT() DESC LIMIT {$offset}, {$limit}
                           OPTION ranker=expr('sum(lcs) + bm25 + date_created*0.000015 + sum(user_weight)')
                                               , field_weights=(name=25, header=10, lead=5, body=1)", $queryString, $nowDate->getTimestamp()));
            $resultIds = $indexDB->results();

            $indexDB->query(sql_placeholder("SELECT COUNT(*) as count_all FROM {$tablePrefix}postsIndex WHERE MATCH(?) AND date_created <= ? AND enabled=1", $queryString, $nowDate->getTimestamp()));
            $countAll = $indexDB->result();

            $indexDB->disconnect();
            $newsIds = array(0);
            $postsIds = array(0);
            $models = array(1 => 'posts', 2 => 'news');
            foreach ($resultIds as $res) {
                $itemID = $res->id;
                // 500000 Новый диапазон новостей ID для сфинкса
                if ($res->id > 500000) {
                    $newsIds [] = $res->id - 500000;
                    $itemID = $res->id - 500000;
                } else {
                    $postsIds[] = $res->id;
                }
                $results[$models[$res->model] . '_' . $itemID] = false;
            }

            $newsIdsStr = join(',', $newsIds);
            $postsIdsStr = join(',', $postsIds);

            $query = sql_placeholder("(SELECT 'posts' as type, b.id, b.blocks, b.tags, b.name, b.header, b.url, b.is_partner_material,
                        b.type_post, b.partner_url, b.post_tag, b.signature_to_sum, b.text_body, b.video_on_main, b.state, b.type_video,
                        b.amount_to_displaying, bw.name AS writer, bw.id AS writer_id, bw.private, bw.bloger, bw.name_genitive,
                        bw.image AS writerImage, bw.stub_image, bw.stub_link, p.name AS partner,
                        p.color AS partner_color, b.anounce_new_author, b.spec_project, b.conference,
                        b.citate_author_name, b.image_1_1, b.image_1_2, b.image_1_3, b.image_rss,
                        DATE_FORMAT(b.created, '%d.%m.%Y %H:%i:%s') AS date_created,
                        bt.name AS tagName, bt.url AS tagUrl, b.format, b.font, p.name AS partnerName,
                        bw.bloger, bw.name as writerName, bw.announcement_img as blogImg, bw.id as writerId, bw.tag_url AS writerTag,
                        b.type_announce_1_1, b.type_announce_1_2, b.type_announce_1_3,
                        bt.url as iconCode,
                        bt.name as iconName,
                        bt.url as btUrl,
                        NULL AS newsUrl,
                            b.type_announce_1_1, b.type_announce_1_2, b.type_announce_1_3
                            ,sp.name AS sp_name, sp.header AS sp_header, sp.external_url AS sp_url
                            ,sp.image_1_1 AS sp_image_1_1, sp.image_1_2 AS sp_image_1_2, sp.image_1_3 AS sp_image_1_3
                            ,sp.type_announce_1_1 AS sp_type_announce_1_1
                            ,sp.type_announce_1_2 AS sp_type_announce_1_2
                            ,sp.type_announce_1_3 AS sp_type_announce_1_3
                        FROM blogposts AS b
                      LEFT JOIN blogwriters AS bw ON bw.id=b.writers
                      LEFT JOIN partners AS p ON p.id=b.partner
                      LEFT JOIN blogtags AS bt ON bt.id = b.tags
                      LEFT JOIN spec_projects AS sp ON sp.id=b.spec_project
                      -- LEFT JOIN conferences AS conf ON conf.id=b.conference
                      WHERE b.enabled=1 AND b.blocks != 'ticker' AND (b.id IN ({$postsIdsStr})) AND (bw.enabled = 1 OR b.spec_project) )
                  UNION ALL
                        (SELECT 'news' as type, n.id, NULL as blocks, n.tags, n.header as name, n.text_content as header, n.url, n.is_partner_material,
                        'news' as type_post, NULL as partner_url, NULL as post_tag, NULL as signature_to_sum, NULL as text_body, NULL as video_on_main, NULL as state, NULL as type_video,
                        NULL as amount_to_displaying, bw.name AS writer, bw.id AS writer_id, bw.private, bw.bloger, bw.name_genitive,
                        bw.image AS writerImage, bw.stub_image, bw.stub_link, NULL as partner,
                        NULL AS partner_color, NULL as anounce_new_author, NULL as spec_project, NULL as conference,
                        NULL as citate_author_name, image_1_1, image_1_2, image_1_3, image_rss,
                        DATE_FORMAT(n.created, '%d.%m.%Y') AS date_created,
                        bt.name AS tagName, CONCAT ('news', '/', DATE_FORMAT(n.created, '%Y/%m/%d')) AS tagUrl, 'gray' as format, NULL as font, NULL as partnerName,
                        bw.bloger, bw.name as writerName, bw.announcement_img as blogImg, bw.id as writerId, bw.tag_url AS writerTag,
                        n.type_announce_1_1, n.type_announce_1_2, n.type_announce_1_3,
                        bt.url as iconCode,
                        bt.name as iconName,
                        bt.url as btUrl,
                        CONCAT ('news', '/', DATE_FORMAT(n.created, '%Y/%m/%d')) AS newsUrl,
                            NULL as sp_type_announce_1_1, NULL as sp_type_announce_1_2, NULL as sp_type_announce_1_3
                            ,NULL AS sp_name, NULL AS sp_header, NULL AS sp_url
                            ,NULL AS sp_image_1_1, NULL AS sp_image_1_2, NULL AS sp_image_1_3
                            ,NULL AS sp_type_announce_1_1
                            ,NULL AS sp_type_announce_1_2
                            ,NULL AS sp_type_announce_1_3
                        FROM news AS n
                      LEFT JOIN blogwriters AS bw ON bw.id=n.writers
                      LEFT JOIN blogtags AS bt ON bt.id = n.tags
                      WHERE n.enabled=1 AND (n.id IN ({$newsIdsStr})) AND bw.enabled = 1)
                   ");


            $this->db->query($query);
            $searchItems = $this->db->results();

            $return['counterRecords'] = count($results);

            foreach ($searchItems AS $item) {
                $results[$item->type . '_' . $item->id] = $item;
            }

            $results = Helper::formatPostList(array_filter($results, function ($item) {
                return $item;
            }), '1_3');

            foreach ($results as & $res ) {
                if ($res->type === 'news') {
                    $res->iconCode = $res->btUrl;
                    $res->tagUrl = $res->btUrl;
                    $res->url = $res->newsUrl . $res->url;
                }
            }

            return array(
                'items' => array_values($results),
                'countItems' => $countAll->count_all
            );
        }

        return array(
            'items' => [],
            'countItems' => 0,
            'error' => 'Error sphinx indexing... '
        );
    }

    function getSearchAutocomplette($searchQuery)
    {
        $results = array();
        $nowDate = new DateTime('now');
        if (isset($this->config->indexing) && $this->config->indexing) {
            require_once './Sphinx.class.php';
            $indexDBConfig = $this->config->sphinx;
            $indexDB = new Sphinx($indexDBConfig['name'], $indexDBConfig['host'], $indexDBConfig['user'], $indexDBConfig['password'], $indexDBConfig['port']);
            $tablePrefix = @$indexDBConfig['table_prefix'];
            $indexDB->connect();
            $indexDB->query(sql_pholder("
                    SELECT id, model FROM {$tablePrefix}posts
                     WHERE MATCH(?) AND date_created <= ? AND enabled = 1
                     ORDER BY WEIGHT() DESC
                     LIMIT 15
                     OPTION  ranker=expr('sum(lcs) + bm25 + date_created*0.000015 + sum(user_weight)')
                                               , field_weights=(name=25, header=10, lead=5, body=1)
                ", $searchQuery, $nowDate->getTimestamp()));

            $resultIds = $indexDB->results();

            $indexDB->disconnect();

            $newsIds = array(0);
            $postsIds = array(0);
            $models  = array(1=>'blog', 2 => 'news');
            foreach ($resultIds as $res) {
                $itemID = $res->id;
                // 500000 Новый диапазон новостей ID для сфинкса
                if ($res->id > 500000) {
                    $newsIds [] = $res->id - 500000;
                    $itemID = $res->id - 500000;
                } else {
                    $postsIds [] = $res->id;
                }
                $results[$models[$res->model].'_'.$itemID] = false;
            }

            $newsIdsStr = join(',', $newsIds);
            $postsIdsStr = join(',', $postsIds);

            $query = sql_placeholder("(SELECT  'blog' as type, b.id, b.blocks, b.tags, b.name, b.header, CONCAT ('/', bt.url) AS blogTagUrl, b.url, b.type_post, b.is_partner_material,
                            b.type_post, b.partner_url, b.post_tag, bw.name AS writer, bw.id AS writer_id, bw.private, bw.bloger, bw.name_genitive,
                            bw.image AS writerImage, bw.stub_image, bw.stub_link, p.name AS partner,
                            p.color AS partner_color, b.anounce_new_author, b.spec_project, b.conference,
                            b.citate_author_name, b.image_1_1, b.image_1_2, b.image_1_3, b.image_rss,
                            DATE_FORMAT(b.created, '%d.%m.%Y %H:%i:%s') AS date_created,
                            bt.name AS tagName, b.format, b.font, p.name AS partnerName,
                            bw.bloger, bw.name as writerName, bw.announcement_img as blogImg, bw.id as writerId, bw.tag_url as writerTag
                            FROM blogposts AS b
                          LEFT JOIN blogwriters AS bw ON bw.id=b.writers
                          LEFT JOIN partners AS p ON p.id=b.partner
                          LEFT JOIN blogtags AS bt ON bt.id = b.tags
                          WHERE b.enabled=1 AND b.blocks != 'ticker' AND (b.id IN ({$postsIdsStr})) AND bw.enabled = 1 AND b.type_post <> 12)
                      UNION ALL
                            (SELECT 'news' as type, n.id, NULL as blocks, n.tags, n.header as name, n.text_content as header, CONCAT ('/news/', DATE_FORMAT(n.created, '%Y/%m/%d')) as blogTagUrl, n.url, NULL as type_post, n.is_partner_material,
                            NULL as type_post, NULL as partner_url, NULL as post_tag, bw.name AS writer, bw.id AS writer_id, bw.private, bw.bloger, bw.name_genitive,
                            bw.image AS writerImage, bw.stub_image, bw.stub_link, NULL as partner,
                            NULL AS partner_color, NULL as anounce_new_author, NULL as spec_project, NULL as conference,
                            NULL as citate_author_name, NULL as image_1_1, NULL as image_1_2, NULL as image_1_3, NULL as image_rss,
                            DATE_FORMAT(n.created, '%d.%m.%Y') AS date_created,
                            bt.name AS tagName, NULL as format, NULL as font, NULL as partnerName,
                            bw.bloger, bw.name as writerName, bw.announcement_img as blogImg, bw.id as writerId, bw.tag_url as writerTag
                            FROM news AS n
                          LEFT JOIN blogwriters AS bw ON bw.id=n.writers
                          LEFT JOIN blogtags AS bt ON bt.id = n.tags
                          WHERE n.enabled=1 AND (n.id IN ({$newsIdsStr})) AND bw.enabled = 1)");

        } else {
            $queryString = '%' . $searchQuery . '%';
            $query = sql_placeholder("
                                  (SELECT b.id AS id, 'blog' as type, b.name, b.url, CONCAT ('/', bt.url) AS blogTagUrl, b.is_partner_material
                                  FROM blogposts AS b
                                  INNER JOIN blogtags AS bt ON bt.id = b.tags
                                  WHERE b.enabled=1 AND (b.name like ? OR b.header LIKE ? OR b.lead LIKE ? OR b.body LIKE ?) AND b.type_post <> 12)
                                  UNION ALL
                                  (SELECT n.id AS id, 'news' as type, n.header as name, n.url, CONCAT ('news/', DATE_FORMAT(n.created, '%Y/%m/%d')) AS blogTagUrl, n.is_partner_material
                                  FROM news AS n
                                  WHERE n.enabled=1 AND (n.header like ? OR n.text_content LIKE ? OR n.lead LIKE ? OR n.article_text LIKE ?))
                                  LIMIT 15",
                $queryString, $queryString, $queryString, $queryString, $queryString, $queryString, $queryString, $queryString);
        }
        $this->db->query($query);
        $searchItems = $this->db->results();

        $return['counterRecords'] = count($results);

        foreach ($searchItems AS $item){
            if ($item->type_post == Helper::TYPE_MANUAL) {
                $itemUrl =  '/manual/' . $item->url;
            } else {
                $itemUrl = $item->blogTagUrl . '/' . $item->url;

            }
            if ($item->type == 'blog') {
                if (preg_match('#^(http|https)://#is', $item->url) === 1) {
                    $itemUrl = $item->url;
                }
            }

            $results[$item->type.'_'.$item->id] = array(
                'label' => str_replace("&#151;", "—", html_entity_decode(strip_tags($item->name))),
                'url' => $itemUrl
            );
        }

        return array_values($results);
    }


    function getPostListToInfinityScroll($rubrikaUrl)
    {
        if ($_GET['mode'] == 'preview'){
            $andEnabled = "";
        }
        else{
            $andEnabled = " AND b.enabled = 1 ";
        }

        $nowDate = new DateTime();

        // Выбираем статью из базы
        $query = sql_placeholder("SELECT b.id, b.body, b.tags, b.partner, b.partner_url, b.name, b.lead, b.header, b.url, b.block_color, b.is_partner_material,
                                   b.type_post, bw.name AS writer, bw.id AS writer_id, b.post_tag,
                                   DATE_FORMAT(b.created, '%d.%m.%Y') AS post_created,
                                   b.meta_title, b.meta_description, b.meta_keywords, p.name AS partner_name,
                                   p.color AS partner_color, b.signature_to_sum, b.text_body, b.amount_to_displaying,
                                   b.header_on_page, b.image_rss, b.image_1_1, b.image_1_1_c, b.image_1_2, b.image_1_3,
                                   b.spec_project, b.conference, b.body_color, b.font_color, b.border_color,
                                   bw.name AS writer, bw.id AS writer_id, bw.private, bw.bloger, bw.name_genitive,
                                   bw.image AS writerImage, bw.useplatforma AS platformPost,
                                   sp.name AS specProjectName, sp.external_url AS specProjectUrl, sp.color AS specProjectColor,
                                   conf.name AS confName, conf.external_url AS confUrl, conf.color AS confColor, b.citate_author_name,
                                   b.seo_title, b.seo_description, b.seo_keywords, b.show_fund, b.shop_article_id, b.fund_resource_id, p.name AS partnerName, b.header_donate_form,
                                   bw.bloger, bw.name as writerName, bw.announcement_img as blogImg, bw.id as writerId, bw.tag_url as writerTag
                                  FROM blogposts AS b
                                  LEFT JOIN blogwriters AS bw ON bw.id=b.writers
                                  LEFT JOIN relations_post_tags AS rel ON rel.post_id=b.id
                                  LEFT JOIN partners AS p ON p.id=b.partner
                                  LEFT JOIN spec_projects AS sp ON sp.id=b.spec_project
                                  LEFT JOIN conferences AS conf ON conf.id=b.conference
                                  INNER JOIN blogtags AS bt ON bt.id = b.tags
                                  WHERE
                                    b.created <= ?
                                    AND b.id = ?
                                    AND bt.url = ?
                                    AND b.spec_project = 0
                                    AND b.type_post NOT IN (5, 12, 11, 14)
                                    {$andEnabled}

                                  ORDER BY b.created DESC
                                  LIMIT 1", $nowDate->format('Y-m-d H:i:s'), $_GET['post_id'], $rubrikaUrl);
        $this->db->query($query);
        $item = $this->db->result();

        if ($_GET['mode'] == 'preview'){
            $andEnabled = "";
        }
        else{
            $andEnabled = " AND b.enabled = 1 ";
        }

        $nowDate = new DateTime();



//        // Выбираем статью из базы
//        $query = sql_placeholder("SELECT b.id
//                                  FROM blogposts AS b
//                                  LEFT JOIN blogwriters AS bw ON bw.id=b.writers
//                                  LEFT JOIN relations_post_tags AS rel ON rel.post_id=b.id
//                                  LEFT JOIN partners AS p ON p.id=b.partner
//                                  LEFT JOIN spec_projects AS sp ON sp.id=b.spec_project
//                                  LEFT JOIN conferences AS conf ON conf.id=b.conference
//                                  INNER JOIN blogtags AS bt ON bt.id = b.tags
//                                  WHERE
//                                    b.created <= ?
//                                    AND bt.url = ?
//                                    AND b.spec_project = 0
//                                    AND b.type_post NOT IN (5, 12, 11)
//                                    {$andEnabled}
//                                    {$andNotInIds}
//                                  ORDER BY b.created DESC
//                                  LIMIT 1", $nowDate->format('Y-m-d H:i:s'), $rubrikaUrl);
//        $this->db->query($query);
//        $nextItem = $this->db->result();

        // замена подписи автора в старых постах
        $pattern = '#<span class="author">(.+?)</span>#is';
        preg_match($pattern, $item->body, $arr, PREG_OFFSET_CAPTURE);
        $newAuthorNameStr = str_ireplace("Автор:", "", $arr[0][0]);
        $newAuthorNameStr = strip_tags($newAuthorNameStr);
        $newHtmlAuthorSign = '<div class="articleAuthor"><p class="articleAuthor_h">Автор</p><p class="articleAuthor_name">' . $newAuthorNameStr . '</p></div>';
        $item->body =  preg_replace($pattern, $newHtmlAuthorSign, $item->body);

        $this->bodyColor = $item->body_color;

        $this->customColors = false;

        if ($item->font_color != "" && $item->border_color != ""){
            $this->customColors = true;
        }


        $item->body = str_replace("<p>&nbsp;</p>", "", $item->body);

        $query = "SELECT name, url, color FROM blogtags WHERE id=" . $item->tags;
        $this->db->query($query);
        $item->tags = $this->db->result();

        $query = "SELECT t.name, t.url FROM relations_postitem_tags AS rel
                              INNER JOIN post_tags AS t ON t.id=rel.posttag_id
                              WHERE rel.post_id=" . $item->id . " AND t.enabled=1 ORDER BY t.name";
        $this->db->query($query);
        $item->postTags = $this->db->results();

        foreach ($item->postTags AS $k=>$v){
            $item->postTags[$k]->url = 'tag/' . $item->tags->url . "/" . $v->url;
        }

        $postTagMainPage = $this->getPostTagMainPage($item->post_tag);
        $item->postTagMainPage = $postTagMainPage;

        $item->postDateStr = $item->post_created; //$this->dateTimeWord($item->post_created);

        if ($item->type_post == 7){
            $analitycData = $this->getAnalitycData($item->id, $item->writer_id);
            $item->analitycData = $analitycData;

            $item->postTags = $this->getPostTagTwoLevel($item->id);
        }

        $name = $item->name;
        $name = str_replace("<p>", "", $name);
        $name = str_replace("</p>", "", $name);
        $item->name = $name;

        $citateName = str_replace("<p>", "", $item->name);
        $citateName = str_replace("</p>", "", $citateName);

        $citateHeader = str_replace("<p>", "", $item->header);
        $citateHeader = str_replace("</p>", "", $citateHeader);

        $item->citateText = $citateName . " <span>" . $citateHeader . "</span>";


        $item->typePost = Helper::getTypeVisualBlock($item->type_post);

        // вычисляем примерное время на прочтение поста
        $item->typePost = Helper::getTypeVisualBlock($item->type_post);
        if ($item->typePost == 'imageday' || $item->typePost == 'diypost'){
            $totalReadTime = 3; // #PLUS1-186 для этого типа поста время на чтение всегда 3 минуты
        }
        else{
            $lenghtLead = iconv_strlen(strip_tags($item->lead));
            $lenghtBody = iconv_strlen(strip_tags($item->body));
            $totalLenght = intval($lenghtBody + $lenghtLead);
            $totalReadTime = round(($totalLenght / 1000) / 2);

            if ($totalReadTime == 0){
                $totalReadTime = 1; // #PLUS1-257 Выводить 1 минуту там где получается 0 минут на чтение
            }
        }

        $totalReadTimeCaption = $this->getHeaderMenuSigns('минут', 'минута', 'минуты', $totalReadTime);

        $item->dateStr = $item->post_created;

        $item->fullUrl = $rubrikaUrl . "/" . $item->url;

        // Устанавливаем метатеги для страницы с этой записью
        $this->title = "";

        if ($item->typePost == 'factday'){
            $this->title = htmlspecialchars(strip_tags(trim($item->name))) . " / +1";
        }
        else{
            if ($item->seo_title !=""){
                $this->title = htmlspecialchars(strip_tags(trim($item->seo_title))) . " / +1";
            }
            else{
                $this->title = htmlspecialchars(strip_tags(trim($item->meta_title))) . " / +1";
            }
        }

        $this->description = "";
        if ($item->seo_description != ""){
            $this->description = strip_tags($item->seo_description);
        }
        else{
            if (!empty($item->lead)){
                $this->description = strip_tags($item->lead);
            }
            elseif(!empty($item->header)){
                $this->description = strip_tags($item->header);
            }
        }

        $this->keywords = $item->seo_keywords;

        $item->meta->title = htmlspecialchars(strip_tags(trim($item->meta_title)))  . " / +1";
        $item->meta->description = htmlspecialchars(strip_tags(trim($item->meta_description)));
        $item->meta->keywords = htmlspecialchars(strip_tags(trim($item->meta_keywords)));
        $item->meta->headerMenu = $this->getHeaderMenu();

        $this->ogImage = $this->config->fileStorage['domainFilePath'] . "/files/blogposts/" . $item->image_rss;
        $this->ogUrl = $this->config->fileStorage['domainFilePath'] . "/" . $rubrikaUrl . "/" . $item->url;
        $this->ogSiteName = $this->settings->site_name;

        $relatedPosts = Helper::getRelatedPostsByParent($item->id, 'post', $nowDate);
        $item->relatedPosts = Helper::formatPostList($relatedPosts, '1_3');

        $result = array(
            'item' => $item,
            'meta' => [
                'title' => $item->meta_title . " / +1",
                'description' => $item->meta_description,
                'keywords' => $item->meta_keywords,
                'ogTitle' => $item->meta_title . " / +1",
                'ogType' => 'article',
                'ogDescription' => $item->meta_description,
                'ogImage' => $this->ogImage,
                'ogSiteName' => $this->ogSiteName,
                'ogUrl' => $this->ogUrl,
                'twitterCard' => 'summary_large_image',
                'twitterTitle' => $item->meta_title . " / +1",
                'twitterDescription' => $item->meta_description,
                'twitterUrl' => $this->ogUrl,
                'twitterImage' => $this->ogImage,
                'twitterImageAlt' => $item->meta_title . " / +1",
                'twitterSite' => $this->ogSiteName
            ],
        );

        header("Content-type: application/json; charset=UTF-8");
        header("Cache-Control: must-revalidate");
        header("Pragma: no-cache");
        header("Expires: -1");

        print json_encode($result);
        exit();
    }

    /**
     * Отображение отдельной статьи
     *
     */
    function fetch_item($url, $rubrika_url, $ajax = false)
    {
        $nowDate = new DateTime();

        if (isset($_GET['mode']) && $_GET['mode'] == 'preview'){
            $andEnabled = "";
        }
        else{
            $nowSTR = $nowDate->format('Y-m-d H:i:s');
            $andEnabled = " AND b.enabled = 1 AND b.created <= '{$nowSTR}' ";
        }

        // Выбираем статью из базы
        $query = sql_placeholder("SELECT b.id, b.body, b.tags, b.partner, b.partner_url, b.name, b.lead, b.header, b.url, b.block_color, b.is_partner_material,
                                   b.type_post, bw.name AS writer, bw.id AS writer_id, b.post_tag, b.header_rss,
                                   DATE_FORMAT(b.created, '%d.%m.%Y') AS post_created,
                                   b.meta_title, b.meta_description, b.meta_keywords, p.name AS partner_name,
                                   p.color AS partner_color, b.signature_to_sum, b.text_body, b.amount_to_displaying,
                                   b.header_on_page, b.image_rss, b.image_1_1, b.image_1_1_c, b.image_1_2, b.image_1_3,
                                   b.spec_project, b.conference, b.body_color, b.font_color, b.border_color,
                                   bw.name AS writer, bw.id AS writer_id, bw.private, bw.bloger, bw.name_genitive,
                                   bw.image AS writerImage, bw.useplatforma AS platformPost,
                                   sp.name AS specProjectName, sp.external_url AS specProjectUrl, sp.color AS specProjectColor,
                                   conf.name AS confName, conf.external_url AS confUrl, conf.color AS confColor, b.citate_author_name,
                                   b.seo_title, b.seo_description, b.seo_keywords, b.show_fund, b.shop_article_id, b.fund_resource_id, p.name AS partnerName, b.header_donate_form,
                                   bw.bloger, bw.name as writerName, bw.announcement_img as blogImg, bw.id as writerId, bw.tag_url as writerTag
                                  FROM blogposts AS b
                                  LEFT JOIN blogwriters AS bw ON bw.id=b.writers
                                  LEFT JOIN relations_post_tags AS rel ON rel.post_id=b.id
                                  LEFT JOIN partners AS p ON p.id=b.partner
                                  LEFT JOIN spec_projects AS sp ON sp.id=b.spec_project
                                  LEFT JOIN conferences AS conf ON conf.id=b.conference
                                  WHERE b.url = ? {$andEnabled}
                                  LIMIT 1", $url);
        $this->db->query($query);

        // Если не существует такая статья - ошибка 404
        if ($this->db->num_rows() == 0) {
            $this->findByHistoryUrl($url, $rubrika_url);
            return false;
        }
        $item = $this->db->result();

        /* get next post ids */
        $nowSTR = $nowDate->format('Y-m-d H:i:s');
        $andEnabled = " AND b.enabled = 1 AND b.created <= '{$nowSTR}' ";

        if ($rubrika_url === 'manual') {
            $query = sql_placeholder("SELECT b.id, bt.url as bt_url
                                  FROM blogposts AS b
                                  LEFT JOIN blogwriters AS bw ON bw.id=b.writers
                                  LEFT JOIN relations_post_tags AS rel ON rel.post_id=b.id
                                  LEFT JOIN partners AS p ON p.id=b.partner
                                  LEFT JOIN spec_projects AS sp ON sp.id=b.spec_project
                                  LEFT JOIN conferences AS conf ON conf.id=b.conference
                                  INNER JOIN blogtags AS bt ON bt.id = b.tags
                                  WHERE
                                    b.created <= ?
                                    AND b.id != ?
                                    AND b.spec_project = 0
                                    AND (bw.id IS NULL OR bw.useplatforma <> 1)
                                    AND b.type_post IN (?@)
                                    {$andEnabled}
                                  ORDER BY b.created DESC
                                  LIMIT 60", $nowDate->format('Y-m-d H:i:s'), $item->id, array(Helper::TYPE_MANUAL)
                    );
        } else {
            $query = sql_placeholder("SELECT b.id, bt.url as bt_url
                                  FROM blogposts AS b
                                  LEFT JOIN blogwriters AS bw ON bw.id=b.writers
                                  LEFT JOIN relations_post_tags AS rel ON rel.post_id=b.id
                                  LEFT JOIN partners AS p ON p.id=b.partner
                                  LEFT JOIN spec_projects AS sp ON sp.id=b.spec_project
                                  LEFT JOIN conferences AS conf ON conf.id=b.conference
                                  INNER JOIN blogtags AS bt ON bt.id = b.tags
                                  WHERE
                                    b.created <= ?
                                    AND b.id != ?
                                    AND bt.url = ?
                                    AND b.spec_project = 0
                                    AND (bw.id IS NULL OR bw.useplatforma <> 1)
                                    AND b.type_post NOT IN (?@)
                                    {$andEnabled}
                                  ORDER BY b.created DESC
                                  LIMIT 60", $nowDate->format('Y-m-d H:i:s'), $item->id, $rubrika_url,
                array(Helper::TYPE_PARTNER_ANNOUNCE, 5, 12, 11));
        }
        $this->db->query($query);
        $nextItems = $this->db->results();

        foreach ($nextItems AS $ind => $nextItem){
            $nextPostUrlList[] = 'getPost/' . $nextItem->bt_url . "/" . $nextItem->id . '?' . 'c=' . $ind;
        }

        // замена подписи автора в старых постах
        $pattern = '#<span class="author">(.+?)</span>#is';
        preg_match($pattern, $item->body, $arr, PREG_OFFSET_CAPTURE);
        $newAuthorNameStr = str_ireplace("Автор:", "", $arr[0][0]);
        $newAuthorNameStr = strip_tags($newAuthorNameStr);
        $newHtmlAuthorSign = '<div class="articleAuthor"><p class="articleAuthor_h">Автор</p><p class="articleAuthor_name">' . $newAuthorNameStr . '</p></div>';
        $item->body =  preg_replace($pattern, $newHtmlAuthorSign, $item->body);

        $this->bodyColor = $item->body_color;

        $this->customColors = false;

        if ($item->font_color != "" && $item->border_color != ""){
            $this->customColors = true;
        }


        $item->body = str_replace("<p>&nbsp;</p>", "", $item->body);

        $query = "SELECT name, url, color FROM blogtags WHERE id=" . $item->tags;
        $this->db->query($query);
        $item->tags = $this->db->result();

        $query = "SELECT t.name, t.url FROM relations_postitem_tags AS rel
                              INNER JOIN post_tags AS t ON t.id=rel.posttag_id
                              WHERE rel.post_id=" . $item->id . " AND t.enabled=1 ORDER BY t.name";
        $this->db->query($query);
        $item->postTags = $this->db->results();

        foreach ($item->postTags AS $k=>$v){
            $item->postTags[$k]->url = 'tag/' . $item->tags->url . "/" . $v->url;
        }

        $postTagMainPage = $this->getPostTagMainPage($item->post_tag);
        $item->postTagMainPage = $postTagMainPage;

        $item->postDateStr = $item->post_created;

        $query = "SELECT * FROM partners WHERE id=" . $item->partner;
        $this->db->query($query);
        $item->partner = $this->db->result();

        $relatedPosts = Helper::getRelatedPostsByParent($item->id, 'post', $nowDate);
        $item->relatedPosts = Helper::formatPostList($relatedPosts, '1_3');

        // партнерские посты
        $query = sql_placeholder('SELECT p.post_id AS id, p.header AS name, p.subheader AS header, p.link AS postUrl,
                                      p.image, t.name AS tagName, t.url AS tagUrl, pr.name AS partner_name, pr.color AS partner_color

                                      FROM partners_posts_links AS p
                                    JOIN blogtags AS t ON t.id=p.rubrika_id
                                    LEFT JOIN partners AS pr ON pr.id=p.partner_id
                                    WHERE p.post_id=?

                                    ORDER BY p.id ASC
                                    LIMIT 3',
                                $item->id);
        $this->db->query($query);
        $partnersPosts = $this->db->results();

        foreach ($partnersPosts AS $key => $post) {

            $post->tagUrl = "" . $post->tagUrl;

            $post->dateStr = '';

            $post->typePost = 'post';

            $postFormat = PresetFormat::getPresetFormatByVisualBlockAndFormat('post', 'gray');
            $postFont = PresetFormat::getPresetFontByKey('MullerBold');

            $post->frontClass = $postFormat['frontClass'];
            $post->fontClass = $postFont['frontClass'];

            if ($post->image) {
                $post->image = "/files/blogposts/" . $post->image;
                $post->format = 'picture';
            }

            $partnersPosts[$key] = $post;
        }


        $item->partnersPosts = $partnersPosts;

        if ($item->type_post == 7){
            $analitycData = $this->getAnalitycData($item->id, $item->writer_id);
            $item->analitycData = $analitycData;

            $item->postTags = $this->getPostTagTwoLevel($item->id);
        }

        $name = $item->name;
        $name = str_replace("<p>", "", $name);
        $name = str_replace("</p>", "", $name);
        $name = str_replace("&shy;", "", $name);
//        $name = preg_replace('/<(\w+)>(.+?)<\/\1>/u', "[$1]$2[/$1]", $name);
        $item->name = $name;

        $citateName = str_replace("<p>", "", $item->name);
        $citateName = str_replace("</p>", "", $citateName);
        $citateName = str_replace("&shy;", "", $citateName);

        $citateHeader = str_replace("<p>", "", $item->header);
        $citateHeader = str_replace("</p>", "", $citateHeader);
        $citateHeader = str_replace("&shy;", "", $citateHeader);

        $item->citateText = $citateName . " <span>" . $citateHeader . "</span>";

        $item->typePost = Helper::getTypeVisualBlock($item->type_post);

        // Устанавливаем метатеги для страницы с этой записью
        $this->title = "";

        if ($item->typePost == 'factday'){
            $this->title = htmlspecialchars(strip_tags(trim($item->name))) . " / +1";
        }
        else{
            if ($item->seo_title !=""){
                $this->title = htmlspecialchars(strip_tags(trim($item->seo_title))) . " / +1";
            }
            else{
                $this->title = htmlspecialchars(strip_tags(trim($item->meta_title))) . " / +1";
            }
        }



        $this->description = "";
        if ($item->seo_description != ""){
            $this->description = strip_tags($item->seo_description);
        }
        else{
            if (!empty($item->lead)){
                $this->description = strip_tags($item->lead);
            }
            elseif(!empty($item->header)){
                $this->description = strip_tags($item->header);
            }
        }

        $this->keywords = $item->seo_keywords;

        $item->meta->title = htmlspecialchars(strip_tags(trim($item->meta_title)));
        $item->meta->description = htmlspecialchars(strip_tags(trim($item->meta_description)));
        $item->meta->keywords = htmlspecialchars(strip_tags(trim($item->meta_keywords)));
        $item->meta->headerMenu = $this->getHeaderMenu();

        $this->ogImage = $this->config->fileStorage['domainFilePath'] . "/files/blogposts/" . $item->image_rss;

        $this->ogImage_1_1 = "files/blogposts/" . $item->image_1_1 . "?" . time();
        $this->ogImage_1_1_c = "files/blogposts/" . $item->image_1_1_c . "?" . time();
        $this->ogImage_1_2 = $this->config->fileStorage['domainFilePath'] . "/files/blogposts/" . $item->image_1_2;
        $this->ogImage_1_3 = "files/blogposts/" . $item->image_1_3 . "?" . time();
        $this->ogImage_galeries = "";

        $queryString = @$_SERVER["QUERY_STRING"] ? '?'. @$_SERVER["QUERY_STRING"] : "";
        $this->ogUrl = $this->config->fileStorage['domainFilePath'] . "/" . $rubrika_url . "/" . $url . $queryString;

        $this->ogSiteName = $this->settings->site_name;

        // вычисляем примерное время на прочтение поста
        $item->typePost = Helper::getTypeVisualBlock($item->type_post);
        if (in_array($item->typePost, array('imageday', 'diypost'))){
            $totalReadTime = 3; // #PLUS1-186 для этого типа поста время на чтение всегда 3 минуты
        }
        else{
            $lenghtLead = iconv_strlen(strip_tags($item->lead));
            $lenghtBody = iconv_strlen(strip_tags($item->body));
            $totalLenght = intval($lenghtBody + $lenghtLead);
            $totalReadTime = round(($totalLenght / 1000) / 2);

            if ($totalReadTime == 0){
                $totalReadTime = 1; // #PLUS1-257 Выводить 1 минуту там где получается 0 минут на чтение
            }
        }

        $totalReadTimeCaption = $this->getHeaderMenuSigns('минут', 'минута', 'минуты', $totalReadTime);

        $item->dateStr = $item->post_created;

        $item->fullUrl = $rubrika_url . "/" . $item->url;

        $item->specProject = false;
        if ($item->spec_project !== 0 || $item->spec_project !== "0"){
            $item->specProject = true;
        }

        if ($ajax){
            $result = [
                'blockAnnouncements' => Helper::getBlockAnnouncements(),
                'item' => $item,
                'meta' => [
                    'title' => $item->meta_title . " / +1",
                    'description' => $item->meta_description,
                    'keywords' => $item->meta_keywords,
                    'ogTitle' => $item->header_rss . " / +1",
                    'ogType' => 'article',
                    'ogDescription' => $item->meta_description,
                    'ogImage' => $this->ogImage,
                    'ogSiteName' => $this->ogSiteName,
                    'ogUrl' => $this->ogUrl,
                    'twitterCard' => 'summary_large_image',
                    'twitterTitle' => $item->header_rss . " / +1",
                    'twitterDescription' => $item->meta_description,
                    'twitterUrl' => $this->ogUrl,
                    'twitterImage' => $this->ogImage,
                    'twitterImageAlt' => $item->meta_title . " / +1",
                    'twitterSite' => $this->ogSiteName,
                    'vkImage' => $this->ogImage_1_2
                ],
                'urlNextPost' => $nextPostUrlList
            ];
            foreach ($result['meta'] as $field => $value) {
                $result['meta'][$field] = str_replace("&shy;", "", $value);
            }
            return $result;
        }
        else{
            // Передаем в шаблон
            $this->smarty->assign('blog', $item);

            $this->smarty->assign('titleToFavorite', $this->title);
            $this->smarty->assign('urlToFavorite', $this->root_url . "/" . $this->ogUrl);
            $this->smarty->assign('totalReadTime', $totalReadTime);
            $this->smarty->assign('totalReadTimeCaption', $totalReadTimeCaption);


            $this->smarty->assign('issubscribe', $this->isSubscribe);

            $this->smarty->assign('bodyColor', $this->bodyColor);
            $this->smarty->assign('customColors', $this->customColors);

            $this->smarty->assign('hasCookieSbscr', @$_COOKIE['sbscr']);


            $this->body = $this->smarty->fetch('blog.tpl');
            return $this->body;
        }

        $headerMenu = $this->getHeaderMenu();

        $this->smarty->assign('headerMenu', $headerMenu['menu']);
        $this->smarty->assign('rubrikaUrl', $headerMenu['currentRubrika']);
        $this->smarty->assign('typeUrl', $headerMenu['currentType']);
        $settingWidgets = $this->getSettingWidgets();
        $this->smarty->assign('settingWidgets', json_encode($this->getSettingWidgets()));
        $this->smarty->assign('settingWidget', @$settingWidgets[0]);

    }

    function getNewsListToInfinityScroll($rubrikaUrl)
    {

        if ($_GET['mode'] == 'preview') {
            $andEnabled = "";
        } else {
            $andEnabled = " AND b.enabled = 1 ";
        }

        if ($_GET['mode'] == 'preview'){
            $andEnabled = "";
        }
        else{
            $andEnabled = " AND n.enabled = 1 ";
        }

        $nowDate = new DateTime();

        // Выбираем новость из базы

        $query = sql_placeholder("SELECT n.id, n.article_text as body, n.tags, n.header as name, n.text_content as header, n.url, n.is_partner_material,
                    n.image_1_1, n.image_1_2, n.image_1_3, n.image_rss,
                    n.lead, bw.name AS writer, bw.id AS writer_id, bw.private, bw.bloger, bw.name_genitive,
                    bw.image AS writerImage, bw.stub_link,
                    DATE_FORMAT(n.created, '%d.%m.%Y %H:%i') AS post_created,
                    n.meta_title, n.meta_description, n.meta_keywords,
                    n.author_signature, n.author_name, n.image_rss_description, n.image_rss_source,
                    bw.bloger, bw.name as writerName, bw.announcement_img as blogImg, bw.id as writerId, bw.tag_url as writerTag
                   FROM news AS n
                    LEFT JOIN blogwriters AS bw ON bw.id=n.writers
                    WHERE n.created <= ?
                    AND n.id = ?
                    {$andEnabled}
                   ORDER BY n.created DESC
                  LIMIT 1", $nowDate->format('Y-m-d H:i:s'), $_GET['post_id']);
        $this->db->query($query);
        $item = $this->db->result();

        // замена подписи автора в старых постах
        $pattern = '#<span class="author">(.+?)</span>#is';
        preg_match($pattern, $item->body, $arr, PREG_OFFSET_CAPTURE);
        $newAuthorNameStr = str_ireplace("Автор:", "", $arr[0][0]);
        $newAuthorNameStr = strip_tags($newAuthorNameStr);
        $newHtmlAuthorSign = '<div class="articleAuthor"><p class="articleAuthor_h">Автор</p><p class="articleAuthor_name">' . $newAuthorNameStr . '</p></div>';
        $item->body =  preg_replace($pattern, $newHtmlAuthorSign, $item->body);

        $this->bodyColor = $item->body_color;

        $this->customColors = false;

        if ($item->font_color != "" && $item->border_color != ""){
            $this->customColors = true;
        }

        $item->body = str_replace("<p>&nbsp;</p>", "", $item->body);

        $query = "SELECT name, url, color FROM blogtags WHERE id=" . $item->tags;
        $this->db->query($query);
        $item->tags = $this->db->result();

        $query = "SELECT t.name, t.url FROM news_to_tags_relations AS rel
                              INNER JOIN post_tags AS t ON t.id=rel.post_tag_id
                              WHERE rel.news_id=" . $item->id . " AND t.enabled=1 ORDER BY t.name";
        $this->db->query($query);
        $item->postTags = $this->db->results();

        foreach ($item->postTags AS $k=>$v){
            $item->postTags[$k]->url = 'tag/' . $item->tags->url . "/" . $v->url;
        }

        $postTagMainPage = $this->getPostTagMainPage($item->post_tag);
        $item->postTagMainPage = $postTagMainPage;

        $item->postDateStr = $item->post_created; //$this->dateTimeWord($item->post_created);

        $name = $item->name;
        $name = str_replace("<p>", "", $name);
        $name = str_replace("</p>", "", $name);
        $item->name = $name;

        $citateName = str_replace("<p>", "", $item->name);
        $citateName = str_replace("</p>", "", $citateName);

        $citateHeader = str_replace("<p>", "", $item->header);
        $citateHeader = str_replace("</p>", "", $citateHeader);

        $item->citateText = $citateName . " <span>" . $citateHeader . "</span>";

        // Устанавливаем метатеги для страницы с этой записью
        $this->title = htmlspecialchars(strip_tags(trim($item->meta_title))) . " / +1";

        $this->description = "";
        if (!empty($item->lead)){
            $this->description = strip_tags($item->lead);
        }
        elseif(!empty($item->header)){
            $this->description = strip_tags($item->header);
        }

        $item->meta->title = htmlspecialchars(strip_tags(trim($item->meta_title))) . " / +1";
        $item->meta->description = htmlspecialchars(strip_tags(trim($item->meta_description)));
        $item->meta->keywords = htmlspecialchars(strip_tags(trim($item->meta_keywords)));
        $item->meta->headerMenu = $this->getHeaderMenu();

        $image_rss = $item->image_rss ? "files/news/" . $item->image_rss : '';

        $this->ogImage = $this->config->fileStorage['domainFilePath'] . "/" . $image_rss;

        $this->ogImage_1_1 = "files/news/" . $item->image_1_1 . "?" . time();
        $this->ogImage_1_1_c = "files/news/" . $item->image_1_1_c . "?" . time();
        $this->ogImage_1_2 = "files/news/" . $item->image_1_2 . "?" . time();
        $this->ogImage_1_3 = "files/news/" . $item->image_1_3 . "?" . time();
        $this->ogImage_galeries = "";

        $this->ogUrl = $this->config->fileStorage['domainFilePath'] . "/" . $rubrikaUrl . "/" . $item->url;
        $this->ogSiteName = $this->settings->site_name;

        // вычисляем примерное время на прочтение поста
        $lenghtLead = iconv_strlen(strip_tags($item->lead));
        $lenghtBody = iconv_strlen(strip_tags($item->body));
        $totalLenght = intval($lenghtBody + $lenghtLead);
        $totalReadTime = round(($totalLenght / 1000) / 2);
        if ($totalReadTime == 0){
            $totalReadTime = 1; // #PLUS1-257 Выводить 1 минуту там где получается 0 минут на чтение
        }

        $totalReadTimeCaption = $this->getHeaderMenuSigns('минут', 'минута', 'минуты', $totalReadTime);

        $item->dateStr = $item->post_created;

        $item->fullUrl = $rubrikaUrl . "/" . date('Y/m/d', strtotime($item->post_created)) . "/" . $item->url;


        // Передаем в шаблон
        $item->mainImage =  "[picture url=\"/{$image_rss}\" title=\"{$item->image_rss_description}\" source=\"{$item->image_rss_source}\"]";
        $item->authorSign = "[sign title=\"{$item->author_signature}\" name=\"{$item->author_name}\"]";


        // Передаем в шаблон
//        $item->mainImage = $image_rss;
//        $this->smarty->assign('blog', $item);
//
//        $this->smarty->assign('titleToFavorite', $this->title);
//        $this->smarty->assign('urlToFavorite', $this->root_url . "/" . $this->ogUrl);
//        $this->smarty->assign('totalReadTime', $totalReadTime);
//        $this->smarty->assign('totalReadTimeCaption', $totalReadTimeCaption);
//
//
//        $this->smarty->assign('issubscribe', $this->isSubscribe);
//
//        $this->smarty->assign('bodyColor', $this->bodyColor);
//        $this->smarty->assign('customColors', $this->customColors);
//
//        $this->smarty->assign('hasCookieSbscr', @$_COOKIE['sbscr']);
//
//
//        $this->body = $this->smarty->fetch('blog.tpl');

        // Устанавливаем метатеги для страницы с этой записью
        $this->title = "";

        if ($item->typePost == 'factday'){
            $this->title = htmlspecialchars(strip_tags(trim($item->name))) . " / +1";
        }
        else{
            if ($item->seo_title !=""){
                $this->title = htmlspecialchars(strip_tags(trim($item->seo_title))) . " / +1";
            }
            else{
                $this->title = htmlspecialchars(strip_tags(trim($item->meta_title))) . " / +1";
            }
        }

        $this->description = "";
        if ($item->seo_description != ""){
            $this->description = strip_tags($item->seo_description);
        }
        else{
            if (!empty($item->lead)){
                $this->description = strip_tags($item->lead);
            }
            elseif(!empty($item->header)){
                $this->description = strip_tags($item->header);
            }
        }

        $this->keywords = $item->seo_keywords;

        $item->meta->title = htmlspecialchars(strip_tags(trim($item->meta_title)));
        $item->meta->description = htmlspecialchars(strip_tags(trim($item->meta_description)));
        $item->meta->keywords = htmlspecialchars(strip_tags(trim($item->meta_keywords)));
        $item->meta->headerMenu = $this->getHeaderMenu();

        $this->ogImage = $this->config->fileStorage['domainFilePath'] . "/files/news/" . $item->image_rss;
        $this->ogUrl = "" . $rubrikaUrl . "/" . $item->url;
        $this->ogSiteName = $this->settings->site_name;

        $relatedPosts = Helper::getRelatedPostsByParent($item->id, 'news', $nowDate);
        $item->relatedPosts = Helper::formatPostList($relatedPosts, '1_3');

        $result = array(
            'item' => $item,
            'meta' => [
                'title' => $item->meta_title . " / +1",
                'description' => $item->meta_description,
                'keywords' => $item->meta_keywords,
                'ogTitle' => $item->meta_title . " / +1",
                'ogType' => 'article',
                'ogDescription' => $item->meta_description,
                'ogImage' => $this->ogImage,
                'ogSiteName' => $this->ogSiteName,
                'ogUrl' => $this->ogUrl,
                'twitterCard' => 'summary_large_image',
                'twitterTitle' => $item->meta_title . " / +1",
                'twitterDescription' => $item->meta_description,
                'twitterUrl' => $this->ogUrl,
                'twitterImage' => $this->ogImage,
                'twitterImageAlt' => $item->meta_title . " / +1",
                'twitterSite' => $this->ogSiteName
            ],
        );

        header("Content-type: application/json; charset=UTF-8");
        header("Cache-Control: must-revalidate");
        header("Pragma: no-cache");
        header("Expires: -1");

        print json_encode($result);
        exit();
    }

    /**
     * Отображение отдельной новости
     *
     */
    function fetchNewsItem($url, $rubrika_url, $ajax = false)
    {
        $nowDate = new DateTime();
        if ($_GET['mode'] == 'preview'){
            $andEnabled = "";
        }
        else{
            $nowSTR = $nowDate->format('Y-m-d H:i:s');
            $andEnabled = " AND n.enabled = 1 AND n.created <= '{$nowSTR}' ";
        }

        // Выбираем новость из базы
        $query = sql_placeholder("SELECT n.id, n.article_text as body, n.tags, n.header as name, n.text_content as header,
                    n.header_social as header_rss, n.url, n.is_partner_material,
                    n.image_1_1, n.image_1_2, n.image_1_3, n.image_rss,
                    n.lead, bw.name AS writer, bw.id AS writer_id, bw.private, bw.bloger, bw.name_genitive,
                    bw.image AS writerImage, bw.stub_link,
                    DATE_FORMAT(n.created, '%d.%m.%Y %H:%i') AS post_created,
                    CONCAT ('news/', DATE_FORMAT(n.created, '%Y/%m/%d')) AS postUrl,
                    n.meta_title, n.meta_description, n.meta_keywords,
                    n.author_signature, n.author_name, n.image_rss_description, n.image_rss_source, n.post_tag,
                    bw.bloger, bw.name as writerName, bw.announcement_img as blogImg, bw.id as writerId, bw.tag_url as writerTag
                   FROM news AS n
              LEFT JOIN blogwriters AS bw ON bw.id=n.writers
              WHERE n.url = ? {$andEnabled}
                  ORDER BY n.created DESC
                  LIMIT 1", $url);
        $this->db->query($query);
        // Если не существует такая новость - ошибка 404
        if ($this->db->num_rows() == 0) {
            $this->findByNewsHistoryUrl($url, $rubrika_url);
            return false;
        }
        $item = $this->db->result();


        /* next post ids */
        $query = sql_placeholder("SELECT n.id
                   FROM news AS n
                    LEFT JOIN blogwriters AS bw ON bw.id=n.writers
                    WHERE n.created <= ?
                    AND n.id != ?
                    AND (bw.id IS NULL OR bw.useplatforma <> 1)
                    {$andEnabled}
                   ORDER BY n.created DESC
                  LIMIT 60", $nowDate->format('Y-m-d H:i:s'), $item->id);
        $this->db->query($query);
        $nextNews = $this->db->results();

        foreach ($nextNews AS $news){
            $nextNewsUrlList[] = 'getPost/news/' . $news->id;
        }


        // замена подписи автора в старых постах
        $pattern = '#<span class="author">(.+?)</span>#is';
        preg_match($pattern, $item->body, $arr, PREG_OFFSET_CAPTURE);
        if (!empty($arr)) {
            $newAuthorNameStr = str_ireplace("Автор:", "", $arr[0][0]);
            $newAuthorNameStr = strip_tags($newAuthorNameStr);
            $newHtmlAuthorSign = '<div class="articleAuthor"><p class="articleAuthor_h">Автор</p><p class="articleAuthor_name">' . $newAuthorNameStr . '</p></div>';
            $item->body =  preg_replace($pattern, $newHtmlAuthorSign, $item->body);
        }

        $this->bodyColor = null;
        $this->customColors = false;

        $item->body = str_replace("<p>&nbsp;</p>", "", $item->body);

        $query = "SELECT name, url, color FROM blogtags WHERE id=" . $item->tags;
        $this->db->query($query);
        $item->tags = $this->db->result();

        $query = "SELECT t.name, t.url FROM news_to_tags_relations AS rel
                              INNER JOIN post_tags AS t ON t.id=rel.post_tag_id
                              WHERE rel.news_id=" . $item->id . " AND t.enabled=1 ORDER BY t.name";
        $this->db->query($query);
        $item->postTags = $this->db->results();

        foreach ($item->postTags AS $k=>$v){
            $item->postTags[$k]->url = 'tag/' . $item->tags->url . "/" . $v->url;
        }

        $postTagMainPage = $this->getPostTagMainPage($item->post_tag);
        $item->postTagMainPage = $postTagMainPage;

        $item->postDateStr = $item->post_created; //$this->dateTimeWord($item->post_created);

        $name = $item->name;
        $name = str_replace("<p>", "", $name);
        $name = str_replace("</p>", "", $name);
        $name = str_replace("&shy;", "", $name);
        $item->name = $name;

        $citateName = str_replace("<p>", "", $item->name);
        $citateName = str_replace("</p>", "", $citateName);

        $citateHeader = str_replace("<p>", "", $item->header);
        $citateHeader = str_replace("</p>", "", $citateHeader);

        $item->citateText = $citateName . " <span>" . $citateHeader . "</span>";

        // Устанавливаем метатеги для страницы с этой записью
        $this->title = htmlspecialchars(strip_tags(trim($item->meta_title))) . " / +1";

        $this->description = "";
        if (!empty($item->lead)){
            $this->description = strip_tags($item->lead);
        }
        elseif(!empty($item->header)){
            $this->description = strip_tags($item->header);
        }

        $item->meta = new stdClass();
        $item->meta->title = htmlspecialchars(strip_tags(trim($item->meta_title)));
        $item->meta->description = htmlspecialchars(strip_tags(trim($item->meta_description)));
        $item->meta->keywords = htmlspecialchars(strip_tags(trim($item->meta_keywords)));
//        $item->meta->headerMenu = $this->getHeaderMenu();

        $image_rss = $item->image_rss ? "files/news/" . $item->image_rss : '';

        $this->ogImage = $this->config->fileStorage['domainFilePath'] . "/" . $image_rss;

        $this->ogImage_1_1 = "files/news/" . $item->image_1_1 . "?" . time();
        $this->ogImage_1_1_c = null;
        $this->ogImage_1_2 = "files/news/" . $item->image_1_2 . "?" . time();
        $this->ogImage_1_3 = "files/news/" . $item->image_1_3 . "?" . time();
        $this->ogImage_galeries = "";

        $this->ogUrl = $this->config->fileStorage['domainFilePath'] . "/{$item->postUrl}/{$url}";
        $this->ogSiteName = $this->settings->site_name;

        // вычисляем примерное время на прочтение поста
        $lenghtLead = iconv_strlen(strip_tags($item->lead));
        $lenghtBody = iconv_strlen(strip_tags($item->body));
        $totalLenght = intval($lenghtBody + $lenghtLead);
        $totalReadTime = round(($totalLenght / 1000) / 2);
        if ($totalReadTime == 0){
            $totalReadTime = 1; // #PLUS1-257 Выводить 1 минуту там где получается 0 минут на чтение
        }

        $totalReadTimeCaption = $this->getHeaderMenuSigns('минут', 'минута', 'минуты', $totalReadTime);

        $item->dateStr = $item->post_created;

        $item->fullUrl = 'news/' . date('Y/m/d', strtotime($item->post_created)) . "/" . $item->url;

        $relatedPosts = Helper::getRelatedPostsByParent($item->id, 'news', $nowDate);
        $item->relatedPosts = Helper::formatPostList($relatedPosts, '1_3');

        // Передаем в шаблон
        $item->mainImage =  "[picture url=\"/{$image_rss}\" title=\"{$item->image_rss_description}\" source=\"{$item->image_rss_source}\"]";
        $item->authorSign = "[sign title=\"{$item->author_signature}\" name=\"{$item->author_name}\"]";

        if ($ajax){
            $result = [
                'blockAnnouncements' => [],
                'item' => $item,
                'meta' => [
                    'title' => $item->meta_title . " / +1",
                    'description' => $item->meta_description,
                    'keywords' => $item->meta_keywords,
                    'ogTitle' => $item->header_rss . " / +1",
                    'ogType' => 'article',
                    'ogDescription' => $item->meta_description,
                    'ogImage' => $this->ogImage,
                    'ogSiteName' => $item->header_rss . " / +1",
                    'ogUrl' => $this->ogUrl,
                    'twitterCard' => 'summary_large_image',
                    'twitterTitle' => $item->header_rss . " / +1",
                    'twitterDescription' => $item->meta_description,
                    'twitterUrl' => $this->ogUrl,
                    'twitterImage' => $this->ogImage,
                    'twitterImageAlt' => $item->header_rss . " / +1",
                    'twitterSite' => $item->header_rss . " / +1"
                ],
                'urlNextPost' => $nextNewsUrlList
            ];

            return $result;
        }
        else {
            $this->smarty->assign('blog', $item);

            $this->smarty->assign('titleToFavorite', $this->title);
            $this->smarty->assign('urlToFavorite', $this->root_url . "/" . $this->ogUrl);
            $this->smarty->assign('totalReadTime', $totalReadTime);
            $this->smarty->assign('totalReadTimeCaption', $totalReadTimeCaption);


            $this->smarty->assign('issubscribe', $this->isSubscribe);

            $this->smarty->assign('bodyColor', $this->bodyColor);
            $this->smarty->assign('customColors', $this->customColors);

            $this->smarty->assign('hasCookieSbscr', @$_COOKIE['sbscr']);


            $this->body = $this->smarty->fetch('blog.tpl');
            return $this->body;
        }
    }

    /**
     * Метод возвращает все записи по выбранному тегу первого уровня
     *
     * @param string $rubrika_url - Урл тега первого уровня
     * @param string $tagUrl - Урл тега второго уровня
     * @param int $page - страница с которой начать отображение блоков (по умолчанию 0 - с самого начала)
     * @param array $usedIdsPosts - Id постов уже выведенных на странице (нужно чтобы не выводить несколько раз одну и ту же статью)
     * @param int $useElasticBlock - какие блоки использованы в 1/4 (блоки 1/4 могут менять свой вид в зависимости от их кол-ва в строке)
     * @param bool $ajax
     * @return array|bool - массив с постами, или false если ничего не найдено
     * @throws Exception
     */
    function fetchListByTag($rubrika_url, $tagUrl = '', $page = 1, $usedIdsPosts = array(), $useElasticBlock = 0, $ajax = true)
    {

        $query = sql_placeholder("SELECT pt.id as pt_id, bt.id as bt_id FROM post_tags AS pt
                                    INNER JOIN blogtags AS bt ON bt.id = pt.parent
                                  WHERE bt.enabled = 1 AND pt.enabled = 1 AND pt.url = ? ", $tagUrl);
        $this->db->query($query);
        $tagId = $this->db->result();

        $nowDate = new DateTime();
        $query = sql_placeholder("SELECT COUNT(DISTINCT bp.id) AS cnt FROM blogposts AS bp
                              INNER JOIN blogwriters AS bw ON bp.writers = bw.id
                              INNER JOIN relations_postitem_tags AS rpt ON rpt.post_id = bp.id
                              INNER JOIN blogtags AS bt ON bp.tags = bt.id
                              INNER JOIN post_tags AS pt ON bt.id = pt.parent
                                   WHERE bw.enabled = 1 AND bp.enabled = 1 AND rpt.posttag_id = ?  AND bw.useplatforma <> 1
                                     AND bp.created <= ?", $tagId->pt_id, $nowDate->format('Y-m-d H:i:s'));
        $this->db->query($query);
        $resultCount = $this->db->result();

        $totalPages = ceil($resultCount->cnt / $this->numItemsOnPage);

        if ($page > $totalPages) {
            return false;
        }

        if ($tagUrl != "") {

            $query = sql_placeholder("SELECT id, name, seo_title, seo_description, seo_keywords FROM post_tags WHERE enabled=1 AND url=?", $tagUrl);
            $this->db->query($query);
            $selectedTag = $this->db->result();

            $start = ($page - 1) * $this->numItemsOnPage;

            $query = sql_placeholder("SELECT b.id, b.blocks_blog AS blocks, b.tags, b.name, b.header, b.url, b.type_post, b.is_partner_material,
                            b.type_post, b.partner_url, b.post_tag, b.signature_to_sum, b.text_body, b.video_on_main, b.state, b.type_video,
                            b.amount_to_displaying, bw.name AS writer, bw.id AS writer_id, bw.private, bw.bloger, bw.name_genitive,
                            bw.image AS writerImage, bw.stub_image, bw.stub_link, p.name AS partner,
                            p.color AS partner_color, b.anounce_new_author, b.spec_project,  b.conference,
                            sp.name AS specProjectName, sp.external_url AS specProjectUrl, sp.color AS marker_color,
                            sp.font_color AS font_color,  b.image_1_1, b.image_1_2, b.image_1_3, b.image_rss,
                            sp.image_1_1 AS specProjectImage, sp.header AS specProjectHeader,
                            conf.name AS confName, conf.external_url AS confUrl, conf.color AS confColor,
                            conf.font_color AS confFontColor, b.citate_author_name,
                            DATE_FORMAT(b.created, '%d.%m.%Y') AS date_created,
                            bt.name AS tagName, bt.url AS tagUrl, b.format, b.font, p.name AS partnerName
                            ,b.type_announce_1_1, b.type_announce_1_2, b.type_announce_1_3,
                            bw.bloger, bw.name as writerName, bw.announcement_img as blogImg, bw.id as writerId, bw.tag_url as writerTag
                            FROM blogposts AS b
                                    INNER JOIN relations_postitem_tags AS rel_pt ON rel_pt.post_id=b.id
                                    INNER JOIN post_tags AS pt ON pt.id=rel_pt.posttag_id
                                    INNER JOIN blogwriters AS bw ON bw.id=b.writers
                                    LEFT JOIN partners AS p ON p.id=b.partner
                                    LEFT JOIN blogtags AS bt ON bt.id = b.tags
                                    LEFT JOIN spec_projects AS sp ON sp.id=b.spec_project
                                    LEFT JOIN conferences AS conf ON conf.id=b.conference
                                  WHERE
                                  b.enabled=1 AND bw.enabled = 1 AND bw.useplatforma <> 1
                                  AND pt.url=?
                                  AND b.blocks != 'ticker'
                                  AND b.created <= ?
                                  ORDER BY b.created DESC
                                  LIMIT ?, ?",
                $tagUrl, $nowDate->format('Y-m-d H:i:s'), $start, $this->numItemsOnPage);

            $this->db->query($query);
            $result = $this->db->results();

            $result = Helper::formatPostList($result, '1_3');

            $page = intval($page + 1);

            $title = $selectedTag->seo_title ?  "+1 — "  . $selectedTag->seo_title : "+1 — " . $this->tags[$rubrika_url] . ", " . $selectedTag->name;
            // метатеги
            $this->title = $title;
            $this->description = $selectedTag->seo_description;
            $this->keywords = $selectedTag->seo_keywords;

            if ($ajax){
                $return = array(
                    'posts' => $result,
                    'page' => $page,
                    'meta' => [
                        'header' => "Материалы по тегу #" . $selectedTag->name,
                        'title' => $this->title,
                        'description' => $this->description,
                        'keywords' => $this->keywords,
                        'ogTitle' => $this->title,
                        'ogType' => 'article',
                        'ogDescription' => $this->description,
                        'ogImage' => '/og-plus-one.ru.png',
                        'ogSiteName' => '',
                        'ogUrl' => '',
                        'twitterCard' => 'summary_large_image',
                        'twitterTitle' => $this->title,
                        'twitterDescription' =>  $this->description,
                        'twitterUrl' => '',
                        'twitterImage' => '/og-plus-one.ru.png',
                        'twitterImageAlt' => '+1 — Проект об устойчивом развитии',
                        'twitterSite' => ''
                    ]
                );
                return $return;
            }
            else{


                $this->smarty->assign('items', $result);
                $this->smarty->assign('rubrika', $rubrika_url);
                $this->smarty->assign('tagUrl', $tagUrl);
                $this->smarty->assign('page', $page);
                $headerMenu = $this->getHeaderMenu();

                $this->smarty->assign('headerMenu', $headerMenu['menu']);
                $this->smarty->assign('rubrikaUrl', $headerMenu['currentRubrika']);
                $this->smarty->assign('typeUrl', $headerMenu['currentType']);
                $this->body = $this->smarty->fetch('ajaxBlogs.tpl');
                return $this->body;
            }
        }
        return false;
    }

    function getPostsByType($writerId, $typePost){

        $query = sql_placeholder("SELECT  b.id, b.blocks_author AS blocks, b.tags, b.is_partner_material, b.name, b.header, b.url, b.block_color, b.type_post, b.post_tag, b.signature_to_sum, b.text_body, b.amount_to_displaying, bw.name AS writer, bw.id AS writer_id, DATE_FORMAT(b.created, '%d.%m.%Y %H:%i:%s') AS post_created,
                            bw.bloger, bw.name as writerName, bw.announcement_img as blogImg, bw.id as writerId, bw.tag_url as writerTag
                          FROM blogposts AS b
                          INNER JOIN blogwriters AS bw ON bw.id=b.writers
                          WHERE
                          b.enabled=1 AND bw.enabled = 1
                          AND b.type_post = ?
                          AND b.writers=?
                          ORDER BY b.created DESC",
            $typePost,
            $writerId
        );
        $this->db->query($query);
        $result = $this->db->results();

        foreach ($result AS $k => $v) {

            $name = $v->name;
            $name = str_replace("<p>", "", $name);
            $name = str_replace("</p>", "", $name);
            $v->name = $name;

            if ($v->type_post == 2) {

                $v->name = $name . ":";

                $body = $v->body;
                $body = str_replace("<p>", "", $body);
                $body = str_replace("</p>", "", $body);
                $v->body = $body;

                $header = $v->header;
                $header = str_replace("<p>", "", $header);
                $header = str_replace("</p>", "", $header);
                $v->header = $header;
            }

            $citateName = str_replace("<p>", "", $v->name);
            $citateName = str_replace("</p>", "", $citateName);

            $citateHeader = str_replace("<p>", "", $v->header);
            $citateHeader = str_replace("</p>", "", $citateHeader);


            $result[$k]->citateText = $citateName . " <span>" . $citateHeader . "</span>";

            if ($v->type_post == 5) {
                $analitycData = $this->getAnalitycData($v->id, $writerId);
                $result[$k]->analitycData = $analitycData;
            }


            $query = "SELECT name, url FROM blogtags WHERE id=" . $v->tags;
            $this->db->query($query);
            $result[$k]->tags = $this->db->result();

            $result[$k]->postTags = $this->getPostTagTwoLevel($v->id);

            $result[$k]->postDateStr = Helper::dateTimeWord($v->post_created);
        }

        $block = '1/3';
        $key = 0;
        $this->postsMatrix = $result;

        $return = array(
            'posts' => $result,
        );
        return $result;


    }

    function countMaxRecords($block, $whereBy, $where)
    {

        if ($whereBy == "rubrika") {
            $queryCnt = sql_placeholder("SELECT count(b.id) as cnt FROM blogposts AS b
                          INNER JOIN blogwriters AS bw ON bw.id=b.writers
                          INNER JOIN relations_post_tags AS rel ON rel.post_id=b.id
                          WHERE
                          b.enabled=1
                          AND b.blocks = ?
                          AND rel.tag_id = ?
                          ORDER BY b.created DESC",
                $block, $where);
            $this->db->query($queryCnt);
            $resultCnt = $this->db->result();
        }

        if ($whereBy == "writer") {
            $queryCnt = sql_placeholder("SELECT count(b.id) as cnt FROM blogposts AS b
                          INNER JOIN blogwriters AS bw ON bw.id=b.writers
                          WHERE
                          b.enabled=1
                          AND b.blocks = ?
                          AND b.writers=?
                          ORDER BY b.created DESC",
                $block, $where);
            $this->db->query($queryCnt);
            $resultCnt = $this->db->result();
        }


        return $resultCnt->cnt;
    }

    /**
     *
     * Отображение списка статей
     *
     */
    function fetch_list($rubrika_url = 'main', $page = 0, $usedBlocks = array())
    {
        $query = sql_placeholder("SELECT COUNT(b.id) AS count FROM blogposts AS b WHERE b.enabled=1");
        $this->db->query($query);
        $resultCount = $this->db->result();

        $totalPages = ceil($resultCount->count / $this->numItemsOnPage);

        if ($page > $totalPages) {
            return false;
        }

        if ($rubrika_url != "") {
            $query = sql_placeholder("SELECT * FROM blogtags WHERE enabled=1 AND url=?", $rubrika_url);
            $this->db->query($query);

            if ($this->db->num_rows() == 0) {
                return false;
            }
            $rubrika = $this->db->result();

            $usedBlocks['1/1']['maxItems'] = $this->countMaxRecords('1/1', 'rubrika', $rubrika->id);
            $usedBlocks['1/2']['maxItems'] = $this->countMaxRecords('1/2', 'rubrika', $rubrika->id);
            $usedBlocks['1/3']['maxItems'] = $this->countMaxRecords('1/3', 'rubrika', $rubrika->id);
            $usedBlocks['1/4']['maxItems'] = $this->countMaxRecords('1/4', 'rubrika', $rubrika->id);

            foreach ($this->postMatrix AS $key => $matrix) {
                foreach ($matrix AS $block => $configBlock) {

                    if ($usedBlocks[$block]['limit']) {
                        $start = $usedBlocks[$block]['limit'];
                    } else {
                        $start = 0;
                    }

                    if ($start <= $usedBlocks[$block]['maxItems']) {
                        if ($rubrika_url == 'main') {
                            $query = sql_placeholder("SELECT b.id, b.name, b.header, b.url, b.block_color, b.type_post, b.is_partner_material, bw.name AS writer, bw.id AS writer_id, DATE_FORMAT(b.created, '%d.%m.%Y %H:%i:%s') AS post_created,
                            bw.bloger, bw.name as writerName, bw.announcement_img as blogImg, bw.id as writerId, bw.tag_url as writerTag
                          FROM blogposts AS b
                          INNER JOIN blogwriters AS bw ON bw.id=b.writers
                          INNER JOIN relations_post_tags AS rel ON rel.post_id=b.id
                          WHERE
                          b.enabled=1
                          AND b.blocks = ?
                          AND rel.tag_id=?
                          ORDER BY b.created DESC
                          LIMIT ?, ?",
                                $block,
                                $rubrika->id,
                                $start,
                                $configBlock['settings']['limit']);
                        } else {
                            $query = sql_placeholder("SELECT b.id, b.name, b.blocks_blog AS blocks, b.header, b.url, b.block_color, b.type_post, b.is_partner_material, bw.name AS writer, bw.id AS writer_id, DATE_FORMAT(b.created, '%d.%m.%Y %H:%i:%s') AS post_created,
                            bw.bloger, bw.name as writerName, bw.announcement_img as blogImg, bw.id as writerId, bw.tag_url as writerTag
                          FROM blogposts AS b
                          INNER JOIN blogwriters AS bw ON bw.id=b.writers
                          INNER JOIN relations_post_tags AS rel ON rel.post_id=b.id
                          WHERE
                          b.enabled=1
                          AND b.blocks_blog = ?
                          AND rel.tag_id=?
                          ORDER BY b.created DESC
                          LIMIT ?, ?",
                                $block,
                                $rubrika->id,
                                $start,
                                $configBlock['settings']['limit']);
                        }
                        $this->db->query($query);
                        $result = $this->db->results();

                        foreach ($result AS $k => $v) {

                            if ($v->type_post == 2) {
                                $body = $v->body;
                                $body = str_replace("<p>", "", $body);
                                $body = str_replace("</p>", "", $body);
                                $v->body = $body;
                            }


                            $query = "SELECT t.name, t.url FROM relations_post_tags AS rel
                              INNER JOIN blogtags AS t ON t.id=rel.tag_id
                              WHERE rel.post_id=" . $v->id . " AND t.url <> 'main' ORDER BY t.name";
                            $this->db->query($query);
                            $result[$k]->tags = $this->db->results();

                            $result[$k]->postTags = $this->getPostTagTwoLevel($v->id);

                            $result[$k]->postDateStr = Helper::dateTimeWord($v->post_created);
                        }

                        $this->postsMatrix->$key->$block = $result;
                        // наращиваем стартовую позицию в следующем запросе постов в такой же блок
                        $usedBlocks[$block]['limit'] = $usedBlocks[$block]['limit'] + $configBlock['settings']['limit'];
                    }
                }
            }
            $this->smarty->assign('postsMatrix', $this->postsMatrix);
            $this->smarty->assign('totalPages', $totalPages);
            $this->smarty->assign('currentPage', $page + 1);

            $this->smarty->assign('usedBlocks', serialize($usedBlocks));
            $this->body = $this->smarty->fetch('blogs.tpl');

            return $this->body;
        }
        return false;
    }

    /**
     * @return int|object|stdClass
     */
    function getConferenceLink()
    {
        $query = "SELECT external_url FROM conferences WHERE enabled = 1 LIMIT 1";
        $this->db->query($query);
        return $this->db->result();
    }


    /**
     * метатеги и метатайтл для страницы
     *
     * Главная: +1 — Главная
     * Рубрики — по тому же шаблону. Вместо "Главная" название рубрики
     * Внутренние страницы: в соответствии с мета этих страниц (это кстати не работает сейчас)
     * Блоги авторов: Блог %автор блога% — +1
     * @param string $page
     * @param string $type
     * @return array
     */
    function getMetaData($page = 'main', $type = '')
    {
        if ($type == 'author'){
            $query = "SELECT w.*, t.name_lead, bt.url, w.image AS imageDesktop, w.stub_image AS imageMobile, w.stub_link, w.description FROM blogwriters AS w
                                  LEFT JOIN post_tags AS t ON t.id=w.leader
                                  LEFT JOIN blogtags AS bt ON bt.id=t.parent
                                  WHERE w.id=" . $page . " LIMIT 1";
            $this->db->query($query);
            $writer = $this->db->result();

            if ($writer->private == 1){
                $writerName = "Колонка " . $writer->name_genitive;
            }
            elseif ($writer->bloger == 1){
                $writerName = "Блог " . $writer->name;
            }
            else{
                $writerName = $writer->name;
            }

            $title = $writer->seo_title ? "+1 — " . $writer->seo_title : "+1 — " . $writerName;
            $description = $writer->seo_description ? $writer->seo_description : "";
            $keywords = $writer->seo_keywords ? $writer->seo_keywords : "";

            if ($writer->image){
                $writer->image = "/files/writers/" . $writer->image;
            }
            else {
                $writer->image = null;
            }

            if ($writer->imageDesktop){
                $writer->imageDesktop = "/files/writers/" . $writer->imageDesktop;
            }
            else {
                $writer->imageDesktop = null;
            }

            if ($writer->imageMobile){
                $writer->imageMobile = "/files/writers/" . $writer->imageMobile;
            }
            else {
                $writer->imageMobile = null;
            }


            $result = array(
                'metaTitle' => $title,
                'metaDescription' => $description,
                'metaKeyword' => $keywords,
                'writer' => $writer
            );
        }
        elseif($type == 'posttags'){
            $query = sql_placeholder("SELECT * FROM seo_content WHERE section = 'posttags'");
            $this->db->query($query);
            $seoContent = $this->db->result();

            $title = $seoContent->seo_title ? "+1 — " . $seoContent->seo_title : "+1 — Теги направления " . $page;
            $description = $seoContent->seo_description ? $seoContent->seo_description : "";
            $keywords = $seoContent->seo_keywords ? $seoContent->seo_keywords : "";

            $result = array(
                'metaTitle' => $title,
                'metaDescription' => $description,
                'metaKeyword' => $keywords,
                'writer' => ""
            );
        }
        elseif ($type == 'authors'){

            $query = sql_placeholder("SELECT * FROM seo_content WHERE section = 'writers'");
            $this->db->query($query);
            $seoContent = $this->db->result();

            $title = $seoContent->seo_title ? "+1 — " . $seoContent->seo_title : "+1 — Все участники блогов";
            $description = $seoContent->seo_description ? $seoContent->seo_description : "";
            $keywords = $seoContent->seo_keywords ? $seoContent->seo_keywords : "";


            $result = array(
                'metaTitle' => $title,
                'metaDescription' => $description,
                'metaKeyword' => $keywords,
                'writer' => ""
            );
        }
        else{
            switch ($page) {
                case Blog::TAG_MAIN:
                case Blog::TAG_ECONOMY:
                case Blog::TAG_ECOLOGY:
                case Blog::TAG_COMMUNITY:
                    $query = sql_placeholder("SELECT * FROM blogtags WHERE url=?", $page);
                    $this->db->query($query);
                    $rubrika = $this->db->result();

                    $title = $rubrika->seo_title ? "+1 — " . $rubrika->seo_title : "+1 — " . $rubrika->name;
                    $description = $rubrika->seo_description ? $rubrika->seo_description : "Все, что вы хотели знать об изменениях в экономике, обществе и экосистеме, но боялись спросить.";
                    $keywords = $rubrika->seo_keywords ? $rubrika->seo_keywords : "";

                    $result = array(
                        'metaTitle' => $title,
                        'metaDescription' => $description,
                        'metaKeyword' => $keywords,
                        'writer' => ""
                    );
                    break;
                case Blog::TAG_PLATFORMA:
                    $title = "+1Платформа";
                    $description = "Все, что вы хотели знать об изменениях в экономике, обществе и экосистеме, но боялись спросить.";
                    $keywords = "";

                    $result = array(
                        'metaTitle' => $title,
                        'metaDescription' => $description,
                        'metaKeyword' => $keywords,
                        'writer' => ""
                    );

                    break;
                case "about":
                    $result = array(
                        'metaTitle' => "+1 — О проекте",
                        'metaDescription' => "",
                        'metaKeyword' => "",
                        'writer' => ""
                    );
                    break;
                case "events":
                case "event":
                    $result = array(
                        'metaTitle' => "+1 — Мероприятия",
                        'metaDescription' => "",
                        'metaKeyword' => "",
                        'writer' => ""
                    );
                    break;
                case "news":
                    $result = array(
                        'metaTitle' => "+1 — Новости",
                        'metaDescription' => "Все, что вы хотели знать о коронавирусе, климатическом кризисе, эпидемиях, экологии и ответственном подходе к собственной жизни. Главные новости за день",
                        'metaKeyword' => "",
                        'writer' => ""
                    );
                    break;
                default:
                    $result = array(
                        'metaTitle' => "+1 — Главная",
                        'metaDescription' => "",
                        'metaKeyword' => "",
                        'writer' => ""
                    );
                    break;
            }
        }
        return $result;
    }

    function getHeaderMenu($currentRubrika = '', $currentType = ''){

        $query = sql_placeholder("SELECT bt.id, bt.url, bt.`name` FROM blogtags as bt WHERE bt.isshow = 1 AND bt.enabled = 1");
        $this->db->query($query);
        $tags = $this->db->results();

        $nowDate = new DateTime();

        $return = array();
        foreach ($tags AS $key => $tag){
            if (!isset($return[$tag->id])) {
                $return[$tag->id] = new stdClass();
            }

            $return[$tag->id]->url = $tag->url;

            $query = sql_placeholder("
        SELECT COUNT( bp.id) AS cnt
          FROM blogposts as bp
    INNER JOIN blogwriters as bw ON bw.id = bp.writers
         WHERE bw.enabled = 1 AND bp.enabled = 1 AND bp.tags = ? AND bw.useplatforma <> 1 AND bp.created <= ?
                ", $tag->id, $nowDate->format('Y-m-d H:i:s'));
            $this->db->query($query);
            $return[$tag->id]->posts = $this->db->result();
            $return[$tag->id]->posts->text = $this->getHeaderMenuSigns('материалов', 'материал', 'материала', $return[$tag->id]->posts->cnt);


            $query = sql_placeholder("
          SELECT COUNT(pt.id) AS cnt
            FROM post_tags as pt
           WHERE pt.enabled = 1 AND pt.parent=? AND EXISTS(
                    SELECT null
                      FROM blogposts as bp
                INNER JOIN relations_postitem_tags as rpt ON rpt.post_id = bp.id
                INNER JOIN blogwriters as bw ON bw.id = bp.writers
                     WHERE bw.enabled = 1 AND bp.enabled = 1 AND  bp.tags = pt.parent  AND rpt.posttag_id = pt.id AND bw.useplatforma <> 1
                       AND bp.created <= ?
                 )", $tag->id, $nowDate->format('Y-m-d H:i:s'));
            $this->db->query($query);
            $return[$tag->id]->postTags = $this->db->result();
            $return[$tag->id]->postTags->text = $this->getHeaderMenuSigns('тегов', 'тег', 'тега', $return[$tag->id]->postTags->cnt);
            $return[$tag->id]->postTags->url = 'tag/' . $tag->url;

//            лидеры
            $query = sql_placeholder("SELECT pt.name_lead, bw.name, bw.id FROM post_tags AS pt
                                    INNER JOIN blogwriters AS bw ON bw.leader = pt.id
                                  WHERE pt.enabled = 1 AND bw.enabled = 1 AND pt.enabled = 1 AND pt.parent = ? AND bw.useplatforma <> 1 AND EXISTS (
                                            SELECT null
                                              FROM blogposts as bp
                                             WHERE bp.enabled = 1 AND bw.id = bp.writers  AND bp.created <= ?
                                             )
                ", $tag->id, $nowDate->format('Y-m-d H:i:s'));
            $this->db->query($query);
            $authors = $this->db->results();

            $return[$tag->id]->leaders = new stdClass();
            $return[$tag->id]->leaders->cnt = count($authors);
            $return[$tag->id]->leaders->text = $this->getHeaderMenuSigns('лидеров', 'лидер', 'лидера', $return[$tag->id]->leaders->cnt);
            $return[$tag->id]->leaders->url = "leaders/" . $tag->url;




        }


        // пдатформа
        $return['platforma'] = new stdClass();
        $return['platforma']->url = Blog::TAG_PLATFORMA;

        $query = sql_placeholder("SELECT COUNT(bp.id) AS cnt FROM blogposts AS bp
            INNER JOIN blogwriters AS w ON w.id = bp.writers WHERE w.useplatforma = 1 AND w.enabled = 1 AND bp.enabled = 1 AND bp.created <= ?"
                , $nowDate->format('Y-m-d H:i:s'));
        $this->db->query($query);
        $postCount = $this->db->result();

        $return['platforma']->posts = new stdClass();
        $return['platforma']->posts->cnt = $postCount->cnt;
        $return['platforma']->posts->text = $this->getHeaderMenuSigns('материалов', 'материал', 'материала', $postCount->cnt);

        $query = sql_placeholder("SELECT COUNT( DISTINCT  w.id) AS cnt FROM blogwriters as w
                    INNER JOIN blogposts AS bp ON w.id=bp.writers WHERE w.useplatforma = 1  AND w.enabled = 1 AND bp.enabled = 1 AND bp.created <= ?"
        , $nowDate->format('Y-m-d H:i:s'));
        $this->db->query($query);
        $writerCount = $this->db->result();

        $return['platforma']->leaders = new stdClass();
        $return['platforma']->leaders->cnt = $writerCount->cnt;
        $return['platforma']->leaders->text = $this->getHeaderMenuSigns('авторов', 'автор', 'автора', $writerCount->cnt);

        $result = array(
            'menu' => $return,
            'currentRubrika' => $currentRubrika,
            'currentType' => $currentType,
        );
        return $result;
    }

    function getHeaderMenuSigns($ptext = '', $ptext1 = '', $ptext2 = '', $num = 0){
        $p1 = $num%10;
        $p2 = $num%100;

        if ($p1==1 && !($p2>=11 && $p2<=19)){
            $ptext = $ptext1;
        }

        if ($p1>=2 && $p1<=4 && !($p2>=11 && $p2<=19)){
            $ptext = $ptext2;
        }

        return $ptext;
    }


    /**
     * Вывод лидеров по тегу первог оуровня
     *
     * @param string $rubrikaUrl
     * @return array|int
     */
    function getLeaders($rubrikaUrl = '')
    {
        if ($rubrikaUrl == 'platform'){
            $query = sql_placeholder("SELECT bw.name, bw.tag_url, bw.id, count(DISTINCT bp.id) as total, bw.enabled
              FROM  blogwriters AS bw
              JOIN blogposts AS bp ON bp.writers = bw.id
              WHERE bw.useplatforma = 1 AND bw.private = 0 AND bw.bloger = 0 AND bw.enabled = 1  AND bp.enabled = 1
              GROUP BY bw.id
              ORDER BY total DESC");
            $this->db->query($query);
            $authors = $this->db->results();
        }
        else{
            $query = sql_placeholder("SELECT bt.id, bt.color, bt.url FROM blogtags as bt WHERE bt.url = ? AND bt.enabled = 1", $rubrikaUrl);
            $this->db->query($query);
            $blogTag = $this->db->result();

            $query = sql_placeholder("
            SELECT pt.name_lead, bw.name, bw.tag_url, bw.id, count(bp.id) AS total
              FROM post_tags AS pt
              JOIN blogwriters AS bw ON bw.leader = pt.id
              JOIN blogposts AS bp ON bp.writers = bw.id
             WHERE pt.enabled = 1 AND pt.parent = ? AND bw.private = 0 AND bw.bloger = 0 AND bw.enabled = 1 AND bp.enabled = 1 AND bw.useplatforma <> 1
          GROUP BY bw.id
          ORDER BY total DESC", $blogTag->id);
            $this->db->query($query);
            $authors = $this->db->results();
        }

        foreach ($authors AS $k=>$author){
            $authors[$k]->lenAuthor = strlen($author->name);
            $authors[$k]->tagurl = $blogTag->url;
            $authors[$k]->blogTagColor = $blogTag->color;
//            $authors[$k]->countPost = $author->total;
//            $authors[$k]->countPostText = $this->getHeaderMenuSigns('материалов', 'материал', 'материала', $author->total);
            $authors[$k]->countPosts->count = $author->total;
            $authors[$k]->countPosts->text = $this->getHeaderMenuSigns('материалов', 'материал', 'материала', $author->total);

            $authors[$k]->url = $author->tag_url;

        }

        $this->title = "Лидеры направления " . $this->tags[$rubrikaUrl] . " / +1";
        $this->description = "";
        $this->keywords = "";

        if ($rubrikaUrl == 'platform'){
            $header = "Авторы";
        }
        else{
            $header = "Лидеры";
        }



        return array(
            'authors' => $authors,
            'rubrikaUrl' => $rubrikaUrl,
            'header' => $header,
            'meta' => [
                'title' => $this->title,
                'description' => $this->description,
                'keywords' => $this->keywords,
                'ogTitle' => $this->title,
                'ogType' => 'article',
                'ogDescription' => $this->description,
                'ogImage' => '/og-plus-one.ru.png',
                'ogSiteName' => '',
                'ogUrl' => '',
                'twitterCard' => 'summary_large_image',
                'twitterTitle' => $this->title,
                'twitterDescription' =>  $this->description,
                'twitterUrl' => '',
                'twitterImage' => '/og-plus-one.ru.png',
                'twitterImageAlt' => '+1 — Проект об устойчивом развитии',
                'twitterSite' => ''
            ]
        );

//        $this->smarty->assign('authors', $authors);
//        $this->smarty->assign('rubrikaUrl', $rubrikaUrl);
//        $this->smarty->assign('header', $header);
//
//        $headerMenu = $this->getHeaderMenu();
//
//        $this->smarty->assign('headerMenu', $headerMenu['menu']);
//        $this->smarty->assign('rubrikaUrl', $headerMenu['currentRubrika']);
//        $this->smarty->assign('typeUrl', $headerMenu['currentType']);
//
//        $this->body = $this->smarty->fetch('allauthors.tpl');
//        return $this->body;
    }

    function getPostTags($tag){

        $query = sql_placeholder("
                SELECT pt.id, pt.url, pt.name, pt.name_lead, COUNT(rel.id) AS total
                  FROM post_tags AS pt
                  JOIN blogtags as bt ON bt.id = pt.parent
                  JOIN relations_postitem_tags AS rel ON rel.posttag_id = pt.id
                  JOIN blogposts AS bp ON bp.id = rel.post_id
                  JOIN blogwriters AS bw ON bp.writers = bw.id
                 WHERE bt.url = ? AND pt.enabled = 1 AND bp.tags = pt.parent AND bp.enabled = 1 AND bw.enabled = 1 AND bw.useplatforma <> 1
              GROUP BY pt.id ORDER BY total DESC", $tag);
        $this->db->query($query);
        $postTags = $this->db->results();

        $postTags->tag = $tag;

        foreach ($postTags AS $k => $res){
            $postTags[$k]->countPosts->count = $res->total;
            $postTags[$k]->countPosts->text = $this->getHeaderMenuSigns('материалов', 'материал', 'материала', $res->total);
        }

        $metaData = $this->getMetaData($this->tags[$tag], 'posttags');

        // метатеги
        $this->title = $metaData['metaTitle'];
        $this->description = $metaData['metaDescription'];
        $this->keywords = $metaData['metaKeyword'];

        return array(
            'tags' => $postTags,
            'tagUrl' => $tag,
            'meta' => [
                'title' => $this->title,
                'description' => $this->description,
                'keywords' => $this->keywords,
                'ogTitle' => $this->title,
                'ogType' => 'article',
                'ogDescription' => $this->description,
                'ogImage' => '/og-plus-one.ru.png',
                'ogSiteName' => '',
                'ogUrl' => '',
                'twitterCard' => 'summary_large_image',
                'twitterTitle' => $this->title,
                'twitterDescription' =>  $this->description,
                'twitterUrl' => '',
                'twitterImage' => '/og-plus-one.ru.png',
                'twitterImageAlt' => '+1 — Проект об устойчивом развитии',
                'twitterSite' => ''
            ]
        );

//        $this->smarty->assign('tags', $postTags);
//        $this->smarty->assign('tagUrl', $tag);
//        $headerMenu = $this->getHeaderMenu();

//        $this->smarty->assign('headerMenu', $headerMenu['menu']);
//        $this->smarty->assign('rubrikaUrl', $headerMenu['currentRubrika']);
//        $this->smarty->assign('typeUrl', $headerMenu['currentType']);
//        $this->body = $this->smarty->fetch('posttags.tpl');
//        return $this->body;

    }

    function countPostsByTag($tag){

        $query = sql_placeholder("SELECT COUNT(bp.id) AS cnt, pt.name FROM blogposts AS bp
                                    INNER JOIN relations_postitem_tags AS rel_pt ON rel_pt.post_id=bp.id
                                    INNER JOIN post_tags AS pt ON pt.id=rel_pt.posttag_id
                                  WHERE pt.url=?", $tag);
        $this->db->query($query);
        $postTagsHeader = $this->db->result();

        $postTagsHeader->text = $this->getHeaderMenuSigns('статей', 'статья', 'статьи', $postTagsHeader->cnt);

        $return = array(
            'tagHeader' => $postTagsHeader,
            'url' => $tag
        );
        return $return;
    }

    function getAllAuthors($page = 0, $usedIdsPosts = array(), $useElasticBlock = 0){

        $query = sql_placeholder("SELECT w.*, t.name_lead AS lead, bt.color AS blog_tags_color, bt.url AS tagurl FROM blogwriters AS w
                                    LEFT JOIN post_tags AS t ON t.id = w.leader
                                    LEFT JOIN blogtags AS bt ON bt.id=t.parent
                                    WHERE w.enabled = 1
                                    ORDER BY w.name ASC");
        $this->db->query($query);
        $authors = $this->db->results();

        foreach ($authors AS $k=>$author){
            $authors[$k]->lenAuthor = strlen($author->name);
        }

        $this->smarty->assign('authors', $authors);

        $metaData = $this->getMetaData('main', 'authors');

        $this->title = $metaData['metaTitle'];
        $this->description = $metaData['metaDescription'];
        $this->keywords = $metaData['metaKeyword'];


        $this->body = $this->smarty->fetch('autorslist.tpl');
        return $this->body;
    }

    function getPictureDay($rubrikaUrl = 'main')
    {
        $pictureDay = array();

        if ($rubrikaUrl == 'main'){
            $query = sql_placeholder("SELECT name, header, url, tags FROM blogposts AS b
                          WHERE
                          b.enabled=1
                          AND b.picture_day = 1
                          AND b.show_main_page=1
                          ORDER BY b.created DESC
                          LIMIT 9");
            $this->db->query($query);
            $pictureDay = $this->db->results();

            foreach ($pictureDay AS $k => $pd){
                $query = "SELECT name, url FROM blogtags WHERE id=" . $pd->tags;
                $this->db->query($query);
                $pictureDay[$k]->tags = $this->db->result();
            }
        }

        return $pictureDay;
    }

    /**
     * получение постов по рубрикам или для главной страницы
     *
     * @param string $rubrika_url
     * @param int $page
     * @param array $usedIdsPosts
     * @return array|bool
     */
    function getPosts($rubrika_url = '', $page = 1, $usedIdsPosts = array(), $ajax = true, $getsimpleblogs = 0, $confirmSubscribe = 0)
    {
        $andWhereUsefulCity = "";
        if ($_GET['mode'] == 'getsimpleblogs'){
            $andWhereUsefulCity = " AND useful_city = 1";
        }
        $andRubrikaUrlWhere = "";
        if (!empty($rubrika_url)) {
            $query = sql_placeholder("SELECT bt.id AS `bt_id` FROM blogtags as bt WHERE bt.url = ? AND bt.enabled =1 " . $andWhereUsefulCity, $rubrika_url);
            $this->db->query($query);
            $res = $this->db->result();
            if ($res) {$andRubrikaUrlWhere  = " AND b.tags = {$res->bt_id}";}
        }

        $query = sql_placeholder("SELECT COUNT(b.id) AS `count` FROM blogposts AS b JOIN blogwriters bw ON b.writers = bw.id
                                    WHERE b.enabled=1 AND bw.enabled = 1 " . $andWhereUsefulCity . $andRubrikaUrlWhere);
        $this->db->query($query);
        $resultCount = $this->db->result();

        $totalPages = ceil($resultCount->count / $this->numItemsOnPage);
        if ($page > $totalPages) {
            return null;
        }

        if ($rubrika_url != 'platform'){
            $query = sql_placeholder("SELECT * FROM blogtags WHERE enabled=1 AND url=?", $rubrika_url);
            $this->db->query($query);

            if ($this->db->num_rows() == 0) {
                return false;
            }
            $rubrika = $this->db->result();
        }

        if ($rubrika_url != "") {
            $start = ($page - 1) * $this->itemPerPage;

            $result = $this->getListPosts($rubrika_url, $rubrika->id, null, $start);

            $result = Helper::formatPostList($result, '1_3');

            $page = intval($page + 1);


            // метатеги
            $meta = $this->getMetaData($rubrika_url);
//            if ($getsimpleblogs){
//                $this->title = $this->settings->site_name;
//            }
//            else{
                $this->title = $meta['metaTitle'];
//            }

            if ($ajax){

                $return = array(
                    'items' => $result,
                    'meta' => [
                        'title' => $this->title,
                        'description' => $meta['metaDescription'],
                        'keywords' => $meta['metaKeyword'],
                        'ogTitle' => $this->title,
                        'ogType' => 'article',
                        'ogDescription' => $meta['metaDescription'],
                        'ogImage' => '/og-plus-one.ru.png',
                        'ogSiteName' => '',
                        'ogUrl' => '',
                        'twitterCard' => 'summary_large_image',
                        'twitterTitle' => $this->title,
                        'twitterDescription' =>  $meta['metaDescription'],
                        'twitterUrl' => '',
                        'twitterImage' => '/og-plus-one.ru.png',
                        'twitterImageAlt' => '+1 — Проект об устойчивом развитии',
                        'twitterSite' => ''
                    ],
                    'page' => $page,
                );
                return $return;
            }
            else{

                if ($rubrika_url == 'platform'){
                    $header = "Платформа";
                }
                else{
                    $header = $rubrika->name;
                }


                $this->description = $meta['metaDescription'];
                $this->keywords = $meta['metaKeyword'];
                $this->smarty->assign('items', $result);
                $this->smarty->assign('header', $header);
                $this->smarty->assign('rubrika', $rubrika_url);
                $this->smarty->assign('page', $page);
                $this->smarty->assign('subscribe', $confirmSubscribe);
                $headerMenu = $this->getHeaderMenu();

                $this->smarty->assign('headerMenu', $headerMenu['menu']);
                $this->smarty->assign('rubrikaUrl', $headerMenu['currentRubrika']);
                $this->smarty->assign('typeUrl', $headerMenu['currentType']);

                $this->body = $this->smarty->fetch('ajaxBlogs.tpl');
                return $this->body;
            }
        }
        return false;
    }

    /**
     * получение постов по автору блога
     *
     * @param string $writerId
     * @param int $page
     * @param array $usedIdsPosts
     * @param int $useElasticBlock
     * @param bool $ajax
     * @return array|bool
     * @throws Exception
     */
    function getAuthorsPosts($writerId = '', $page = 0, $usedIdsPosts = array(), $useElasticBlock = 0, $ajax = true)
    {
        $query = sql_placeholder("SELECT bw.* FROM blogwriters AS bw WHERE bw.enabled=1 AND bw.tag_url = ?", $writerId);
        $this->db->query($query);
        $user = $this->db->result();

/*        if (!$user) {
            $query = sql_placeholder(" SELECT buh.* FROM blogwriters_url_history AS buh  WHERE buh.url = ?", $writerId);
            $this->db->query($query);
            $urlHistory = $this->db->results();
            if (count($urlHistory) > 0 ) {
                $url = array_pop($urlHistory);
                $query = sql_placeholder("SELECT bw.* FROM blogwriters AS bw WHERE bw.enabled=1 AND bw.id = ?", $url->writers);
                $this->db->query($query);
                $user = $this->db->result();
                if ($url->url !== $user->tag_url) {
                    header('Location: /author/' . ($user->tag_url ? $user->tag_url : $user->id), true, 301);
                }
            }
        } else {
            if ($user->tag_url) {
                header('Location: /author/' . $user->tag_url, true, 301);
                exit(0);
            }
        }
*/
        $nowDate = new DateTime();
        $query = sql_placeholder(" SELECT COUNT(b.id) AS `count` FROM blogposts AS b WHERE b.enabled = 1 AND b.writers = ? AND b.created <= ?"
            , $user->id
            , $nowDate->format('Y-m-d H:i:s'));
        $this->db->query($query);
        $resultCount = $this->db->result();
        $totalPages = ceil($resultCount->count / $this->numItemsOnPage);


        // метатеги, информация об авторе
        $meta = $this->getMetaData($user->id, 'author');
        $this->title = $meta['metaTitle'];
        $this->description = $meta['metaDescription'];
        $this->keywords = $meta['metaKeyword'];

        if ($page > $totalPages) {
            $return = array(
                'posts' => null,
                'page' => $page,

            );
            return $return;
        }

        if ($user->id != "") {
            $start = ($page - 1) * $this->numItemsOnPage;

            $result = $this->getListPosts(null, null, $user->id, $start, $this->numItemsOnPage);

            $result = Helper::formatPostList($result, '1_3');

            $page = intval($page + 1);

            if ($ajax){
                $return = array(
                    'posts' => $result,
                    'writerInfo' => $meta['writer'],
                    'page' => $page,
                    'meta' => [
                        'title' => $this->title,
                        'description' => $this->description,
                        'keywords' => $this->keywords,
                        'ogTitle' => $this->title,
                        'ogType' => 'article',
                        'ogDescription' => $this->description,
                        'ogImage' => '/og-plus-one.ru.png',
                        'ogSiteName' => '',
                        'ogUrl' => '',
                        'twitterCard' => 'summary_large_image',
                        'twitterTitle' => $this->title,
                        'twitterDescription' =>  $this->description,
                        'twitterUrl' => '',
                        'twitterImage' => '/og-plus-one.ru.png',
                        'twitterImageAlt' => '+1 — Проект об устойчивом развитии',
                        'twitterSite' => ''
                    ]
                );


                return $return;
            }
            else{


                $this->smarty->assign('items', $result);
                $this->smarty->assign('writerId', $user->id);
                $this->smarty->assign('writerInfo', $meta['writer']);
                $this->smarty->assign('page', $page);

                $headerMenu = $this->getHeaderMenu();

                $this->smarty->assign('headerMenu', $headerMenu['menu']);
                $this->smarty->assign('rubrikaUrl', $headerMenu['currentRubrika']);
                $this->smarty->assign('typeUrl', $headerMenu['currentType']);

                $this->body = $this->smarty->fetch('ajaxBlogs.tpl');
                return $this->body;
            }
        }
        return false;
    }


    /**
     * метод возвращает теги второго уровня для статьи,
     *
     * Помимо самого тега второг оуровня - метод так же вернет
     * и название тега перовго уровня, к которому он привязан, и его урл.
     *
     * @param int $postId - Id поста - к которому надо вернуть список тегов второг оуровня
     * @return array|int|null
     */
    function getPostTagTwoLevel($postId){
        $postTagTwoLevel = null;
        if ($postId){
            $query = "SELECT t.name, t.url, bt.name AS blogtag_name, bt.url AS blogtag_url FROM relations_postitem_tags AS rel
                              INNER JOIN post_tags AS t ON t.id=rel.posttag_id
                              INNER JOIN blogtags AS bt ON bt.id=t.parent
                              WHERE rel.post_id=" .$postId . " AND t.enabled=1 ORDER BY t.name";
            $this->db->query($query);
            $postTagTwoLevel = $this->db->results();
        }

        return $postTagTwoLevel;
    }

    function getPostTagMainPage($tagId){
        $postTagMainPage = null;
        if ($tagId){
//            $query = "SELECT name, url FROM post_tags WHERE id=" . $tagId;

            $query = "SELECT t.name, t.url, bt.name AS blogtag_name, bt.url AS blogtag_url FROM relations_postitem_tags AS rel
                              INNER JOIN post_tags AS t ON t.id=rel.posttag_id
                              INNER JOIN blogtags AS bt ON bt.id=t.parent
                              WHERE t.id=" .$tagId ;

            $this->db->query($query);
            $postTagMainPage = $this->db->result();
        }

        return $postTagMainPage;
    }

    /**
     * метод формирует данные для отображения в карточкас с типом "данные"
     *
     * @param int $postId Id поста с данными, для которого необходимо вернуть эти самые данные
     * @param int $writerId Id писателя, "автора" поста, по которому формируются данные
     * @return array|int
     */
    function getAnalitycData($postId, $writerId){

        $query = sql_placeholder("SELECT years, value_to_yeats FROM blogwriters_data WHERE post_id=? AND writer_id=? ORDER BY years ASC LIMIT 7",
            $postId,
            $writerId);
        $this->db->query($query);
        $analitycData = $this->db->results();

        if (!empty($analitycData)){
            $query = sql_placeholder("SELECT year_to_ammount AS years, amount_to_account AS value_to_yeats FROM blogposts WHERE id=? AND writers=? LIMIT 1",
                $postId,
                $writerId);
            $this->db->query($query);
            $analitycDataCurrent = $this->db->result();

            // нам нужны еще и данные по текущей записи (текущеиму году)
            if (intval($analitycDataCurrent->years) > 0){
                array_push($analitycData, $analitycDataCurrent);
            }

            $query = sql_placeholder("SELECT bp.amount_to_account, MAX(wd.value_to_yeats) AS max_value FROM blogwriters_data AS wd
                                    INNER JOIN blogposts AS bp ON bp.id=wd.post_id
                                    WHERE wd.post_id=? AND wd.writer_id=?",
                $postId,
                $writerId);

            $this->db->query($query);
            $maxValue = $this->db->result();

            if ($maxValue->max_value > $maxValue->amount_to_account){
                $maxValue = $maxValue->max_value;
            }
            elseif($maxValue->max_value < $maxValue->amount_to_account){
                $maxValue = $maxValue->amount_to_account;
            }
            else{
                $maxValue = $maxValue->amount_to_account;
            }

            foreach ($analitycData AS $keyAnalitic=>$valueAnalitic){
                if ($maxValue == $valueAnalitic->value_to_yeats){
                    $analitycData[$keyAnalitic]->percent = 100;
                }
                else{
                    $p = $valueAnalitic->value_to_yeats * 100 / $maxValue;
                    $analitycData[$keyAnalitic]->percent = round($p);
                }
            }
        }



        return $analitycData;
    }

    /**
     * возвращает набор данных для RSS канала
     *
     * @return array|int
     */
    function getRssFeed(){

        $query = sql_placeholder("SELECT b.id, b.header_rss, b.header, b.url, b.image_rss, t.name AS tag, t.url AS tag_url, DATE_FORMAT(b.created, '%d.%m.%Y %H:%i') AS post_created FROM blogposts AS b
                          INNER JOIN blogtags AS t on t.id=b.tags
                          WHERE b.enabled = 1
                          AND b.url != ''
                          AND b.header_rss != ''
                          ORDER BY b.created DESC");
        $this->db->query($query);
        $result = $this->db->results();

        return $result;
    }

    function getAboutPage($ajax = false){
        $query = sql_placeholder("SELECT * FROM about LIMIT 1");
        $this->db->query($query);
        $item = $this->db->result();

        $item->meta_title = "+1 — О проекте";

        $item->meta_description = "Проект +1 (Плюс Один) — коммуникационный проект, рассказывающий о лидерских практиках в области социальной и экологической ответственности. Мы создаем площадку для прямой коммуникации и обмена ресурсами между бизнесом, некоммерческими организациями, государством и обществом.";

        $item->meta_keywords = "";

        $item->ogImage = "https://plus-one.ru/og-plus-one.ru.png?";

        $item->ogUrl = "https://plus-one.ru/about";
        $item->ogSiteName = $this->settings->site_name;



        if ($ajax){
            $result = [
                'content' => $item,
                'meta' => [
                    'title' => $item->meta_title,
                    'description' => $item->meta_description,
                    'keywords' => $item->meta_keywords,
                    'ogTitle' => $item->meta_title,
                    'ogType' => 'article',
                    'ogDescription' => $item->meta_description,
                    'ogImage' => $item->ogImage,
                    'ogSiteName' => $item->ogSiteName,
                    'ogUrl' => $item->ogUrl,
                    'twitterCard' => 'summary_large_image',
                    'twitterTitle' => $item->meta_title,
                    'twitterDescription' => $item->meta_description,
                    'twitterUrl' => $item->ogUrl,
                    'twitterImage' => $item->ogImage,
                    'twitterImageAlt' => $item->meta_title,
                    'twitterSite' => $item->ogSiteName
                ],
            ];

            return $result;
        }
        else {

            $this->title = "+1 — О проекте";
            $this->smarty->assign('about', $item);

            $this->body = $this->smarty->fetch('about.tpl');
            return $this->body;
        }
    }

    function getEvents(){
        $query = sql_placeholder("SELECT * FROM events WHERE enabled = 1 ORDER BY created DESC");
        $this->db->query($query);
        $result = $this->db->results();

        foreach ($result AS $k => $event){
            $query = sql_placeholder("SELECT * FROM blogtags WHERE id = ?", $event->tag);
            $this->db->query($query);
            $tag = $this->db->result();

            if ($tag->url == Blog::TAG_ECONOMY){
                $tag->blockColor = 'blue';
            }

            if ($tag->url == Blog::TAG_ECOLOGY){
                $tag->blockColor = 'green';
            }

            if ($tag->url == Blog::TAG_COMMUNITY){
                $tag->blockColor = 'yellow';
            }

            $result[$k]->tags = $tag;
        }

        return $result;
    }

    function getEvent($url){
        $query = sql_placeholder("SELECT * FROM events WHERE url = ?", $url);
        $this->db->query($query);
        $result = $this->db->result();

        $query = sql_placeholder("SELECT * FROM blogtags WHERE id = ?", $result->tag);
        $this->db->query($query);
        $tag = $this->db->result();

        if ($tag->url == Blog::TAG_ECONOMY) {
            $tag->blockColor = 'blue';
        }

        if ($tag->url == Blog::TAG_ECOLOGY) {
            $tag->blockColor = 'green';
        }

        if ($tag->url == Blog::TAG_COMMUNITY) {
            $tag->blockColor = 'yellow';
        }

        $result->tags = $tag;


        return $result;
    }

    function translitIt($str)
    {
        $tr = array(
            "А" => "a", "Б" => "b", "В" => "v", "Г" => "g",
            "Д" => "d", "Е" => "e", "Ё" => "E", "Ж" => "j", "З" => "z", "И" => "i",
            "Й" => "y", "К" => "k", "Л" => "l", "М" => "m", "Н" => "n",
            "О" => "o", "П" => "p", "Р" => "r", "С" => "s", "Т" => "t",
            "У" => "u", "Ф" => "f", "Х" => "h", "Ц" => "ts", "Ч" => "ch",
            "Ш" => "sh", "Щ" => "sch", "Ъ" => "", "Ы" => "yi", "Ь" => "",
            "Э" => "e", "Ю" => "yu", "Я" => "ya", "а" => "a", "б" => "b",
            "в" => "v", "г" => "g", "д" => "d", "е" => "e", "ё" => "e", "ж" => "j",
            "з" => "z", "и" => "i", "й" => "y", "к" => "k", "л" => "l",
            "м" => "m", "н" => "n", "о" => "o", "п" => "p", "р" => "r",
            "с" => "s", "т" => "t", "у" => "u", "ф" => "f", "х" => "h",
            "ц" => "ts", "ч" => "ch", "ш" => "sh", "щ" => "sch", "ъ" => "y",
            "ы" => "yi", "ь" => "", "э" => "e", "ю" => "yu", "я" => "ya",
            " " => "-", "." => "", "/" => "-", "»" => "", "«" => "",
            " - " => "-", " — " => "-", "&quot;" => "", "!" => "",
            "”" => "", "“" => "", "," => "", "(" => "", ")" => "",
            "°" => "", "\"" => "", "№" => "", "*" => "-"
        );
        return strtr($str, $tr);
    }

    function getActualVideo()
    {
        $query = sql_placeholder("SELECT id, image_1_1_c, image_1_4 FROM `blogposts` WHERE type_post=11 ORDER BY created DESC LIMIT 1");
        $this->db->query($query);
        $video = $this->db->result();

        $videoFragments = $this->getVideoFragments($video->id);

        $result = [
            "imageDesktop" => "/files/blogposts/" . $video->image_1_1_c,
            "imageMobile" => "/files/blogposts/" . $video->image_1_4,
            "items" => $videoFragments
        ];
        return $result;
    }

    function getVideoFragments($videoId = 0){

        $result = new stdClass();

        if ($videoId != 0){
            $query = sql_placeholder("SELECT
                    id, video_url AS frame, name
                    FROM videos WHERE parent_video_post=?", $videoId);
            $this->db->query($query);
            $videos = $this->db->results();

            foreach ($videos AS $k=>$video){
                $query = sql_placeholder("SELECT id, name, minute, secunde FROM division_time_videos WHERE
                parent_video=?
                AND enabled=1
                AND name != ''
              ORDER BY minute, secunde ASC", $video->id);
                $this->db->query($query);
                $videoFragments = $this->db->results();

                foreach ($videoFragments AS $key => $fragment){

                    $min = $fragment->minute;
                    $sec = $fragment->secunde;

                    if (strlen($min) < 2){
                        $min = '0' . $min;
                    }

                    if (strlen($sec) < 2){
                        $sec = '0' . $sec;
                    }

                    $videoFragments[$key]->name = $min . ":" . $sec . ": " . $fragment->name;
                    $videoFragments[$key]->currentTime = $min * 60 + $sec;
                }
                $videos[$k]->fragments = $videoFragments;

            }
        }
        return $videos;
    }

    function test(){
        $query = sql_placeholder("SELECT b.id, b.body FROM blogposts AS b
                          WHERE b.body != ''
                          ORDER BY b.created DESC");
        $this->db->query($query);
        $result = $this->db->results();

        return $result;
    }

    function saveTr($body, $id){
        if ($body && $id){
            $query = sql_placeholder('UPDATE blogposts SET body=? WHERE id=?', $body, $id);
            $this->db->query($query);
        }
    }


    function getListPosts($rubrika_url, $rubrikaId, $writerId = null, $start, $limit = null ) {

        $limit = $limit ? $limit : $this->itemPerPage;

        $andWhereUsefulCity = "";
        if ($_GET['mode'] == 'getsimpleblogs'){
            $andWhereUsefulCity = " AND useful_city = 1";
        }

        $nowDate = new DateTime();
        if (is_null($writerId)){
            if ($rubrika_url == 'main') {
                $query = sql_placeholder("SELECT b.id, b.blocks, b.tags, b.name, b.header, b.url, b.type_post, b.is_partner_material,  b.url_target,
                            b.type_post, b.partner_url, b.post_tag, b.signature_to_sum, b.text_body, b.video_on_main, b.state, b.type_video,
                            b.amount_to_displaying, b.body_color, bw.name AS writer, bw.id AS writer_id, bw.private, bw.bloger, bw.name_genitive,
                            bw.image AS writerImage, bw.stub_image, bw.stub_link, p.name AS partner,
                            p.color AS partner_color, b.anounce_new_author, b.spec_project, b.conference,
                            sp.name AS specProjectName, sp.external_url AS specProjectUrl, sp.color AS marker_color,
                            sp.font_color AS font_color,  b.image_1_1, b.image_1_2, b.image_1_3, b.image_rss,
                            sp.image_1_1 AS specProjectImage, sp.header AS specProjectHeader,
                            conf.name AS confName, conf.external_url AS confUrl, conf.color AS confColor,
                            conf.font_color AS confFontColor, b.citate_author_name,
                            DATE_FORMAT(b.created, '%d.%m.%Y') AS date_created,
                            bt.name AS tagName, bt.url AS tagUrl, b.format, b.font, p.name AS partnerName
                            ,b.type_announce_1_1, b.type_announce_1_2, b.type_announce_1_3
                            ,sp.name AS sp_name, sp.header AS sp_header, sp.external_url AS sp_url
                            ,sp.image_1_1 AS sp_image_1_1, sp.image_1_2 AS sp_image_1_2, sp.image_1_3 AS sp_image_1_3
                            ,sp.type_announce_1_1 AS sp_type_announce_1_1
                            ,sp.type_announce_1_2 AS sp_type_announce_1_2
                            ,sp.type_announce_1_3 AS sp_type_announce_1_3,
                            bw.bloger, bw.name as writerName, bw.announcement_img as blogImg, bw.id as writerId, bw.tag_url as writerTag
                           FROM blogposts AS b
                      LEFT JOIN blogwriters AS bw ON bw.id=b.writers
                      LEFT JOIN partners AS p ON p.id=b.partner
                      LEFT JOIN blogtags AS bt ON bt.id = b.tags
                      LEFT JOIN spec_projects AS sp ON sp.id=b.spec_project
                      LEFT JOIN conferences AS conf ON conf.id=b.conference
                          WHERE
                          b.enabled = 1 AND (bw.enabled = 1 OR b.spec_project ) AND bt.enabled = 1
                          AND b.created <= ?
                          AND b.blocks != 'ticker'
                          AND bw.useplatforma <> 1
                          " . $andWhereUsefulCity . "
                          ORDER BY b.created DESC
                          LIMIT ?, ?",
                    $nowDate->format('Y-m-d H:i:s'),
                    $start,
                    $limit);
            }
            elseif($rubrika_url == 'platform'){
                $query = sql_placeholder("SELECT b.id, b.blocks_blog AS blocks, b.tags, b.name, b.header, b.url, b.type_post, b.is_partner_material,  b.url_target,
                            b.type_post, b.partner_url, b.post_tag, b.signature_to_sum, b.text_body, b.video_on_main, b.state, b.type_video,
                            b.amount_to_displaying, b.body_color, bw.name AS writer, bw.id AS writer_id, bw.private, bw.bloger, bw.name_genitive,
                            bw.image AS writerImage, bw.stub_image, bw.stub_link, p.name AS partner,
                            p.color AS partner_color, b.anounce_new_author, b.spec_project,  b.conference,
                            sp.name AS specProjectName, sp.external_url AS specProjectUrl, sp.color AS marker_color,
                            sp.font_color AS font_color,  b.image_1_1, b.image_1_2, b.image_1_3, b.image_rss,
                            sp.image_1_1 AS specProjectImage, sp.header AS specProjectHeader,
                            conf.name AS confName, conf.external_url AS confUrl, conf.color AS confColor,
                            conf.font_color AS confFontColor,  b.citate_author_name,
                            DATE_FORMAT(b.created, '%d.%m.%Y') AS date_created,
                            bt.name AS tagName, bt.url AS tagUrl, b.format, b.font, p.name AS partnerName
                            ,b.type_announce_1_1, b.type_announce_1_2, b.type_announce_1_3
                            ,sp.name AS sp_name, sp.header AS sp_header, sp.external_url AS sp_url
                            ,sp.image_1_1 AS sp_image_1_1, sp.image_1_2 AS sp_image_1_2, sp.image_1_3 AS sp_image_1_3
                            ,sp.type_announce_1_1 AS sp_type_announce_1_1
                            ,sp.type_announce_1_2 AS sp_type_announce_1_2
                            ,sp.type_announce_1_3 AS sp_type_announce_1_3,
                            bw.bloger, bw.name as writerName, bw.announcement_img as blogImg, bw.id as writerId, bw.tag_url as writerTag
                              FROM blogposts AS b
                            INNER JOIN blogwriters AS bw ON bw.id=b.writers
                            LEFT JOIN partners AS p ON p.id=b.partner
                            LEFT JOIN blogtags AS bt ON bt.id = b.tags
                            LEFT JOIN spec_projects AS sp ON sp.id=b.spec_project
                            LEFT JOIN conferences AS conf ON conf.id=b.conference
                            WHERE bw.useplatforma = 1
                            AND b.enabled = 1 AND (bw.enabled = 1 OR b.spec_project ) AND bt.enabled = 1
                            AND b.created <= ?
                            AND b.blocks != 'ticker'
                            AND b.type_post NOT IN (?@) 
                            ORDER BY b.created DESC
                          LIMIT ?, ?",
                    $nowDate->format('Y-m-d H:i:s'),
                    array(Helper::TYPE_PARTNER_ANNOUNCE),
                    $start,
                    $limit
                    );
            }
            else {
                $query = sql_placeholder("SELECT b.id, b.blocks_blog AS blocks, b.tags, b.name, b.header, b.url, b.type_post, b.is_partner_material,  b.url_target,
                            b.type_post, b.partner_url, b.post_tag, b.signature_to_sum, b.text_body, b.video_on_main, b.state, b.type_video,
                            b.amount_to_displaying, b.body_color, bw.name AS writer, bw.id AS writer_id, bw.private, bw.bloger, bw.name_genitive,
                            bw.image AS writerImage, bw.stub_image, bw.stub_link, p.name AS partner,
                            p.color AS partner_color, b.anounce_new_author, b.spec_project,  b.conference,
                            sp.name AS specProjectName, sp.external_url AS specProjectUrl, sp.color AS marker_color,
                            sp.font_color AS font_color,  b.image_1_1, b.image_1_2, b.image_1_3, b.image_rss,
                            sp.image_1_1 AS specProjectImage, sp.header AS specProjectHeader,
                            conf.name AS confName, conf.external_url AS confUrl, conf.color AS confColor,
                            conf.font_color AS confFontColor, b.citate_author_name,
                            DATE_FORMAT(b.created, '%d.%m.%Y') AS date_created,
                            bt.name AS tagName, bt.url AS tagUrl, b.format, b.font, p.name AS partnerName
                            ,b.type_announce_1_1, b.type_announce_1_2, b.type_announce_1_3
                            ,sp.name AS sp_name, sp.header AS sp_header, sp.external_url AS sp_url
                            ,sp.image_1_1 AS sp_image_1_1, sp.image_1_2 AS sp_image_1_2, sp.image_1_3 AS sp_image_1_3
                            ,sp.type_announce_1_1 AS sp_type_announce_1_1
                            ,sp.type_announce_1_2 AS sp_type_announce_1_2
                            ,sp.type_announce_1_3 AS sp_type_announce_1_3,
                            bw.bloger, bw.name as writerName, bw.announcement_img as blogImg, bw.id as writerId, bw.tag_url as writerTag
                            FROM blogposts AS b
                          LEFT JOIN blogwriters AS bw ON bw.id=b.writers
                          LEFT JOIN partners AS p ON p.id=b.partner
                          LEFT JOIN blogtags AS bt ON bt.id = b.tags
                          LEFT JOIN spec_projects AS sp ON sp.id=b.spec_project
                          LEFT JOIN conferences AS conf ON conf.id=b.conference
                          WHERE
                          b.enabled = 1 AND (bw.enabled = 1 OR b.spec_project )
                          AND b.created <= ?
                          AND b.blocks != 'ticker'
                          AND bw.useplatforma = 0
                          AND tags=?
                          AND b.type_post NOT IN (?@) 
                          ORDER BY b.created DESC
                          LIMIT ?, ?",
                    $nowDate->format('Y-m-d H:i:s'),
                    $rubrikaId,
                    array(Helper::TYPE_PARTNER_ANNOUNCE),
                    $start,
                    $limit);
            }
        }
        else{
            $query = sql_placeholder("SELECT b.id, b.blocks_author AS blocks, b.tags, b.name, b.header, b.url, b.type_post, b.is_partner_material,  b.url_target,
                            b.type_post, b.post_tag, b.signature_to_sum, b.text_body, b.amount_to_displaying, b.video_on_main, b.state, b.type_video,
                            b.partner_url, b.body_color, bw.name AS writer, bw.id AS writer_id, bw.private, bw.bloger, bw.name_genitive,
                            bw.image AS writerImage, bw.stub_image, bw.stub_link, p.name AS partner,
                            p.color AS partner_color, b.anounce_new_author, b.spec_project, b.conference,
                            sp.name AS specProjectName, sp.external_url AS specProjectUrl, sp.color AS marker_color,
                            sp.font_color AS font_color, b.image_1_1, b.image_1_2, b.image_1_3, b.image_rss,
                            conf.name AS confName, conf.external_url AS confUrl, conf.color AS confColor,
                            conf.font_color AS confFontColor, b.citate_author_name,
                            DATE_FORMAT(b.created, '%d.%m.%Y') AS date_created,
                            bt.name AS tagName, bt.url AS tagUrl, b.format, b.font, p.name AS partnerName
                            ,b.type_announce_1_1, b.type_announce_1_2, b.type_announce_1_3
                            ,sp.name AS sp_name, sp.header AS sp_header, sp.external_url AS sp_url
                            ,sp.image_1_1 AS sp_image_1_1, sp.image_1_2 AS sp_image_1_2, sp.image_1_3 AS sp_image_1_3
                            ,sp.type_announce_1_1 AS sp_type_announce_1_1
                            ,sp.type_announce_1_2 AS sp_type_announce_1_2
                            ,sp.type_announce_1_3 AS sp_type_announce_1_3,
                            bw.bloger, bw.name as writerName, bw.announcement_img as blogImg, bw.id as writerId, bw.tag_url as writerTag
                            FROM blogposts AS b
                          INNER JOIN blogwriters AS bw ON bw.id=b.writers
                          LEFT JOIN partners AS p ON p.id=b.partner
                          LEFT JOIN blogtags AS bt ON bt.id = b.tags
                          LEFT JOIN spec_projects AS sp ON sp.id=b.spec_project
                          LEFT JOIN conferences AS conf ON conf.id=b.conference
                          WHERE
                          b.enabled = 1 AND (bw.enabled = 1 OR b.spec_project ) AND bt.enabled = 1
                          AND b.created <= ?
                          AND b.blocks != 'ticker'
                          AND b.writers=?
                          AND b.type_post NOT IN (?@) 
                          ORDER BY b.created DESC
                          LIMIT ?, ?",
                $nowDate->format('Y-m-d H:i:s'),
                $writerId,
                array(Helper::TYPE_PARTNER_ANNOUNCE),
                $start,
                $limit);

        }

        if (!empty($query)){
            $this->db->query($query);
            $result = $this->db->results();
        }
        else{
            $result = null;
        }
        return $result;
    }


    function getListNews($rubrika_url, $start, $limit = null ) {
        $limit = $limit ? $limit : $this->itemPerPage;

        $nowDate = new DateTime();
        $query = sql_placeholder("SELECT 'news' as  type_post, n.id, n.tags, n.header as name, n.text_content as header, n.url, n.is_partner_material,
                    n.image_1_1, n.image_1_2, n.image_1_3, n.image_rss,
                    n.type_announce_1_1, n.type_announce_1_2, n.type_announce_1_3,
                    n.meta_title, n.meta_description, n.meta_keywords,
                    n.text_content as lead, bw.name AS writer, bw.id AS writer_id, bw.private, bw.bloger, bw.name_genitive,
                    bw.image AS writerImage, bw.stub_image, bw.stub_link,
                    DATE_FORMAT(n.created, '%d.%m.%Y') AS date_created,
                    bt.name AS tagName, bt.url AS tagUrl, CONCAT ('news/', DATE_FORMAT(n.created, '%Y/%m/%d')) AS postUrl,
                    bw.bloger, bw.name as writerName, bw.announcement_img as blogImg, bw.id as writerId, bw.tag_url as writerTag,
                    n.format as format
                   FROM news AS n
              LEFT JOIN blogwriters AS bw ON bw.id=n.writers
              LEFT JOIN blogtags AS bt ON bt.id = n.tags
              WHERE
                  n.enabled = 1 AND bw.enabled = 1
                  AND n.created <= ?
                  AND bw.useplatforma <> 1
                  ORDER BY n.created DESC
                  LIMIT ?, ?",
            $nowDate->format('Y-m-d H:i:s'),
            $start,
            $limit);

        if (!empty($query)){
            $this->db->query($query);
            $result = $this->db->results();
        }
        else{
            $result = null;
        }
        return $result;
    }

    function htmlReplacer($str)
    {

        $str = trim ($str);
        $tr = array(
            "<p>" => "", "</p>" => "",
            "&laquo;" => "","&raquo;" => "","&mdash;" => "","&ndash;" => "", "&amp;" => "", "&pound;" => "", "&trade;" => "",
            "&cent;" => "", "&yen;" => "", "&uml;" => "", "&deg;" => "", "&plusmn;" => "", "&sup1;" => "", "&sup2;" => "",
            "&sup3;" => "", "&acute;" => "", "&micro;" => "", "&para;" => "", "&middot;" => "", "&ordm;" => "",
            "&frac12;" => "", "&frac34;" => "", "&times;" => "", "&ordf;" => "", "&sect;" => "", "&curren;" => "",
            "&rsaquo;" => " ",
            "&lsaquo;" => " ",
            "&tilde;" => " ",
            "&bull;" => " ",
            "&rdquo;" => " ",
            "&ldquo;" => " ",
            "&rsquo;" => " ",
            "&lsquo;" => " ",
            "&permil;" => " ",
            "&circ;" => " ",
            "&hellip;" => " ",
            "&bdquo;" => " ",
            "&sbquo;" => " ",
            "&euro;" => " ",
            "&fnof;" => " ",
            "&macr;" => " ",
            "&cedil;" => " ",
            "&reg;" => "-",
            "&copy;" => "-",
            "&quot;" => "-",
            "&lt;" => "-",
            "&gt;" => "-",
            "&nbsp;" => "-",

        );
        return strtr($str,$tr);

    }

    private function findByHistoryUrl($url, $rubrika_url)
    {
        $nowDate = new DateTime();
        $query = sql_placeholder("SELECT bp.id, bp.url FROM blogposts bp
                                   WHERE EXISTS (SELECT null FROM blogposts_url_history buh WHERE bp.id = buh.blogpost_id AND buh.url = ? ) AND bp.created <= ?"
                    , $url, $nowDate->format('Y-m-d H:i:s'));
        $this->db->query($query);
        $res = $this->db->results();
        if (count($res) > 0) {
            $bp = array_pop($res);
            header( 'Location: /blog/'. $rubrika_url . '/' . $bp->url, true, 301 );
            exit(0);
        }
    }

    private function findByNewsHistoryUrl($url, $rubrika_url)
    {
        $nowDate = new DateTime();
        $query = sql_placeholder("SELECT n.id, n.url, n.created FROM news n
                                   WHERE EXISTS (SELECT null FROM news_url_history buh WHERE n.id = buh.news_id AND buh.url = ? ) AND n.created <= ?"
                    , $url, $nowDate->format('Y-m-d H:i:s'));
        $this->db->query($query);
        $res = $this->db->results();
        if (count($res) > 0) {
            $newsItem = array_pop($res);
            $newsDate = (new DateTime($newsItem->created))->format('Y/m/d');
            header( "Location: /news/$newsDate/" . $newsItem->url, true, 301 );
            exit(0);
        }
    }

    function getNews($rubrika_url, $page = 1, $ajax = true, $getsimpleblogs = 0, $confirmSubscribe = 0)
    {
        $nowDate = new DateTime('now');
        $query = sql_placeholder("SELECT COUNT(n.id) AS `count` FROM news AS n JOIN blogwriters bw ON n.writers = bw.id
                                    WHERE n.enabled=1 AND bw.enabled = 1 AND n.created <= ?", $nowDate->format('Y-m-d H:i:s'));
        $this->db->query($query);
        $resultCount = $this->db->result();
        $totalPages = ceil($resultCount->count / $this->itemPerPage);

        if ($page > $totalPages) {
            return null;
        }

        $start = ($page - 1) * $this->itemPerPage;
        $result = $this->getListNews($rubrika_url, $start);
        $result = Helper::formatPostList($result, '1_3');
        $page = intval($page + 1);

        $meta = $this->getMetaData($rubrika_url);

        $this->title = $meta['metaTitle'];
        $this->description = $meta['metaDescription'];
        $this->keywords = $meta['metaKeyword'];

        if ($ajax){
            $return = array(
                'items' => $result,
                'meta' => [
                    'title' => $this->title,
                    'description' => $this->description,
                    'keywords' => $this->keywords,
                    'ogTitle' => $this->title,
                    'ogType' => 'article',
                    'ogDescription' => $this->description,
                    'ogImage' => '/og-plus-one.ru.png',
                    'ogSiteName' => $this->settings->site_name,
                    'ogUrl' => '',
                    'twitterCard' => 'summary_large_image',
                    'twitterTitle' => $this->title,
                    'twitterDescription' =>  $this->description,
                    'twitterUrl' => '',
                    'twitterImage' => '/og-plus-one.ru.png',
                    'twitterImageAlt' => $this->title,
                    'twitterSite' => $this->settings->site_name
                ],
                'page' => $page,
            );
            return $return;
        }
        else {

            $header = 'Новости';
            // метатеги

//            if ($getsimpleblogs){
//                $this->title = $this->settings->site_name;
//            }
//            else{
                $this->title = $meta['metaTitle'];
//            }
            $this->description = $meta['metaDescription'];
            $this->keywords = $meta['metaKeyword'];
            $this->smarty->assign('items', $result);
            $this->smarty->assign('header', $header);
            $this->smarty->assign('rubrika', 'Новости');
            $this->smarty->assign('page', $page);
            $this->smarty->assign('subscribe', $confirmSubscribe);

            $headerMenu = $this->getHeaderMenu();

            $this->smarty->assign('headerMenu', $headerMenu['menu']);
            $this->smarty->assign('rubrikaUrl', $headerMenu['currentRubrika']);
            $this->smarty->assign('typeUrl', $headerMenu['currentType']);

            $this->body = $this->smarty->fetch('ajaxBlogs.tpl');
            return $this->body;
        }
    }


    public function getSettingWidgets()
    {
        $this->db->query('SELECT wrap_class, widget_name, widget_code, order_num FROM setting_widgets WHERE enabled = 1 ORDER BY order_num ASC;');
        $widgetList = $this->db->results();

        return $widgetList;
    }
}


