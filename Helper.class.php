<?PHP

/**
 * Simpla CMS
 * StaticPage class: Отображение статической страницы
 *
 * @copyright    2009 Denis Pikusov
 * @link        http://simp.la
 * @author        Denis Pikusov
 *
 * Этот класс использует шаблон static_page.tpl,
 * для вывода статической страницы
 *
 */

require_once('PresetFormat.class.php');

class Helper
{
    const TAG_ECONOMY = 'economy';
    const TAG_ECOLOGY = 'ecology';
    const TAG_COMMUNITY = 'society';
    const TAG_MAIN = 'main';
    const TAG_PLATFORMA = 'platform';

    const TYPE_POST = 1;
    const TYPE_CITATE = 8;
    const TYPE_SPEC_PROJECT = 9;
    const TYPE_MANUAL = 13;
    const TYPE_PARTNER_ANNOUNCE = 14;

    static public $typeVisualBlock = array(
        1 => 'post',
        2 => 'post',
        3 => 'post',
        4 => 'post',
        5 => 'factday',
        6 => 'imageday',
        7 => 'factday',
        8 => 'citate',
        9 => 'post',
        10 => 'diypost',
        12 => 'ticker',
        13 => 'post',
        14 => 'post',
        'news' => 'news'
    );

    static public $tagsMap = array(
        1 => self::TAG_ECONOMY,
        2 => self::TAG_ECOLOGY,
        3 => self::TAG_COMMUNITY,
    );

    static public $db = null;

    /**
     * преобразование даты поста в вид сегодня, 2 дня назад, 5 месяцев назад etc
     *
     * @param string $dateStr
     * @return string
     */
    public static function dateTimeWord($dateStr = '')
    {
        $currentDateTimeStamp = time();

        $dateStr = strtotime($dateStr);

        $datetime1 = date_create(date('d.m.Y H:i:s', $currentDateTimeStamp));
        $datetime2 = date_create(date('d.m.Y H:i:s', $dateStr));

        $interval = date_diff($datetime1, $datetime2);

        // дни
        if ($interval->y == 0 && $interval->m == 0 && ($interval->d >= 0 && $interval->d <= 5)) {
            switch ($interval->d) {
                case 0:
                    $dateToStr = "Сегодня";
                    break;
                case 1:
                    $dateToStr = "Вчера";
                    break;
                case 2:
                case 3:
                case 4:
                    $dateToStr = $interval->d . " дня назад";
                    break;
                case 5:
                    $dateToStr = $interval->d . " дней назад";
                    break;
            }
        }

        // недели
        if ($interval->y == 0 && $interval->m == 0 && ($interval->d > 5 && $interval->d <= 30)) {

            $countWeek = round($interval->d / 7);

            switch ($countWeek) {
                case 1:
                    $dateToStr = "Неделю назад";
                    break;
                case 2:
                case 3:
                    $dateToStr = $countWeek . " недели назад";
                    break;
                case 4:
                    $dateToStr = "Месяц назад";
                    break;
            }
        }

        // месяцы
        if ($interval->y == 0 && $interval->m >= 1) {

            $countMonth = $interval->m % 12;

            switch ($countMonth) {
                case 1:
                    $dateToStr = "Месяц назад";
                    break;
                case 2:
                case 3:
                case 4:
                    $dateToStr = $countMonth . " месяца назад";
                    break;

                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                    $dateToStr = $countMonth . " месяцев назад";
                    break;

            }
        }

        // годы
        if ($interval->y > 0) {

            $countYears = $interval->y;

            switch ($countYears) {
                case 1:
                    $dateToStr = "Год назад";
                    break;
                case 2:
                case 3:
                case 4:
                    $dateToStr = $countYears . " года назад";
                    break;
                default:
                    $dateToStr = $countYears . " лет назад";

            }
        }
        return $dateToStr;
    }


    public static function getHeaderMenuSigns($ptext = '', $ptext1 = '', $ptext2 = '', $num = 0)
    {
        $p1 = $num % 10;
        $p2 = $num % 100;

        if ($p1 == 1 && !($p2 >= 11 && $p2 <= 19)) {
            $ptext = $ptext1;
        }

        if ($p1 >= 2 && $p1 <= 4 && !($p2 >= 11 && $p2 <= 19)) {
            $ptext = $ptext2;
        }

        return $ptext;
    }

    public static function formatPostList($postList, $rowType = '1_3', $host = "")
    {

        foreach ($postList AS $key=>$post){

            $post->typeRow = $rowType;

            if ($rowType == 'ticker'){
                $post->postUrl = $post->url;
                $post->postUrlTarget = $post->url_target;
            }
            elseif (!empty($post->url)){

                if ($post->type_post == "news"){
                    $post->postUrl = "" . $post->postUrl . "/" . $post->url;
                } else {
                    $post->postUrl = "" . $post->tagUrl . "/" . $post->url;
                }
            }
            else{
                if ($post->type_post == "news"){
                    $post->postUrl = "" . $post->postUrl . "/" . $post->url;
                } else{
                    $post->postUrl = null;
                }
            }
            if (!in_array($rowType, ['banner', 'ticker', 'subscribe', 'news'])) {
                $bloggerImage = null;
                if (@$post->bloger && trim(@$post->blogImg)) {
                    $bloggerImage  = $host . "/files/writers/" .  @$post->blogImg;
                }

                $post->iconCode = $post->tagUrl;
                $post->iconBlogger = @$post->bloger;
                $post->iconBloggerName = @$post->writerName;
                $post->iconBloggerImage = $bloggerImage;
                $post->tagUrl = "" . $post->tagUrl;

                if ($post->type_post == "news"){
                    $post->url = $post->postUrl;
                } else if (in_array($post->type_post, array(Helper::TYPE_MANUAL))) {
                        $post->postUrl = "manual/" . $post->url;
                        $post->url = "manual/" . $post->url;
                } else {
                    $post->url = $post->tagUrl . "/" . $post->url;
                }

                $post->dateStr = Helper::dateTimeWord($post->date_created);


                $post->image = null;
                $post->imageMobile = null;
                $post->imageFolder = "/files/blogposts/";
                $post->specProject = false;
                $post->showPartner = true;
                $post->linkParentWindowForPost = false;
                $post->linkParentWindowForTag = false;

                if ($post->type_post == 'news') {
                    $post->imageFolder = "/files/news/";
                }
                /* spec_projects */
                if ($post->type_post == 9) {
                    $post->name = $post->sp_name;
                    $post->header = $post->sp_header;
                    $post->image_1_1 = $post->sp_image_1_1;
                    $post->image_1_2 = $post->sp_image_1_2;
                    $post->image_1_3 = $post->sp_image_1_3;
                    $post->type_announce_1_1 = $post->sp_type_announce_1_1;
                    $post->type_announce_1_2 = $post->sp_type_announce_1_2;
                    $post->type_announce_1_3 = $post->sp_type_announce_1_3;
                    $post->postUrl = $post->sp_url;
                    $post->url = $post->sp_url;
                    $post->imageFolder = "/files/spec_project/";
                    $post->tagUrl = $post->sp_url;
                    $post->iconCode = $post->sp_url;
                    $post->tagName = "СПЕЦПРОЕКТ";
                    $post->specProject = true;
                    if ($_SERVER['HTTP_HOST'] != parse_url($post->sp_url, PHP_URL_HOST)) {
                        $post->linkParentWindowForPost = true;
                        $post->linkParentWindowForTag = true;
                    }
//                    var_dump($post);die();
//                    2020–2050
                } else if (in_array($post->type_post, array(1, 6, 7, 10)) && ($post->spec_project != 0 || $post->spec_project != "0")) {
                    $post->tagName = "СПЕЦПРОЕКТ " . $post->sp_name;
                    $post->showPartner = false;
                    $post->tagUrl = $post->sp_url;
                    $post->specProject = true;

                    /* 6 - Мем недели */
                    if ($_SERVER['HTTP_HOST'] != parse_url($post->sp_url, PHP_URL_HOST) && !in_array($post->type_post, array(6))) {
                        $post->linkParentWindowForPost = false;
                        $post->linkParentWindowForTag = true;
                        /* Карточка DIY */
                        if (in_array($post->type_post, array(10))) {
                            $post->linkParentWindowForPost = false;
                            $post->linkParentWindowForTag = false;
                        }
                    }
                }

                if (in_array($post->type_post, array(10))) {
                    $post->type_announce_1_1 = "picture";
                    $post->type_announce_1_2 = "picture";
                    $post->type_announce_1_3 = "picture";
                }

                if (in_array($post->type_post, array(Helper::TYPE_PARTNER_ANNOUNCE))) {
                    $post->linkParentWindowForPost = $post->url_target === '_blank';
                    $post->linkParentWindowForTag = $post->url_target === '_blank';
                    $post->postUrl = $post->partner_url;
                    $post->url = $post->partner_url;
                }

                if ($post->typeRow == '1_1' && $post->image_1_1 && $post->type_announce_1_1 == "picture") {
                    $post->image = $post->imageFolder . $post->image_1_1;
                }
                if ($post->typeRow == '1_2' && $post->image_1_2 && $post->type_announce_1_2 == "picture") {
                    $post->image = $post->imageFolder . $post->image_1_2;
                }
                if ($post->typeRow == '1_3' && $post->image_1_3 && $post->type_announce_1_3 == "picture") {
                    $post->image = $post->imageFolder . $post->image_1_3;
                }
                $imageExists = $post->image ? file_exists(__DIR__ . $post->image) : false;


                if ($post->typeRow == '1_1' && $post->type_announce_1_1 == "picture") {
                    $post->format = $imageExists ? 'picture' : 'text';
                }

                if ($post->typeRow == '1_2' && $post->type_announce_1_2 == "picture") {
                    $post->format = $imageExists ? 'picture' : 'text';
                }

                if ($post->typeRow == '1_3' && $post->type_announce_1_3 == "picture") {
                    $post->format = $imageExists ? 'picture' : 'text';
                }

                if (in_array($post->type_post, array(10))) {
                    $post->format = 'picture';
                }

                $post->imageMobile = ($post->image_1_3 && file_exists(__DIR__.$post->imageFolder.$post->image_1_3)) ? $post->imageFolder . $post->image_1_3 : null;

                $writersUploadDir = '/files/writers/';
                if (!empty($post->blogImg)) {
                    $post->blogImg = $writersUploadDir . $post->blogImg;
                }

                $writerPagePrefix = '/author/';
                if (!empty($post->writerTag)) {
                    $post->writerPageUrl = $writerPagePrefix . $post->writerTag;
                    $post->iconBloggerUrl = $writerPagePrefix . $post->writerTag;
                } elseif (!empty($post->writerId)) {
                    $post->writerPageUrl = $writerPagePrefix . $post->writerId;
                    $post->iconBloggerUrl = $writerPagePrefix . $post->writerId;
                }


                $post->typePost = Helper::$typeVisualBlock[$post->type_post];

                if ($post->type_post === 'news') {
                    $post->format = !$imageExists ? $post->format : null;
                }
                $postFormat = PresetFormat::getPresetFormatByVisualBlockAndFormat($post->typePost, $post->format);
                $postFont = PresetFormat::getPresetFontByKey($post->font);


                $post->frontClass = isset($postFormat['frontClass']) ? $postFormat['frontClass'] : "bgGray";
                $post->fontClass = $postFont['frontClass'];

                $fontColors = array(
                    'bgcGreen' => '#000',
                    'bgcGray' => '#000',
                    'bgcYellow' => '#000',
                    'bgcPink' => '#000',
                    'bgcBlue' => '#fff',
                    'bgcRed' => '#ffbc00',
                    'bgcDarkBlue' => '#ffbc00',
                );

                $backgroundColors = array(
                    'bgcGray' => '#fff',
                );

                $postFormat['fontColor'] = isset($fontColors[@$postFormat['frontClass']]) ? $fontColors[@$postFormat['frontClass']] : '#fff';
                $postFormat['backgroundColor'] = isset($backgroundColors[@$post->frontClass]) ? $backgroundColors[@$post->frontClass] : @$postFormat['backgroundColor'];

                $post->postFormat = $postFormat;

                switch ($post->typePost) {
                    case 'post':
                    case 'diypost':
                        $name = $post->name;
                        $header = $post->header;
                        break;
                    case 'factday':
                    case 'imageday':
                        $name = $post->name;
                        $header = "";
                        break;
                    case 'citate':
                        $name = $post->name;
                        $header = $post->citate_author_name;
                        break;
                    default:
                        $name = $post->name;
                        $header = $post->header;
                }

                $post->name = $name;
                $post->header = $header;
            }

            $post->iconCode = $post->tagUrl;
            $post->iconName = $post->tagName;

            if ($rowType !== '1_3') {
                $post->name = str_replace('&shy;', '', $post->name);
                $post->header = str_replace('&shy;', '', $post->header);
            }

            $postList[$key] = $post;
        }
//        echo "<pre>";
//        print_r($postList);
//        echo "</pre>";
//        die();
        return $postList;
    }


    static function getTypeVisualBlock($typePost)
    {
        return self::$typeVisualBlock[$typePost];
    }


    static public function getBlockAnnouncements()
    {
        $query = sql_placeholder("
           SELECT bai.* 
             FROM block_announcements ba 
        LEFT JOIN block_announcement_items bai ON ba.id = bai.block_announcement_id 
            WHERE ba.enabled = 1
         ORDER BY ba.created DESC, bai.order_num ASC 
            LIMIT 100
        ");

        Helper::$db->query($query);
        $res = Helper::$db->results();

        $blockAnnouncements = array();
        $contentIDs = array('blog' => array(0), 'news' => array(0));
        foreach ($res as $block) {
            if (!isset($blockAnnouncements[$block->block_announcement_id])) {
                $blockAnnouncements[$block->block_announcement_id] = new stdClass();
                $blockAnnouncements[$block->block_announcement_id]->items = [];
            }
            if (!isset($contentIDs[$block->content_type])) {
                $contentIDs[$block->content_type] = array();
            }
            $contentIDs[$block->content_type][] = $block->content_id;
            $blockAnnouncements[$block->block_announcement_id]->items[] = $block->content_type.'_'.$block->content_id;
        }

        $info = Helper::getInfoByContentIDs($contentIDs);

        foreach ($info as $index => $post) {
            foreach ($blockAnnouncements as $blockAnnouncement) {
                foreach ($blockAnnouncement->items as $id => & $item) {
                    if ($item === $post->type . '_' . $post->id ) {
                        $item = $post;
                        break 2;
                    }
                }
            }
        }
        foreach ($blockAnnouncements as $blockAnnouncement) {
            $blockAnnouncement->items = array_values(array_filter($blockAnnouncement->items, function ($item) {return is_object($item);}));
        }

        return array_values($blockAnnouncements);
    }

    static private function getInfoByContentIDs(array $contentIDs)
    {
        $query = sql_placeholder("(SELECT 'blog' as type, b.id, b.blocks, b.tags, b.name, b.header, b.url, b.url_target, b.is_partner_material,
                        b.type_post, b.partner_url, b.post_tag, b.signature_to_sum, b.text_body, b.video_on_main, b.state, b.type_video,
                        b.amount_to_displaying, bw.name AS writer, bw.id AS writer_id, bw.private, bw.bloger, bw.name_genitive,
                        bw.image AS writerImage, bw.stub_image, bw.stub_link, p.name AS partner,
                        p.color AS partner_color, b.anounce_new_author, b.spec_project, b.conference,
                        b.citate_author_name, b.image_1_1, b.image_1_2, b.image_1_3, b.image_rss,
                        DATE_FORMAT(b.created, '%d.%m.%Y %H:%i:%s') AS date_created,
                        bt.name AS tagName, bt.url AS tagUrl, b.format, b.font, p.name AS partnerName,
                        bw.bloger, bw.name as writerName, bw.announcement_img as blogImg, bw.id as writerId, bw.tag_url AS writerTag,
                        b.type_announce_1_1, b.type_announce_1_2, b.type_announce_1_3,
                        bt.url as iconCode,
                        bt.name as iconName,
                        bt.url as btUrl,
                        NULL AS newsUrl
                            ,sp.name AS sp_name, sp.header AS sp_header, sp.external_url AS sp_url
                            ,sp.image_1_1 AS sp_image_1_1, sp.image_1_2 AS sp_image_1_2, sp.image_1_3 AS sp_image_1_3
                            ,sp.type_announce_1_1 AS sp_type_announce_1_1
                            ,sp.type_announce_1_2 AS sp_type_announce_1_2
                            ,sp.type_announce_1_3 AS sp_type_announce_1_3
                        FROM blogposts AS b
                      LEFT JOIN blogwriters AS bw ON bw.id=b.writers
                      LEFT JOIN partners AS p ON p.id=b.partner
                      LEFT JOIN blogtags AS bt ON bt.id = b.tags
                      LEFT JOIN spec_projects AS sp ON sp.id=b.spec_project
                      -- LEFT JOIN conferences AS conf ON conf.id=b.conference
                      WHERE b.enabled=1 AND b.blocks != 'ticker' AND (b.id IN (?@)) AND (bw.enabled = 1 OR b.spec_project) )
                  UNION ALL
                        (SELECT 'news' as type, n.id, NULL as blocks, n.tags, n.header as name, n.text_content as header, n.url, NULL as url_target, n.is_partner_material,
                        'news' as type_post, NULL as partner_url, NULL as post_tag, NULL as signature_to_sum, NULL as text_body, NULL as video_on_main, NULL as state, NULL as type_video,
                        NULL as amount_to_displaying, bw.name AS writer, bw.id AS writer_id, bw.private, bw.bloger, bw.name_genitive,
                        bw.image AS writerImage, bw.stub_image, bw.stub_link, NULL as partner,
                        NULL AS partner_color, NULL as anounce_new_author, NULL as spec_project, NULL as conference,
                        NULL as citate_author_name, image_1_1, image_1_2, image_1_3, image_rss,
                        DATE_FORMAT(n.created, '%d.%m.%Y') AS date_created,
                        bt.name AS tagName, CONCAT ('news', '/', DATE_FORMAT(n.created, '%Y/%m/%d')) AS tagUrl, 'gray' as format, NULL as font, NULL as partnerName,
                        bw.bloger, bw.name as writerName, bw.announcement_img as blogImg, bw.id as writerId, bw.tag_url AS writerTag,
                        n.type_announce_1_1, n.type_announce_1_2, n.type_announce_1_3,
                        bt.url as iconCode,
                        bt.name as iconName,
                        bt.url as btUrl,
                        CONCAT ('news', '/', DATE_FORMAT(n.created, '%Y/%m/%d/')) AS newsUrl

                            ,NULL AS sp_name, NULL AS sp_header, NULL AS sp_url
                            ,NULL AS sp_image_1_1, NULL AS sp_image_1_2, NULL AS sp_image_1_3
                            ,NULL AS sp_type_announce_1_1
                            ,NULL AS sp_type_announce_1_2
                            ,NULL AS sp_type_announce_1_3
                        FROM news AS n
                      LEFT JOIN blogwriters AS bw ON bw.id=n.writers
                      LEFT JOIN blogtags AS bt ON bt.id = n.tags
                      WHERE n.enabled=1 AND (n.id IN (?@)) AND bw.enabled = 1)
                   ", @$contentIDs['blog'],  @$contentIDs['news']);


        Helper::$db->query($query);
        $searchItems = Helper::$db->results();



        foreach ($searchItems as & $res ) {
            if ($res->type === 'news') {
                $res->iconCode = $res->btUrl;
                $res->tagUrl = $res->btUrl;
                $res->url = $res->newsUrl . $res->url;
            }
        }

        return Helper::formatPostList($searchItems, '1_3');
    }




    public static function getRelatedPostsByParent($id, $type, $nowDate)
    {
        $query = sql_placeholder("SELECT  'post' as type, b.id, rp.id as rp_id, b.created as created, b.name, b.header, bt.url AS tagUrl, b.tags,
                        b.url as url, NULL as postUrl,  b.url_target,
                        b.block_color, b.type_post as type_post, b.is_partner_material,
                        b.image_1_1, b.image_1_2, b.image_1_3, b.image_rss, b.format, b.font,
                        b.spec_project, b.conference, b.body_color,
                        CASE WHEN sp.id THEN sp.font_color ELSE b.font_color END, b.border_color, b.citate_author_name,
                        DATE_FORMAT(b.created, '%d.%m.%Y') AS date_created,
                        bw.id as writerId, bw.name AS writer, bw.tag_url as writerTag, bw.bloger, bw.name as writerName, bw.announcement_img as blogImg,
                        pr.name AS partner_name, pr.color AS partner_color,
                        bt.name AS tagName, pr.name AS partnerName, b.partner_url,
                        b.type_announce_1_1, b.type_announce_1_2, b.type_announce_1_3,
                        sp.name AS specProjectName, sp.external_url AS specProjectUrl, sp.color AS marker_color
                                ,sp.name AS sp_name, sp.header AS sp_header, sp.external_url AS sp_url
                                ,sp.image_1_1 AS sp_image_1_1, sp.image_1_2 AS sp_image_1_2, sp.image_1_3 AS sp_image_1_3
                                ,sp.type_announce_1_1 AS sp_type_announce_1_1
                                ,sp.type_announce_1_2 AS sp_type_announce_1_2
                                ,sp.type_announce_1_3 AS sp_type_announce_1_3

                        FROM blogposts AS b
                          LEFT JOIN partners AS pr ON pr.id=b.partner
                          LEFT JOIN blogtags AS bt ON bt.id = b.tags
                          LEFT JOIN blogwriters AS bw ON bw.id=b.writers
                          INNER JOIN related_posts AS rp ON rp.post_id=b.id
                          LEFT JOIN spec_projects AS sp ON sp.id=b.spec_project
                          WHERE rp.parent_id = ? AND rp.parent_type = ?  AND (COALESCE(rp.post_type, 0) <> 100)  AND b.enabled=1 AND b.created <= ?

                        UNION ALL

                        SELECT 'news' as type, n.id, rp.id as rp_id, n.created as created, n.header as name, n.text_content as header,
                        bt.url as tagUrl, n.tags,
                        n.url as url,
                        CONCAT ('/news/', DATE_FORMAT(n.created, '%Y/%m/%d')) as postUrl,   NULL as url_target,
                            NULL as block_color, 'news' as type_post, n.is_partner_material,
                            n.image_1_1, n.image_1_2, n.image_1_3, n.image_rss, n.format as format, NULL as font,
                            NULL as spec_project, NULL as conference, NULL as body_color, NULL as font_color, NULL as border_color, NULL as citate_author_name,
                            DATE_FORMAT(n.created, '%d.%m.%Y') AS date_created,
                            bw.id as writerId, bw.name AS writer, bw.tag_url as writerTag, bw.bloger, bw.name as writerName, bw.announcement_img as blogImg,
                            NULL as partner_name, NULL AS partner_color,
                            bt.name AS tagName, NULL as partnerName, NULL as partner_url,
                            n.type_announce_1_1,  n.type_announce_1_2,  n.type_announce_1_3,
                            NULL AS specProjectName, NULL AS specProjectUrl, NULL AS specProjectColor
                                ,NULL AS sp_name, NULL AS sp_header, NULL AS sp_url
                                ,NULL AS sp_image_1_1, NULL AS sp_image_1_2, NULL AS sp_image_1_3
                                ,NULL AS sp_type_announce_1_1
                                ,NULL AS sp_type_announce_1_2
                                ,NULL AS sp_type_announce_1_3
                          FROM news AS n
                          LEFT JOIN blogwriters AS bw ON bw.id=n.writers
                          LEFT JOIN blogtags AS bt ON bt.id = n.tags
                          INNER JOIN related_posts AS rp ON rp.post_id=n.id
                          WHERE  rp.parent_id = ? AND rp.parent_type = ?  AND (COALESCE(rp.post_type, 0) = 100) AND n.enabled=1 AND bw.enabled = 1  AND n.created <= ?

                    ORDER BY rp_id DESC
                    LIMIT 3"
            , $id , $type, $nowDate->format('Y-m-d H:i:s'), $id , $type, $nowDate->format('Y-m-d H:i:s'));
        Helper::$db->query($query);
        return Helper::$db->results();
    }
}
