<?PHP

/**
 * Simpla CMS
 * StaticPage class: Отображение статической страницы
 *
 * @copyright 	2009 Denis Pikusov
 * @link 		http://simp.la
 * @author 		Denis Pikusov
 *
 * Этот класс использует шаблон static_page.tpl,
 * для вывода статической страницы
 *
 */

//require_once('Widget.class.php');

class PresetFormat
{

    private static $presets = array(
        'post' => array(
            'colors' => array(
                'gray' => array(
                    'backgroundColor' => '#EAEAEA',
                    'fontColor' => '#000000',
                    'frontClass' => 'bgcGray'
                ),
                'orange' => array(
                    'backgroundColor' => '#FDBE0F',
                    'fontColor' => '#000000',
                    'frontClass' => 'bgcYellow'
                ),
                'blue' => array(
                    'backgroundColor' => '#003BC3',
                    'fontColor' => '#ff1619',
                    'frontClass' => 'bgcBlue'
                ),
                'pink' => array(
                    'backgroundColor' => '#FF5F97',
                    'fontColor' => '#002957',
                    'frontClass' => 'bgcPink'
                ),
                'green' => array(
                    'backgroundColor' => '#00B57D',
                    'fontColor' => '#003BC3',
                    'frontClass' => 'bgcGreen'
                ),
                'red' => array(
                    'backgroundColor' => '#ff1619',
                    'fontColor' => '#FDBE0F',
                    'frontClass' => 'bgcRed'
                ),
                'deepblue' => array(
                    'backgroundColor' => '#002957',
                    'fontColor' => '#FDBE0F',
                    'frontClass' => 'bgcDarkBlue'
                ),
                'picture' => array(
                    'backgroundColor' => null,
                    'fontColor' => '#ffffff',
                    'frontClass' => 'bgcWhite'
                )
            )
        ),
        'specproject' => array(
            'colors' => array(
                'gray' => array(
                    'backgroundColor' => '#EAEAEA',
                    'fontColor' => '#000000',
                    'frontClass' => 'bgcGray'
                ),
                'orange' => array(
                    'backgroundColor' => '#FDBE0F',
                    'fontColor' => '#000000',
                    'frontClass' => 'bgcYellow'
                ),
                'blue' => array(
                    'backgroundColor' => '#003BC3',
                    'fontColor' => '#ff1619',
                    'frontClass' => 'bgcBlue'
                ),
                'pink' => array(
                    'backgroundColor' => '#FF5F97',
                    'fontColor' => '#002957',
                    'frontClass' => 'bgcPink'
                ),
                'green' => array(
                    'backgroundColor' => '#00B57D',
                    'fontColor' => '#003BC3',
                    'frontClass' => 'bgcGreen'
                ),
                'red' => array(
                    'backgroundColor' => '#ff1619',
                    'fontColor' => '#FDBE0F',
                    'frontClass' => 'bgcRed'
                ),
                'deepblue' => array(
                    'backgroundColor' => '#002957',
                    'fontColor' => '#FDBE0F',
                    'frontClass' => 'bgcDarkBlue'
                ),
                'picture' => array(
                    'backgroundColor' => null,
                    'fontColor' => '#ffffff',
                    'frontClass' => 'bgcWhite'
                )
            )
        ),
        'citate' => array(
            'colors' => array(
                'gray' => array(
                    'backgroundColor' => '#EAEAEA',
                    'fontColor' => '#000000',
                    'frontClass' => 'bgcGray'
                ),
                'blue' => array(
                    'backgroundColor' => '#EAEAEA',
                    'fontColor' => '#003fcc',
                    'frontClass' => 'bgGray'
                ),
                'red' => array(
                    'backgroundColor' => '#EAEAEA',
                    'fontColor' => '#ff1619',
                    'frontClass' => 'bgGray'
                ),
                'green' => array(
                    'backgroundColor' => '#EAEAEA',
                    'fontColor' => '#00B57D',
                    'frontClass' => 'bgGray'
                )
            )
        ),
        'diypost' => array(
            'colors' => array(
                'picture' => array(
                    'backgroundColor' => null,
                    'fontColor' => '#ffffff'
                )
            )
        ),
        'imageday' => array(
            'colors' => array(
                'picture' => array(
                    'backgroundColor' => null,
                    'fontColor' => '#ffffff'
                )
            )
        ),
        'factday' => array(
            'colors' => array(
                'gray' => array(
                    'backgroundColor' => '#EAEAEA',
                    'fontColor' => '#000000',
                    'frontClass' => 'bgcGray'
                ),
                'orange' => array(
                    'backgroundColor' => '#FDBE0F',
                    'fontColor' => '#003BC3',
                    'frontClass' => 'bgcYellow'
                ),
                'pink' => array(
                    'backgroundColor' => '#FF5F97',
                    'fontColor' => '#002957',
                    'frontClass' => 'bgcPink'
                ),
                'deepblue' => array(
                    'backgroundColor' => '#002957',
                    'fontColor' => '#00B57D',
                    'frontClass' => 'bgcDarkblue'
                )
            )
        ),
        'ticker' => array(
            'colors' => array(
                'gray' => array(
                    'backgroundColor' => '#EAEAEA',
                    'fontColor' => '#000000',
                    'frontClass' => 'bgcGray'
                ),
                'orange' => array(
                    'backgroundColor' => '#FDBE0F',
                    'fontColor' => '#003BC3',
                    'frontClass' => 'bgcYellow'
                ),
                'pink' => array(
                    'backgroundColor' => '#FF5F97',
                    'fontColor' => '#002957',
                    'frontClass' => 'bgcPink'
                ),
                'deepblue' => array(
                    'backgroundColor' => '#002957',
                    'fontColor' => '#00B57D',
                    'frontClass' => 'bgcDarkblue'
                )
            )
        ),
        'news' => array(
            'colors' => array(
                'gray' => array(
                    'backgroundColor' => '#EAEAEA',
                    'fontColor' => '#000000',
                    'frontClass' => 'bgcGray'
                ),
                'orange' => array(
                    'backgroundColor' => '#FDBE0F',
                    'fontColor' => '#000000',
                    'frontClass' => 'bgcYellow'
                ),
                'blue' => array(
                    'backgroundColor' => '#003BC3',
                    'fontColor' => '#ff1619',
                    'frontClass' => 'bgcBlue'
                ),
                'pink' => array(
                    'backgroundColor' => '#FF5F97',
                    'fontColor' => '#002957',
                    'frontClass' => 'bgcPink'
                ),
                'green' => array(
                    'backgroundColor' => '#00B57D',
                    'fontColor' => '#003BC3',
                    'frontClass' => 'bgcGreen'
                ),
                'red' => array(
                    'backgroundColor' => '#ff1619',
                    'fontColor' => '#FDBE0F',
                    'frontClass' => 'bgcRed'
                ),
                'deepblue' => array(
                    'backgroundColor' => '#002957',
                    'fontColor' => '#FDBE0F',
                    'frontClass' => 'bgcDarkBlue'
                ),
                'picture' => array(
                    'backgroundColor' => null,
                    'fontColor' => '#ffffff',
                    'frontClass' => 'bgcWhite'
                )
            )
        ),
        'manual' => array(
            'colors' => array(
                'gray' => array(
                    'backgroundColor' => '#EAEAEA',
                    'fontColor' => '#000000',
                    'frontClass' => 'bgcGray'
                ),
                'orange' => array(
                    'backgroundColor' => '#FDBE0F',
                    'fontColor' => '#000000',
                    'frontClass' => 'bgcYellow'
                ),
                'blue' => array(
                    'backgroundColor' => '#003BC3',
                    'fontColor' => '#ff1619',
                    'frontClass' => 'bgcBlue'
                ),
                'pink' => array(
                    'backgroundColor' => '#FF5F97',
                    'fontColor' => '#002957',
                    'frontClass' => 'bgcPink'
                ),
                'green' => array(
                    'backgroundColor' => '#00B57D',
                    'fontColor' => '#003BC3',
                    'frontClass' => 'bgcGreen'
                ),
                'red' => array(
                    'backgroundColor' => '#ff1619',
                    'fontColor' => '#FDBE0F',
                    'frontClass' => 'bgcRed'
                ),
                'deepblue' => array(
                    'backgroundColor' => '#002957',
                    'fontColor' => '#FDBE0F',
                    'frontClass' => 'bgcDarkBlue'
                ),
                'picture' => array(
                    'backgroundColor' => null,
                    'fontColor' => '#ffffff',
                    'frontClass' => 'bgcWhite'
                )
            )
        ),
    );

    private static $fonts = array(
        'Krok' => [
            'frontClass' => 'Krok'
        ],
        'CSTM' => [
            'frontClass' => 'cstm'
        ],
        'Geometric' => [
            'frontClass' => 'Geometric'
        ],
        'MullerBold' => [
            'frontClass' => 'MullerBold'
        ],
    );


	/**
	 *
	 * Конструктор
	 *
	 */
//	function PresetFormat(&$parent)
//	{
//		Widget::Widget($parent);
//	}


	public static function getPresetFormats($typeVisualBlock = 'post')
    {
        return self::$presets[$typeVisualBlock]['colors'];
    }

    public static function getPresetFormatByVisualBlockAndFormat($typeVisualBlock = '', $format = '')
    {
        return isset(self::$presets[$typeVisualBlock]['colors'][$format]) ? self::$presets[$typeVisualBlock]['colors'][$format] : [];
    }

    public static function getPresetFontByKey($key)
    {

        return isset(self::$fonts[$key]) ? self::$fonts[$key] : null;
    }

    public static function getPresetFont()
    {
        return self::$fonts;
    }

	/**
	 *
	 * Отображение
	 *
	 */
	function fetch()
	{
		// url страницы
		$section_url = $this->url_filtered_param('section');

		// Выбираем из базы этот раздел
		$query = sql_placeholder("SELECT sections.*
								FROM sections
								WHERE enabled=1 AND sections.url = ? LIMIT 1", $section_url);
		$this->db->query($query);
		$page = $this->db->result();

		if (!$page)
		{
			// return false приводит к отображению ошибки 404
			return false;
		}

		// Метатеги
		$this->title = $page->meta_title;
		$this->keywords = $page->meta_keywords;
		$this->description = $page->meta_description;



		if ($_GET['section']=="forpartners")
		{
			$this->smarty->assign('access', $_SESSION['group']);
			$this->smarty->assign('close_section', "12");
		}
		else
		{
			$this->smarty->assign('access', 0);
			$this->smarty->assign('close_section', 0);
		}


		// Передаем в шаблон
		$this->smarty->assign('page', $page);
		$this->body = $this->smarty->fetch('static_page.tpl');
		return $this->body;
	}
}
