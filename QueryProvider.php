<?php


class QueryProvider
{
    public function getPostsQuery($idsStr)
    {

        $andWhere = "";
        if ($idsStr || $idsStr === '0') {
            $andWhere = " AND b.id IN ($idsStr) ";
        }

        $nowDate = new DateTime();

        $query = sql_placeholder("
                         SELECT 
                         -- Blogposts field
                            b.id,
                            b.blocks_author             AS blocks, 
                            b.tags,
                            b.name,
                            b.header, 
                            b.url,
                            b.type_post, 
                            b.is_partner_material,
                            b.type_post, 
                            b.post_tag, 
                            b.signature_to_sum, 
                            b.text_body, 
                            b.amount_to_displaying, 
                            b.video_on_main, 
                            b.state, 
                            b.type_video,
                            b.partner_url, 
                            b.body_color, 
                            b.anounce_new_author, 
                            b.spec_project, 
                            b.conference,
                            b.image_1_1, 
                            b.image_1_2,
                            b.image_1_3,
                            b.image_rss,
                            b.format, 
                            b.font,                
                            b.type_announce_1_1, 
                            b.type_announce_1_2, 
                            b.type_announce_1_3,
                            b.citate_author_name,
                            
                        -- Partners field
                            p.name                      AS partner,
                            p.color                     AS partner_color, 
                            p.name                      AS partnerName,
                            
                        -- Conference field            
                            conf.name                   AS confName,
                            conf.external_url           AS confUrl,
                            conf.color                  AS confColor,
                            conf.font_color             AS confFontColor,
                            
                        -- Blogtags field
                            bt.name                     AS tagName, 
                            bt.url                      AS tagUrl,
                        
                        -- Spec_project Fields
                            sp.name                     AS sp_name,
                            sp.header                   AS sp_header, 
                            sp.external_url             AS sp_url,
                            sp.image_1_1                AS sp_image_1_1,
                            sp.image_1_2                AS sp_image_1_2,
                            sp.image_1_3                AS sp_image_1_3,
                            sp.type_announce_1_1        AS sp_type_announce_1_1,
                            sp.type_announce_1_2        AS sp_type_announce_1_2,
                            sp.type_announce_1_3        AS sp_type_announce_1_3,
                            sp.name                     AS specProjectName, 
                            sp.external_url             AS specProjectUrl, 
                            sp.color                    AS marker_color,
                            sp.font_color               AS font_color,
                        
                        -- Blogwriters field
                            bw.id                       as writerId, 
                            bw.id                       AS writer_id,
                            bw.bloger, 
                            bw.name                     AS writer, 
                            bw.private,
                            bw.name_genitive,
                            bw.image                    AS writerImage, 
                            bw.stub_image, 
                            bw.stub_link,
                            bw.name                     as writerName, 
                            bw.tag_url                  as writerTag,  
                            bw.announcement_img         as blogImg, 
                            
                        -- Other fields
                            DATE_FORMAT(b.created, '%d.%m.%Y') AS date_created, 
                            'posts'                     as p_type
                            
                           FROM blogposts AS b
                      LEFT JOIN blogwriters AS bw ON bw.id=b.writers
                      LEFT JOIN partners AS p ON p.id=b.partner
                      LEFT JOIN blogtags AS bt ON bt.id = b.tags
                      LEFT JOIN spec_projects AS sp ON sp.id=b.spec_project
                      LEFT JOIN conferences AS conf ON conf.id=b.conference
                      
                          WHERE 
                                b.enabled = 1 
                            {$andWhere}
                            AND (bw.enabled = 1 OR b.spec_project )
                            AND bt.enabled = 1 
                            AND b.created <= ? 
                            AND b.blocks != 'ticker'
                       ORDER BY b.created DESC",
            $nowDate->format('Y-m-d H:i:s'));

        return $query;
    }

    public function getNewsQuery($idsStr)
    {

        $andWhere = "";
        if ($idsStr || $idsStr === '0') {
            $andWhere = " AND n.id IN ($idsStr) ";
        }

        $nowDate = new DateTime();

        $query = sql_placeholder("
                SELECT 
                        'news' as  type_post,
                        'news' as  p_type,
                         n.id,
                         n.tags,
                         n.header as name,
                         n.text_content as header,
                         n.url,
                         n.is_partner_material,
                         n.image_1_1,
                         n.image_1_2,
                         n.image_1_3,
                         n.image_rss,
                         n.type_announce_1_1,
                         n.type_announce_1_2,
                         n.type_announce_1_3,
                         n.meta_title,
                         n.meta_description,
                         n.meta_keywords, 
                         n.text_content as lead,
                         n.format as format,
                         
                     -- Blogtags fields                         
                         bt.name AS tagName,
                         bt.url AS tag_url_original,
                     
                     -- Blogwriters fields
                         bw.name AS writer,
                         bw.id AS writer_id,
                         bw.private,
                         bw.bloger,
                         bw.name_genitive,
                         bw.image AS writerImage,
                         bw.stub_image,
                         bw.stub_link,
                         bw.bloger, 
                         bw.name as writerName,
                         bw.announcement_img as blogImg,
                         bw.id as writerId,
                         bw.tag_url as writerTag,
                         
                     -- Other fields
                         CONCAT ('news', '/', DATE_FORMAT(n.created, '%Y/%m/%d')) AS tagUrl,
                         DATE_FORMAT(n.created, '%d.%m.%Y') AS date_created
                         
                    FROM news AS n
               LEFT JOIN blogwriters AS bw ON bw.id=n.writers
               LEFT JOIN blogtags AS bt ON bt.id = n.tags
                   WHERE
                         n.enabled = 1
                     AND bw.enabled = 1
                     AND n.created <= ?
                     AND bw.useplatforma <> 1
                     {$andWhere}
                ORDER BY n.created DESC
                
                ",
            $nowDate->format('Y-m-d H:i:s')
        );

        return $query;
    }
}
