<?PHP

//error_reporting(E_ALL);
//ini_set("display_errors", 1);


require_once('Widget.class.php');


class Rssfeed extends Widget
{

    private $rssDescriptionDefault = "Все, что нужно знать об устойчивом развитии. Зеленая экономика, ответственное потребление, социальная ответственность. Новости экологии, экономики, науки и общества. Мнения, репортажи, интервью, фотогалереи, блоги.";
    private $resultFilePath = null;
    private $defaultAuthorEmail = "info@plus-one.ru";

    private  $_allowedTags = ['h1', 'h2', 'h3', 'h4', 'strong', 'b', 'i', 'p', 'img', 'sub', 'sup', 'a', 'header',  'figure'];
    private function getAllowedTags() {
        return '<'. join('><', ['h1', 'h2', 'h3', 'h4', 'strong', 'b', 'i', 'p', 'img', 'sub', 'sup', 'a', 'header',  'figure']) . '<>';
    }

    /**
     *
     * Конструктор
     * @param null $parent
     */
    function Rssfeed(&$parent = null)
    {
        Widget::Widget($parent);
    }

    private $authors = false;
    /**
     *
     * Отображение
     *
     */
    function fetch()
    {

        switch ($_GET['mode']) {
            case 'zen':
                $this->zen();
                $this->showFileContent();
                break;
            case 'google':
                $this->google();
                $this->showFileContent();
                break;
            case 'yandex':
                $this->yandex();
                $this->showFileContent();
                break;
            case 'facebook':
                $this->facebook();
                $this->showFileContent();
                break;
            case "flipboard":
                $this->flipboard();
                $this->showFileContent();
                break;
            case "flipboard-news":
                $this->flipboardNews();
                $this->showFileContent();
                break;
            case "pulse":
                $this->pulse();
                $this->showFileContent();
                break;
            case "rambler":
                $this->rambler();
                $this->showFileContent();
                break;
            case 'smi2':
                $this->smi2();
                $this->showFileContent();
                break;
            case 'lentainform':
                $this->lentainform();
                $this->showFileContent();
                break;
            case 'yandex-zen':
                $this->yandex_zen();
                $this->showFileContent();
                break;
            case 'sitemap':
                echo "no content";
                break;
        }



        die();
    }

    function showFileContent()
    {
        header("Content-type: text/xml; charset=UTF-8");
        header("Cache-Control: must-revalidate");
        header("Pragma: no-cache");
        header("Expires: -1");

        print_r(file_get_contents($this->resultFilePath));
    }

    function showFileContentGoogle()
    {
        header("Content-type: application/rss+xml; charset=UTF-8");
        header("Cache-Control: must-revalidate");
        header("Pragma: no-cache");
        header("Expires: -1");

        echo file_get_contents($this->resultFilePath);
    }

    function showFileContentPdf()
    {
        header("Content-type: application/pdf; charset=UTF-8");
        header('Content-Length: ' . filesize($this->resultFilePath));
        header("Cache-Control: must-revalidate");
        header("Pragma: no-cache");
        header("Expires: -1");
        @readfile($this->resultFilePath);
    }

    function textPrepare($text)
    {
        $text = str_replace(["&#151;", "&#146;", "&shy;"], ["—", "'", ""], $text); // для избавления от кода длинных тире
        $text = html_entity_decode(strip_tags($text), ENT_COMPAT | ENT_HTML401, "UTF-8");
        $text = str_replace('&', '&amp;', $text);
        return $text;
    }

    function bodyPrepare($text)
    {
//        $text = str_replace("&#151;", "—", $text); // для избавления от кода длинных тире
//        $text = str_replace("&#146;", "'", $text); // для избавления от кода апострофа
//        $text = str_replace(['«', '»', "„", "“"], '"', $text); // заменяем парные ковычки
        $text = htmlspecialchars_decode(html_entity_decode($text, ENT_COMPAT | ENT_HTML401, "UTF-8"));
        $text = str_replace('&', '&amp;', $text);
        $text = preg_replace('/\"\/files\//', '"'. $this->root_url .'/files/', $text);
        $text = str_replace('href="/', 'href="'. $this->root_url .'/', $text);
        $text = str_replace('strong> <a', 'strong>&#32;<a', $text);
        return $text;
    }

    function zen()
    {
        $filePath = "files/zen.xml";
        $this->root_dir = __DIR__;

        $dom = new domDocument("1.0", "utf-8");
        $root = $dom->createElement("rss");

        $root->setAttribute('xmlns:yandex', "http://news.yandex.ru");
        $root->setAttribute('xmlns:media', "http://search.yahoo.com/mrss/");
        $root->setAttribute('xmlns:turbo', "http://turbo.yandex.ru");
        $root->setAttribute('version', '2.0');


        $dom->appendChild($root);

        $channel = $dom->createElement("channel");

        $title = $dom->createElement("title", "+1 — Проект об устойчивом развитии");
        $channel->appendChild($title);

        $link = $dom->createElement("link", "https://plus-one.ru/");
        $channel->appendChild($link);

        // аналитика Yandex
        $analitics = $dom->createElement("turbo:analytics", " ");
        //
        $analitics->setAttribute('type', "Yandex");
        $analitics->setAttribute('id', "40580670");
        $analitics->setAttribute('params', "{'param':'val'}");
        $channel->appendChild($analitics);

        // Google
        // <turbo:analytics type="Google" id="UA-86584410-1">
        $analitics = $dom->createElement("turbo:analytics", " ");
        $analitics->setAttribute('type', "Google");
        $analitics->setAttribute('id', "UA-86584410-1");
        $channel->appendChild($analitics);
        // MailRu
        $analitics = $dom->createElement("turbo:analytics", " ");
        $analitics->setAttribute('type', "MailRu");
        $analitics->setAttribute('id', "1753981");
        $channel->appendChild($analitics);
        // LiveInternet
        $analitics = $dom->createElement("turbo:analytics", " ");
        $analitics->setAttribute('type', "LiveInternet");
        $analitics->setAttribute('params', "plus-one.ru");
        $channel->appendChild($analitics);
        // Rambler
        $analitics = $dom->createElement("turbo:analytics", " ");
        $analitics->setAttribute('type', "Rambler");
        $analitics->setAttribute('id', "6829220");
        $channel->appendChild($analitics);

        $description = $dom->createElement("description", $this->rssDescriptionDefault);
        $channel->appendChild($description);

        $language = $dom->createElement("language", "ru");
        $channel->appendChild($language);

        $items = $this->getNewsList('zen');

        foreach ($items AS $itemBlog){

            // связанные материалы
            $relatedPostList = $this->getRelatedPostList($itemBlog->id);
            if (!empty($relatedPostList)){
                $yandexRelated = $dom->createElement("yandex:related");
                foreach ($relatedPostList AS $relatedPostItem){

                    $header = $this->textPrepare($relatedPostItem->header);
                    $relatedLink = $dom->createElement("link", $header);

                    $relatedLink->setAttribute('url', $this->root_url . '/' . $relatedPostItem->tag_url . "/" . $relatedPostItem->url);
                    if ($relatedPostItem->image_rss){
                        $relatedLink->setAttribute('img', $this->root_url . "/files/blogposts/" . $relatedPostItem->image_rss);
                    }

                    $yandexRelated->appendChild($relatedLink);

                }
                $channel->appendChild($yandexRelated);
            }

            $item = $dom->createElement("item");
            $item->setAttribute('turbo', "true");
            $title = $this->textPrepare($itemBlog->name);
            $link = $this->makeUrlToItem($itemBlog->url_prefix, $itemBlog->created, $itemBlog->url);
            $category = $itemBlog->tag_name;
            $guid = md5($itemBlog->url);
            $pubDate = date('r', strtotime($itemBlog->created));
            $author = $itemBlog->author;
            $description = $this->textPrepare($itemBlog->lead);

            $itemTitle = $dom->createElement("title", $title);
            $itemTurboTopic = $dom->createElement("turbo:topic", $title);
            $itemLink = $dom->createElement("link", $link);
            $itemTurboSource = $dom->createElement("turbo:source", $link);
            $itemPdalink = $dom->createElement("pdalink", $link);
            $itemAmplink = $dom->createElement("amplink", $link);
            $itemGuid = $dom->createElement("guid", $guid);
            $itemPubDate = $dom->createElement("pubDate", $pubDate);
            $itemAuthor = $dom->createElement("author", $author);
            $itemCategory = $dom->createElement("category", $category);

            if ($itemBlog->image_rss){
                $enclosure = $dom->createElement("enclosure");
                $enclosure->setAttribute('url', $this->root_url . '/files/' . $itemBlog->image_rss);
                $enclosure->setAttribute('type', 'image/jpeg');
                $enclosure->setAttribute('length', @filesize($this->root_dir.'/files/'.$itemBlog->image_rss));
                $item->appendChild($enclosure);
            }

            // тут надо выцепить картинки и вставить их все в тег <enclosure> с указанием типа
            preg_match_all('/<img[^>]*?src=\"(.*)\"/iU', $itemBlog->body, $imagesContent);
            $images = $this->getImages($imagesContent[1]);

            foreach ($images AS $image) {
                $enclosure = $dom->createElement("enclosure");
                $enclosure->setAttribute('url', $this->root_url . $image['url']);
                $enclosure->setAttribute('type', $image['mimeType']);
                $enclosure->setAttribute('length', @filesize($this->root_dir.$image['fileName']));
                $item->appendChild($enclosure);
            }

            $content = '';
            if ($header = $this->textPrepare($itemBlog->name)) {
                $content .= "<h1>" . $this->textPrepare($header) . "</h1>";
            }

            if ($lead = $this->textPrepare($itemBlog->lead)) {
                $content .= "<h2>" . $lead . "</h2>";
            }
            $content .= "<menu><a href='https://plus-one.ru/ecology'>Экология</a><a href='https://plus-one.ru/society'>Общество</a><a href='https://plus-one.ru/economy'>Экономика</a></menu>";
            $content = '<header>' . $content . '</header>';

            $content .= $this->makePictureHtml($itemBlog);

            $body = html_entity_decode($itemBlog->body);
            // Замена твиттерского апострофа
            $body = str_replace(["&#34;" , "&#x27;", '&shy;', '&#151'], ['&rsquo;', '&rsquo;', '', '—'], $body);

            $body = str_replace('&', '&amp;', $body);
            $body = preg_replace('/\"\/files\//', '"'. $this->root_url .'/files/', $body);
            $body = str_replace('href="/', 'href="'. $this->root_url .'/', $body);
            $body = str_replace('strong> <a', 'strong>&#32;<a', $body);


            if ($body) {
                $content .= $body;
            }

            $itemDescription = $dom->createElement("description", $description);


            $cdata = $dom->createCDATASection($content);
            $itemContent = $dom->createElement("turbo:content");
            $itemContent->appendChild($cdata);

            $item->appendChild($itemTitle);
            $item->appendChild($itemTurboTopic);
            $item->appendChild($itemLink);
            $item->appendChild($itemTurboSource);
            $item->appendChild($itemPdalink);
            $item->appendChild($itemAmplink);
            $item->appendChild($itemGuid);
            $item->appendChild($itemPubDate);
            $item->appendChild($itemAuthor);
            $item->appendChild($itemCategory);


            $item->appendChild($itemDescription);
            $item->appendChild($itemContent);

            $channel->appendChild($item);
        }

        $root->appendChild($channel);

        $this->resultFilePath = $filePath;

        if ($dom->save($filePath)){
            $this->resultFilePath = $filePath;
        }
    }

    function lentainform()
    {
        $filePath = "files/lentainform.xml";

        $dom = new domDocument("1.0", "utf-8");

        $root = $dom->createElement("rss");
        $root->setAttribute('xmlns:media', "http://search.yahoo.com/mrss/");
        $root->setAttribute('xmlns:line', "https://www.lentainform.com/");
        $root->setAttribute('version', '2.0');
        $dom->appendChild($root);

        $channel = $dom->createElement("channel");

        $title = $dom->createElement("title", "+1 — Проект об устойчивом развитии");
        $channel->appendChild($title);

        $link = $dom->createElement("link", "https://plus-one.ru/");
        $channel->appendChild($link);

        $description = $dom->createElement("description", $this->rssDescriptionDefault);
        $channel->appendChild($description);

        $language = $dom->createElement("language", "ru");
        $channel->appendChild($language);

        $items = $this->getNewsList('zen');
        foreach ($items AS $itemBlog) {
            $item = $dom->createElement("item");
            $item->setAttribute('line', "true");
            $title = $this->textPrepare($itemBlog->name);
            $link = $this->makeUrlToItem($itemBlog->url_prefix, $itemBlog->created, $itemBlog->url);
            $category = $itemBlog->tag_name;
            $guid = md5($itemBlog->url);
            $pubDate = date('r', strtotime($itemBlog->created));
            $author = $itemBlog->author;
            $description = $this->textPrepare($itemBlog->lead);

            $itemTitle = $dom->createElement("title", $title);
            $itemLineTopic = $dom->createElement("line:topic", $title);
            $itemLink = $dom->createElement("link", $link);
            $itemLineSource = $dom->createElement("line:source", $link);
            $itemPdalink = $dom->createElement("pdalink", $link);
            $itemAmplink = $dom->createElement("amplink", $link);
            $itemGuid = $dom->createElement("guid", $guid);
            $itemPubDate = $dom->createElement("pubDate", $pubDate);
            $itemAuthor = $dom->createElement("author", $author);
            $itemCategory = $dom->createElement("category", $category);

            if ($itemBlog->image_rss) {
                $enclosure = $dom->createElement("enclosure");
                $enclosure->setAttribute('url', $this->root_url . '/files/' . $itemBlog->image_rss);
                $enclosure->setAttribute('type', 'image/jpeg');
                $item->appendChild($enclosure);
            }

            // тут надо выцепить картинки и вставить их все в тег <enclosure> с указанием типа
            preg_match_all('/<img[^>]*?src=\"(.*)\"/iU', $itemBlog->body, $imagesContent);
            $images = $this->getImages($imagesContent[1]);

            foreach ($images AS $image) {
                $enclosure = $dom->createElement("enclosure");
                $enclosure->setAttribute('url', $image['url']);
                $enclosure->setAttribute('type', $image['mimeType']);
                $item->appendChild($enclosure);
            }

            $content = '';
            if ($header = $this->textPrepare($itemBlog->name)) {
                $content .= "<h1>" . $this->textPrepare($header) . "</h1>";
            }

            if ($lead = $this->textPrepare($itemBlog->lead)) {
                $content .= "<h2>" . $lead . "</h2>";
            }
            $content .= "<menu><a href='https://plus-one.ru/ecology'>Экология</a><a href='https://plus-one.ru/society'>Общество</a><a href='https://plus-one.ru/economy'>Экономика</a></menu>";
            $content = '<header>' . $content . '</header>';
            $body = html_entity_decode($itemBlog->body);
            $body = str_replace('&', '&amp;', $body);
            if ($body) {
                $content .= $body;
            }

            $itemDescription = $dom->createElement("description", $description);

            $cdata = $dom->createCDATASection($content);
            $itemContent = $dom->createElement("line:content");
            $itemContent->appendChild($cdata);

            $item->appendChild($itemTitle);
            $item->appendChild($itemLineTopic);
            $item->appendChild($itemLink);
            $item->appendChild($itemLineSource);
            $item->appendChild($itemPdalink);
            $item->appendChild($itemAmplink);
            $item->appendChild($itemGuid);
            $item->appendChild($itemPubDate);
            $item->appendChild($itemAuthor);
            $item->appendChild($itemCategory);


            $item->appendChild($itemDescription);
            $item->appendChild($itemContent);

            $channel->appendChild($item);
        }

        $root->appendChild($channel);

        $this->resultFilePath = $filePath;

        if ($dom->save($filePath)){
            $this->resultFilePath = $filePath;
        }
    }

    function yandex()
    {
        $filePath = "files/yandex.xml";
        $this->root_dir = __DIR__;

        $dom = new domDocument("1.0", "utf-8");
        $root = $dom->createElement("rss");

        $root->setAttribute('xmlns:yandex', "http://news.yandex.ru");
        $root->setAttribute('xmlns:media', "http://search.yahoo.com/mrss/");
        $root->setAttribute('version', '2.0');

        $dom->appendChild($root);

        $channel = $dom->createElement("channel");

        $title = $dom->createElement("title", "+1 — Проект об устойчивом развитии");
        $channel->appendChild($title);

        $link = $dom->createElement("link", "https://plus-one.ru/");
        $channel->appendChild($link);

        $description = $dom->createElement("description", $this->rssDescriptionDefault);
        $channel->appendChild($description);

        $language = $dom->createElement("language", "ru");
        $channel->appendChild($language);

        $items = $this->getNewsList();
        foreach ($items AS $itemBlog){

            $item = $dom->createElement("item");

            $title = strip_tags(htmlspecialchars($itemBlog->header_rss));
            $link = $this->makeUrlToItem($itemBlog->url_prefix, $itemBlog->created, $itemBlog->url);
            $category = $itemBlog->tag_name;
            $guid = md5($itemBlog->url);
            $pubDate = date('r', strtotime($itemBlog->created));
            $author = $itemBlog->author;
            $description = $this->textPrepare($itemBlog->lead);
            $textBody = $this->textPrepare($itemBlog->body);

            $itemTitle = $dom->createElement("title", $title);
            $itemLink = $dom->createElement("link", $link);
            $itemPdalink = $dom->createElement("pdalink", $link);
            $itemAmplink = $dom->createElement("amplink", $link);
            $itemGuid = $dom->createElement("guid", $guid);
            $itemPubDate = $dom->createElement("pubDate", $pubDate);
            $itemAuthor = $dom->createElement("author", $author);
            $itemCategory = $dom->createElement("category", $category);

            if ($itemBlog->image_rss){
                $enclosure = $dom->createElement("enclosure");
                $enclosure->setAttribute('url', $this->root_url . '/files/' . $itemBlog->image_rss);
                $enclosure->setAttribute('type', 'image/jpeg');
                $enclosure->setAttribute('length', @filesize($this->root_dir. '/files/' . $itemBlog->image_rss));
                $item->appendChild($enclosure);
            }

            // тут надо выцепить картинки и вставить их все в тег <enclosure> с указанием типа
            preg_match_all('/<img[^>]*?src=\"(.*)\"/iU', $itemBlog->body, $imagesContent);
            $images = $this->getImages($imagesContent[1]);

            foreach ($images AS $image) {
                $enclosure = $dom->createElement("enclosure");
                $enclosure->setAttribute('url', $this->root_url . $image['url']);
                $enclosure->setAttribute('type', $image['mimeType']);
                $enclosure->setAttribute('length', @filesize($this->root_dir . $image['fileName']));
                $item->appendChild($enclosure);
            }

            $itemDescription = $dom->createElement("description", $description);
//            $itemContent = $dom->createElement("turbo:content", '<![CDATA[' . $content);
            $itemFullText = $dom->createElement("yandex:full-text", $textBody);

            $item->appendChild($itemTitle);
            $item->appendChild($itemLink);
            $item->appendChild($itemPdalink);
            $item->appendChild($itemAmplink);
            $item->appendChild($itemGuid);
            $item->appendChild($itemPubDate);
            $item->appendChild($itemAuthor);
            $item->appendChild($itemCategory);
            $item->appendChild($itemFullText);
            $item->appendChild($itemDescription);

            $channel->appendChild($item);
        }

        $root->appendChild($channel);

        $this->resultFilePath = $filePath;

        if ($dom->save($filePath)){
            $this->resultFilePath = $filePath;
        }
    }

    function smi2()
    {
        $filePath = "files/smi2.xml";

        $dom = new domDocument("1.0", "utf-8");
        $root = $dom->createElement("rss");

        $root->setAttribute('xmlns:media', "http://search.yahoo.com/mrss/");
        $root->setAttribute('version', '2.0');

        $dom->appendChild($root);

        $channel = $dom->createElement("channel");

        $title = $dom->createElement("title", "+1 — Проект об устойчивом развитии");
        $channel->appendChild($title);

        $link = $dom->createElement("link", "https://plus-one.ru/");
        $channel->appendChild($link);

        $description = $dom->createElement("description", $this->rssDescriptionDefault);
        $channel->appendChild($description);

        $language = $dom->createElement("language", "ru");
        $channel->appendChild($language);

        $items = $this->getNewsList();
        foreach ($items AS $itemBlog){
            $item = $dom->createElement("item");

            $title = strip_tags(htmlspecialchars($itemBlog->header_rss));
            $link = $this->makeUrlToItem($itemBlog->url_prefix, $itemBlog->created, $itemBlog->url);
            $category = $itemBlog->tag_name;
            $guid = md5($itemBlog->url);
            $pubDate = date('r', strtotime($itemBlog->created));
            $author = $itemBlog->author;
            $description = $this->textPrepare($itemBlog->lead);
            $textBody = $this->textPrepare($itemBlog->body);

            $itemTitle = $dom->createElement("title", $title);
            $itemLink = $dom->createElement("link", $link);
            $itemPdalink = $dom->createElement("pdalink", $link);
            $itemAmplink = $dom->createElement("amplink", $link);
            $itemGuid = $dom->createElement("guid", $guid);
            $itemPubDate = $dom->createElement("pubDate", $pubDate);
            $itemAuthor = $dom->createElement("author", $author);
            $itemCategory = $dom->createElement("category", $category);

            if ($itemBlog->image_rss){
                $enclosure = $dom->createElement("enclosure");
                $enclosure->setAttribute('url', $this->root_url . '/files/' . $itemBlog->image_rss);
                $enclosure->setAttribute('type', 'image/jpeg');
                $item->appendChild($enclosure);
            }

            // тут надо выцепить картинки и вставить их все в тег <enclosure> с указанием типа
            preg_match_all('/<img[^>]*?src=\"(.*)\"/iU', $itemBlog->body, $imagesContent);
            $images = $this->getImages($imagesContent[1]);

            foreach ($images AS $image) {
                $enclosure = $dom->createElement("enclosure");
                $enclosure->setAttribute('url', $image['url']);
                $enclosure->setAttribute('type', $image['mimeType']);
                $item->appendChild($enclosure);
            }

            $itemDescription = $dom->createElement("description", $description);

            $item->appendChild($itemTitle);
            $item->appendChild($itemLink);
            $item->appendChild($itemPdalink);
            $item->appendChild($itemAmplink);
            $item->appendChild($itemGuid);
            $item->appendChild($itemPubDate);
            $item->appendChild($itemAuthor);
            $item->appendChild($itemCategory);
            $item->appendChild($itemDescription);

            $channel->appendChild($item);
        }

        $root->appendChild($channel);

        $this->resultFilePath = $filePath;

        if ($dom->save($filePath)){
            $this->resultFilePath = $filePath;
        }
    }

    function facebook()
    {
        $filePath = "files/facebook.xml";

        $dom = new domDocument("1.0", "utf-8");
        $root = $dom->createElement("rss");
        $root->setAttribute('version', '2.0');
        $root->setAttribute("xmlns:content", "http://purl.org/rss/1.0/modules/content/");
        $root->setAttribute("xmlns:dc", "http://purl.org/dc/elements/1.1/");
        $root->setAttribute("xmlns:media", "http://search.yahoo.com/mrss/");
        $root->setAttribute("xmlns:atom", "http://www.w3.org/2005/Atom");
        $root->setAttribute("xmlns:georss", "http://www.georss.org/georss");

        $dom->appendChild($root);

        $channel = $dom->createElement("channel");

        $title = $dom->createElement("title", "+1 — Проект об устойчивом развитии");
        $channel->appendChild($title);

        $link = $dom->createElement("link", "https://plus-one.ru/");
        $channel->appendChild($link);

        $description = $dom->createElement("description", $this->rssDescriptionDefault);
        $channel->appendChild($description);

        $language = $dom->createElement("language", "ru");
        $channel->appendChild($language);

        $items = $this->getNewsList('zen');
        foreach ($items AS $itemBlog){

            $item = $dom->createElement("item");

            $title = strip_tags(htmlspecialchars($itemBlog->header_rss));
            $link = $this->makeUrlToItem($itemBlog->url_prefix, $itemBlog->created, $itemBlog->url);
            $category = $itemBlog->tag_name;
            $guid = md5($itemBlog->url);
            $pubDate = date('c', strtotime($itemBlog->created));
            $author = $itemBlog->author;

            $description = $this->textPrepare($itemBlog->lead);
            $content = strip_tags(htmlspecialchars($itemBlog->body));

            $itemTitle = $dom->createElement("title", $title);
            $itemLink = $dom->createElement("link", $link);
            $itemPdalink = $dom->createElement("pdalink", $link);
            $itemAmplink = $dom->createElement("amplink", $link);
            $itemGuid = $dom->createElement("guid", $guid);
            $itemPubDate = $dom->createElement("pubDate", $pubDate);
            $itemAuthor = $dom->createElement("author", $author);
            $itemCategory = $dom->createElement("category", $category);

            if ($itemBlog->image_rss) {
                $enclosure = $dom->createElement("enclosure");
                $enclosure->setAttribute('url', $this->root_url . '/files/' . $itemBlog->image_rss);
                $enclosure->setAttribute('type', 'image/jpeg');
                $item->appendChild($enclosure);
            }

            // тут надо выцепить картинки и вставить их все в тег <enclosure> с указанием типа
            preg_match_all('/<img[^>]*?src=\"(.*)\"/iU', $itemBlog->body, $imagesContent);
            $images = $this->getImages($imagesContent[1]);

            foreach ($images AS $image) {
                $enclosure = $dom->createElement("enclosure");
                $enclosure->setAttribute('url', $image['url']);
                $enclosure->setAttribute('type', $image['mimeType']);
                $item->appendChild($enclosure);
            }

            $itemDescription = $dom->createElement("description", $description);
            $itemContent = $dom->createElement("content:encoded", '<![CDATA[' . $content . ']]>');

            $item->appendChild($itemTitle);
            $item->appendChild($itemLink);
            $item->appendChild($itemPdalink);
            $item->appendChild($itemAmplink);
            $item->appendChild($itemGuid);
            $item->appendChild($itemPubDate);
            $item->appendChild($itemAuthor);
            $item->appendChild($itemCategory);

            $item->appendChild($itemDescription);
            $item->appendChild($itemContent);

            $channel->appendChild($item);
        }

        $root->appendChild($channel);

        $this->resultFilePath = $filePath;

        if ($dom->save($filePath)){
            $this->resultFilePath = $filePath;
        }
    }

    function flipboard()
    {
        $whiteListAttributes = 'abbr, accept, accept-charset, accesskey, action, align, alt, axis, border, cellpadding, cellspacing, char, charoff, charset, checked, cite, class, clear, cols, colspan, color, compact, coords, datetime, dir, disabled, enctype, for, frame, frameborder, allowfullscreen, headers, height, href, hreflang, hspace, id, ismap, label, lang, longdesc, maxlength, media, method, multiple, name, nohref, noshade, nowrap, prompt, readonly, rel, rev, rows, rowspan, rules, scope, selected, shape, size, span, src, srcset, start, summary, tabindex, target, title, type, usemap, valign, value, vspace, and width';
        $whiteListAttributes = array_map(function ($attr){return trim($attr);}, explode(',', $whiteListAttributes));

        $filePath = "files/flipboard.xml";
        $this->root_dir = __DIR__;

        $dom = new domDocument("1.0", "utf-8");
        $root = $dom->createElement("rss");
        $root->setAttribute('version', '2.0');
        $root->setAttribute('xmlns:content', "http://purl.org/rss/1.0/modules/content/");
        $root->setAttribute('xmlns:media', "http://search.yahoo.com/mrss/");
        $root->setAttribute('xmlns:atom', "http://www.w3.org/2005/Atom");
        $root->setAttribute("xmlns:dc", "http://purl.org/dc/elements/1.1/");
        $root->setAttribute("xmlns:georss", "http://www.georss.org/georss");

        $dom->appendChild($root);

        $channel = $dom->createElement("channel");

        $title = $dom->createElement("title", "+1 — Проект об устойчивом развитии");
        $channel->appendChild($title);

        $link = $dom->createElement("link", "https://plus-one.ru/");
        $channel->appendChild($link);

        $atomLink = $dom->createElement("atom:link");
        $atomLink->setAttribute('href', $this->root_url.'/flipboard.xml');
        $atomLink->setAttribute('rel', 'self');
        $atomLink->setAttribute('type', 'application/rss+xml');
        $channel->appendChild($atomLink);

        $description = $dom->createElement("description", $this->rssDescriptionDefault);
        $channel->appendChild($description);

        $language = $dom->createElement("language", "ru");
        $channel->appendChild($language);

        $items = $this->getNewsList();
        foreach ($items AS $itemBlog){

            $item = $dom->createElement("item");

            $title = strip_tags(htmlspecialchars($itemBlog->header_rss));
            $link = $this->makeUrlToItem($itemBlog->url_prefix, $itemBlog->created, $itemBlog->url);
            $category = $itemBlog->tag_name;
            $guid = md5($itemBlog->url);
            $pubDate = date('r', strtotime($itemBlog->created));
            $author = $itemBlog->author;

            $description = $this->textPrepare($itemBlog->lead);

            $itemTitle = $dom->createElement("title", $title);
            $itemLink = $dom->createElement("link", $link);
//            $itemPdalink = $dom->createElement("pdalink", $link);
//            $itemAmplink = $dom->createElement("amplink", $link);
            $itemGuid = $dom->createElement("guid", $guid);
            $itemGuid->setAttribute('isPermaLink', 'false');

            $itemPubDate = $dom->createElement("pubDate", $pubDate);
            $itemAuthor = $dom->createElement("dc:creator", $author);
            $itemCategory = $dom->createElement("category", $category);

            $domChild = new domDocument("1.0", "UTF-8");


            $domChild->loadHTML(mb_convert_encoding( $itemBlog->lead . PHP_EOL . $itemBlog->body, 'HTML-ENTITIES', 'UTF-8'));

            foreach ($domChild->getElementsByTagName('figure') as $childElem) {
                $childElem->parentNode->removeChild($childElem);
            }
            foreach ($domChild->getElementsByTagName('img') as $childElem) {
                $childElem->removeAttribute('ext_url');
                $childElem->removeAttribute('caption');
            }
            foreach ($domChild->getElementsByTagName('*') as $childElem) {
                if (in_array($childElem->tagName, array('script', 'style')) ) {
                    $childElem->parentNode->removeChild($childElem);
                }
                $classes = $childElem->getAttribute('class');
                if ($classes) {
                    $classList = explode(' ', $classes);
                    $removeClassList = array('instagram-media', 'twitter-tweet');
                    foreach ($removeClassList as $remClass) {
                        if (in_array($remClass, $classList)) {
                            $childElem->parentNode->removeChild($childElem);
                        }
                    }
                }

                foreach ($childElem->attributes as $attribute) {
                    if (!in_array($attribute->name, $whiteListAttributes)) {
                        if ($childElem->tagName === 'iframe') {
                            $childElem->removeAttribute('allow');
                            $childElem->removeAttribute('allowfullscreen');
                        } else {
                            $childElem->removeAttribute($attribute->name);
                        }
                    }
                }
            }

            $content = $this->textPrepare($this->bodyPrepare(strip_tags($this->get_inner_html($domChild->documentElement->lastChild))));
            $textBodyNode = $dom->createCDATASection($content);

            $textDescriptionNode = $dom->createCDATASection($content);
            $itemDescription = $dom->createElement("description");
            $itemDescription->appendChild($textDescriptionNode);

//            $itemContent = $dom->createElement("content:encoded");
//            $itemContent->appendChild($textBodyNode);

            $category = $dom->createElement("category", $itemBlog->tag_name);

            $item->appendChild($itemTitle);
            $item->appendChild($itemLink);
//            $item->appendChild($itemPdalink);
//            $item->appendChild($itemAmplink);
            $item->appendChild($itemGuid);
            $item->appendChild($itemPubDate);
            $item->appendChild($itemAuthor);

            $item->appendChild($itemDescription);
//            $item->appendChild($itemContent);

            $imagesExists = false;
            if ($itemBlog->image_rss) {
                $enclosure = $dom->createElement("enclosure");
                $enclosure->setAttribute('url', $this->root_url . '/files/' . $itemBlog->image_rss);
                $enclosure->setAttribute('type', 'image/jpeg');

                $enclosure->setAttribute('length', @filesize($this->root_dir.'/files/'.$itemBlog->image_rss));
                $item->appendChild($enclosure);
                $imagesExists = true;
            }

            if (!$imagesExists) {
                // тут надо выцепить картинки и вставить их все в тег <enclosure> с указанием типа
                preg_match_all('/<img[^>]*?src=\"(.*)\"/iU', $content, $imagesContent);
                $images = $this->getImages($imagesContent[1]);
                foreach ($images AS $image) {
                    if ($imagesExists = true) continue;
                    $enclosure = $dom->createElement("enclosure");
                    $enclosure->setAttribute('url', $this->root_url . $image['url']);
                    $enclosure->setAttribute('type', $image['mimeType']);
                    $enclosure->setAttribute('length', @filesize(str_ireplace($this->root_url, $this->root_dir, $image['fileName'])));
                    $item->appendChild($enclosure);
                    break;
                }
            }
            $item->appendChild($itemCategory);
            $channel->appendChild($item);
        }

        $root->appendChild($channel);

        $this->resultFilePath = $filePath;

        if ($dom->save($filePath)){
            $this->resultFilePath = $filePath;
        }
    }

    function flipboardNews()
    {
        $whiteListAttributes = 'abbr, accept, accept-charset, accesskey, action, align, alt, axis, border, cellpadding, cellspacing, char, charoff, charset, checked, cite, class, clear, cols, colspan, color, compact, coords, datetime, dir, disabled, enctype, for, frame, frameborder, allowfullscreen, headers, height, href, hreflang, hspace, id, ismap, label, lang, longdesc, maxlength, media, method, multiple, name, nohref, noshade, nowrap, prompt, readonly, rel, rev, rows, rowspan, rules, scope, selected, shape, size, span, src, srcset, start, summary, tabindex, target, title, type, usemap, valign, value, vspace, and width';
        $whiteListAttributes = array_map(function ($attr){return trim($attr);}, explode(',', $whiteListAttributes));

        $filePath = "files/flipboard-news.xml";
        $this->root_dir = __DIR__;

        $dom = new domDocument("1.0", "utf-8");
        $root = $dom->createElement("rss");
        $root->setAttribute('version', '2.0');
        $root->setAttribute('xmlns:content', "http://purl.org/rss/1.0/modules/content/");
        $root->setAttribute('xmlns:media', "http://search.yahoo.com/mrss/");
        $root->setAttribute('xmlns:atom', "http://www.w3.org/2005/Atom");
        $root->setAttribute("xmlns:dc", "http://purl.org/dc/elements/1.1/");
        $root->setAttribute("xmlns:georss", "http://www.georss.org/georss");

        $dom->appendChild($root);

        $channel = $dom->createElement("channel");

        $title = $dom->createElement("title", "+1 — Проект об устойчивом развитии");
        $channel->appendChild($title);

        $link = $dom->createElement("link", "https://plus-one.ru/");
        $channel->appendChild($link);

        $atomLink = $dom->createElement("atom:link");
        $atomLink->setAttribute('href', $this->root_url.'/flipboard.xml');
        $atomLink->setAttribute('rel', 'self');
        $atomLink->setAttribute('type', 'application/rss+xml');
        $channel->appendChild($atomLink);

        $description = $dom->createElement("description", $this->rssDescriptionDefault);
        $channel->appendChild($description);

        $language = $dom->createElement("language", "ru");
        $channel->appendChild($language);

        $items = $this->getNewsList();
        foreach ($items AS $itemBlog){

            $item = $dom->createElement("item");

            $title = strip_tags(htmlspecialchars($itemBlog->header_rss));
            $link = $this->makeUrlToItem($itemBlog->url_prefix, $itemBlog->created, $itemBlog->url);
            $category = $itemBlog->tag_name;
            $guid = md5($itemBlog->url);
            $pubDate = date('r', strtotime($itemBlog->created));
            $author = $itemBlog->author;

            $description = $this->textPrepare($itemBlog->lead);

            $itemTitle = $dom->createElement("title", $title);
            $itemLink = $dom->createElement("link", $link);
//            $itemPdalink = $dom->createElement("pdalink", $link);
//            $itemAmplink = $dom->createElement("amplink", $link);
            $itemGuid = $dom->createElement("guid", $guid);
            $itemGuid->setAttribute('isPermaLink', 'false');

            $itemPubDate = $dom->createElement("pubDate", $pubDate);
            $itemAuthor = $dom->createElement("dc:creator", $author);
            $itemCategory = $dom->createElement("category", $category);

            $domChild = new domDocument("1.0", "UTF-8");


            $domChild->loadHTML(mb_convert_encoding( $itemBlog->lead . PHP_EOL . $itemBlog->body, 'HTML-ENTITIES', 'UTF-8'));

            foreach ($domChild->getElementsByTagName('figure') as $childElem) {
                $childElem->parentNode->removeChild($childElem);
            }
            foreach ($domChild->getElementsByTagName('img') as $childElem) {
                $childElem->removeAttribute('ext_url');
                $childElem->removeAttribute('caption');
            }
            foreach ($domChild->getElementsByTagName('*') as $childElem) {
                if (in_array($childElem->tagName, array('script', 'style')) ) {
                    $childElem->parentNode->removeChild($childElem);
                }
                $classes = $childElem->getAttribute('class');
                if ($classes) {
                    $classList = explode(' ', $classes);
                    $removeClassList = array('instagram-media', 'twitter-tweet');
                    foreach ($removeClassList as $remClass) {
                        if (in_array($remClass, $classList)) {
                            $childElem->parentNode->removeChild($childElem);
                        }
                    }
                }

                foreach ($childElem->attributes as $attribute) {
                    if (!in_array($attribute->name, $whiteListAttributes)) {
                        if ($childElem->tagName === 'iframe') {
                            $childElem->removeAttribute('allow');
                            $childElem->removeAttribute('allowfullscreen');
                        } else {
                            $childElem->removeAttribute($attribute->name);
                        }
                    }
                }
            }

            $content = $this->textPrepare($this->bodyPrepare(strip_tags($this->get_inner_html($domChild->documentElement->lastChild))));
            $textBodyNode = $dom->createCDATASection($content);

            $textDescriptionNode = $dom->createCDATASection($content);
            $itemDescription = $dom->createElement("description");
            $itemDescription->appendChild($textDescriptionNode);

//            $itemContent = $dom->createElement("content:encoded");
//            $itemContent->appendChild($textBodyNode);

            $category = $dom->createElement("category", $itemBlog->tag_name);

            $item->appendChild($itemTitle);
            $item->appendChild($itemLink);
//            $item->appendChild($itemPdalink);
//            $item->appendChild($itemAmplink);
            $item->appendChild($itemGuid);
            $item->appendChild($itemPubDate);
            $item->appendChild($itemAuthor);

            $item->appendChild($itemDescription);
//            $item->appendChild($itemContent);

            $imagesExists = false;
            if ($itemBlog->image_rss) {
                $enclosure = $dom->createElement("enclosure");
                $enclosure->setAttribute('url', $this->root_url . '/files/' . $itemBlog->image_rss);
                $enclosure->setAttribute('type', 'image/jpeg');

                $enclosure->setAttribute('length', @filesize($this->root_dir.'/files/'.$itemBlog->image_rss));
                $item->appendChild($enclosure);
                $imagesExists = true;
            }

            if (!$imagesExists) {
                // тут надо выцепить картинки и вставить их все в тег <enclosure> с указанием типа
                preg_match_all('/<img[^>]*?src=\"(.*)\"/iU', $content, $imagesContent);
                $images = $this->getImages($imagesContent[1]);
                foreach ($images AS $image) {
                    if ($imagesExists = true) continue;
                    $enclosure = $dom->createElement("enclosure");
                    $enclosure->setAttribute('url', $this->root_url . $image['url']);
                    $enclosure->setAttribute('type', $image['mimeType']);
                    $enclosure->setAttribute('length', @filesize(str_ireplace($this->root_url, $this->root_dir, $image['fileName'])));
                    $item->appendChild($enclosure);
                    break;
                }
            }
            $item->appendChild($itemCategory);
            $channel->appendChild($item);
        }

        $root->appendChild($channel);

        $this->resultFilePath = $filePath;

        if ($dom->save($filePath)){
            $this->resultFilePath = $filePath;
        }
    }

    function pulse()
    {
        $filePath = "files/pulse.xml";

        $dom = new domDocument("1.0", "utf-8");
        $root = $dom->createElement("rss");
        $root->setAttribute('version', '2.0');
        $root->setAttribute("xmlns:content", "http://purl.org/rss/1.0/modules/content/");

        $dom->appendChild($root);

        $channel = $dom->createElement("channel");

        $title = $dom->createElement("title", "+1 — Проект об устойчивом развитии");
        $channel->appendChild($title);

        $link = $dom->createElement("link", "https://plus-one.ru/");
        $channel->appendChild($link);

        $logoImage = $dom->createElement("image");
        $logoUrl = $dom->createElement("url", $this->root_url . "/files/images/plus-one.ru-logo.png");
        $logoImage->appendChild($logoUrl);
        $channel->appendChild($logoImage);

        $description = $dom->createElement("description", $this->rssDescriptionDefault);
        $channel->appendChild($description);

        $language = $dom->createElement("language", "ru");
        $channel->appendChild($language);

        $items = $this->getNewsList("pulse");
        foreach ($items AS $itemBlog){

            $item = $dom->createElement("item");

            $title = strip_tags(htmlspecialchars($itemBlog->header_rss));
            $link = $this->makeUrlToItem($itemBlog->url_prefix, $itemBlog->created, $itemBlog->url);
            $category = $itemBlog->tag_name;
            $guid = md5($itemBlog->url);
            $pubDate = date('r', strtotime($itemBlog->created));
            $author = $itemBlog->author;

            $description = $this->textPrepare($itemBlog->lead);
            $content = strip_tags(htmlspecialchars($itemBlog->body));

            $itemTitle = $dom->createElement("title", $title);
            $itemLink = $dom->createElement("link", $link);
            $itemAmplink = $dom->createElement("amplink", $link);
            $itemGuid = $dom->createElement("guid", $guid);
            $itemPubDate = $dom->createElement("pubDate", $pubDate);
            $itemAuthor = $dom->createElement("author", $author);
            $itemCategory = $dom->createElement("category", $category);

            if ($itemBlog->image_rss) {
                $enclosure = $dom->createElement("enclosure");
                $enclosure->setAttribute('url', $this->root_url . '/files/' . $itemBlog->image_rss);
                $enclosure->setAttribute('type', 'image/jpeg');
                $item->appendChild($enclosure);
            }
            // тут надо выцепить картинки и вставить их все в тег <enclosure> с указанием типа
            preg_match_all('/<img[^>]*?src=\"(.*)\"/iU', $itemBlog->body, $imagesContent);
            $images = $this->getImages($imagesContent[1]);

            foreach ($images AS $image) {
                $enclosure = $dom->createElement("enclosure");
                $enclosure->setAttribute('url', $image['url']);
                $enclosure->setAttribute('type', $image['mimeType']);
                $item->appendChild($enclosure);
            }

            $itemDescription = $dom->createElement("description", '<![CDATA[' . $description);
            $itemContent = $dom->createElement("content:encoded", $content);

            $item->appendChild($itemTitle);
            $item->appendChild($itemLink);
            $item->appendChild($itemAmplink);
            $item->appendChild($itemGuid);
            $item->appendChild($itemPubDate);
            $item->appendChild($itemAuthor);
            $item->appendChild($itemCategory);

            $item->appendChild($itemDescription);
            $item->appendChild($itemContent);

            $channel->appendChild($item);
        }

        $root->appendChild($channel);

        $this->resultFilePath = $filePath;

        if ($dom->save($filePath)){
            $this->resultFilePath = $filePath;
        }
    }

    function rambler()
    {
        $filePath = "files/rambler.xml";

        $dom = new domDocument("1.0", "utf-8");
        $root = $dom->createElement("rss");
        $root->setAttribute('version', '2.0');
        $root->setAttribute("xmlns:rambler", "http://news.rambler.ru");

        $dom->appendChild($root);

        $channel = $dom->createElement("channel");

        $title = $dom->createElement("title", "+1 — Проект об устойчивом развитии");
        $channel->appendChild($title);

        $link = $dom->createElement("link", "https://plus-one.ru/");
        $channel->appendChild($link);

        $description = $dom->createElement("description", $this->rssDescriptionDefault);
        $channel->appendChild($description);

        $language = $dom->createElement("language", "ru");
        $channel->appendChild($language);

        $items = $this->getNewsList();
        foreach ($items AS $itemBlog){

            $item = $dom->createElement("item");

            $guid = md5($itemBlog->url);
            $title = strip_tags(htmlspecialchars($itemBlog->header_rss));
            $link = $this->makeUrlToItem($itemBlog->url_prefix, $itemBlog->created, $itemBlog->url);
            $pubDate = date('r', strtotime($itemBlog->created));
            $description = $this->textPrepare($itemBlog->lead);
            $category = $itemBlog->tag_name;

            $host = $this->config->protocol . $this->config->host;

            $content = preg_replace("/<a([^>]+?)href=\"(\/.*?)\"([^>]*?)>/", '<a$1href="'.$host.'$2"$3>', $itemBlog->body);
            $content = strip_tags(htmlspecialchars($content));
            $author = $itemBlog->author;

            $itemGuid = $dom->createElement("guid", $guid);
            $itemGuid->setAttribute("isPermaLink", "false");
            $itemTitle = $dom->createElement("title", $title);
            $itemLink = $dom->createElement("link", $link);
            $itemPubDate = $dom->createElement("pubDate", $pubDate);
            $itemDescription = $dom->createElement("description", $description);
            $itemCategory = $dom->createElement("category", $category);
            $itemContent = $dom->createElement("rambler:fulltext", '<![CDATA[' . $content . ']]>');
            $itemAuthor = $dom->createElement("author", $author);

            $item->appendChild($itemTitle);
            $item->appendChild($itemLink);
            $item->appendChild($itemGuid);
            $item->appendChild($itemPubDate);
            $item->appendChild($itemAuthor);
            $item->appendChild($itemCategory);
            $item->appendChild($itemDescription);
            $item->appendChild($itemContent);

            if ($itemBlog->image_rss){
                $enclosure = $dom->createElement("enclosure");
                $enclosure->setAttribute('url', $this->root_url . '/files/' . $itemBlog->image_rss);
                $enclosure->setAttribute('type', 'image/jpeg');
                $item->appendChild($enclosure);
            }

            // тут надо выцепить картинки и вставить их все в тег <enclosure> с указанием типа
            preg_match_all('/<img[^>]*?src=\"(.*)\"/iU', $itemBlog->body, $imagesContent);
            $images = $this->getImages($imagesContent[1]);

            foreach ($images AS $image) {
                $enclosure = $dom->createElement("enclosure");
                $enclosure->setAttribute('url', $image['url']);
                $enclosure->setAttribute('type', $image['mimeType']);
                $item->appendChild($enclosure);
            }

            $channel->appendChild($item);
        }

        $root->appendChild($channel);

        $this->resultFilePath = $filePath;

        if ($dom->save($filePath)){
            $this->resultFilePath = $filePath;
        }
    }


    function legal()
    {
        $filePath = "files/legal.pdf";
        $this->resultFilePath = __DIR__. '/'. $filePath;

    }

    function makeUrlToItem($urlPrefix, $postDate, $url)
    {
        if ($urlPrefix == 'news'){
            $urlTmpLinkArr = explode('-', date('Y-m-d', strtotime($postDate)));
            $newsUrl = implode('/', $urlTmpLinkArr);
            $urlPrefix .= "/" . $newsUrl;
        }

        $link = $this->root_url . "/" . $urlPrefix . "/" . $url;

        return $link;

    }

    function getNewsList($mode = "", $stripTags = true)
    {
        $datePrev = new DateTime('5 days ago');
        $dateNow = date('Y-m-d H:i:s');
        $andWhere = " AND b.created BETWEEN '{$datePrev->format('Y-m-d H:i:s')}' AND '{$dateNow}'";

        if ($mode == "pulse"){
            $andWhereZenPost = " AND b.dzen = 1";
            $andWhereZenNews = " AND b.publish_news_zen_pulse = 1";
        }
        elseif ($mode == "zen"){
            $andWhereZenPost = " AND b.dzen = 1";
            $andWhereZenNews = " AND b.publish_news_zen_pulse = 1";
        }
        else{
            $andWhereZenPost = " AND b.publish_news_aggregators = 1";
            $andWhereZenNews = " AND b.publish_news_aggregators = 1";
        }

        $query = sql_placeholder("select * from
        (
            select 
                b.id, 
                b.header as name, 
                b.url, 
                b.header_social AS header_rss, 
                b.text_content as header, 
                b.feed_lead_html as lead, 
                b.feed_body_html as body, 
                b.created, 
                concat('news', '/', b.image_rss) AS image_rss,          
                t.name AS tag_name,
                t.url AS tag_url, 
                concat('news') AS url_prefix,
                b.author_name AS author,
                'news' AS entity,
                b.image_rss_description as image_rss_description,
                b.image_rss_source as image_rss_source,
                b.author_name AS author_zen
                            FROM news AS b
                            INNER JOIN blogtags AS t on t.id=b.tags
                            LEFT JOIN blogwriters AS w ON w.id=b.writers
                            WHERE b.enabled = 1
                            AND b.url != ''
                            {$andWhere}
                            {$andWhereZenNews}
            UNION
            select b.id, b.name, b.url,
                b.header_rss, 
                b.header as header, 
                b.feed_lead_html as lead, 
                b.feed_body_html as body, 
                b.created, 
                concat('blogposts', '/', b.image_rss) AS image_rss,
                t.name AS tag_name, 
                t.url AS tag_url, 
                concat(t.url) AS url_prefix,
                w.name AS author,
                'posts' AS entity,
                NULL as image_rss_description,
                NULL as image_rss_source,
                b.author_name AS author_zen
                
                            FROM blogposts AS b
                            INNER JOIN blogtags AS t on t.id=b.tags
                            LEFT JOIN blogwriters AS w ON w.id=b.writers
                            WHERE b.enabled = 1
                            AND b.url != ''
                            {$andWhere}
                            {$andWhereZenPost}
        ) AS b
        ORDER BY b.created DESC");
//        echo $query;die();
        $this->db->query($query);
        $items = $this->db->results();

        if ($stripTags) {
            foreach ($items as &$item) {
                $item->body = strip_tags($item->body, $this->getAllowedTags());
                $item->body = preg_replace("/(\sclass=\".+?\"|onclick=\".+?\")/","", $item->body);
                $item->lead = strip_tags($item->lead, $this->getAllowedTags());
                $item->lead = preg_replace("/(\sclass=\".+?\"|onclick=\".+?\")/","", $item->lead);
                $item->header = strip_tags($item->header, $this->getAllowedTags());
                $item->header = preg_replace("/(\sclass=\".+?\"|onclick=\".+?\")/","", $item->header);
            }
        }

        return $items;
    }

    function getRelatedPostList($parentId)
    {
        $query = "SELECT b.id, b.header, b.url, b.image_rss, bt.name AS tag_name, bt.url AS tag_url
                        FROM blogposts AS b
                          LEFT JOIN partners AS pr ON pr.id=b.partner
                          LEFT JOIN blogtags AS bt ON bt.id = b.tags
                          LEFT JOIN blogwriters AS bw ON bw.id=b.writers
                          INNER JOIN related_posts AS rp ON rp.post_id=b.id
                          WHERE rp.parent_id={$parentId} AND b.enabled=1 AND (bw.enabled = 1 OR b.spec_project )
                       ORDER BY b.created 
                          LIMIT 3";
        $this->db->query($query);
        $relatedPosts = $this->db->results();

        return $relatedPosts;
    }


    function getImages($images)
    {
        $return = array();

        if (!empty($images)){
            foreach ($images as $image){
                $imageClear  = preg_replace('/\?.*$/iU', '',  $image);
                $imgFileNameInfo = explode("/", $imageClear);

                $maxArrayIndex = count($imgFileNameInfo) - 1;

                $imgFileNameInfo = explode(".", $imgFileNameInfo[$maxArrayIndex]);

                $mimeType = "";

                switch ($imgFileNameInfo[1]){
                    case 'jpg':
                    case 'jpeg':
                        $mimeType = "image/jpeg";
                        break;
                    case 'gif':
                        $mimeType = "image/gif";
                        break;
                    case 'png':
                        $mimeType = "image/png";
                        break;
                }
                $return[] = array(
                    'url' => $image,
                    'mimeType' => $mimeType,
                    'fileName' => $imageClear
                );
            }
        }
        return $return;
    }


    function get_inner_html( $node ) {
        $innerHTML= '';
        $children = $node->childNodes;
        foreach ($children as $child) {
            $html = $node->ownerDocument->saveHTML($child);
            $innerHTML .= $html;
        }

        return $innerHTML;
    }


    function google()
    {
        $whiteListAttributes = 'abbr, accept, accept-charset, accesskey, action, align, alt, axis, border, cellpadding, cellspacing, char, charoff, charset, checked, cite, class, clear, cols, colspan, color, compact, coords, datetime, dir, disabled, enctype, for, frame, frameborder, allowfullscreen, headers, height, href, hreflang, hspace, id, ismap, label, lang, longdesc, maxlength, media, method, multiple, name, nohref, noshade, nowrap, prompt, readonly, rel, rev, rows, rowspan, rules, scope, selected, shape, size, span, src, srcset, start, summary, tabindex, target, title, type, usemap, valign, value, vspace, and width';
        $whiteListAttributes = array_map(function ($attr){return trim($attr);}, explode(',', $whiteListAttributes));

        $filePath = "files/google-news.xml";
        $this->root_dir = __DIR__;

        $dom = new domDocument("1.0", "utf-8");
        $root = $dom->createElement("rss");

        $root->setAttribute('xmlns:content', "http://purl.org/rss/1.0/modules/content/");
        $root->setAttribute('xmlns:media', "http://search.yahoo.com/mrss/");
        $root->setAttribute('xmlns:atom', "http://www.w3.org/2005/Atom");
        $root->setAttribute('version', '2.0');

        $dom->appendChild($root);

        $channel = $dom->createElement("channel");

        $lastBuildDate = $dom->createElement("lastBuildDate", date('D, d M Y H:i:s +0300'));
        $channel->appendChild($lastBuildDate);

        $atomLink = $dom->createElement("atom:link");
        $atomLink->setAttribute('href', $this->root_url.'/google-news.xml');
        $atomLink->setAttribute('rel', 'self');
        $atomLink->setAttribute('type', 'application/rss+xml');
        $channel->appendChild($atomLink);

        $title = $dom->createElement("title", "+1 — Проект об устойчивом развитии");
        $channel->appendChild($title);

        $link = $dom->createElement("link", "https://plus-one.ru/");
        $channel->appendChild($link);

        $description = $dom->createElement("description", $this->rssDescriptionDefault);
        $channel->appendChild($description);

        $krapivina = new stdClass();
        $krapivina->id = 111;
        $krapivina->name = "Наталья Крапивина";
        $krapivina->login = "n.borisova@plus-one.ru";
        $authors[] = $krapivina;

        $items = $this->getNewsListForGoogle();

        foreach ($items AS $itemBlog){

            $item = $dom->createElement("item");

            $title = strip_tags(htmlspecialchars($itemBlog->header_rss));
            $link = $this->makeUrlToItem($itemBlog->url_prefix, $itemBlog->created, $itemBlog->url);

            $pubDate = date('r', strtotime($itemBlog->created));

            $description = $this->textPrepare($itemBlog->lead);

            $itemGuid = $dom->createElement("guid", $link);

            $itemPubDate = $dom->createElement("pubDate", $pubDate);
            $itemTitle = $dom->createElement("title", $title);
            $itemDescription = $dom->createElement("description", $description);
            $itemLink = $dom->createElement("link", $link);



            $domChild = new domDocument("1.0", "UTF-8");
            $picture = $this->makePictureHtml($itemBlog);

            $domChild->loadHTML(mb_convert_encoding(str_ireplace(['\n', '\r'], "", $itemBlog->lead . $picture . $itemBlog->body), 'HTML-ENTITIES', 'UTF-8'));

            /** @var DOMElement $childElem */
            foreach ($domChild->getElementsByTagName('img') as $childElem) {
                $childElem->removeAttribute('caption');
            }
            while (($r = $domChild->getElementsByTagName("script")) && $r->length) {
                $r->item(0)->parentNode->removeChild($r->item(0));
            }
            $author = false;
            foreach ($domChild->getElementsByTagName('*') as $childElem) {
                if (in_array($childElem->tagName, array('script', 'style')) ) {
                    $childElem->parentNode->removeChild($childElem);
                }
                $classes = $childElem->getAttribute('class');
                if ($classes) {
                    $classList = explode(' ', $classes);
                    $removeClassList = array('instagram-media', 'twitter-tweet');
                    foreach ($removeClassList as $remClass) {
                        if (in_array($remClass, $classList)) {
                            $childElem->parentNode->removeChild($childElem);
                        }
                    }
                    if ($itemBlog->entity === "posts") {
                        $authorClass = "articleAuthor_name";
                        if (in_array($authorClass, $classList)) {
                            $author = $childElem->textContent;
                        }
                    }
                }
                foreach ($childElem->attributes as $attribute) {
                    if (!in_array($attribute->name, $whiteListAttributes)) {
                        if ($childElem->tagName === 'iframe') {
                            $childElem->removeAttribute('allow');
                            $childElem->removeAttribute('allowfullscreen');
                        } else {
                            $childElem->removeAttribute($attribute->name);
                        }
                    }
                }
            }

            if ($author) {
                $itemBlog->author = $author;
            }

            $author = $this->makeAuthorHtml($itemBlog);
            $itemAuthor = $dom->createElement("author", $author);

            $textBody = $this->bodyPrepare($this->get_inner_html($domChild->documentElement->lastChild));
            $textBodyNode = $dom->createCDATASection($textBody);

            $itemFullText = $dom->createElement("content:encoded");
            $itemFullText->appendChild($textBodyNode);

            $item->appendChild($itemGuid);
            $item->appendChild($itemPubDate);
            $item->appendChild($itemTitle);
            $item->appendChild($itemDescription);

            $item->appendChild($itemFullText);
            $item->appendChild($itemLink);
            $item->appendChild($itemAuthor);

            $channel->appendChild($item);
        }

        $root->appendChild($channel);

        $this->resultFilePath = $filePath;

        if ($dom->save($filePath)){
            $this->resultFilePath = $filePath;
        }
    }


    function getNewsListForGoogle()
    {
        $datePrev = new DateTime('30 days ago');
        $dateNow = date('Y-m-d H:i:s');
        $andWhere = " AND b.created BETWEEN '{$datePrev->format('Y-m-d H:i:s')}' AND '{$dateNow}' ";

        $andWhereZenNews = " AND b.publish_news_aggregators = 1 ";
        /* type_material: 1-Новость; 2-Кейс; 3-Интервью; 4-Статья; 5-Колонка; 6-Инфографика */
        $andWhereZenPost = " AND b.publish_news_aggregators = 1 ";

        $query = sql_placeholder("select * from
        (
            select 
                b.id, 
                b.header as name, 
                b.url, 
                b.header_social AS header_rss, 
                b.text_content as header, 
                b.feed_lead_html as lead, 
                b.feed_body_html as body, 
                b.created, 
                concat('news', '/', b.image_rss) AS image_rss,          
                t.name AS tag_name,
                t.url AS tag_url, 
                concat('news') AS url_prefix,
                b.author_name AS author
                
                            FROM news AS b
                            INNER JOIN blogtags AS t on t.id=b.tags
                            LEFT JOIN blogwriters AS w ON w.id=b.writers
                            WHERE b.enabled = 1
                            AND b.url != ''
                            {$andWhere}
                            {$andWhereZenNews}
            UNION
            select b.id, b.name, b.url,
                b.header_rss, 
                b.header as header, 
                b.feed_lead_html as lead, 
                b.feed_body_html as body, 
                b.created, 
                concat('blogposts', '/', b.image_rss) AS image_rss,
                t.name AS tag_name, 
                t.url AS tag_url, 
                concat(t.url) AS url_prefix,
                w.name AS author
                            FROM blogposts AS b
                            INNER JOIN blogtags AS t on t.id=b.tags
                            LEFT JOIN blogwriters AS w ON w.id=b.writers
                            WHERE b.enabled = 1
                            AND b.url != ''
                            {$andWhere}
                            {$andWhereZenPost}
        ) AS b
        ORDER BY b.created DESC
        LIMIT 60
        ");

        $this->db->query($query);

        $items = $this->db->results();
        foreach ($items as &$item) {
            $item->body = strip_tags($item->body, $this->getAllowedTags());
            $item->body = preg_replace("/(\sclass=\".+?\"|onclick=\".+?\")/","", $item->body);
            $item->lead = strip_tags($item->lead, $this->getAllowedTags());
            $item->lead = preg_replace("/(\sclass=\".+?\"|onclick=\".+?\")/","", $item->lead);
            $item->header = strip_tags($item->header, $this->getAllowedTags());
            $item->header = preg_replace("/(\sclass=\".+?\"|onclick=\".+?\")/","", $item->header);
        }

        return $items;
    }

    private function makePictureHtml($itemBlog)
    {
        $picture = "";

        if ($itemBlog->entity === 'posts') {return $picture;}
        if ($itemBlog->image_rss) {
            $src = $this->root_url.'/files/'.$itemBlog->image_rss;
            $description = trim($itemBlog->image_rss_description);
            $source = trim($itemBlog->image_rss_source);
            $picture = <<<PICTURE
<figure><img src="{$src}"/><figcaption>{$description}<span class="copyright">{$source}</span></figcaption></figure>
PICTURE;
        }

        return $picture;
    }


    private function makeAuthorHtml($item)
    {
        $result = "";
        if (!$this->authors) {
            $this->db->query('SELECT id, name, login FROM users_admin WHERE enabled = 1');
            $this->authors = $this->db->results();
        }

        $found = false;
        $author = $item->author;
        if (is_array($this->authors)) {
            foreach ($this->authors as $authorAdmin) {
                if (mb_strtolower($author) === mb_strtolower($authorAdmin->name)) {
                    $result = $authorAdmin->login . " ($author)";
                    $found = true;
                    break;
                }
            }
        }
        if (!$found) {
            $result = $this->defaultAuthorEmail . " ($author)";
        }
        return $result;
    }


    function yandex_zen()
    {
        $filePath = "files/yandex-zen.xml";
        $this->root_dir = __DIR__;

        $dom = new domDocument("1.0", "utf-8");
        $root = $dom->createElement("rss");

        $root->setAttribute('version', '2.0');
        $root->setAttribute('xmlns:content', "http://purl.org/rss/1.0/modules/content/");
        $root->setAttribute('xmlns:dc', "http://purl.org/dc/elements/1.1/");
        $root->setAttribute('xmlns:media', "http://search.yahoo.com/mrss/");
        $root->setAttribute('xmlns:atom', "http://www.w3.org/2005/Atom");
        $root->setAttribute('xmlns:georss', "http://www.georss.org/georss");

        $dom->appendChild($root);
        $channel = $dom->createElement("channel");

        $title = $dom->createElement("title", "+1 — Проект об устойчивом развитии");
        $channel->appendChild($title);

        $link = $dom->createElement("link", "https://plus-one.ru");
        $channel->appendChild($link);


        $description = $dom->createElement("description", $this->rssDescriptionDefault);
        $channel->appendChild($description);

        $language = $dom->createElement("language", "ru");
        $channel->appendChild($language);

        $items = $this->getNewsList('zen');

        foreach ($items AS $itemBlog){

            $item = $dom->createElement("item");
//            $item->setAttribute('turbo', "true");
            $title = $this->textPrepare($itemBlog->name);
            $link = $this->makeUrlToItem($itemBlog->url_prefix, $itemBlog->created, $itemBlog->url);
            $category = $itemBlog->tag_name;
            $guid = md5($itemBlog->url);
            $pubDate = date('r', strtotime($itemBlog->created));
            $author = $itemBlog->author_zen;
            $description = $this->textPrepare($itemBlog->lead);

            $itemTitle = $dom->createElement("title", $title);
            $itemTurboTopic = $dom->createElement("turbo:topic", $title);
            $itemLink = $dom->createElement("link", $link);
            $itemTurboSource = $dom->createElement("turbo:source", $link);
            $itemPdalink = $dom->createElement("pdalink", $link);
            $itemAmplink = $dom->createElement("amplink", $link);
            $itemGuid = $dom->createElement("guid", $guid);
            $itemPubDate = $dom->createElement("pubDate", $pubDate);
            $itemAuthor = $dom->createElement("author", $author);
            $itemCategory = $dom->createElement("category", $category);

            $content = '';
            if ($header = $this->textPrepare($itemBlog->name)) {
                $content .= "<h1>" . $this->textPrepare($header) . "</h1>";
            }

            if ($lead = $this->textPrepare($itemBlog->lead)) {
                $content .= "<h2>" . $lead . "</h2>";
            }
            $content .= "<menu><a href='https://plus-one.ru/ecology'>Экология</a><a href='https://plus-one.ru/society'>Общество</a><a href='https://plus-one.ru/economy'>Экономика</a></menu>";
            $content = '<header>' . $content . '</header>';

            $content .= $this->makePictureHtml($itemBlog);

            $body = html_entity_decode($itemBlog->body);
            // Замена твиттерского апострофа
            $body = str_replace(["&#34;" , "&#x27;", '&shy;', '&#151'], ['&rsquo;', '&rsquo;', '', '—'], $body);

            $body = str_replace('&', '&amp;', $body);
            $body = preg_replace('/\"\/files\//', '"'. $this->root_url .'/files/', $body);
            $body = str_replace('href="/', 'href="'. $this->root_url .'/', $body);
            $body = str_replace('strong> <a', 'strong>&#32;<a', $body);


            if ($body) {
                $content .= $body;
            }

//            $itemDescription = $dom->createElement("description", );

            $cdata = $dom->createCDATASection($description);
            $itemDescription = $dom->createElement("description");
            $itemDescription->appendChild($cdata);

            $cdata = $dom->createCDATASection($content);
            $itemContent = $dom->createElement("content:encoded");
            $itemContent->appendChild($cdata);

            $item->appendChild($itemTitle);
//            $item->appendChild($itemTurboTopic);
            $item->appendChild($itemLink);
//            $item->appendChild($itemTurboSource);
            $item->appendChild($itemPdalink);
            $item->appendChild($itemAmplink);
            $item->appendChild($itemGuid);
            $item->appendChild($itemPubDate);
            $item->appendChild($itemAuthor);
            $item->appendChild($itemCategory);



            if ($itemBlog->image_rss){
                $enclosure = $dom->createElement("enclosure");
                $enclosure->setAttribute('url', $this->root_url . '/files/' . $itemBlog->image_rss);
                $enclosure->setAttribute('type', 'image/jpeg');
//                $enclosure->setAttribute('length', @filesize($this->root_dir.'/files/'.$itemBlog->image_rss));
                $item->appendChild($enclosure);
            }

            // тут надо выцепить картинки и вставить их все в тег <enclosure> с указанием типа
            preg_match_all('/<img[^>]*?src=\"(.*)\"/iU', $itemBlog->body, $imagesContent);
            $images = $this->getImages($imagesContent[1]);

            foreach ($images AS $image) {
                $enclosure = $dom->createElement("enclosure");
                $enclosure->setAttribute('url', $this->root_url . $image['url']);
                $enclosure->setAttribute('type', $image['mimeType']);
//                $enclosure->setAttribute('length', @filesize($this->root_dir.$image['fileName']));
                $item->appendChild($enclosure);
            }


            $item->appendChild($itemDescription);
            $item->appendChild($itemContent);

            $channel->appendChild($item);
        }

        $root->appendChild($channel);

        $this->resultFilePath = $filePath;

        if ($dom->save($filePath)){
            $this->resultFilePath = $filePath;
        }
    }
}


