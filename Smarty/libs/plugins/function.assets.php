<?php

function smarty_function_assets($params, &$smarty)
{
    $resultURL = "";
    $absolute = isset($params['absolute']) ? $params['absolute'] : false;
    if (isset($params['url']) && $params['url']) {
        if ($absolute) {
            $resultURL = $smarty->_tpl_vars['root_url'] . $params['url'];
        } else {
            $resultURL = $params['url'];
        }
    }

    return $resultURL . "?" . $smarty->_tpl_vars['_cache_version'];
}
