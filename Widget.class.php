<?php

use PHPMailer\PHPMailer\PHPMailer;

require_once('Config.class.php');
require_once('Database.class.php');
require_once('placeholder.php');
require_once('detect_browser.php');
require_once('Smarty/libs/Smarty.class.php');

require_once('vendor/phpmailer/phpmailer/src/Exception.php');
require_once('vendor/phpmailer/phpmailer/src/PHPMailer.php');
require_once('vendor/phpmailer/phpmailer/src/SMTP.php');

class Widget
{
    var $parent; // Родительский контейнер
	var $params = array(); // get-параметры
    var $title = null; // meta title
    var $description = null; // meta description
    var $descriptionDefault = "Все, что вы хотели знать об изменениях в экономике, обществе и экосистеме, но боялись спросить."; // meta-description по умолчанию
    var $keywords = null; // metakeywords
    var $body = null; // содержимое блока
	var $ogImageDefault = 'og-plus-one.ru.png'; // Картинка по умолчаниб
	var $ogImage = null;
	var $ogImage_1_1 = null;
	var $ogImage_1_1_c = null;
	var $ogImage_1_2 = null;
	var $ogImage_1_3 = null;
	var $ogImage_galeries = null;
	var $ogSiteName = null;
	var $ogUrl = null;
	var $rubrikaUrl = "";
    var $typeUrl = "";
    var $tagUrl = "";

    var $db; // база данных
	var $smarty; // смарти
	var $config; // конфиг (из конфиг-класса)
    var $settings; // параметры сайта (из таблицы settings)
    var $user; // пользователь, если залогинен
    var $currency; // текущая валюта
    var $gd_loaded = false; // подключена ли графическая библиотека
    var $mobile_user = false;
    var $salt = "coMersPlusOneXoma";
    var $bodyColor = null;

    /**
     *
     * Конструктор
     * @param null $parent
     */
    function Widget(&$parent = null)
    {
    	// если текущий блок входит в некий другой блок
        if (is_object($parent))
        {
        	// стырить у того все параметры
        	$this->parent=$parent;
            $this->db=&$parent->db;
            $this->smarty=&$parent->smarty;
            $this->config=&$parent->config;
            $this->params=&$parent->params;
            $this->settings=&$parent->settings;
            $this->user=&$parent->user;
            $this->root_dir=&$parent->root_dir;
            $this->root_url=&$parent->root_url;
			$this->ogImage=&$parent->ogImage;

            $this->ogImage_1_1 = &$parent->ogImage_1_1;
            $this->ogImage_1_1_c = &$parent->ogImage_1_1_c;
            $this->ogImage_1_2 = &$parent->ogImage_1_2;
            $this->ogImage_1_3 = &$parent->ogImage_1_3;

			$this->ogSiteName=&$parent->ogSiteName;
			$this->ogUrl=&$parent->ogUrl;

            $this->rubrikaUrl = &$parent->rubrikaUrl;
            $this->typeUrl = &$parent->typeUrl;
            $this->tagUrl = &$parent->tagUrl;
            $this->title = &$parent->title;

            $this->currency=&$parent->currency;
            $this->main_currency=&$parent->main_currency;
            $this->currencies=&$parent->currencies;
            $this->gd_loaded=&$parent->gd_loaded;
            $this->bodyColor=&$parent->bodyColor;
        }
        else
        {
            $this->bodyColor=null;
			// Читаем конфиг
			$this->config = new Config();

			// Если установлены magic_quotes, убираем лишние слеши
			if(get_magic_quotes_gpc())
			{
				$_POST = $this->stripslashes_recursive($_POST);
				$_GET = $this->stripslashes_recursive($_GET);
			}

			// Подключаемся к базе данных
			$this->db=new Database($this->config->dbname,$this->config->dbhost,
			$this->config->dbuser,$this->config->dbpass);

			if(!$this->db->connect())
			{
				print "Не могу подключиться к базе данных. Проверьте настройки подключения";
				exit();
			}
			$this->db->query('SET NAMES utf8');

			// Выбираем из базы настройки, которые задаются в разделе Настройки в панели управления
			$query = 'SELECT name, value FROM settings';
			$this->db->query($query);
			$sts = $this->db->results();
			$this->settings = new stdClass();
			foreach($sts as $s)
			{
				$name = $s->name;
				$this->settings->$name = $s->value;
			}

			// Настраиваем смарти
			$this->smarty = new Smarty();
			$this->smarty->compile_check = true;
			$this->smarty->caching = false;
			$this->smarty->cache_lifetime = 0;
			$this->smarty->debugging = false;
			$this->smarty->security = true;
			$this->smarty->secure_dir[] = 'adminv2/templates';
			$this->smarty->compile_dir = 'compiled';

			$this->smarty->template_dir = $this->smarty->secure_dir[] = 'design/'.$this->settings->theme.'/html';

			$this->smarty->config_dir = 'configs';
			$this->smarty->cache_dir = 'cache';

			$this->smarty->assign('settings', $this->settings);

			// Проверка подключения графической библиотеки
			if(extension_loaded('gd'))
				$this->gd_loaded = true;
			$this->smarty->assign('gd_loaded', $this->gd_loaded);

			// Определяем корневую директорию сайта
			$this->root_dir =  str_replace(basename($_SERVER["PHP_SELF"]), '', $_SERVER["PHP_SELF"]);
			$this->smarty->assign('root_dir', $this->root_dir);

			// Корневой url сайта
			$dir = trim(dirname($_SERVER['SCRIPT_NAME']));
			$dir = str_replace("\\", '/', $dir);
			$this->root_url = $this->config->protocol . $_SERVER['HTTP_HOST'];

			if ($this->config->env=='local'){
                $this->root_url .= ":" . $this->config->port;
			}

			if($dir!='/')
			    $this->root_url = $this->root_url.$dir;
			$this->smarty->assign('root_url', $this->root_url);
			$this->smarty->assign('fullRootUrl', 'http://' . $this->root_url);

			// Залогиниваем юзера
			$this->user = null;
			if(isset($_SESSION['user_email']) && isset($_SESSION['user_password']))
			{
				$email = $_SESSION['user_email'];
				$password = $_SESSION['user_password'];
				if(!empty($email) && !empty($password))
				{
					$query = sql_placeholder("SELECT users.*, groups.discount AS discount, groups.name AS group_name FROM users LEFT JOIN groups ON groups.group_id=users.group_id WHERE email=? AND password=? AND enabled=1", $email, $password);
					$this->db->query($query);
					$this->user = $this->db->result();
					$this->smarty->assign('user', $this->user);
				}
			}

			if (@$this->config->timezone) {
                date_default_timezone_set( $this->config->timezone);
            }

			// Курсы валют
			$query = "SELECT currency_id, name, sign, code, rate_from, rate_to, main, def FROM currencies ORDER BY currency_id";
			$this->db->query($query);
			$temp_currencies = $this->db->results();
            $this->main_currency = null;
            $this->default_currency = null;
            $this->currencies = array();
            if (is_array($temp_currencies)) {
                foreach ($temp_currencies as $currency) {
                    $this->currencies[$currency->currency_id] = $currency;
                    if ($currency->main)
                        $this->main_currency = $currency;
                    if ($currency->def)
                        $this->default_currency = $currency;
                }
            }

			if(isset($_POST['currency_id']))
			{
				$_SESSION['currency_id'] = intval($_POST['currency_id']);
			}

			if(isset($_SESSION['currency_id']))
			{
				$this->currency = $this->currencies[intval($_SESSION['currency_id'])];
			}
			else
			{
				$this->currency = $this->default_currency;
			}
            $this->smarty->assign('_cache_version', @$this->config->cache_version ? $this->config->cache_version : 'v=1');

			$this->smarty->assign('currencies', $this->currencies);
			$this->smarty->assign('currency', $this->currency);
			$this->smarty->assign('main_currency', $this->main_currency);

		}
    }

    function setPushSubscriber($token)
    {
        $result = false;

        $query = sql_placeholder("INSERT IGNORE INTO subscribe_push SET token = ?", $token);

        if ($this->db->query($query)){
            $result = true;
        }

        return $result;
    }


    function setSubscribeCheck($email)
    {
        $result = false;

        $hash = md5($email . $this->salt . time());

        $query = sql_placeholder("SELECT email FROM subscribe WHERE email = ? ", $email);
        $this->db->query($query);
        $s = $this->db->result();

        if ($s && $s->email) {
            $query = sql_placeholder("UPDATE subscribe SET check_hash = ? WHERE email = ?", $hash, $s->email);
        } else {
            $query = sql_placeholder("INSERT IGNORE INTO subscribe SET email = ?, check_hash = ?", $email, $hash);
        }

        if ($this->db->query($query)){
            $result = $hash;
            $_COOKIE['sbscr'] = true;
        }

        return $result;
    }

    private function getHost()
    {
        $host = $this->config->protocol . $this->config->host;

        return $host;
    }

    function sendConfirmSubscribeEmail($email, $hash)
    {
        $url = $this->getHost() . "/confirm-subscribe/" . $hash;

        $subject = "Подписка на новости: " . $this->settings->site_name;
        $mess = file_get_contents('email.html');
        $message = str_ireplace('{%link%}', $url, $mess);

        return $this->email($email, $subject, $message);
    }

	/**
	 *
	 * Отображение блока
	 *
	 */
    function fetch()
    {
    	return $this->body="";
    }

	/**
	 *
	 * Рекурсивная уборка магических слешей
	 *
	 */
    function stripslashes_recursive($var)
    {
    	$res = null;
    	if(is_array($var))
    	  foreach($var as $k=>$v)
    	    $res[stripcslashes($k)] = $this->stripslashes_recursive($v);
    	  else
    	    $res = stripcslashes($var);
    	return $res;
    }


    function param($name)
    {
    	if(!empty($name))
      	{
      		if(isset($this->params[$name]))
	  		  return $this->params[$name];
	  		elseif(isset($_GET[$name]))
	  		  return $_GET[$name];
    	}
	    return null;
    }

	/**
	 *
	 * Подмена параметра get
	 *
	 */

    function add_param($name)
    {
    	if(!empty($name) && isset($_GET[$name]))
    	{
			$this->params[$name] = $_GET[$name];
	        return true;
    	}
	    return false;
    }

    function url_filter($val)
    {
    	$val =  preg_replace('/[^A-zА-я0-9_\-\.\%\s]/ui', '', $val);
	    return $val;
    }

    function url_filtered_param($name)
    {
	    return $this->url_filter($this->param($name));
    }

    function form_get($extra_params)
    {
    	$copy=$this->params;
      	foreach($extra_params as $key=>$value)
      	{
	    	if(!is_null($value))
    	  	{
          		$copy[$key]=$value;
	        }
    	}

	    $get='';
    	foreach($copy as $key=>$value)
		{
        	if(strval($value)!="")
	        {
    		    if(empty($get))
            	  $get .= '?';
	        	else
    	          $get .= '&';
    	        $get .= urlencode($key).'='.urlencode($value);
        	}
	    }
      	return $get;
    }


	function sendReportErrorText($text, $userMessage, $url)
	{
        $subject = "Найдена ошибка на сайте " . $this->settings->site_name;
        $message = "Найдена ошибка на странице <b><a href='{$url}'>{$url}</a></b>.<br/>Пользователь посчитал ошибкой фразу: <b>{$text}</b>.";
        if (!empty($userMessage)){
            $message .= "<br/><br/>Пользователь так же оставил комментарий к найденной ошибке:<br/>";
            $message .= "<i>" . nl2br($userMessage) . "</i>";
        }

        return $this->email('plus-one.ru@plus-one.ru', $subject, $message);
	}

    function email($to, $subject, $message)
    {
/*        $msg = "";
        $mail = new PHPMailer();
        $mail->
        $mail->isSMTP();
        $mail->SMTPDebug  = 0;
        $mail->SMTPSecure = 'tls';
        $mail->Host = 'smtp.gmail.com';
        $mail->Port = 587;
        $mail->SMTPAuth = true;
        $mail->Username = "plus.one.typo";
        $mail->Password = "8B%20h^7";
        $mail->CharSet = 'utf-8';
        $mail->setFrom('no-reply@plus-one.ru', 'Платформа +1');
        $mail->addReplyTo('no-reply@plus-one.ru', 'Платформа +1');
        $mail->addAddress($to);
        $mail->Subject = $subject;
        $mail->msgHTML($message);

        if (!$mail->send()) {
            $msg .= "Mailer Error: " . $mail->ErrorInfo;
        } else {
            $msg .= "Message sent!";
        }
        return $msg;*/

        $domain = "mg.plus-one.ru";
        $key = "key-f9c4a2e648108d33d6d692c572a41279";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "api:$key");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_URL,
            "https://api.mailgun.net/v3/{$domain}/messages");
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            array('from' => 'Проект +1 <no-reply@plus-one.ru>',
                'to' => "$to",
                'subject' => $subject,
                'html' => $message
            ));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;

    }

    function mkdir_custom($path) {

        if (file_exists($path)) {
            return true;
        }
        return @mkdir($path, 0775, true);
    }
}
