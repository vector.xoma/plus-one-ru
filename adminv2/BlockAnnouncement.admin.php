<?PHP

//
//error_reporting(E_ALL);
//ini_set("display_errors", 1);

require_once('Widget.admin.php');
require_once('BlogTags.admin.php');
require_once('PostTags.admin.php');
require_once('BlogWriters.admin.php');
require_once ('../PresetFormat.class.php');
require_once('../placeholder.php');
require_once('Traits/TypografCicle.php');

/**
 * Class Plot
 */
class BlockAnnouncement extends Widget
{

    use TypografCicle;

    /** @var   */
    public $errors;
    var $item;
    /** @var string  */
    var $uploaddir = '../files/block_announcement/'; # Папка для хранения картинок (default)
    private $tableName = 'block_announcements';
    private $tableItemsName = 'block_announcement_items';
    public $preview = false;

    function __construct(&$parent){
        Widget::Widget($parent);
        $this->prepare();
    }

    function prepare(){
        $this->errors = new stdClass();
        $itemId = intval($this->param('item_id'));

        if(isset($_POST['name'])){
            $this->check_token();

            $action = $_POST['save'];

            // создаем объект из переданных данных
            $this->setData($_POST, $itemId);
            // проверка на обязательные поля
            $this->checkRequiredFields();

            if (!@$_POST['group_items'] || count($_POST['group_items']) !== 3) {
                $this->errors->group_items = "Необходимо ровно 3 связанных поста";
            }

            if (empty($this->error_msg) && count(get_object_vars($this->errors)) == 0) {

                $this->typografFields($this->item, [], ['name']);

                if(empty($itemId)) {
                    $this->item->modified = date('Y-m-d H:i:s', time());
                    $this->item->created = date('Y-m-d H:i:s', time());

                    $itemId = $this->add_article();

                    if (is_null($itemId)){
                        $this->errors->general = 'Блок анонсов не сохранена. Ошибка при сохранении.';
                    }
                }
                else{
                    $this->item->modified = date('Y-m-d H:i:s', time());

                    if (is_null($this->update_article($itemId))){
                        $this->error_msg = 'Блок анонсов не обновлена. Ошибка при сохранении.';
                    }
                }

                if (@$_POST['delete_large_image'] === "1" && !empty($item_id)) {
                    $this->db->query("UPDATE {$this->tableName} SET image = '' WHERE id={$item_id}");
                }
                if (@$_POST['delete_stub_image'] === "1" && !empty($item_id)) {
                    $this->db->query("UPDATE {$this->tableName} SET stub_image=NULL WHERE id={$item_id}");
                }
                $this->add_fotos($itemId);

                $this->updateBlockAnnouncementItems($itemId);

                if (empty($this->error_msg) && count(get_object_vars($this->errors)) == 0) {

                    if ($action == 'save'){
                        header("Location: /adminv2/block_announcement");
                        exit();
                    } else if ($action == 'apply'){
                        if (!empty($itemId)) {
                            $redirectURL = "/adminv2/block_announcement/edit/{$itemId}/{$_REQUEST['token']}";
                            header("Location: {$redirectURL}" );
                            exit();
                        }
                    } else {
                        $query = sql_placeholder("SELECT * FROM " . $this->tableName . " WHERE id=?", $itemId);
                        $this->db->query($query);
                        $this->item = $this->db->result();

                    }
                }
            }

            header("X-XSS-Protection: 0");

            $groupItemsSorted = @$_POST['group_items'];
            if ($groupItemsSorted) {
                $arrayIds = array('blog'=>array(0), 'news'=>array(0));
                $orderNums = [];
                foreach ($groupItemsSorted as $ind => $item) {
                    $arrayIds[$item['content_type']][] = $item['content_id'];
                    $orderNums[] = $ind;
                }

                $this->item->group_items = array();
                $items = $this->getPostsByArrayIds($arrayIds, $orderNums);
                foreach ($groupItemsSorted as $ind => $item) {
                    if (isset ($items[$item['content_type'].'_'.$item['content_id']])) {
                        $this->item->group_items[] = $items[$item['content_type'] . '_' . $item['content_id']];
                    }
                }
                $this->item->group_items = json_encode($this->item->group_items);
            }

            if (!(empty($this->error_msg) && count(get_object_vars($this->errors)) == 0)) {
                $this->item->date_created = $_POST['created'];
            }
  	    } elseif (!empty($itemId)) {
		    $query = sql_placeholder('SELECT *, DATE_FORMAT(created, \'%d.%m.%Y %H:%i\') AS date_created FROM ' . $this->tableName . ' WHERE id=?', $itemId);
		    $this->db->query($query);
		    $this->item = $this->db->result();
		    $this->item->group_items = $this->getBlockAnnouncementItems($itemId);
  	    }
    }

    function setData($data, $itemId)
    {
        $this->item->id = $itemId;
        $this->item->name = $data['name'];
        $this->item->creator_id = (int)$this->user->id;
        $this->item->created = date('Y-m-d H:i:s', strtotime($data['created']));

        $this->item->enabled = 0;
        if(isset($data['enabled']) && $data['enabled']==1) {
            $this->item->enabled = 1;
        }
    }

    function checkRequiredFields(){
        $validations = array(
            'name' => array(
                'maxLength' => array(
                    'length' => 255,
                    'error' => 'Превышено максимальное количество символов в поле \'Имя\' (255 символов)')
            ),
        );

        $this->errors = new stdClass();

        foreach ($validations as $field => $rules) {
            foreach ($rules as $rule => $options) {
                if ($rule === "maxLength") {
                    if (@iconv_strlen($this->item->{$field}, "UTF-8") > $options['length']) {
                        $this->errors->{$field} = $options['error'];
                    }
                }
            }
        }

    }


	function fetch()
	{
		if(empty($this->item->id)){
			$this->title = 'Новый Блок Анонсов';
            if (!@$_POST) {
                $this->item = new stdClass();
                $this->item->date_created = date('d.m.Y H:i');
                if (!property_exists($this->item, 'group_items')) {
                    $this->item->group_items = json_encode(array());
                }
            }
		} else {
			$this->title = 'Изменение Блока Анонса: <em>"' . trim(strip_tags(html_entity_decode($this->item->name))) . '"</em>';
		}
        $this->item->form_id = !empty($this->item->form_id) ? $this->item->form_id : uniqid();


        $userLogin = $_SESSION['adminarea']['login'];
        if (!empty($userLogin)){
            $query = sql_placeholder("SELECT name FROM users_admin WHERE login=?", $userLogin);
            $this->db->query($query);
            $userInfo = $this->db->result();
            $this->smarty->assign('currentUserName', $userInfo->name);
        }

        $this->smarty->assign('title', $this->title);
        $this->smarty->assign('Item', $this->item);
		$this->smarty->assign('Errors', $this->errors);
		$this->smarty->assign('hasErrors', count(get_object_vars($this->errors)) !== 0);
		$this->smarty->assign('Lang', $this->lang);
        $this->smarty->assign('preview', $this->preview);
        $this->smarty->assign('images_uploaddir', $this->uploaddir);

		$this->body = $this->smarty->fetch('block_announcement/_edit.tpl');
	}

    /**
     * @return int|null
     */
    function add_article(){
        $data = (array)$this->item;
        unset($data['form_id']);
        unset($data['group_items']);
        $query = sql_placeholder('INSERT INTO ' . $this->tableName . ' SET ?%', $data);

        if ($this->db->query($query)){
            $item_id = $this->db->insert_id();

            $query = sql_placeholder('UPDATE ' . $this->tableName . ' SET order_num=id WHERE id=?', $item_id);
            $this->db->query($query);

            return $item_id;
        }
        else{
            $this->errors->errors = join('<br/>',  $this->db->errors );
            return null;
        }
    }

    /**
     * @param $item_id
     * @return null
     */
    function update_article($item_id){
        $data = (array)$this->item;
        unset($data['form_id']);
        unset($data['group_items']);
        $query = sql_placeholder('UPDATE ' . $this->tableName . ' SET ?% WHERE id=?', $data, $item_id);

        if ($this->db->query($query)){
            return $item_id;
        }
        else{
            $this->errors->errors = join('<br/>',  $this->db->errors );
            return null;
        }
    }


    public function updateBlockAnnouncementItems($blockAnnouncementId){
        $groupItems = @$_POST['group_items'];
        $existsIds = [];
        if ($blockAnnouncementId) {
            $this->db->query(sql_placeholder("SELECT id FROM {$this->tableItemsName} WHERE block_announcement_id = ?", $blockAnnouncementId));
            $result1 = $this->db->results();
            $existsIds = array_map(function ($e){return (int)$e->id;}, $result1);
        }
        $queries = [];
        $groupItemsIDs =  array_map(function ($g){ return (int)$g['id'];}, $groupItems);

        if ($groupItems) {
            $now = new DateTime('now');
            foreach ($groupItems as $ind => $gi) {
                if (in_array((int)$gi['id'], $existsIds)) {
                    $q = sql_placeholder("REPLACE INTO {$this->tableItemsName} (id, content_id, content_type, created, modified, order_num, block_announcement_id) VALUES (?,?,?,?,?,?,?)",
                        $gi['id'], $gi['content_id'], $gi['content_type'], $now->format('Y-m-d H:i:s'), $now->format('Y-m-d H:i:s'), $ind, $blockAnnouncementId);
                    $queries[] = $q;
                } else {
                    $q = sql_placeholder("INSERT INTO {$this->tableItemsName} ( content_id, content_type, created, modified, order_num, block_announcement_id) VALUES (?,?,?,?,?,?)",
                        $gi['content_id'], $gi['content_type'], $now->format('Y-m-d H:i:s'), $now->format('Y-m-d H:i:s'), $ind, $blockAnnouncementId);
                    $queries[] = $q;
                }
                $this->db->query($q);
            }

        }
        foreach ($existsIds as $eId) {
            if (!in_array($eId, $groupItemsIDs)) {
                $this->db->query(sql_placeholder("DELETE FROM {$this->tableItemsName} WHERE block_announcement_id = ? AND id = ?", $blockAnnouncementId, $eId));
            }
        }
    }

    public function getBlockAnnouncementItems($blockAnnouncementId) {
        $return = [];
        if ($blockAnnouncementId) {
            $query = sql_placeholder("SELECT id, content_id, content_type, order_num FROM {$this->tableItemsName} WHERE block_announcement_id=? ORDER BY order_num ASC", $blockAnnouncementId);
            $this->db->query($query);
            $groupItemsSorted = $this->db->results();

            $arrayIds = array('blog'=>array(0), 'news'=>array(0));
            $orderNums = [];
            foreach ($groupItemsSorted as $item) {
                $arrayIds[$item->content_type][] = $item->content_id;
                $orderNums[$item->content_type.'_'.$item->content_id] = $item->order_num;
            }

            $items = $this->getPostsByArrayIds($arrayIds, $orderNums);
            foreach ($groupItemsSorted as $ind => $item) {
                if (isset ($items[$item->content_type.'_'.$item->content_id])) {
                    $return[] = $items[$item->content_type.'_'.$item->content_id];
                }
            }
        }
        return array('ok' => true, 'items' => $return);
    }

    public function getPostsByArrayIds(array $arrayIds, $orderNums)
    {
        $postsIdsStr = join(',', $arrayIds['blog']);
        $newsIdsStr = join(',', $arrayIds['news']);
        $query = sql_placeholder("(SELECT  'blog' as type, b.id, b.blocks, b.tags,
                            CASE WHEN b.type_post = 9 THEN sp.name ELSE b.name END as name,
                            CASE WHEN b.type_post = 9 THEN sp.header ELSE b.header END as subtitle,
                            CASE WHEN b.type_post = 9 THEN sp.image_1_1 ELSE b.image_1_1 END as image_1_1,
                            CASE WHEN b.type_post = 9 THEN sp.image_1_2 ELSE b.image_1_2 END as image_1_2,
                            CASE WHEN b.type_post = 9 THEN sp.image_1_3 ELSE b.image_1_3 END as image_1_3,
                            CONCAT ('/', bt.url) AS blogTagUrl, b.url, b.type_post,
                            b.type_post, b.partner_url, b.post_tag, bw.name AS writer, bw.id AS writer_id, bw.private, bw.bloger, bw.name_genitive,
                            bw.image AS writerImage, bw.stub_image, bw.stub_link, p.name AS partner,
                            p.color AS partner_color, b.anounce_new_author, b.spec_project, b.conference,
                            b.citate_author_name,
                            b.image_rss,
                            DATE_FORMAT(b.created, '%d.%m.%Y %H:%i:%s') AS date_created,
                            bt.name AS tagName, b.format, b.font, p.name AS partnerName,
                            bw.enabled as writer_enabled
                            FROM blogposts AS b
                          LEFT JOIN blogwriters AS bw ON bw.id=b.writers
                          LEFT JOIN partners AS p ON p.id=b.partner
                          LEFT JOIN blogtags AS bt ON bt.id = b.tags
                          LEFT JOIN spec_projects AS sp ON sp.id = b.spec_project
                          WHERE  b.blocks != 'ticker' AND (b.id IN ({$postsIdsStr}))  AND b.type_post <> 12)
                      UNION ALL
                            (SELECT 'news' as type, n.id, NULL as blocks, n.tags, n.header as name,  n.text_content as subtitle, CONCAT ('/news/', DATE_FORMAT(n.created, '%Y/%m/%d')) as blogTagUrl, n.url, NULL as type_post,
                            NULL as type_post, NULL as partner_url, NULL as post_tag, bw.name AS writer, bw.id AS writer_id, bw.private, bw.bloger, bw.name_genitive,
                            bw.image AS writerImage, bw.stub_image, bw.stub_link, NULL as partner,
                            NULL AS partner_color, NULL as anounce_new_author, NULL as spec_project, NULL as conference,
                            NULL as citate_author_name, NULL as image_1_1, NULL as image_1_2, n.image_1_3, n.image_rss,
                            DATE_FORMAT(n.created, '%d.%m.%Y') AS date_created,
                            bt.name AS tagName, NULL as format, NULL as font, NULL as partnerName,
                            bw.enabled as writer_enabled
                            FROM news AS n
                          LEFT JOIN blogwriters AS bw ON bw.id=n.writers
                          LEFT JOIN blogtags AS bt ON bt.id = n.tags
                          WHERE n.enabled=1 AND (n.id IN ({$newsIdsStr})))");

        $this->db->query($query);
        $groupItems = $this->db->results();

        $dirPrefix = @$this->config->fileStorage['localRootFilePath'] ? @$this->config->fileStorage['localRootFilePath']  :  '../files/';

        foreach ($groupItems AS $item){
            $imageUrl = '';
            $itemUrl = $item->blogTagUrl . '/' . $item->url;
            if ($item->type == 'blog') {
                if (preg_match('#^(http|https)://#is', $item->url) === 1) {
                    $itemUrl = $item->url;
                }
                if ($item->spec_project && $item->type_post == 9) {
                    $imageUrl .= 'spec_project';
                } else {
                    $imageUrl .= 'blogposts';
                }
            } elseif ($item->type == 'news') {
                $imageUrl .= 'news';
            }

            $imagePath = false;

            if (trim($item->image_rss)) {
                $imagePath = $imageUrl . '/' . $item->image_rss;
                $imagePath = file_exists($dirPrefix.$imagePath) ? $imagePath : false;
            } else if (trim($item->image_1_3)) {
                $imagePath = $imageUrl . '/' . $item->image_1_3;
                $imagePath = file_exists($dirPrefix.$imagePath) ? $imagePath : false;
            } else if (trim($item->image_1_1)) {
                $imagePath = $imageUrl . '/' . $item->image_1_1;
                $imagePath = file_exists($dirPrefix.$imagePath) ? $imagePath : false;
            } else if (trim($item->image_1_2)) {
                $imagePath = $imageUrl . '/' . $item->image_1_2;
                $imagePath = file_exists($dirPrefix.$imagePath) ? $imagePath : false;
            }

            $return[$item->type . '_' . $item->id] = array(
                'value' => $item->type.'_'.$item->id,
                'name' => htmlspecialchars_decode(html_entity_decode(strip_tags($item->name))),
                'subtitle' => htmlspecialchars_decode(html_entity_decode(strip_tags($item->subtitle))),
                'url' => $itemUrl,
                'id' => '',
                'order_num' => $orderNums[$item->type.'_'.$item->id],
                'content_id' => $item->id,
                'content_type' => $item->type,
                'type_post' => $item->type_post,
                'image' =>  $imagePath ? '/files/'.$imagePath : false,
                'imagePath' => $imagePath
            );
        }

        uasort($return, function ($a, $b) {
            if ($a['order_num'] == $b['order_num']) {
                return 0;
            }
            if ($a['order_num'] > $b['order_num']) {
                return 1;
            } else {
                return -1;
            }
        });

        return $return;
    }

    /**
     * метод добавлдяет фото к текущей статье
     * @param $article_id
     * @return bool
     */
    function add_fotos($itemId){
        $result = false;

        $dateFolder = '' . (new DateTime('now'))->format('Y/m') . '/';

        $largeuploadfile = $dateFolder. $itemId.".jpg";
        $stubImage = $dateFolder. $itemId . '_stub_image.jpg';

        $this->mkdir_custom($this->uploaddir . $dateFolder);

        if(isset($_FILES['large_image']) && !empty($_FILES['large_image']['tmp_name'])){
            if (!move_uploaded_file($_FILES['large_image']['tmp_name'], $this->uploaddir.$largeuploadfile)){
                $this->error_msg = 'Ошибка при загрузке файла';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET image='$largeuploadfile' WHERE id={$itemId}");
                $result = true;
                $forOptimized[] = array(realpath($this->uploaddir.$largeuploadfile), $largeuploadfile);
            }
        }

        if(isset($_FILES['stub_image']) && !empty($_FILES['stub_image']['tmp_name'])){

            if (!move_uploaded_file($_FILES['stub_image']['tmp_name'], $this->uploaddir.$stubImage)){
                $this->error_msg = 'Ошибка при загрузке файла';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET stub_image='$stubImage' WHERE id={$itemId}");
                $result = true;
            }
        }

        return $result;
    }

}
