<?php

require_once('Widget.admin.php');
require_once('../placeholder.php');
require_once('BlogPosts.admin.php');
require_once('NewsItems.admin.php');

/**
 * Class Plots
 */
class BlockAnnouncements extends Widget
{
    private $tableName = 'block_announcements';
    private $itemTableName = 'block_announcement_items';
    private $section = 'block_announcements';
    private $items_per_page = 50;

    /**
     * @param $parent
     */
    function __construct(&$parent = null)
    {
        parent::Widget($parent);
        $this->prepare();
    }

    /**
     *
     */
    function prepare()
    {
        if (isset($_GET['act']) && $_GET['act'] == 'delete' && (isset($_POST['items']) || isset($_GET['item_id']))) {
            $this->check_token();

            $this->deleteItem($_GET['item_id'], $this->tableName);
            $get = $this->form_get(array());

            header("Location: index.php$get");
        }

        if (isset($_GET['set_enabled'])) {
            $this->check_token();

            $this->setEnable($_GET['set_enabled'], $this->tableName);

            $get = $this->form_get(array());

            header("Location: index.php$get");
        }
    }

    /**
     *
     */
    function fetch()
    {
        $this->title = 'Блоки анонсов';
        $link = "index.php?section=BlockAnnouncements";

        $where = "";
        $writerId = intval($this->param('writer_id'));
        if(!empty($writerId)){
            $where = ' AND writers=' . $writerId;
        }

        if ($search = $this->param('search')){

            $where .= " AND (g.name LIKE '%$search%')";
            $link .= "&search=" . $search;
        }

        $totalRecordCount = $this->getTotalRows($where);

        if ($this->param('page')){
            $page = intval($this->param('page'));
        }
        else{
            $page = 1;
        }

        $start = ($page * $this->items_per_page) - $this->items_per_page;

        $totalPages = ceil($totalRecordCount / $this->items_per_page);

        if ($page > $totalPages){
            $page = $totalPages;
        }

        $paginationStructure = new stdClass();

        $paginationStructure->startPageLink = null;
        $paginationStructure->prevPageLink = null;
        $paginationStructure->nextPageLink = null;
        $paginationStructure->lastPageLink = null;

        $paginationStructure->page3left = null;
        $paginationStructure->page2left = null;
        $paginationStructure->page1left = null;

        $paginationStructure->page3right = null;
        $paginationStructure->page2right = null;
        $paginationStructure->page1right = null;


        if ($page != 1) {
            $paginationStructure->startPageLink = $link . "&page=1";
        }
        // Проверяем нужны ли стрелки вперед
        if ($page != $totalPages) {
            $paginationStructure->lastPageLink = $link . "&page=" . $totalPages;
        }

        // Находим три ближайшие станицы с обоих краев, если они есть
        if($page - 3 > 0) {
            $paginationStructure->page3left = $link . "&page=" . ($page - 3);
        }

        if($page - 2 > 0) {
            $paginationStructure->page2left = $link . "&page=" . ($page - 2);
        }
        if($page - 1 > 0) {
            $paginationStructure->page1left = $link . "&page=" . ($page - 1);
        }

        if($page + 3 < $totalPages) {
            $paginationStructure->page3right = $link . "&page=" . ($page + 3);
        }
        if($page + 2 < $totalPages) {
            $paginationStructure->page2right = $link . "&page=" . ($page + 2);
        }
        if($page + 1 < $totalPages) {
            $paginationStructure->page1right = $link . "&page=" . ($page + 1);
        }

        $this->smarty->assign('paginationStructure', $paginationStructure);
        $this->smarty->assign('page', $page);
        $this->smarty->assign('totalRecordCount', $totalRecordCount);

        $items = $this->getItems($where, $start);

        $this->appendChildItems($items);

        foreach ($items as $key => $item) {
            $items[$key]->edit_get = $this->form_get(array('section' => 'BlockAnnouncement', 'item_id' => $item->id, 'token' => $this->token));
            $items[$key]->delete_get = $this->form_get(array('act' => 'delete', 'item_id' => $item->id, 'token' => $this->token));
            $items[$key]->enable_get = $this->form_get(array('set_enabled'=>$item->id, 'item_id'=>$item->id, 'token'=>$this->token));
            $items[$key]->show_get = $this->form_get(array('set_show'=>$item->id, 'item_id'=>$item->id, 'token'=>$this->token));
        }

        $this->smarty->assign('ItemsCount', count($items));
        $this->smarty->assign('search', $search);
        $this->smarty->assign('Items', $items);
        $this->smarty->assign('title', $this->title);
        $this->smarty->assign('Lang', $this->lang);
        $this->body = $this->smarty->fetch('block_announcement/_list.tpl');
    }

    function getItems($where = '', $start){
        $limit = $this->items_per_page;
        $query = sql_placeholder("SELECT g.*, DATE_FORMAT(g.created, '%d.%m.%Y %H:%i') AS date_created, ua.name as creator_name
            FROM {$this->tableName} as g
            LEFT JOIN users_admin ua ON ua.id = g.creator_id
          WHERE 1 {$where}
          ORDER BY g.created DESC
          LIMIT {$start}, {$limit}");

        $this->db->query($query);
        $items = $this->db->results();
        return $items;
    }

    function getTotalRows($where)
    {
        $query = sql_placeholder("SELECT COUNT(*) AS recordCount FROM {$this->tableName} WHERE 1 {$where}");
        $this->db->query($query);
        $count = $this->db->result();

        return $count->recordCount;
    }

    function getBlockAnnouncementContent($id)
    {
        $groupItemIdListQuery = sql_placeholder("SELECT g.id, g.content_type, g.content_id
                              FROM " . $this->itemTableName . " AS g
                              WHERE group_id = ?
                              ORDER BY g.order_num ASC", $id);

        $this->db->query($groupItemIdListQuery);
        $groupItemIdList = $this->db->results();
        $postList = [];
        foreach ($groupItemIdList as $groupItemId) {
            if ($groupItemId->content_type == 'blog') {
                $post = new BlogPosts();
                $groupItem = $post->getPostByTypeBlock('1_3', $groupItemId->content_id);
            } elseif ($groupItemId->content_type == 'news') {
                $post = new NewsItems();
                $groupItem = $post->getNewsById($groupItemId->content_id);
            }
            if (!empty($groupItem)) {
                $postList = array_merge($postList, $groupItem);
            }
        }

        return $postList;
    }

    function formatBlockAnnouncement($group)
    {
        $postList = $this->getBlockAnnouncementContent($group->id);

        $formattedGroup = array(
            'id' => intval($group->id),
            'name' => strip_tags($group->name),
            'date' => date('d.m.Y', strtotime($group->created)),
            'url' => $group->url,
            'enabled' => boolval($group->enabled),
            'postList' => $postList
        );
        return $formattedGroup;
    }

    function getBlockAnnouncementById($id)
    {
        $query = sql_placeholder("SELECT g.id, g.name, g.created, g.enabled, g.url
                              FROM " . $this->tableName . " AS g
                              WHERE g.enabled = 1
                                AND g.id = ?
                              ORDER BY g.created DESC", $id);
        $this->db->query($query);
        $group = $this->db->result();

        $result = $this->formatBlockAnnouncement($group);

        return $result;
    }

    function getBlockAnnouncementList()
    {
        $result = array();

        $query = sql_placeholder("SELECT g.id, g.name, g.created, g.enabled, g.url
                              FROM " . $this->tableName . " AS g
                              ORDER BY g.created DESC");
        $this->db->query($query);
        $items = $this->db->results();

        foreach ($items as $item) {
            $result[] = $this->formatBlockAnnouncement($item);
        }

        return $result;
    }

    private function appendChildItems(array & $items)
    {
        $ids = array_map(function ($item) {return $item->id;}, $items);

        $this->db->query(sql_placeholder("
                    SELECT *, NULL as post FROM {$this->itemTableName} bai 
                     WHERE bai.block_announcement_id IN (?@)
                  ORDER BY bai.block_announcement_id, bai.order_num ASC"
            , $ids)
        );

        $childs = $this->db->results();


        $contentIDs = array('blog' => array(0), 'news' => array(0));
        foreach ($childs as $block) {
            $contentIDs[$block->content_type][] = $block->content_id;
        }

        $info = $this->getInfoByContentIDs($contentIDs);

        foreach ($info as $index => $post) {
            foreach ($childs as $ind => $child) {
                if ($child->content_type . '_' . $child->content_id === $post->type . '_' . $post->id ) {
                    $childs[$ind]->post = $post;
                }
            }
        }

        foreach ($items as $item ) {
            $item->childs = [];
            foreach ($childs as $child) {
                if ($child->block_announcement_id === $item->id) {
                    $item->childs[] = $child;
                }
            }
        }
    }


    private function getInfoByContentIDs(array $contentIDs)
    {
        $query = sql_placeholder("(SELECT 'blog' as type, b.id, b.blocks, b.tags, b.name, b.header, b.url, b.is_partner_material,
                        b.type_post, b.partner_url, b.post_tag, b.signature_to_sum, b.text_body, b.video_on_main, b.state, b.type_video,
                        b.amount_to_displaying, bw.name AS writer, bw.id AS writer_id, bw.private, bw.bloger, bw.name_genitive,
                        bw.image AS writerImage, bw.stub_image, bw.stub_link, p.name AS partner,
                        p.color AS partner_color, b.anounce_new_author, b.spec_project, b.conference,
                        b.citate_author_name, b.image_1_1, b.image_1_2, b.image_1_3, b.image_rss,
                        DATE_FORMAT(b.created, '%d.%m.%Y %H:%i:%s') AS date_created,
                        bt.name AS tagName, bt.url AS tagUrl, b.format, b.font, p.name AS partnerName,
                        bw.bloger, bw.name as writerName, bw.announcement_img as blogImg, bw.id as writerId, bw.tag_url AS writerTag,
                        b.type_announce_1_1, b.type_announce_1_2, b.type_announce_1_3,
                        bt.url as iconCode,
                        bt.name as iconName,
                        bt.url as btUrl,
                        NULL AS newsUrl,

                            b.type_announce_1_1, b.type_announce_1_2, b.type_announce_1_3
                            ,sp.name AS sp_name, sp.header AS sp_header, sp.external_url AS sp_url
                            ,sp.image_1_1 AS sp_image_1_1, sp.image_1_2 AS sp_image_1_2, sp.image_1_3 AS sp_image_1_3
                            ,sp.type_announce_1_1 AS sp_type_announce_1_1
                            ,sp.type_announce_1_2 AS sp_type_announce_1_2
                            ,sp.type_announce_1_3 AS sp_type_announce_1_3
                        FROM blogposts AS b
                      LEFT JOIN blogwriters AS bw ON bw.id=b.writers
                      LEFT JOIN partners AS p ON p.id=b.partner
                      LEFT JOIN blogtags AS bt ON bt.id = b.tags
                      LEFT JOIN spec_projects AS sp ON sp.id=b.spec_project
                      -- LEFT JOIN conferences AS conf ON conf.id=b.conference
                      WHERE  b.blocks != 'ticker' AND (b.id IN (?@)) AND (bw.enabled = 1 OR b.spec_project) )
                  UNION ALL
                        (SELECT 'news' as type, n.id, NULL as blocks, n.tags, n.header as name, n.text_content as header, n.url, n.is_partner_material,
                        'news' as type_post, NULL as partner_url, NULL as post_tag, NULL as signature_to_sum, NULL as text_body, NULL as video_on_main, NULL as state, NULL as type_video,
                        NULL as amount_to_displaying, bw.name AS writer, bw.id AS writer_id, bw.private, bw.bloger, bw.name_genitive,
                        bw.image AS writerImage, bw.stub_image, bw.stub_link, NULL as partner,
                        NULL AS partner_color, NULL as anounce_new_author, NULL as spec_project, NULL as conference,
                        NULL as citate_author_name, image_1_1, image_1_2, image_1_3, image_rss,
                        DATE_FORMAT(n.created, '%d.%m.%Y') AS date_created,
                        bt.name AS tagName, CONCAT ('news', '/', DATE_FORMAT(n.created, '%Y/%m/%d')) AS tagUrl, 'gray' as format, NULL as font, NULL as partnerName,
                        bw.bloger, bw.name as writerName, bw.announcement_img as blogImg, bw.id as writerId, bw.tag_url AS writerTag,
                        n.type_announce_1_1, n.type_announce_1_2, n.type_announce_1_3,
                        bt.url as iconCode,
                        bt.name as iconName,
                        bt.url as btUrl,
                        CONCAT ('news', '/', DATE_FORMAT(n.created, '%Y/%m/%d')) AS newsUrl,

                            NULL as sp_type_announce_1_1, NULL as sp_type_announce_1_2, NULL as sp_type_announce_1_3
                            ,NULL AS sp_name, NULL AS sp_header, NULL AS sp_url
                            ,NULL AS sp_image_1_1, NULL AS sp_image_1_2, NULL AS sp_image_1_3
                            ,NULL AS sp_type_announce_1_1
                            ,NULL AS sp_type_announce_1_2
                            ,NULL AS sp_type_announce_1_3
                        FROM news AS n
                      LEFT JOIN blogwriters AS bw ON bw.id=n.writers
                      LEFT JOIN blogtags AS bt ON bt.id = n.tags
                      WHERE  (n.id IN (?@)) AND bw.enabled = 1)
                   ", @$contentIDs['blog'],  @$contentIDs['news']);


        $this->db->query($query);
        $searchItems = $this->db->results();



        foreach ($searchItems as & $res ) {
            if ($res->type === 'news') {
                $res->iconCode = $res->btUrl;
                $res->tagUrl = $res->btUrl;
                $res->url = $res->newsUrl . $res->url;
            }
        }

        return Helper::formatPostList($searchItems, '1_3');
    }

}
