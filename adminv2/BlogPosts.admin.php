<?PHP

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

require_once('Widget.admin.php');
require_once('../placeholder.php');
require_once('PagesNavigation.admin.php');
require_once('BlogWriters.admin.php');
require_once ('../PresetFormat.class.php');
require_once ('../Helper.class.php');
require_once('Plots.admin.php');


/**
 * Class BlogPosts
 */
class BlogPosts extends Widget{
    var $pages_navigation;
    var $items_per_page = 50;
    var $category;
    var $uploaddir = '../files/blogposts/'; # Папка для хранения картинок (default)
    private $tableName = 'blogposts';
    public $POST_TYPES = array(1 => 'Пост', 2 => 'Пост', 3 => 'Пост', 5 => "Данные", 11 => 'Видео пост', 6 => 'Мем недели', 7 => 'Факт', 8 => 'Цитата', 9 => 'Анонс спецпроекта', 10 => 'DIY', 12 => 'Бегущая строка', 13 => 'Инструкция', 14 => 'Партнерский анонс', 100 => 'Новость');

    function BlogPosts(&$parent = null){
        parent::Widget($parent);
        $this->prepare();
    }

    function prepare(){

        if (isset($_GET['act']) && $_GET['act'] == 'delete_post' && (isset($_POST['items']) || isset($_GET['item_id']))) {

            $this->check_token();

            $this->deleteItem($_GET['item_id'], $this->tableName);

            header("Location: /adminv2/blogpost/");
        }

        if (isset($_GET['enable_post'])) {
            $this->check_token();

            $this->setEnable($_GET['enable_post'], $this->tableName);

            header("Location: /adminv2/blogpost/");
        }
    }

    function fetch(){
        $this->title = 'Записи в блогах';

        $link = "index.php?section=BlogPosts";

        $where = "";

        $writerId = intval($this->param('writer_id'));

        if ($search = $this->param('search')) {

            if (isset($this->config->indexing) && $this->config->indexing) {
                require_once './../Sphinx.class.php';
                $indexDBConfig = $this->config->sphinx;
                $sphinxDB = new Sphinx($indexDBConfig['name'], $indexDBConfig['host'], $indexDBConfig['user'], $indexDBConfig['password'], $indexDBConfig['port']);
                $tablePrefix = @$indexDBConfig['table_prefix'];
                $sphinxDB->connect();
                $sphinxDB->query(sql_pholder("SELECT id FROM {$tablePrefix}posts WHERE MATCH (?) AND model = 1 ORDER BY WEIGHT() DESC LIMIT 5000
                                              OPTION field_weights=(name=100, header=60, lead=10, body=5)", $search));
                $postsIds = $sphinxDB->results();
                $sphinxDB->disconnect();
                $postsIdsStr = join(',', array_merge([0],array_map(function ($item ){ return $item->id;}, $postsIds)));
                $where .= " AND (id IN ({$postsIdsStr})) ";
            } else {
                $where .= " AND (name LIKE '%$search%' OR header LIKE '%$search%' OR lead LIKE '%$search%' OR text_body LIKE '%$search%')";
            }
            $link .= "&search=" . $search;
        }
        if ($type_post = $this->param('type_post')){
            $where .= " AND type_post = " . (int)$type_post;
            $link .= "&type_post=" . $type_post;
        }


        if(!empty($writerId)){
            $where = ' AND writers=' . $writerId;
        }
        // общее кол-во записей
        $totalRecordCount = $this->getTotalRows($where);

        if ($this->param('page')){
            $page = intval($this->param('page'));
        }
        else{
            $page = 1;
        }

        $start = ($page * $this->items_per_page) - $this->items_per_page;

        $totalPages = ceil($totalRecordCount / $this->items_per_page);

        if ($page > $totalPages){
            $page = $totalPages;
        }

        $paginationStructure = new stdClass();

        $paginationStructure->startPageLink = null;
        $paginationStructure->prevPageLink = null;
        $paginationStructure->nextPageLink = null;
        $paginationStructure->lastPageLink = null;

        $paginationStructure->page3left = null;
        $paginationStructure->page2left = null;
        $paginationStructure->page1left = null;

        $paginationStructure->page3right = null;
        $paginationStructure->page2right = null;
        $paginationStructure->page1right = null;


        if ($page != 1) {
            $paginationStructure->startPageLink = $link . "&page=1";
        }
        // Проверяем нужны ли стрелки вперед
        if ($page != $totalPages) {
            $paginationStructure->lastPageLink = $link . "&page=" . $totalPages;
        }

        // Находим три ближайшие станицы с обоих краев, если они есть
        if($page - 3 > 0) {
            $paginationStructure->page3left = $link . "&page=" . ($page - 3);
        }

        if($page - 2 > 0) {
            $paginationStructure->page2left = $link . "&page=" . ($page - 2);
        }
        if($page - 1 > 0) {
            $paginationStructure->page1left = $link . "&page=" . ($page - 1);
        }

        if($page + 3 < $totalPages) {
            $paginationStructure->page3right = $link . "&page=" . ($page + 3);
        }
        if($page + 2 < $totalPages) {
            $paginationStructure->page2right = $link . "&page=" . ($page + 2);
        }
        if($page + 1 < $totalPages) {
            $paginationStructure->page1right = $link . "&page=" . ($page + 1);
        }

        $this->smarty->assign('paginationStructure', $paginationStructure);
        $this->smarty->assign('page', $page);
        $this->smarty->assign('totalRecordCount', $totalRecordCount);

        $items = $this->getPostDataJson($where, $start);

        $this->smarty->assign('search', $search);
        $this->smarty->assign('type_post', $type_post);
        $this->smarty->assign('Items', $items);
        $this->smarty->assign('ItemsCount', count($items));
        $this->smarty->assign('Lang', $this->lang);
        $this->smarty->assign('images_uploaddir', $this->uploaddir);
        $this->smarty->assign('current_writer', $writerId);
        $this->smarty->assign('token', $this->token);
        $this->body = $this->smarty->fetch('blogposts.tpl');
    }

    function getTotalRows($where)
    {
        $query = sql_placeholder("SELECT COUNT(*) AS recordCount FROM {$this->tableName} WHERE 1 {$where}");
        $this->db->query($query);
        $count = $this->db->result();

        return $count->recordCount;
    }

    function getPostDataJson($where = '', $start){
        $limit = $this->items_per_page;

        $query = sql_placeholder("SELECT id, url, image_1_1, image_1_2, image_1_3, image_rss, created, modified, show_main_page, name, header, meta_title,
                              type_post, tags, partner, writers, type_material, enabled, post_tag, spec_project, text_body, thumbnail,
                              DATE_FORMAT(created, '%d.%m.%Y') AS date_created,
                              DATE_FORMAT(created, '%H:%i') AS time_created,
                              DATE_FORMAT(modified, '%d.%m.%Y') AS date_modified,
                              DATE_FORMAT(modified, '%H:%i') AS time_modified
                              FROM " . $this->tableName . "
                              WHERE 1 $where
                              ORDER BY created DESC, order_num ASC
                              LIMIT {$start}, {$limit}");
        $this->db->query($query);
        $items = $this->db->results();

        $mainPagePostIDs = $this->getMainPagePostIDs();

        foreach ($items as &$item_1) {
            if (in_array($item_1->id, $mainPagePostIDs)) {
                $item_1->show_main_page = 1;
            }
        }

        $items = $this->makeAddInfoToPostData($items);

        return $items;
    }


    function makeAddInfoToPostData($items){
        $nowDate = new DateTime();
        foreach($items as $key=>$item){
            try {
                $createdDate = new DateTime($item->created);
                $item->deferred = $createdDate > $nowDate;
            } catch (Exception $e) {
                $item->deferred = false;
            }
            $enabledLink = '<a href="blogpost/enable/' . $item->id . '/' . $this->token . '">';
            if ($item->enabled == 1){
                $enabledLink .= '<i class="fa fa-toggle-on fa-1x"></i>';
            }
            else{
                $enabledLink .= '<i class="fa fa-toggle-off fa-1x"></i>';
            }
            $enabledLink .= '</a>';
            $items[$key]->enabled = $enabledLink;

            $query = "SELECT t.name, t.url FROM blogtags AS t
                          WHERE t.id=" . $item->tags . " ORDER BY t.name";
            $this->db->query($query);
            $tag = $this->db->result();
            $items[$key]->tag = $tag;

            $category = !empty($tag) ? $tag->url : '';
            $items[$key]->linkToPost = '<a href="../blog/' . $category . '/' . $item->url . '?mode=preview" target="_blank"><i class="fa fa-eye"></i></a>';

            $showMainPage = "<em>На главной: </em>";
            if ($item->show_main_page == 1){
                $showMainPage .= '<i class="glyphicon glyphicon-check text-success"></i>';
            }
            else{
                $showMainPage .= '<i class="glyphicon glyphicon-unchecked text-muted"></i>';
            }

            $tagName = !empty($tag) ? $tag->name : '';
            $items[$key]->tagStr = '<span class="text-bolder">' . $tagName . "</span><br/>" . $showMainPage. '';

            // Тег для главной страницы
            $query = "SELECT name FROM post_tags WHERE id=" . $item->post_tag . " LIMIT 1";
            $this->db->query($query);
            $postTagMain = $this->db->result();

            // Теги для внутренней страницы
            $query = "SELECT t.name, t.url FROM relations_postitem_tags AS rel
                          INNER JOIN post_tags AS t ON t.id=rel.posttag_id
                          WHERE rel.post_id=" . $item->id . " AND t.enabled=1 ORDER BY t.name";
            $this->db->query($query);
            $postTags = $this->db->results();

            $mainTag = "";
            if (!empty($postTagMain->name)){
                $mainTag = "<span class='mainTag'>" . $postTagMain->name . "</span><br/> ";
            }

            $items[$key]->postTagsStr = $mainTag . $this->getStrFromObject($postTags);
            $items[$key]->postTags = $postTags;

            $query = "SELECT name FROM partners WHERE id=" . $item->partner . " LIMIT 1";
            $this->db->query($query);
            $partner = $this->db->result();

            $items[$key]->partnerRecordStr = $this->getStrFromObject($partner);
            $items[$key]->partnerRecord = $partner;

            $query = "SELECT name FROM blogwriters WHERE id=" . $item->writers . " LIMIT 1";
            $this->db->query($query);
            $writer = $this->db->result();

            $items[$key]->writerStr = $this->getStrFromObject($writer);
            $items[$key]->partnerRecord = $writer;


            if ($item->spec_project != 0){
                $query = "SELECT name, image_1_1 FROM spec_projects WHERE id=" . $item->spec_project . " LIMIT 1";
                $this->db->query($query);
                $sp = $this->db->result();

//                $items[$key]->spStr = $this->getStrFromObject($sp);
                $items[$key]->specProject = $sp;
            }

            $item->date_created .= "<br/>" . $item->time_created;
            $item->date_modified .= "<br/>" . $item->time_modified;

            $items[$key]->linked = '<a href="#" type="button" post_id="' . $item->id . '" onclick="return false;" class="btn btn-success btn-xs link_to_linked_post"><i class="fa fa-link"></i> Связать</a>';

            $items[$key]->deleteBtn = '<a href="blogpost/delete/' . $item->id . '/' . $this->token . '" class="btn btn-danger btn-xs" type="button" onclick="if(!confirm("Удалить запись?")) return false;">
                                            <i class="fa fa-times-circle fa-1x"></i>
                                        </a>';


            $name = $item->name;
            $name = str_replace("<p>", "", $name);
            $name = str_replace("</p>", "", $name);
            $name = "" . $name;

            $header = "";
            if (!empty($item->header)){
                $header = $item->header;
                $header = str_replace("<p>", "", $header);
                $header = str_replace("</p>", "", $header);
                $header = "" . $header;
            }

            $items[$key]->image = '<img src="images/no_foto.gif" style="width: 80px;" />';
            switch ($item->type_post) {
                case 1:
                case 2:
                case 3:

                    $linkEditStart = '<a href="blogpost/edit/post/' . $item->id . '/' . $this->token . '">';
                    $linkEditEnd = '</a>';

                    $items[$key]->typePost = "post";
                    $items[$key]->name = $linkEditStart . $name . $linkEditEnd;
                    $items[$key]->typematerial = '<em>Пост</em>';

                    if (!empty($header)){
                        $items[$key]->header = $linkEditStart .  $header . $linkEditEnd;
                    }

                    if ($item->image_1_2){
                        if (file_exists($this->uploaddir . $item->image_1_2)){
                            $items[$key]->image = '<img src="' . $this->uploaddir . $item->image_1_2 . '" style="width: 80px;" />';
                        }
                    }
                    break;
                case 4:

                    $linkEditStart = '<a href="blogpost/edit/newblog/' . $item->id . '/' . $this->token . '">';
                    $linkEditEnd = '</a>';

                    $items[$key]->typePost = "newblog";
                    $items[$key]->name = $linkEditStart  .  $item->partnerRecord->name . $linkEditEnd;

                    $items[$key]->typematerial = '<em>Анонс нового блога</em>';
                    if (!empty($header)){
                        $items[$key]->header = $linkEditStart .  $header . $linkEditEnd;
                    }
                    break;
                case 5:

                    $linkEditStart = '<a href="blogpost/edit/data/' . $item->id . '/' . $this->token . '">';
                    $linkEditEnd = '</a>';

                    $items[$key]->typePost = "data";
                    $items[$key]->name = $linkEditStart . $item->partnerRecord->name . $linkEditEnd;

                    $items[$key]->typematerial = '<em>Данные</em>';
                    if (!empty($header)){
                        $items[$key]->header = $linkEditStart  .  $header . $linkEditEnd;
                    }
                    break;
                case 6:

                    $linkEditStart = '<a href="blogpost/edit/imageday/' . $item->id . '/' . $this->token . '">';
                    $linkEditEnd = '</a>';

                    $items[$key]->typePost = "imageday";
                    $items[$key]->name = $linkEditStart . $name . $linkEditEnd;

                    $items[$key]->typematerial = '<em>Фото дня</em>';
                    if (!empty($header)){
                        $items[$key]->header = $linkEditStart .  $header . $linkEditEnd;
                    }

                    if ($item->image_1_2){
                        if (file_exists($this->uploaddir . $item->image_1_2)){
                            $items[$key]->image = '<img src="' . $this->uploaddir . $item->image_1_2 . '" style="width: 80px;" />';
                        }
                    }
                    break;
                case 7:

                    $linkEditStart = '<a href="blogpost/edit/factday/' . $item->id . '/' . $this->token . '">';
                    $linkEditEnd = '</a>';

                    $items[$key]->typePost = "factday";
                    $items[$key]->name = $linkEditStart . $name . $linkEditEnd;

                    $items[$key]->typematerial = '<em>Факт</em>';
                    if (!empty($header)){
                        $items[$key]->header = $linkEditStart .  $header . $linkEditEnd;
                    }
                    break;
                case 8:

                    $linkEditStart = '<a href="blogpost/edit/citate/' . $item->id . '/' . $this->token . '">';
                    $linkEditEnd = '</a>';

                    $items[$key]->typePost = "citate";
                    $items[$key]->name = $linkEditStart . $name . $linkEditEnd;
                    $items[$key]->typematerial = '<em>Цитата</em>';
                    if (!empty($header)){
                        $items[$key]->header = $linkEditStart . $header . $linkEditEnd;
                    }

                    if ($item->image_1_1){
                        if (file_exists($this->uploaddir . $item->image_1_1)){
                            $items[$key]->image = '<img src="' . $this->uploaddir . $item->image_1_1 . '" style="width: 80px;" />';
                        }
                    }
                    break;
                case 9:

                    $linkEditStart = '<a href="blogpost/edit/specproject/' . $item->id . '/' . $this->token . '">';
                    $linkEditEnd = '</a>';

                    $items[$key]->typePost = "specproject";
                    $items[$key]->name = $linkEditStart  .  $item->specProject->name . $linkEditEnd;
                    if (!empty($header)){
                        $items[$key]->header = $linkEditStart  .  $header . $linkEditEnd;
                    }

                    $items[$key]->typematerial = '<em>Анонс спецпроекта</em>';

                    if ($item->specProject->image_1_1){
                        if (file_exists('../files/spec_project/' . $item->specProject->image_1_1)){
                            $items[$key]->image = '<img src="../files/spec_project/' . $item->specProject->image_1_1 . '" style="width: 80px;" />';
                        }
                    }
                    break;
                case 10:

                    $linkEditStart = '<a href="blogpost/edit/diypost/' . $item->id . '/' . $this->token . '">';
                    $linkEditEnd = '</a>';

                    $items[$key]->typePost = "diypost";
                    $items[$key]->name = $linkEditStart . $name . $linkEditEnd;

                    if (!empty($header)){
                        $items[$key]->header = $linkEditStart .   $header . $linkEditEnd;
                    }

                    $items[$key]->typematerial = '<em>Карточка DIY</em>';

                    if ($item->image_1_2){
                        if (file_exists($this->uploaddir . $item->image_1_2)){
                            $items[$key]->image = '<img src="' . $this->uploaddir . $item->image_1_2 . '" style="width: 80px;" />';
                        }
                    }
                    break;
                case 11:

                    $linkEditStart = '<a href="blogpost/edit/newvideo/' . $item->id . '/' . $this->token . '">';
                    $linkEditEnd = '</a>';

                    $items[$key]->typePost = "newvideo";
                    $items[$key]->name = $linkEditStart . $name . $linkEditEnd;

                    if (!empty($header)){
                        $items[$key]->header = $linkEditStart  .  $header . $linkEditEnd;
                    }

                    $items[$key]->typematerial = '<em>Видео</em>';
                    break;
                case 12:

                    $linkEditStart = '<a href="blogpost/edit/ticker/' . $item->id . '/' . $this->token . '">';
                    $linkEditEnd = '</a>';

                    $items[$key]->typePost = "ticker";

                    $items[$key]->typematerial = '<em>Бегущая строка</em>';
                    $items[$key]->name = $linkEditStart . $name . $linkEditEnd;
                    break;
                case Helper::TYPE_MANUAL:

                    $linkEditStart = '<a href="blogpost/edit/manual/' . $item->id . '/' . $this->token . '">';
                    $linkEditEnd = '</a>';

                    $items[$key]->typePost = "manual";
                    $items[$key]->typematerial = '<em>Инструкция</em>';

                    $items[$key]->name = $linkEditStart .  $name . $linkEditEnd;

                    if (!empty($header)){
                        $items[$key]->header = $linkEditStart .  $header . $linkEditEnd;
                    }
                    if ($item->image_1_3){
                        if (file_exists($this->uploaddir . $item->image_1_3)){
                            $items[$key]->image = '<img src="' . $this->uploaddir . $item->image_1_3 . '" style="width: 80px;" />';
                        }
                    }
                    break;
                case Helper::TYPE_PARTNER_ANNOUNCE:

                    $linkEditStart = '<a href="blogpost/edit/partner_announce/' . $item->id . '/' . $this->token . '">';
                    $linkEditEnd = '</a>';

                    $items[$key]->typePost = "partner_announce";
                    $items[$key]->typematerial = '<em>Партнерский анонс</em>';
                    $items[$key]->name = $linkEditStart .  $name . $linkEditEnd;

                    if (!empty($header)){
                        $items[$key]->header = $linkEditStart .  $header . $linkEditEnd;
                    }
                    if ($item->image_1_3){
                        if (file_exists($this->uploaddir . $item->image_1_3)){
                            $items[$key]->image = '<img src="' . $this->uploaddir . $item->image_1_3 . '" style="width: 80px;" />';
                        }
                    }
                    break;
            }
            if (trim($item->thumbnail) && file_exists($this->uploaddir . $item->thumbnail)){
                $items[$key]->image = '<img src="' . $this->uploaddir . $item->thumbnail . '" style="max-width: 80px;" />';
            }
        }

        return $items;
    }


    function getPostsData($where = ''){

        $query = sql_placeholder("SELECT bp.id, bp.image_1_1, bp.image_1_2, bp.image_rss, bp.created, bp.modified, bp.show_main_page, bp.name, bp.header, bp.meta_title,
                              bp.type_post, bp.tags, bp.partner, bp.writers, bp.type_material, bp.enabled,
                              DATE_FORMAT(bp.created, '%d.%m.%Y') AS date_created,
                              DATE_FORMAT(bp.created, '%H:%i') AS time_created,
                              DATE_FORMAT(bp.modified, '%d.%m.%Y') AS date_modified,
                              DATE_FORMAT(bp.modified, '%H:%i') AS time_modified,
                              bw.enabled as writersEnabled
                              FROM " . $this->tableName . " as bp
                              LEFT JOIN blogwriters as bw ON bw.id = bp.writers
                              WHERE 1 $where
                              ORDER BY bp.order_num ASC");
        $this->db->query($query);
        $items = $this->db->results();
        if (is_array($items)) {
        foreach($items as $key=>$item){

            $items[$key]->edit_get = $this->form_get(array('page'=>'post', 'item_id'=>$item->id, 'token'=>$this->token, 'section'=>'BlogPost'));
            $items[$key]->delete_get = $this->form_get(array('act'=>'delete_post','item_id'=>$item->id, 'writer_id'=> (isset($writerId) ? $writerId: null ), 'token'=>$this->token));
            $items[$key]->enable_get = $this->form_get(array('enable_post'=>$item->id, 'token'=>$this->token));

            $query = "SELECT t.name, t.url FROM blogtags AS t
                          WHERE t.id=" . $item->tags . " ORDER BY t.name";
            $this->db->query($query);
            $items[$key]->tag = $this->db->result();

            $query = "SELECT t.name, t.url FROM relations_postitem_tags AS rel
                          INNER JOIN post_tags AS t ON t.id=rel.posttag_id
                          WHERE rel.post_id=" . $item->id . " AND t.enabled=1 ORDER BY t.name";
            $this->db->query($query);

            $postTags = $this->db->results();

            $items[$key]->postTagsStr = $this->getStrFromObject($postTags);
            $items[$key]->postTags = $postTags;

            $query = "SELECT name FROM partners WHERE id=" . $item->partner . " LIMIT 1";
            $this->db->query($query);
            $partner = $this->db->result();

            $items[$key]->partnerRecordStr = $this->getStrFromObject($partner);
            $items[$key]->partnerRecord = $partner;

            $query = "SELECT name FROM blogwriters WHERE id=" . $item->writers . " LIMIT 1";
            $this->db->query($query);
            $writer = $this->db->result();

            $items[$key]->writerStr = $this->getStrFromObject($writer);
            $items[$key]->partnerRecord = $writer;

            $items[$key]->writer = $this->db->result();

            $query = "SELECT id, name FROM typematerial WHERE id=" . $item->type_material . " LIMIT 1";
            $this->db->query($query);
            $items[$key]->typematerial = $this->db->result();

            $item->date_created .= "<br/>" . $item->time_created;
            $item->date_modified .= "<br/>" . $item->time_modified;

            $items[$key]->typePost = "post";

            if ($item->enabled == 1 && $item->writersEnabled == 1) {
                $items[$key]->linked = '<a href="#" type="button" post_id="' . $item->id . '" onclick="return false;" class="btn btn-success btn-xs link_to_linked_post"><i class="fa fa-link"></i> Связать</a>';
            } else {
                $text = "Статья скрыта";
                if ($item->enabled == 1 && $item->writersEnabled != 1) { $text = "Автор скрыт";}
                $items[$key]->linked = '<a href="#" type="button" post_id="' . $item->id . '" onclick="return false;" class="btn btn-success btn-xs link_to_linked_post btn--disabled disabled">
                    <i class="fa fa-link"></i> Связать</a><br><br><div class="alert-msg">' . $text . '</div>';
            }

            $name = $item->name;
            $name = str_replace("<p>", "", $name);
            $name = str_replace("</p>", "", $name);

            $name = "<br/>" . $name;

            $items[$key]->image = '<img src="images/no_foto.gif" style="width: 80px;" />';
            switch ($item->type_post) {
                case 1:
                case 2:
                case 3:
                    $items[$key]->typePost = "post";
                    $items[$key]->name = "Пост " .  $name;

                    if (file_exists($this->uploaddir . $item->image_1_2)){
                        $items[$key]->image = '<img src="' . $this->uploaddir . $item->image_1_2 . '" style="width: 80px;" />';
                    }
                    break;
                case 4:
                    $items[$key]->typePost = "newblog";
                    $items[$key]->name = "Анонс нового блога <br/>" .  @$items[$key]->writer->name;
                    break;
                case 5:
                    $items[$key]->typePost = "data";
                    $items[$key]->name = "Данные <br/>" . @$items[$key]->writer->name;
                    break;
                case 6:
                    $items[$key]->typePost = "imageday";
                    $items[$key]->name = "Фото дня " . $name;

                    if (file_exists($this->uploaddir . $item->image_1_1)){
                        $items[$key]->image = '<img src="' . $this->uploaddir . $item->image_1_1 . '" style="width: 80px;" />';
                    }
                    break;
                case 7:
                    $items[$key]->typePost = "factday";
                    $items[$key]->name = "Факт " . $name;
                    break;
                case 8:
                    $items[$key]->typePost = "citate";
                    $items[$key]->name = "Цитата " . $name;

                    if (file_exists($this->uploaddir . $item->image_1_1)){
                        $items[$key]->image = '<img src="' . $this->uploaddir . $item->image_1_1 . '" style="width: 80px;" />';
                    }
                    break;
                case 11:
                    $items[$key]->typePost = "data";
                    $items[$key]->name = "Видео<br/>" . $items[$key]->name;
                    break;
                case 12:
                    $items[$key]->typePost = "ticker";
                    $items[$key]->name = "Бегущая строка <br/>" . $items[$key]->name;
                    break;
            }
            if (property_exists($item, 'image_rss') && trim($item->image_rss) && file_exists($this->uploaddir . $item->image_rss)){
                $items[$key]->image = '<img src="' . $this->uploaddir . $item->image_rss . '" style="width: 80px;" />';
            }
        }}

        return $items;
    }


    function getPostsDataPaging($start = 0, $limit = 10, $filter = array()) {
        $start = (int)$start;
        $limit = (int)$limit;
        $limitStr = "";
        $wherePosts = "";
        $whereNews = "";
        $results = array();
        if (@$filter['post']){
            $wherePosts = $filter['post'];
        }
        if (@$filter['news']){
            $whereNews = $filter['news'];
        }
        if (@$filter['search']['value']) {
            require_once './../Sphinx.class.php';
            $indexDBConfig = $this->config->sphinx;
            $sphinxDB = new Sphinx($indexDBConfig['name'], $indexDBConfig['host'], $indexDBConfig['user'], $indexDBConfig['password'], $indexDBConfig['port']);
            $tablePrefix = @$indexDBConfig['table_prefix'];
            $sphinxDB->connect();
            $sphinxDB->query(sql_pholder("SELECT id, model FROM {$tablePrefix}posts WHERE MATCH(?) ORDER BY WEIGHT() DESC LIMIT {$start}, {$limit}
                                           OPTION   ranker=expr('sum(lcs) + bm25 + date_created*0.000015 + sum(user_weight)')
                                               , field_weights=(name=25, header=10, lead=5, body=1)
                                         ", $filter['search']['value']));
            $resultIds = $sphinxDB->results();

            $sphinxDB->disconnect();
            $newsIds = array(0);
            $postsIds = array(0);
            $models = array( 1=>'posts', 2=>'news');
            foreach ($resultIds as $res) {
                $itemId = $res->id;
                // 500000 Новый диапазон новостей ID для сфинкса
                if ($res->id > 500000) {
                    $newsIds[] = $res->id - 500000;
                    $itemId = $res->id - 500000;
                } else {
                    $postsIds [] = $res->id;
                }
                $results[$models[$res->model].'_'.$itemId] = false;
            }

            $newsIdsStr = join(',', $newsIds);
            $postsIdsStr = join(',', $postsIds);

            $wherePosts  .= " AND (bp.id IN ({$postsIdsStr})) ";
            $whereNews   .= " AND (news.id IN ({$newsIdsStr})) ";
        } else {
            $limitStr = "LIMIT {$start}, {$limit}";
        }

        $query = sql_placeholder("(SELECT 'posts' as type, bp.id, bp.image_1_1, bp.image_1_2, bp.image_1_3, bp.image_rss, bp.created,
                              bp.modified,  bp.name,
                              bp.type_post, bp.tags, bp.partner, bp.writers, bp.enabled,
                              DATE_FORMAT(bp.created, '%d.%m.%Y') AS date_created,
                              DATE_FORMAT(bp.created, '%H:%i') AS time_created,
                              bw.name as writersName, bw.enabled as writersEnabled,
                              bt.name as bt_name, bt.url as bt_url, p.name as partners_name
                                ,sp.name AS sp_name, sp.header AS sp_header, sp.external_url AS sp_url
                                ,sp.image_1_1 AS sp_image_1_1, sp.image_1_2 AS sp_image_1_2, sp.image_1_3 AS sp_image_1_3
                                ,sp.type_announce_1_1 AS sp_type_announce_1_1
                                ,sp.type_announce_1_2 AS sp_type_announce_1_2
                                ,sp.type_announce_1_3 AS sp_type_announce_1_3
                              FROM blogposts as bp
                              LEFT JOIN blogwriters as bw ON bw.id = bp.writers
                              LEFT JOIN blogtags as bt ON bt.id = bp.tags
                              LEFT JOIN partners as p ON p.id = bp.partner
                              LEFT JOIN spec_projects AS sp ON sp.id=bp.spec_project
                              WHERE bp.type_post <> 12 $wherePosts)
                              
                              UNION ALL
                             
                             (SELECT 'news' as type, news.id, news.image_1_1, news.image_1_2, news.image_1_3, news.image_rss, news.created,
                              news.modified,  news.header as name,
                              100 as type_post, news.tags, NULL as partner, news.writers, news.enabled,
                              DATE_FORMAT(news.created, '%d.%m.%Y') AS date_created,
                              DATE_FORMAT(news.created, '%H:%i') AS time_created,
                              bw.name as writersName, bw.enabled as writersEnabled,
                              bt.name as bt_name, bt.url as bt_url, NULL as partners_name
                                ,NULL AS sp_name, NULL AS sp_header, NULL AS sp_url
                                ,NULL AS sp_image_1_1, NULL AS sp_image_1_2, NULL AS sp_image_1_3
                                ,NULL AS sp_type_announce_1_1
                                ,NULL AS sp_type_announce_1_2
                                ,NULL AS sp_type_announce_1_3
                              FROM news as news
                              LEFT JOIN blogwriters as bw ON bw.id = news.writers
                              LEFT JOIN blogtags as bt ON bt.id = news.tags
                              WHERE  1=1 $whereNews)
                              ORDER BY created DESC
                              $limitStr");

        $this->db->query($query);
        $items = Helper::formatPostList($this->db->results());

        foreach($items as $key=>$item){

            $tag = new stdClass();
            $tag->name = $item->bt_name;
            $tag->url = $item->bt_url;
            $items[$key]->tag = $tag;

            $query = "SELECT t.name, t.url FROM relations_postitem_tags AS rel
                          INNER JOIN post_tags AS t ON t.id=rel.posttag_id
                          WHERE rel.post_id=" . $item->id . " AND t.enabled=1 ORDER BY t.name";
            $this->db->query($query);
            $postTags = $this->db->results();

            $items[$key]->postTagsStr = $this->getStrFromObject($postTags);
            $items[$key]->postTags = $postTags;

            $partner = new stdClass();
            $partner->name = $item->partners_name;
            $items[$key]->partnerRecordStr = $partner->name ;

            $writer = new stdClass();
            $writer->name = $item->writersName;
            $items[$key]->writerStr = $writer->name;
            $items[$key]->writer = $writer;

            $typeMaterial = new stdClass();
            $typeMaterial->name = $item->tm_name;
            $items[$key]->typematerial = $typeMaterial;

            $item->created =  $item->date_created . "<br/>" . $item->time_created;

            if ($item->enabled == 1 && $item->writersEnabled == 1) {
                $items[$key]->linked = '<a href="#" type="button" post_type="'. $item->type_post .'" post_id="' . $item->id . '"
                            onclick="return false;" class="btn btn-success btn-xs link_to_linked_post"><i class="fa fa-link"></i> Связать</a>';
            } else {
                $text = "Статья скрыта";
                if ($item->enabled == 1 && $item->writersEnabled != 1) { $text = "Автор скрыт";}
                $items[$key]->linked = '<a href="#" type="button" post_type="'. $item->type_post .'" post_id="' . $item->id . '"
                            onclick="return false;" class="btn btn-success btn-xs link_to_linked_post btn--disabled disabled">
                    <i class="fa fa-link"></i> Связать</a><br><br><div class="alert-msg">' . $text . '</div>';
            }

            $item->name = htmlspecialchars_decode(strip_tags($item->name));

            $items[$key]->name = '<em>['. $this->POST_TYPES[$item->type_post] .']</em><br/>' . $item->name;

            $results[$item->type . '_'. $item->id] = $item;
        }
        return array_values(array_filter($results, function ($item) {return $item;}));
    }

    function getPostsDataPagingCount($filter) {
        if (@$filter['search']['value']) {
            require_once './../Sphinx.class.php';
            $indexDBConfig = $this->config->sphinx;
            $sphinxDB = new Sphinx($indexDBConfig['name'], $indexDBConfig['host'], $indexDBConfig['user'], $indexDBConfig['password'], $indexDBConfig['port']);
            $tablePrefix = @$indexDBConfig['table_prefix'];
            $sphinxDB->connect();
            $sphinxDB->query(sql_pholder("SELECT COUNT(*) as c_count FROM {$tablePrefix}posts WHERE MATCH(?)", $filter['search']['value']));
            $postsIds = $sphinxDB->result();
            $sphinxDB->disconnect();
            return $postsIds->c_count;
        }
        $wherePosts = "";
        if (@$filter['post']){
            $wherePosts = $filter['post'];
        }
        $whereNews = "";
        if (@$filter['news']){
            $whereNews = $filter['news'];
        }
        $query = sql_placeholder("SELECT COUNT(*) as c_count FROM ((SELECT bp.id
                              FROM blogposts as bp
                              LEFT JOIN blogwriters as bw ON bw.id = bp.writers
                              LEFT JOIN blogtags as bt ON bt.id = bp.tags
                              LEFT JOIN partners as p ON p.id = bp.partner
                              WHERE bp.type_post <> 12 $wherePosts)
                              UNION ALL
                             (SELECT news.id
                              FROM news as news
                              LEFT JOIN blogwriters as bw ON bw.id = news.writers
                              LEFT JOIN blogtags as bt ON bt.id = news.tags
                              WHERE  1=1 $whereNews)) as t");
        $this->db->query($query);
        $items = $this->db->results();
        return $items[0]->c_count;
    }


    /**
     * object to string, string, string
     *
     * @param $object
     * @return string
     */
    function getStrFromObject($object){
        $objectString = "";
        if (!empty($object)){

            if (is_array($object)){
                foreach ($object AS $obj){
                    $tmp[] = $obj->name;
                }
            }
            else{
                $tmp[] = $object->name;
            }
            $objectString = implode(", ", $tmp);
        }

        return $objectString;
    }



    /**
     * @param null $writerId
     * @return array|bool|int
     */
    function getPostsShortList($writerId = null){
        if (!is_null($writerId)){
            $query = sql_placeholder("SELECT *
                              FROM " . $this->tableName . "
                              WHERE  writers=" . $writerId . "
                              ORDER BY order_num ASC");
            $this->db->query($query);
            $items = $this->db->results();
            return $items;
        }
        else{
            return false;
        }
    }

    /**
     * @param null $postId
     * @param null $parentId
     * @param null $postType
     * @param null $parentType
     * @return bool
     */
    function setLinkedPost($postId = null, $parentId = null, $postType = null, $parentType = null){
        if (!is_null($postId) && !is_null($parentId)) {
            $query = sql_placeholder("INSERT IGNORE INTO related_posts SET post_id=?, post_type=?, parent_id=?, parent_type=?", $postId, $postType, $parentId, $parentType);
            $this->db->query($query);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param null $parentId
     * @param string $parentType
     * @return array|bool|int
     */
    function getLinkedPost($parentId = null, $parentType = 'post'){
        if (!is_null($parentId)) {
            $query = sql_placeholder("
            ( SELECT p.id, COALESCE(sp.name, p.name) as name, COALESCE(sp.image_1_3, p.image_1_3) as image_1_3, p.type_post as post_type, p.created, rp.id as rp_id
                FROM blogposts AS p
              INNER JOIN related_posts AS rp ON rp.post_id=p.id
              LEFT JOIN spec_projects AS sp ON p.spec_project = sp.id
              WHERE rp.parent_id=? AND rp.parent_type=? AND (COALESCE(rp.post_type, 0) <> 100))
            UNION ALL
            (SELECT n.id, n.header as name, n.image_1_3 as image_1_3, 100 as post_type, n.created, rp2.id as rp_id
                FROM news as n
               INNER JOIN related_posts AS rp2 ON rp2.post_id = n.id
               WHERE rp2.parent_id=? AND rp2.parent_type=? AND (COALESCE(rp2.post_type,0) = 100))
               ORDER BY rp_id DESC
             ", $parentId, $parentType, $parentId, $parentType
            );
            $this->db->query($query);
            $results = $this->db->results();
            foreach ($results as $res) {
                if ($res->post_type == Helper::TYPE_SPEC_PROJECT) {
                    $res->image_1_3 = '/files/spec_project/'. $res->image_1_3;
                } else {
                    $res->image_1_3 = '/files/blogposts/'. $res->image_1_3;
                }
                $res->name = html_entity_decode(strip_tags($res->name));
                $res->postTypeStr = isset($this->POST_TYPES[$res->post_type]) ? $this->POST_TYPES[$res->post_type] : 'Пост';
                $res->post_type = (int)$res->post_type;
            }
            return $results;
        }
        else{
            return false;
        }
    }

    /**
     * @param null $postId
     * @param null $parentId
     * @param null $postType
     * @param string $parentType
     * @return bool
     */
    function setUnLinkedPost($postId = null, $parentId = null, $postType = null, $parentType = 'post'){
        if (!is_null($postId) && !is_null($parentId)) {
            $query = sql_placeholder("DELETE FROM related_posts WHERE post_id=? AND parent_id=? AND post_type=? AND parent_type=?", $postId, $parentId, $postType, $parentType);
            $this->db->query($query);
            $result = true;
        } else {
            $result = false;
        }
        return $result;
    }
    /**
     * Резервирование записи в БД под новое видео.
     * необходимое чтобы знать ИД обрабатываемой записи
     * в последующем, к этому видео добавяться еще записи, динамически,
     * и вот там нужен будет этот Id
     *
     * @param integer $parentVideoId Id родительской записи (поста)
     * @return int Id новой записи в которую запишется добавляемое видео и дочерние элементы к нему
     */
    function getNewVideoId($parentVideoId){
        $newVideoId = '';

        if (!empty($parentVideoId) && $parentVideoId != 0){
            $query = sql_placeholder('INSERT INTO videos SET parent_video_post = ?, created = now(), modified = now()',
                intval($parentVideoId));
            $this->db->query($query);
            $newVideoId = $this->db->insert_id();
        }

        return $newVideoId;
    }

    function enableVideo($videoId){
        $result = false;

        if (!empty($videoId) && $videoId != 0){
            $query = sql_placeholder('UPDATE videos SET enabled=1-enabled WHERE id=?', intval($videoId));
            if ($this->db->query($query)){
                $result = true;
            }
        }

        return $result;
    }

    function deleteVideo($videoId){
        $result = false;

        if (!empty($videoId) && $videoId != 0){
            $query = sql_placeholder('DELETE FROM videos WHERE id=?', intval($videoId));
            if ($this->db->query($query)){
                $result = true;
            }
        }

        return $result;
    }

    /**
     * Резервирование записи в БД под новое видео.
     * необходимое чтобы знать ИД обрабатываемой записи
     * в последующем, к этому видео добавяться еще записи, динамически,
     * и вот там нужен будет этот Id
     *
     * @param integer $parentVideoId Id родительской записи (поста)
     * @return int Id новой записи в которую запишется добавляемое видео и дочерние элементы к нему
     */
    function getNewDivisionVideoId($parentVideoId){
        $newVideoId = '';

        if (!empty($parentVideoId) && $parentVideoId != 0){
            $query = sql_placeholder('INSERT INTO division_time_videos SET parent_video = ?, created = now(), modified = now()',
                intval($parentVideoId));
            $this->db->query($query);
            $newVideoId = $this->db->insert_id();
        }

        return $newVideoId;
    }

    /**
     * активация/дезактивация видеофрагмента
     *
     * @param $videoId
     * @return bool
     */
    function enablePartVideo($videoId){
        $result = false;

        if (!empty($videoId) && $videoId != 0){
            $query = sql_placeholder('UPDATE division_time_videos SET enabled=1-enabled WHERE id=?', intval($videoId));
            if ($this->db->query($query)){
                $result = true;
            }
        }

        return $result;
    }

    /**
     * удаление видеофрагмента
     *
     * @param $videoId
     * @return bool
     */
    function deletePartVideo($videoId, $parentVideoId){
        $result = false;

        if (!empty($videoId) && $videoId != 0){
            $query = sql_placeholder('DELETE FROM division_time_videos WHERE id=?', intval($videoId));
            $this->db->query($query);

            $query = sql_placeholder("SELECT COUNT(id) AS cnt FROM division_time_videos WHERE parent_video=?", intval($parentVideoId));
            $this->db->query($query);
            $result = $this->db->result();
        }
        return $result;
    }

    /**
     * Сохранение сортировки видео
     *
     * @param array $values
     * @param string $table
     * @return bool
     */
    function saveSort($values = array(), $table = ''){

        if ($table != "" && !empty($values)){
            $order_num = 1;
            foreach ($values as $value_id){
                $value_id = str_replace("row_", "", $value_id);
                $query = sql_placeholder("UPDATE $table SET order_num=? WHERE id=?", $order_num, $value_id);
                if (!$this->db->query($query)){
                    return false;
                }
                ++$order_num;
            }
            return true;
        }
        else{
            return false;
        }

    }

    function getCountTotalPost()
    {
        $query = sql_placeholder("SELECT COUNT(id) AS cnt FROM " . $this->tableName);
        $this->db->query($query);
        $item = $this->db->result();

        return $item->cnt;
    }

    function getPostByPage($start = 0)
    {
        $query = sql_placeholder("SELECT id, header, body, lead
                              FROM " . $this->tableName . "
                              ORDER BY id DESC
                              LIMIT " . $start . ", 100");
        $this->db->query($query);
        $items = $this->db->results();

        return $items;
    }

    function setNewPostToHttps($id, $header, $body, $lead)
    {
        $query = sql_placeholder("UPDATE " . $this->tableName . " SET header=?, body=?, lead=? WHERE id=?", $header, $body, $lead, $id);
        return $this->db->query($query);
    }


    function getPostByTypeBlock($blockType, $id = null)
    {
        if ($blockType == 'ticker'){
            $blockType = str_ireplace('_', '/', $blockType);
            $andWhere = "AND b.blocks = '" . $blockType . "'";
        }
        else{
            $andWhere = "AND b.blocks != 'ticker'";
        }
        if (!empty($id)) {
            $andWhere .= " AND b.id = " . $id;
        }


        $query = sql_placeholder("SELECT b.id, b.url, b.name, b.header, b.lead, b.created, b.blocks,
                              b.image_1_1, b.image_1_2, b.image_1_3, b.image_rss,
                              b.format, b.font, b.citate_author_name,
                              b.amount_to_displaying, b.signature_to_sum, b.text_body,
                              b.body_color, b.spec_project, b.font_color, b.format,
                              b.type_post, bt.url AS tagUrl, bt.name AS tagName
                              ,sp.name AS sp_name, sp.header AS sp_header, sp.external_url AS sp_url
                              ,sp.image_1_1 AS sp_image_1_1, sp.image_1_2 AS sp_image_1_2, sp.image_1_3 AS sp_image_1_3
                              ,sp.type_announce_1_1 as sp_type_announce_1_1, sp.type_announce_1_2 as sp_type_announce_1_2, sp.type_announce_1_3  as sp_type_announce_1_3
                              ,b.type_announce_1_1, b.type_announce_1_2, b.type_announce_1_3,
                              bw.id AS writerId,
                              bw.name AS writersName,
                              bw.enabled AS writersEnabled,
                              bw.bloger AS blogger,
                              bw.tag_url AS bloggerUrl,
                              bw.announcement_img AS bloggerImage
                              FROM " . $this->tableName . " AS b
                              INNER JOIN blogtags AS bt ON bt.id=b.tags
                              INNER JOIN blogwriters AS bw ON bw.id = b.writers
                              LEFT JOIN spec_projects AS sp ON sp.id = b.spec_project
                              WHERE 1 = 1
                              $andWhere
                              AND b.format IS NOT NULL
                              AND b.enabled = 1

                              ORDER BY b.created DESC");
        $this->db->query($query);
        $postList = $this->db->results();

        $rowContent = array();

        foreach ($postList AS $post){

            /* spec_projects */
            if ($post->type_post == 9){
                $post->name = $post->sp_name;
                $post->header = $post->sp_header;
                $post->image_1_1 = $post->sp_image_1_1;
                $post->image_1_2 = $post->sp_image_1_2;
                $post->image_1_3 = $post->sp_image_1_3;
                $post->type_announce_1_1 = $post->sp_type_announce_1_1;
                $post->type_announce_1_2 = $post->sp_type_announce_1_2;
                $post->type_announce_1_3 = $post->sp_type_announce_1_3;
                $post->postUrl = $post->sp_url;
                $post->imageFolder = "/files/spec_project/";
                $post->tagName = "СПЕЦПРОЕКТ";
                $post->specProject = true;
                $post->writersEnabled = "1";
            }

            if  ($post->spec_project || $post->body_color) {
                $post->tagUrl = $post->sp_url;
                $post->tagName = "СПЕЦПРОЕКТ " . $post->sp_name;
            }

            /** 12 - ticker (бегущая строка) */
            if ($post->type_post == 12) {
                $post->writersEnabled = "1";
            }

            $image = $this->getImage($post, '1_3');

            switch (Helper::getTypeVisualBlock($post->type_post)) {
                case 'post':
                case 'diypost':
                case 'factday':
                case 'imageday':
                    $name = $post->name;
                    $header = $post->header;
                    break;
                case 'citate':
                    $name = $post->name;
                    $header = $post->citate_author_name;
                    break;
                default:
                    $name = $post->name;
                    $header = $post->header;
            }

            $postFormat = PresetFormat::getPresetFormatByVisualBlockAndFormat(
                Helper::getTypeVisualBlock($post->type_post),
                $post->format
            );

            $fontColors = array(
                'bgcGreen' => '#000',
                'bgcGray' => '#000',
                'bgcYellow' => '#000',
                'bgcPink' => '#000',
                'bgcBlue' => '#fff',
                'bgcRed' => '#ffbc00',
                'bgcDarkBlue' => '#ffbc00',
            );

            $backgroundColors = array(
                'bgcGray' => '#fff',
            );

            $postFont = PresetFormat::getPresetFontByKey($post->font);

            $post->frontClass = $postFormat['frontClass'] ? $postFormat['frontClass'] : "bgGray";
            $post->fontClass = $postFont['frontClass'];

            $postFormat['fontColor'] = isset($fontColors[$postFormat['frontClass']]) ? $fontColors[$postFormat['frontClass']] : '#fff';
            $postFormat['backgroundColor'] = isset($backgroundColors[$post->frontClass]) ? $backgroundColors[$post->frontClass] : $postFormat['backgroundColor'];

            $post->specProject = false;

            $post->linkParentWindowForPost = false;
            $post->linkParentWindowForTag = false;

            if ($post->type_post == 'news') {
                $post->imageFolder = "/files/news/";
            }
            /* spec_projects */
            if ($post->type_post == 9) {
                $post->name = $post->sp_name;
                $post->header = $post->sp_header;
                $post->image_1_1 = $post->sp_image_1_1;
                $post->image_1_2 = $post->sp_image_1_2;
                $post->image_1_3 = $post->sp_image_1_3;
                $post->type_announce_1_1 = $post->sp_type_announce_1_1;
                $post->type_announce_1_2 = $post->sp_type_announce_1_2;
                $post->type_announce_1_3 = $post->sp_type_announce_1_3;
                $post->url = $post->sp_url;
                $post->postUrl = $post->sp_url;
                $post->imageFolder = "/files/spec_project/";
                $post->tagUrl = $post->sp_url;
                $post->tagName = "СПЕЦПРОЕКТ";
                $post->specProject = true;
//                    if ($_SERVER['HTTP_HOST'] != parse_url($post->sp_url, PHP_URL_HOST)) {
                $post->linkParentWindowForPost = true;
                $post->linkParentWindowForTag = true;
//                    }
            } else if (in_array($post->type_post, array(1, 6, 7, 10)) && ($post->spec_project != 0 || $post->spec_project != "0")) {
                $post->tagName = "СПЕЦПРОЕКТ " . $post->sp_name;
                $post->showPartner = false;
                $post->tagUrl = $post->sp_url;
                $post->specProject = true;

                /* 6 - Мем недели */
                if ($_SERVER['HTTP_HOST'] != parse_url($post->sp_url, PHP_URL_HOST) && !in_array($post->type_post, array(6))) {
                    $post->linkParentWindowForPost = false;
                    $post->linkParentWindowForTag = true;
                    /* Карточка DIY */
                    if (in_array($post->type_post, array(10))) {
                        $post->linkParentWindowForPost = false;
                        $post->linkParentWindowForTag = false;
                    }
                }
            }

            if (in_array($post->type_post, array(10))) {
                $post->type_announce_1_1 = "picture";
                $post->type_announce_1_2 = "picture";
                $post->type_announce_1_3 = "picture";
            }

            if (parse_url($post->url, PHP_URL_HOST) && $_SERVER['HTTP_HOST'] != parse_url($post->url, PHP_URL_HOST)){
                $url = $post->sp_url;
                $post->linkParentWindowForPost = true;
                $post->linkParentWindowForTag = true;
            }
            else{
                $url = $post->tagUrl . "/" . $post->url;
                $post->linkParentWindowForPost = false;
                $post->linkParentWindowForTag = false;
            }


            $bloggerImage = null;
            if ($post->blogger && trim($post->bloggerImage)) {
                $bloggerImage  = $this->getHost() . "/files/writers/" .  $post->bloggerImage;
            }

            $writerPagePrefix = '/author/';
            $bloggerUrl = null;
            if (!empty($post->bloggerUrl)) {
                $bloggerUrl = $writerPagePrefix . $post->bloggerUrl;
            } elseif (!empty($post->writerId)) {
                $bloggerUrl = $writerPagePrefix . $post->bloggerUrl;
            }

            $rowContent[] = [
                'type_post' => $post->type_post,
                'id' => intval($post->id),
                'block' => '',
                'url' => $url, //$post->tags . "/" . $post->url,
                'name' => strip_tags($name),
                'header' => strip_tags($header),
                'date' => Helper::dateTimeWord($post->date_created),
                'image' => $image,
                'iconCode' => $post->tagUrl,
                'iconName' => $post->tagName,
                'iconBlogger' => $post->blogger,
                'iconBloggerImage' => $bloggerImage,
                'iconBloggerUrl' => $bloggerUrl,
                'iconBloggerName' => $post->writersName,
                'tagUrl' => $post->tagUrl,
                'format' => $post->format,
                'postFormat' => $postFormat,
                'font' => $post->font,
                'writersEnabled' => $post->writersEnabled,
                'typePost' => Helper::getTypeVisualBlock($post->type_post),
                'linkParentWindowForPost' => $post->linkParentWindowForPost,
                'linkParentWindowForTag' => $post->linkParentWindowForTag,
                'specProject' => $post->specProject,
                'partnerUrl' => $post->partner_url,
                'partnerName' => $post->partnerName,
                'type_announce_1_1' => $post->type_announce_1_1,
                'type_announce_1_2' => $post->type_announce_1_2,
                'type_announce_1_3' => $post->type_announce_1_3,
            ];
        }

        return $rowContent;
    }

    public function getMainPage()
    {
        $result = [];

        $query = sql_placeholder("SELECT * FROM main_page ORDER BY row_number ASC");
        $this->db->query($query);
        $mainPageItems = $this->db->results();

        foreach ($mainPageItems AS $mainPageItem){
            if ($mainPageItem->row_type === 'subscribe') {
                $mainPageItem->typePost = 'subscribe';

                $result[] = [
                    'rowNumber' => $mainPageItem->row_number,
                    'rowType' => $mainPageItem->row_type,
                    'rowContent' => array($mainPageItem)
                ];
                continue;
            } elseif ($mainPageItem->row_type === 'group') {
                $mainPageItem->typePost = 'group';
                $rowContentDecoded = json_decode($mainPageItem->row_content);
                $content = (new Plots())->getGroupById($rowContentDecoded[0]);
                $result[] = [
                    'rowNumber' => $mainPageItem->row_number,
                    'rowType' => $mainPageItem->row_type,
                    'rowContent' => [$content]
                ];
                continue;
            }

            $rowContentDecoded = json_decode($mainPageItem->row_content);

            $query = sql_placeholder("SELECT bp.id, bp.header, bp.name, bp.lead, bp.url, bp.format, bp.font, bp.url_target,
                    bp.image_1_1, bp.image_1_2, bp.image_1_3, bp.image_rss, bp.type_post, bp.partner_url,
                     bp.amount_to_displaying, bp.signature_to_sum, bp.text_body, bp.citate_author_name,
                     bp.spec_project, bp.body_color, bp.font_color,
                    DATE_FORMAT(bp.created, '%d.%m.%Y') AS date_created,bt.url AS tags, bp.is_partner_material,
                    bt.name AS tagName, bt.url AS tagUrl,
                    p.name AS partnerName,
                    bw.id AS writerId,
                    bw.name AS writersName,
                    bw.enabled AS writersEnabled,
                    bw.bloger AS blogger,
                    bw.tag_url AS bloggerUrl,
                    bw.announcement_img AS bloggerImage,
                    bp.type_announce_1_1, bp.type_announce_1_2, bp.type_announce_1_3
                              ,sp.name AS sp_name, sp.header AS sp_header, sp.external_url AS sp_url
                              ,sp.image_1_1 AS sp_image_1_1, sp.image_1_2 AS sp_image_1_2, sp.image_1_3 AS sp_image_1_3
                              ,sp.type_announce_1_1 as sp_type_announce_1_1, sp.type_announce_1_2 as sp_type_announce_1_2, sp.type_announce_1_3  as sp_type_announce_1_3
                  FROM blogposts AS bp
                  LEFT JOIN blogtags AS bt ON bt.id = bp.tags
                  INNER JOIN blogwriters AS bw ON bw.id = bp.writers
                  LEFT JOIN partners AS p ON p.id = bp.partner
                  LEFT JOIN spec_projects AS sp ON sp.id = bp.spec_project
                  WHERE bp.id IN (?@)
                  AND (bw.enabled = 1 OR bp.spec_project OR bp.type_post = 12)",
                $rowContentDecoded);

            $this->db->query($query);
            $posts = $this->db->results();

            $postList = [];
            foreach ($rowContentDecoded AS $key=>$v){
                if ($v == 'banner' ){
                    $post = new stdClass();
                    $post->row_type = 'banner';
                    $post->params = $v->params;
                    $post->typeRow = $mainPageItem->row_type;
                    $postList[$key] = $post;
                } else {
                    foreach ($posts AS $post){
                        if ($post->id == $v){
                            $postList[$key] = $post;
                        }
                    }
                }
            }


            $rowContent = [];

            foreach ($postList AS $post){

                /* spec_projects */
                if ($post->type_post == 9){
                    $post->name = $post->sp_name;
                    $post->header = $post->sp_header;
                    $post->image_1_1 = $post->sp_image_1_1;
                    $post->image_1_2 = $post->sp_image_1_2;
                    $post->image_1_3 = $post->sp_image_1_3;
                    $post->type_announce_1_1 = $post->sp_type_announce_1_1;
                    $post->type_announce_1_2 = $post->sp_type_announce_1_2;
                    $post->type_announce_1_3 = $post->sp_type_announce_1_3;
                    $post->postUrl = $post->sp_url;
                    $post->imageFolder = "/files/spec_project/";
                    $post->tagName = "СПЕЦПРОЕКТ";
                    $post->specProject = true;
                    $post->writersEnabled = "1";
                }

                if  ($post->spec_project) {
                    $post->tagUrl = $post->sp_url;
                    $post->tagName = "СПЕЦПРОЕКТ " . $post->sp_name;
                }

                /** 12 - ticker (бегущая строка) */
                if ($post->type_post == 12) {
                    $post->writersEnabled = "1";
                }

                $image = $this->getImage($post, $mainPageItem->row_type);

                switch (Helper::getTypeVisualBlock($post->type_post)) {
                    case 'post':
                    case 'diypost':
                    case 'factday':
                    case 'imageday':
                        $name = $post->name;
                        $header = $post->header;
                        break;
                    case 'citate':
                        $name = $post->name;
                        $header = $post->citate_author_name;
                        break;
                    default:
                        $name = $post->name;
                        $header = $post->header;
                }

                $postFormat = PresetFormat::getPresetFormatByVisualBlockAndFormat(
                    Helper::getTypeVisualBlock($post->type_post),
                    $post->format
                );

                $fontColors = array(
                    'bgcGreen' => '#000',
                    'bgcGray' => '#000',
                    'bgcYellow' => '#000',
                    'bgcPink' => '#000',
                    'bgcBlue' => '#fff',
                    'bgcRed' => '#ffbc00',
                    'bgcDarkBlue' => '#ffbc00',
                );

                $backgroundColors = array(
                    'bgcGray' => '#fff',
                );

                $postFont = PresetFormat::getPresetFontByKey($post->font);

                $post->frontClass = $postFormat['frontClass'] ? $postFormat['frontClass'] : "bgGray";
                $post->fontClass = $postFont['frontClass'];

                $postFormat['fontColor'] = isset($fontColors[$postFormat['frontClass']]) ? $fontColors[$postFormat['frontClass']] : '#fff';
                $postFormat['backgroundColor'] = isset($backgroundColors[$post->frontClass]) ? $backgroundColors[$post->frontClass] : $postFormat['backgroundColor'];

                $imageExists = $image ? file_exists(__DIR__.'/..'.str_replace($this->getHost(), '', $image)) : false;

                if ($mainPageItem->row_type == '1_1' && $post->type_announce_1_1 == "picture"){
                    $post->format = $imageExists ? 'picture' : 'text';
                }

                if ($mainPageItem->row_type == '1_2' && $post->type_announce_1_2 == "picture"){
                    $post->format = $imageExists ? 'picture' : 'text';
                }

                if ($mainPageItem->row_type == '1_3' && $post->type_announce_1_3 == "picture"){
                    $post->format = $imageExists ? 'picture' : 'text';
                }

                $imageMobile = $this->getImage($post, '1_3');

                $post->specProject = false;
                $post->linkParentWindowForPost = false;
                $post->linkParentWindowForTag = false;

                if ($post->type_post == 'news') {
                    $post->imageFolder = "/files/news/";
                }
                /* spec_projects */
                if ($post->type_post == 9) {
                    $post->name = $post->sp_name;
                    $post->header = $post->sp_header;
                    $post->image_1_1 = $post->sp_image_1_1;
                    $post->image_1_2 = $post->sp_image_1_2;
                    $post->image_1_3 = $post->sp_image_1_3;
                    $post->type_announce_1_1 = $post->sp_type_announce_1_1;
                    $post->type_announce_1_2 = $post->sp_type_announce_1_2;
                    $post->type_announce_1_3 = $post->sp_type_announce_1_3;
                    $post->url = $post->sp_url;
                    $post->postUrl = $post->sp_url;
                    $post->imageFolder = "/files/spec_project/";
                    $post->tagUrl = $post->sp_url;
                    $post->tagName = "СПЕЦПРОЕКТ";
                    $post->specProject = true;
//                    if ($_SERVER['HTTP_HOST'] != parse_url($post->sp_url, PHP_URL_HOST)) {
                        $post->linkParentWindowForPost = true;
                        $post->linkParentWindowForTag = true;
//                    }
                } else if (in_array($post->type_post, array(1, 6, 7, 10)) && ($post->spec_project != 0 || $post->spec_project != "0")) {
                    $post->tagName = "СПЕЦПРОЕКТ " . $post->sp_name;
                    $post->showPartner = false;
                    $post->tagUrl = $post->sp_url;
                    $post->specProject = true;

                    /* 6 - Мем недели */
                    if ($_SERVER['HTTP_HOST'] != parse_url($post->sp_url, PHP_URL_HOST) && !in_array($post->type_post, array(6))) {
                        $post->linkParentWindowForPost = false;
                        $post->linkParentWindowForTag = true;
                        /* Карточка DIY */
                        if (in_array($post->type_post, array(10))) {
                            $post->linkParentWindowForPost = false;
                            $post->linkParentWindowForTag = false;
                        }
                    }
                }

                if (in_array($post->type_post, array(10))) {
                    $post->type_announce_1_1 = "picture";
                    $post->type_announce_1_2 = "picture";
                    $post->type_announce_1_3 = "picture";
                }

                if (parse_url($post->url, PHP_URL_HOST) && $_SERVER['HTTP_HOST'] != parse_url($post->url, PHP_URL_HOST)){
                    $url = $post->sp_url;
                    $post->linkParentWindowForPost = true;
                    $post->linkParentWindowForTag = true;
                } else if (in_array($post->type_post, array(Helper::TYPE_MANUAL))) {
                    $url = "manual/" . $post->url;
                } else{
                    $url = $post->tags . "/" . $post->url;
                    $post->linkParentWindowForPost = false;
                    $post->linkParentWindowForTag = false;
                }

                // Бегущая строка
                if ($post->type_post == 12) {
                    $url = $post->url;
                }

                $bloggerImage = null;
                if ($post->blogger && trim($post->bloggerImage)) {
                    $bloggerImage  = $this->getHost() . "/files/writers/" .  $post->bloggerImage;
                }

                $writerPagePrefix = '/author/';
                $bloggerUrl = null;
                if (!empty($post->bloggerUrl)) {
                    $bloggerUrl = $writerPagePrefix . $post->bloggerUrl;
                } elseif (!empty($post->writerId)) {
                    $bloggerUrl = $writerPagePrefix . $post->bloggerUrl;
                }

                if (in_array($post->type_post, array(Helper::TYPE_PARTNER_ANNOUNCE))) {
                    $post->linkParentWindowForPost = $post->url_target === '_blank';
                    $post->linkParentWindowForTag = $post->url_target === '_blank';
                    $post->postUrl = $post->partner_url;
                    $post->url = $post->partner_url;
                    $url = $post->url;
                }

                if ($mainPageItem->row_type !== '1_3') {
                    $name = str_replace('&shy;', '',$name);
                    $header = str_replace('&shy;', '', $header);
                }

                $rowContent[] = [
                    'type_post' => $post->type_post,
                    'id' => intval($post->id),
                    'block' => $mainPageItem->row_type,
                    'url' => $url, //$post->tags . "/" . $post->url,
                    'name' => strip_tags($name),
                    'header' => strip_tags($header),
                    'date' => Helper::dateTimeWord($post->date_created),
                    'image' => $image,
                    'imageMobile' => $imageMobile,
                    'iconCode' => $post->tagUrl,
                    'iconName' => $post->tagName,
                    'iconBlogger' => $post->blogger,
                    'iconBloggerImage' => $bloggerImage,
                    'iconBloggerUrl' => $bloggerUrl,
                    'iconBloggerName' => $post->writersName,
                    'is_partner_material' => $post->is_partner_material,
                    'tagUrl' => $post->tagUrl,
                    'format' => $post->format,
                    'postFormat' => $postFormat,
                    'font' => $post->font,
                    'writersEnabled' => $post->writersEnabled,
                    'typePost' => Helper::getTypeVisualBlock($post->type_post),
                    'linkParentWindowForPost' => $post->linkParentWindowForPost,
                    'linkParentWindowForTag' => $post->linkParentWindowForTag,
                    'specProject' => $post->specProject,
                    'partnerUrl' => $post->partner_url,
                    'partnerName' => $post->partnerName,
                    'type_announce_1_1' => $post->type_announce_1_1,
                    'type_announce_1_2' => $post->type_announce_1_2,
                    'type_announce_1_3' => $post->type_announce_1_3,
                ];
            }

            $result[] = [
                'rowNumber' => $mainPageItem->row_number,
                'rowType' => $mainPageItem->row_type,
                'rowContent' => $rowContent,
            ];
        }

        return $result;
    }

    private function getHost()
    {
        $port = '';
        if (property_exists($this->config, 'port') && $this->config->port){
            $port = ':' . $this->config->port;
        }

        $host = $this->config->protocol . $this->config->host . $port;

        return $host;
    }

    private function getImage($item, $blockType)
    {
        $image = null;

        $typeFile = $item->type_post == Helper::TYPE_SPEC_PROJECT ? 'spec_project' :'blogposts';

        if ($blockType == '1_1' && $item->image_1_1 && $item->type_announce_1_1 === 'picture'){
            $image = $this->getHost() . "/files/{$typeFile}/" . $item->image_1_1;
        }

        if ($blockType == '1_2' && $item->image_1_2 && $item->type_announce_1_2 === 'picture'){
            $image = $this->getHost() . "/files/{$typeFile}/" . $item->image_1_2;
        }

        if ($blockType == '1_3' && $item->image_1_3 && $item->type_announce_1_3 === 'picture'){
            $image = $this->getHost() . "/files/{$typeFile}/" . $item->image_1_3;
        }

        return $image;
    }


    public function setMainPage($data)
    {
        $result = array();
        if (!empty($data)){

            $query = sql_placeholder("TRUNCATE TABLE main_page");
            $this->db->query($query);

            foreach ($data AS $index => $post){

                $query = sql_placeholder("INSERT IGNORE INTO main_page
                                          (`id`, `row_number`, `row_type`, `row_content`)
                                          VALUES
                                          (?, ?, ?, ?)",
                    $index + 1,
                    $post->rowNumber,
                    $post->rowType,
                    json_encode($post->rowContent)
                );
                $this->db->query($query);
            }

            $result = array(
                'result' => 'success',
                'message' => 'Данные успешно сохранены.'
            );
        }
        else{
            $result = array(
                'result' => 'error',
                'message' => 'Не переданны данные для сохранения.'
            );
        }

        return $result;
    }

    public function getMainPagePostIDs()
    {
        $query = sql_placeholder("SELECT * FROM main_page");
        $mainPageItems = $this->db->query($query);

        $mainPageItemIDs = array();
        foreach ($mainPageItems as $item) {
            $row = json_decode($item['row_content']);
            if (is_array($row)) {
                $mainPageItemIDs = array_merge($mainPageItemIDs, $row);
            }
        }
        return $mainPageItemIDs;
    }

    public function getSearchAutocomplete($searchQuery){
        $results = array();
        if (isset($this->config->indexing) && $this->config->indexing) {
            require_once './../Sphinx.class.php';
            $indexDBConfig = $this->config->sphinx;
            $indexDB = new Sphinx($indexDBConfig['name'], $indexDBConfig['host'], $indexDBConfig['user'], $indexDBConfig['password'], $indexDBConfig['port']);
            $tablePrefix = @$indexDBConfig['table_prefix'];
            $indexDB->connect();
            $indexDB->query(sql_pholder("SELECT id, model FROM {$tablePrefix}posts WHERE MATCH(?) ORDER BY WEIGHT() DESC LIMIT 20
                                         OPTION field_weights=(name=100, header=60, lead=10, body=5)", $searchQuery));
            $resultIds = $indexDB->results();

            $indexDB->disconnect();

            $newsIds = array(0);
            $postsIds = array(0);

            $models  = array(1 => 'blog', 2 => 'news');
            foreach ($resultIds as $res) {
                $itemID = $res->id;
                // 500000 Новый диапазон новостей ID для сфинкса
                if ($res->id > 500000) {
                    $newsIds [] = $res->id - 500000;
                    $itemID = $res->id - 500000;
                } else {
                    $postsIds[] = $res->id;
                }
                $modelsKey = @$models[$res->model].'_'.$itemID;
                $results[$modelsKey] = false;
            }

            $newsIdsStr = join(',', $newsIds);
            $postsIdsStr = join(',', $postsIds);

            $query = sql_placeholder("(SELECT  'blog' as type, b.id, b.blocks, b.tags,
                            CASE WHEN b.type_post = 9 THEN sp.name ELSE b.name END as name,
                            CASE WHEN b.type_post = 9 THEN sp.header ELSE b.header END as subtitle,
                            CASE WHEN b.type_post = 9 THEN sp.image_1_1 ELSE b.image_1_1 END as image_1_1,
                            CASE WHEN b.type_post = 9 THEN sp.image_1_2 ELSE b.image_1_2 END as image_1_2,
                            CASE WHEN b.type_post = 9 THEN sp.image_1_3 ELSE b.image_1_3 END as image_1_3,
                            CONCAT ('/', bt.url) AS blogTagUrl, b.url, b.type_post,
                            b.type_post, b.partner_url, b.post_tag, bw.name AS writer, bw.id AS writer_id, bw.private, bw.bloger, bw.name_genitive,
                            bw.image AS writerImage, bw.stub_image, bw.stub_link, p.name AS partner,
                            p.color AS partner_color, b.anounce_new_author, b.spec_project, b.conference,
                            b.citate_author_name,
                            b.image_rss,
                            DATE_FORMAT(b.created, '%d.%m.%Y %H:%i:%s') AS date_created,
                            bt.name AS tagName, b.format, b.font, p.name AS partnerName,
                            bw.enabled as writer_enabled
                            FROM blogposts AS b
                          LEFT JOIN blogwriters AS bw ON bw.id=b.writers
                          LEFT JOIN partners AS p ON p.id=b.partner
                          LEFT JOIN blogtags AS bt ON bt.id = b.tags
                          LEFT JOIN spec_projects AS sp ON sp.id = b.spec_project
                          WHERE  b.blocks != 'ticker' AND (b.id IN ({$postsIdsStr}))  AND b.type_post <> 12)
                      UNION ALL
                            (SELECT 'news' as type, n.id, NULL as blocks, n.tags, n.header as name, n.text_content as subtitle, CONCAT ('/news/', DATE_FORMAT(n.created, '%Y/%m/%d')) as blogTagUrl, n.url, NULL as type_post,
                            NULL as type_post, NULL as partner_url, NULL as post_tag, bw.name AS writer, bw.id AS writer_id, bw.private, bw.bloger, bw.name_genitive,
                            bw.image AS writerImage, bw.stub_image, bw.stub_link, NULL as partner,
                            NULL AS partner_color, NULL as anounce_new_author, NULL as spec_project, NULL as conference,
                            NULL as citate_author_name, NULL as image_1_1, NULL as image_1_2,  n.image_1_3,  n.image_rss,
                            DATE_FORMAT(n.created, '%d.%m.%Y') AS date_created,
                            bt.name AS tagName, NULL as format, NULL as font, NULL as partnerName,
                            bw.enabled as writer_enabled
                            FROM news AS n
                          LEFT JOIN blogwriters AS bw ON bw.id=n.writers
                          LEFT JOIN blogtags AS bt ON bt.id = n.tags
                          WHERE (n.id IN ({$newsIdsStr})))");

            $this->db->query($query);
            $searchItems = $this->db->results();

            $dirPrefix = @$this->config->fileStorage['localRootFilePath'] ? @$this->config->fileStorage['localRootFilePath']  :  '../files/';

            foreach ($searchItems AS $item){
                $imageUrl = '';
                $itemUrl = $item->blogTagUrl . '/' . $item->url;
                if ($item->type == 'blog') {
                    if (preg_match('#^(http|https)://#is', $item->url) === 1) {
                        $itemUrl = $item->url;
                    }
                    if ($item->type_post == 9) {
                        $imageUrl .= 'spec_project';
                    } else {
                        $imageUrl .= 'blogposts';
                    }
                } elseif ($item->type == 'news') {
                    $imageUrl .= 'news';
                }


                $imagePath = false;
                if (trim($item->image_rss)) {
                    $imagePath = $imageUrl . '/' . $item->image_rss;
                    $imagePath = file_exists($dirPrefix.$imagePath) ? $imagePath : false;
                } else if (trim($item->image_1_3)) {
                    $imagePath = $imageUrl . '/' . $item->image_1_3;
                    $imagePath = file_exists($dirPrefix.$imagePath) ? $imagePath : false;
                } else if (trim($item->image_1_1)) {
                    $imagePath = $imageUrl . '/' . $item->image_1_1;
                    $imagePath = file_exists($dirPrefix.$imagePath) ? $imagePath : false;
                } else if (trim($item->image_1_2)) {
                    $imagePath = $imageUrl . '/' . $item->image_1_2;
                    $imagePath = file_exists($dirPrefix.$imagePath) ? $imagePath : false;
                }

                $results[$item->type.'_'.$item->id] = array(
                    'value' => $item->type.'_'.$item->id,
                    'name' => htmlspecialchars_decode(html_entity_decode(strip_tags($item->name))),
                    'subtitle' => htmlspecialchars_decode(html_entity_decode(strip_tags($item->subtitle))),
                    'url' => $itemUrl,
                    'id' => '',
                    'content_id' => $item->id,
                    'content_type' => $item->type,
                    'type_post' => $item->type_post,
                    'image' => $imagePath && file_exists($dirPrefix.$imagePath) ?  '/files/'.$imagePath : false,
                    'imagePath' => $imagePath,
                );
            }
            $results = array_filter($results, function ($item){ return $item;});
        }

        return array_values($results);
    }
}

