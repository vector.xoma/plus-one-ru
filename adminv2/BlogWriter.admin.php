<?PHP
require_once('Widget.admin.php');
require_once('BlogTags.admin.php');
require_once('PostTags.admin.php');
require_once('Traits/TypografCicle.php');
require_once('../placeholder.php');

/**
 * Class BlogWriter
 */
class BlogWriter extends Widget
{

    use TypografCicle;

    var $item;
    var $uploaddir = '../files/writers/'; # Папка для хранения картинок (default)
    var $large_image_width = "600";
    var $large_image_height = "600";
    private $tableName = 'blogwriters';

    function BlogWriter(&$parent){
        Widget::Widget($parent);
        $this->prepare();
    }

    function prepare(){
        $item_id = intval($this->param('item_id'));

        if(isset($_POST['name'])){
            $this->check_token();

            $this->item->name = $_POST['name'];

            $this->item->enabled = 0;
            if(isset($_POST['enabled']) && $_POST['enabled']==1) {
                $this->item->enabled = 1;
            }

            $this->item->private = 0;
            if(isset($_POST['private']) && $_POST['private']==1) {
                $this->item->private = 1;
            }

            $this->item->bloger = 0;
            if(isset($_POST['bloger']) && $_POST['bloger']==1) {
                $this->item->bloger = 1;
            }

            $this->item->tag_url = @$_POST['tag_url'] ? $_POST['tag_url'] : null;
            $this->item->name_genitive = $_POST['name_genitive'];
            $this->item->description = $_POST['description'];

            $this->item->leader = $_POST['leader'];
            $this->item->body = $_POST['body'];

            $this->item->stub_link = $_POST['stub_link'];

            $this->item->seo_title = $_POST['seo_title'];
            $this->item->seo_description = $_POST['seo_description'];
            $this->item->seo_keywords = $_POST['seo_keywords'];


            $this->item->useplatforma = 0;
            if(isset($_POST['useplatforma']) && $_POST['useplatforma']==1) {
                $this->item->useplatforma = 1;
            }

            ## Не допустить одинаковые имена Авторов
    	    $query = sql_placeholder('SELECT COUNT(*) AS `count` FROM ' . $this->tableName . '
    	                               WHERE `name`=? AND `id` != ? AND `useplatforma` = ?'
                                        , $this->item->name, $item_id, $this->item->useplatforma);
            $this->db->query($query);
            $res = $this->db->result();

            $tagUrlExists = false;
            if ($this->item->tag_url) {
                $query = sql_placeholder('SELECT COUNT(*) AS `count` FROM  blogwriters_url_history WHERE `url` = ? AND `writers` != ?', $this->item->tag_url, $item_id);
                $this->db->query($query);
                $tagUrlHistories = $this->db->result();
                if ($tagUrlHistories->count > 0) {
                    $this->error_msg = "С Таким URL уже используется, (или использовался в ранее для другого автора)";
                    $tagUrlExists = true;
                }
            }

  		    if(empty($this->item->name)){
			    $this->error_msg = $this->lang->ENTER_TITLE;
  		    }
  		    elseif($res->count > 0){
			    $this->error_msg = 'Писатель с таким названием уже существует. Укажите другое.';
  		    }
            else{
                $this->beforeTypograf($this->item, ['description',]);
                $this->typografFields($this->item, [], ['description']);
                $this->afterTypograf($this->item, ['description']);
  			    if(empty($item_id)) {
                    $this->item->image = null;
                    $item_id = $this->add_article();
                    if (is_null($item_id)){
                        $this->error_msg = 'Ошибка при создании записи.';
                    }
                }
  	    		else{
                    if (is_null($this->update_article($item_id))){
                        $this->error_msg = 'Ошибка при сохранении записи.';
                    }
                }
                if (@$_POST['delete_large_image'] === "1" && !empty($item_id)) {
                    $this->db->query("UPDATE {$this->tableName} SET image = '' WHERE id={$item_id}");
                }
                if (@$_POST['delete_stub_image'] === "1" && !empty($item_id)) {
                    $this->db->query("UPDATE {$this->tableName} SET stub_image=NULL WHERE id={$item_id}");
                }
                if (@$_POST['delete_announcement_img'] === "1" && !empty($item_id)) {
                    $this->db->query("UPDATE {$this->tableName} SET announcement_img=NULL WHERE id={$item_id}");
                }
                $this->add_fotos($item_id);


  	    		if (!$tagUrlExists && $this->item->tag_url) {
                    $query = sql_placeholder('SELECT COUNT(*) as `count` FROM  blogwriters_url_history WHERE `url` = ? AND `writers` = ?', $this->item->tag_url, $item_id);
                    $this->db->query($query);
                    $histories = $this->db->result();
                    if ($histories->count == 0) {
                        $this->add_history_url($item_id);
                    }
                }

                $get = $this->form_get(array('section'=>'BlogWriters'));

                if (empty($this->error_msg)){
                    if (@$_POST['apply']) {
                        $_SESSION['flush']['BlogWriter'] = "Успешно сохранено!";
                        header("Location: " . $_SERVER['HTTP_REFERER']);
                        exit(0);
                    }
                    if(isset($_GET['from'])){
                        header("Location: ".$_GET['from']);
                        exit(0);
                    }
                    else{
                        header("Location: index.php$get");
                        exit(0);
                    }
                }
  		    }
  	    }
  	    elseif (!empty($item_id)){
		    $query = sql_placeholder('SELECT * FROM ' . $this->tableName . ' WHERE id=?', $item_id);
		    $this->db->query($query);
		    $this->item = $this->db->result();
  	    }
    }

	function fetch()
	{
		if(empty($this->item->id)){
			$this->title = 'Новый автор';
		}
		else{
			$this->title = 'Изменение автора: ' . $this->item->name;
		}


        $blogTagsClass = new BlogTags();

        $blogTags = $blogTagsClass->getTags();

        foreach ($blogTags AS $k=>$blogTag){
            $query = sql_placeholder("SELECT id, name_lead FROM post_tags WHERE parent=? ORDER BY name ASC", $blogTag->id);
            $this->db->query($query);
            $blogTags[$k]->items = $this->db->results();
        }

//        echo "<pre>";
//        print_r($this->item);
//        echo "</pre>";

        $this->smarty->assign('title', $this->title);
        $this->smarty->assign('Item', $this->item);
        $this->smarty->assign('blogTags', $blogTags);
		$this->smarty->assign('Error', $this->error_msg);
		$this->smarty->assign('Lang', $this->lang);
        $this->smarty->assign('images_uploaddir', $this->uploaddir);
        $this->smarty->assign('flash', $_SESSION['flush']['BlogWriter']);
        unset($_SESSION['flush']['BlogWriter']);


		$this->body = $this->smarty->fetch('blogwriter.tpl');
	}

    /**
     * @return int|null
     */
    function add_article(){
        $query = sql_placeholder('INSERT INTO ' . $this->tableName . ' SET ?%', (array)$this->item);

        if ($this->db->query($query)){
            $item_id = $this->db->insert_id();

            $query = sql_placeholder('UPDATE ' . $this->tableName . ' SET order_num=id WHERE id=?', $item_id);
            $this->db->query($query);

            return $item_id;
        }
        else{
            return null;
        }
    }

    /**
     * @param $item_id
     * @return null
     */
    function update_article($item_id){
        $query = sql_placeholder('UPDATE ' . $this->tableName . ' SET ?% WHERE id=?', (array)$this->item, $item_id);

        if ($this->db->query($query)){
            return $item_id;
        }
        else{
            return null;
        }
    }

    /**
     * метод добавлдяет фото к текущей статье
     * @param $article_id
     * @return bool
     */
    function add_fotos($itemId){
        $result = false;

        $dateFolder = '' . (new DateTime('now'))->format('Y/m') . '/';

        $largeuploadfile = $dateFolder. $itemId.".jpg";
        $stubImage = $dateFolder. $itemId . '_stub_image.jpg';
        $announcementImage = $dateFolder. $itemId . '_announcement_img.jpg';

        $forOptimized = array();
        $this->mkdir_custom($this->uploaddir . $dateFolder);

        if(isset($_FILES['large_image']) && !empty($_FILES['large_image']['tmp_name'])){
            if (!move_uploaded_file($_FILES['large_image']['tmp_name'], $this->uploaddir.$largeuploadfile)){
                $this->error_msg = 'Ошибка при загрузке файла';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET image='$largeuploadfile' WHERE id={$itemId}");
                $result = true;
                $forOptimized[] = array(realpath($this->uploaddir.$largeuploadfile), $largeuploadfile);
            }
        }

        if(isset($_FILES['stub_image']) && !empty($_FILES['stub_image']['tmp_name'])){

            if (!move_uploaded_file($_FILES['stub_image']['tmp_name'], $this->uploaddir.$stubImage)){
                $this->error_msg = 'Ошибка при загрузке файла';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET stub_image='$stubImage' WHERE id={$itemId}");
                $result = true;
                $forOptimized[] = array(realpath($this->uploaddir.$stubImage), $stubImage);
            }
        }

        if(isset($_FILES['announcement_img']) && !empty($_FILES['announcement_img']['tmp_name'])){

            if (!move_uploaded_file($_FILES['announcement_img']['tmp_name'], $this->uploaddir.$announcementImage)){
                $this->error_msg = 'Ошибка при загрузке файла';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET announcement_img='$announcementImage' WHERE id={$itemId}");
                $result = true;
                $forOptimized[] = array(realpath($this->uploaddir.$announcementImage), $announcementImage);
            }
        }

        if ($this->config->tinypng['optimized']) {
            $projectDir = realpath(__DIR__.'/../');
            $nowDate = date('Y-m-d H:i:s');
            foreach ($forOptimized as $image) {
                $fileSaveLog = '/files/writers/'.$image[1];
                $this->db->query(sql_placeholder("INSERT INTO optimizer_log (item_id, item_type, image, status, created, `size`) VALUE (?, 'writer', ?, 0, ?, ?)"
                    , $itemId, $fileSaveLog, $nowDate, (int)@filesize($projectDir.$fileSaveLog)));
            }
        }

        return $result;
    }

    /**
     * метод добавлдяет исотрию URL к текущему автору
     * @param $itemId
     * @return bool
     * @throws Exception
     */
    function add_history_url($itemId){
        if ($this->item->tag_url) {
            $query = sql_placeholder('INSERT INTO blogwriters_url_history SET ?%', array(
                'url' => $this->item->tag_url,
                'writers' => $itemId,
                'created' => (new DateTime('now'))->format('Y-m-d H:i:s'),
                'modified' => (new DateTime('now'))->format('Y-m-d H:i:s')
            ));
            if ($this->db->query($query)){
                return true;
            } else {
                return false;
            }
        }
    }
}
