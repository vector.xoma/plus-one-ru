<?PHP



/**
 * Class Permission
 */
class FileSystem
{
    private $filearray = array();
    private $directory;
    private $extensions = array("jpg", "jpeg", "gif", "png");

    public function __construct($directory) {

        $this->directory = $directory;

        if (is_dir($directory)) {
            $d = opendir($directory) or die("Failed to open directory.");
            while ( false !== ($f = readdir($d)) ){
                if (is_file($directory.'/'.$f)){
                    $this->filearray[] = $f;
                }
            }
            closedir($d);
        }
        else {
            die("Must pass in a directory.");
        }
    }

    public function __destruct(){
        unset($this->filearray);
    }

    public function getDirectoryName(){
        return $this->directory;
    }

    public function indexOrder($typeSort = 'asc'){
//        if ($typeSort == 'asc'){
//            ksort($this->filearray);
//        }
//        elseif($typeSort == 'desc'){
//            krsort($this->filearray);
//        }

        sort($this->filearray);
    }

    public function getCount() {
        return count($this->filearray);
    }

    public function getFileArray() {
        return $this->filearray;
    }

    public function getFileArraySlice($start, $numberitems) {
        return array_slice($this->filearray, $start, $numberitems);
    }

    // исключить из массива все элементы с недопустимым расширением
    public function filter(){

        foreach ($this->filearray as $key => $value) {
            $ext = substr($value,(strpos($value, ".")+1));
            $ext =  strtolower($ext);
            if(!in_array($ext, $this->extensions)){
                unset($this->filearray[$key]);
            }
        }
    }
}