<?PHP

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '180' );

require_once('Widget.admin.php');
require_once('BlogPosts.admin.php');
require_once('BlogTags.admin.php');
require_once('PostTags.admin.php');
require_once('BlogWriters.admin.php');
require_once ('../PresetFormat.class.php');
require_once('../placeholder.php');
require_once('RemoteTypograf.php');
require_once('./Traits/TypografCicle.php');

/**
 * Class NewsItem
 */
class NewsItem extends Widget
{
    use TypografCicle;

    /** @var   */
    public $errors;
    var $item;
    var $uploaddir = '../files/news/'; # Папка для хранения картинок (default)
    /** @var string  */
    public $tmpImgDir =  '../files/news/tmp_img/';
    var $large_image_width = "600";
    var $large_image_height = "600";
    private $tableName = 'news';
    private $descriptionLength = 640;
    private $imageWidth = 906;

    public $preview = false;
    public $articleUrl = "";

    function NewsItem(&$parent){
        Widget::Widget($parent);
        $this->prepare();
    }

    function prepare(){
        $itemId = intval($this->param('item_id'));

//        $this->check_token();
        if(isset($_POST['name'])){

            $action = $_POST['save'];

            // создаем объект из переданных данных
            $this->setData($_POST, $itemId);
            // проверка на обязательные поля
            $this->checkRequiredFields();
            // проверка на уникальные URL
            $this->checkUniquieUrl($this->item->id, $this->item->url);

            if (empty($this->error_msg) && count(get_object_vars($this->errors)) == 0) {

                $this->db->query(sql_placeholder("SELECT url FROM news n WHERE n.id =?", $itemId));
                $oldURL = $this->db->result();
                $this->beforeTypograf($this->item, ['article_text', 'text_content', 'lead']);
                $this->typografFields($this->item, [], [ 'header', 'meta_title', 'meta_description', 'header_social', 'image_rss_description', 'image_rss_source', 'article_text', 'text_content', 'lead']);
                $this->afterTypograf($this->item, ['article_text', 'text_content', 'lead']);

                $this->item->header = $this->hyphenate($this->item->header);

                if(empty($itemId)) {
                    $this->item->modified = date('Y-m-d H:i:s', time());

                    $itemId = $this->add_article();

                    if (is_null($itemId)){
                        $this->error_msg = 'Пост не сохранен. Ошибка при сохранении записи.';
                    }
                }
                else{
                    $this->item->modified = date('Y-m-d H:i:s', time());

                    if (is_null($this->update_article($itemId))){
                        $this->error_msg = 'Пост не обновлён. Ошибка при сохранении записи.';
                    }
                }

//                // партнерские ссылки-посты
//                if (!empty($_POST['partnerlink'])){
//                    $this->addRelatedPartnersPosts($itemId, $_POST['partnerlink']);
//                }
//
//                //
                $this->setRelationsNewsItemTags($_POST['post_tags'], $itemId);
//                // добавляем данные для аналитики
//                if ($_POST['value_to_yeats']){
//                    // записываем данные аналитики в бд
//                    $this->setWritersData($itemId, $this->item->writers, $_POST);
//                }

                // загрузка картинки
                $this->add_fotos($itemId);


                // История URL поста
                if ($oldURL && $oldURL->url && $oldURL->url !== $this->item->url) {
                    if (!$this->add_url_history($itemId, $oldURL->url)) {
                        $this->error_msg = "URL записи: уже был использован ранее";
                    }
                }

                if (empty($this->error_msg) && count(get_object_vars($this->errors)) == 0) {

                    if (isset($this->config->indexing) && $this->config->indexing) {
                        $newsObject = new stdClass();
                        $newsObject->id = $itemId;
                        $newsObject->writers = (int)$this->item->writers;
                        $newsObject->enabled = (int)$this->item->enabled;
                        $newsObject->created = trim($this->item->created);
                        $newsObject->name = trim($this->item->name);
                        $newsObject->header = trim($this->item->header);
                        $newsObject->lead = trim($this->item->lead);
                        $newsObject->text_content = trim($this->item->text_content);
                        $newsObject->article_text = trim($this->item->article_text);
                        $newsObject->author_name = trim($this->item->author_name);
                        $this->addNewsSearchIndexSphinx($newsObject);
                    }

                    $this->clearSsrCache($this->item);

                    if ($action == 'save'){
                        header("Location: /adminv2/index.php?section=NewsItems");
                        exit();
                    }
                    else if ($action == 'apply'){
                        if (!empty($itemId)) {
                            $redirectURL = "/adminv2/news/edit/{$itemId}/{$_REQUEST['token']}";
                            header("Location: {$redirectURL}" );
                            exit();
                        }
                    }
                    elseif ($action == 'preview'){
                        $query = sql_placeholder("SELECT *, DATE_FORMAT(created, '%d.%m.%Y %H:%i') AS date_created, CONCAT ('news/', DATE_FORMAT(created, '%Y/%m/%d')) AS tagUrl FROM " . $this->tableName . " WHERE id=?", $itemId);
                        $this->db->query($query);
                        $this->item = $this->db->result();

                        $this->articleUrl = "/" . $this->item->tagUrl . "/" . $this->item->url;

                        $this->preview = true;
                    }
                    else {
                        $query = sql_placeholder("SELECT *, DATE_FORMAT(created, '%d.%m.%Y %H:%i') AS date_created FROM " . $this->tableName . " WHERE id=?", $itemId);
                        $this->db->query($query);
                        $this->item = $this->db->result();
                    }
                }
            } else {
                // сохраняем изображение во избежании его потери по причине возникновения ошибки
                $this->imagesPresave();
            }
            header("X-XSS-Protection: 0");
  	    }
  	    elseif (!empty($itemId)){
		    $query = sql_placeholder('SELECT * FROM ' . $this->tableName . ' WHERE id=?', $itemId);
		    $this->db->query($query);
		    $this->item = $this->db->result();
  	    }
    }

    function setData($data, $itemId)
    {

        $this->item->id = $itemId;
        $this->item->header = $data['name'];
        $this->item->header_social = $data['header_rss'];
        $this->item->created = date('Y-m-d H:i:s', strtotime($data['created']));
        $this->item->url = $data['url'];
        $this->item->author_signature = $data['author_signature'];
        $this->item->author_name = $data['author_name'];
        $this->item->text_content = $data['header'];
        $this->item->lead = $data['lead'];
        $this->item->article_text = $data['body'];
        $this->item->meta_title = $data['meta_title'];
        $this->item->meta_description = $data['meta_description'];
        $this->item->meta_keywords = $data['meta_keywords'];
        $this->item->image_rss_description = $data['image_rss_description'];
        $this->item->image_rss_source = $data['image_rss_source'];
        $this->item->form_id = $data['form_id'];
        $this->item->format = $data['format'];

        $this->item->writers = 0;
        if (isset($data['writer'])){
            $this->item->writers = $data['writer'];
        }

        $this->item->enabled = 0;
        if(isset($data['enabled']) && $data['enabled']==1) {
            $this->item->enabled = 1;
        }

        $this->item->publish_news_aggregators = 0;
        if(isset($data['publish_news_aggregators']) && $data['publish_news_aggregators']==1) {
            $this->item->publish_news_aggregators = 1;
        }

        $this->item->publish_news_zen_pulse = 0;
        if(isset($data['publish_news_zen_pulse']) && $data['publish_news_zen_pulse']==1) {
            $this->item->publish_news_zen_pulse = 1;
        }

        $this->item->is_partner_material = 0;
        if(!empty($_POST['is_partner_material'])) {
            $this->item->is_partner_material = $data['is_partner_material'];
        }

        $this->item->tags = 0;
        if (isset($data['tags'])){
            $this->item->tags = $data['tags'];
        }

        $this->item->post_tag = 0;
        if (isset($_POST['post_tag_main'])){
            $this->item->post_tag = $_POST['post_tag_main'];
        }

        $this->item->type_announce_1_1 = "text";
        if (isset($data['type_announce_1_1'])){
            $this->item->type_announce_1_1 = $data['type_announce_1_1'];
        }

        $this->item->type_announce_1_2 = "text";
        if (isset($data['type_announce_1_2'])){
            $this->item->type_announce_1_2 = $data['type_announce_1_2'];
        }

        $this->item->type_announce_1_3 = "text";
        if (isset($data['type_announce_1_3'])){
            $this->item->type_announce_1_3 = $data['type_announce_1_3'];
        }

        $this->item->feed_header_html = @$data['header_html_for_feed'];
        $this->item->feed_lead_html =   @$data['lead_html_for_feed'];
        $this->item->feed_body_html =   @$data['body_html_for_feed'];

        $this->item->text_content = preg_replace(Widget::PREG_REPLACE_HOST, '"', $this->item->text_content);
        $this->item->lead = preg_replace(Widget::PREG_REPLACE_HOST, '"', $this->item->lead);
        $this->item->article_text = preg_replace(Widget::PREG_REPLACE_HOST, '"', $this->item->article_text);

        $this->smarty->assign('post_tags', $data['post_tags']);
    }

    function checkRequiredFields(){
        $validations = array(
            'meta_title' => array(
                'maxLength' => array(
                    'length' => 100,
                    'error' => 'Превышено максимальное количество символов в поле \'Метатег Title\' (100 символов)')
            ),
            'meta_description' => array(
                'maxLength' => array(
                    'length' => $this->descriptionLength,
                    'error' => 'Пост не сохранен. Превышено максимальное количество символов в поле \'Метатег Description\' (' . $this->descriptionLength . ' символов)')
            ),
            'meta_keywords' => array(
                'maxLength' => array(
                    'length' => 1024,
                    'error' => 'Превышено максимальное количество символов в поле \'Метатег Keywords\' (170 символов)')
            ),
            'url' => array(
                'maxLength' => array(
                    'length' => 255,
                    'error' => 'Превышено максимальное количество символов в поле \'URL\' (255 символов)')
            ),
            'header' => array(
                'maxLength' => array(
                    'length' => 100,
                    'error' => 'Превышено максимальное количество символов в поле \'Заголовок\' (100 символов)')
            ),
            'header_social' => array(
                'maxLength' => array(
                    'length' => 140,
                    'error' => 'Превышено максимальное количество символов в поле \'Заголовок для социальных сетей\' (140 символов)')
            )
        );

        $this->errors = new stdClass();

        if (empty($this->item->header) || empty($this->item->header_social)
            || $this->item->created == '1970-01-01 00:00:00' || $this->item->writers == 0
            || empty($this->item->url) || $this->item->tags == 0){
            $this->error_msg = "Пост не сохранен. Заполните обязательные поля, отмеченные красным цветом!";
        }

        foreach ($validations as $field => $rules) {
            foreach ($rules as $rule => $options) {
                if ($rule === "maxLength") {
                    if (@iconv_strlen($this->item->{$field}, "UTF-8") > $options['length']) {
                        $this->errors->{$field} = $options['error'];
                    }
                }
            }
        }

    }

    function checkUniquieUrl($itemId, $itemUrl){
        if (!trim($itemUrl)) {return true;}
        $query = sql_placeholder('SELECT COUNT(*) AS `count` FROM ' . $this->tableName . ' WHERE url=? AND id!=?', $itemUrl, $itemId);
        $this->db->query($query);
        $res = $this->db->result();
        if($res->count>0){
            $this->errors->url = 'Пост не сохранен. Запись с таким URL уже существует. Укажите другой.';
            return false;
        }

        $query = sql_placeholder('SELECT COUNT(*) AS `count` FROM news_url_history WHERE url=? AND news_id!=?', $itemUrl, $itemId);
        $this->db->query($query);
        $res = $this->db->result();
        if($res->count > 0){
            $this->errors->url = 'Пост не сохранен. Запись с таким URL уже существует в истории. Укажите другой.';
            return false;
        }
        return true;
    }

	function fetch()
	{
		if(empty($this->item->id)){
			$this->title = 'Новая запись';
			$this->item->publish_news_aggregators = 1;
            $this->item->publish_news_zen_pulse = 1;
            $this->item->date_created = date('d.m.Y H:i');
		}
		else{
			$this->title = 'Изменение записи: ' . $this->item->header;
            $this->item->date_created =  (new DateTime( $this->item->created))->format('d.m.Y H:i');
		}
        $this->item->form_id = !empty($this->item->form_id) ? $this->item->form_id : uniqid();

        $bw = new BlogWriters();
        $writers = $bw->getWriters();

        foreach ($writers as $key=>$writer) {
            if ($writer->useplatforma == 1){
                $writers[$key]->name = "Платформа: " . $writers[$key]->name;
            }
        }

        $bt = new BlogTags();
        $tags = $bt->getTags();

        $blogTags = $tags;

        foreach ($blogTags AS $k=>$blogTag){
            $query = sql_placeholder("SELECT id, name FROM post_tags WHERE parent=? ORDER BY name ASC", $blogTag->id);
            $this->db->query($query);
            $blogTags[$k]->items = $this->db->results();
        }

        $userLogin = $_SESSION['adminarea']['login'];
        if (!empty($userLogin)){
            $query = sql_placeholder("SELECT name FROM users_admin WHERE login=?", $userLogin);
            $this->db->query($query);
            $userInfo = $this->db->result();
            $this->smarty->assign('currentUserName', $userInfo->name);
        }

        $query = sql_placeholder('SELECT post_tag_id FROM news_to_tags_relations WHERE news_id=?', $this->item->id);
        $this->db->query($query);
        $selectedTags = $this->db->results();
        $selectTags = array();
        if (is_array($selectedTags)) {
            foreach ($selectedTags AS $selectedTag) {
                $selectTags[$selectedTag->post_tag_id] = $selectedTag->post_tag_id;
            }
        }

        foreach ($tags AS $key=>$tag){
            foreach ($tag->items AS $k=>$v){
                if (in_array($v->id, (array)$selectTags)){
                    $tags[$key]->items[$k]->check = 1;
                }
                else {
                    $tags[$key]->items[$k]->check = 0;
                }
            }

        }

        $postsClass = new BlogPosts();
        $linkedPosts = $postsClass->getLinkedPost($this->item->id, 'news');
        $this->smarty->assign('linkedPosts', $linkedPosts);

        $this->smarty->assign('presetFormat', PresetFormat::getPresetFormats('post'));
        $this->smarty->assign('presetFont',  PresetFormat::getPresetFont());
        $this->smarty->assign('title', $this->title);
        $this->smarty->assign('tags', $tags);
        $this->smarty->assign('blogTags', $blogTags);
        $this->smarty->assign('writers', $writers);
        $this->smarty->assign('Item', $this->item);
		$this->smarty->assign('Error', $this->error_msg);
		$this->smarty->assign('Errors', $this->errors);
		$this->smarty->assign('Lang', $this->lang);
        $this->smarty->assign('images_uploaddir', $this->uploaddir);
        $this->smarty->assign('descriptionLength', $this->descriptionLength);
        $this->smarty->assign('preview', $this->preview);
        $this->smarty->assign('articleUrl', $this->articleUrl);
        if(!isset($this->smarty->_tpl_vars['post_tags'])) {
            $this->smarty->assign('post_tags', array());
        }

		$this->body = $this->smarty->fetch('newsitem.tpl');
	}

    /**
     * @return int|null
     */
    function add_article(){
        $data = (array)$this->item;
        unset($data['form_id']);
        $query = sql_placeholder('INSERT INTO ' . $this->tableName . ' SET ?%', $data);

        if ($this->db->query($query)){
            $item_id = $this->db->insert_id();

            $query = sql_placeholder('UPDATE ' . $this->tableName . ' SET order_num=id WHERE id=?', $item_id);
            $this->db->query($query);

            return $item_id;
        }
        else{
            $this->errors->errors = join('<br/>',  $this->db->errors );
            return null;
        }
    }

    /**
     * @param $item_id
     * @return null
     */
    function update_article($item_id){
        $data = (array)$this->item;
        unset($data['form_id']);
        $query = sql_placeholder('UPDATE ' . $this->tableName . ' SET ?% WHERE id=?', $data, $item_id);

        if ($this->db->query($query)){
            return $item_id;
        } else {
            $this->errors->errors = join('<br/>',  $this->db->errors );
            return null;
        }
    }

    /**
     * метод добавлдяет фото к текущей статье
     * @param $itemId
     * @return bool
     * @throws Exception
     */
    function add_fotos($itemId){
        $result = false;

        $dateFolder = '' . (new DateTime($this->item->created))->format('Y/m') . '/';

        $image_1_1 = $dateFolder . $itemId . '-1_1.jpg';
        $image_1_2 = $dateFolder . $itemId . '-1_2.jpg';
        $image_1_3 = $dateFolder . $itemId . '-1_3.jpg';
        $image_rss = $dateFolder . $itemId . '-rss.jpg';
        $image_origin = $dateFolder. $itemId . '-origin.jpg';

        $image_1_1_tmp = $this->item->form_id . '-1_1.jpg';
        $image_1_2_tmp = $this->item->form_id . '-1_2.jpg';
        $image_1_3_tmp = $this->item->form_id . '-1_3.jpg';
        $image_rss_tmp = $this->item->form_id . '-rss.jpg';
        $image_origin_tmp = $this->item->form_id . '-origin.jpg';

        $forOptimized = array();

        if (!$this->mkdir_custom($this->uploaddir.$dateFolder)) {$this->error_msg = 'Ошибка при создании каталога год/месяц';}

        if(isset($_FILES['image_1_1']) && !empty($_FILES['image_1_1']['tmp_name'])){
            if (file_exists($this->tmpImgDir . $image_1_1_tmp)) {
                unlink($this->tmpImgDir . $image_1_1_tmp);
            }
            if (!move_uploaded_file($_FILES['image_1_1']['tmp_name'], $this->uploaddir.$image_1_1)){
                echo $this->uploaddir.$image_1_1;
                $this->error_msg = 'Ошибка при загрузке файла 1/1';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET image_1_1='$image_1_1' WHERE id={$itemId}");
                $result = true;
                $forOptimized[] = array(realpath($this->uploaddir.$image_1_1), $image_1_1);
            }
        } elseif (file_exists($this->tmpImgDir . $image_1_1_tmp) && rename($this->tmpImgDir . $image_1_1_tmp, $this->uploaddir.$image_1_1)) {
            $this->db->query("UPDATE {$this->tableName} SET image_1_1='$image_1_1' WHERE id={$itemId}");
            $result = true;
            $forOptimized[] = array(realpath($this->uploaddir.$image_1_1), $image_1_1);
        }

        if(isset($_FILES['image_1_2']) && !empty($_FILES['image_1_2']['tmp_name'])){
            if (file_exists($this->tmpImgDir . $image_1_2_tmp)) {
                unlink($this->tmpImgDir . $image_1_2_tmp);
            }
            if (!move_uploaded_file($_FILES['image_1_2']['tmp_name'], $this->uploaddir.$image_1_2)){
                $this->error_msg = 'Ошибка при загрузке файла 1/1';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET image_1_2='$image_1_2' WHERE id={$itemId}");
                $result = true;
                $forOptimized[] = array(realpath($this->uploaddir.$image_1_2), $image_1_2);
            }
        } elseif (file_exists($this->tmpImgDir . $image_1_2_tmp) && rename($this->tmpImgDir . $image_1_2_tmp, $this->uploaddir.$image_1_2)) {
            $this->db->query("UPDATE {$this->tableName} SET image_1_2='$image_1_2' WHERE id={$itemId}");
            $result = true;
            $forOptimized[] = array(realpath($this->uploaddir.$image_1_2), $image_1_2);
        }

        if(isset($_FILES['image_1_3']) && !empty($_FILES['image_1_3']['tmp_name'])){
            if (file_exists($this->tmpImgDir . $image_1_3_tmp)) {
                unlink($this->tmpImgDir . $image_1_3_tmp);
            }
            if (!move_uploaded_file($_FILES['image_1_3']['tmp_name'], $this->uploaddir.$image_1_3)){
                $this->error_msg = 'Ошибка при загрузке файла 1/3';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET image_1_3='$image_1_3' WHERE id={$itemId}");
                $result = true;
                $forOptimized[] = array(realpath($this->uploaddir.$image_1_3), $image_1_3);
            }
        } elseif (file_exists($this->tmpImgDir . $image_1_3_tmp) && rename($this->tmpImgDir . $image_1_3_tmp, $this->uploaddir.$image_1_3)) {
            $this->db->query("UPDATE {$this->tableName} SET image_1_3='$image_1_3' WHERE id={$itemId}");
            $result = true;
            $forOptimized[] = array(realpath($this->uploaddir.$image_1_3), $image_1_3);
        }

        if(isset($_FILES['image_rss']) && !empty($_FILES['image_rss']['tmp_name'])){
            if (file_exists($this->tmpImgDir . $image_rss_tmp)) {
                unlink($this->tmpImgDir . $image_rss_tmp);
            }
            if (file_exists($this->tmpImgDir . $image_origin_tmp)) {
                unlink($this->tmpImgDir . $image_origin_tmp);
            }
            $imgPath = $_FILES['image_rss']['tmp_name'];
            list($widthOrigin, $heightOrigin) = getimagesize($imgPath);
            $k = $widthOrigin / $heightOrigin;
            $img = $this->resize_image($imgPath, $this->imageWidth, ($this->imageWidth / $k));
            if (!imagejpeg($img, $this->uploaddir.$image_rss) || !move_uploaded_file($imgPath, $this->uploaddir.$image_origin)){
                $this->error_msg = 'Ошибка при загрузке файла RSS';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET image_rss='$image_rss' WHERE id={$itemId}");
                $this->db->query("UPDATE {$this->tableName} SET origin_image='$image_origin' WHERE id={$itemId}");
                $result = true;
                $forOptimized[] = array(realpath($this->uploaddir.$image_rss), $image_rss);
            }
        } elseif (file_exists($this->tmpImgDir . $image_rss_tmp) && rename($this->tmpImgDir . $image_rss_tmp, $this->uploaddir.$image_rss)) {
            $this->db->query("UPDATE {$this->tableName} SET image_rss='$image_rss' WHERE id={$itemId}");
            if (file_exists($this->tmpImgDir . $image_origin_tmp) && rename($this->tmpImgDir . $image_origin_tmp, $this->uploaddir.$image_origin)) {
                $this->db->query("UPDATE {$this->tableName} SET origin_image='$image_origin' WHERE id={$itemId}");
            }
            $result = true;
            $forOptimized[] = array(realpath($this->uploaddir.$image_rss), $image_rss);
        }


        if ($this->config->tinypng['optimized']) {
            $projectDir = realpath(__DIR__.'/../');
            $nowDate = date('Y-m-d H:i:s');
            foreach ($forOptimized as $image) {
                $fileSaveLog = '/files/news/'.$image[1];
                $this->db->query(sql_placeholder("INSERT INTO optimizer_log (item_id, item_type, image, status, created, `size`) VALUE (?, 'news', ?, 0, ?, ?)"
                    , $itemId, $fileSaveLog, $nowDate, (int)@filesize($projectDir.$fileSaveLog)));
            }
        }

        return $result;
    }

    function resize_image($file, $widthFinal, $heightFinal) {
        $imgInfo = getimagesize($file);
        $widthOrigin = $imgInfo[0];
        $heightOrigin = $imgInfo[1];
        switch ($imgInfo['mime']) {
            case 'image/png':
                $src = imagecreatefrompng($file);
                break;
            case 'image/gif':
                $src = imagecreatefromgif($file);
                break;
            default:
                $src = imagecreatefromjpeg($file);
        }
        $dst = imagecreatetruecolor($widthFinal, $heightFinal);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $widthFinal, $heightFinal, $widthOrigin, $heightOrigin);
        return $dst;
    }

    /**
     * метод сохраняет изображения к посту ещё до сохранения самого поста
     * @return bool
     */
    function imagesPresave(){
        $image_1_1 = $this->item->form_id . '-1_1.jpg';
        $image_1_2 = $this->item->form_id . '-1_2.jpg';
        $image_1_3 = $this->item->form_id . '-1_3.jpg';
        $image_rss = $this->item->form_id . '-rss.jpg';
        $image_origin = $this->item->form_id . '-origin.jpg';

        $tmpFolder = basename($this->tmpImgDir);

        if (!empty($_FILES['image_1_1']['tmp_name'])){
            if (move_uploaded_file($_FILES['image_1_1']['tmp_name'], $this->tmpImgDir.$image_1_1)){
                $this->item->image_1_1 = $tmpFolder . DIRECTORY_SEPARATOR . $image_1_1;
            }
        } elseif (!empty($this->item->form_id)) {
            $this->item->image_1_1 = $tmpFolder . DIRECTORY_SEPARATOR . $image_1_1;
        }

        if (!empty($_FILES['image_1_2']['tmp_name'])){
            if (move_uploaded_file($_FILES['image_1_2']['tmp_name'], $this->tmpImgDir.$image_1_2)){
                $this->item->image_1_2 = $tmpFolder . DIRECTORY_SEPARATOR . $image_1_2;
            }
        } elseif (!empty($this->item->form_id)) {
            $this->item->image_1_2 = $tmpFolder . DIRECTORY_SEPARATOR . $image_1_2;
        }

        if (!empty($_FILES['image_1_3']['tmp_name'])){
            if (move_uploaded_file($_FILES['image_1_3']['tmp_name'], $this->tmpImgDir.$image_1_3)){
                $this->item->image_1_3 = $tmpFolder . DIRECTORY_SEPARATOR . $image_1_3;
            }
        } elseif (!empty($this->item->form_id)) {
            $this->item->image_1_3 = $tmpFolder . DIRECTORY_SEPARATOR . $image_1_3;
        }

        if (!empty($_FILES['image_rss']['tmp_name'])){
            $imgPath = $_FILES['image_rss']['tmp_name'];
            list($widthOrigin, $heightOrigin, $type) = getimagesize($imgPath);
            $k = $widthOrigin / $heightOrigin;
            $img = $this->resize_image($imgPath, $this->imageWidth, ($this->imageWidth / $k));
            if (imagejpeg($img, $this->tmpImgDir.$image_rss) && move_uploaded_file($imgPath, $this->tmpImgDir.$image_origin)){
                $this->item->image_rss = $tmpFolder . DIRECTORY_SEPARATOR . $image_rss;
                $this->item->image_origin = $tmpFolder . DIRECTORY_SEPARATOR . $image_origin;
            }
        } elseif (!empty($this->item->form_id)) {
            $this->item->image_rss = $tmpFolder . DIRECTORY_SEPARATOR . $image_rss;
            $this->item->image_origin = $tmpFolder . DIRECTORY_SEPARATOR . $image_origin;
        }

        return true;
    }

    /**
     * метод добавлдяет историю изменения URL для posts
     * @param $itemId
     * @param $urlStr
     * @return bool
     * @throws Exception
     */
    function add_url_history($itemId, $urlStr) {

        $query = sql_placeholder(" SELECT COUNT(*) as `count` FROM news_url_history as buh WHERE buh.news_id = ? AND buh.url = ? ", $itemId, $urlStr);
        $this->db->query($query);
        $url = $this->db->result();

        if ($url->count == 0) {
            $nowDate = (new DateTime('now'))->format('Y-m-d H:i:s');

            $query = sql_placeholder(" INSERT INTO news_url_history SET ?% ", array(
                'url' => $urlStr,
                'news_id' => $itemId,
                'created' => $nowDate,
                'modified' => $nowDate,
            ));

            $this->db->query($query);
            $res = $this->db->result();
            if ( $res === 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * сохранение связей между постом и тегами новостями (облаком тегов)
     *
     * @param array $tags
     * @param int $itemId
     */
    function setRelationsNewsItemTags($tags=array(), $itemId=0){
        // чистим предыдущие связи
        $query = sql_placeholder('DELETE FROM news_to_tags_relations WHERE news_id=?', $itemId);
        $this->db->query($query);
        if (!empty($tags) && $itemId!=0){
            if (is_array($tags)){
                foreach ($tags AS $postTagId){
                    $query = sql_placeholder('INSERT INTO news_to_tags_relations SET post_tag_id=?, news_id=?', $postTagId, $itemId);
                    $this->db->query($query);
                }
            }
            elseif($tags != 0){
                $query = sql_placeholder('INSERT INTO news_to_tags_relations SET post_tag_id=?, news_id=?', $tags, $itemId);
                $this->db->query($query);
            }
        }
    }

    public function addNewsSearchIndexSphinx($content)
    {
        require_once './../Sphinx.class.php';
        $sphinxConfig = $this->config->sphinx;
        $indexDB = new Sphinx($sphinxConfig['name'], $sphinxConfig['host'], $sphinxConfig['user'], $sphinxConfig['password'], $sphinxConfig['port']);
        $tablePrefix = @$sphinxConfig['table_prefix'];
        $indexDB->connect();

        $content->created = (new DateTime($content->created))->getTimestamp();
        $now = new DateTime();
        $query = sql_placeholder("REPLACE INTO {$tablePrefix}postsRT (id, `name`, header, lead, body, writers, enabled, date_add, date_created, model) VALUES (?,?,?,?,?,?,?,?,?,2)",
            (int)$content->id + 500000, $content->header, $content->text_content, $content->lead,  $content->article_text, (int)$content->writers, $content->enabled, (int)$now->getTimestamp(), $content->created);

        $indexDB->query($query);
        $indexDB->disconnect();
        return true;
    }

    private function clearSsrCache($item)
    {
        $dateUrl = (new DateTime($item->created))->format('Y/m/d');
        $fileCache = '/opt/node/puppeteer/' . $this->config->host . '/news/'. $dateUrl .'/' . $item->url . '_.html';
        unlink($fileCache);
    }
}
