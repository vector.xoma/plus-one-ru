<?PHP

require_once('Widget.admin.php');
require_once('../placeholder.php');

/**
 * Class NewsItems
 */
class NewsItems extends Widget
{
    private $tableName = 'news';
    private $section = 'news';
    private $items_per_page = 50;

    /**
     * @param $parent
     */
    function NewsItems(&$parent = null)
    {
        parent::Widget($parent);
        $this->prepare();
    }

    /**
     *
     */
    function prepare()
    {
        if (isset($_GET['act']) && $_GET['act'] == 'delete' && (isset($_POST['items']) || isset($_GET['item_id']))) {
            $this->check_token();

            $this->deleteItem($_GET['item_id'], $this->tableName);
            $get = $this->form_get(array());

            header("Location: index.php$get");
        }

        if (isset($_GET['set_enabled'])) {
            $this->check_token();

            $this->setEnable($_GET['set_enabled'], $this->tableName);

            $get = $this->form_get(array());

            header("Location: index.php$get");
        }

        if (!empty($_POST['seo_title']) || !empty($_POST['seo_description']) || !empty($_POST['seo_keywords'])){

            $this->setSeoContent($_POST);

            $get = $this->form_get(array());

            header("Location: index.php$get");
        }

    }

    /**
     *
     */
    function fetch()
    {
        $this->title = 'Новости';
        $link = "index.php?section=NewsItems";

        $where = "";
        $writerId = intval($this->param('writer_id'));
        if(!empty($writerId)){
            $where = ' AND writers=' . $writerId;
        }
        if ( $this->param('buildIndex')) {
            return $this->buildNewsIndex($this->param('buildIndexOffset') ? $this->param('buildIndexOffset')  : 0);
        }
        if ($search = $this->param('search')){

            if (isset($this->config->indexing) && $this->config->indexing) {
                require_once './../Sphinx.class.php';
                $indexDBConfig = $this->config->sphinx;
                $sphinxDB = new Sphinx($indexDBConfig['name'], $indexDBConfig['host'], $indexDBConfig['user'], $indexDBConfig['password'], $indexDBConfig['port']);
                $tablePrefix = @$indexDBConfig['table_prefix'];
                $sphinxDB->connect();
                $sphinxDB->query(sql_pholder("SELECT id FROM {$tablePrefix}posts WHERE MATCH (?) AND model = 2 ORDER BY WEIGHT() DESC  LIMIT 5000 
                                              OPTION field_weights=(name=100, header=60, lead=10, body=5)", $search));
                $postsIds = $sphinxDB->results();
                $sphinxDB->disconnect();

                $postsIdsStr = join(',', array_merge([0],array_map(function ($item ){ return $item->id - 500000;}, $postsIds)));
                $where .= " AND (id IN ({$postsIdsStr})) ";
            } else {
                $where .= " AND (article_text LIKE '%$search%' OR header LIKE '%$search%' OR lead LIKE '%$search%' OR text_content LIKE '%$search%')";
            }
            $link .= "&search=" . $search;
        }





        $totalRecordCount = $this->getTotalRows($where);

        if ($this->param('page')){
            $page = intval($this->param('page'));
        }
        else{
            $page = 1;
        }

        $start = ($page * $this->items_per_page) - $this->items_per_page;

        $totalPages = ceil($totalRecordCount / $this->items_per_page);

        if ($page > $totalPages){
            $page = $totalPages;
        }

        $paginationStructure = new stdClass();

        $paginationStructure->startPageLink = null;
        $paginationStructure->prevPageLink = null;
        $paginationStructure->nextPageLink = null;
        $paginationStructure->lastPageLink = null;

        $paginationStructure->page3left = null;
        $paginationStructure->page2left = null;
        $paginationStructure->page1left = null;

        $paginationStructure->page3right = null;
        $paginationStructure->page2right = null;
        $paginationStructure->page1right = null;


        if ($page != 1) {
            $paginationStructure->startPageLink = $link . "&page=1";
        }
        // Проверяем нужны ли стрелки вперед
        if ($page != $totalPages) {
            $paginationStructure->lastPageLink = $link . "&page=" . $totalPages;
        }

        // Находим три ближайшие станицы с обоих краев, если они есть
        if($page - 3 > 0) {
            $paginationStructure->page3left = $link . "&page=" . ($page - 3);
        }

        if($page - 2 > 0) {
            $paginationStructure->page2left = $link . "&page=" . ($page - 2);
        }
        if($page - 1 > 0) {
            $paginationStructure->page1left = $link . "&page=" . ($page - 1);
        }

        if($page + 3 < $totalPages) {
            $paginationStructure->page3right = $link . "&page=" . ($page + 3);
        }
        if($page + 2 < $totalPages) {
            $paginationStructure->page2right = $link . "&page=" . ($page + 2);
        }
        if($page + 1 < $totalPages) {
            $paginationStructure->page1right = $link . "&page=" . ($page + 1);
        }

        $this->smarty->assign('paginationStructure', $paginationStructure);
        $this->smarty->assign('page', $page);
        $this->smarty->assign('totalRecordCount', $totalRecordCount);




        $items = $this->getItems($start, $where);
        $nowDate = new DateTime('now');
        foreach ($items as $key => $item) {
            $items[$key]->edit_get = $this->form_get(array('section' => 'NewsItem', 'item_id' => $item->id, 'token' => $this->token));
            $items[$key]->delete_get = $this->form_get(array('act' => 'delete', 'item_id' => $item->id, 'token' => $this->token));
            $items[$key]->enable_get = $this->form_get(array('set_enabled'=>$item->id, 'item_id'=>$item->id, 'token'=>$this->token));
            $items[$key]->show_get = $this->form_get(array('set_show'=>$item->id, 'item_id'=>$item->id, 'token'=>$this->token));
            $items[$key]->previewUrl = '/news/'. (new DateTime($item->created))->format('Y/m/d'). '/' . $item->url. '?mode=preview';
            $item->image = '/adminv2/images/no_foto.gif';
            if ($item->image_rss) { $item->image = $item->image_rss;}
            $items[$key]->image = '<img src="/files/news/'.$item->image.'" style="max-width:100%;max-height:100%"/>';

            $createdDate = new DateTime($item->created);
            $items[$key]->deferred = $createdDate > $nowDate;
        }
        $seoContent = $this->getSeoContent();





        $this->smarty->assign('ItemsCount', count($items));
        $this->smarty->assign('search', $search);
        $this->smarty->assign('Items', $items);
        $this->smarty->assign('seoContent', $seoContent);
        $this->smarty->assign('title', $this->title);
        $this->smarty->assign('Lang', $this->lang);
        $this->body = $this->smarty->fetch('newsitems.tpl');
    }

    function getItems($start, $where = ''){
        $limit = $this->items_per_page;
        $query = sql_placeholder("SELECT *, DATE_FORMAT(created, '%d.%m.%Y %H:%i') AS date_created FROM {$this->tableName}
          WHERE 1 {$where}
          ORDER BY created DESC
          LIMIT {$start}, {$limit}");

        $this->db->query($query);
        $items = $this->db->results();
        return $items;
    }

    function getTotalRows($where)
    {
        $query = sql_placeholder("SELECT COUNT(*) AS recordCount FROM {$this->tableName} WHERE 1 {$where}");
        $this->db->query($query);
        $count = $this->db->result();

        return $count->recordCount;
    }

    function setSeoContent($post)
    {
        $query = sql_placeholder("SELECT * FROM seo_content WHERE section = ?", $this->section);
        $this->db->query($query);
        $seoContent = $this->db->result();

        if (!empty($seoContent->id)){
            $query = sql_placeholder("UPDATE seo_content SET 
              section = ?,
              seo_title =?,
              seo_description =?,
              seo_keywords = ?
              WHERE id = ?
              ", $this->section, $post['seo_title'], $post['seo_description'], $post['seo_keywords'], $seoContent->id);
        }
        else{
            $query = sql_placeholder("INSERT IGNORE INTO seo_content SET 
              section = ?,
              seo_title =?,
              seo_description =?,
              seo_keywords = ?
              ", $this->section, $post['seo_title'], $post['seo_description'], $post['seo_keywords']);
        }

        $this->db->query($query);
    }

    function getSeoContent()
    {
        $query = sql_placeholder("SELECT * FROM seo_content WHERE section = ?", $this->section);
        $this->db->query($query);
        $seoContent = $this->db->result();

        return $seoContent;
    }


    public function buildNewsIndex($offset = 0) {

        require_once  'NewsItem.admin.php';
        require_once './../Postgresql.class.php';

        $indexDBConfig = $this->config->indexDB;
        $indexDB = new Postgresql($indexDBConfig['name'], $indexDBConfig['host'], $indexDBConfig['user'], $indexDBConfig['password'], $indexDBConfig['port']);
        $indexDB->connect();

        $a = 0;
        $blogPostClass = new NewsItem($a);
        $limit = 1000;
        $countAll = 0;
        do {
            $query = sql_placeholder("SELECT id,  header,  lead, text_content, article_text  FROM news LIMIT {$offset}, {$limit}");
            $this->db->query($query);
            $results = $this->db->results();
            foreach ($results as $post) {
                echo "<pre>";
                echo 'post_'.$post->id .':';
                echo ' ' . $blogPostClass->addNewsSearchIndex($post, $indexDB);
                echo "</pre>";
            }

            $offset += $limit;
            $countAll += $limit;
        } while (count($results) > 0 && $countAll < 5000);

        $indexDB->disconnect();

        return $this;
    }

    private function getHost()
    {
        $host = $this->config->protocol . $this->config->host;

        return $host;
    }

    function getNewsById($id) {
        $query = sql_placeholder("SELECT n.id, NULL as blocks, n.tags, n.header as name, n.text_content as header, n.url, n.is_partner_material,
                            'news' as type_post, 
                            bw.name AS writer, bw.id AS writer_id, bw.private, bw.bloger, bw.name_genitive,
                            bw.image AS writerImage, bw.stub_image, bw.stub_link,
                            image_1_1, image_1_2, image_1_3, image_rss,
                            n.type_announce_1_1, n.type_announce_1_2, n.type_announce_1_3,
                            DATE_FORMAT(n.created, '%d.%m.%Y') AS date_created,
                            bt.name AS tagName, 
                            CONCAT ('news/', DATE_FORMAT(n.created, '%Y/%m/%d/'), n.url) AS tagUrl,
                            bw.bloger, bw.name as writerName, bw.announcement_img as blogImg, bw.id as writerId, bw.tag_url as writerTag,
                            n.format,
                            bt.name as bt_name, 
                            bt.url as bt_url 
                            
                            FROM news AS n
                          LEFT JOIN blogwriters AS bw ON bw.id=n.writers
                          LEFT JOIN blogtags AS bt ON bt.id = n.tags
                          WHERE n.id = ?",
            $id);

        if (!empty($query)){
            $this->db->query($query);
            $news = $this->db->result();
        }
        else{
            $news = null;
        }

        if ($news->image_1_3 && $news->type_announce_1_3 === 'picture'){
            $image = $this->getHost() . "/files/news/" . $news->image_1_3;
            $format = 'picture';
        } else {
            $image = null;
            $format = 'text';
        }

        $result[] = array(
            'id' => intval($news->id),
            'block' => null,
            'url' => $news->tagUrl,
            'name' => strip_tags($news->name),
            'header' => strip_tags($news->header),
            'date' => date('d.m.Y', strtotime($news->created)),
            'image' => $image,
            'iconCode' => $news->bt_url,
            'iconName' => $news->bt_name,
            'format' => $format,
            'postFormat' => null,
            'font' => null,
            'writersEnabled' => null,
            'typePost' => 'news',
            'type_announce_1_1' => $news->type_announce_1_1,
            'type_announce_1_2' => $news->type_announce_1_2,
            'type_announce_1_3' => $news->type_announce_1_3
        );

        return $result;
    }
}
