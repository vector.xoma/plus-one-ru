<?php

require_once('Widget.admin.php');
require_once('../placeholder.php');
require_once('BlogPosts.admin.php');
require_once('NewsItems.admin.php');

/**
 * Class Plots
 */
class Plots extends Widget
{
    private $tableName = 'groups';
    private $itemTableName = 'group_items';
    private $section = 'plots';
    private $items_per_page = 50;

    /**
     * @param $parent
     */
    function __construct(&$parent = null)
    {
        parent::Widget($parent);
        $this->prepare();
    }

    /**
     *
     */
    function prepare()
    {
        if (isset($_GET['act']) && $_GET['act'] == 'delete' && (isset($_POST['items']) || isset($_GET['item_id']))) {
            $this->check_token();

            $this->deleteItem($_GET['item_id'], $this->tableName);
            $get = $this->form_get(array());

            header("Location: index.php$get");
        }

        if (isset($_GET['set_enabled'])) {
            $this->check_token();

            $this->setEnable($_GET['set_enabled'], $this->tableName);

            $get = $this->form_get(array());

            header("Location: index.php$get");
        }

        if (!empty($_POST['seo_title']) || !empty($_POST['seo_description']) || !empty($_POST['seo_keywords'])){

            $this->setSeoContent($_POST);

            $get = $this->form_get(array());

            header("Location: index.php$get");
        }

    }

    /**
     *
     */
    function fetch()
    {
        $this->title = 'Сюжеты';
        $link = "index.php?section=Plots";

        $where = "";
        $writerId = intval($this->param('writer_id'));
        if(!empty($writerId)){
            $where = ' AND writers=' . $writerId;
        }

        if ($search = $this->param('search')){

            $where .= " AND (name LIKE '%$search%')";
            $link .= "&search=" . $search;
        }

        $totalRecordCount = $this->getTotalRows($where);

        if ($this->param('page')){
            $page = intval($this->param('page'));
        }
        else{
            $page = 1;
        }

        $start = ($page * $this->items_per_page) - $this->items_per_page;

        $totalPages = ceil($totalRecordCount / $this->items_per_page);

        if ($page > $totalPages){
            $page = $totalPages;
        }

        $paginationStructure = new stdClass();

        $paginationStructure->startPageLink = null;
        $paginationStructure->prevPageLink = null;
        $paginationStructure->nextPageLink = null;
        $paginationStructure->lastPageLink = null;

        $paginationStructure->page3left = null;
        $paginationStructure->page2left = null;
        $paginationStructure->page1left = null;

        $paginationStructure->page3right = null;
        $paginationStructure->page2right = null;
        $paginationStructure->page1right = null;


        if ($page != 1) {
            $paginationStructure->startPageLink = $link . "&page=1";
        }
        // Проверяем нужны ли стрелки вперед
        if ($page != $totalPages) {
            $paginationStructure->lastPageLink = $link . "&page=" . $totalPages;
        }

        // Находим три ближайшие станицы с обоих краев, если они есть
        if($page - 3 > 0) {
            $paginationStructure->page3left = $link . "&page=" . ($page - 3);
        }

        if($page - 2 > 0) {
            $paginationStructure->page2left = $link . "&page=" . ($page - 2);
        }
        if($page - 1 > 0) {
            $paginationStructure->page1left = $link . "&page=" . ($page - 1);
        }

        if($page + 3 < $totalPages) {
            $paginationStructure->page3right = $link . "&page=" . ($page + 3);
        }
        if($page + 2 < $totalPages) {
            $paginationStructure->page2right = $link . "&page=" . ($page + 2);
        }
        if($page + 1 < $totalPages) {
            $paginationStructure->page1right = $link . "&page=" . ($page + 1);
        }

        $this->smarty->assign('paginationStructure', $paginationStructure);
        $this->smarty->assign('page', $page);
        $this->smarty->assign('totalRecordCount', $totalRecordCount);

        $items = $this->getItems($where, $start);

        foreach ($items as $key => $item) {
            $items[$key]->edit_get = $this->form_get(array('section' => 'Plot', 'item_id' => $item->id, 'token' => $this->token));
            $items[$key]->delete_get = $this->form_get(array('act' => 'delete', 'item_id' => $item->id, 'token' => $this->token));
            $items[$key]->enable_get = $this->form_get(array('set_enabled'=>$item->id, 'item_id'=>$item->id, 'token'=>$this->token));
            $items[$key]->show_get = $this->form_get(array('set_show'=>$item->id, 'item_id'=>$item->id, 'token'=>$this->token));
        }

        $this->smarty->assign('ItemsCount', count($items));
        $this->smarty->assign('search', $search);
        $this->smarty->assign('Items', $items);
        $this->smarty->assign('title', $this->title);
        $this->smarty->assign('Lang', $this->lang);
        $this->body = $this->smarty->fetch('plots/_list.tpl');
    }

    function getItems($where = '', $start){
        $limit = $this->items_per_page;
        $query = sql_placeholder("SELECT g.*, DATE_FORMAT(g.created, '%d.%m.%Y %H:%i') AS date_created, ua.name as creator_name
            FROM {$this->tableName} as g
            LEFT JOIN users_admin ua ON ua.id = g.creator_id
          WHERE 1 {$where}
          ORDER BY g.created DESC
          LIMIT {$start}, {$limit}");

        $this->db->query($query);
        $items = $this->db->results();
        return $items;
    }

    function getTotalRows($where)
    {
        $query = sql_placeholder("SELECT COUNT(*) AS recordCount FROM {$this->tableName} WHERE 1 {$where}");
        $this->db->query($query);
        $count = $this->db->result();

        return $count->recordCount;
    }

    function getGroupContent($id)
    {
        $groupItemIdListQuery = sql_placeholder("SELECT g.id, g.content_type, g.content_id
                              FROM " . $this->itemTableName . " AS g
                              WHERE group_id = ?
                              ORDER BY g.order_num ASC", $id);

        $this->db->query($groupItemIdListQuery);
        $groupItemIdList = $this->db->results();
        $postList = [];
        foreach ($groupItemIdList as $groupItemId) {
            if ($groupItemId->content_type == 'blog') {
                $post = new BlogPosts();
                $groupItem = $post->getPostByTypeBlock('1_3', $groupItemId->content_id);
            } elseif ($groupItemId->content_type == 'news') {
                $post = new NewsItems();
                $groupItem = $post->getNewsById($groupItemId->content_id);
            }
            if (!empty($groupItem)) {
                $postList = array_merge($postList, $groupItem);
            }
        }

        return $postList;
    }

    function formatGroup($group)
    {
        $postList = $this->getGroupContent($group->id);

        $formattedGroup = array(
            'id' => intval($group->id),
            'name' => strip_tags($group->name),
            'date' => date('d.m.Y', strtotime($group->created)),
            'url' => $group->url,
            'enabled' => boolval($group->enabled),
            'postList' => $postList
        );
        return $formattedGroup;
    }

    function getGroupById($id)
    {
        $query = sql_placeholder("SELECT g.id, g.name, g.created, g.enabled, g.url
                              FROM " . $this->tableName . " AS g
                              WHERE g.enabled = 1
                                AND g.id = ?
                              ORDER BY g.created DESC", $id);
        $this->db->query($query);
        $group = $this->db->result();

        return $this->formatGroup($group);
    }

    function getGroupList()
    {
        $result = array();

        $query = sql_placeholder("SELECT g.id, g.name, g.created, g.enabled, g.url
                              FROM " . $this->tableName . " AS g
                              ORDER BY g.created DESC");
        $this->db->query($query);
        $items = $this->db->results();

        foreach ($items as $item) {
            $result[] = $this->formatGroup($item);
        }

        return $result;
    }
}
