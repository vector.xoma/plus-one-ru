<?PHP
require_once('Widget.admin.php');
require_once('BlogTags.admin.php');
require_once('BlogPosts.admin.php');
require_once('Traits/TypografCicle.php');
require_once('../placeholder.php');


/** Class PostTag */
class PostTag extends Widget
{
    use TypografCicle;

    var $item;
    var $uploaddir = '../files/tags/'; # Папка для хранения картинок (default)
    private $tableName = 'post_tags';

    private $errors = null;
    function __construct(&$parent){
        Widget::Widget($parent);
        $this->prepare();
    }


    function prepare() {
        $this->item  = new stdClass();
        $item_id = intval($this->param('item_id'));
        if ($_POST['save'] || $_POST['apply']) {
            $action = $_POST['save'];
            $this->errors = new stdClass();
            $this->check_token();
            $this->item->name = @$_POST['name'];
            $this->item->name_lead = @$_POST['name_lead'];
            $this->item->header = @$_POST['header'];
            $this->item->lead = @$_POST['lead'];
            $this->item->body = @$_POST['body'];
            $this->item->url = @$_POST['url'];
            $this->item->image_rss_description = @$_POST['image_rss_description'];
            $this->item->image_rss_source = @$_POST['image_rss_source'];
            $this->item->parent = @$_POST['parent'];

            $this->item->seo_title = $_POST['seo_title'];
            $this->item->seo_description = $_POST['seo_description'];
            $this->item->seo_keywords = $_POST['seo_keywords'];

            if (isset($_POST['enabled']) && $_POST['enabled'] == 1) {
                $this->item->enabled = 1;
            } else {
                $this->item->enabled = 0;
            }

            ## Не допустить одинаковые URL
            $query = sql_placeholder('SELECT COUNT(*) AS count FROM ' . $this->tableName . ' WHERE url=? AND id!=?', $this->item->url, $item_id);
            $this->db->query($query);
            $res = $this->db->result();


            if (empty($this->item->name)) {
                $this->errors->name = $this->lang->ENTER_TITLE;
            } elseif ($res->count > 0) {
                $this->errors->count = 'Тэг с таким латинским названием уже существует. Укажите другое.';
            }

            // првоверка на обязательные поля
            if (empty($this->item->name) || empty($this->item->name_lead) || $this->item->parent == 0 || empty($this->item->url)) {
                $this->errors->name = "не все обязательные поля заполнены!";
            }


            if (empty($this->error_msg) && count(get_object_vars($this->errors)) == 0){
                $this->beforeTypograf($this->item, ['body', 'header', 'lead']);
                $this->typografFields($this->item, [], ['name', 'name_lead', 'seo_title',  'seo_description', 'body', 'header', 'lead']);
                $this->afterTypograf($this->item, ['body', 'header', 'lead']);

                if(empty($item_id)) {
                    $this->item->created = date('Y-m-d H:i:s', time());
                    $this->item->modified = date('Y-m-d H:i:s', time());

                    $item_id = $this->add_article();
                    if (is_null($item_id)){
                        $this->errors->general = 'Ошибка при сохранении записи.';
                    }
                }
                else{
                    $this->item->modified = date('Y-m-d H:i:s', time());
                    if (is_null($this->update_article($item_id))){
                        $this->errors->general = 'Ошибка при сохранении записи.';
                    }
                }

                if (empty($this->error_msg) && count(get_object_vars($this->errors)) == 0) {

                    $this->add_fotos($item_id);

                    $get = $this->form_get(array('section' => 'PostTags'));

                    if (isset($_GET['from'])) {
                        header("Location: " . $_GET['from']);
                    } else {
                        header("Location: index.php$get");
                    }
                }
            }
            header("X-XSS-Protection: 0");
  	    }
  	    elseif (!empty($item_id)){
		    $query = sql_placeholder('SELECT * FROM ' . $this->tableName . ' WHERE id=?', $item_id);
		    $this->db->query($query);
		    $this->item = $this->db->result();
  	    }
    }

	function fetch()
	{
		if(empty($this->item->id)){
			$this->title = 'Новый тег второго уровня: ';
		}
		else{
			$this->title = 'Изменение тега второго уровня: ' . $this->item->name;
		}


        $blogTags = new BlogTags();
        $parentTags = $blogTags->getTags();

        $this->smarty->assign('title', $this->title);
        $this->smarty->assign('Item', $this->item);
        $this->smarty->assign('parentTags', $parentTags);
		$this->smarty->assign('Error', $this->error_msg);
        $this->smarty->assign('Errors', $this->errors);
		$this->smarty->assign('Lang', $this->lang);
        $this->smarty->assign('images_uploaddir', $this->uploaddir);


        $postsClass = new BlogPosts();
        $linkedPosts = $postsClass->getLinkedPost($this->item->id, 'post_tag');
        $this->smarty->assign('linkedPosts', $linkedPosts);

		$this->body = $this->smarty->fetch('posttags/posttag.tpl');
	}

    /**
     * @return int|null
     */
    function add_article(){
        $query = sql_placeholder('INSERT INTO ' . $this->tableName . ' SET ?%', (array)$this->item);
        if ($this->db->query($query)){
            $item_id = $this->db->insert_id();

            $query = sql_placeholder('UPDATE ' . $this->tableName . ' SET order_num=id WHERE id=?', $item_id);
            $this->db->query($query);

            return $item_id;
        } else {
            $this->errors->errors = join('<br/>',  $this->db->errors );
            return null;
        }
    }

    /**
     * @param $item_id
     * @return null
     */
    function update_article($item_id){
        $query = sql_placeholder('UPDATE ' . $this->tableName . ' SET ?% WHERE id=?', (array)$this->item, $item_id);

        if ($this->db->query($query)){
            return $item_id;
        }
        else{
            $this->errors->errors = join('<br/>',  $this->db->errors );
            return null;
        }
    }

    /**
     * метод добавлдяет фото к текущей статье
     * @param $article_id
     * @return bool
     */
    function add_fotos($itemId){
        $result = false;

        $largeuploadfile = $itemId.".jpg";
//        var_dump($_FILES);die();
        /// Загрузка большой картинки
        $large_image_uploaded = false;

        if(isset($_FILES['large_image']) && !empty($_FILES['large_image']['tmp_name'])){
            if (!move_uploaded_file($_FILES['large_image']['tmp_name'], $this->uploaddir.$largeuploadfile)){
                $this->error_msg = 'Ошибка при загрузке файла';
            }
            else{
                $large_image_uploaded = true;
            }
        }

        if($large_image_uploaded){
            $upload_folder = $this->uploaddir.$largeuploadfile;
//            $this->im_image_resize($upload_folder, $upload_folder, $this->large_image_width, $this->large_image_height);
            @chmod($this->uploaddir.$largeuploadfile, 0644);
            $this->db->query("UPDATE {$this->tableName} SET image='$largeuploadfile' WHERE id={$itemId}");
            $result = true;
        }

        return $result;
    }
}
