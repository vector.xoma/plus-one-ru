<?PHP
require_once('Widget.admin.php');
require_once('../placeholder.php');

/**
 * Class Article
 */
class PushMessage extends Widget
{
    var $item;
    private $tableName = 'push_notify';
    var $uploaddir = '../files/push/'; # Папка для хранения картинок (default)

    function PushMessage(&$parent){
        Widget::Widget($parent);
        $this->prepare();
    }

    function prepare(){
        $item_id = intval($this->param('item_id'));

        if(isset($_POST['title']) && isset($_POST['link'])){
//            $this->check_token();

            $this->item->title = $_POST['title'];
            $this->item->body = $_POST['body'];
            $this->item->link = $_POST['link'];
            $this->item->icon = "";


            if(empty($this->item->title)){
                $this->error_msg = $this->lang->ENTER_TITLE;
            }
            else{
                if(empty($item_id)) {
                    $this->item->created = date('Y-m-d H:i:s', time());

                    $item_id = $this->add_article();
                    if (is_null($item_id)){
                        $this->error_msg = 'Ошибка при сохранении записи.';
                    }
                }

                $this->add_fotos($item_id);

                $this->preparePushNotify($item_id);

                $get = $this->form_get(array('section'=>'PushMessages'));

                if(isset($_GET['from'])){
                    header("Location: ".$_GET['from']);
                }
                else{
                    header("Location: index.php$get");
                }
            }
        }
        elseif (!empty($item_id)){
            $query = sql_placeholder('SELECT * FROM ' . $this->tableName . ' WHERE id=?', $item_id);
            $this->db->query($query);
            $this->item = $this->db->result();
        }
    }

    function fetch()
    {
        if(empty($this->item->id)){
            $this->title = 'Отправить Push уведомление';
        }

        $this->smarty->assign('title', $this->title);
        $this->smarty->assign('Item', $this->item);
        $this->smarty->assign('Error', $this->error_msg);
        $this->smarty->assign('Lang', $this->lang);
        $this->smarty->assign('images_uploaddir', $this->uploaddir);

        $this->body = $this->smarty->fetch('pushNotify/push_notify.tpl');
    }

    /**
     * @return int|null
     */
    function add_article(){
        $query = sql_placeholder('INSERT INTO ' . $this->tableName . ' SET ?%', (array)$this->item);
        if ($this->db->query($query)){
            $item_id = $this->db->insert_id();
            return $item_id;
        }
        else{
            return null;
        }
    }

    /**
     * @param $item_id
     * @return null
     */
    function update_article($item_id){
        $query = sql_placeholder('UPDATE ' . $this->tableName . ' SET ?% WHERE id=?', (array)$this->item, $item_id);

        if ($this->db->query($query)){
            return $item_id;
        }
        else{
            return null;
        }
    }


    function preparePushNotify($itemId)
    {
        $query = sql_placeholder('SELECT * FROM subscribe_push');
        $this->db->query($query);
        $tokens = $this->db->results();

        $query = sql_placeholder('SELECT * FROM ' . $this->tableName . ' WHERE id=?', $itemId);
        $this->db->query($query);
        $item = $this->db->result();

        foreach ($tokens AS $to){
            $message = array(
                'to' => $to->token,
                'notification' => array(
                    "title" => $item->title,
                    "body" => $item->body,
                    "icon" => "/files/push/" . $item->icon,
                    "badge" => 8,
                    "click_action" => $item->link
                )
            );

            $results[] = $this->sendPushNotify($message);
        }

        return $results;
    }

    /**
     * метод добавлдяет фото к текущей статье
     * @param $article_id
     * @return bool
     */
    function add_fotos($itemId){
        $result = false;

        $largeuploadfile = $itemId.".jpg";

        /// Загрузка большой картинки
        if(isset($_FILES['large_image']) && !empty($_FILES['large_image']['tmp_name'])){
            if (!move_uploaded_file($_FILES['large_image']['tmp_name'], $this->uploaddir.$largeuploadfile)){
                $this->error_msg = 'Ошибка при загрузке файла';
            }
            else{
                $result = true;

                $query = sql_placeholder("UPDATE {$this->tableName} SET icon='$largeuploadfile' WHERE id={$itemId}");
                $this->db->query($query);
            }
        }
        return $result;
    }

    function sendPushNotify($message)
    {
        $url = "https://fcm.googleapis.com/fcm/send";

        $ch = curl_init($url);

        $head = array(
            "Content-Type: application/json",
            "Authorization: key=AAAAU1AB4zE:APA91bFsai58H0GC8bslm9-Nj6vPvTdtC9BJR9xh7yNFkJ0IC0WwhxjK0UD2I7h26TVsdq6ZlvHJSe2T5bB1pOfn04w6q-yVcOd0FJDfD4h3hNV2piizG95fFpi12BnCUZaGoytJ6BqfY3PeLRZoT5Y121lxVf3VjQ"
        );

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);   // возвращает веб-страницу
        curl_setopt($ch, CURLOPT_HTTPHEADER, $head);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($message));

        $content = curl_exec($ch);
//        $err = curl_errno($ch);
//        $errmsg = curl_error($ch);
//        $header = curl_getinfo($ch);
        curl_close($ch);

        return $content;
    }
}