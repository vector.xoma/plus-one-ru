<?PHP

require_once('BlogTags.admin.php');
require_once('BlogPosts.admin.php');
require_once('Widget.admin.php');
require_once('../placeholder.php');
require_once ('../PresetFormat.class.php');
require_once ('../Helper.class.php');
require_once ('Traits/TypografCicle.php');

/** Class SeoContents */
class SeoContent extends Widget
{
    use TypografCicle;

    private $item;
    private $uploaddir = '../files/seo_content/'; # Папка для хранения картинок (default)
    private $tableName = 'seo_content';
    private $errors = null;

    function __construct(&$parent){
        Widget::Widget($parent);
        $this->prepare();
    }


    function prepare() {
        $this->item  = new stdClass();
        $this->errors = new stdClass();

        $item_id = intval($this->param('item_id'));

        if (@$_POST['save'] || @$_POST['apply']) {

            $this->check_token();

            $this->item->name = $_POST['name'];
            $this->item->section = $_POST['section'];
            $this->item->url = $_POST['url'];
            /** Редактор реакт делает только такое поле "description"*/
            $this->item->header = $_POST['description'];
            $this->item->seo_title = $_POST['seo_title'];
            $this->item->seo_description = $_POST['seo_description'];
            $this->item->seo_keywords = $_POST['seo_keywords'];


            $this->beforeTypograf($this->item, ['header']);
            $this->typografFields($this->item, [], ['header', 'seo_title', 'seo_description']);
            $this->afterTypograf($this->item, ['header']);

            if (empty($this->error_msg) && count(get_object_vars($this->errors)) == 0){
                if(empty($item_id)) {
                    $item_id = $this->add_article();
                    if (is_null($item_id)){
                        $this->errors->general = 'Ошибка при сохранении записи.';
                    }
                }
                else{
                    if (is_null($this->update_article($item_id))){
                        $this->errors->general = 'Ошибка при сохранении записи.';
                    }
                }


                if (empty($this->error_msg) && count(get_object_vars($this->errors)) == 0) {
                    $this->add_fotos($item_id);

                    if (empty($this->error_msg) && count(get_object_vars($this->errors)) == 0) {
                        $get = $this->form_get(array('section' => 'SeoContents'));

                        if (@$_POST['apply']) {
                            $_SESSION['SeoContent']['flash'] = "Успешно сохранено!";
                            header("Location: " . $_SERVER['HTTP_REFERER']);
                            exit(0);
                        }
                        if (isset($_GET['from'])) {
                            header("Location: " . $_GET['from']);
                            exit(0);
                        } else {
                            header("Location: index.php$get");
                            exit(0);
                        }
                    }
                }
            }
            header("X-XSS-Protection: 0");
  	    }
  	    elseif (!empty($item_id)){
		    $query = sql_placeholder('SELECT * FROM ' . $this->tableName . ' WHERE id=?', $item_id);
		    $this->db->query($query);
		    $this->item = $this->db->result();
        }
    }

	function fetch()
	{

		if(empty($this->item->id)){
			$this->title = 'Seo Content: ';
		}
		else{
			$this->title = 'SeoContent: ' . $this->item->name;
		}

        $this->smarty->assign('title', $this->title);
        $this->smarty->assign('Item', $this->item);
		$this->smarty->assign('Error', $this->error_msg);
        $this->smarty->assign('Errors', $this->errors);
		$this->smarty->assign('Lang', $this->lang);
        $this->smarty->assign('images_uploaddir', $this->uploaddir);
        $this->smarty->assign('flash', $_SESSION['SeoContent']['flash']);
        unset($_SESSION['SeoContent']['flash']);

		$this->body = $this->smarty->fetch('seo_contents/item.tpl');
	}

    /**
     * @return int|null
     */
    function add_article(){
        $query = sql_placeholder('REPLACE INTO ' . $this->tableName . ' SET ?%', (array)$this->item);
        if ($this->db->query($query)){
            return $this->db->insert_id();
        } else {
            $this->errors->errors = join('<br/>',  $this->db->errors );
            return null;
        }
    }

    /**
     * @param $item_id
     * @return null
     */
    function update_article($item_id){
        $query = sql_placeholder('UPDATE ' . $this->tableName . ' SET ?% WHERE id=?', (array)$this->item, $item_id);

        if ($this->db->query($query)){
            return $item_id;
        }
        else{
            $this->errors->errors = join('<br/>',  $this->db->errors );
            return null;
        }
    }


    /**
     * метод добавлдяет фото к текущей статье
     * @param $article_id
     * @return bool
     */
    function add_fotos($itemId){

        if (@$_POST['delete_large_image'] === "1" && !empty($itemId)) {
            $this->db->query("UPDATE {$this->tableName} SET large_image = NULL WHERE id={$itemId}");
        }
        if (@$_POST['delete_stub_image'] === "1" && !empty($itemId)) {
            $this->db->query("UPDATE {$this->tableName} SET stub_image=NULL WHERE id={$itemId}");
        }

        $result = false;

        $dateFolder = '' . (new DateTime('now'))->format('Y/m') . '/';

        $largeuploadfile = $dateFolder. $itemId.".jpg";
        $stubImage = $dateFolder. $itemId . '_stub_image.jpg';
        $announcementImage = $dateFolder. $itemId . '_announcement_img.jpg';

        $forOptimized = array();
        $this->mkdir_custom($this->uploaddir . $dateFolder);

        if(isset($_FILES['large_image']) && !empty($_FILES['large_image']['tmp_name'])){
            if (!move_uploaded_file($_FILES['large_image']['tmp_name'], $this->uploaddir.$largeuploadfile)){
                $this->errors->large_image = 'Ошибка при загрузке файла';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET large_image='$largeuploadfile' WHERE id={$itemId}");
                $result = true;
                $forOptimized[] = array(realpath($this->uploaddir.$largeuploadfile), $largeuploadfile);
            }
        }

        if(isset($_FILES['stub_image']) && !empty($_FILES['stub_image']['tmp_name'])){

            if (!move_uploaded_file($_FILES['stub_image']['tmp_name'], $this->uploaddir.$stubImage)){
                $this->errors->stub_image = 'Ошибка при загрузке файла';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET stub_image='$stubImage' WHERE id={$itemId}");
                $result = true;
                $forOptimized[] = array(realpath($this->uploaddir.$stubImage), $stubImage);
            }
        }

        if ($this->config->tinypng['optimized']) {
            $projectDir = realpath(__DIR__.'/../');
            $nowDate = date('Y-m-d H:i:s');
            foreach ($forOptimized as $image) {
                $fileSaveLog = '/files/seo_content/'.$image[1];
                $this->db->query(sql_placeholder("INSERT INTO optimizer_log (item_id, item_type, image, status, created, `size`) VALUE (?, 'writer', ?, 0, ?, ?)"
                    , $itemId, $fileSaveLog, $nowDate, (int)@filesize($projectDir.$fileSaveLog)));
            }
        }

        return $result;
    }
}
