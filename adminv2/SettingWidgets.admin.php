<?PHP
require_once('Widget.admin.php');
require_once('../placeholder.php');

class SettingWidgets extends Widget
{
    var $item;
    private $tableName = 'setting_widgets';

    function __construct(&$parent = null)
    {
        Widget::Widget($parent);
        $this->prepare();
    }

    function prepare()
    {
        $item_id = intval($this->param('item_id'));

        if (isset($_GET['act']) && $_GET['act'] == 'delete' && (isset($_POST['items']) || isset($_GET['item_id']))) {
            $this->check_token();

            $this->deleteItem($_GET['item_id'], $this->tableName);
            $get = $this->form_get(array());

            header("Location: index.php$get");
        }
        if (isset($_GET['set_enabled'])) {
            $this->check_token();

            $this->setEnable($_GET['set_enabled'], $this->tableName);

            $get = $this->form_get(array());

            header("Location: index.php$get");
        }

        if (isset($_POST['widget_name']) && isset($_POST['widget_code'])) {
            $this->check_token();

            $this->item->widget_name = $_POST['widget_name'];
            $this->item->widget_code = $_POST['widget_code'];
            $this->item->wrap_class = $_POST['wrap_class'];

            if (isset($_POST['enabled']) && $_POST['enabled'] == 1) {
                $this->item->enabled = 1;
            } else {
                $this->item->enabled = 0;
            }


            if (empty($this->item->widget_name)) {
                $this->error_msg = $this->lang->ENTER_TITLE;
            } else {
                if (empty($item_id)) {
                    $this->item->created = date('Y-m-d H:i:s', time());

                    $item_id = $this->add_article();
                    if (is_null($item_id)) {
                        $this->error_msg = 'Ошибка при сохранении записи.';
                    }
                } else {
                    $this->item->modified = date('Y-m-d H:i:s', time());
                    if (is_null($this->update_article($item_id))) {
                        $this->error_msg = 'Ошибка при сохранении записи.';
                    }
                }

                $get = $this->form_get(array('section' => 'SettingWidgets'));

                if (isset($_GET['from'])) {
                    header("Location: " . $_GET['from']);
                } else {
                    header("Location: index.php$get");
                }
            }
        } elseif (!empty($item_id)) {
            $query = sql_placeholder('SELECT * FROM ' . $this->tableName . ' WHERE id=?', $item_id);
            $this->db->query($query);
            $this->item = $this->db->result();
        }
    }

    function fetch()
    {
        $this->title = 'настройки ленты новостей';

        $this->db->query("SELECT * FROM {$this->tableName} ORDER BY order_num ASC");
        $items = $this->db->results();

        foreach ($items as $key => $item) {
            $items[$key]->delete_get = $this->form_get(array('act' => 'delete', 'item_id' => $item->id));
            $items[$key]->enable_get = $this->form_get(array('set_enabled' => $item->id));
        }

        $this->smarty->assign('title', $this->title);
        $this->smarty->assign('Items', $items);
        $this->smarty->assign('Error', $this->error_msg);
        $this->smarty->assign('Lang', $this->lang);

        $this->body = $this->smarty->fetch('setting/widgets.tpl');
    }

    /**
     * @return int|null
     */
    function add_article()
    {
        $query = sql_placeholder('INSERT INTO ' . $this->tableName . ' SET ?%', (array)$this->item);
        if ($this->db->query($query)) {
            $item_id = $this->db->insert_id();

            $query = sql_placeholder('UPDATE ' . $this->tableName . ' SET order_num=id WHERE id=?', $item_id);
            $this->db->query($query);
            return $item_id;
        } else {
            return null;
        }
    }

    /**
     * @param $item_id
     * @return null
     */
    function update_article($item_id)
    {
        $query = sql_placeholder('UPDATE ' . $this->tableName . ' SET ?% WHERE id=?', (array)$this->item, $item_id);

        if ($this->db->query($query)) {
            return $item_id;
        } else {
            return null;
        }
    }

    function sortable($orderIds) {
        $order = 1;

        foreach ($orderIds as $id) {
            $this->db->query(sql_placeholder("UPDATE {$this->tableName} SET order_num = ? WHERE id = ?", $order++, $id));
        }
    }
}


