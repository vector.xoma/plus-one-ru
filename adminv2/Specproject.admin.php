<?PHP
require_once('Widget.admin.php');
require_once('BlogTags.admin.php');
require_once('PostTags.admin.php');
require_once('../placeholder.php');
require_once('RemoteTypograf.php');

/**
 * Class Specproject
 */
class Specproject extends Widget
{
    private $tableName = 'spec_projects';

    var $item;
    var $large_image_width = "600";
    var $large_image_height = "600";
    var $uploaddir = '../files/spec_project/'; # Папка для хранения картинок (default)

    function Specproject(&$parent){
        Widget::Widget($parent);
        $this->prepare();
    }

    function setData($post, $itemId){
        $this->item->id = $itemId;
        $this->item->name = $post['name'];
        $this->item->header = $post['header'];
        $this->item->color = $post['color'];
        $this->item->font_color = $post['font_color'];
        $this->item->external_url = $post['external_url'];
        $this->item->tag = $post['tags'];
        $this->item->type_announce_1_1 = $post['type_announce_1_1'];
        $this->item->type_announce_1_2 = $post['type_announce_1_2'];
        $this->item->type_announce_1_3 = $post['type_announce_1_3'];

        $created = $post['created'];
        $createdToDb = date('Y-m-d H:i:s', strtotime($created));
        $this->item->created = $createdToDb;

        $this->item->enabled = 0;
        if(isset($post['enabled']) && $post['enabled']==1) {
            $this->item->enabled = 1;
        }
    }

    function checkRequiredFields(){
        // проверка на обязательные поля
        if (empty($this->item->name) || /*empty($this->item->color) ||*/ empty($this->item->external_url) || $this->item->tag == 0){
            $this->error_msg = "Пост не сохранен. Заполните обязательные поля, отмеченные красным цветом";
        }
    }

    function checkUrl($itemId){
        $query = sql_placeholder('SELECT COUNT(*) AS count FROM ' . $this->tableName . ' WHERE url=? AND id!=?', $this->item->url, $itemId);
        $this->db->query($query);
        $res = $this->db->result();

        if($res->count>0){
            $this->error_msg = 'Запись с таким URL уже существует. Укажите другой.';
        }
    }

    function prepare(){
        $item_id = intval($this->param('item_id'));

        if(isset($_POST['name'])){
            $this->check_token();

            // создаем объект из переданных данных
            $this->setData($_POST, $item_id);
            // проверка на обязательные поля
            $this->checkRequiredFields();
            //Не допустить одинаковые URL статей
            $this->checkUrl($item_id);

            if (empty($this->error_msg)) {
                $typograf = new RemoteTypograf('UTF-8');
                $typograf->typografFields($this->item, [], ['name', 'header']);
                if (empty($item_id)) {
                    $this->item->image_1_1 = null;
                    $this->item->created = date('Y-m-d H:i:s', time());
                    $this->item->modified = date('Y-m-d H:i:s', time());

                    $item_id = $this->add_article();

                    if (is_null($item_id)){
                        $this->error_msg = 'Ошибка при сохранении записи.';
                    }
                }
                else{
                    $this->item->modified = date('Y-m-d H:i:s', time());
                    if (is_null($this->update_article($item_id))){
                        $this->error_msg = 'Ошибка при сохранении записи.';
                    }
                }

                $this->add_fotos($item_id);

                $get = $this->form_get(array('section'=>'Specprojects'));

                header("Location: index.php$get");
            }
  	    }
  	    elseif (!empty($item_id)){
		    $query = sql_placeholder('SELECT *, DATE_FORMAT(created, \'%d.%m.%Y %H:%i\') AS date_created FROM ' . $this->tableName . ' WHERE id=?', $item_id);
		    $this->db->query($query);
		    $this->item = $this->db->result();
  	    }

        if (!(empty($this->error_msg))) {
            $this->item->date_created = $_POST['created'];
        }
    }

	function fetch()
	{
		if(empty($this->item->id)){
			$this->title = 'Новый спецпроект';
            $this->item->date_created = date('d.m.Y H') . ":00";
		}
		else{
			$this->title = 'Изменение спецпроекта: ' . $this->item->header;
		}

        $blogTagsClass = new BlogTags();

        $blogTags = $blogTagsClass->getTags();

        foreach ($blogTags AS $k=>$blogTag){
            $query = sql_placeholder("SELECT id, name_lead FROM post_tags WHERE parent=? ORDER BY name ASC", $blogTag->id);
            $this->db->query($query);
            $blogTags[$k]->items = $this->db->results();
        }

        $bt = new BlogTags();
        $tags = $bt->getTags();

        $this->smarty->assign('title', $this->title);
        $this->smarty->assign('Item', $this->item);
        $this->smarty->assign('tags', $tags);
        $this->smarty->assign('Error', $this->error_msg);
		$this->smarty->assign('Lang', $this->lang);
        $this->smarty->assign('images_uploaddir', $this->uploaddir);



		$this->body = $this->smarty->fetch('spec_project.tpl');
	}

    /**
     * @return int|null
     */
    function add_article(){
        $query = sql_placeholder('INSERT INTO ' . $this->tableName . ' SET ?%', (array)$this->item);
        if ($this->db->query($query)){
            $item_id = $this->db->insert_id();

            $query = sql_placeholder('UPDATE ' . $this->tableName . ' SET order_num=id WHERE id=?', $item_id);
            $this->db->query($query);

            return $item_id;
        }
        else{
            return null;
        }
    }

    /**
     * @param $item_id
     * @return null
     */
    function update_article($item_id){
        $query = sql_placeholder('UPDATE ' . $this->tableName . ' SET ?% WHERE id=?', (array)$this->item, $item_id);
        if ($this->db->query($query)){
            return $item_id;
        }
        else{
            return null;
        }
    }

    /**
     * метод добавлдяет фото к текущей статье
     * @param $article_id
     * @return bool
     */
    function add_fotos($itemId){
        $result = false;

        $dateFolder = '' . (new DateTime($this->item->created))->format('Y/m') . '/';

        $largeuploadfile_1_1 = $dateFolder . $itemId."_1-1.jpg";
        $largeuploadfile_1_2 = $dateFolder . $itemId."_1-2.jpg";
        $largeuploadfile_1_3 = $dateFolder . $itemId."_1-3.jpg";

        $forOptimized = array();
        $this->mkdir_custom($this->uploaddir . $dateFolder);
        /// Загрузка большой картинки
        $large_image_uploaded = false;

        if(isset($_FILES['image_1_1']) && !empty($_FILES['image_1_1']['tmp_name'])){
            if (!move_uploaded_file($_FILES['image_1_1']['tmp_name'], $this->uploaddir.$largeuploadfile_1_1)){
                $this->error_msg = 'Ошибка при загрузке файла';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET image_1_1='$largeuploadfile_1_1' WHERE id={$itemId}");
                $forOptimized[] = array(realpath($this->uploaddir.$largeuploadfile_1_1), $largeuploadfile_1_1);
            }
        }

        if(isset($_FILES['image_1_2']) && !empty($_FILES['image_1_2']['tmp_name'])){
            if (!move_uploaded_file($_FILES['image_1_2']['tmp_name'], $this->uploaddir.$largeuploadfile_1_2)){
                $this->error_msg = 'Ошибка при загрузке файла';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET image_1_2='$largeuploadfile_1_2' WHERE id={$itemId}");
                $forOptimized[] = array(realpath($this->uploaddir.$largeuploadfile_1_2), $largeuploadfile_1_2);
            }
        }

        if(isset($_FILES['image_1_3']) && !empty($_FILES['image_1_3']['tmp_name'])){
            if (!move_uploaded_file($_FILES['image_1_3']['tmp_name'], $this->uploaddir.$largeuploadfile_1_3)){
                $this->error_msg = 'Ошибка при загрузке файла';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET image_1_3='$largeuploadfile_1_3' WHERE id={$itemId}");
                $forOptimized[] = array(realpath($this->uploaddir.$largeuploadfile_1_3), $largeuploadfile_1_3);
            }
        }

        if ($this->config->tinypng['optimized']) {
            $projectDir = realpath(__DIR__.'/../');
            $nowDate = date('Y-m-d H:i:s');
            foreach ($forOptimized as $image) {
                $fileSaveLog = '/files/spec_project/'.$image[1];
                $this->db->query(sql_placeholder("INSERT INTO optimizer_log (item_id, item_type, image, status, created, `size`) VALUE (?, 'spec_project', ?, 0, ?, ?)"
                    , $itemId, $fileSaveLog, $nowDate, (int)@filesize($projectDir.$fileSaveLog)));
            }
        }

        return $result;
    }
}
