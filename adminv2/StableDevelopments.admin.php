<?PHP

require_once('BlogTags.admin.php');
require_once('BlogPosts.admin.php');
require_once('Widget.admin.php');
require_once('../placeholder.php');
require_once ('../PresetFormat.class.php');
require_once ('../Helper.class.php');

/** Class StableDevelopment */
class StableDevelopments extends Widget
{

    private $id = 1;
    var $item;
    var $uploaddir = '../files/stable_development/'; # Папка для хранения картинок (default)
    private $tableName = 'stable_development';

    private $errors = null;
    function __construct(&$parent){
        Widget::Widget($parent);
        Helper::$db = $this->db;
        $this->prepare();
    }


    function prepare() {
        $this->item  = new stdClass();
        $this->errors = new stdClass();

        $item_id = intval($this->id);

        if (@$_POST['save'] || @$_POST['apply']) {
            $action = $_POST['save'];
            $this->check_token();

            $this->item->id = $this->id;
            $this->item->seo_title = $_POST['seo_title'];
            $this->item->seo_description = $_POST['seo_description'];
            $this->item->seo_keywords = $_POST['seo_keywords'];


            if (empty($this->error_msg) && count(get_object_vars($this->errors)) == 0){
                if(empty($item_id)) {
                    $item_id = $this->add_article();
                    if (is_null($item_id)){
                        $this->errors->general = 'Ошибка при сохранении записи.';
                    }
                }
                else{
                    if (is_null($this->update_article($item_id))){
                        $this->errors->general = 'Ошибка при сохранении записи.';
                    }
                }

                if (empty($this->error_msg) && count(get_object_vars($this->errors)) == 0) {

                    $this->add_fotos($item_id);

                    $get = $this->form_get(array('section' => 'StableDevelopments'));
                    $_SESSION['flash'] = "Успешно сохранено!";

                    if (isset($_GET['from'])) {
                        header("Location: " . $_GET['from']);
                        exit(0);
                    } else {
                        header("Location: index.php$get");
                        exit(0);
                    }

                }
            }
            header("X-XSS-Protection: 0");
  	    }
  	    elseif (!empty($item_id)){
		    $query = sql_placeholder('SELECT * FROM ' . $this->tableName . ' WHERE id=?', $item_id);
		    $this->db->query($query);
		    $this->item = $this->db->result();
        }
    }

	function fetch()
	{

		if(empty($this->item->id)){
			$this->title = 'Устойчивое развитие: ';
		}
		else{
			$this->title = 'Устойчивое развитие: ' . $this->item->name;
		}


        $blogTags = new BlogTags();
        $parentTags = $blogTags->getTags();

        $this->smarty->assign('title', $this->title);
        $this->smarty->assign('Item', $this->item);
        $this->smarty->assign('parentTags', $parentTags);
		$this->smarty->assign('Error', $this->error_msg);
        $this->smarty->assign('Errors', $this->errors);
		$this->smarty->assign('Lang', $this->lang);
        $this->smarty->assign('images_uploaddir', $this->uploaddir);
        $this->smarty->assign('flash', $_SESSION['flash']);
        unset($_SESSION['flash']);

        $postsClass = new BlogPosts();
        $linkedPosts = $postsClass->getLinkedPost($this->item->id, 'post_tag');
        $this->smarty->assign('linkedPosts', $linkedPosts);

		$this->body = $this->smarty->fetch('stableDevelopment/stableDevelopment.tpl');
	}

    /**
     * @return int|null
     */
    function add_article(){
        $query = sql_placeholder('REPLACE INTO ' . $this->tableName . ' SET ?%', (array)$this->item);
        if ($this->db->query($query)){
            $item_id = $this->db->insert_id();

            $query = sql_placeholder('UPDATE ' . $this->tableName . ' SET order_num=id WHERE id=?', $item_id);
            $this->db->query($query);

            return $item_id;
        } else {
            $this->errors->errors = join('<br/>',  $this->db->errors );
            return null;
        }
    }

    /**
     * @param $item_id
     * @return null
     */
    function update_article($item_id){
        $query = sql_placeholder('UPDATE ' . $this->tableName . ' SET ?% WHERE id=?', (array)$this->item, $item_id);

        if ($this->db->query($query)){
            return $item_id;
        }
        else{
            $this->errors->errors = join('<br/>',  $this->db->errors );
            return null;
        }
    }

    /**
     * метод добавлдяет фото к текущей статье
     * @param $article_id
     * @return bool
     */
    function add_fotos($itemId){
        $result = false;

        $largeuploadfile = $itemId.".jpg";
//        var_dump($_FILES);die();
        /// Загрузка большой картинки
        $large_image_uploaded = false;

        if(isset($_FILES['large_image']) && !empty($_FILES['large_image']['tmp_name'])){
            if (!move_uploaded_file($_FILES['large_image']['tmp_name'], $this->uploaddir.$largeuploadfile)){
                $this->error_msg = 'Ошибка при загрузке файла';
            }
            else{
                $large_image_uploaded = true;
            }
        }

        if($large_image_uploaded){
            $upload_folder = $this->uploaddir.$largeuploadfile;
//            $this->im_image_resize($upload_folder, $upload_folder, $this->large_image_width, $this->large_image_height);
            @chmod($this->uploaddir.$largeuploadfile, 0644);
            $this->db->query("UPDATE {$this->tableName} SET image='$largeuploadfile' WHERE id={$itemId}");
            $result = true;
        }

        return $result;
    }

    public function getTagsForPublic($page = 1) {

        $query = sql_placeholder("
              SELECT pt.name, pt.url, pt.id, pt.enabled
                FROM post_tags AS pt
               WHERE pt.enabled = 1 AND TRIM(body) <> ''
            GROUP BY pt.id
        ");

        $this->db->query($query);
        $tags = $this->db->results();

        $this->db->query(sql_placeholder("SELECT * FROM seo_content WHERE section = 'stable-development'"));
        $stableDev = $this->db->result();
        $this->title = $stableDev->seo_title;
        $this->keywords = $stableDev->seo_keywords;
        $this->description = $stableDev->seo_description;

        usort($tags, function ($a, $b) { return  strnatcasecmp($a->name, $b->name);});

        return array(
            'items' => $tags,
            'rubrikaUrl' => "устойчивое-развитие",
            'header' => $stableDev->seo_title,
            'description' => $stableDev->header,
            'imageDesktop' => $stableDev->large_image ? "/files/seo_content/" . $stableDev->large_image : null,
            'imageMobile' => $stableDev->stub_image ? "/files/seo_content/" . $stableDev->stub_image : null,
            'page' => $page + 1,
            'meta' => [
                'title' => $this->title,
                'description' => $this->description,
                'keywords' => $this->keywords,
                'ogTitle' => $this->title,
                'ogType' => 'article',
                'ogDescription' => $this->description,
                'ogImage' => $stableDev->large_image ? $this->getHost() . "/files/seo_content/" . $stableDev->large_image : $this->getHost(). '/og-plus-one.ru.png',
                'ogSiteName' => '',
                'ogUrl' => '',
                'twitterCard' => 'summary_large_image',
                'twitterTitle' => $this->title,
                'twitterDescription' =>  $this->description,
                'twitterUrl' => '',
                'twitterImage' => $this->getHost(). '/og-plus-one.ru.png',
                'twitterImageAlt' => '+1 — Проект об устойчивом развитии',
                'twitterSite' => ''
            ]
        );
    }

    public function getOnceTagForPublic($tag = null) {

        $query = sql_placeholder("
              SELECT * FROM post_tags AS pt
               WHERE pt.enabled = 1 AND TRIM(body) <> '' AND pt.url = ?
        ", $tag);


        $this->db->query($query);
        $article = $this->db->result();

        $this->title = $article->seo_title;
        $this->keywords = $article->seo_keywords;
        $this->description = $article->seo_description;

        $article->tags = array(
            'color' => 'ffffff',
            'name' => 'Устойчивое развитие',
            'url' => 'устойчивое-развитие'
        );

        $article->relatedPosts =  Helper::formatPostList(Helper::getRelatedPostsByParent($article->id, 'post_tag', new DateTime()), '1_3', $this->getHost());

        $article->fullUrl = 'устойчивое-развитие/'. $article->url;

        $ogImage = $article->image ? '/files/tags/' . $article->image : '/og-plus-one.ru.png';
        $ogImage = $this->getHost(). $ogImage;

        $result = array(
            'item' => $article,
            'meta' => [
                'title' => $this->title,
                'description' => $this->description,
                'keywords' => $this->keywords,
                'ogTitle' => $this->title,
                'ogType' => 'article',
                'ogDescription' => $this->description,
                'ogImage' => $ogImage,
                'ogSiteName' => '',
                'ogUrl' => '',
                'twitterCard' => 'summary_large_image',
                'twitterTitle' => $this->title,
                'twitterDescription' =>  $this->description,
                'twitterUrl' => '',
                'twitterImage' => $ogImage,
                'twitterImageAlt' => '+1 — Проект об устойчивом развитии',
                'twitterSite' => ''
            ],
        );
        if (!@$_GET['scroll']) {
            $result['urlNextPost'] = $this->getNextPosts($article->id);
            $result['blockAnnouncements'] = Helper::getBlockAnnouncements();
        }
        return $result;
    }

    public function getNextPosts($tagId) {

        $nowDate = new DateTime();

        $nowSTR = $nowDate->format('Y-m-d H:i:s');

        $andPostEnabled = " AND b.enabled = 1 AND b.created <= '{$nowSTR}' ";
        $andNewsEnabled = " AND n.enabled = 1 AND n.created <= '{$nowSTR}' ";

        $query = sql_placeholder("(SELECT b.id, bt.url as tagUrl, b.created as created
                                  FROM blogposts AS b
                                  LEFT JOIN blogwriters AS bw ON bw.id=b.writers
                                  LEFT JOIN relations_postitem_tags AS rel ON rel.post_id=b.id
                                  LEFT JOIN partners AS p ON p.id=b.partner
                                  LEFT JOIN spec_projects AS sp ON sp.id=b.spec_project
                                  LEFT JOIN conferences AS conf ON conf.id=b.conference
                                  INNER JOIN blogtags AS bt ON bt.id = b.tags
                                  WHERE
                                        rel.posttag_id = ?
                                    AND b.spec_project = 0
                                    AND (bw.id IS NULL OR bw.useplatforma <> 1)
                                    AND b.type_post NOT IN (?@)
                                    {$andPostEnabled}
                                    ) UNION (
                                    SELECT n.id, 'news' as tagUrl, n.created as created
                                      FROM news AS n
                                 LEFT JOIN blogwriters AS bw ON bw.id=n.writers
                                 LEFT JOIN news_to_tags_relations AS rel ON rel.news_id=n.id
                                INNER JOIN blogtags AS bt ON bt.id = n.tags
                                     WHERE
                                           rel.post_tag_id = ?
                                       AND (bw.id IS NULL OR bw.useplatforma <> 1)
                                       {$andNewsEnabled}
                                    )
                                  ORDER BY created DESC
                                  LIMIT 60", $tagId, array(5, 12, 11, Helper::TYPE_PARTNER_ANNOUNCE), $tagId );
        $this->db->query($query);

        $nextItems = $this->db->results();

        $nextPostUrlList = [];
        foreach ($nextItems AS $ind => $nextItem){
            $nextPostUrlList[] = 'getPost/' . $nextItem->tagUrl . "/" . $nextItem->id . '?' . 'c=' . $ind;
        }
        return $nextPostUrlList;

    }

    private function getHost()
    {
        return $this->config->protocol . $this->config->host;
    }
}
