<?php

class TinyPngImageOptimizer
{
    private static $INIT = false;
    private static $API_KEY;

    public static function init(){
        if (!self::$INIT) {

            $vendorPath = realpath(__DIR__ . '/../vendor/');
            require_once($vendorPath . "/tinify/tinify/lib/Tinify/Exception.php");
            require_once($vendorPath . "/tinify/tinify/lib/Tinify/ResultMeta.php");
            require_once($vendorPath . "/tinify/tinify/lib/Tinify/Result.php");
            require_once($vendorPath . "/tinify/tinify/lib/Tinify/Source.php");
            require_once($vendorPath . "/tinify/tinify/lib/Tinify/Client.php");
            require_once($vendorPath . "/tinify/tinify/lib/Tinify.php");
            self::$INIT = true;
        }
    }

    public static function setApiKey($key)
    {
        TinyPngImageOptimizer::$API_KEY = $key;
    }

    public static function optimize($pathFrom, $pathTo = false)
    {
        try {
            Tinify\setKey(TinyPngImageOptimizer::$API_KEY);
            $source = Tinify\fromFile($pathFrom);
            if (!$pathTo) {
                $pathTo = $pathFrom;
            }
            return $source->toFile($pathTo);
        } catch (Exception $e) {
            return false;
        }
    }
}