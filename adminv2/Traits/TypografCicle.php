<?php

require_once(__DIR__ . '/../../vendor/symfony/polyfill-mbstring/Mbstring.php');
require_once(__DIR__ . '/../../vendor/symfony/polyfill-mbstring/bootstrap.php');
require_once(__DIR__ . '/../RemoteTypograf.php');

trait  TypografCicle
{

    private $_isDebug = false;
    // Символ мягкого переноса
    private $_shySymbol = '&shy;';

    private $_replaceArray = array();

    public $typografReplaceSymbols = array(
        "\n" => "</p>\n<p class='typograf'>",
        "[" => '&lquota;[#',
        ']' => '#]&rquota;'
    );

    // Ограничение типографа на размер 32 Кб
    function getMaxSizeChunk() {
        return 32 * 1024;
    }

    public function getTypografReplaceSymbols()
    {
        return array_keys($this->typografReplaceSymbols);
    }

    public function getTypografReplaceSymbolsBack($clear = true)
    {
        $result = array();
        foreach ($this->typografReplaceSymbols as $ind => $symbol) {
            $result[$ind] = $symbol;
//            if ($ind === ']' ) {
//                $result[$ind] = mb_chr($symbol) . ($clear ? '': ' ');
//            }
        }
        return array_values($result);
    }

    private $_quotaSymbol = '"';

    private $_quotaSymbolBackOpen = "\\( ";
    private $_quotaSymbolBackClose = " \\)";

    public function getQuotaSymbolBack($open = true)
    {
        if ($open) {
            return $this->_quotaSymbolBackOpen;
        } else {
            return  $this->_quotaSymbolBackClose;
        }

    }


    function _replaceQuota($text)
    {
        $isTagOpen = 0;
        $isPreQuota = false;
        $lastQuotaIndex = false;

        $i = -1;
        while ( isset($text[$i+1]) ) {
            $i++;
            if ($text[$i] === ']') {
                $isTagOpen--;
                if ($isTagOpen < 0) $isTagOpen = 0;
                if ($lastQuotaIndex) {
                    $text = substr_replace($text, $this->getQuotaSymbolBack(false), $lastQuotaIndex, 1);
                    $i = $i + strlen($this->getQuotaSymbolBack(false));
                }
                $lastQuotaIndex = false;
            }
            if ($text[$i] === '[') {
                $isTagOpen++;
                $lastQuotaIndex = false;
            }

            if ($isTagOpen > 0) {
                if ($text[$i] === '=') {
                    $isPreQuota = true;
                    if ($lastQuotaIndex) {
                        $text = substr_replace($text, $this->getQuotaSymbolBack(false), $lastQuotaIndex, 1);
                        $i = $i + strlen($this->getQuotaSymbolBack(false)) - strlen($this->_quotaSymbol);
                        $lastQuotaIndex = false;
                    }
                    continue;
                }
                if ( $text[$i] === $this->_quotaSymbol) {
                    if ($isPreQuota) {
                        $text = substr_replace($text, $this->getQuotaSymbolBack(), $i, 1);
                        $i = $i + strlen($this->getQuotaSymbolBack()) - strlen($this->_quotaSymbol);
                        $lastQuotaIndex = false;
                        $isPreQuota = false;
                    } else {
                        $lastQuotaIndex = $i;
                    }
                } elseif ($text[$i] !== ' ') {
                    $isPreQuota = false;
                }
            }
        }

        return $text;
    }

    function _replaceQuotaBack($text)
    {
        $text = str_replace($this->getQuotaSymbolBack(false), $this->_quotaSymbol, str_replace($this->getQuotaSymbolBack(), $this->_quotaSymbol, $text));
        return str_replace(trim($this->getQuotaSymbolBack(false)), $this->_quotaSymbol, str_replace(trim($this->getQuotaSymbolBack()), $this->_quotaSymbol, $text));
    }

    function _replacePathAndEmbed($field, $text) {

        $this->_replaceArray[$field] = array();
        $text = preg_replace_callback("/(\[embed((?>[^\[\]]|\[(?1)\])*)\])/us", function ($matches) use ($field) {
            $embedKey = "embed_replace_". count(@$this->_replaceArray[$field]) . "_counter";
            $this->_replaceArray[$field][$embedKey] = $matches[1];
            return $embedKey;
        }, $text);

        $text = preg_replace_callback('/(url|link)="[^"]+"/us', function ($matches) use ($field) {
            $pathKey = "path_replace_". count(@$this->_replaceArray[$field]) . "_counter";
            $this->_replaceArray[$field][$pathKey] = $matches[0];
            return $pathKey;
        }, $text);

        return $text;
    }

    function _replacePathAndEmbedBack($field, $text) {
        if ($this->_isDebug) {
            print_r($this->_replaceArray);
        }
        return str_replace(array_keys($this->_replaceArray[$field]), array_values($this->_replaceArray[$field]), $text);
    }

    /** Нужна для обработки текста из полей нового React-редактора используется совмество с afterTypograf */
    function beforeTypograf(&$item, $fields)
    {
        if ($this->_isDebug) {
            header('Content-Type: text/plain; charset=utf-8');
            echo "<pre>";
            var_dump($item->body);
            var_dump($item->article_text);
            echo "</pre>";
            echo "<br>";
            echo PHP_EOL;
            echo PHP_EOL;
            echo PHP_EOL;
        }

        $keys = $this->getTypografReplaceSymbols();
        $values = $this->getTypografReplaceSymbolsBack();


        foreach ($fields as $field) {
            $item->$field = $this->_replacePathAndEmbed($field, $item->$field);
            $item->$field = str_replace(array("\r"), array(''), $item->$field);
            $item->$field = '<p>'.str_replace($keys, $values, $item->$field).'</p>';
            $item->$field = preg_replace('/(&lquota;\[#[^\/][^\]]*?#\]&rquota;)/u', '$1 ', $item->$field);
            $item->$field = preg_replace('/(&lquota;\[#\/\w+?#\]&rquota;)/u', ' $1', $item->$field);
            $item->$field = $this->_replaceQuota($item->$field);

        }

        if ($this->_isDebug) {
            echo "<pre>";
            var_dump($item->body);
            var_dump($item->article_text);
            echo "</pre>";
            echo "<br>";
            echo PHP_EOL;
            echo PHP_EOL;
            echo PHP_EOL;
        }
    }

    function getContentFieldsSize(&$obj, array $fields) {
        $sizeFields = array();
        foreach ($fields as $field) {
            $sizeFields[$field] = mb_strlen($obj->$field);
        }
        return  array_sum($sizeFields);
    }

    function separateToChunks(&$item, $fields) {
        $chunks = array();

        $chunkSize = 0;
        $obj = new stdClass();
        $chunks[] = $obj;
        foreach ($fields as $field) {
            $sizeField = strlen($item->$field);
            if ($chunkSize + $sizeField >= $this->getMaxSizeChunk()) {
                if ($chunkSize > 0) {
                    $chunkSize = 0;
                    $obj = new stdClass();
                    $chunks[] = $obj;
                }
                if ($sizeField >= $this->getMaxSizeChunk()) {
                    $splitArray = explode("\n", $item->$field);
                    $bufferCount = 0;
                    foreach ($splitArray as $ind => $part){
                        $partSize = strlen($part);
                        if ($chunkSize + $partSize + 32 >= $this->getMaxSizeChunk()) {
                            if ($chunkSize > 0 && $bufferCount > 0) {
                                $chunkSize = 0;
                                $obj = new stdClass();
                                $chunks[] = $obj;
                            }
                            $bufferCount++;
                        }
                        if (!isset($obj->{$field . '_____' . $bufferCount})) {
                            $obj->{$field . '_____' . $bufferCount} = "";
                        }
                        $obj->{$field.'_____'.$bufferCount} .= "\n".$part;
                        $chunkSize += $partSize;
                    }
                    continue;
                } else {
                    $obj->$field = $item->$field;
                }
            } else {
                $obj->$field = $item->$field;
            }
            $chunkSize += $sizeField;
        }

        return $chunks;
    }

    function typografFields(&$item, array $rawFieldList, array $formatFieldList) {

        $typograg = new RemoteTypograf('UTF-8');

        $s = $this->getContentFieldsSize($item, array_merge($rawFieldList, $formatFieldList));

        if ($s < $this->getMaxSizeChunk()) {
            $typograg->typografFields($item, $rawFieldList, $formatFieldList);
        } else {
            $joinObj = new stdClass();
            $chunksWithHtml = $this->separateToChunks($item, $rawFieldList);
            foreach ($chunksWithHtml as & $ch1) {
                $keys = array_keys(get_object_vars($ch1));
                $typograg->typografFields($ch1, $keys, []);
                foreach ($keys as $key) {
                    $joinObj->$key = $ch1->$key;
                }
            }

            $chunksWithoutHtml = $this->separateToChunks($item, $formatFieldList);
            foreach ($chunksWithoutHtml as & $ch2) {
                $keys = array_keys(get_object_vars($ch2));
                $typograg->typografFields($ch2, [], $keys);
                foreach ($keys as $key) {
                    $joinObj->$key = $ch2->$key;
                }
            }

            $resultObject = new stdClass();
            foreach (get_object_vars($joinObj) as $key => $value) {
                if (preg_match("/_____\d+$/", $key)) {
                    $newKey = preg_replace("/_____\d+$/", '', $key);
                    if (isset($resultObject->$newKey)) {
                        $resultObject->$newKey .= "\n" . $value;
                    } else {
                        $resultObject->$newKey = $value;
                    }
                } else {
                    $resultObject->$key = $value;
                }
            }

            foreach (get_object_vars($resultObject) as $key => $value) {
                $item->$key = $resultObject->$key;
            }
        }

        //Clear &shy;
        foreach (array_merge($rawFieldList, $formatFieldList) as $field) {
            $item->{$field} = str_replace('&shy;', '', $item->{$field});
        }
    }

    function afterTypograf(&$item, $fields)
    {

        if ($this->_isDebug) {
            echo "<pre>";
            var_dump($item->body);
            var_dump($item->article_text);
            echo "</pre>";
            echo "<br>";
            echo PHP_EOL;
            echo PHP_EOL;
            echo PHP_EOL;
        }
        $values = $this->getTypografReplaceSymbols();
        foreach ($fields as $field) {
            $item->$field = preg_replace_callback('/(&lquota;\[#[^\/][^\]]*?#\]&rquota;) /u', function ($matches) {
                return $matches[1];
            }, $item->$field);
            // Пробелов имеет другой код (типографный не разрывный пробел)
            $item->$field = preg_replace_callback('/( | |&nbsp;)(&lquota;\[#\/\w+?#\]&rquota;) /', function ($matches) {
                return $matches[2].$matches[1];
            }, $item->$field);
            // Вынос последнего пробела в шорткоде за закрывающий шорткод
            $item->$field = preg_replace_callback('/( )(&lquota;\[#\/\w+?#\]&rquota;)/u', function ($matches) {
                return $matches[2].$matches[1];
            }, $item->$field);
            $item->$field = preg_replace_callback('/( )(&lquota;\[#\/\w+?#\]&rquota;)/u', function ($matches) {
                return $matches[2];
            }, $item->$field);
            $item->$field = preg_replace_callback('/(&lquota;\[#\/\w+?#\]&rquota;) (\.|,|!|\?|:|;|\))/u', function ($matches) {
                return $matches[1].$matches[2];
            }, $item->$field);
            $item->$field = preg_replace(['/^<p>/u', '/<\/p>$/u', '/<\/p&gt$/u'], '', $item->$field);
            $item->$field = $this->_replaceQuotaBack($item->$field);
            $item->$field = str_replace( $this->getTypografReplaceSymbolsBack(true), $values, $item->$field);
            $item->$field = preg_replace(['/<\/p>\n+/u', '/ (»|“)/u'], ["</p>", '$1'], $item->$field);

            $item->$field = $this->_replacePathAndEmbedBack($field, $item->$field);

            $item->$field = $this->clearEmptyShortCodes($item->$field);
        }

        if ($this->_isDebug) {
            echo "<pre>";
            var_dump($item->body);
            var_dump($item->article_text);
            echo "</pre>";
            echo "<br>";
            echo PHP_EOL;
            echo PHP_EOL;
            echo PHP_EOL;
            die;
        }
    }

    function clearEmptyShortCodes($text) {
        $res = preg_replace('/(\[([a-zA-Z]+?)[^\]]*?\]?\s*?\[\/\2\])/u', '', $text);
        if (!$res) {
            $res = $text;
        }
        return $res;
    }

    function hyphenate($text, $wordLength = 12)
    {
        $splitArray = preg_split('/ /u', $text);

        foreach ($splitArray as $ind => $word) {
            $clearWord = html_entity_decode(strip_tags(str_replace($this->_shySymbol, '', $word)));
            if (mb_strlen(str_replace([$this->_shySymbol, '&nbsp;'], ['', ' '], $clearWord )) > $wordLength) {
                $splitArray[$ind] = str_replace([' '], ['&nbsp;'], $this->_requestHyphenate(str_replace([$this->_shySymbol, '&nbsp;'], ['', ' '], $clearWord )));
            }
        }

        return join(' ', $splitArray);
    }

    function _requestHyphenate($string)
    {
        $result = $string;
        try {
            $host = isset($this->config->hyphenHost) ? $this->config->hyphenHost : 'http://localhost:8071';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $host);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array('text' => $string)));
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            $res = curl_exec($ch);
            curl_close($ch);

            $res = json_decode($res, true);
            if (isset($res['ok'])) {
                $result = $res['result'];
            }

        } catch (Exception $e) {

        }
        return $result;
    }
}
