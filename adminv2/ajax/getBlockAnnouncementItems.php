<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
session_start();
chdir('..');
require_once('Widget.admin.php');
$widget = new Widget();

require_once('BlockAnnouncement.admin.php');
$a=  0;
$postObject = new BlockAnnouncement($a);


$result = $postObject->getBlockAnnouncementItems(@$_GET['block_announcement_id']);


header("Content-type: application/json; charset=UTF-8");
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");
print json_encode($result);
