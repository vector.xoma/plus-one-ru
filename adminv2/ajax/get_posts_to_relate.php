<?php
session_start();
chdir('..');
require_once('Widget.admin.php');
$widget = new Widget();

require_once('BlogPosts.admin.php');
$postsClass = new BlogPosts();
session_write_close();
$filter = array();
if (isset($_GET['item_id']) && isset($_GET['parentType'])) {
    if ($_GET['parentType'] == 'news') {
        $filter['news'] = " AND news.id != " . (int)$_GET['item_id'];
    } else if ($_GET['parentType'] == 'post_tag') {
    } else {
        $filter['post'] = " AND bp.id != " . (int)$_GET['item_id'];
    }
}
$filter['search'] = $_GET['search'];

$posts['data'] = $postsClass->getPostsDataPaging((int)$_GET['start'], (int)$_GET['length'], $filter);
$posts["draw"] = $_GET['draw'];
$posts["recordsTotal"] = $postsClass->getPostsDataPagingCount($filter);
$posts["recordsFiltered"] = $posts["recordsTotal"];

header("Content-type: application/json; charset=UTF-8");
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");
echo json_encode($posts);