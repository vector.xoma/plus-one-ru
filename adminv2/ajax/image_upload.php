<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

chdir('../../');

require_once('Widget.class.php');

$widget = new Widget();

$uploaddir = "/files/photogallerys/";

$targetPath = getcwd() . $uploaddir;


if (!empty($_FILES)) {

    $galleryId = @$_GET['galleryId'];
    $query = sql_placeholder('SELECT * FROM photo_galleries WHERE id=?', $galleryId);
    $widget->db->query($query);
    $gallary = $widget->db->result();

    if (!$gallary->id ) { echo 0; }

    $dateFolder = '' . (new DateTime($gallary->created))->format('Y/m') . '/';

    $tempFile = $_FILES['file']['tmp_name'];

    $tmpFileName = explode(".", $_FILES['file']['name']);
    $fileExt = $tmpFileName[1];

    $newFullFileName = $dateFolder . $galleryId . "_" . microtime(true) . "_image." . $fileExt;

    $targetFile = $targetPath . $newFullFileName;

    if (!$widget->mkdir_custom(dirname($targetFile))) { echo 0;}

    if (move_uploaded_file($tempFile, $targetFile)) {
        // картинок может быть несколько,
        // поэтому необходимо убедиться, что в к данному товару еще небыло загруженно ни одной картинки,
        // а если было - то к имени файла добавить хэш строку

        $query = sql_placeholder('SELECT COUNT(id) AS cnt FROM images WHERE filename=? AND gallery_id=?', $newFullFileName, $galleryId);
        $widget->db->query($query);
        $res = $widget->db->result();

        // запишем в базу картинку товара
        $query = sql_placeholder('INSERT IGNORE INTO images SET filename=?, gallery_id=?', $newFullFileName, $galleryId);
        if ($widget->db->query($query)) {
            $image_id = $widget->db->insert_id();

            $query = sql_placeholder('UPDATE images SET order_num=foto_id WHERE foto_id=?', $image_id);
            $widget->db->query($query);

            echo $galleryId;
        } else {
            echo 0;
        }
    } else {
        echo 0;
    }
} else {
    echo 0;
}
