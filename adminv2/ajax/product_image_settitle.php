<?php
chdir('../');

require_once('Widget.admin.php');
require_once('RemoteTypograf.php');

$widget = new Widget();

$galleryId = $_POST['galleryId'];
$photo_id   = $_POST['photo_id'];

$type = $_POST['type'];
$value = $_POST['value'];

if ($galleryId!="") {

    // сохраняем подпись к картинке
    if ($type == 'image_name'  || $type == 'image_author'){

        $item = new stdClass();
        $item->value  = $value;
        $typograf = new RemoteTypograf('UTF-8');
        $typograf->typografFields($item, [], ['value']);

        $query = sql_placeholder("UPDATE images SET {$type}=?  WHERE gallery_id=? AND id=?", $item->value, $galleryId, intval($photo_id));
        $widget->db->query($query);
        $result = true;
    }

    print json_encode($result);
}
