<?php
session_start();
chdir('..');
require_once('Widget.admin.php');
$widget = new Widget();

require_once('BlogPosts.admin.php');
$postsClass = new BlogPosts();

$postId = $_POST['postId'];
$postType = $_POST['postType'];
$parentId = $_POST['parentId'];
$parentType = $_POST['parentType'];

// пишем связку
$res = $postsClass->setLinkedPost($postId, $parentId, $postType, $parentType);
// показываем уже связанные
$linkedPosts = $postsClass->getLinkedPost($parentId, $parentType);

$widget->smarty->assign('linkedPosts', $linkedPosts);
$widget->smarty->assign('postTypes', $postsClass->POST_TYPES);
$result['html'] = $widget->smarty->fetch('include/include_already_linked_posts.tpl');

header("Content-type: application/json; charset=UTF-8");
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");
echo json_encode($result);