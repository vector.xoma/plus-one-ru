<?php
session_start();
chdir('..');
require_once('Widget.admin.php');
$widget = new Widget();

require_once('SettingWidgets.admin.php');
$settingWidgetClass = new SettingWidgets();

$result = array('ok' => true);

$orderIds = $_POST['ids'];
$settingWidgetClass->sortable($orderIds);

header("Content-type: application/json; charset=UTF-8");
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");
echo json_encode($result);