<?php
error_reporting(E_ERROR);
ini_set("display_errors", 1);
	session_start();
	chdir('..'); 
	require_once('Widget.admin.php');
    $widget = new Widget();
    require_once('BlogPosts.admin.php');
    $bp = new BlogPosts();

    $totalRecord = $bp->getCountTotalPost();

    for ($i=0; $i<=$totalRecord; $i = $i+100){

        echo "\n================== " . $i . " =======================\n";
        $r = $bp->getPostByPage($i);
        foreach ($r AS $v){

            $newBody = str_ireplace('http://plus-one.ru/', 'https://plus-one.ru/', $v->body);
            $newLead = str_ireplace('http://plus-one.ru/', 'https://plus-one.ru/', $v->lead);
            $newHeader = str_ireplace('http://plus-one.ru/', 'https://plus-one.ru/', $v->header);

            if ($bp->setNewPostToHttps($v->id, $newHeader, $newBody, $newLead)){
                echo "Record #" . $v->id . " has updated; \n";
            }


        }
    }

    echo "complete";
    exit();
?>