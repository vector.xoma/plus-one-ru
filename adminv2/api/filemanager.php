<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
session_start();
require_once('filemanager_helper.php');

chdir('..');
require_once('Widget.admin.php');
$widget = new Widget();

$dir = $widget->config->fileStorage['localRootFilePath'];
$configFileFix = array(
    'convert_spaces' => true,
    'replace_with' => '_',
    'transliteration' => true,
    'lower_case' => true,
    'empty_filename' => false,
);

// создание папки
if (@$_GET['command'] == 'create_folder' && isset($_GET['folder_name']) && isset($_GET['parent_folder'])){
    mkdir(FileManagerHelper::_clearSlashes($dir . $_GET['parent_folder'] . "/" . $_GET['folder_name']), 0777);
    $dir = $dir . $_GET['parent_folder'] ;
}

// удаление
if (@$_GET['command'] == 'delete' && isset($_GET['name']) && isset($_GET['parent_folder'])){
    FileManagerHelper::rm(FileManagerHelper::_clearSlashes($dir . $_GET['parent_folder'] . "/" . $_GET['name']));
    $dir = $dir . $_GET['parent_folder'];
}

// переименование
if (@$_GET['command'] == 'rename' && isset($_GET['new_name']) && isset($_GET['old_name']) && isset($_GET['parent_folder'])){
    $oldName = $dir . $_GET['parent_folder'] . "/" . $_GET['old_name'];
    $newName = $dir . $_GET['parent_folder'] . "/" . $_GET['new_name'];

    rename(FileManagerHelper::_clearSlashes($oldName), FileManagerHelper::_clearSlashes($newName));

    $dir = $dir . $_GET['parent_folder'];
}


$dir = FileManagerHelper::_clearSlashes($dir);

$fileList = array();


if(@$_REQUEST['parent_folder']){
    $path =  FileManagerHelper::_clearSlashes("/files/" . $_REQUEST['parent_folder']);
}
else{
    $path = "/files";
}



if (@$_REQUEST['dir'] && @$_REQUEST['dir'] != ''){
    if ($_REQUEST['dir'] != '.' || $_REQUEST['dir'] != '..'){
        $dir    = $dir . $_REQUEST['dir'];
    }
}

if ($_FILES) {

    $uploaddir = $dir;
    $tryCounter = 0;
    foreach ($_FILES AS $files){
        // для преобразования символов кириллицы кодируем в URL и обратно
        $fileName = rawurldecode(basename(rawurlencode($files['name'])));
        $fileFullName = FileManagerHelper::_clearSlashes($uploaddir . "/" . FileManagerHelper::fix_filename($fileName, $configFileFix));
        while ( file_exists($fileFullName) &&  $tryCounter++ < 100) {
            $pathInfo = pathinfo($fileName);
            $fileName = $pathInfo['filename']. '-'. $tryCounter . '.' . $pathInfo['extension'];
            $fileFullName =  $uploaddir . "/" . $fileName;
        }
        move_uploaded_file($files['tmp_name'], $fileFullName);
    }
}

$allovedFileExtension = ['png', 'PNG',
    'jpg', 'JPG',
    'jpeg', 'JPEG',
    'gif', 'GIF',
    'bmp', 'BMP',
    'svg', 'SVG',
    'tif', 'TIF',
    'tiff', 'TIFF',
    'pdf', 'PDF',
    'doc', 'DOC',
    'docx', 'DOCX',
    'rar', 'RAR',
    'zip', 'ZIP',
    'txt', 'TXT'
    ];

$tmpFiles = scandir($dir);
$isRootFilesDir = $widget->config->fileStorage['localRootFilePath'] === $dir;
$hiddenFolders = array( 'about',
    'banners',
    'blogposts',
    'conferences',
    'events',
//    'images',
    'photogallerys',
    'push',
    'spec_project',
    'tags',
    'writers',
    'news'
);

foreach ($tmpFiles AS $file){

    if ($file != "." && $file != ".." && $file != ".empty") {

        $fileTime = 0;
        $fileType = 'folder';
        if (is_file($dir . "/" . $file)){
            $fileType = 'file';
            $fileTime = filemtime($dir . "/" . $file);
        }
        else{
            if ($isRootFilesDir) {
                if (in_array($file, $hiddenFolders)) {
                    continue;
                }
            }
            $fileTime = filemtime($dir);
        }

        if (@$_REQUEST['dir']){
            $pathToFile = $path . "/" . $_REQUEST['dir'] . "/" . $file;
        }
        else{
            $pathToFile = $path . "/" . $file;
        }

        $fileInfo = pathinfo($pathToFile);

        if (in_array(@$fileInfo['extension'], $allovedFileExtension) && $fileType == 'file'){
            $fileList[] = [
                'fileName' => $file,
                'fullFileName' => $pathToFile,
                'fileTime' => $fileTime,
                'extension' => $fileInfo['extension'],
                'humanFileTime' => date('d.m.Y', $fileTime),
                'humanFileSize' => FileManagerHelper::humanFilesize(filesize($dir . "/" . $file)),
                'fileSize' => filesize($dir . "/" . $file),
                'type' => $fileType,
            ];
        }
        elseif ($fileType == 'folder'){
            $fileList[] = [
                'fileName' => $file,
                'fullFileName' => $pathToFile,
                'fileTime' => $fileTime,
                'extension' => null,
                'humanFileTime' => date('d.m.Y', $fileTime),
                'humanFileSize' => FileManagerHelper::humanFilesize(filesize($dir . "/" . $file)),
                'fileSize' => filesize($dir . "/" . $file),
                'type' => $fileType,
            ];
        }


    }
}

header("Content-type: application/json; charset=UTF-8");
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");
echo json_encode($fileList);

