<?php

class FileManagerHelper {

    static function sanitize($str)
    {
        return strip_tags(htmlspecialchars($str));
    }

    static function fix_strtolower($str)
    {
        if (function_exists('mb_strtoupper'))
        {
            return mb_strtolower($str);
        }
        else
        {
            return strtolower($str);
        }
    }

    static function fix_filename($str, $config, $is_folder = false)
    {
        $str = self::sanitize(trim($str));
        if ($config['convert_spaces'])
        {
            $str = str_replace(' ', $config['replace_with'], $str);
        }

        if ($config['transliteration'])
        {
            if (!mb_detect_encoding($str, 'UTF-8', true))
            {
                $str = utf8_encode($str);
            }
            if (function_exists('transliterator_transliterate'))
            {
                $str = transliterator_transliterate('Russian-Latin/BGN', $str);
            }
            else
            {
                $cyr = [
                    'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п',
                    'р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',
                    'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П',
                    'Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я'
                ];
                $lat = [
                    'a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p',
                    'r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya',
                    'A','B','V','G','D','E','Io','Zh','Z','I','Y','K','L','M','N','O','P',
                    'R','S','T','U','F','H','Ts','Ch','Sh','Sht','A','I','Y','e','Yu','Ya'
                ];
                $str = str_replace($cyr, $lat, $str);
            }
            $str = preg_replace("/[^a-zA-Z0-9\.\[\]_| -]/", '', $str);
        }

        if ($config['lower_case'])
        {
            $str = self::fix_strtolower($str);
        }
        $str = str_replace(array( '"', "'", "/", "\\" ), "", $str);
        $str = strip_tags($str);

        // Empty or incorrectly transliterated filename.
        // Here is a point: a good file UNKNOWN_LANGUAGE.jpg could become .jpg in previous code.
        // So we add that default 'file' name to fix that issue.
        if (!$config['empty_filename'] && strpos($str, '.') === 0 && $is_folder === false)
        {
            $str = 'file' . $str;
        }

        return trim($str);
    }


    static function humanFilesize($bytes, $decimals = 2) {
        $sz = 'bkmgtp';
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
    }

    static function rm($dir) {
        if (is_dir($dir)) {
            $iterator = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($dir),
                RecursiveIteratorIterator::CHILD_FIRST
            );

            foreach ($iterator as $path) {
                if (in_array(basename((string)$path), ['.', '..'])) {continue;}
                if ($path->isDir()) {
                    rmdir((string)$path);
                } else {
                    unlink((string)$path);
                }
            }
            rmdir($dir);
        } elseif(is_file($dir)) {
            unlink($dir);
        }
    }

    static function _clearSlashes ($path) {
        return preg_replace("/[\/]{2,}/u", '/', $path);
    }
}
