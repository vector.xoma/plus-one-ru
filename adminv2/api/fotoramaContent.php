<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
chdir('../');

require_once('Widget.admin.php');

$widget = new Widget();

$galleryId = $_GET['gallery_id'];


$result = '';

if (!empty($galleryId)){

    $query = sql_placeholder('SELECT id, gallery_id, filename, image_name, image_author 
            FROM images WHERE gallery_id=? ORDER BY order_num DESC',
        $galleryId);
    $widget->db->query($query);
    $imagesList = $widget->db->results();

    foreach ($imagesList AS $k=>$image){
        $imagesList[$k]->filename = $widget->config->protocol . $_SERVER['HTTP_HOST'] . '/' . $widget->config->path['photogallerys'] . '/' . $image->filename;
    }
}

header("Content-type: application/json; charset=UTF-8");
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");
print json_encode($imagesList);
