<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
session_start();
chdir('..');
require_once('Widget.admin.php');
$widget = new Widget();

require_once('Permissions.admin.php');
$postObject = new Permissions($widget);
session_write_close();
$users = $postObject->getUsers();

header("Content-type: application/json; charset=UTF-8");
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");
echo json_encode(array('ok' => true, 'users' => $users));
