<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
chdir('../');

require_once('Widget.admin.php');

$widget = new Widget();

$galeriesList = array();

$query = sql_placeholder('SELECT * FROM photo_galleries ORDER BY created DESC');
$widget->db->query($query);
$galeriesList = $widget->db->results();

//    $widget->smarty->assign('galeriesList', $galeriesList);
//
//    $result = $widget->smarty->fetch('include/galleriesList.tpl');

header("Content-type: application/json; charset=UTF-8");
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");
print json_encode($galeriesList);

?>
