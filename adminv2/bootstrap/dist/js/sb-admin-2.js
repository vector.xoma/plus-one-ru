$(document).ready(function($) {
    $("#usrp").val('');
});

$(function() {
    $('#side-menu').metisMenu();
    $('#products_categories_tree').metisMenu({
        doubleTapToGo: true,
        preventDefault: false,
        collapsingClass: 'collapsing'
    });
    $("#products_categories_tree_in_productlist").metisMenu({
        toggle: false,
        doubleTapToGo: true,
        preventDefault: false,
        collapsingClass: 'collapsingg'
    });
});


//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            height = height - 120;
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    var element = $('ul.nav a').filter(function() {
        return this.href == url || url.href.indexOf(this.href) == 0;
    }).addClass('active').parent().parent().addClass('in').parent();
    if (element.is('li')) {
        element.addClass('active');
    }
});




    // сортировка настроек сайта
    $(function() {
        $("#sortable_settings").sortable({
            axis: 'y',
            update: function (event, ui) {
                var values  = $(this).sortable('toArray');
                var table   = "settings";
                $.ajax({
                    url: "ajax/save_sort.php",
                    data: {values:values, table: table},
                    type: "POST",
                    dataType: 'json',
                    success: function(data)
                    {
                        //console.log(data);
                    }
                });
            }
        });
    });

    $.datetimepicker.setLocale('ru');
    $('input#calendar').datetimepicker({
        format:'d.m.Y H:i',
        step: 1,
    });

    //$('input#calendar').datepicker({
    //    format: "dd.mm.yyyy",
    //    language: "ru-RU",
    //    autoclose: true,
    //    todayHighlight: true
    //});

//    // удаление картинки
//    $(document).on('click', '#button_delete_largeimage', function(e)
//    {
//        $("#large_image").attr("src", "images/no_foto.gif");
//        $("#large_image").css("width", "auto");
//        $("#delete_large_image").val("1");
//
//        return false;
//    });
//
//
//$(document).on('click', '#button_delete_image_1_1', function(e)
//{
//    posttag_id
//
//    $("#image_1_1").attr("src", "images/no_foto.gif");
//    $("#image_1_1").css("width", "auto");
//    $("#delete_image_1_1").val("1");
//
//    return false;
//});



//    // удаление картинки Авторы
   $(document).on('click', '.js-button_delete_largeimage', function(e)
   {
       $("#large_image").attr("src", "images/no_foto.gif").css("width", "auto");
       $("#delete_large_image").val("1");

       return false;
   });
//    // удаление картинки Авторы
   $(document).on('click', '.js-button_delete_stub_image', function(e)
   {
       $("#stub_image").attr("src", "images/no_foto.gif").css("width", "auto");
       $("#delete_stub_image").val("1");

       return false;
   });

//    // удаление картинки Авторы
   $(document).on('click', '.js-button_delete_announcement_img', function(e)
   {
       $("#announcement_img").attr("src", "images/no_foto.gif").css("width", "auto");
       $("#delete_announcement_img").val("1");

       return false;
   });


    // удаление картинки товара
    $(document).on('click', '.delete-photo', function(e)
    {
        var product_sku = $(this).attr('productid');
        var photo_id = $(this).attr('photoId') * 1;

        $.ajax({
            url: "ajax/product_image_delete.php",
            data: {product_sku:product_sku, photo_id: photo_id},
            type: "POST",
            dataType: 'json',
            success: function(data)
            {
                //$("#product_images").html(data);
                $.ajax({
                    url: "ajax/product_image_show.php",
                    data: {product_sku:product_sku},
                    type: "POST",
                    dataType: 'json',
                    success: function(data)
                    {
                        $("#product_images").html(data);
                    }
                });
            }
        });

        return false;
    });

    /**
     * Делаем картинку главной
     */
    $(document).on('click', '.set-main-photo', function(e)
    {
        var product_sku = $(this).data('product-id');
        var photo_id = $(this).data('photo-id') * 1;

        $.ajax({
            url: "ajax/product_image_set_main.php",
            data: {product_sku:product_sku, photo_id: photo_id},
            type: "POST",
            dataType: 'json',
            success: function(data)
            {
                //$("#product_images").html(data);
                $.ajax({
                    url: "ajax/product_image_show.php",
                    data: {product_sku:product_sku},
                    type: "POST",
                    dataType: 'json',
                    success: function(data)
                    {
                        $("#product_images").html(data);
                    }
                });
            }
        });

        return false;
    });


    // удаление тегов второго уровня из статьи
    $(document).on('click', '#clear-list_tag', function(e)
    {
        var postId  = $(this).data('postid');
console.log(postId);
        $.ajax({
            url: "ajax/deletePostTags.php",
            data: {postId:postId},
            type: "POST",
            dataType: 'json',
            success: function(data)
            {
                $("#available_tags").html('');
            }
        });

        return false;
    });


    // удаление картинки
    $(document).on('click', '.deletePhoto', function(e)
    {
        var galleryId = $(this).attr('galleryid');
        var photo_id = $(this).attr('photoId') * 1;

        $.ajax({
            url: "ajax/product_image_delete.php",
            data: {galleryId:galleryId, photo_id: photo_id},
            type: "POST",
            dataType: 'json',
            success: function(data)
            {
                //$("#product_images").html(data);
                $.ajax({
                    url: "ajax/product_image_show.php",
                    data: {galleryId:galleryId},
                    type: "POST",
                    dataType: 'json',
                    success: function(data)
                    {
                        $("#product_images").html(data);
                    }
                });
            }
        });

        return false;
    });

    // сохранение подписи к картинке
    $(document).on('click', '.saveImageTitle', function(e)
    {
        let $but = $(e.currentTarget);
        $but.button('loading');
        var galleryId = $(this).attr('galleryid');

        $.each($('#imagesList input'),function(){

            var obj = this;

            if (obj.value != ''){
                $.ajax({
                    url: "ajax/product_image_settitle.php",
                    data: {galleryId:galleryId, photo_id: obj.id, type: obj.name, value: obj.value},
                    type: "POST",
                    dataType: 'json',
                    success: function(data)
                    {
                        //$("#product_images").html(data);
                    }
                });
            }
        });

        setTimeout(function () {

            $.ajax({
                url: "ajax/product_image_show.php",
                data: {galleryId: galleryId},
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    $("#product_images").html(data);
                },
                complete: function (resp) {
                    $but.button('loading');
                }
            });
        }, 800);
        return false;
    });

    // сохранение тега в "облаке тегов"
    $(document).on('click', '#saveTag', function(e)
    {
        var tag = $("#newtagname").val();
        var itemId = $("#posttag_id").val();

        $.ajax({
            url: "ajax/add_tag.php",
            data: {tag:tag, itemId:itemId},
            type: "POST",
            dataType: 'json',
            success: function(data)
            {
                $("#available-tags").html(data);
                $("#newtagname").val('');
            }
        });

        return false;
    });

    $(document).on('change', 'input[type=radio][name=type_post]', function(e)
    {
        // разблокируем все блоки во всех местах
        $("#blocks option, #blocks_blog option, #blocks_author option").each(function(a){
            $(this).removeAttr("disabled");
        });

        if (this.value == '1') { //  Фото
            $("#header_label").html('Текст подзаголовка');
        }
        else if (this.value == '2') { // Цитата
            $("#header_label").html('Текст цитаты');

            // у этого типа поста - блочим все блоки с геометрией 1/4
            $("#blocks option, #blocks_blog option, #blocks_author option").each(function(){
                if ($(this).val() == '1/4'){
                    $(this).attr("disabled","disabled");
                }
            });
        }
        else if (this.value == '3'){ // Цв. Карточка
            $("#header_label").html('Текст цитаты');

            // у этого типа поста - блочим все блоки с геометрией 1/4 и 1/1
            $("#blocks option, #blocks_blog option, #blocks_author option").each(function(){
                if ($(this).val() == '1/4' || $(this).val() == '1/1'){
                    $(this).attr("disabled","disabled");
                }
            });
        }

        return false;
    });

//
$(document).on('change', '#writers_input', function(e)
{
    if ($(this).val() != 0){
        $("#writers_label").removeClass('text-danger');
        $("#writers_label").addClass('text-success');
    }
    else{
        $("#writers_label").removeClass('text-success');
        $("#writers_label").addClass('text-danger');
    }

    return false;
});
//
$(document).on('keyup', 'input[name=url]', function(e)
{
    if ($(this).val() != ''){
        $("#url_label").removeClass('text-danger');
        $("#url_label").addClass('text-success');
    }
    else{
        $("#url_label").removeClass('text-success');
        $("#url_label").addClass('text-danger');
    }

    return false;
});

$(document).on('keyup', 'input#header_rss_input', function(e)
{
    if ($(this).val() != ''){
        $("#header_rss_label").removeClass('text-danger');
        $("#header_rss_label").addClass('text-success');
    }
    else{
        $("#header_rss_label").removeClass('text-success');
        $("#header_rss_label").addClass('text-danger');
    }

    return false;
});
//
//
$(document).on('change', 'input#calendar', function(e)
{
    if ($(this).val() != ''){
        $("#created_label").removeClass('text-danger');
        $("#created_label").addClass('text-success');
    }
    else{
        $("#created_label").removeClass('text-success');
        $("#created_label").addClass('text-danger');
    }

    return false;
});

$(document).on('change', 'input[name=tags]', function(e)
{
    e.preventDefault();
    if ($(e.currentTarget).val()){
        $(".js_tags_label").removeClass('text-danger').addClass('text-success');
    } else {
        $(".js_tags_label").removeClass('text-success').addClass('text-danger');
    }

    return false;
});

$(document).on('change', '#font_input', function(e)
{
    e.preventDefault();
    if ($(e.currentTarget).val()){
        $("#font_label").removeClass('text-danger').addClass('text-success');
    } else {
        $("#font_label").removeClass('text-success').addClass('text-danger');
    }

    return false;
});

$(document).on('change', '#format_input', function(e)
{
    e.preventDefault();
    if ($(e.currentTarget).val()){
        $("#format_label").removeClass('text-danger').addClass('text-success');
    } else {
        $("#format_label").removeClass('text-success').addClass('text-danger');
    }

    return false;
});

if (document.blogpost) {
    let ArrayMeta = ['[name=name]', '#meta_title', '#meta_description', '#meta_keywords', '#header_rss_input', '#header_donate_form_input'];

    ArrayMeta.forEach(function (tag) {
        let $tag = $(tag);
        if ($tag.length > 0) {
            if ($tag.closest('.form-group').find('.js_counters').length === 0) {
                $('<small class="js_counters text-muted pull-right"></small>').insertAfter($(tag));
            }
            $(document.blogpost).on('change keyup focus', tag, function (e) {
                let $input = $(e.currentTarget);
                let val = "" + $input.val();
                let $counter = $input.closest('.form-group').find('.js_counters');
                $counter.html(val.length + ' символов');
                if ($counter.data('max-length') && val.length > $counter.data('max-length')) {
                    $counter.addClass('text-danger');
                }
            });
        }
    });
    $(ArrayMeta.join(',')).trigger('change');
}

    function setHeaderRssContent(str){
        str = str.trim();
        str = str.replace(/<\/?[^>]+>/g,'');
        str = str.replace(/(\&mdash;)/ig, '—');
        str = str.replace(/(\&laquo;)/ig, '«');
        str = str.replace(/(\&raquo;)/ig, '»');
        str = str.replace(/(\&copy;)/ig, '©');
        str = str.replace(/(\&reg;)/ig, '®');
        str = str.replace(/(\&pound;)/ig, '£');
        str = str.replace(/(\&ndash;)/ig, ' ');
        str = str.replace(/(\&trade;)/ig, '™');
        str = str.replace(/(\&cent;)/ig, '¢');
        str = str.replace(/(\&yen;)/ig, '¥');
        str = str.replace(/(\&uml;)/ig, '¨');
        str = str.replace(/(\&deg;)/ig, '°');
        str = str.replace(/(\&plusmn;)/ig, '±');
        str = str.replace(/(\&sup1;)/ig, '¹');
        str = str.replace(/(\&sup2;)/ig, '²');
        str = str.replace(/(\&sup3;)/ig, '³');
        str = str.replace(/(\&acute;)/ig, '´');
        str = str.replace(/(\&micro;)/ig, 'µ');
        str = str.replace(/(\&para;)/ig, '¶');
        str = str.replace(/(\&middot;)/ig, '·');
        str = str.replace(/(\&ordm;)/ig, 'º');
        str = str.replace(/(\&frac14;)/ig, '¼');
        str = str.replace(/(\&frac12;)/ig, '½');
        str = str.replace(/(\&frac34;)/ig, '¾');
        str = str.replace(/(\&times;)/ig, '×');
        str = str.replace(/(\&ordf;)/ig, 'ª');
        str = str.replace(/(\&sect;)/ig, '§');
        str = str.replace(/(\&curren;)/ig, '¤');
        str = str.replace(/(\&rsaquo;)/ig, '›');
        str = str.replace(/(\&lsaquo;)/ig, '‹');
        str = str.replace(/(\&tilde;)/ig, '˜');
        str = str.replace(/(\&bull;)/ig, '•');
        str = str.replace(/(\&rdquo;)/ig, '”');
        str = str.replace(/(\&ldquo;)/ig, '“');
        str = str.replace(/(\&rsquo;)/ig, '’');
        str = str.replace(/(\&lsquo;)/ig, '‘');
        str = str.replace(/(\&permil;)/ig, '‰');
        str = str.replace(/(\&circ;)/ig, 'ˆ');
        str = str.replace(/(\&hellip;)/ig, '…');
        str = str.replace(/(\&bdquo;)/ig, '„');
        str = str.replace(/(\&sbquo;)/ig, '‚');
        str = str.replace(/(\&euro;)/ig, '€');
        str = str.replace(/(\&fnof;)/ig, 'ƒ');
        str = str.replace(/(\&macr;)/ig, '¯');
        str = str.replace(/(\&cedil;)/ig, '¸');
        str = str.replace(/(\&divide;)/ig, '÷');
        str = str.replace(/(\&amp;)/ig, ' ');
        str = str.replace(/(\&quot;)/ig, ' ');
        str = str.replace(/(\&lt;)/ig, '');
        str = str.replace(/(\&gt;)/ig, '');
        str = str.replace(/(\&nbsp;)/ig, ' ');

        return str;
    }


    function clearStrOfHtmlEntities(str){
        str = str.trim();
        str = str.replace(/<\/?[^>]+>/g,'');
        str = str.replace(/(\&laquo;)/ig, '');
        str = str.replace(/(\&raquo;)/ig, ' ');
        str = str.replace(/(\&mdash;)/ig, ' ');
        str = str.replace(/(\&ndash;)/ig, ' ');
        str = str.replace(/(\&amp;)/ig, ' ');
        str = str.replace(/(\&pound;)/ig, ' ');
        str = str.replace(/(\&trade;)/ig, ' ');
        str = str.replace(/(\&cent;)/ig, ' ');
        str = str.replace(/(\&yen;)/ig, ' ');
        str = str.replace(/(\&uml;)/ig, ' ');
        str = str.replace(/(\&deg;)/ig, ' ');
        str = str.replace(/(\&plusmn;)/ig, ' ');
        str = str.replace(/(\&sup1;)/ig, ' ');
        str = str.replace(/(\&sup2;)/ig, ' ');
        str = str.replace(/(\&sup3;)/ig, ' ');
        str = str.replace(/(\&acute;)/ig, ' ');
        str = str.replace(/(\&micro;)/ig, ' ');
        str = str.replace(/(\&para;)/ig, ' ');
        str = str.replace(/(\&middot;)/ig, ' ');
        str = str.replace(/(\&ordm;)/ig, ' ');
        str = str.replace(/(\&frac12;)/ig, ' ');
        str = str.replace(/(\&frac34;)/ig, ' ');
        str = str.replace(/(\&times;)/ig, ' ');
        str = str.replace(/(\&ordf;)/ig, ' ');
        str = str.replace(/(\&sect;)/ig, ' ');
        str = str.replace(/(\&curren;)/ig, ' ');
        str = str.replace(/(\&rsaquo;)/ig, ' ');
        str = str.replace(/(\&lsaquo;)/ig, ' ');
        str = str.replace(/(\&tilde;)/ig, ' ');
        str = str.replace(/(\&bull;)/ig, ' ');
        str = str.replace(/(\&rdquo;)/ig, ' ');
        str = str.replace(/(\&ldquo;)/ig, ' ');
        str = str.replace(/(\&rsquo;)/ig, ' ');
        str = str.replace(/(\&lsquo;)/ig, ' ');
        str = str.replace(/(\&permil;)/ig, ' ');
        str = str.replace(/(\&circ;)/ig, ' ');
        str = str.replace(/(\&hellip;)/ig, ' ');
        str = str.replace(/(\&bdquo;)/ig, ' ');
        str = str.replace(/(\&sbquo;)/ig, ' ');
        str = str.replace(/(\&euro;)/ig, ' ');
        str = str.replace(/(\&fnof;)/ig, ' ');
        str = str.replace(/(\&macr;)/ig, ' ');
        str = str.replace(/(\&cedil;)/ig, ' ');
        str = str.replace(/(\&reg;)/ig, '-');
        str = str.replace(/(\&copy;)/ig, '-');
        str = str.replace(/(\&quot;)/ig, '-');
        str = str.replace(/(\&lt;)/ig, '-');
        str = str.replace(/(\&gt;)/ig, '-');
        str = str.replace(/(\&nbsp;)/ig, '-');

        return str;
    }

    // транслитерация
    function translit(str)
    {
        str = clearStrOfHtmlEntities(str);

        var cyr2latChars = [['а', 'a'], ['б', 'b'], ['в', 'v'], ['г', 'g'],
            ['д', 'd'],  ['е', 'e'], ['ё', 'yo'], ['ж', 'zh'], ['з', 'z'],
            ['и', 'i'], ['й', 'y'], ['к', 'k'], ['л', 'l'],
            ['м', 'm'],  ['н', 'n'], ['о', 'o'], ['п', 'p'],  ['р', 'r'],
            ['с', 's'], ['т', 't'], ['у', 'u'], ['ф', 'f'],
            ['х', 'h'],  ['ц', 'c'], ['ч', 'ch'],['ш', 'sh'], ['щ', 'shch'],
            ['ъ', ''],  ['ы', 'y'], ['ь', ''],  ['э', 'e'], ['ю', 'yu'], ['я', 'ya'],

            ['А', 'a'], ['Б', 'b'], ['В', 'v'], ['Г', 'g'],
            ['Д', 'd'], ['Е', 'e'], ['Ё', 'yo'],['Ж', 'zh'], ['З', 'z'],
            ['И', 'i'], ['Й', 'y'], ['К', 'k'], ['Л', 'l'],
            ['М', 'm'], ['Н', 'n'], ['О', 'o'],  ['П', 'p'],  ['Р', 'r'],
            ['С', 's'], ['Т', 't'], ['У', 'u'], ['Ф', 'f'],
            ['Х', 'h'], ['Ц', 'c'], ['Ч', 'ch'], ['Ш', 'sh'], ['Щ', 'shch'],
            ['Ъ', ''],  ['Ы', 'y'], ['Ь', ''],  ['Э', 'e'], ['Ю', 'yu'],
            ['Я', 'ya'],

            ['a', 'a'], ['b', 'b'], ['c', 'c'], ['d', 'd'], ['e', 'e'],
            ['f', 'f'], ['g', 'g'], ['h', 'h'], ['i', 'i'], ['j', 'j'],
            ['k', 'k'], ['l', 'l'], ['m', 'm'], ['n', 'n'], ['o', 'o'],
            ['p', 'p'], ['q', 'q'], ['r', 'r'], ['s', 's'], ['t', 't'],
            ['u', 'u'], ['v', 'v'], ['w', 'w'], ['x', 'x'], ['y', 'y'],
            ['z', 'z'],

            ['A', 'a'], ['B', 'b'], ['C', 'c'], ['D', 'd'],['E', 'e'],
            ['F', 'f'],['G', 'g'],['H', 'h'],['I', 'i'],['J', 'j'],['K', 'k'],
            ['L', 'l'], ['M', 'm'], ['N', 'n'], ['O', 'o'],['P', 'p'],
            ['Q', 'q'],['R', 'r'],['S', 's'],['T', 't'],['U', 'u'],['V', 'v'],
            ['W', 'w'], ['X', 'x'], ['Y', 'y'], ['Z', 'z'],

            [' ', '-'],['0', '0'],['1', '1'],['2', '2'],['3', '3'],
            ['4', '4'],['5', '5'],['6', '6'],['7', '7'],['8', '8'],['9', '9'],
            ['-', '-'], ['"', '-']];

        var newStr = String();

        for (var i = 0; i < str.length; i++)
        {
            ch = str.charAt(i);
            var newCh = '';
            for (var j = 0; j < cyr2latChars.length; j++)
            {
                if (ch == cyr2latChars[j][0])
                {
                    newCh = cyr2latChars[j][1];
                }
            }
            // Если найдено совпадение, то добавляется соответствие, если нет - пустая строка
            newStr += newCh;
        }
        // Удаляем повторяющие знаки - Именно на них заменяются пробелы.
        // Так же удаляем символы перевода строки, но это наверное уже лишнее
        return newStr.replace(/[-]{2,}/gim, '-').replace(/\n/gim, '');
    }


function renderDataTable(itemId, parentType){

    $('#relatedPostDataTable').DataTable({
        serverSide: true,
        searching: true,
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.20/i18n/Russian.json",
            searchPlaceholder: "Минимум 2 символа"
        },
        order: [[0, "desc"]],
        ajax: 'ajax/get_posts_to_relate.php?item_id=' + itemId + '&parentType='+parentType,
        searchDelay: 380,
        columns: [
            {data: 'created'},
            {data: 'postTagsStr'},
            {data: 'writerStr'},
            {data: 'partnerRecordStr'},
            {data: 'name'},
            {data: 'linked'}
        ],
        fixedColumns: true,
        aoColumnDefs: [
            {'bSortable': true,  'targets': [0]},
            {'bSortable': false, 'targets': [1]},
            {'bSortable': false, 'targets': [2]},
            {'bSortable': false, 'targets': [3]},
            {'bSortable': false, 'targets': [4]},
            {'bSortable': false, 'targets': [5]}
        ],
        pageLength: 10
    });
}



var _videoCounter = 0;

function addDynamicVideo(){

    var countRows = $("#countrows").val() * 1;
    var parentVideoId = $("#posttag_id").val() * 1;

    $.ajax({
        url: "ajax/getNewVideoId.php",
        data: {parentVideoId:parentVideoId},
        type: "POST",
        dataType: 'json',
        success: function(data)
        {
            let newVideoIndex = data.newVideoId ? data.newVideoId : 'new_' + (_videoCounter++);

            var el = '<div id="row_' + newVideoIndex +'" class="row disabled" style="border-bottom: 1px solid #C0C0C0; padding: 14px 0; margin: 7px 0">' +
                '<input type="hidden" name="video[' + newVideoIndex +'][id]" value="' + newVideoIndex + '" />' +
                '<input type="hidden" name="video[' + newVideoIndex +'][type]" value="live" />' +
                '<div class="col-lg-12">' +
                '<div class="col-lg-12">' +
                '<div class="form-group"><label>Код для вставки</label><input type="text" name="video[' + newVideoIndex + '][url]" class="form-control disabled" /></div>' +
                '<div class="form-group"><label>Название записи</label><input type="text" name="video[' + newVideoIndex + '][header_interview]" class="form-control disabled" /></div>' +
                '<button onclick="addTimingDivisionVideo(\'' + newVideoIndex + '\'); return false;" class="btn btn-primary btn-xs" type="button">Добавить разбивку по времени</button>' +
                '<button onclick="enabledVideo(\'' + newVideoIndex + '\'); return false;" id="disableButton_' + newVideoIndex + '" class="btn btn-warning btn-xs" type="button">Включить в показ</button>' +
                '<button onclick="deleteVideo(\'' + newVideoIndex + '\'); return false;" class="btn btn-danger btn-xs" type="button">Удалить видео</button>' +
                '<button class="btn btn-default btn-xs pull-right showTimingVideo hidden" type="button" data-toggle="collapse" href="#timingDivision_' + newVideoIndex + '" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-bars"></i></button>' +
                '<div id="timingDivision_' + newVideoIndex + '" class="collapse">' +
                '</div></div></div>';
            $("#dynamic_video").prepend(el);
        }
    });
    return false;
}

function addDynamicVideoRecord(){

    var countRows = $("#countrows").val() * 1;
    var parentVideoId = $("#posttag_id").val() * 1;


    $.ajax({
        url: "ajax/getNewVideoId.php",
        data: {parentVideoId:parentVideoId},
        type: "POST",
        dataType: 'json',
        success: function(data)
        {
            let newVideoId = data.newVideoId ? data.newVideoId : 'new_' + (_videoCounter++);

            var el = '<div id="row_' + newVideoId +'" class="row disabled" style="border-bottom: 1px solid #C0C0C0; padding: 14px 0; margin: 7px 0">' +
                '<input type="hidden" name="video[' + newVideoId +'][id]" value="' + newVideoId + '" />' +
                '<input type="hidden" name="video[' + newVideoId +'][type]" value="record" />' +
                '<div class="col-lg-12">' +
                '<div class="col-lg-12">' +
                '<div class="form-group"><label>Код для вставки</label><input type="text" name="video[' + newVideoId + '][url]" class="form-control disabled" /></div>' +
                '<div class="form-group"><label>Название записи</label><input type="text" name="video[' + newVideoId + '][header_interview]" class="form-control disabled" /></div>' +
                '<button onclick="addTimingDivisionVideo(\'' + newVideoId + '\'); return false;" class="btn btn-primary btn-xs" type="button">Добавить разбивку по времени</button>' +
                '<button onclick="enabledVideo(\'' + newVideoId + '\'); return false;" id="disableButton_' + newVideoId + '" class="btn btn-warning btn-xs" type="button">Включить в показ</button>' +
                '<button onclick="deleteVideo(\'' + newVideoId + '\'); return false;" class="btn btn-danger btn-xs" type="button">Удалить видео</button>' +
                '<button class="btn btn-default btn-xs pull-right showTimingVideo hidden" type="button" data-toggle="collapse" href="#timingDivision_' + newVideoId + '" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-bars"></i></button>' +
                '<div id="timingDivision_' + newVideoId + '" class="collapse">' +
                '</div></div></div>';
            $("#dynamic_video_record").prepend(el);
        }
    });
    return false;
}

function enabledVideo(videoId){
    $.ajax({
        url: "ajax/enableVideo.php",
        data: {videoId:videoId},
        type: "POST",
        dataType: 'json',
        success: function(data)
        {
            if (data == true){
                $("#row_" + videoId).toggleClass('disabled');
                $("#row_" + videoId).find('input').toggleClass('disabled');

                $('#disableButton_' + videoId).html(
                    $('#disableButton_' + videoId).text() === 'Выключить из показа' ? 'Включить в показ' : 'Выключить из показа'
                );
            }
        }
    });
    return false;
}

function deleteVideo(videoId){
    $.ajax({
        url: "ajax/deleteVideo.php",
        data: {videoId:videoId},
        type: "POST",
        dataType: 'json',
        success: function(data)
        {
            if (data == true){
                $("#row_" + videoId).remove();
            }
        }
    });
    return false;
}

var _divisionTimeCounter = 0;
function addTimingDivisionVideo(videoId ){
    videoId = videoId ? videoId : '';
    $.ajax({
        url: "ajax/getNewVideoTimingPartId.php",
        data: {parentVideoId:videoId},
        type: "POST",
        dataType: 'json',
        success: function(data)
        {
            let divisionVideoId = data.divisionVideoId || 'new_' + (_divisionTimeCounter++);
            var el = '<div id="row_division_'+divisionVideoId+'" class="row  disabled" style="border-top: 1px solid #C0C0C0; margin: 7px 0">' +
                '<div class="col-lg-12" style="margin: 15px 0;">' +
                '<input type="hidden" name="video['+videoId+'][divisionTime]['+divisionVideoId+'][id]" value="' + divisionVideoId + '" />' +
                '<div class="form-group">' +
                '<label>Временная метка</label>' +
                '<label class="checkbox-inline">' +
                '<input type="text" name="video['+videoId+'][divisionTime]['+divisionVideoId+'][minute]" class="form-control disabled" style="width:40px; float: left;" value="" />' +
                '</label>' +
                '<label>' +
                '<p>мин</p>' +
                '</label>' +
                '<label class="checkbox-inline">' +
                '<input type="text" name="video['+videoId+'][divisionTime]['+divisionVideoId+'][secunde]" class="form-control disabled" style="width:40px; float: left;" value="" />' +
                '</label>' +
                '<label>' +
                '<p>сек</p>' +
                '</label>' +
                '</div>' +
                '<div class="form-group">' +
                '<label>Подпись</label>' +
                '<input type="text" name="video['+videoId+'][divisionTime]['+divisionVideoId+'][name]" class="form-control disabled" value="" />' +
                '</div>' +
                '<button onclick="enabledTimingPartVideo(\'' + divisionVideoId + '\'); return false;" class="btn btn-warning btn-xs" type="button">Включить в показ</button>' +
                '<button onclick="deleteTimingPartVideo(\'' + divisionVideoId + '\',\'' + videoId + '\'); return false;" class="btn btn-danger btn-xs" type="button">Удалить временную метку</button>' +
                '</div>' +
                '</div>';


            $("#timingDivision_" + videoId).prepend(el);
            $("#timingDivision_" + videoId).addClass('in');
            $("#showTimingDivision_" + videoId).removeClass('hidden');

        }
    });
    return false;
}


function enabledTimingPartVideo(partVideoId){
    if ((''+partVideoId).indexOf('new_') === -1) {
        $.ajax({
            url: "ajax/enablePartVideo.php",
            data: {partVideoId: partVideoId},
            type: "POST",
            dataType: 'json',
            success: function (data) {
                if (data == true) {
                    $("#row_division_" + partVideoId).toggleClass('disabled');
                    $("#row_division_" + partVideoId).find('input').toggleClass('disabled');

                    $('#disableButton_' + partVideoId).html(
                        $('#disableButton_' + partVideoId).text() == 'Выключить из показа' ? 'Включить в показ' : 'Выключить из показа'
                    );
                }
            }
        });
    }
    return false;
}

function deleteTimingPartVideo (partVideoId, videoId){

    if ((''+partVideoId).indexOf('new_') === -1) {
        $.ajax({
            url: "ajax/deletePartVideo.php",
            data: {partVideoId: partVideoId, parentVideoId: videoId},
            type: "POST",
            dataType: 'json',
            success: function (data) {
                $("#row_division_" + partVideoId).remove();

                if (data.cnt == '0' || data.cnt == 0) {
                    $("#showTimingDivision_" + videoId).addClass('hidden');
                }

            }
        });
    } else {
        $("#row_division_" + partVideoId).remove();
    }
    return false;
}

function _getParentTypeForLinkPosts(){
    let type_block = $("[name=type_visual_block]").val();
    let parentType  = 'post';
    if (type_block === 'news') {
        parentType = 'news';
    } else if(type_block === 'post_tag') {
        parentType = 'post_tag';
    }
    return parentType;
}
    $(document).ready(function () {

        renderDataTable($('#item_id').val(), _getParentTypeForLinkPosts());

        Dropzone.autoDiscover = false;

        $("form.blogpost div.z").dropzone({
            url: "ajax/image_upload.php",
            addRemoveLinks: false,
            success: function (file, response) {
                file.previewElement.classList.add("dz-success");

                var product_sku = response;
                if (product_sku!=0){
                    $.ajax({
                        url: "ajax/product_image_show.php",
                        data: {product_sku:product_sku},
                        type: "POST",
                        dataType: 'json',
                        success: function(data)
                        {
                            $("#product_images").html(data);
                        }
                    });
                }
            },
            error: function (file, response) {
                file.previewElement.classList.add("dz-error");
            }
        });
    });


    // вывод статей для раздела "связанные статьи"
    $(document).on('click', '.linked_posts_writers', function(e)
    {
        var writer_id  = $(this).attr('writer_id') * 1;
        $.ajax({
            url: "ajax/get_posts_short_list.php",
            data: {writer_id:writer_id},
            type: "POST",
            dataType: 'json',
            success: function(data)
            {
                $("#prepare_posts_list").html(data);
            }
        });

        return false;
    });


    $(document).on('click', '.link_to_linked_post', function(e)
    {
        // ID поста который привязываем
        var postId  = $(e.currentTarget).attr('post_id') * 1;
        // Тип поста 100 - это новость
        var postType  = $(e.currentTarget).attr('post_type') * 1;
        // ID поста к КОТОРОМУ привязываем
        var parentId  = $("#item_id").val() * 1;
        // Type статьи от КОТОРОЙ отвязываем
        var parentType  = _getParentTypeForLinkPosts();

        $.ajax({
            url: "ajax/set_link_posts.php",
            data: {postId:postId, postType:postType, parentId: parentId, parentType: parentType},
            type: "POST",
            dataType: 'json',
            success: function(data)
            {
                $("#already_linked_posts").html(data.html);
            }
        });
        return false;
    });

    $(document).on('click', '.unlink_to_linked_post', function(e)
    {
        // ID статьи которую отвязываем
        var postId  = $(e.currentTarget).attr('post_id') * 1;
        // Тип поста 100 - это новость
        var postType  = $(e.currentTarget).attr('post_type') * 1;
        // ID статьи от КОТОРОЙ отвязываем
        var parentId  = $("#item_id").val() * 1;
        // Type статьи от КОТОРОЙ отвязываем
        var parentType  = _getParentTypeForLinkPosts();

        $.ajax({
            url: "ajax/set_unlink_posts.php",
            data: {postId:postId, parentId: parentId, postType: postType, parentType: parentType},
            type: "POST",
            dataType: 'json',
            success: function(data)
            {
                $("#already_linked_posts").html(data);
            }
        });
        return false;
    });

$(document).on('click', '[name="type_post"]', function(e)
{
    if ($(this).val() == 2){
        $('#name_label').html("Автор цитаты");
        $('#header_on_page_block').show();
    }
    else if ($(this).val() == 8){
        $('#name_label').html("Текст цитаты");
        $('#header_on_page_block').show();
    }
    else{
        $('#name_label').html("Заголовок");
        $('#header_on_page_block').hide();
    }
});

function addDynamicAnaliticElements(){

    var countRows = $("#countrows").val() * 1;

    var el = '<div class="row"><div class="col-lg-6"><div class="form-group"><label>Год</label><select name="years[]" class="form-control"><option>2017</option><option>2016</option><option>2015</option><option>2014</option><option>2013</option><option>2012</option><option>2011</option><option>2010</option><option>2009</option><option>2008</option><option>2007</option><option>2006</option><option>2005</option><option>2004</option><option>2003</option><option>2002</option><option>2001</option><option>2000</option></select></div></div><div class="col-lg-6"><div class="form-group"><label>Значение</label><input type="text" class="form-control" placeholder="Укажите значение" value="" name="value_to_yeats[]"></div></div></div>';


    if (countRows <= 6){
        $("#dynamic_analitic").append(el);
        $("#countrows").val(countRows + 1);
    }
    else{
        $("#btnAddRow").attr('disabled', 'disabled')
    }


    return false;
}

$(document).on('click', '.upload_button', function(e)
{
    var type = e.target.dataset.imagetype;
    $("#" + type + "_picture").prop("checked", true);
});


$(document).on('onblur', '.upload_button', function(e)
{
    console.log('sdsd');


});



$(document).ready(function () {
    // if ($('[name="type_post"]:checked').val() == 2) {
    //     $('#name_label').html("Автор цитаты");
    //     $('#header_on_page_block').show();
    // }
    // else if ($('[name="type_post"]').val() == 8){
    //     $('#name_label').html("Текст цитаты");
    //     $('#header_on_page_block').show();
    // }
    // else {
    //     $('#name_label').html("Заголовок");
    //     $('#header_on_page_block').hide();
    // }

    Dropzone.autoDiscover = false;

    var galleryId = $("#galleryId").val();
    $("form.dropzone div.z").dropzone({
        url: "ajax/image_upload.php?galleryId=" + galleryId,
        addRemoveLinks: false,
        parallelUploads: true,
        success: function (file, response) {
            file.previewElement.classList.add("dz-success");

            var $galleryId = response;

            if ($galleryId!=0){
                $.ajax({
                    url: "ajax/product_image_show.php",
                    data: {galleryId:$galleryId},
                    type: "POST",
                    dataType: 'json',
                    success: function(data)
                    {
                        $("#product_images").html(data);
                    }
                });
            }
        },
        error: function (file, response) {
            file.previewElement.classList.add("dz-error");
        }
    });
});

if (document.blogpost && document.blogpost.type_post && document.blogpost.type_post.value == '12') {
    let ArrayName = ['[name="name"]'];
    setTimeout(function () {
        tinymce.editors['mce_0'].contentDocument.addEventListener('keyup', function (e) {
           ArrayName.forEach(function (tag) {
                $(tag).val(tinymce.editors['mce_0'].getContent()).trigger('change');
            });
        });
    }, 1500);



    ArrayName.forEach(function (tag) {
        if ($(tag).closest('.form-group').find('.js_counters').length === 0) {
            $('<small class="js_counters text-muted pull-right"></small>').insertAfter($(tag));
        }
        $(document.blogpost).on('change keyup focus', tag, function (e) {
            let $input = $(e.currentTarget);
            let val = "" + $input.val();
            let div = document.createElement('div');
            div.innerHTML = val;
            val = div.innerText;
            $input.closest('.form-group').find('.js_counters').html(val.length + ' символов');
        });
    });
    $(ArrayName.join(',')).trigger('change');
}



var $groupItems = $( "#group_items" );

if (document.blogpost && $groupItems.length > 0) {
    let ArrayName = ['[name="name"]'];
    let counterAdded = false;
    ArrayName.forEach(function (tag) {
        if (!counterAdded && $(tag).closest('.form-group').find('.js_counters').length === 0) {
            $('<small class="js_counters text-muted pull-right"></small>').insertAfter($(tag));
            counterAdded = true;
        }
        $(document.blogpost).on('change keyup focus', tag, function (e) {
            let $input = $(e.currentTarget);
            let val = "" + $input.val();
            $input.closest('.form-group').find('.js_counters').removeClass('text-danger').html(val.length + ' символов');
            if (val.length > 50) {
                $input.closest('.form-group').find('.js_counters').addClass('text-danger');
            }
        });
    });
    $(ArrayName.join(',')).trigger('change');
    let POST_TYPES = {
        1 : 'Пост',
        5 : "Данные",
        11: 'Видео пост',
        6: 'Мем недели',
        7: 'Факт',
        8: 'Цитата',
        9: 'Анонс спецпроекта',
        10: 'DIY',
        12: 'Бегущая строка',
        13: 'Инструкция',
        14: 'Партнерский анонс'
    };
    let _linkedTemplate = '<div class="group-item">' +
        '<input type="hidden" class="js_id" name="group_items[%index%][id]" value="%itemId%">' +
        '<input type="hidden" class="js_content_id" name="group_items[%index%][content_id]" value="%contentId%">' +
        '<input type="hidden" class="js_content_type" name="group_items[%index%][content_type]" value="%contentType%">' +
        '<div class="actions"><a href="#" class="js_remove"><i class="fa fa-remove"></i></a></div>' +
        '<div class="preview-image">%image%</div>' +
        '<div class="content"><div class="type">%type%</div>' +
        '<div class="header">%header%</div><div class="description">%description%</div></div></div>';

    function _templateRender(item) {
        let image = '<span class="no_image"></span>';
        if (item.image) {
            image = '<div style="background-image: url('+ item.image +'); background-position: 50% 50%; background-size: cover; width: 100%;height: 100%"></div>';
        }

        let type = '<span class="label label-primary">' + (POST_TYPES[item.type_post] || 'Пост ['+item.type_post+']') + '</span>';
        if (item.content_type === 'news') {
            type = '<span class="label label-success">Новость</span>';
        }
        let index = $( "#group_items .group-item").length ;
        let indexRegExp = new RegExp(/%index%/, 'g');

        return  _linkedTemplate
            .replace('%header%', item.name)
            .replace('%description%', item.subtitle)
            .replace(indexRegExp, index)
            .replace('%itemId%', item.id)
            .replace('%contentType%', item.content_type)
            .replace('%contentId%', item.content_id)
            .replace('%image%', image)
            .replace('%type%', type);
    }

    function replaceIndexes() {

        let $items = $( "#group_items").find('.group-item');
        $items.each(function(i, e) {
            $(e).find('.js_id').attr('name', 'group_items['+i+'][id]');
            $(e).find('.js_content_id').attr('name', 'group_items['+i+'][content_id]');
            $(e).find('.js_content_type').attr('name', 'group_items['+i+'][content_type]');
        });
        $('.js_group_items_counter').html($items.length);
    }


    function _loadExistsGroupItems(groupId) {
        let field = $groupItems.data('field') ? $groupItems.data('field') : 'group_id';
        let path = $groupItems.data('items-path') ? $groupItems.data('items-path') : 'ajax/getGroupItems.php';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: path + '?' + field  + '=' + groupId,
            success: function (resp) {
                if (resp.ok) {
                    resp.items.forEach(function (el) {
                        $("#group_items").append( _templateRender(el));
                    });
                    $('.js_group_items_counter').html($('#group_items .group-item').length);
                }
            }
        });
    }

    function initAutocomplete() {
        let $inputSearch = $("input.js_search_posts");
        if ($inputSearch.length > 0) {
            $inputSearch.on('keydown keyup', function (e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    return false;
                }
            }).autocomplete({
                source: "api/getAutocomplete.php",
                appendTo: ".autocomplete-block",
                highlight: true,
                delay: 400,
                minLength: 2,
                select: function (event, ui) {
                    $('.js_search_posts').val('');
                    $('#group_items').append(_templateRender(ui.item));
                    $('.js_group_items_counter').html($('#group_items .group-item').length);
                    return false;
                },
                focus: function (event, ui) {
                    return false;
                }
            }).data('ui-autocomplete')._renderItem = function (ul, item) {
                let $li = $("<li/>");

                let image = '<span class="no_image"></span>';
                if (item.image) {
                    image = '<div style="background-image: url(' + item.image + '); background-position: 50% 50%; background-size: cover; width: 100%;height: 100%"></div>';
                }

                let type = '<span class="label label-primary">' + (POST_TYPES[item.type_post] || 'Пост [' + item.type_post + ']') + '</span>';
                if (item.content_type === 'news') {
                    type = '<span class="label label-success">Новость</span>';
                }
                let content = '<div>' +
                    '<div class="image_preview">' + image + '</div>' +
                    '<div class="type">' + type + '</div>' +
                    '<div class="title">' + item.name + '</div>' +
                    '<span class="description small text-small">' + item.subtitle + '</span>' +
                    '</div>';
                $li.html(content);
                return $li.appendTo(ul);
            };
        }
    }
    $( function() {
        initAutocomplete();

        $groupItems.on('click', '.js_remove', function (e) {
            e.preventDefault();
            $(e.currentTarget).closest('.group-item').remove();
            replaceIndexes();
        });

        if ($groupItems.data('group_id')) {
            _loadExistsGroupItems($groupItems.data('group_id'));
        } else {
            if (window.hasOwnProperty('_existsContents')) {
                window._existsContents.forEach(function (el) {
                    $("#group_items").append(_templateRender(el));
                });
                $('.js_group_items_counter').html($('#group_items .group-item').length);
            }
        }

        $groupItems.sortable({
            update: function( event, ui ) { replaceIndexes();}
        });

        $groupItems.disableSelection();
    });
}



$(function () {
    var socketInterval = false;
    if (document.forms.blogpost && document.forms.blogpost.item_id) {
        window._users = false;
        window._checkRepeat = 4;
        function getUserById(userId) {
            if (!window._users) {
                $.ajax({
                    url: '/adminv2/api/getUsers.php',
                    data: {userId : userId},
                    async: false,
                    success: function (resp) {
                        if (resp.ok) {
                            window._users = resp.users;
                        }
                    }
                });
            }
            return window._users[userId];
        }


        function createSocket() {
            let socket = new WebSocket('wss://' + document.location.host + '/wsCheckEditable');

            socket.onopen = function (e) {
                socket.send(JSON.stringify({postID: postID, author: userID, pageType: pageType, host: window.location.host}));
                window._checkRepeat = 4;
            };

            socket.onmessage = function (event) {
                let data = false;
                try {
                    data = JSON.parse(event.data);
                    if (data && data.hasOwnProperty('ok') && !data.ok) {
                        setTimeout(function () {tinyMCE.editors.forEach(function(e){e.setMode('readonly');})}, 3000);
                        $(document.forms.blogpost).find('input, select, [type=submit], textarea').prop('disabled', true);
                        let _user =  getUserById(data.user);
                        if (data.user === userID) {
                            $('body').append(_alert.replace('__user__', _warning));
                        } else {
                            $('body').append(_alert.replace('__user__', _user));
                        }
                    }
                } catch (e) {
                }

            };

            socket.onclose = function (event) {
                console.log('[close]');
                if (event.wasClean) {
                    console.log(`[close] Соединение закрыто чисто, код=${event.code} причина=${event.reason}`);
                } else {
                    if (socketInterval) {clearInterval(socketInterval);}
                    if (window._checkRepeat > 0) {
                        window._checkRepeat--;
                        setTimeout(function () {
                            window._socketCheckEditor = createSocket();
                        }, 2000);
                    } else {
                        delete window._socketCheckEditor;
                    }
                }
            };
            if (socketInterval) {clearInterval(socketInterval);}
            socketInterval = setInterval(function () {
                socket.send(JSON.stringify({message: 'ping'}));
            }, 30000);
            return socket;
        }

        let _alert = '<div class="alert-danger alert" style="position:fixed; right: 15px; top: 90px; z-index: 50;">' +
            '<small>РЕДАКТИРУЕТСЯ:</small><br/><i class="fa fa-pencil"></i> __user__</div>';
        let _warning = '<div class="alert-warning alert" style="position:fixed; right: 15px; top: 90px; z-index: 50;">' +
            '<small>РЕДАКТИРУЕТСЯ:</small><br/><i class="fa fa-info"></i> Вы уже редактируете этот документ на другой вкладке</div>';
        let userID = $('#user_id').data('value');
        let postID = $(document.forms.blogpost).find('#item_id').val();
        let pageType = $(document.forms.blogpost).find('[name=type_visual_block]').val() || '';

        window._socketCheckEditor = createSocket();

    }
});



$(function () {

    let $groupItems = $(".setting-widgets");
    $groupItems.on('click', '.js_edit', function (e) {
        e.preventDefault();
    });

    var __settingWidgetsUpdateTimeout = false;
    $groupItems.find('tbody').sortable({
        handle: '.handle-sort',
        update: function (event, ui) {
            if (__settingWidgetsUpdateTimeout) { clearTimeout(__settingWidgetsUpdateTimeout);}
            setTimeout(function () {
                var values  = $groupItems.find('tbody').sortable('toArray', {attribute: 'data-id'});
                $.ajax({
                    url: "ajax/setting_widgets.php",
                    data: {ids:values},
                    type: "POST",
                    dataType: 'json',
                    success: function(data)
                    {
                        console.log(data);
                    }
                });
            }, 100);
        }
    });
});

//
// $(function () {
//     $(document).on('click', '.js-button_delete_announcement_img', function (e) {
//         e.preventDefault();
//         let $btn = $(e.currentTarget);
//         $btn.button('loading');
//         $.ajax({
//             url: 'writers/fotos/remove/' + 'token',
//             data: {item_id: $('.js-item-writer-id').data('id')},
//             dataType: 'json',
//             success: function (resp) {
//                 if(resp.ok) {
//                     $btn.parent().parent().find('img').attr('src', 'images/no_foto.gif');
//                     $btn.remove();
//                 }
//             },
//             complete: function (resp) {
//                 $btn.button('reset')
//             }
//         })
//
//     })
// });
