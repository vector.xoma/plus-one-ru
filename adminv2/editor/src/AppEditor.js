import React, { useState, useEffect } from "react";
import { BrowserRouter } from "react-router-dom";

import Editor from "pages/Editor";

import "./reset.css";
import "./styles.scss";
import "fonts/fonts.scss";

function App({ globalVar, areaName }) {
  const [serverData, setServerData] = useState(
    window[globalVar] ? { text: window[globalVar] } : null
  );

  useEffect(() => {
    setServerData({ text: window[globalVar] });
  }, [window[globalVar]]);

  return (
    <BrowserRouter>
      <Editor
        serverData={serverData}
        updateData={setServerData}
        areaName={areaName}
      />
    </BrowserRouter>
  );
}

export default App;
