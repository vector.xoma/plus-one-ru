import React, { useState, useEffect } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import { Blocks } from "connectors/query/Blocks";
import Tags from "pages/Tags";
import RubricBlocksList from "pages/RubricBlocksList";
import SearchResultList from "pages/SearchResultList";
import TagBlocksList from "pages/TagBlocksList";
import NotFoundPage from "pages/404";
import Show from "pages/Show";
import { MenuProvider } from "contexts/menuContext";

import "./reset.css";
import "./styles.scss";
import "fonts/fonts.scss";

function AppEditor({ globalVar, areaName }) {
  const [blocksList, setBlocksList] = useState(
    window[globalVar] ? { text: window[globalVar] } : null
  );

  // TODO: перенести куда надо
  useEffect(() => {
    async function getBlocks() {
      const result = await Blocks.getRows();
      if (result && result.length) setBlocksList(result);
    }
    getBlocks();
  }, []);

  const rubricsTypes = ["ecology", "society", "economy", "platform"];

  return (
    <BrowserRouter>
      <MenuProvider>
        <Switch>
          <Route exact path="/search" component={SearchResultList} />
          {rubricsTypes.map(r => (
            <Route
              exact
              path={`/tag/${r}`}
              children={<Tags type="tag" rubric={r} />}
            />
          ))}
          {rubricsTypes.map(r => (
            <Route
              exact
              path={`/leaders/${r}`}
              children={<Tags type="leaders" rubric={r} />}
            />
          ))}
          {rubricsTypes.map(r => (
            <Route
              exact
              path={`/${r}`}
              children={<RubricBlocksList rubric={r} />}
            />
          ))}
          {rubricsTypes.map(r => (
            <Route
              exact
              path={`/tag/${r}/:tagName`}
              children={<TagBlocksList rubric={r} />}
            />
          ))}
          <Route
            exact
            path="/"
            children={<Show data={{ text: "", rowsOfBlocks: blocksList }} />}
          />
          <Route exact path="/404" component={NotFoundPage} />
          {/* <Show data={{ text: "", rowsOfBlocks: blocksList }} /> */}
        </Switch>
      </MenuProvider>
    </BrowserRouter>
  );
}

export default AppEditor;
