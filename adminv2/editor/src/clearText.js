export const convertHTMLEntity = text => {
  return text.replace(/&[#A-Za-z0-9]+;/gi, (entity, position, text) => {
    let span = document.createElement("span");
    span.innerHTML = entity;
    return span.innerText;
  });
};

export const clearText = text => {
  return text ? convertHTMLEntity(text.replace(/<\/?.+?>/g, "")) : "";
};
