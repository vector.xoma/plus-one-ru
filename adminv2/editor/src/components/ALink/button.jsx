import React, { useState, useEffect } from "react";
import { Popover, Button } from "antd";
import { Input, Icon } from "antd";
import { splitBySelection } from "utils";
import { Wrap } from "../Grid/Grid";

const LinkButton = ({ setALink, textData }) => {
  const { middle } = splitBySelection(textData.text, textData.area);

  const [visible, setVisible] = useState(false);
  const [url, setUrl] = useState("");
  const [urlText, setUrlText] = useState(middle || "");

  useEffect(() => {
    setUrlText(middle);
  }, [middle]);

  const Content = (
    <div className="editorTextInsert">
      <Wrap className="editorSign_name">
        <Input
          placeholder="Url"
          value={url || ""}
          onChange={e => setUrl(e.target.value)}
        />
      </Wrap>
      <Wrap className="editorSign_name">
        <Input
          placeholder="Текст ссылки"
          value={urlText || ""}
          onChange={e => setUrlText(e.target.value)}
        />
      </Wrap>
      <Wrap>
        <Button
          onClick={() => {
            setUrl("");
            setALink({ url, urlText });
            setVisible(false);
          }}
        >
          Добавить
        </Button>
      </Wrap>
    </div>
  );

  return (
    <Popover
      content={Content}
      title="Ссылка"
      trigger="click"
      placement="right"
      visible={visible}
      onVisibleChange={visible => {
        setVisible(visible);
      }}
    >
      <Button>
        <Icon type="link" />
      </Button>
    </Popover>
  );
};

export default LinkButton;
