import React from "react";
import styled from "styled-components";

const A = styled.a`
  display: inline;
  border-bottom: 2px solid #f8c946;
  -webkit-transition: all 0.4s;
  transition: all 0.4s;
  text-decoration: none;
  color: #000;
  :hover {
    color: #000;
    border-bottom: 2px solid #fff;
  }
`;

const ALink = ({ data }) => (
  // <div>
  <A href={data.url}>{data.text}</A>
  // </div>
);

export default ALink;
