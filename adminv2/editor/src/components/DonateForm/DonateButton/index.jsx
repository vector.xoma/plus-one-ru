import React from "react";
import styled from "styled-components";

import { SmallTextDescription } from "../../DonateForm";

const DonateButtonWrap = styled.div`
  width: 100%;
  max-width: 230px;
`;

const Btn = styled.button`
  width: 100%;
  height: 55px;
  background-color: #e7442b;
  border: 2px solid #e7442b;
  font-family: "MabryPro-Medium", sans-serif;
  color: #fff;
  cursor: pointer;
  border-radius: 5px;
`;

const DonateButton = ({ text, submit }) => {
  return (
    <DonateButtonWrap>
      <Btn type="submit" onClick={event => submit(event)}>
        Поддержать
      </Btn>
      <SmallTextDescription>{text}</SmallTextDescription>
    </DonateButtonWrap>
  );
};

export default DonateButton;
