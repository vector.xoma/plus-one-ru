import React from "react";
import styled from "styled-components";
import { ErrorMessage } from "../../DonateForm";
import { SmallTextDescription } from "../../DonateForm";

const DonateInputContainer = styled.div`
  width: 100%;
  max-width: 250px;
`;

const DonateInputWrap = styled.div`
  width: 100%;
  height: 100%;
  border: solid 2px #000000;
  font-size: 18px;
  background-color: #fff;
  outline: none;
  border-radius: 5px;
  font-family: "MabryPro-Regular", sans-serif;
  appearance: none;
  height: 55px;
  input {
    width: 100%;
    display: block;
    height: 100%;
    padding: 15px 25px 15px 14px;
    color: #000;
    box-sizing: border-box;
    ::placeholder {
      color: #000;
    }
  }
`;

const DonateInput = ({
  ph,
  text,
  onChange,
  value,
  touched,
  valid,
  errorText
}) => {
  return (
    <DonateInputContainer>
      <DonateInputWrap>
        <input type="text" placeholder={ph} onChange={onChange} value={value} />
        {!valid && touched ? <ErrorMessage>{errorText}</ErrorMessage> : null}
      </DonateInputWrap>
      <SmallTextDescription>{text}</SmallTextDescription>
    </DonateInputContainer>
  );
};

export default DonateInput;
