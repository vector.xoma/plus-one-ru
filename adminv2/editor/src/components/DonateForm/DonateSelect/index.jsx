import React from "react";
import "antd/dist/antd.css";
import { Select } from "antd";
import styled from "styled-components";
import { SmallTextDescription } from "../../DonateForm";
import { ErrorMessage } from "../../DonateForm";

const { Option } = Select;

const SelectContainer = styled.div`
  width: 100%;
  max-width: 260px;
`;

const SelectWrap = styled.div`
  width: 100%;
  height: 100%;
`;
const CustomSelect = styled(Select)`
  li {
    font-family: "MullerRegular";
    font-size: 20px;
    color: #000;
    line-height: 35px;
  }
  outline: none;
  .ant-select-selection {
    height: 100%;
    outline: none;
    border: none;
    :active {
      border: none;
    }
    :focus {
      border: none;
    }
    :hover {
      border: none;
    }
    :active {
      border: none;
    }
    .ant-select-selection__rendered {
      height: 100%;
      outline: none;
      .ant-select-selection-selected-value {
        height: 100%;
        display: flex !important;
        align-items: center;
        font-family: "MabryPro-Regular", sans-serif;
        font-size: 18px;
        color: #000;
        outline: none;
      }
    }
  }
`;

// TODO: Option застилизованы спомощью scss т.к. не получается стилизовать с помощью styled.Component(нельзя обратиться к option);

const DonateSelect = ({
  text,
  selectHandler,
  value,
  errorText,
  touched,
  valid
}) => {
  return (
    <SelectContainer>
      <SelectWrap>
        <CustomSelect
          // suffixIcon={arrowSelect}
          value={value}
          defaultValue="Способ оплаты"
          placeholder="Способ оплаты"
          style={{
            height: "55px",
            border: "solid 2px #000000",
            borderRadius: "5px"
          }}
          onChange={selectHandler}
        >
          <Option className="custom-option" value="Банковская карта">
            Банковская карта
          </Option>
          <Option className="custom-option" value="Сбербанк">
            Сбербанк
          </Option>
          <Option className="custom-option" value="Яндекс.Деньги">
            Яндекс.Деньги
          </Option>
          <Option className="custom-option" value="Через кассы и терминалы">
            Через кассы и терминалы
          </Option>
          <Option className="custom-option" value="Альфа-Клик">
            Альфа-Клик
          </Option>
          <Option className="custom-option" value="WebMoney">
            WebMoney
          </Option>
          <Option className="custom-option" value="MasterPass">
            MasterPass
          </Option>
        </CustomSelect>
        {value === "Способ оплаты" && touched ? (
          <ErrorMessage>{errorText}</ErrorMessage>
        ) : null}
        <SmallTextDescription>{text}</SmallTextDescription>
      </SelectWrap>
    </SelectContainer>
  );
};

export default DonateSelect;
