import React from "react";
import { Button } from "antd";

const DonateButton = ({ setDonateForm }) => {
  return <Button onClick={setDonateForm}>Форма пожертвований</Button>;
};

export default DonateButton;
