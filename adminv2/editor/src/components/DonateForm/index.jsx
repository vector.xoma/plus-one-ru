import React, { useState } from "react";
import styled from "styled-components";

import DonateInput from "./DonateInput";
import DonateSelect from "./DonateSelect";
import DonateButton from "./DonateButton";
import peopleSvg from "img/people-short.svg";

export const SmallTextDescription = styled.div`
  width: 100%;
  font-family: "MabryPro-Regular", sans-serif;
  font-size: 13px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.23;
  letter-spacing: normal;
  text-align: left;
  color: #868686;
  margin-top: 20px;
`;

export const ErrorMessage = styled.div`
  font-size: 10px;
  color: #e7442b;
  position: absolute;
`;

const FormImg = styled.div`
  position: absolute;
  background-image: url(${peopleSvg});
  background-repeat: no-repeat;
  background-size: auto;
  max-width: 319px;
  width: 100%;
  height: 125px;
  top: 0;
`;

const FormImgLeft = styled(FormImg)`
  left: 0;
  background-position: bottom right;
`;

const FormImgRight = styled(FormImg)`
  right: 0;
  background-position: bottom left;
`;

const FormHeader = styled.div`
  position: relative;
`;

const FormLogo = styled.div`
  max-width: 106px;
  margin: 0 auto;
`;

const FormLogoImg = styled.img.attrs({
  src: require("img/form-title.svg"),
  alt: "logo"
})`
  text-align: center;
  margin-bottom: 30px;
`;

const FormTitle = styled.h2`
  max-width: 500px;
  padding: 0 20px;
  margin: 0 auto;
  margin-bottom: 35px;
  text-align: center;
  font-family: "MabryPro-Bold", sans-serif;
  font-size: 24px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 30px;
  letter-spacing: normal;
  text-align: center;
  color: #000000;
`;

const DonateFormWrap = styled.form`
  background-color: #fff;
  padding: 12px 42px 24px;
  border-radius: 5px;
  position: relative;
  margin: 20px 0;
`;

const DonateControls = styled.div`
  display: flex;
  justify-content: space-between;
`;

const textSum =
  "Мы не берем комиссию за пожертвования и делаем все, чтобы комиссия оператора становилась ниже";

const textMail =
  "Введите e-mail, чтобы получить подтверждение о зачислении пожертвования и отчет о его расходовании";

const textPayment = "Выберите способ оплаты";

const textButton =
  "Нажимая на кнопку «Поддержать», я принимаю условия Положения о благотворительной программе «Поможем вместе».";

const DonateForm = () => {
  const [sum, setSum] = useState("");
  const [mail, setMail] = useState("");
  const [select, setSelect] = useState("Способ оплаты");

  const [sumValid, setSumValid] = useState(false);
  const [selectValid, setSelectValid] = useState(false);
  const [mailValid, setMailValid] = useState(true);

  const [sumTouched, setSumTouched] = useState(false);
  const [mailTouched, setMailTouched] = useState(false);
  const [selectTouched, setSelectTouched] = useState(false);

  const data = { sum, select, mail };
  const regMailValidator = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
  const dataValue = () => {
    console.log(data);
  };
  const enterOnlyNumbers = event => {
    const currentSum = event.target.value;
    if (currentSum[0] === "0") {
      return;
    }
    setSum(currentSum.replace(/[^\d,]/g, ""));
    if (currentSum.replace(/[^\d,]/g, "")) {
      setSumValid(true);
    }
  };

  const validEmail = event => {
    const currentMail = event.target.value.trim();

    setMail(currentMail);
    setMailTouched(true);

    if (currentMail.match(regMailValidator)) {
      setMailValid(true);
    }
  };

  const selectHandler = value => {
    setSelect(value);
    setSelectTouched(true);
    setSelectValid(true);
  };

  const submit = event => {
    event.preventDefault();
    let _mailValid = true;
    let _sumValid = true;
    let _selectValid = true;

    setSumTouched(true);
    setMailTouched(true);
    setSelectTouched(true);

    if (!mail.match(regMailValidator)) {
      setMailValid(false);
      _mailValid = false;
    }

    if (!(sum && sum > 0)) {
      setSumValid(false);
      _sumValid = false;
      setSum("");
    }

    if (select === "Способ оплаты") {
      _selectValid = false;
      setSelectValid(false);
    }

    if (_mailValid && _sumValid && _selectValid) {
      setMail("");
      setSum("");
      setSelect("Способ оплаты");
      setSumTouched(false);
      setMailTouched(false);
      setSelectTouched(false);
      dataValue();
    }
  };

  return (
    <DonateFormWrap>
      <FormImgLeft />
      <FormHeader>
        <FormLogo>
          <FormLogoImg />
        </FormLogo>
        <FormTitle>Заголовок для формы пожертвований</FormTitle>
      </FormHeader>
      <DonateControls>
        <DonateInput
          errorText="Введите сумму"
          ph="Сумма пожертвования"
          onChange={enterOnlyNumbers}
          text={textSum}
          value={sum}
          touched={sumTouched}
          valid={sumValid}
        />
        <DonateSelect
          errorText="Выберите способ оплаты"
          text={textPayment}
          selectHandler={selectHandler}
          // select={select}
          value={select}
          selectValid={selectValid}
          touched={selectTouched}
          valid={selectValid}
        />
        <DonateInput
          errorText="E-mail некорректен"
          ph="Email"
          text={textMail}
          onChange={validEmail}
          value={mail}
          touched={mailTouched}
          valid={mailValid}
        />
        <DonateButton text={textButton} submit={submit} />
      </DonateControls>
      <FormImgRight />
    </DonateFormWrap>
  );
};

export default DonateForm;
