import React from "react";
import styled from "styled-components";

const ButtonContainer = styled.div`
  position: absolute;
  left: 0;
  top: 50px;
  width: 40px;
  height: 40px;
  cursor: pointer;
  @media (max-width: 767px) {
    width: 20px;
    height: 20px;
    top: 25px;
    left: 25px;
  }
`;

const LineFirst = styled.div`
  position: absolute;
  left: 0;
  top: 50%;
  transform: rotate(45deg) translateY(-50%);
  width: 100%;
  height: 2px;
  background: #fff;
  transform-origin: top;
`;

const LineLast = styled.div`
  position: absolute;
  right: 0;
  top: 50%;
  transform: rotate(-45deg) translateY(-50%);
  width: 100%;
  height: 2px;
  background: #fff;
  transform-origin: top;
`;

const CloseButton = ({ setVisible, isVisible }) => (
  <ButtonContainer onClick={() => setVisible(!isVisible)}>
    <LineFirst />
    <LineLast />
  </ButtonContainer>
);

export default CloseButton;
