import React from "react";
import styled from "styled-components";

const NavListCol = styled.div`
  font-size: 36px;
  text-transform: initial;
  font-family: "MullerMedium";
  padding: 12px 0;
  @media (max-width: 767px) {
    font-size: 20px;
  }
`;

const LinkItem = styled.a`
  border-bottom-width: 0;
  display: inline-block;
  color: #fff;
  position: relative;
  :after {
    content: "";
    position: absolute;
    left: 0;
    width: 100%;
    height: 2px;
    bottom: 3px;
    background: ${({ borderColor }) => borderColor};
  }
  :hover {
    border-bottom-width: inherit;
    color: inherit;
  }
  @media (max-width: 767px) {
    font-size: 20px;
    line-height: 1.6;
    font-family: "MullerMedium", sans-serif;
    font-weight: 500;
    padding: 0;
  }
`;

const NavList = ({ contentType }) => {
  return (
    <NavListCol>
      {contentType.map((el, i) => (
        <div key={i}>
          <LinkItem borderColor={el.borderColor} href={el.src} key={i}>
            {el.title}
          </LinkItem>
        </div>
      ))}
    </NavListCol>
  );
};

export default NavList;
