import React, { useState, useEffect } from "react";
import styled, { keyframes } from "styled-components";
import { Transition } from "react-transition-group";

import CloseButton from "./CloseButton";
import NavList from "./NavList";
import Informer from "../Informer";
import useDesktop from "hooks/useDesktop";

const dropAnimation = keyframes`
    from {
      top: -100%;
    }
    to {
      top:0;
    }
  `;

const dropAnimationReverse = keyframes`
    from {
      top: 0;
    }
    to {
      top: -100%;
    }
  `;

const DropDownWrap = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  min-height: 200px;
  width: 100%;
  background: #000;
  z-index: 50;
  animation: ${({ isVisible }) =>
      isVisible ? dropAnimation : dropAnimationReverse}
    0.3s;
`;

const DropDownContainer = styled.div`
  max-width: 1140px;
  margin: 0 auto;
  color: #fff;
  position: relative;
`;

const DropdownNavWrap = styled.nav`
  width: 100%;
  display: flex;
  justify-content: space-around;
  padding: 140px 0 100px;
  @media (max-width: 767px) {
    flex-direction: column;
    padding-left: 20px;
    padding-top: 40px;
  }
`;

const partners = [
  { title: "О проекте", src: "/about/" },
  { title: "Спецпроекты", src: "/special-projects/" },
  { title: "+1Премия", src: "https://award.plus-one.ru/" },
  { title: "+1Conf", src: "https://conf.plus-one.ru/" },
  { title: "+1Город", src: "https://полезныйгород.рф/" },
  { title: "+1Платформа", src: "https://platform.plus-one.ru" }
];

const projects = [
  { title: "РБК+1", src: "http://plus-one.rbc.ru/", borderColor: " #02c418" },
  {
    title: "Forbes+1",
    src: "http://plus-one.forbes.ru",
    borderColor: "#00bcee"
  },
  {
    title: "Ведомости+1",
    src: "http://plus-one.vedomosti.ru/",
    borderColor: "#fff"
  },
  {
    title: "BFM+1",
    src: "https://www.bfm.ru/special/visioners",
    borderColor: "#b98b00"
  }
];

const socialNetworks = [
  { title: "Facebook", src: "https://www.facebook.com/ProjectPlus1Official/" },
  { title: "Вконтакте", src: "https://vk.com/plus1ru" },
  { title: "Twitter", src: "https://twitter.com/plus1_official" }
];

const styles = {
  container: {
    flexDirection: "column",
    margin: "20px 0 0 0 ",
    display: "block"
  },
  item: {
    justifyContent: "flex-start",
    borderBottom: "1px solid #333333"
  },
  block: {
    paddingLeft: "0"
  }
};
// const style = { flexDirection: "column", margin: 0 };

const DropDown = ({ visible, setVisible }) => {
  const isDesktop = useDesktop();
  return (
    <Transition
      in={visible}
      timeout={280}
      unmountOnExit
      setVisible={setVisible}
      isVisible={visible}
    >
      <DropDownWrap isVisible={visible}>
        <DropDownContainer>
          <CloseButton setVisible={setVisible} isVisible={visible} />
          <DropdownNavWrap>
            {!isDesktop && <Informer styles={styles} />}
            <NavList contentType={partners} />
            <NavList contentType={projects} />
            <NavList contentType={socialNetworks} />
          </DropdownNavWrap>
        </DropDownContainer>
      </DropDownWrap>
    </Transition>
  );
};

export default DropDown;
