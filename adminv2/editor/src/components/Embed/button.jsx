import React, { useState } from "react";
import { Input, Select, Popover, Button } from "antd";
import styled from "styled-components";
import { Wrap } from "../Grid/Grid";

const { Option } = Select;
const { TextArea } = Input;

const links = [
  { value: "Youtube", selected: false },
  { value: "Twitter", selected: false },
  { value: "Facebook", selected: false },
  { value: "Instagram", selected: false },
  { value: "ВКонтакте", selected: false },
  { value: "Vimeo", selected: false }
];

const EditorTextInsert = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const EmbedButton = ({ setEmbed }) => {
  const [type, setType] = useState(null);
  const [text, setText] = useState(null);
  const [visible, setVisible] = useState(false);

  const Content = (
    <EditorTextInsert className="editorTextInsert">
      <Wrap>
        <TextArea
          placeholder="Ссылка"
          rows={4}
          cols={40}
          value={text}
          onChange={e => setText(e.target.value)}
        />
      </Wrap>
      <Select
        // showSearch - добавляет возможность ввода в инпут
        style={{ width: "100%" }}
        placeholder="Выберите источник"
        onChange={value => setType(value)}
        value={type}
      >
        {links.map((el, index) => {
          return (
            <Option key={index} value={el.value}>
              {el.value}
            </Option>
          );
        })}
      </Select>
      <Wrap>
        <Button
          disabled={!type}
          onClick={() => {
            setEmbed({ embedInsert: text, type });
            setVisible(false);
            setText("");
            setType(null);
          }}
        >
          Добавить
        </Button>
      </Wrap>
    </EditorTextInsert>
  );

  return (
    <Popover
      content={Content}
      title="Текстовый врез"
      trigger="click"
      placement="right"
      visible={visible}
      onVisibleChange={visible => {
        setVisible(visible);
      }}
    >
      <Button>Embed</Button>
    </Popover>
  );
};

export default EmbedButton;
