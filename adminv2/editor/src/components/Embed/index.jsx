import React from "react";
import styled from "styled-components";

const EmbedWrap = styled.div`
  display: flex;
  justify-content: center;
  margin: 0 auto;
  color: #000;
`;

const Embed = ({ data }) => {
  // data.type - хранит инфу о типе эмбеда. устанавливается пользователем.
  return (
    <EmbedWrap>
      <div
        dangerouslySetInnerHTML={{
          __html: data.embedInsert
        }}
      />
    </EmbedWrap>
  );
};
export default Embed;
