import React, { useState, useEffect } from "react";
import ImageGallery from "react-image-gallery";

import { Galleries } from "connectors/query/Galleries";

import "react-image-gallery/styles/scss/image-gallery.scss";

const FotoRama = ({ data }) => {
  const [gallery, setGallery] = useState([]);

  useEffect(() => {
    async function getGallery() {
      const result = await Galleries.getGallery(data.id);
      setGallery(result);
    }
    getGallery();
  }, []);

  const images = gallery.map(g => ({
    original: g.filename,
    thumbnail: g.filename
  }));

  return (
    <div className="containerGallery">
      <ImageGallery items={images} />
    </div>
  );
};

export default FotoRama;
