import React, { useState } from "react";
import { Popover, Button, Input } from "antd";
import styled from "styled-components";

import FileManager from "containers/FileManager";
import { BtnFindImg, FilesControl } from "../_styles/buttonsStyles";
import { Wrap } from "../Grid/Grid";

const EditorTextInsert = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  div {
    margin: 5px 0;
  }
`;

const FullWidthPicButton = ({ setFullWidthPic }) => {
  const [visible, setVisible] = useState(false);
  const [showFM, setShowFM] = useState(false);
  const [url, setUrl] = useState("");
  const [title, setTitle] = useState("");
  const [source, setSource] = useState("");

  const Content = (
    <EditorTextInsert className="editorTextInsert">
      <FilesControl>
        <Input
          placeholder="Url картинки"
          value={url || ""}
          onChange={e => setUrl(e.target.value)}
        />
        <BtnFindImg onClick={() => setShowFM(true)}></BtnFindImg>
      </FilesControl>
      <Wrap>
        <Input
          placeholder="описание фото"
          value={title || ""}
          onChange={e => setTitle(e.target.value)}
        />
      </Wrap>
      <Wrap>
        <Input
          placeholder="Источник фото"
          value={source || ""}
          onChange={e => setSource(e.target.value)}
        />
      </Wrap>

      <Button
        onClick={() => {
          setFullWidthPic({ url, title, source });
          setVisible(false);
          setUrl("");
          setSource("");
          setTitle("");
        }}
      >
        Добавить
      </Button>
    </EditorTextInsert>
  );

  return (
    <>
      <Popover
        content={Content}
        title="Вставка картинки во всю ширину"
        trigger="click"
        placement="right"
        visible={visible}
        onVisibleChange={visible => {
          setVisible(visible);
        }}
      >
        <Button>Во всю ширину</Button>
      </Popover>

      <FileManager
        visible={showFM}
        setPicUrl={setUrl}
        hideModal={() => setShowFM(false)}
      />
    </>
  );
};

export default FullWidthPicButton;
