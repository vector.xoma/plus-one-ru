import React from "react";
import styled from "styled-components";

const FullWidthPicImgWrap = styled.div`
  width: 100%;
  height: auto;
  position: relative;
`;

const FullWidthPicImg = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

const FullWidthPicTextWrap = styled.div`
  width: 755px;
  margin: 0 auto;
`;

const FullWidthPicTextTitle = styled.div`
  display: block;
  color: #000;
  font-size: 12px;
  font-family: "MullerRegular", sans-serif;
`;

const FullWidthPicText = styled(FullWidthPicTextTitle)``;

const FullWidthDevider = styled.div`
  position: absolute;
`;

const FullWidthPic = ({ data }) => (
  <div>
    <FullWidthDevider />
    <FullWidthPicImgWrap>
      <FullWidthPicImg src={data.url} alt="неверный url" />
    </FullWidthPicImgWrap>
    <FullWidthPicTextWrap>
      <FullWidthPicTextTitle>{data.title}</FullWidthPicTextTitle>
      <FullWidthPicText>{data.source}</FullWidthPicText>
    </FullWidthPicTextWrap>
    <FullWidthDevider />
  </div>
);

export default FullWidthPic;
