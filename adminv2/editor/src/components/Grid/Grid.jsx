import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 20px auto;
  @media (max-width: 767px) {
    max-width: 300px;
    flex-direction: column;
  }
`;
export const Grid1_1 = styled.div`
  width: 100%;
  position: relative;
`;

export const Grid1_2 = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  position: relative;
`;

export const Grid1_3 = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
`;

export const GridItem1_1 = styled.div`
  position: relative;
  width: 100%;
  min-height: 480px;
  padding: 30px 25px;
  border-radius: 5px;
  transform: scale(1);
  transition: transform 1s;

  :hover {
    transform: scale(1.01);
  }
`;

export const GridItem1_2 = styled.div`
  position: relative;
  width: 100%;
  max-width: 560px;
  min-height: 365px;
  padding: 20px 30px 30px;
  border-radius: 5px;
  transform: scale(1);
  transition: transform 1s;
  :hover {
    transform: scale(1.01);
  }
`;
export const GridItem1_3 = styled.div`
  position: relative;
  width: 100%;
  max-width: 365px;
  min-height: 505px;
  padding: 20px 30px 30px;
  border-radius: 5px;
  transform: scale(1);
  transition: transform 1s;
  :hover {
    transform: scale(1.01);
  }
`;

export const GridItemGroup = styled.div`
  position: relative;
  /* width: 100%; */
  margin: 0 10px;
  max-width: 365px;
  height: 410px;
  padding: 20px 30px 30px;
  border-radius: 5px;
  position: relative;
`;

// Компонент для кликабельности всего поста

export const AWrap = styled.a`
  z-index: 6;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  display: block;
  cursor: pointer;
`;

//------------- Обёртка для модалок в админке
export const Wrap = styled.div`
  width: 100%;
  margin: 5px 0;
`;
