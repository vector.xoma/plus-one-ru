import React from "react";

import styled from "styled-components";

const BurgerWrap = styled.div`
  width: 37px;
  height: 25px;
  position: relative;
  cursor: pointer;
`;

const BurgerItem = styled.div`
  position: absolute;
  left: 0;
  width: 100%;
  height: 3px;
  background-color: #fff;
  ${({ customStyle }) => customStyle}
`;

const BurgerItemFirst = styled(BurgerItem)`
  top: 0;
`;
const BurgerItemSecond = styled(BurgerItem)`
  top: 50%;
  transform: translateY(-50%);
`;
const BurgerItemThird = styled(BurgerItem)`
  bottom: 0;
`;

const BurgerButton = ({ setVisible, isVisible, customStyle = {} }) => {
  return (
    <BurgerWrap onClick={() => setVisible(!isVisible)}>
      <BurgerItemFirst customStyle={customStyle} />
      <BurgerItemSecond customStyle={customStyle} />
      <BurgerItemThird customStyle={customStyle} />
    </BurgerWrap>
  );
};

export default BurgerButton;
