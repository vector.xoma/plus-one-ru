import React from "react";

import styled from "styled-components";
import logoSearch from "img/navSearchBlack.svg";
import logoSearchBlack from "img/navSearch.svg";

const SearchWrap = styled.div`
  width: 34px;
  height: 34px;
  z-index: 20;
  position: relative;
  z-index: 21;
  transition: right 0.3s;
  right: ${({ showed }) => (!showed ? "20px" : "0")};
  transition: right 0.3s;
  ${({ customStyle }) => customStyle}
  @media (max-width: 767px) {
    right: 0;
  }
`;

const SearchImg = styled.img`
  width: 100%;
  height: 100%;
  cursor: pointer;
  object-fit: contain;
`;

const Search = ({
  showed,
  setToggle,
  isHeaderFixed,
  customStyle,
  setLinksList,
  setSearchStr
}) => {
  return (
    <div
      onClick={() => {
        setToggle(!showed);
        setLinksList([]);
        setSearchStr("");
      }}
    >
      <SearchWrap showed={showed} customStyle={customStyle}>
        <SearchImg
          className="SearchImg"
          src={isHeaderFixed && showed ? logoSearch : logoSearchBlack}
        />
      </SearchWrap>
    </div>
  );
};

export default Search;
