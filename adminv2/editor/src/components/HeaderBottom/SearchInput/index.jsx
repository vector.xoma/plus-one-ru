import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import useDebounce from "hooks/useDebounce";
import { Transition } from "react-transition-group";
import styled, { keyframes } from "styled-components";
import { Search } from "connectors/query/Search";

// import "./styles.scss";

const inputAnimate = keyframes`
    from {
    width:20px;
   
  }
  to {
    width: 100%;
    
    
  }
`;

const inputAnimateReverse = keyframes`
   from {
    width: 100%;
  }
  to {
    width: 0;
  }
`;

const SearchInputWrapAnimate = styled.div`
  position: absolute;
  right: 0;
  top: 50%;
  transform: translateY(-50%);
  width: 100%;
  height: 64px;
  animation-name: ${inputAnimate};
  animation-duration: 0.3s;
  z-index: 20;
  box-sizing: border-box;
`;
const SearchInputWrapAnimateReverse = styled.div`
  position: absolute;
  right: 0;
  top: 50%;
  transform: translateY(-50%);
  width: 100%;
  height: 64px;
  animation-name: ${inputAnimateReverse};
  animation-duration: 0.3s;
`;

const SearchInp = styled.input`
  color: #fff;
  height: 100%;
  width: 100%;
  background-color: #121212;
  border-radius: 5px;
  font-size: 32px;
  font-family: "Krok";
  padding-left: 18px;
`;

const AutoCompleteWrap = styled.div`
  width: 100%;
  max-height: 380px;
  background-color: #121212;
  border: ${({ border }) => (border ? "1px solid #c5c5c5" : "none")};
  overflow-y: scroll;
  border-right: none;
  box-sizing: border-box;
`;

const AutoCompleteItem = styled.div`
  padding: 5px;
  margin: 5px 10px;
  border-radius: 0 0 5px 5px;
  cursor: pointer;
  :hover {
    background: #242424;
  }
`;
const AutoCompleteLink = styled.a`
  line-height: 21px;
  text-align: left;
  color: #fff;
  font-size: 18px;
  font-family: "MullerRegular", sans-serif;
  :hover {
    color: #fff;
  }
`;

const SearchInput = ({
  showed,
  registerRef,
  setLinksList,
  linksList = [],
  searchStr,
  setSearchStr
}) => {
  const debouncedValue = useDebounce(searchStr, 500);
  const history = useHistory();

  useEffect(() => {
    if (debouncedValue.length > 1)
      Search.getList({
        term: debouncedValue
      }).then(setLinksList);
    else {
      setLinksList([]);
    }
  }, [debouncedValue]);

  const handleEnter = e => {
    if (e.key === "Enter") history.push(`/search?${searchStr}`);
  };

  const ComponentType = showed
    ? SearchInputWrapAnimate
    : SearchInputWrapAnimateReverse;

  return (
    <Transition in={showed} timeout={280} unmountOnExit>
      <ComponentType ref={registerRef}>
        <SearchInp
          type="text"
          className="search__input"
          autoFocus={true}
          onChange={e => setSearchStr(e.target.value)}
          onKeyDown={handleEnter}
          value={searchStr}
        />
        {/* // TODO: сделать нормальное отображение списка */}
        <AutoCompleteWrap border={linksList.length > 0}>
          {linksList.length > 0 &&
            linksList.map((l, i) => (
              <AutoCompleteItem key={i}>
                <AutoCompleteLink href={l.url}>{l.label}</AutoCompleteLink>
              </AutoCompleteItem>
            ))}
        </AutoCompleteWrap>
      </ComponentType>
    </Transition>
  );
};

export default SearchInput;
