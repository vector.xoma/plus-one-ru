import React, { useState, useRef, useEffect } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import useOnclickOutside from "react-cool-onclickoutside";

import BurgerButton from "./BurgerButton";
import Search from "./Search";
import SearchInput from "./SearchInput/index";

import logoPlusOne from "img/plus_one_logo.svg";
import logoPlusOneBlack from "img/plus_one_logoBlack.svg";

const HeaderBottomStyle = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  max-width: 1140px;
  margin: 0 auto;
  padding: 20px 0;
  box-sizing: border-box;
  position: relative;
  ${({ customStyle }) => customStyle}
  @media (max-width: 767px) {
    padding: 20px;
  }
`;

const LogoWrap = styled(Link)`
  width: 120px;
  height: 63px;
  border: 0;

  ${({ customStyle }) => customStyle};
  :hover {
    border: 0;
  }
`;

const Logo = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
  display: block;
`;

const HeaderBottom = ({
  setVisible,
  isVisible,
  styles = {},
  isHeaderFixed,
  setHeaderRef
}) => {
  const [toggle, setToggle] = useState(true);
  const [linksList, setLinksList] = useState([]);
  const [searchStr, setSearchStr] = useState("");

  const refHeader = useRef(null);

  useEffect(() => {
    setHeaderRef && setHeaderRef(refHeader.current);
  }, [refHeader]);

  const registerRef = useOnclickOutside(e => {
    if (!e.target.classList.contains("SearchImg")) {
      setToggle(true);
      setLinksList([]);
      setSearchStr("");
    }
  });

  return (
    <HeaderBottomStyle customStyle={styles.headerWrap} ref={refHeader}>
      <BurgerButton
        setVisible={setVisible}
        isVisible={isVisible}
        customStyle={styles.line}
      />
      <LogoWrap to="/" customStyle={styles.logo}>
        <Logo src={!isHeaderFixed ? logoPlusOne : logoPlusOneBlack} />
      </LogoWrap>
      <Search
        showed={toggle}
        setToggle={setToggle}
        isHeaderFixed={isHeaderFixed}
        customStyle={styles.logoSearch}
        setLinksList={setLinksList}
        setSearchStr={setSearchStr}
      />
      <SearchInput
        showed={!toggle}
        setToggle={setToggle}
        registerRef={registerRef}
        setLinksList={setLinksList}
        linksList={linksList}
        setSearchStr={setSearchStr}
        searchStr={searchStr}
      />
    </HeaderBottomStyle>
  );
};

export default HeaderBottom;
