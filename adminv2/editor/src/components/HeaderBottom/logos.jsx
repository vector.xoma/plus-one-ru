import React from "react";

export const logoSearch = (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30.25 29.24">
    <defs>
      <style>.cls-1{{ fill: "#000" }}</style>
    </defs>
    <title>Asset 9</title>
    <g id="Layer_2" data-name="Layer 2">
      <g id="background">
        <path
          class="cls-1"
          d="M28.12,29.24,21,22.06a12.48,12.48,0,0,1-8.41,3.24A12.64,12.64,0,1,1,23,19.8l7.29,7.33ZM12.58,3a9.65,9.65,0,1,0,9.59,9.65A9.63,9.63,0,0,0,12.58,3Z"
        />
      </g>
    </g>
  </svg>
);

