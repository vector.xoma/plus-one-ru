import React from "react";
import NavComponent from "./NavComponent/NavComponent";
import NavRightComponent from "./NavRightComponent";
import styled from "styled-components";

const HeaderTop = styled.header`
  background-color: #0f0f0f;
`;
const HeaderWrap = styled.div`
  display: flex;
  max-width: 1140px;
  margin: 0 auto;
  justify-content: space-between;
`;
const HeaderComponent = () => {
  return (
    <HeaderTop>
      <HeaderWrap>
        <NavComponent />
        <NavRightComponent />
      </HeaderWrap>
    </HeaderTop>
  );
};
export default HeaderComponent;
