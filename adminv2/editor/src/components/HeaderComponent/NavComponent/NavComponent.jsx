import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

const NavContainer = styled.ul`
  display: flex;
  max-width: 1140px;
  margin: 0 auto;
  @media (max-width: 767px) {
    justify-content: space-between;
    padding: 0 20px;
  }
`;
const Nav = styled.nav`
  @media (max-width: 767px) {
    width: 100%;
  }
`;
const NavItem = styled.li`
  padding: 12px 0;
  margin-right: 20px;
  cursor: pointer;
  text-transform: uppercase;
  font-family: "MullerMedium", sans-serif;
  font-size: 12px;
  :last-child {
    margin-right: 0;
  }
  @media (max-width: 767px) {
    margin-right: 0;
    :first-child {
      display: none;
    }
    :nth-child(2) {
      display: none;
    }
    :nth-child(4) {
      display: none;
    }
  }
`;

const LinkA = styled.a`
  color: #ffffff;
  border: none;
  :hover {
    border: none;
    color: #ffffff;
  }
`;

const LinkRoute = styled(Link)`
  color: #ffffff;
  border: none;
  :hover {
    border: none;
    color: #ffffff;
  }
`;
const nav = [
  { title: "О проекте", src: "about" },
  { title: "Спецпроекты", src: "specproject" },
  { title: "+1Премия", src: "https://award.plus-one.ru/" },
  { title: "+1CONF", src: "https://conf.plus-one.ru/" },
  { title: "+1Город", src: "https://xn--c1acbikjqegacu3l.xn--p1ai/" },
  { title: "+1Платформа", src: "https://platform.plus-one.ru/organizations" },
  { title: "+1Люди", src: "https://ivsezaodnogo.ru/" }
];

const NavComponent = () => {
  return (
    <Nav>
      <NavContainer>
        {nav.map((elem, i) => {
          if (elem.src.includes("http")) {
            return (
              <NavItem key={i}>
                <LinkA
                  href={elem.src}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {elem.title}
                </LinkA>
              </NavItem>
            );
          }
          return (
            <NavItem key={i} className="nav-item">
              <LinkRoute to={elem.src} key={i} rel="noopener noreferrer">
                {elem.title}
              </LinkRoute>
            </NavItem>
          );
        })}
      </NavContainer>
    </Nav>
  );
};
export default NavComponent;
