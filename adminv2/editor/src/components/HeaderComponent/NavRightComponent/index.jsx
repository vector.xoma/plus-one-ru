import React from "react";
import styled from "styled-components";

const nav = [
  { title: "РБК+1", src: "http://plus-one.rbc.ru/" },
  { title: "Forbes+1", src: "http://plus-one.forbes.ru" },
  { title: "Ведомости+1", src: "http://plus-one.vedomosti.ru/" },
  { title: "BFM+1", src: "https://www.bfm.ru/special/visioners" },
  { title: "fb", src: "https://www.facebook.com/ProjectPlus1Official/" },
  { title: "vk", src: "https://vk.com/plus1ru" }
];

const Nav = styled.nav`
  @media (max-width: 767px) {
    display: none;
  }
`;
const NavContainer = styled.ul`
  display: flex;
  max-width: 1140px;
  margin: 0 auto;
`;

const NavItem = styled.li`
  position: relative;
  padding: 12px 0;
  margin-right: 20px;
  cursor: pointer;
  text-transform: uppercase;
  font-family: "MullerMedium", sans-serif;
  font-size: 12px;
  :last-child {
    margin-right: 0;
  }
  ::after {
    position: absolute;
    content: "";
    left: 0;
    bottom: 7px;
    width: 100%;
    height: 2px;
  }
  :nth-child(1) {
    ::after {
      background-color: #02c418;
    }
  }
  :nth-child(2) {
    ::after {
      background-color: #00bcee;
    }
  }
  :nth-child(3) {
    ::after {
      background-color: #fff;
    }
  }
  :nth-child(4) {
    ::after {
      background-color: #b98b00;
    }
  }
  :nth-child(5) {
    margin-left: 20px;
    ::after {
      background-color: #575757;
      height: 20px;
      top: 50%;
      transform: translateY(-50%);
      width: 1px;
      left: -20px;
    }
  }
`;

const A = styled.a`
  line-height: normal;
  list-style: none;
  color: #fff;
  :hover {
    color: #fff;
  }
`;

const NavRightComponent = () => {
  return (
    <Nav>
      <NavContainer>
        {nav.map((el, i) => {
          return (
            <NavItem key={i}>
              <A href={el.src} target="_blank">
                {el.title}
              </A>
            </NavItem>
          );
        })}
      </NavContainer>
    </Nav>
  );
};

export default NavRightComponent;
