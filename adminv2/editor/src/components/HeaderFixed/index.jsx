import React, { useState } from "react";
import styled, { keyframes } from "styled-components";
import { Transition, CSSTransition } from "react-transition-group";
import HeaderBottom from "../HeaderBottom";

const headerDownAnimation = keyframes`
    from {
      top: -100%;
    }
    to {
      top:0;
    }
  `;

const headerUpAnimation = keyframes`
    from {
      top: 0;
    }
    to {
      top: -100%;
    }
  `;

const HeaderFixedWrap = styled.div`
  position: fixed;
  width: 100%;
  margin: 0 auto;
  z-index: 100;
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
  background-color: #eaeaea;
  /* transition: top ease 0.8s; */
  animation: ${({ visible }) =>
      visible ? headerDownAnimation : headerUpAnimation}
    ease 1s;
`;

const styles = {
  headerWrap: {
    padding: "10px 0"
  },
  burgerButton: {
    height: "25px",
    width: "37px",
    position: "relative",
    transition: " all 0.8s"
  },
  logoSearch: {
    width: "27px",
    height: "26px"
  },
  logo: {
    width: "76px",
    height: "40px"
  },
  line: {
    background: "#000"
  }
};

const HeaderFixed = ({ setVisible, visible }) => {
  const [isHeaderFixed, setIsHeaderFixed] = useState(true);
  return (
    <Transition
      in={visible}
      // mountOnEnter
      unmountOnExit
      timeout={280}
      setVisible={setVisible}
      isVisible={visible}
    >
      <HeaderFixedWrap className="headerFixedWrap" visible={visible}>
        <HeaderBottom
          styles={styles}
          isHeaderFixed={isHeaderFixed}
          setVisible={setVisible}
        />
      </HeaderFixedWrap>
    </Transition>
  );
};

export default HeaderFixed;
