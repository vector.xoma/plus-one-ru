import React, { useState } from "react";
import { Menu, Popover, Button } from "antd";

import "antd/dist/antd.css";
// import "./HeaderType.scss";

const headers = [
  { title: "h1", size: 1, margin: "0.67em", fz: "2em" },
  { title: "h2", size: 2, margin: "0.83em", fz: "1.5em" },
  { title: "h3", size: 3, margin: "1em", fz: "1.17em" },
  { title: "h4", size: 4, margin: "1.33em", fz: "1em" },
  { title: "h5", size: 5, margin: "1.67em", fz: "0.83em" },
  { title: "h6", size: 6, margin: "2.33em", fz: "0.67em" }
];

const HeaderType = ({ setHeaders }) => {
  const [visible, setVisible] = useState(false);

  const Content = (
    <Menu>
      {headers.map((elem, i) => (
        <Menu.Item
          onClick={() => {
            setHeaders({ size: elem.size, margin: elem.margin, fz: elem.fz });
            setVisible(false);
          }}
          key={i}
          style={{ width: 256 }}
        >
          {`Заголовок ${i + 1} уровня`}
        </Menu.Item>
      ))}
    </Menu>
  );

  return (
    <Popover
      content={Content}
      title="Заголовок"
      trigger="hover"
      placement="right"
      visible={visible}
      onVisibleChange={visible => {
        setVisible(visible);
      }}
    >
      <Button>Заголовок</Button>
    </Popover>
  );
};

export default HeaderType;
