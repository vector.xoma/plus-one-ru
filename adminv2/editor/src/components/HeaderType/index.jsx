import React from "react";
import styled from "styled-components";

const HeaderTitleWrap = styled.div`
  display: block;
  font-weight: bold;
  font-family: "MullerRegular", sans-serif;
  color: #000000;
`;
const HeaderTitleWrap1 = styled(HeaderTitleWrap)`
  font-size: 2em;
  margin-top: 0.67em;
  margin-bottom: 0.67em;
`;
const HeaderTitleWrap2 = styled(HeaderTitleWrap)`
  font-size: 1.5em;
  margin-top: 0.83em;
  margin-bottom: 0.83em;
`;
const HeaderTitleWrap3 = styled(HeaderTitleWrap)`
  font-size: 1.17em;
  margin-top: 1em;
  margin-bottom: 1em;
`;
const HeaderTitleWrap4 = styled(HeaderTitleWrap)`
  font-size: 1em;
  margin-top: 1.33em;
  margin-bottom: 1.33em;
`;
const HeaderTitleWrap5 = styled(HeaderTitleWrap)`
  font-size: 0.83em;
  margin-top: 1.67em;
  margin-bottom: 1.67em;
`;
const HeaderTitleWrap6 = styled(HeaderTitleWrap)`
  font-size: 0.67em;
  margin-top: 2.33em;
  margin-bottom: 2.33em;
`;
const TextInsert = ({ data }) => {
  let HeaderTitleType = null;

  switch (data.size) {
    case "1":
      HeaderTitleType = HeaderTitleWrap1;
      break;
    case "2":
      HeaderTitleType = HeaderTitleWrap2;
      break;
    case "3":
      HeaderTitleType = HeaderTitleWrap3;
      break;
    case "4":
      HeaderTitleType = HeaderTitleWrap4;
      break;
    case "5":
      HeaderTitleType = HeaderTitleWrap5;
      break;
    case "6":
      HeaderTitleType = HeaderTitleWrap6;
      break;
    default:
      HeaderTitleType = HeaderTitleWrap1;
      break;
  }

  // const className = `header-title header-title__${data.size}`;
  return <HeaderTitleType>{data.text}</HeaderTitleType>;
};
export default TextInsert;
