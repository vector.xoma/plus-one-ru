import React from "react";
import { useMenuData } from "contexts/menuContext";
import _get from "lodash/get";
import styled from "styled-components";
import { Link } from "react-router-dom";

import logos from "./logos.jsx";

const InformerContainer = styled.div`
  max-width: 1140px;
  margin: 0 auto;
  display: flex;
  justify-content: space-between;
  cursor: pointer;
  @media (max-width: 767px) {
    ${({ customStyle }) => customStyle};
  }
`;
const InformerItem = styled.div`
  display: flex;
  justify-content: space-between;
  ${({ customStyle }) => customStyle}
`;

const InformerBlock = styled(Link)`
  display: flex;
  padding: 20px 15px;
  border-radius: 8px;
  transition: background-color ease 0.2s;
  :first-child {
    ${({ customStyle }) => customStyle};
  }
  :hover {
    background-color: rgba(255, 255, 255, 0.2);
  }
`;

const InformerLogoWrap = styled.div`
  width: 42px;
  height: 42px;
  margin-right: 12px;
  circle {
    fill: #fff;
  }
  path:first-child,
  path:first-child {
    fill: #fff;
  }
`;

const InformerText = styled.div`
  color: #fff;
`;
const InformerTitle = styled.div`
  font-size: 9px;
  font-family: "MullerMedium";
  text-transform: uppercase;
  color: #fff;
`;

const InformerNum = styled.div`
  font-size: 21px;
  font-family: "MullerMedium";
  color: #fff;
  line-height: 1;
`;

const InformerDescription = styled.div`
  font-size: 9px;
  font-family: "MullerMedium";
  text-transform: uppercase;
  color: #fff;
  line-height: 1.2;
`;

const InformerBlockOther = styled(Link)`
  padding: 20px 15px;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  cursor: pointer;
  border-radius: 8px;
  transition: background-color ease 0.2s;
  :hover {
    background-color: rgba(255, 255, 255, 0.2);
  }
`;

const translate = {
  society: "Общество",
  economy: "Экономика",
  ecology: "Экология",
  platform: "Платформа"
};

const Informer = ({ styles = {} }) => {
  const { data: menuData } = useMenuData();
  const data = Object.values(_get(menuData, "menu", {}));

  return (
    <InformerContainer customStyle={styles.container}>
      {data.map((item, index) => (
        <InformerItem key={index} customStyle={styles.item}>
          <InformerBlock to={`/${item.url}`} customStyle={styles.block}>
            <InformerLogoWrap>{logos[item.url]}</InformerLogoWrap>
            <InformerText>
              <InformerTitle>{translate[item.url]}</InformerTitle>
              <InformerNum>{_get(item, "posts.cnt")}</InformerNum>
              <InformerDescription>
                {_get(item, "posts.text")}
              </InformerDescription>
            </InformerText>
          </InformerBlock>
          {_get(item, "postTags.cnt", 0) > 0 && (
            <InformerBlockOther to={`/tag/${item.url}`}>
              <InformerNum>{item.postTags.cnt}</InformerNum>
              <InformerDescription>{item.postTags.text}</InformerDescription>
            </InformerBlockOther>
          )}
          {_get(item, "leaders.cnt", 0) > 0 && (
            <InformerBlockOther to={`/leaders/${item.url}`}>
              <InformerNum>{item.leaders.cnt}</InformerNum>
              <InformerDescription>{item.leaders.text}</InformerDescription>
            </InformerBlockOther>
          )}
        </InformerItem>
      ))}
    </InformerContainer>
  );
};

export default Informer;
