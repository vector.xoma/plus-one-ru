import React, { useState } from "react";
import { Popover, Button } from "antd";
import { Input } from "antd";
import { Wrap } from "../Grid/Grid";

const InterviewButton = ({ setInterview }) => {
  const [visible, setVisible] = useState(false);
  const [question, setQuestion] = useState("");
  const [answer, setAnswer] = useState("");

  const Content = (
    <div className="editorTextInsert">
      <Wrap className="editorSign_name">
        <Input
          placeholder="Вопрос"
          value={question || ""}
          onChange={e => setQuestion(e.target.value)}
        />
      </Wrap>
      <Wrap className="editorSign_name">
        <Input
          placeholder="Подпись"
          value={answer || ""}
          onChange={e => setAnswer(e.target.value)}
        />
      </Wrap>

      <Wrap>
        <Button
          onClick={() => {
            setInterview({ question, answer });
            setVisible(false);
            setQuestion("");
            setAnswer("");
          }}
        >
          Добавить
        </Button>
      </Wrap>
    </div>
  );

  return (
    <Popover
      content={Content}
      title="Вставка интервью"
      trigger="click"
      placement="right"
      visible={visible}
      onVisibleChange={visible => {
        setVisible(visible);
      }}
    >
      <Button>Интервью</Button>
    </Popover>
  );
};

export default InterviewButton;
