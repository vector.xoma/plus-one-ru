import React from "react";
import styled from "styled-components";

const InterviewWrap = styled.div`
  max-width: 755px;
  margin: 0 auto;
  color: #000000;
  font-family: "MullerRegular", sans-serif;
  line-height: 35px;
  position: relative;
`;

const InterviewTitle = styled.div`
  font-size: 26px;
  font-weight: bold;
`;

const InterviewAuthor = styled.div`
  font-size: 20px;
  margin-bottom: 28px;
`;

const InterviewDevider = styled.div`
  position: absolute;
`;

const Interview = ({ data }) => (
  <InterviewWrap>
    <InterviewDevider />
    <InterviewTitle>{data.question}</InterviewTitle>
    <InterviewAuthor>{data.answer}</InterviewAuthor>
    <InterviewDevider />
  </InterviewWrap>
);

export default Interview;
