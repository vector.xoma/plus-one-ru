import React, { useRef } from "react";
import _get from "lodash/get";
import useScrollPosition from "utils/hooks/useScroll";
import styled from "styled-components";
import { clearText } from "clearText";

const ParallaxTxtW = styled.div`
  position: relative;
  height: 148px;
  margin-bottom: 22px;
  margin-top: 30px;
  // overflow: hidden;
`;
const ParallaxTxtBorder = styled.div`
  position: absolute;
  left: 0;
  right: 0;
  bottom: 26px;
  border: 3px solid #fff;
  -webkit-transition: all 0.4s;
  transition: all 0.4s;
`;
const ParallaxTxt = styled.div`
  font-size: 136px;
  line-height: 100px;
  color: #fff;
  font-family: "GeometricSansSerifv1";
  position: absolute;
  width: 100%;
  white-space: nowrap;
  position: absolute;
  text-transform: uppercase;
  transition: all 0.8s;
  right: ${({ rightMove }) => rightMove}px;
`;
const ParallaxTxtFlex = styled.div`
  display: flex;
`;

const LongRow = ({ data }) => {
  let rightMove = 0;
  const rowRef = useRef(null);
  const textRef = useRef(null);

  // позиционирование строки по скроллу
  const scroll = useScrollPosition(rowRef);
  // ширина, насколько строка превышает размер окна
  const overflowX = textRef.current
    ? textRef.current.offsetWidth - window.innerWidth + 30
    : 0;
  // высота блока
  const blockHeight = textRef.current ? textRef.current.offsetHeight + 50 : 0;
  // "рабочая" высота, где происходит анимация
  const workHeight = window.innerHeight - blockHeight;
  if (scroll && scroll.top >= 0 && scroll.top < workHeight) {
    const heightPercent = scroll.top / (workHeight / 100);
    rightMove = (100 - heightPercent) * (overflowX / 100);
  }
  if (scroll && scroll.top < 30) rightMove = overflowX;
  if (scroll && scroll.top > workHeight) rightMove = 0;

  return (
    <ParallaxTxtW ref={rowRef}>
      <ParallaxTxtBorder />
      <ParallaxTxt rightMove={rightMove}>
        <ParallaxTxtFlex>
          <p
            ref={textRef}
            style={{ color: _get(data, "postFormat.fontColor", "#000") }}
          >
            {clearText(data.name)}
          </p>
        </ParallaxTxtFlex>
      </ParallaxTxt>
    </ParallaxTxtW>
  );
};

export default LongRow;
