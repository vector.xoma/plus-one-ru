import React, { useEffect, useState } from "react";
import styled from "styled-components";
import Slider from "react-slick";
import Block from "containers/BlocksViewer/components/Block";
import newsMock from "mock/newsMock";
import useDesktop from "hooks/useDesktop";
import { News } from "connectors/query/newsSlider";

import leftArrowImg from "img/arrowLeft.svg";
import rightArrowImg from "img/arrowRight.svg";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const SliderContainer = styled.div`
  margin-bottom: 20px;
  padding: 30px 33px;
  border-radius: 5px;
  position: relative;
  background-color: #eaeaea;
  box-sizing: border-box;

  .slick-arrow {
    opacity: 0;
    transition: opacity ease 0.3s;
  }
  :hover .slick-arrow {
    opacity: 1;
  }
  h2 {
    position: absolute;
    top: 30px;
    left: 33px;
    font-family: "MullerRegular", sans-serif;
    font-weight: 500;
    font-size: 13px;
    line-height: 1.2;
    text-transform: uppercase;
    text-decoration: none;
    color: #000;
    z-index: 10;
  }
  .slick-dots {
    li {
      margin: 0;
      button {
        ::before {
          font-size: 8px;
        }
      }
    }
  }
  @media (max-width: 767px) {
    margin-left: 10px;
    margin-right: 10px;
  }
`;

const SingleGroup = styled.div`
  margin-top: 35px;
  justify-content: space-between;
  display: flex !important;
  @media (max-width: 767px) {
    flex-direction: column;
  }
`;

const SlideItem = styled.div`
  padding: 0 30px;
  flex: 1 1;
  :nth-child(2) {
    border-left: 1px solid #d3d3d3;
    border-right: 1px solid #d3d3d3;
  }
  :first-child {
    padding-left: 0;
  }
  :last-child {
    padding-right: 0;
  }
`;

const LinkTo = styled.a`
  text-decoration: none;
  font-family: "MullerRegular", sans-serif;
  color: #000;
  line-height: 1.29;
  margin-bottom: 16px;
  display: block;
  :hover {
    color: #000;
  }
  :last-child {
    margin-bottom: 0;
  }
  @media (max-width: 767px) {
    margin-top: 10px;
    padding-bottom: 10px;
    font-size: 15px;
    border-bottom: 1px solid #d3d3d3;
  }
`;

const CustomArrows = styled.div`
  font-size: 0;
  line-height: 0;
  position: absolute;
  bottom: -8px;
  top: auto;
  display: block;
  width: 20px;
  height: 20px;
  padding: 0;
  cursor: pointer;
  color: transparent;
  border: none;
  outline: none;
  background: transparent;
`;

const CustomLeftArrow = styled(CustomArrows)`
  left: 40%;
  ::before {
    left: 40%;
    content: url(${leftArrowImg});
  }
`;

const CustomRightArrow = styled(CustomArrows)`
  right: 40%;
  ::before {
    right: 40%;
    content: url(${rightArrowImg});
  }
`;

const SampleNextArrow = ({ onClick, className }) => {
  return <CustomRightArrow className={className} onClick={onClick} />;
};

const SamplePrevArrow = ({ onClick, className }) => {
  return <CustomLeftArrow className={className} onClick={onClick} />;
};

const NewsSlider = () => {
  const [data, setData] = useState([]);
  useEffect(() => {
    News.getList().then(setData);
  }, []);
  // console.log("----------------->", data);

  const settings = {
    className: "center",
    infinite: false,
    slidesToShow: 1,
    speed: 500,
    // waitForAnimate: false,  ---- Не дожидается окончания перелистывания слайда , работает хуже чем в slick jquery
    dots: true,
    swipeToSlide: true,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    appendDots: dots => (
      <div
        style={{
          position: "static",
          marginTop: "20px"
        }}
      >
        <ul style={{ margin: "0px" }}> {dots} </ul>
      </div>
    ),
    responsive: [
      {
        breakpoint: 767,
        settings: {
          arrows: false,
          swipe: true,
          centerMode: false,
          slidesToShow: 1,
          touchMove: true
        }
      }
    ]
  };

  const chunkArray = (arr, chunk) => {
    let i,
      j,
      tmp = [];
    for (i = 0, j = arr.length; i < j; i += chunk) {
      tmp.push(arr.slice(i, i + chunk));
    }
    return tmp;
  };

  const spreadArray = arr => {
    let spreadArr = [];
    for (let i = 0; i < arr.length; i++) {
      arr[i].map(el => {
        spreadArr.push(el);
      });
    }
    return spreadArr.slice(0, 12);
  };

  const isDesktop = useDesktop();
  const chunk = chunkArray(newsMock, 3);
  const chunkMobile = chunkArray(spreadArray(newsMock), 4);

  return (
    <SliderContainer>
      <h2>Новости</h2>

      <Slider {...settings}>
        {isDesktop
          ? chunk.map((elem, index) => (
              <SingleGroup key={index} className="slideGroup">
                {elem.map((el, idx) => (
                  <SlideItem key={idx} className="slideItem">
                    {el.map((e, i) => (
                      <LinkTo key={i} href={e.url}>
                        <span>{e.title}</span>
                      </LinkTo>
                    ))}
                  </SlideItem>
                ))}
              </SingleGroup>
            ))
          : chunkMobile.map((elem, index) => (
              <SingleGroup key={index}>
                {elem.map((el, i) => {
                  return (
                    <LinkTo key={i} href={el.url}>
                      <span>{el.title}</span>
                    </LinkTo>
                  );
                })}
              </SingleGroup>
            ))}
      </Slider>
    </SliderContainer>
  );
};

export default NewsSlider;
