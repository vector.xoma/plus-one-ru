import React, { useState } from "react";
import ListItem from "./ListItem/";
// import "./NumberingListComponent.scss";
import { Popover, Button, Checkbox, Icon } from "antd";

const NumberingListComponent = ({ setList }) => {
  const defaultItem = { id: 1, name: "" };
  const [items, setItems] = useState([defaultItem]);
  const [count, setCount] = useState(2);
  const [numericList, setNumericList] = useState(false);
  const [visible, setVisible] = useState(false);

  const addItem = () => {
    const _items = items;
    _items.push({ id: count, name: "" });
    setItems(_items);
    setCount(count + 1);
  };

  const delItem = i => {
    const _items = items;
    _items.splice(i, 1);
    setItems([..._items]);
  };

  const editItem = (index, name) => {
    const _items = items.map((el, i) => {
      return i === index ? { id: el.id, name } : el;
    });
    setItems(_items);
  };
  const Content = (
    <div className="Numbering-list__wrap">
      <Checkbox
        onChange={() => setNumericList(!numericList)}
        checked={numericList}
      >
        Нумерация
      </Checkbox>
      <Button onClick={addItem}>Добавить элемент</Button>
      {items.map((el, i) => (
        <ListItem
          key={el.id}
          item={el}
          index={i}
          delItem={delItem}
          numericList={numericList}
          editItem={editItem}
          addItem={addItem}
        />
      ))}
      <Button
        onClick={() => {
          setList({ elements: items, numeric: numericList });
          setVisible(false);
          setItems([defaultItem]);
        }}
      >
        Добавить на страницу
      </Button>
    </div>
  );
  return (
    <Popover
      content={Content}
      title="Список"
      trigger="click"
      placement="right"
      visible={visible}
      onVisibleChange={visible => {
        setVisible(visible);
      }}
    >
      <Button>
        <Icon type="ordered-list" />
      </Button>
    </Popover>
  );
};

export default NumberingListComponent;
