import React from "react";
import styled from "styled-components";

const NumericContainer = styled.div`
  display: flex;
  max-width: 755px;
  margin: 0 auto;
  font-family: "MullerRegular", sans-serif;
  font-size: 20px;
  margin: 50px auto;
  color: #000000;
`;

const Num = styled.div`
  font-family: "MullerRegular", sans-serif;
  color: #fdbe0f;
  font-size: 20px;
  text-align: left;
  line-height: 1.54;
  display: inline-block;
  min-width: 36px;
`;

const UnNum = styled.div`
  background-color: #fdbe0f;
  width: 7px;
  height: 7px;
  display: block;
  align-self: center;
  margin-right: 29px;
  border-radius: 50%;
`;

const ListTitle = styled.div`
  color: #000;
`;

const ListDevider = styled.div`
  position: absolute;
`;

const List = ({ data }) => (
  <div>
    <ListDevider />
    {data.elements &&
      data.elements.map((el, index) => (
        <NumericContainer key={index}>
          {data.numeric ? <Num>{index + 1}</Num> : <UnNum></UnNum>}
          <ListTitle>{el}</ListTitle>
        </NumericContainer>
      ))}
    <ListDevider />
  </div>
);

export default List;
