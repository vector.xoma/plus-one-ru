import React from "react";
import styled from "styled-components";

const PicRetractionWrap = styled.div`
  max-width: 755px;
  margin: 0 auto;
  display: flex;
  min-height: 128px;
  background-color: #ffffff;
  position: relative;
`;

const PicRetractionImgWrap = styled.div`
  max-width: 175px;
  width: 100%;
`;

const PicRetractionImg = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
  display: block;
`;

const PicRetractionTextWrap = styled.div`
  font-family: "MullerBold", sans-serif;
  border: none;
  font-style: normal;
  padding: 20px 47px 30px 40px;
  margin: 0;
`;

const PicRetractionTitle = styled.div`
  font-size: 19px;
  line-height: 24px;
  color: #000;
`;

const PicRetractionText = styled.div`
  margin-top: 5px;
  font-size: 15px;
  line-height: 20px;
  font-family: "MullerRegular", sans-serif;
  color: #000;
`;
const PicRetractionDivider = styled.div`
  position: absolute;
`;

const PicRetraction = ({ data }) => {
  const rows = data.textPic.split("\n");
  return (
    <PicRetractionWrap>
      <PicRetractionDivider />
      <PicRetractionImgWrap>
        <PicRetractionImg src={data.url} alt="неверный url" />
      </PicRetractionImgWrap>
      <PicRetractionTextWrap>
        <PicRetractionTitle>{data.title}</PicRetractionTitle>
        {rows.length ? (
          <PicRetractionText>
            {rows.map((r, i) => (
              <p key={i}>{r}</p>
            ))}
          </PicRetractionText>
        ) : (
          <PicRetractionText>{data.textPic}</PicRetractionText>
        )}
        <PicRetractionText>{data.textPic}</PicRetractionText>
      </PicRetractionTextWrap>
      <PicRetractionDivider />
    </PicRetractionWrap>
  );
};

export default PicRetraction;
