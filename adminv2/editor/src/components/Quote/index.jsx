import React from "react";
import styled from "styled-components";

const QuoteWrap = styled.div`
  max-width: 755px;
  display: flex;
  align-items: center;
  margin: 0 auto;
`;
const QuoteImgWrap = styled.div`
  max-width: 45px;
  max-height: 45px;
  margin-right: 18px;
`;

const QuoteImg = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

const QuoteTitle = styled.div`
  font-size: 33px;
  font-family: "Krok";
  margin-bottom: 18px;
  line-height: 1.2;
  color: #000;
`;

const QuoteAuthor = styled.div`
  font-size: 17px;
  font-family: "MullerRegular";
  font-style: normal;
  line-height: 25px;
  color: #000;
`;

const Quote = ({ data }) => {
  const rows = data.text.split("\n");

  return (
    <QuoteWrap>
      <QuoteImgWrap>
        <QuoteImg src={data.url} alt="неверный url" />
      </QuoteImgWrap>
      <div>
        <QuoteTitle>{data.author}</QuoteTitle>
        {rows.length ? (
          <QuoteAuthor>
            {rows.map((r, i) => (
              <p key={i}>{r}</p>
            ))}
          </QuoteAuthor>
        ) : (
          <QuoteAuthor>{data.text}</QuoteAuthor>
        )}
      </div>
    </QuoteWrap>
  );
};

export default Quote;
