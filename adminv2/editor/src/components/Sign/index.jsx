import React from "react";
import styled from "styled-components";

const SignContainer = styled.div`
  max-width: 755px;
  margin: 85px auto 100px auto;
`;

const SignDevider = styled.div`
  width: 110px;
  height: 2px;
  background: #000;
  margin-bottom: 15px;
`;

const SignTitle = styled.div`
  font-size: 20px;
  font-family: "MullerBold", sans-serif;
  margin-bottom: 15px;
  color: #000;
  line-height: 35px;
`;

const SignAuthor = styled.div`
  font-size: 20px;
  font-family: "MullerRegular", sans-serif;
  line-height: 25px;
  color: #000;
`;

const Sign = ({ data }) => (
  <SignContainer>
    <SignDevider />
    <SignTitle>{data.title}</SignTitle>
    <SignAuthor>{data.name}</SignAuthor>
  </SignContainer>
);

export default Sign;
