import { splitBySelection } from "utils";

export const setBold = ({ changeText, text, area }) => {
  const { start, middle, end } = splitBySelection(text, area);
  changeText(`${start}[b]${middle}[/b]${end}`);
};

export const setUnder = ({ changeText, text, area }) => {
  const { start, middle, end } = splitBySelection(text, area);
  changeText(`${start}[u]${middle}[/u]${end}`);
};

export const setItalic = ({ changeText, text, area }) => {
  const { start, middle, end } = splitBySelection(text, area);
  changeText(`${start}[i]${middle}[/i]${end}`);
};

export const setLink = ({ changeText, text, area }, { url, urlText }) => {
  const { start, end } = splitBySelection(text, area);
  changeText(`${start}[url="${url}"]${urlText}[/url]${end}`);
};

export const setSign = (
  { changeText, text, area },
  { title = "", name = "" }
) => {
  const { start, end } = splitBySelection(text, area);
  changeText(
    `${start ? `${start}\n` : ""}[sign title="${title}" name="${name}"]\n${end}`
  );
};

export const setTextInsert = (
  { changeText, text, area },
  { textInsert = "" }
) => {
  const { start, end } = splitBySelection(text, area);
  changeText(
    `${start ? `${start}\n` : ""}[textInsert text="${textInsert}"]\n${end}`
  );
};

export const setList = (
  { changeText, text, area },
  { elements, numeric = false }
) => {
  const { start, end } = splitBySelection(text, area);
  const elementsRow = elements.map(el => `[li]${el.name}[/li]`).join("");
  changeText(
    `${
      start ? `${start}\n` : ""
    }[listRows numeric="${numeric}"]${elementsRow}[/listRows]\n${end}`
  );
};

export const setHeaders = (
  { changeText, text, area },
  { size, margin, fz }
) => {
  const { start, middle, end } = splitBySelection(text, area);
  changeText(
    `${start}[headerTitle size="${size}" margin="${margin}" fz="${fz}"]${middle}[/headerTitle]${end}`
  );
};

export const setPicRetraction = (
  { changeText, text, area },
  { url, title, textPic = "" }
) => {
  const { start, end } = splitBySelection(text, area);
  changeText(
    `${
      start ? `${start}\n` : ""
    }[textInsert url="${url}" title="${title}" textPic="${textPic}"]\n${end}`
  );
};

export const setQuote = (
  { changeText, text, area },
  { url = "", author = "", textQuote = "" }
) => {
  const { start, end } = splitBySelection(text, area);
  changeText(
    `${
      start ? `${start}\n` : ""
    }[quote url="${url}" author="${author}" text="${textQuote}"]\n${end}`
  );
};

export const setInterview = (
  { changeText, text, area },
  { question = "", answer = "" }
) => {
  const { start, end } = splitBySelection(text, area);
  changeText(
    `${
      start ? `${start}\n` : ""
    }[interview question="${question}" answer="${answer}"]\n${end}`
  );
};

export const setFullWidthPic = (
  { changeText, text, area },
  { url = "", title = "", source = "" }
) => {
  const { start, end } = splitBySelection(text, area);
  changeText(
    `${
      start ? `${start}\n` : ""
    }[fullWidthPic url="${url}" title="${title}" source="${source}"]\n${end}`
  );
};

export const setEmbed = ({ changeText, text, area }, { embedInsert, type }) => {
  const { start, end } = splitBySelection(text, area);
  changeText(
    `${
      start ? `${start}\n` : ""
    }[embed embedInsert="${embedInsert}" type="${type}"]\n${end}`
  );
};

export const setALink = ({ changeText, text, area }, { url, urlText }) => {
  const { start, end } = splitBySelection(text, area);
  changeText(`${start}[alink url="${url}"]${urlText}[/alink]${end}`);
};

export const setFotoRama = ({ changeText, text, area }, { id }) => {
  const { start, end } = splitBySelection(text, area);
  changeText(`${start ? `${start}\n` : ""}[fotorama id="${id}"]\n${end}`);
};

export const setDonateForm = ({ changeText, text, area }) => {
  const { start, end } = splitBySelection(text, area);
  changeText(`${start ? `${start}\n` : ""}[donateForm]\n${end}`);
};

export const setSubscribeForm = ({ changeText, text, area }) => {
  const { start, end } = splitBySelection(text, area);
  console.log("here!");
  changeText(`${start ? `${start}\n` : ""}[subscribeForm]\n${end}`);
};
