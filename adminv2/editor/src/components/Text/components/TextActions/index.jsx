import React from "react";
import { Button, Icon } from "antd";
import styled from "styled-components";

import SignButton from "components/Sign/button";
import TextInsertButton from "components/TextInsert/button";
import PicRetractionButton from "components/PicRetraction/button";
import LinkButton from "components/ALink/button";
import QuoteButton from "components/Quote/button";
import InterviewButton from "components/Interview/button";
import FullWidthPicButton from "components/FullWidthPic/button";
import NumberingListComponent from "components/NumberingListComponent/button";
import HeaderType from "components/HeaderType/button";
import EmbedButton from "components/Embed/button";
import FotoRamaButton from "components/FotoRama/button";
import DonateButton from "components/DonateForm/button";
import SubscribeFormButton from "containers/BlocksViewer/components/Block/components/SubscribeForm/button";

import {
  setBold,
  setUnder,
  setItalic,
  setALink,
  setSign,
  setTextInsert,
  setPicRetraction,
  setQuote,
  setInterview,
  setFullWidthPic,
  setList,
  setHeaders,
  setEmbed,
  setFotoRama,
  setDonateForm,
  setSubscribeForm
} from "./actions";

const ButtonContainerWrap = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-bottom: 10px;
  > * {
    margin: 2px;
  }
`;

const Btn = styled(Button)``;

const TextActions = ({
  changeText,
  text,
  area,
  textHistory,
  setTextHistory
}) => {
  const previos = () => {
    const newHistory = [...textHistory];
    const newText = newHistory[newHistory.length - 2];
    newHistory.splice(-2, 2);
    setTextHistory(newHistory);
    changeText(newText);
  };

  const propsForActions = { changeText, text, area };
  const _setBold = () => setBold(propsForActions);
  const _setUnder = () => setUnder(propsForActions);
  const _setItalic = () => setItalic(propsForActions);
  const _setSign = data => setSign(propsForActions, data);
  const _setTextInsert = data => setTextInsert(propsForActions, data);
  const _setPicRetraction = data => setPicRetraction(propsForActions, data);
  const _setQuote = data => setQuote(propsForActions, data);
  const _setInterview = data => setInterview(propsForActions, data);
  const _setFullWidthPic = data => setFullWidthPic(propsForActions, data);
  const _setList = data => setList(propsForActions, data);
  const _setHeaders = data => setHeaders(propsForActions, data);
  const _setEmbed = data => setEmbed(propsForActions, data);
  const _setALink = data => setALink(propsForActions, data);
  const _setFotoRama = data => setFotoRama(propsForActions, data);
  const _setDonateForm = () => setDonateForm(propsForActions);
  const _setSubscribeForm = () => setSubscribeForm(propsForActions);

  const buttons = [
    { title: <Icon type="arrow-left" />, action: previos },
    { title: <Icon type="bold" />, action: _setBold },
    { title: <Icon type="underline" />, action: _setUnder },
    { title: <Icon type="italic" />, action: _setItalic }
  ];

  return (
    <ButtonContainerWrap>
      {buttons.map((b, i) => (
        <Btn key={i} className="button" onClick={b.action}>
          {b.title}
        </Btn>
      ))}
      <LinkButton setALink={_setALink} textData={propsForActions} />
      <NumberingListComponent setList={_setList} />
      <SignButton setSign={_setSign} />
      <TextInsertButton setTextInsert={_setTextInsert} />
      <PicRetractionButton setPicRetraction={_setPicRetraction} />
      <QuoteButton setQuote={_setQuote} />
      <InterviewButton setInterview={_setInterview} />
      <FullWidthPicButton setFullWidthPic={_setFullWidthPic} />
      <DonateButton setDonateForm={_setDonateForm} />
      <HeaderType setHeaders={_setHeaders} />
      <EmbedButton setEmbed={_setEmbed} />
      <FotoRamaButton setFotoRama={_setFotoRama} />
      <SubscribeFormButton setSubscribeForm={_setSubscribeForm} />
    </ButtonContainerWrap>
  );
};

export default TextActions;
