import React, { useEffect, useState, useRef } from "react";
import _isEmpty from "lodash/isEmpty";
import styled from "styled-components";
// import { clearTextSpaces } from "utils";
import TextActions from "./components/TextActions";
import { parserCustomBBCode } from "./parser";

const FullWidth = styled.div`
  width: 100%;
`;
const CenterWidth = styled.div`
  max-width: ${({ width }) => width};
  margin: 0 auto;
`;

const ShowTextField = styled.div`
  line-height: 1.54;
  width: 100%;
  margin-left: auto;
  margin-right: auto;
  font-size: 20px;
`;

const EditorTextField = styled.textarea`
  border-radius: 10px;
  outline: none;
`;

// разбиваем весь текст по переносам. затем, если вид
const SplitOnRows = text => {
  const splitted = text && text.split("\n");
  const parsedByRows = [];
  if (splitted) {
    for (let i = 0; i < splitted.length; i++) {
      let row = splitted[i];
      if (splitted[i].startsWith("[") && !splitted[i].includes("]")) {
        const currentRow = i;
        for (let y = currentRow + 1; y < splitted.length; y++) {
          row = `${row}\n${splitted[y]}`;
          i++;
          if (splitted[y].endsWith("]")) break;
        }
      }
      parsedByRows.push(row);
    }
  }

  return parsedByRows;
};

const WidthComponent = ({ component, fullWidth, width }, i) =>
  fullWidth ? (
    <FullWidth key={i}>{component}</FullWidth>
  ) : (
    <CenterWidth key={i} width={width}>
      {component}
    </CenterWidth>
  );

const Text = ({ data }) => {
  const parsedByRows = SplitOnRows(data.text);
  return (
    <ShowTextField>
      {data.text &&
        parsedByRows.map((r, i) => WidthComponent(parserCustomBBCode(r, i), i))}
    </ShowTextField>
  );
};

const TextEditor = ({ updateField, data, areaName }) => {
  const textareaRef = useRef();
  const area = textareaRef.current;

  const [textHistory, setTextHistory] = useState([]);

  useEffect(() => {
    if (_isEmpty(data)) updateField({ text: "" });
  }, [data, updateField]);

  useEffect(() => {
    if (typeof data.text === "string")
      setTextHistory([...textHistory, data.text]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data.text]);

  const changeText = val => {
    updateField({
      text: val //clearTextSpaces(val)
    });
  };

  return (
    <>
      <TextActions
        area={area}
        textHistory={textHistory}
        setTextHistory={setTextHistory}
        text={data.text}
        changeText={changeText}
      />

      <EditorTextField
        ref={textareaRef}
        name={areaName}
        rows={10}
        value={data.text}
        onChange={e => changeText(e.target.value)}
      />
    </>
  );
};

Text.Editor = TextEditor;

export default Text;
