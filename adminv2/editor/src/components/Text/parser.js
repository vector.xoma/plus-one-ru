import React from "react";
import parser from "bbcode-to-react";
import styled from "styled-components";

import HeaderTypes from "../HeaderType";
import Sign from "../Sign";
import FotoRama from "../FotoRama";
import TextInsert from "../TextInsert";
import PicRetraction from "../PicRetraction";
import Quote from "../Quote";
import Interview from "../Interview";
import FullWidthPic from "../FullWidthPic";
import List from "../NumberingListComponent";
import Embed from "../Embed";
import ALink from "../ALink";
import DonateForm from "../DonateForm";
import SubscribeForm from "containers/BlocksViewer/components/Block/components/SubscribeForm";

const TextAll = styled.p`
  margin: 0;
  font-family: "MullerRegular", sans-serif;
  font-size: 20px;
  color: #000000;
  line-height: 35px;
  min-height: 35px;
  strong {
    font-weight: 700;
  }
`;

export const parserCustomBBCode = (r, i) => {
  const sign = r.match(/\[sign title="([\s\S]*)" name="([\s\S]*)"]/);
  if (sign)
    return {
      component: <Sign key={i} data={{ title: sign[1], name: sign[2] }} />,
      fullWidth: false,
      width: "755px"
    };

  const insert = r.match(/\[textInsert text="([\s\S]*||\n)"]/m);
  if (insert)
    return {
      component: <TextInsert key={i} data={{ text: insert[1] }} />,
      fullWidth: false,
      width: "755px"
    };

  const interview = r.match(
    /\[interview question="([\s\S]*||\n)" answer="([\s\S]*||\n)"]/m
  );
  if (interview)
    return {
      component: (
        <Interview
          key={i}
          data={{ question: interview[1], answer: interview[2] }}
        />
      ),
      fullWidth: false,
      width: "755px"
    };

  const headerTitle = r.match(
    /\[headerTitle size="([\s\S]*)" margin="([\s\S]*)" fz="([\s\S]*)"\]([\s\S]*)\[\/headerTitle\]/
  );
  if (headerTitle)
    return {
      component: (
        <HeaderTypes
          key={i}
          data={{
            text: headerTitle[4],
            size: headerTitle[1],
            margin: headerTitle[2],
            fz: headerTitle[3]
          }}
        />
      ),
      fullWidth: false,
      width: "755px"
    };

  const retraction = r.match(
    /\[textInsert url="([\s\S]*||\n)" title="([\s\S]*||\n)" textPic="([\s\S]*||\n)"]/m
  );
  if (retraction)
    return {
      component: (
        <PicRetraction
          key={i}
          data={{
            url: retraction[1],
            title: retraction[2],
            textPic: retraction[3]
          }}
        />
      ),
      fullWidth: false,
      width: "755px"
    };

  const quote = r.match(
    /\[quote url="([\s\S]*||\n)" author="([\s\S]*||\n)" text="([\s\S]*||\n)"]/m
  );
  if (quote)
    return {
      component: (
        <Quote
          key={i}
          data={{
            url: quote[1],
            author: quote[2],
            text: quote[3]
          }}
        />
      ),
      fullWidth: false,
      width: "755px"
    };

  const fwp = r.match(
    /\[fullWidthPic url="([\s\S]*||\n)" title="([\s\S]*||\n)" source="([\s\S]*||\n)"]/m
  );
  if (fwp)
    return {
      component: (
        <FullWidthPic
          key={i}
          data={{
            url: fwp[1],
            title: fwp[2],
            source: fwp[3]
          }}
        />
      ),
      fullWidth: true,
      width: "100%"
    };

  const list = r.match(
    /\[listRows numeric="([\s\S]*)"]([\s\S]*)\[\/listRows]/m
  );
  if (list) {
    const elements = list[2];
    const regex = /\[li\](.+?)\[\/li\]/gim;
    let listss = [];
    let m;

    while ((m = regex.exec(elements)) !== null) listss.push(m[1]);

    return {
      component: (
        <List
          key={i}
          data={{ numeric: list[1] === "true", elements: listss }}
        />
      ),
      fullWidth: false,
      width: "755px"
    };
  }

  const embed = r.match(
    /\[embed embedInsert="([\s\S]*||\n)" type="([\s\S]*||\n)"]/m
  );

  if (embed) {
    return {
      component: (
        <Embed
          key={i}
          data={{
            embedInsert: embed[1],
            type: embed[2]
          }}
        />
      ),
      fullWidth: false,
      width: "755px"
    };
  }

  const fotorama = r.match(/\[fotorama id="([\s\S]*)"]/m);
  if (fotorama)
    return {
      component: <FotoRama key={i} data={{ id: fotorama[1] }} />,
      fullWidth: false,
      width: "920px"
    };

  const donateForm = r.match(/\[donateForm]/m);
  if (donateForm)
    return {
      component: <DonateForm key={i} />,
      fullWidth: false,
      width: "1140px"
    };

  const subscribeForm = r.match(/\[subscribeForm]/m);
  if (subscribeForm)
    return {
      component: <SubscribeForm key={i} />,
      fullWidth: false,
      width: "1140px"
    };

  const regexLinkForData = /\[alink url="([\s\S]*)"\]([\s\S]*)\[\/alink\]/m;
  const regexLinkForSplit = /(\[alink url=".+?"\].+?\[\/alink])/gm;
  const link = r.match(regexLinkForData);

  if (link) {
    const linksAndText = r
      .split(regexLinkForSplit)
      .map(item => ({ row: item, link: !!item.match(regexLinkForSplit) }));

    const parseLink = (linkRow, index) => {
      const linkData = linkRow.match(regexLinkForData);
      return (
        <ALink key={index} data={{ url: linkData[1], text: linkData[2] }} />
      );
    };

    return {
      component: (
        <TextAll key={i}>
          {linksAndText.map((el, index) => {
            return el.link ? parseLink(el.row, index) : parser.toReact(el.row);
          })}
        </TextAll>
      ),
      fullWidth: false,
      width: "755px"
    };
  }

  return {
    component: <TextAll key={i}>{parser.toReact(r)}</TextAll>,
    fullWidth: false,
    width: "755px"
  };
};
