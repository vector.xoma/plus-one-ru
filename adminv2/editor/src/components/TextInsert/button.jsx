import React, { useState } from "react";
import { Popover, Button } from "antd";
import { Input } from "antd";
import styled from "styled-components";

const EditorTextInsert = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  textarea {
    width: 500px;
  }
`;

const { TextArea } = Input;

const TextInsertButton = ({ setTextInsert }) => {
  const [text, setText] = useState("");
  const [visible, setVisible] = useState(false);

  const Content = (
    <EditorTextInsert>
      <div>
        <TextArea
          placeholder="Текст"
          value={text || ""}
          rows={4}
          onChange={e => setText(e.target.value)}
        />
      </div>
      <Button
        onClick={() => {
          setTextInsert({ textInsert: text });
          setVisible(false);
          setText("");
        }}
      >
        Добавить
      </Button>
    </EditorTextInsert>
  );

  return (
    <Popover
      content={Content}
      title="Текстовый врез"
      trigger="click"
      placement="right"
      visible={visible}
      onVisibleChange={visible => {
        setVisible(visible);
      }}
    >
      <Button>Текстовый врез</Button>
    </Popover>
  );
};

export default TextInsertButton;
