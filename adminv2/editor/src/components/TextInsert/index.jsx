import React from "react";
import styled from "styled-components";

const TextInsertTitle = styled.div`
  max-width: 755px;
  margin: 0 auto;
  text-align: center;
  border: 3px solid #f8c946;
  padding: 48px 80px;
  margin: 45px auto;
  font-family: "MullerMedium";
  font-size: 38px;
  color: #000000;
`;

const TextInsert = ({ data }) => {
  const rows = data.text.split("\n");
  if (rows.length)
    return (
      <div>
        <TextInsertTitle>
          {rows.map((r, i) => (
            <p key={i}>{r}</p>
          ))}
        </TextInsertTitle>
      </div>
    );

  return (
    <div>
      <TextInsertTitle>{data.text}</TextInsertTitle>
    </div>
  );
};

export default TextInsert;
