import axios from "axios";

const instance = axios.create({
  // baseURL: "/api"
});

const axiosApi = async () => {
  return instance;
};

export const APIService = {
  async checkStatus401(status) {
    if (status === 401) {
      console.error("wotaFACK! 401");
    }
  },

  async checkStatus403(status) {
    if (status === 403) {
      console.error("wotaFACK! 403");
    }
  },

  checkStatus500(status) {
    if (status === 500) {
      console.error("wotaFACK! 500");
    }
  },

  async GET(route, params) {
    try {
      const API = await axiosApi();
      const { data } = await API.get(route, { params });
      return data;
    } catch ({ response: { status } }) {
      this.checkStatus401(status);
      this.checkStatus403(status);
      this.checkStatus500(status);
    }
  },

  async POST(route, params) {
    try {
      const API = await axiosApi();
      const { data } = await API.post(route, params);
      return data;
    } catch (e) {
      const {
        response: { status }
      } = e;
      this.checkStatus401(status);
      this.checkStatus403(status);
      this.checkStatus500(status);
      console.log("status", status, e);
      throw e;
    }
  },

  async DELETE(route, params) {
    try {
      const API = await axiosApi();
      console.log("route, params", route, params);
      const { data } = await API.delete(route, params);
      return data;
    } catch ({ response: { status } }) {
      this.checkStatus401(status);
      this.checkStatus403(status);
      this.checkStatus500(status);
    }
  },
  async PUT(route, params) {
    try {
      const API = await axiosApi();
      const { data } = await API.put(route, params);
      return data;
    } catch ({ response: { status } }) {
      this.checkStatus401(status);
      this.checkStatus403(status);
      this.checkStatus500(status);
    }
  }
};
