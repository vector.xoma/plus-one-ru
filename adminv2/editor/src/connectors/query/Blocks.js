import { APIService } from "connectors/api/rest";

export const Blocks = {
  async getList(rubric) {
    try {
      return await APIService.GET(`https://react.plus-one.ru/api/v1/${rubric}`);
    } catch (e) {
      throw e;
    }
  },
  async getRows() {
    try {
      return await APIService.GET(
        // "https://dev.plus-one.ru/adminv2/api/v1/main-page"
        "https://react.plus-one.ru/api/v1/main-page"
      );
    } catch (e) {
      throw e;
    }
  }
};
