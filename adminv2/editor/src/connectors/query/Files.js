import { APIService } from "connectors/api/rest";

export const Files = {
  async getFolder(params) {
    try {
      return await APIService.GET(
        "https://react.plus-one.ru/adminv2/api/v1/file-manager",
        params
      );
    } catch (e) {
      throw e;
    }
  }
};
