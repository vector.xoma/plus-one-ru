import { APIService } from "connectors/api/rest";

export const Galleries = {
  async getList() {
    try {
      return await APIService.GET(
        "https://react.plus-one.ru/adminv2/api/v1/galleries"
      );
    } catch (e) {
      throw e;
    }
  },

  async getGallery(id) {
    try {
      return await APIService.GET(
        `https://react.plus-one.ru/adminv2/api/v1/galleries/${id}`
      );
    } catch (e) {
      throw e;
    }
  }
};
