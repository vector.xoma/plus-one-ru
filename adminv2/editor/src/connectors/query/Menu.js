import { APIService } from "connectors/api/rest";

export const Menu = {
  async getList() {
    try {
      return await APIService.GET("https://react.plus-one.ru/api/v1/main-menu");
    } catch (e) {
      throw e;
    }
  }
};
