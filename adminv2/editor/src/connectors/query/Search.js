import { APIService } from "connectors/api/rest";

export const Search = {
  async getList(params) {
    try {
      return await APIService.GET(
        "https://dev.plus-one.ru/api/autocomplette",
        params
      );
    } catch (e) {
      throw e;
    }
  }
};
