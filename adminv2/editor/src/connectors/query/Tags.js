import { APIService } from "connectors/api/rest";

export const Tags = {
  async getList(type, page) {
    try {
      return await APIService.GET(
        `https://react.plus-one.ru/api/v1/${type}/${page}`
      );
    } catch (e) {
      throw e;
    }
  }
};
