import { APIService } from "connectors/api/rest";

export const News = {
  async getList() {
    try {
      return await APIService.GET("https://react.plus-one.ru/api/v1/main-news");
    } catch (e) {
      throw e;
    }
  }
};
