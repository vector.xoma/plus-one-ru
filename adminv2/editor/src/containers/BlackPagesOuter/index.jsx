import React, { useState } from "react";
import useDesktop from "hooks/useDesktop";
import _get from "lodash/get";

import DropDown from "components/DropDown";
import HeaderComponent from "components/HeaderComponent/HeaderComponent";
import HeaderBottom from "components/HeaderBottom";
import Informer from "components/Informer";

const BlackPagesOuter = ({ children }) => {
  const isDesktop = useDesktop();
  const [visible, setVisible] = useState(false);

  return (
    <div
      style={{
        height: "100%",
        background: "#212121",
        minHeight: "100vh"
      }}
    >
      <DropDown visible={visible} setVisible={setVisible} />
      <HeaderComponent />
      <HeaderBottom visible={visible} setVisible={setVisible} />
      {isDesktop && <Informer />}

      {children}
    </div>
  );
};

export default BlackPagesOuter;
