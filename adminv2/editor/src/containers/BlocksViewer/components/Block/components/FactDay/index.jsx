import React from "react";
import _get from "lodash/get";
import styled from "styled-components";

import { clearText } from "clearText";

import {
  GridItem1_1,
  GridItem1_2,
  GridItem1_3,
  GridItemGroup,
  AWrap
} from "components/Grid/Grid";

const FactDay1_1 = styled(GridItem1_1)`
  height: 100%;
  min-height: auto;
  background-color: ${({ bgc }) => bgc};
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  ${({ customStyle }) => customStyle}
  @media (max-width: 767px) {
    padding: 20px;
    min-height: auto;
  }
`;
const FactDay1_2 = styled(GridItem1_2)`
  height: 100%;
  background-color: ${({ bgc }) => bgc};
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  ${({ customStyle }) => customStyle}
  @media (max-width: 767px) {
    padding: 20px;
    min-height: auto;
  }
`;

const FactDay1_3 = styled(GridItem1_3)`
  height: 100%;
  background-color: ${({ bgc }) => bgc};
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  ${({ customStyle }) => customStyle}
  @media (max-width: 767px) {
    padding: 20px;
    min-height: auto;
  }
`;
const FactDay1_1Text = styled.a``;
const FactDay1_2Text = styled.a``;
const FactDay1_3Text = styled.a``;

const FactDay1_1Title = styled.div`
  font-size: 42px;
  font-family: "GeometricSansSerifv1";
  text-align: center;
  line-height: 42px;
  color: ${({ fontColor }) => fontColor};
  ${({ customStyleTitle }) => customStyleTitle}
  @media (max-width: 767px) {
    font-size: 28px;
    line-height: 1.07;
  }
`;
const FactDay1_2Title = styled.div`
  font-size: 42px;
  font-family: "GeometricSansSerifv1";
  text-align: center;
  line-height: 42px;
  color: ${({ fontColor }) => fontColor};
  ${({ customStyleTitle }) => customStyleTitle}
  @media (max-width: 767px) {
    font-size: 28px;
    line-height: 1.07;
  }
`;
const FactDay1_3Title = styled.div`
  font-size: 36px;
  font-family: "GeometricSansSerifv1";
  text-align: center;
  line-height: 1;
  color: ${({ fontColor }) => fontColor};
  ${({ customStyleTitle }) => customStyleTitle}
  @media (max-width: 767px) {
    font-size: 28px;
    line-height: 1.07;
  }
`;

const FactGroupItem = styled(GridItemGroup)`
  background-color: ${({ bgc }) => bgc};
`;

const FactDay = ({ size, url, data, style = {} }) => {
  console.log("size", size);
  switch (size) {
    case "1_1": {
      return (
        <FactDay1_1
          bgc={_get(data, "postFormat.backgroundColor")}
          customStyle={style.container}
        >
          <AWrap href={url}></AWrap>
          <FactDay1_1Text href={url}>
            <FactDay1_1Title
              fontColor={_get(data, "postFormat.fontColor")}
              customStyleTitle={style.textTitle}
            >
              {clearText(data.name)}
            </FactDay1_1Title>
          </FactDay1_1Text>
        </FactDay1_1>
      );
    }
    case "1_2": {
      return (
        <FactDay1_2
          bgc={_get(data, "postFormat.backgroundColor")}
          customStyle={style.container}
        >
          <AWrap href={url}></AWrap>
          <FactDay1_2Text href={url}>
            <FactDay1_2Title
              fontColor={_get(data, "postFormat.fontColor")}
              customStyleTitle={style.textTitle}
            >
              {clearText(data.name)}
            </FactDay1_2Title>
          </FactDay1_2Text>
        </FactDay1_2>
      );
    }
    case "1_3": {
      return (
        <FactDay1_3
          bgc={_get(data, "postFormat.backgroundColor")}
          customStyle={style.container}
        >
          <AWrap href={url}></AWrap>
          <FactDay1_3Text href={url}>
            <FactDay1_3Title
              fontColor={_get(data, "postFormat.fontColor")}
              customStyleTitle={style.textTitle}
            >
              {clearText(data.name)}
            </FactDay1_3Title>
          </FactDay1_3Text>
        </FactDay1_3>
      );
    }
    default: {
      return (
        <FactGroupItem bgc={_get(data, "postFormat.backgroundColor")}>
          <AWrap href={url}></AWrap>
          <FactDay1_3Text href={url}>
            <FactDay1_3Title fontColor={_get(data, "postFormat.fontColor")}>
              {clearText(data.name)}
            </FactDay1_3Title>
          </FactDay1_3Text>
        </FactGroupItem>
      );
    }
  }
};

export default FactDay;
