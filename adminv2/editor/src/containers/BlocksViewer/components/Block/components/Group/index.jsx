import React from "react";
import styled from "styled-components";
import Slider from "react-slick";
import Block from "containers/BlocksViewer/components/Block";

import leftArrowImg from "img/arrowLeft.svg";
import rightArrowImg from "img/arrowRight.svg";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const SliderContainer = styled.div`
  width: 100%;
  margin: 0 auto;
  background-color: #eaeaea;
  padding: 30px 0 30px;
  margin-bottom: 20px;
  border-radius: 5px;
  h2 {
    margin-bottom: 20px;
    font-family: "MullerBold";
    font-size: 30px;
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.2;
    letter-spacing: normal;
    text-align: center;
    color: #000000;
  }
  .slick-dots {
    li {
      margin: 0;
      button {
        ::before {
          font-size: 8px;
        }
      }
    }
  }
`;

const CustomArrows = styled.div`
  font-size: 0;
  line-height: 0;
  position: absolute;
  bottom: -8px;
  top: auto;
  display: block;
  width: 20px;
  height: 20px;
  padding: 0;
  cursor: pointer;
  color: transparent;
  border: none;
  outline: none;
  background: transparent;
`;

const CustomLeftArrow = styled(CustomArrows)`
  left: 40%;
  ::before {
    left: 40%;
    content: url(${leftArrowImg});
  }
`;

const CustomRightArrow = styled(CustomArrows)`
  right: 40%;
  ::before {
    right: 40%;
    content: url(${rightArrowImg});
  }
`;

const SampleNextArrow = ({ onClick, className }) => {
  return <CustomRightArrow className={className} onClick={onClick} />;
};

const SamplePrevArrow = ({ onClick, className }) => {
  return <CustomLeftArrow className={className} onClick={onClick} />;
};

const Group = ({ data = [], rowType }) => {
  console.log("data", data);
  const settings = {
    className: "center",
    centerMode: true,
    infinite: true,
    centerPadding: "50px",
    slidesToShow: 3,
    speed: 500,
    // waitForAnimate: false,  ---- Не дожидается окончания перелистывания слайда , работает хуже чем в slick jquery
    dots: true,
    swipeToSlide: true,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    appendDots: dots => (
      <div
        style={{
          position: "static",
          marginTop: "20px"
        }}
      >
        <ul style={{ margin: "0px" }}> {dots} </ul>
      </div>
    ),
    responsive: [
      {
        breakpoint: 767,
        settings: {
          arrows: false,
          swipe: true,
          centerMode: true,
          centerPadding: "20px",
          slidesToShow: 1,
          touchMove: true
        }
      }
    ]
  };
  return data.length > 0 ? (
    <SliderContainer>
      <h2>{data.name}</h2>

      <Slider {...settings}>
        {data.map(({ id, block, url, typePost, ...other }) => (
          <Block
            key={id}
            size={"group"}
            url={url}
            type={typePost}
            data={other}
          />
        ))}
      </Slider>
    </SliderContainer>
  ) : null;
};

export default Group;
