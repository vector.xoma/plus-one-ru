import React from "react";
import styled from "styled-components";

import { clearText } from "clearText";

import { GridItemGroup, AWrap } from "components/Grid/Grid";

const NewsWrap = styled(GridItemGroup)`
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  img {
    z-index: auto;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    display: block;
    width: 100%;
    height: 100%;
    border-radius: 5px;
    object-fit: cover;
  }
  ::after {
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background-image: linear-gradient(
      to bottom,
      rgba(0, 0, 0, 0.22),
      rgba(0, 0, 0, 0.41) 48%,
      rgba(0, 0, 0, 0.8)
    );
    z-index: 5;
    border-radius: 5px;
  }
  ${({ customStyle }) => customStyle}
  @media (max-width: 767px) {
    padding: 20px;
    min-height: 410px;
  }
`;
const NewsText = styled.a`
  min-height: initial;
  text-align: left;
  z-index: 6;
  position: relative;
  text-decoration: none;
`;

const NewsTitle = styled.div`
  font-size: 30px;
  line-height: 1.2;
  font-family: "MullerBold", sans-serif;
  color: ${({ fontColor }) => fontColor};
  color: #fff;
  ${({ customStyle }) => customStyle}
  @media (max-width: 767px) {
    font-size: 18px;
    font-family: "MullerBold";
  }
`;

const NewsSubTitle = styled.div`
  display: flex;
  margin-top: 20px;
  justify-content: center;
  font-size: 24px;
  font-family: "MullerRegular";
  line-height: 1.33;
  color: ${({ fontColor }) => fontColor};
  color: #fff;
  ${({ customStyle }) => customStyle}
  @media (max-width: 767px) {
    font-size: 16px;
  }
`;

const News = ({ size, url, data, style = {} }) => {
  return (
    <NewsWrap className="News" customStyle={style.container}>
      <AWrap href={url}></AWrap>
      {data.image ? <img src={data.image} /> : null}
      <NewsText href={url}>
        <NewsTitle
          //   fontColor={data.postFormat.fontColor}
          customStyle={style.textTitle}
        >
          {clearText(data.name)}
        </NewsTitle>
        {data.header ? (
          <NewsSubTitle
            className="newsSubtitle"
            // fontColor={data.postFormat.fontColor}
            customStyle={style.textSubTitle}
          >
            {clearText(data.header)}
          </NewsSubTitle>
        ) : null}
      </NewsText>
    </NewsWrap>
  );
};

export default News;
