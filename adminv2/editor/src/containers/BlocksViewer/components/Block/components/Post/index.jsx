import React from "react";
import _get from "lodash/get";
import styled from "styled-components";

import { clearText } from "clearText";

import {
  GridItem1_1,
  GridItem1_2,
  GridItem1_3,
  GridItemGroup,
  AWrap
} from "components/Grid/Grid";

const Post1_1 = styled(GridItem1_1)`
  display: flex;
  flex-direction: column;
  img {
    z-index: auto;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    display: block;
    width: 100%;
    height: 100%;
    border-radius: 5px;
    object-fit: cover;
  }
  ::after {
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background-image: linear-gradient(
      to bottom,
      rgba(0, 0, 0, 0.22),
      rgba(0, 0, 0, 0.41) 48%,
      rgba(0, 0, 0, 0.8)
    );
    z-index: 5;
    border-radius: 5px;
  }
  ${({ customStyle }) => customStyle}
  @media (max-width: 767px) {
    justify-content: flex-end;
    padding: 20px;
    min-height: 410px;
  }
`;

const Post1_1Text = styled.a`
  min-height: initial;
  text-align: center;
  z-index: 6;
  position: relative;
  margin-bottom: 20px;
  order: 1;
  margin: auto;
  text-decoration: none;
  ${({ customStyle }) => customStyle}
  @media (max-width: 767px) {
    margin: 0;
    text-align: left;
  }
`;

const Post1_1Title = styled.div`
  font-family: "CSTMXprmntl02-Bold", sans-serif;
  font-size: 66px;
  line-height: 1;
  color: ${({ fontColor }) => fontColor};
  color: #fff;
  ${({ customStyle }) => customStyle}
  @media (max-width: 767px) {
    font-size: 18px;
    font-family: "MullerBold";
  }
`;

const Post1_1SubTitle = styled.div`
  margin-top: 20px;
  font-size: 24px;
  font-family: "MullerRegular";
  display: flex;
  justify-content: center;
  line-height: 1.33;
  color: ${({ fontColor }) => fontColor};
  color: #fff;

  ${({ customStyle }) => customStyle}
  @media (max-width: 767px) {
    font-size: 16px;
  }
`;
// Post1_2 -----------------------------------------------

const Post1_2 = styled(GridItem1_2)`
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  img {
    z-index: auto;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    display: block;
    width: 100%;
    height: 100%;
    border-radius: 5px;
    object-fit: cover;
  }
  ::after {
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background-image: linear-gradient(
      to bottom,
      rgba(0, 0, 0, 0.22),
      rgba(0, 0, 0, 0.41) 48%,
      rgba(0, 0, 0, 0.8)
    );
    z-index: 5;
    border-radius: 5px;
  }
  ${({ customStyle }) => customStyle}
  @media (max-width: 767px) {
    padding: 20px;
    min-height: 410px;
  }
`;

const Post1_2Text = styled.a`
  max-width: 400px;
  min-height: initial;
  text-align: left;
  z-index: 6;
  position: relative;
  text-decoration: none;
`;

const Post1_2Title = styled.div`
  font-size: 30px;
  line-height: 1.2;
  font-family: "MullerBold", sans-serif;
  color: ${({ fontColor }) => fontColor};
  color: #fff;
  ${({ customStyle }) => customStyle}
  @media (max-width: 767px) {
    font-size: 18px;
    font-family: "MullerBold";
  }
`;

const Post1_2SubTitle = styled.div`
  margin-top: 20px;
  font-family: "MullerRegular", sans-serif;
  font-size: 19px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.26;
  letter-spacing: normal;
  text-align: left;
  color: ${({ fontColor }) => fontColor};
  color: #fff;
  ${({ customStyle }) => customStyle}
  @media (max-width: 767px) {
    font-size: 16px;
  }
`;

//  Post1_3 ---------------------------------------

const Post1_3 = styled(GridItem1_3)`
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  img {
    z-index: auto;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    display: block;
    width: 100%;
    height: 100%;
    border-radius: 5px;
    object-fit: cover;
  }
  ::after {
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background-image: linear-gradient(
      to bottom,
      rgba(0, 0, 0, 0.22),
      rgba(0, 0, 0, 0.41) 48%,
      rgba(0, 0, 0, 0.8)
    );
    z-index: 5;
    border-radius: 5px;
  }
  ${({ customStyle }) => customStyle}
  @media (max-width: 767px) {
    padding: 20px;
    min-height: 410px;
  }
`;

const Post1_3Text = styled.a`
  min-height: initial;
  text-align: left;
  z-index: 6;
  position: relative;
  text-decoration: none;
`;

const Post1_3Title = styled.div`
  font-size: 30px;
  line-height: 1.2;
  font-family: "MullerBold", sans-serif;
  color: ${({ fontColor }) => fontColor};
  color: #fff;
  ${({ customStyle }) => customStyle}
  @media (max-width: 767px) {
    font-size: 18px;
    font-family: "MullerBold";
  }
`;

const Post1_3SubTitle = styled.div`
  display: flex;
  margin-top: 20px;
  justify-content: center;
  font-size: 24px;
  font-family: "MullerRegular";
  line-height: 1.33;
  color: ${({ fontColor }) => fontColor};
  color: #fff;
  ${({ customStyle }) => customStyle}
  @media (max-width: 767px) {
    font-size: 16px;
  }
`;

const PostGroupItem = styled(GridItemGroup)`
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  img {
    z-index: auto;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    display: block;
    width: 100%;
    height: 100%;
    border-radius: 5px;
    object-fit: cover;
  }
  ::after {
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background-image: linear-gradient(
      to bottom,
      rgba(0, 0, 0, 0.22),
      rgba(0, 0, 0, 0.41) 48%,
      rgba(0, 0, 0, 0.8)
    );
    z-index: 5;
    border-radius: 5px;
  }
`;

const Post = ({ size, url, data, style = {} }) => {
  switch (size) {
    case "1_1": {
      return (
        <Post1_1 customStyle={style.container}>
          <AWrap href={url}></AWrap>
          {data.image ? <img src={data.image} /> : null}
          <Post1_1Text href={url} customStyle={style.textWrap}>
            <Post1_1Title
              fontColor={_get(data, "postFormat.fontColor")}
              customStyle={style.textTitle}
            >
              {clearText(data.name)}
            </Post1_1Title>
            {data.header ? (
              <Post1_1SubTitle
                customStyle={style.textSubTitle}
                fontColor={_get(data, "postFormat.fontColor")}
              >
                {clearText(data.header)}
              </Post1_1SubTitle>
            ) : null}
          </Post1_1Text>
        </Post1_1>
      );
    }
    case "1_2": {
      return (
        <Post1_2 customStyle={style.container}>
          <AWrap href={url}></AWrap>
          {data.image ? <img src={data.image} /> : null}
          <Post1_2Text href={url}>
            <Post1_2Title
              fontColor={_get(data, "postFormat.fontColor")}
              customStyle={style.textTitle}
            >
              {clearText(data.name)}
            </Post1_2Title>
            {data.header ? (
              <Post1_2SubTitle
                fontColor={_get(data, "postFormat.fontColor")}
                customStyle={style.textSubTitle}
              >
                {clearText(data.header)}
              </Post1_2SubTitle>
            ) : null}
          </Post1_2Text>
        </Post1_2>
      );
    }
    case "1_3": {
      return (
        <Post1_3 className="here" customStyle={style.container}>
          <AWrap href={url}></AWrap>
          {data.image ? <img src={data.image} /> : null}
          <Post1_3Text href={url}>
            <Post1_3Title
              fontColor={_get(data, "postFormat.fontColor")}
              customStyle={style.textTitle}
            >
              {clearText(data.name)}
            </Post1_3Title>
            {data.header ? (
              <Post1_3SubTitle
                fontColor={_get(data, "postFormat.fontColor")}
                customStyle={style.textSubTitle}
              >
                {clearText(data.header)}
              </Post1_3SubTitle>
            ) : null}
          </Post1_3Text>
        </Post1_3>
      );
    }
    default: {
      return (
        <PostGroupItem>
          <AWrap href={url}></AWrap>
          {data.image ? <img src={data.image} /> : null}
          <Post1_3Text href={url}>
            <Post1_3Title fontColor={_get(data, "postFormat.fontColor")}>
              {clearText(data.name)}
            </Post1_3Title>
            {data.header ? (
              <Post1_3SubTitle fontColor={_get(data, "postFormat.fontColor")}>
                {clearText(data.header)}
              </Post1_3SubTitle>
            ) : null}
          </Post1_3Text>
        </PostGroupItem>
      );
    }
  }
  return "Post";
};

export default Post;
