import React from "react";
import styled from "styled-components";

import { clearText } from "clearText";

import {
  GridItem1_1,
  GridItem1_2,
  GridItem1_3,
  GridItemGroup,
  AWrap
} from "components/Grid/Grid";

const H2 = styled.h2``;
const Quote1_1 = styled(GridItem1_1)`
  width: 100%;
  min-height: auto;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 20px 30px;
  background-color: ${({ bgc }) => bgc};
  background-color: #eaeaea;
  ${({ customStyle }) => customStyle}

  h2 {
    font-size: 28px;
    line-height: 1.21;
    font-family: "Krok", sans-serif;
    text-align: center;
    color: #000;
    ${({ customStyleH2 }) => customStyleH2}
  }
  div {
    font-family: "MullerRegular", sans-serif;
    font-size: 19px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.26;
    letter-spacing: normal;
    text-align: left;
    color: #000000;
  }
  @media (max-width: 767px) {
    padding: 20px;
    ${H2} {
      font-size: 23px;
      line-height: 1.26;
    }
  }
`;
const Quote1_2 = styled(GridItem1_2)`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 20px 30px;
  background-color: ${({ bgc }) => bgc};
  background-color: #eaeaea;
  ${({ customStyle }) => customStyle}
  ${H2} {
    font-size: 34px;
    line-height: 1.12;
    font-family: "Krok", sans-serif;
    text-align: left;
    color: #000;
    ${({ customStyleH2 }) => customStyleH2}
  }
  div {
    font-family: "MullerRegular", sans-serif;
    font-size: 19px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.26;
    letter-spacing: normal;
    text-align: left;
    color: #000000;
  }
  @media (max-width: 767px) {
    padding: 20px;
    min-height: auto;
    ${H2} {
      font-size: 23px;
      line-height: 1.26;
      text-align: center;
    }
  }
`;

const Quote1_3 = styled(GridItem1_3)`
  width: 100%;
  display: flex;
  background-color: #eaeaea;
  flex-direction: column;
  justify-content: space-between;
  background-color: ${({ bgc }) => bgc};
  padding: 20px 30px;
  margin: 0 10px;
  ${({ customStyle }) => customStyle}
  ${H2} {
    text-align: left;
    font-size: 27px;
    font-family: "Krok", sans-serif;
    text-align: center;
    color: #000;
    ${({ customStyleH2 }) => customStyleH2}
  }
  div {
    font-family: "MullerRegular", sans-serif;
    font-size: 19px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.26;
    letter-spacing: normal;
    text-align: left;
    color: #000000;
  }

  @media (max-width: 767px) {
    padding: 20px;
    min-height: auto;
    ${H2} {
      font-size: 23px;
      line-height: 1.26;
      text-align: center;
    }
  }
`;

const Quote1_3_Group = styled(GridItemGroup)`
  background-color: ${({ bgc }) => bgc};
`;

const Quote = ({ size, data, url, style = {} }) => {
  switch (size) {
    case "1_1":
      return (
        <Quote1_1
          size={size}
          customStyle={style.container}
          customStyleH2={style.textTitle}
        >
          <AWrap href={url}></AWrap>
          <H2>{clearText(data.name)}</H2>
          <div>{clearText(data.header)}</div>
        </Quote1_1>
      );
    case "1_2":
      return (
        <Quote1_2
          size={size}
          customStyle={style.container}
          customStyleH2={style.textTitle}
        >
          <AWrap href={url}></AWrap>
          <H2>{clearText(data.name)}</H2>
          <div>{clearText(data.header)}</div>
        </Quote1_2>
      );
    case "1_3":
      return (
        <Quote1_3
          size={size}
          customStyle={style.container}
          customStyleH2={style.textTitle}
        >
          <AWrap href={url}></AWrap>
          <H2>{clearText(data.name)}</H2>
          <div>{clearText(data.header)}</div>
        </Quote1_3>
      );
    default:
      return (
        <Quote1_3_Group size={size}>
          <AWrap href={url}></AWrap>
          <H2>{clearText(data.name)}</H2>
          <div>{clearText(data.header)}</div>
        </Quote1_3_Group>
      );
  }
};

export default Quote;
