import React from "react";
import styled from "styled-components";

const SubscribeContainer = styled.div`
  @media (max-width: 767px) {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    height: 100%;
  }
`;
const SubscribeEmailTitle = styled.div`
  display: block;
  font-family: "MullerRegular", sans-serif;
  font-size: 20px;
  line-height: 1.4;
  margin-bottom: 10px;
  text-align: left;
  color: #000;
  @media (max-width: 767px) {
    font-family: "CSTMXprmntl02-Bold", sans-serif;
    font-size: 26px;
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.15;
    letter-spacing: normal;
    text-align: center;
    color: #000000;
  }
`;
const FormContaier = styled.form`
  display: flex;
  justify-content: space-between;
  @media (max-width: 767px) {
    flex-direction: column;
  }
`;

const InformMessageContainer = styled.div`
  h2 {
    max-width: 570px;
    margin: 0 auto 20px;
    text-align: center;
    font-size: 24px;
    line-height: 1.17;
    font-family: "MullerRegular", sans-serif;
    color: #000;
  }
  button {
    min-height: 62px;
    display: block;
    margin: 0 auto;
    text-align: center;
  }
  @media (max-width: 767px) {
    height: 100%;
    justify-content: space-between;
    display: flex;
    flex-direction: column;
    h2 {
      font-size: 20px;
      line-height: 1.05;
      margin: auto;
    }
  }
`;

const Input = styled.input`
  width: 100%;
  max-width: 692px;
  display: block;
  padding: 11px 0;
  border-radius: 5px;
  border: 2px solid black;
  background-color: transparent;
  outline: none;
  line-height: 1.2;
  font-family: "CSTMXprmntl02-Bold", sans-serif;
  font-size: 30px;
  text-align: center;
  color: #000;
  :focus {
    border-color: #003bc3;
  }
  ::placeholder {
    color: #000;
  }
  @media (max-width: 767px) {
    max-width: auto;
    min-height: 66px;
    margin-bottom: 10px;
    padding: 0;
    font-size: 20px;
    font-family: "MullerRegular", sans-serif;
  }
`;

const Button = styled.button`
  background-color: black;
  border: none;
  outline: none;
  cursor: pointer;
  box-sizing: border-box;
  max-width: 220px;
  line-height: 1.69;
  color: #eaeaea;
  padding: 2px 54px;
  border-radius: 5px;
  min-width: 220px;
  font-family: "MullerRegular", sans-serif;
  font-size: 18px;
  transition: background-color 0.2s;
  :hover {
    background-color: #003bc3;
  }
  @media (max-width: 767px) {
    max-width: 100%;
    padding: 0;
    min-height: 66px;
  }
`;

const SubscribFormContainer = ({
  placeholder,
  onFocus,
  onBlur,
  onChanghe,
  onSubmit,
  value,
  mailValid,
  informMessage,
  onClick,
  title
}) => {
  return !informMessage ? (
    <SubscribeContainer>
      <SubscribeEmailTitle>{`${
        mailValid ? title : "Ошибка! Введите правильный email"
      }`}</SubscribeEmailTitle>

      <FormContaier>
        <Input
          placeholder={placeholder}
          onFocus={onFocus}
          onBlur={onBlur}
          onChange={onChanghe}
          value={value}
        />
        <Button onClick={onSubmit}>Подписаться</Button>
      </FormContaier>
    </SubscribeContainer>
  ) : (
    <InformMessageContainer>
      <h2>
        На вашу почту отправлено письмо со ссылкой для подтверждения подписки
      </h2>
      <Button onClick={onClick}>Ok</Button>
    </InformMessageContainer>
  );
};

export default SubscribFormContainer;
