import React, { useState, useEffect } from "react";
import styled, { keyframes } from "styled-components";

import SubscribeFormContainer from "./SubscribeFormContainer/";

const shake = keyframes`
  from,
  to {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }

  10%,
  30%,
  50%,
  70%,
  90% {
    -webkit-transform: translate3d(-10px, 0, 0);
    transform: translate3d(-10px, 0, 0);
  }

  20%,
  40%,
  60%,
  80% {
    -webkit-transform: translate3d(10px, 0, 0);
    transform: translate3d(10px, 0, 0);
  }
`;

const SubscribeWrap = styled.div`
  width: 100%;
  height: 1px;
  min-height: 210px;
  padding: 42px 108px 26px;
  background-color: ${({ mainPage }) => (mainPage ? "#eaeaea" : "#fff")};
  border-radius: 5px;
  animation-name: ${({ animate }) => {
    return animate ? shake : "none";
  }};
  animation-duration: 1s;
  animation-iteration-count: 1;
  @media (max-width: 767px) {
    height: 1px;
    min-height: 410px;
    padding: 18px 20px 30px;
  }
`;

const SubscribeForm = ({ mainPage }) => {
  const regMailValidator = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
  const [isDesktop, setIsDesktop] = useState(window.outerWidth >= 767);

  const oldPhVisible = isDesktop ? "Подпишитесь на рассылку +1" : "Ваш Email";
  const title = !isDesktop ? "Подпишитесь на рассылку +1" : "Ваш Email";
  const [phVisible, setPhVisible] = useState(oldPhVisible);
  const [mail, setMail] = useState("");
  const [mailValid, setMailValid] = useState(true);
  const [animate, setAnimate] = useState(false);
  const [informMessage, setInformMessage] = useState(false);
  const [closeMessage, setCloseMessage] = useState(false);

  const dataMail = mail;

  const placehoalderUnVisible = () => setPhVisible("");
  const placeholderVisible = () => setPhVisible(oldPhVisible);
  const closeInformMessage = () => setCloseMessage(true);

  useEffect(() => {
    const resizeHandler = () => setIsDesktop(window.outerWidth >= 767);
    window.addEventListener("resize", resizeHandler);
    return () => window.removeEventListener("resize", resizeHandler);
  }, []);

  useEffect(() => {
    setPhVisible(oldPhVisible);
  }, [isDesktop]);

  const validEmail = event => {
    const currentMail = event.target.value.trim();
    setMail(currentMail);
    if (currentMail.match(regMailValidator)) {
      setMailValid(true);
    }
  };

  const onSubmitForm = event => {
    event.preventDefault();
    let _mailValid = true;

    if (!mail.match(regMailValidator)) {
      setMailValid(false);
      _mailValid = false;
      setAnimate(true);
      setTimeout(() => setAnimate(false), 1000);
      return;
    }
    setMail("");
    setInformMessage(true);
    console.log("Верный", dataMail);
  };

  {
    return !closeMessage ? (
      <SubscribeWrap
        mailValid={mailValid}
        animate={animate}
        mainPage={mainPage}
      >
        <SubscribeFormContainer
          placeholder={phVisible}
          title={title}
          onFocus={placehoalderUnVisible}
          onBlur={placeholderVisible}
          onChanghe={validEmail}
          onSubmit={onSubmitForm}
          value={mail}
          mailValid={mailValid}
          informMessage={informMessage}
          onClick={closeInformMessage}
        />
      </SubscribeWrap>
    ) : null;
  }
};

export default SubscribeForm;
