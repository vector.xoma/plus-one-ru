import React from "react";
import Quote from "./components/Quote";
import Post from "./components/Post";
import News from "./components/News";
import FactDay from "./components/FactDay";
import Group from "./components/Group";
import SubscribeForm from "./components/SubscribeForm";

const Block = ({ size, url, type, data, style }) => {
  switch (type) {
    case "post": {
      return <Post size={size} url={url} data={data} style={style} />;
    }
    case "citate": {
      return <Quote size={size} url={url} data={data} style={style} />;
    }

    case "factday": {
      return <FactDay size={size} url={url} data={data} style={style} />;
    }
    case "subscribe": {
      return <SubscribeForm mainPage></SubscribeForm>;
    }

    case "news": {
      return <News size={size} url={url} data={data} style={style} />;
    }
  }
  return `block = ${type}`;
};

export default Block;
