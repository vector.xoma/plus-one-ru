import React from "react";
import styled from "styled-components";
import _get from "lodash/get";

const CountersContainer = styled.div`
  width: 100%;
`;

const CountersTitle = styled.h1`
  font-size: 68px;
  color: #fff;
  font-family: "MullerBold", sans-serif;
  margin-bottom: 50px;
  margin-top: 65px;
  @media (max-width: 767px) {
    font-size: 32px;
    padding: 0 20px;
  }
`;

const CounterWrap = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-wrap: wrap;
  padding-bottom: 40px;
  margin-bottom: 40px;
  border-bottom: 1px solid #252525;
  padding: 0 10px;
  :last-child {
    margin: 0;
  }
  @media (max-width: 767px) {
    padding: 10px 20px;
    margin-bottom: 0;
    align-items: center;
  }
`;

const CounterText = styled.a`
  font-size: 63px;
  text-transform: uppercase;
  font-family: "GeometricSansSerifv1";
  max-width: 900px;
  color: ${({ color }) => color};
  :hover {
    color: ${({ color }) => color};
  }
  @media (max-width: 767px) {
    max-width: 270px;
    font-size: 18px;
  }
`;
const CounterNumWrap = styled.div`
  font-family: "GeometricSansSerifv1";
`;

const CounterNum = styled.p`
  font-size: 91px;
  color: #fff;
  margin: 0;
  @media (max-width: 767px) {
    font-size: 31px;
  }
`;

const CounterDescr = styled.p`
  font-size: 19px;
  font-family: "MullerRegular";
  text-transform: uppercase;
  color: #fff;
  margin: 0;
  text-align: right;
  @media (max-width: 767px) {
    font-size: 7px;
  }
`;
const toStr = ({ r, g, b }) =>
  `rgb(${parseInt(r)}, ${parseInt(g)}, ${parseInt(b)})`;

const CountersPage = ({ data = [], name = "", rubric }) => {
  const quantity = data.length;
  const greenGradient = [24, 214, 0, quantity, 191, 244, 199];
  const yellowGradient = [250, 214, 0, quantity, 250, 245, 215];
  const blueGradient = [0, 186, 233, quantity, 201, 238, 247];
  const grayGradient = [143, 143, 143, quantity, 54, 54, 54];
  const colorSet = {
    ecology: greenGradient,
    society: yellowGradient,
    economy: blueGradient,
    platform: grayGradient
  };

  const colorChange = (r, g, b, q, rEnd, gEnd, bEnd, gray = false) => {
    const rangeColorR = (rEnd - r) / (q - 1);
    const rangeColorG = (gEnd - g) / (q - 1);
    let rangeColorB = (bEnd - b) / (q - 1);
    if (gray) {
      rangeColorB = ((bEnd - b) / (q - 1)) * -1;
    }
    let colors = [];

    for (let i = 0; i < q; i++) {
      colors.push({
        r: r + rangeColorR * i,
        g: g + rangeColorG * i,
        b: b + rangeColorB * i
      });
    }
    return colors;
  };

  let colors = colorSet[rubric]
    ? colorChange(...colorSet[rubric])
    : colorChange(...greenGradient);

  return (
    <CountersContainer>
      <CountersTitle>{name}</CountersTitle>
      {data.map((count, index) => {
        return (
          <CounterWrap key={index}>
            <CounterText color={toStr(colors[index])}>{count.name}</CounterText>
            <CounterNumWrap>
              <CounterNum>{count.total}</CounterNum>
              <CounterDescr>{_get(count, "countPosts.text")}</CounterDescr>
            </CounterNumWrap>
          </CounterWrap>
        );
      })}
    </CountersContainer>
  );
};

export default CountersPage;
