import React from "react";
import Block from "../Block";
import Masonry from "react-masonry-component";
import { Container } from "components/Grid/Grid";
import { GridItem1_3 } from "components/Grid/Grid";
import styled from "styled-components";

const stylePost = {
  container: {
    minHeight: "505px",
    marginBottom: "22px",
    maxWidth: "365px",
    justifyContent: "flex-end"
  },
  textWrap: { margin: 0, textAlign: "left" },
  textTitle: {
    fontSize: "30px",
    fontFamily: "'MullerBold', sans-serif",
    lineHeight: "1.2",
    textAlign: "left"
  },
  textSubTitle: {
    textAlign: "left",
    minHeight: "initial",
    zIndex: "6",
    postition: "relative",
    fontSize: "19px",
    lineHeight: "1.26"
  }
};

const styleQuote = {
  container: {
    minHeight: "320px",
    margin: "0",
    marginBottom: "22px",
    maxWidth: "365px"
  },
  textTitle: {
    fontSize: "27px",
    textAlign: "left",
    lineHeight: "1.07"
  }
};

const styleFact = {
  container: {
    maxWidth: "365px",
    minHeight: "320px",
    height: "auto",
    marginBottom: "22px"
  },
  textTitle: {
    fontSize: "36px",
    lineHeight: 1.06
  }
};

const MasonryPage = ({ blocks }) => {
  // TODO: БЭК ДОЛЖЕН ОТДАВАТЬ ТОЛЬКО МАССИВЫ!

  const styleConfig = typePost => {
    switch (typePost) {
      case "post":
        return stylePost;
      case "citate":
        return styleQuote;
      case "factday":
        return styleFact;
    }
  };

  return (
    <Masonry options={{ gutter: 22 }}>
      {blocks &&
        blocks.length > 0 &&
        blocks.map(({ id, block, url, typePost, ...other }) => (
          <Block
            key={id}
            size={block}
            url={url}
            type={typePost}
            data={other}
            style={styleConfig(typePost)}
          />
        ))}
    </Masonry>
  );
};

export default MasonryPage;
