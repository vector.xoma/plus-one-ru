import React from "react";
import _get from "lodash/get";

import Block from "../Block";
import { Container } from "components/Grid/Grid";
import LongRow from "components/LongRow";
import Group from "containers/BlocksViewer/components/Block/components/Group";

const RowBlock = ({ content, rowType }) => {
  const exeptions = ["group", "ticker"];
  return (
    <Container>
      {rowType && rowType === "group" && (
        <Group data={_get(content, "[0].postList")} rowType={rowType}></Group>
      )}

      {rowType && rowType === "ticker" && (
        <LongRow data={content && content.length && content[0]} />
      )}

      {content &&
        content.length &&
        !exeptions.includes(rowType) &&
        content.map(({ id, block, url, typePost, ...other }) => (
          <Block key={id} size={block} url={url} type={typePost} data={other} />
        ))}
    </Container>
  );
};

export default RowBlock;
