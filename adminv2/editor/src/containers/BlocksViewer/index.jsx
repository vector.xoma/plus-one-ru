import React from "react";

import RowBlock from "./components/RowBlock";
import MasonryPage from "./components/MasonryPage";

import styled from "styled-components";
import mock from "./mock";
import { counters } from "./mock";

const BlocksViewerContainer = styled.div`
  color: #fff;
  margin: 0 auto;
  max-width: 1140px;
  @media (max-width: 767px) {
    div {
      /*:nth-child(2) {
        margin: 20px auto;
      } */
      :last-child {
        margin-bottom: 0;
      }
    }
  }
`;

const BlocksViewer = ({
  rowsOfBlocks = [],
  blockList = [],
  masonry = false
}) => {
  return (
    <BlocksViewerContainer>
      {masonry ? (
        <MasonryPage blocks={blockList} />
      ) : (
        rowsOfBlocks.map((rb, i) => (
          <RowBlock key={i} content={rb.rowContent} rowType={rb.rowType} />
        ))
      )}
      {/* {rowsOfBlocks.map((rb, i) => (
        <RowBlock key={i} content={rb.rowContent} rowType={rb.rowType} />
      ))} */}
      {/* <MasonryPage blocks={mock} /> */}
      {/* <CountersPage counters={counters} /> */}
      {/* <NewsSlider data={rowsOfBlocks} /> */}
    </BlocksViewerContainer>
  );
};

export default BlocksViewer;
