const masonryBlocks = [
  {
    id: 4494211,
    block: "1_1",
    url: "https://dev.plus-one.ru/blog/society/chmyaya",
    name:
      "Нот грэйт, нот тэррибл нот тэррибл нот тэррибл нот тэррибл нот тэррибл нот тэррибл нот тэррибл нот тэррибл нот тэррибл",
    header: "Подзаг: Как программа & развиваться провинциальным школам",
    date: "4 месяца назад",
    image: "https://dev.plus-one.ru/files/blogposts/4491-1_1.jpg",
    iconCode: "society",
    iconName: "Общество",
    format: "picture",
    postFormat: {
      backgroundColor: "#ff4e98",
      fontColor: "#000",
      frontClass: "bgcPink"
    },
    font: "",
    writersEnabled: "1",
    typePost: "post"
  },
  {
    id: 452321323,
    block: "1_1",
    url:
      "https://dev.plus-one.ru/blog/ecology/udivitelnyy-fakt-s-predlogom-i-kavychkami-",
    name:
      "Удивительный факт длиной в&nbsp;100 символов&nbsp;&#151; с&nbsp;тире &laquo;и&nbsp;&#132;кавычками&#147; в&nbsp;кавычках&raquo;, а&nbsp;также знаком &copy;&nbsp;123456",
    header: "",
    date: "3 месяца назад",
    image: null,
    iconCode: "ecology",
    iconName: "Экология",
    format: "pink",
    postFormat: {
      backgroundColor: "#ff4e98",
      fontColor: "#000",
      frontClass: "bgcPink"
    },
    font: "",
    writersEnabled: "1",
    typePost: "factday"
  },
  {
    id: 44191,
    block: "1_2",
    url: "https://dev.plus-one.ru/blog/society/chmyaya",
    name: "Нот грэйт, нот тэррибл",
    header: "Подзаг: Как программа &laquo; провинциальным школам",
    date: "4 месяца назад",
    image: "https://dev.plus-one.ru/files/blogposts/4491-1_2.jpg",
    iconCode: "society",
    iconName: "Общество",
    format: "picture",
    postFormat: {
      backgroundColor: "#ff4e98",
      fontColor: "#000",
      frontClass: "bgcPink"
    },
    font: "",
    writersEnabled: "1",
    typePost: "post"
  },
  {
    id: 44391,
    block: "1_3",
    url: "https://dev.plus-one.ru/blog/society/chmyaya",
    name: "Нот грэйт, нот тэррибл",
    header: "Подзаг: Как программа &laquo;школам",
    date: "4 месяца назад",
    image: "https://dev.plus-one.ru/files/blogposts/4491-1_3.jpg",
    iconCode: "society",
    iconName: "Общество",
    format: "picture",
    postFormat: {
      backgroundColor: "#ff4e98",
      fontColor: "#000",
      frontClass: "bgcPink"
    },
    font: "",
    writersEnabled: "1",
    typePost: "post"
  },
  {
    id: 457321,
    block: "1_1",
    url: "https://dev.plus-one.ru/blog/ecology/citatenew",
    name: "Текст цитаты",
    header: "хедер цитаты бла бла бла блабал балалба балла",
    date: "Неделю назад",
    image: "https://dev.plus-one.ru/files/blogposts/4571-1_1.jpg",
    iconCode: "ecology",
    iconName: "Экология",
    format: "",
    postFormat: { fontColor: "#fff", backgroundColor: null },
    font: "",
    writersEnabled: "1",
    typePost: "citate"
  },
  {
    id: 4572321,
    block: "1_2",
    url: "https://dev.plus-one.ru/blog/ecology/citatenew",
    name:
      "Текст цл нот тэррибл л нот тэррибл нот тэррибл нот тэррибл нот тэррибл нот тэррибл нотнот тэррибл нот тэррибл нот тэррибл нот тэррибл нотитаты",
    header:
      "хедер цитаты бл нот тэррибл нот тэррибл нот тэррибл нот тэррибл нот тэрри бл нотла бла бла блабал балалба балла",
    date: "Неделю назад",
    image: "https://dev.plus-one.ru/files/blogposts/4571-1_2.jpg",
    iconCode: "ecology",
    iconName: "Экология",
    format: "",
    postFormat: { fontColor: "#fff", backgroundColor: null },
    font: "",
    writersEnabled: "1",
    typePost: "citate"
  },
  {
    id: 4532171,
    block: "1_3",
    url: "https://dev.plus-one.ru/blog/ecology/citatenew",
    name:
      "Тексибл нот тэррибл нот тэррибл нот тэррибл нот тэррибл нот т тэррибл нот тэррибл нот тэррибл нот тэррибл нот тэррибл нот тэррибл нот тэррибл нот тэррибл нот т тэррибл нот тэррибл нот тэрриб титаты",
    header: "хедер цитаты бла бла бла блабал балалба балла",
    date: "Неделю назад",
    image: "https://dev.plus-one.ru/files/blogposts/4571-1_3.jpg",
    iconCode: "ecology",
    iconName: "Экология",
    format: "",
    postFormat: { fontColor: "#fff", backgroundColor: null },
    font: "",
    writersEnabled: "1",
    typePost: "citate"
  },
  {
    id: 4573221,
    block: "1_1",
    url: "https://dev.plus-one.ru/blog/ecology/citatenew",
    name: "Текст цитаты",
    header: "хедер цитаты бла бла бла блабал балалба балла",
    date: "Неделю назад",
    image: "https://dev.plus-one.ru/files/blogposts/4571-1_1.jpg",
    iconCode: "ecology",
    iconName: "Экология",
    format: "",
    postFormat: { fontColor: "#fff", backgroundColor: null },
    font: "",
    writersEnabled: "1",
    typePost: "citate"
  },
  {
    id: 45723321,
    block: "1_2",
    url: "https://dev.plus-one.ru/blog/ecology/citatenew",
    name:
      "Текст цл от тэррибл нот тэррибл нотнот тэррибл нот тэррибл нот тэррибл нот тэррибл нотитаты",
    header: "хедер цитаты бл нот тэр",
    image: "https://dev.plus-one.ru/files/blogposts/4571-1_2.jpg",
    iconCode: "ecology",
    iconName: "Экология",
    format: "",
    postFormat: { fontColor: "#fff", backgroundColor: null },
    font: "",
    writersEnabled: "1",
    typePost: "citate"
  },
  {
    id: 4523,
    block: "1_1",
    url:
      "https://dev.plus-one.ru/blog/ecology/udivitelnyy-fakt-s-predlogom-i-kavychkami-",
    name:
      "Удивительный факт длиной в&nbsp;100 символов&nbsp;&#151; с&nbsp;тире &laquo;и&nbsp;&#132;кавычками&#147; в&nbsp;кавычках&raquo;, а&nbsp;также знаком &copy;&nbsp;123456",
    header: "",
    date: "3 месяца назад",
    image: null,
    iconCode: "ecology",
    iconName: "Экология",
    format: "pink",
    postFormat: {
      backgroundColor: "#ff4e98",
      fontColor: "#000",
      frontClass: "bgcPink"
    },
    font: "",
    writersEnabled: "1",
    typePost: "factday"
  },
  {
    id: 4521,
    block: "1_2",
    url: "https://dev.plus-one.ru/blog/ecology/url-testtt",
    name: "факт для теста",
    header: "",
    date: "3 месяца назад",
    image: null,
    iconCode: "ecology",
    iconName: "Экология",
    format: "pink",
    postFormat: {
      backgroundColor: "#ff4e98",
      fontColor: "#000",
      frontClass: "bgcPink"
    },
    font: "",
    writersEnabled: "1",
    typePost: "factday"
  },
  {
    id: 45213,
    block: "1_3",
    url:
      "https://dev.plus-one.ru/blog/ecology/udivitelnyy-fakt-s-predlogom-i-kavychkami-",
    name:
      "Удивительный факт длиной в&nbsp;100 символов&nbsp;&#151; с&nbsp;тире &laquo;и&nbsp;&#132;кавычками&#147; в&nbsp;кавычках&raquo;, а&nbsp;также знаком &copy;&nbsp;123456",
    header: "",
    date: "3 месяца назад",
    image: null,
    iconCode: "ecology",
    iconName: "Экология",
    format: "pink",
    postFormat: {
      backgroundColor: "#ff4e98",
      fontColor: "#000",
      frontClass: "bgcPink"
    },
    font: "",
    writersEnabled: "1",
    typePost: "factday"
  },

  {
    id: 45332171,
    block: "1_3",
    url: "https://dev.plus-one.ru/blog/ecology/citatenew",
    name:
      "Тексибл нот тэррибл нот тэррибл нот т тэррибл нот тэррибл нот тэрриб титаты",
    header: "хедер цитаты бла бла бла блабал балалба балла",
    date: "Неделю назад",
    image: "https://dev.plus-one.ru/files/blogposts/4571-1_3.jpg",
    iconCode: "ecology",
    iconName: "Экология",
    format: "",
    postFormat: { fontColor: "#fff", backgroundColor: null },
    font: "",
    writersEnabled: "1",
    typePost: "citate"
  }
];

export const counters = {
  name: "Тэги",
  data: [
    {
      type: "tags",
      title: "СНИЖЕНИЕ ВОЗДЕЙСТВИЯ НА ОКРУЖАЮЩУЮ СРЕДУ",
      amount: 2
    },
    {
      type: "tags",
      title: "ПРОТИВОДЕЙСТВИЕ ИЗМЕНЕНИЮ КЛИМАТА",
      amount: 32
    },
    {
      type: "tags",
      title: "ЭКОЛОГИЧЕСКАЯ БЕЗОПАСНОСТЬ",
      amount: 110
    },
    {
      type: "tags",
      title: "ОТХОДЫ",
      amount: 32
    }
  ]
};
export default masonryBlocks;
