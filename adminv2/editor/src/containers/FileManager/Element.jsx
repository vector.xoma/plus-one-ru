import React from "react";
import styled from "styled-components";

const FolderImg = styled.img.attrs({
  src: require("img/folder.png"),
  alt: "FolderImg"
})`
  width: 100%;
  height: 100%;
  object-fit: cover;
  display: block;
  min-height: 94px;
`;

const FileImg = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
  display: block;
  min-height: 94px;
`;
const ElementWrap = styled.div`
  margin: 5px;
  background-color: #dddddd;
  width: 125px;
  min-height: 100px;
  height: 100%;
  cursor: pointer;
`;

const ElementName = styled.div`
  height: 25px;
  font-size: 12px;
  background-color: #fff;
  color: #000;
  text-align: center;
`;

const Element = ({ onClick, type, fullFileName, fileName }) => {
  return (
    <ElementWrap onClick={onClick}>
      {type === "file" ? (
        <FileImg src={`https://dev.plus-one.ru/${fullFileName}`} />
      ) : (
        <FolderImg />
      )}

      <ElementName>{fileName}</ElementName>
    </ElementWrap>
  );
};

export default Element;
