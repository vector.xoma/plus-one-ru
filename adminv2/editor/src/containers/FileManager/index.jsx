import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { Modal, Icon } from "antd";
import { Files } from "connectors/query/Files";
import Element from "./Element";

const FileManagerWrap = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

const FileManager = ({ setPicUrl, hideModal, visible }) => {
  const [position, setPosition] = useState([]);
  const [currentFiles, setCurrentFiles] = useState([]);
  // console.log(currentFiles);

  useEffect(() => {
    async function getGalleries() {
      const result = await Files.getFolder({ dir: position.join("/") });
      if (result) setCurrentFiles(result);
    }
    getGalleries();
  }, [position]);

  const backDirectory = () => {
    const newPos = [...position];
    newPos.pop();
    setPosition(newPos);
  };

  const clickEvent = element => {
    switch (element.type) {
      case "folder": {
        setPosition([...position, element.fileName]);
        break;
      }
      case "file": {
        setPicUrl(element.fullFileName.slice(3));
        hideModal();
        break;
      }
    }
  };

  return (
    <Modal
      visible={visible}
      onCancel={hideModal}
      footer={null}
      zIndex={1100}
      bodyStyle={{
        background: "#ececec",
        padding: "20px 30px"
      }}
      centered
      width={"80%"}
    >
      <FileManagerWrap>
        {position.length > 0 && (
          <button onClick={backDirectory}>
            <Icon type="arrow-left" />
          </button>
        )}
        {currentFiles.map((f, i) => (
          <Element key={i} onClick={() => clickEvent(f)} {...f} />
        ))}
      </FileManagerWrap>
    </Modal>
  );
};

export default FileManager;
