import React, { useState, useEffect } from "react";
import { Button, Input } from "antd";

import { useComponents } from "contexts/componentsContext";

import ComponentSelect from "components/ComponentSelect";
import "./styles.scss";

const Main = ({ save, remove, setData }) => {
  const [compName, setCompName] = useState("");
  const [selectedComp, setSelectedComp] = useState(0);
  const { components } = useComponents();

  useEffect(() => {
    const newComp = components && components.find(c => c.id === selectedComp);
    const newData = newComp && newComp.data;
    if (selectedComp === 0) {
      setData([]);
      setCompName("");
    }
    if (selectedComp && newComp) {
      setData(newData);
      setCompName(newComp.name);
    }
  }, [selectedComp]);

  return (
    <div className="header">
      <div className="header__componentsControls">
        <div className="buttonContainer">
          <ComponentSelect
            value={selectedComp}
            onChange={e => {
              setSelectedComp(e.target.value);
            }}
            showEmpty
          />
        </div>

        <div className="buttonContainer">
          <Input
            placeholder="Имя статьи"
            value={compName || ""}
            onChange={e => setCompName(e.target.value)}
          />
        </div>

        <div className="buttonContainer">
          <Button
            onClick={() => save(compName, selectedComp, setSelectedComp)}
            className="saveButton"
          >
            Сохранить
          </Button>
        </div>

        <div className="buttonContainer">
          <Button
            type="secondary"
            onClick={() => remove(selectedComp, setCompName, setSelectedComp)}
            disabled={!selectedComp}
          >
            Удалить
          </Button>
        </div>
      </div>
    </div>
  );
};

export default Main;
