import React from "react";
import { Button } from "antd";

import "./styles.scss";

const HeaderOfContents = ({ showAll }) => (
  <div className="header">
    <div className="header__componentsControls">
      <div className="buttonContainer">
        <Button type="primary" onClick={() => showAll()}>
          Создать статью
        </Button>
      </div>
    </div>
  </div>
);

export default HeaderOfContents;
