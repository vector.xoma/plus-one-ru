import React, { useState } from "react";
import { Button, Modal } from "antd";

import Text from "components/Text";
import Show from "pages/Show";
// import "./styles.scss";
import styled from "styled-components";

const InputDataWrap = styled.div`
  padding: 5px;
  width: 50%;
  background: cornsilk;
`;

const InputDataContent = styled.div`
  display: flex;
  flex-direction: column;

  padding: 10px;
`;
const InputData = ({ data, updateField, areaName }) => {
  const update = fieldData => updateField(fieldData);
  const [visible, setVisible] = useState(false);

  return (
    <InputDataWrap>
      Редактор:
      <Button onClick={() => setVisible(true)}>Полный экран</Button>
      <Modal
        visible={visible}
        onCancel={() => setVisible(false)}
        footer={null}
        width={"100vw"}
        bodyStyle={{ padding: 0 }}
        // centered
      >
        <Show data={data} />
      </Modal>
      <InputDataContent>
        {data && (
          <Text.Editor data={data} updateField={update} areaName={areaName} />
        )}
      </InputDataContent>
    </InputDataWrap>
  );
};

export default InputData;
