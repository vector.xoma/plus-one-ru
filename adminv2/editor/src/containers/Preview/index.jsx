import React from "react";
import styled from "styled-components";

import Text from "components/Text";

const PreviewContainer = styled.div`
  background: #eaeaea;
  height: 100%;
`;
const Preview = ({ data, showTitle }) => (
  <PreviewContainer>
    {showTitle && <div style={{ marginBottom: "10px" }}>Предпросмотр:</div>}
    <div>
      <Text data={data} />
    </div>
  </PreviewContainer>
);

export default Preview;
