import React, { useContext, useState, createContext, useEffect } from "react";
import { Menu } from "connectors/query/Menu";

const MenuContetx = createContext({ data: [] });

function MenuProvider({ children }) {
  const [data, setData] = useState([]);

  useEffect(() => {
    async function getMenuData() {
      const result = await Menu.getList();
      setData(result);
    }
    getMenuData();
  }, []);

  return (
    <MenuContetx.Provider value={{ data, setData }}>
      {children}
    </MenuContetx.Provider>
  );
}

const useMenuData = () => {
  const context = useContext(MenuContetx);
  return context;
};

export { MenuProvider, useMenuData };
