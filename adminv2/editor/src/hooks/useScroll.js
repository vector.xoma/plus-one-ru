import { useEffect } from "react";

const useScroll = action => {
  useEffect(() => {
    document.addEventListener("scroll", action);
    return () => document.removeEventListener("scroll", action);
  }, []);
};

export default useScroll;
