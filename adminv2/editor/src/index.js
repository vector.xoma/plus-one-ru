import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import "antd/dist/antd.min.css";
import AppEditor from "./AppEditor";
import AppUserFront from "./AppUserFront";
import * as serviceWorker from "./serviceWorker";

const root = document.getElementById("root");
root &&
  ReactDOM.render(<AppEditor globalVar="TestVar" areaName="name" />, root);

// основная страница пользователя
// root &&
//   ReactDOM.render(<AppUserFront globalVar="TestVar" areaName="name" />, root);

// О проекте
const about = document.getElementById("reactEditor_about");
about &&
  ReactDOM.render(<AppEditor globalVar="EditorAbout" areaName="body" />, about);

// Записи в блогах
const blogpost_header = document.getElementById("reactEditor_blogpost_header");
blogpost_header &&
  ReactDOM.render(
    <AppEditor globalVar="EditorBlogpostHeader" areaName="header" />,
    blogpost_header
  );

const blogpost_lead = document.getElementById("reactEditor_blogpost_lead");
blogpost_lead &&
  ReactDOM.render(
    <AppEditor globalVar="EditorBlogpostLead" areaName="lead" />,
    blogpost_lead
  );

const blogpost_body = document.getElementById("reactEditor_blogpost_body");
blogpost_body &&
  ReactDOM.render(
    <AppEditor globalVar="EditorBlogpostBody" areaName="body" />,
    blogpost_body
  );

serviceWorker.unregister();
