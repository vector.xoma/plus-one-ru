import React, { useState, useEffect } from "react";
import _get from "lodash/get";

import BlocksViewer from "containers/BlocksViewer";
import BlackPagesOuter from "containers/BlackPagesOuter";
import { Blocks } from "connectors/query/Blocks";

const NotFoundPage = () => {
  return <BlackPagesOuter>404</BlackPagesOuter>;
};

export default NotFoundPage;
