import React, { useState, useEffect } from "react";
import InputData from "containers/InputData";
import Preview from "containers/Preview";

// import FileManager from "containers/FileManager";

// import "./styles.scss";
import styled from "styled-components";

const EditorWorkPlace = styled.div`
  height: 100%;
  display: flex;
`;

const EditorPreviewPlace = styled.div`
  width: 50%;
  padding: 5px;
  background-color: #eaeaea;
`;

const Editor = ({ updateData, serverData = null, areaName }) => {
  const [data, setData] = useState(serverData || {});

  // костыль для отображения данных на второй странице.
  // TODO: убрать по готовности бэка
  useEffect(() => {
    updateData(data);
  }, [data, updateData]);

  const updateField = fieldData => setData(fieldData);

  // return <FileManager />;

  return (
    <div className="editor_wrapper">
      <EditorWorkPlace>
        <InputData data={data} updateField={updateField} areaName={areaName} />
        <EditorPreviewPlace className="editor_preview-place">
          <Preview data={data} showTitle />
        </EditorPreviewPlace>
      </EditorWorkPlace>
    </div>
  );
};

export default Editor;
