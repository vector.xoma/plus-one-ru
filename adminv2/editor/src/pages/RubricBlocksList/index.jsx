import React, { useState, useEffect } from "react";
import _get from "lodash/get";

import BlocksViewer from "containers/BlocksViewer";
import { Blocks } from "connectors/query/Blocks";
import BlackPagesOuter from "containers/BlackPagesOuter";

const RubricBlocksList = ({ rubric }) => {
  const [data, setData] = useState([]);

  useEffect(() => {
    Blocks.getList(rubric).then(res => {
      setData(_get(res, "items"));
    });
  }, [rubric]);

  return (
    <BlackPagesOuter>
      <BlocksViewer blockList={data} masonry />
    </BlackPagesOuter>
  );
};

export default RubricBlocksList;
