import React, { useState, useEffect } from "react";
import _get from "lodash/get";

import BlocksViewer from "containers/BlocksViewer";
import BlackPagesOuter from "containers/BlackPagesOuter";
import { Blocks } from "connectors/query/Blocks";

const TagBlocksList = () => {
  const [data, setData] = useState([]);
  const searchRow = window.location.search.slice(1);

  // useEffect(() => {
  //   Blocks.getList(rubric).then(res => {
  //     setData(_get(res, "items"));
  //   });
  // }, [rubric]);

  return (
    <BlackPagesOuter>
      {/* <BlocksViewer blockList={data} masonry /> */}
    </BlackPagesOuter>
  );
};

export default TagBlocksList;
