import React, { useState, useEffect } from "react";

import Preview from "containers/Preview";
import BlocksViewer from "containers/BlocksViewer";
import DropDown from "components/DropDown";
import HeaderComponent from "components/HeaderComponent/HeaderComponent";
import HeaderBottom from "components/HeaderBottom";
import Informer from "components/Informer";
import HeaderFixed from "components/HeaderFixed";
import useDesktop from "hooks/useDesktop";
import useScroll from "hooks/useScroll";

const Show = ({ data }) => {
  const [visible, setVisible] = useState(false);
  const isDesktop = useDesktop();
  const [headerRef, setHeaderRef] = useState(null);
  const [scrollY, setScrollY] = useState(window.scrollY);
  const [fixedHeader, setFixedHeader] = useState(false);

  const scrollAction = () => {
    setScrollY(window.scrollY);
  };

  useScroll(scrollAction);

  const isHeaderFixedVisible = () => {
    if (headerRef) {
      return scrollY >= parseInt(getComputedStyle(headerRef).height);
    }
  };

  useEffect(() => {
    setFixedHeader(isHeaderFixedVisible());
  }, [scrollY]);

  return (
    <div
      style={{
        height: "100%",
        background: "#212121",
        minHeight: "100vh"
      }}
    >
      <HeaderFixed setVisible={setVisible} visible={fixedHeader} />
      <DropDown visible={visible} setVisible={setVisible} />
      <HeaderComponent />
      <HeaderBottom
        visible={visible}
        setVisible={setVisible}
        setHeaderRef={setHeaderRef}
      />
      {isDesktop && <Informer />}
      {data && data.rowsOfBlocks ? (
        <BlocksViewer rowsOfBlocks={data.rowsOfBlocks} />
      ) : (
        <Preview data={data} />
      )}
    </div>
  );
};

export default Show;
