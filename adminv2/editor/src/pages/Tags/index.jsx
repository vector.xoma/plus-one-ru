import React, { useState, useEffect } from "react";
import _get from "lodash/get";

import CountersPage from "containers/BlocksViewer/components/CountersPage";
import BlackPagesOuter from "containers/BlackPagesOuter";
import { Tags } from "connectors/query/Tags";

const translate = {
  tag: "Тэги",
  leaders: "Лидеры"
};

const path = {
  tag: "tags",
  leaders: "authors"
};

const TagsPage = ({ type, rubric }) => {
  const [data, setData] = useState([]);

  useEffect(() => {
    Tags.getList(type, rubric).then(res => {
      setData(_get(res, `${path[type]}`));
    });
  }, [type, rubric]);

  return (
    <BlackPagesOuter>
      <CountersPage name={translate[type]} data={data} rubric={rubric} />
    </BlackPagesOuter>
  );
};

export default TagsPage;
