const proxy = require("http-proxy-middleware");

module.exports = function(app) {
  app.use(
    "/api",
    proxy({
      target: "https://po.ww12.ru/",
      changeOrigin: true
    })
  );
};
