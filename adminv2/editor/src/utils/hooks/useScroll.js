import { useState, useEffect } from "react";

function useScrollPosition(elementRef) {
  function getScroll() {
    const data = elementRef.current
      ? elementRef.current.getBoundingClientRect()
      : 0;
    const { bottom, top, left, right, x, y } = data;
    return { bottom, top, left, right, x, y };
  }

  const [scrollPosition, setScrollPosition] = useState(getScroll);

  useEffect(() => {
    function handleScroll() {
      setScrollPosition(getScroll());
    }

    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  }, []); // Empty array ensures that effect is only run on mount and unmount

  return scrollPosition;
}

export default useScrollPosition;
