export const clearTextSpaces = text =>
  text.replace(/\n\n/g, "\n").replace(/\s{2,}/g, " ");

export const splitBySelection = (startText = "", area = {}) => {
  const range = {
    start: startText.slice(0, area.selectionStart),
    middle: startText.slice(area.selectionStart, area.selectionEnd),
    end: startText.slice(area.selectionEnd)
  };

  return range;
};
