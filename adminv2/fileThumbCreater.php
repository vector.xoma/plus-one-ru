<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

createThumb();

function createThumb()
{
    $pathToImage = '../files/image/';
    $dir = opendir($pathToImage);

    while ($fileName = readdir($dir)) {
        if (!is_dir($fileName) && $fileName != '.empty') {

            if (file_exists($pathToImage . $fileName)){

                if (!fnmatch("*_thumb.*", $fileName)) {
                    $fileInfo = pathinfo($pathToImage . $fileName);

                    $thumbFileName = $fileInfo['filename'] . "_thumb." . $fileInfo['extension'];

                    $sourceFile = $pathToImage . $fileName;
                    $thumbFile = $pathToImage . $thumbFileName;

                    if (make_thumb($sourceFile, $thumbFile) === true){
                        echo "thumbnail created for image: " . $fileName . "\n";
                    }
                }
            }
        }
    }

}

?>


<?php

function make_thumb($src, $dest, $desired_width = 150) {

    $imgType = mime_content_type($src);

    switch ($imgType) {
        case 'image/png':
            $source_image = imagecreatefrompng($src);
            break;
        case 'image/jpeg':
            $source_image = imagecreatefromjpeg($src);
            break;
        case 'image/gif':
            $source_image = imagecreatefromgif($src);
            break;
        default:
            $source_image = imagecreatefromjpeg($src);
    }

    $width = imagesx($source_image);
    $height = imagesy($source_image);

    /* find the "desired height" of this thumbnail, relative to the desired width  */
    $desired_height = floor($height * ($desired_width / $width));

    /* create a new, "virtual" image */
    $virtual_image = imagecreatetruecolor($desired_width, $desired_height);

    /* copy source image at a resized size */
    imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);

    /* create the physical thumbnail image to its destination */
    if (imagejpeg($virtual_image, $dest)){
        return true;
    }
    else{
        return false;
    }
}


function addPhoto(){
    $result = false;

    $uploadDir = '../files/image/';

    if (isset($_FILES['uploadfile']) && !empty($_FILES['uploadfile']['tmp_name'])) {
        if (!move_uploaded_file($_FILES['uploadfile']['tmp_name'], $uploadDir . $_FILES['uploadfile']['name'])) {
            $result = 'Ошибка при загрузке файла ' . $_FILES['uploadfile']['name'];
        }
        else {

            $fileInfo = pathinfo($uploadDir . $_FILES['uploadfile']['name']);

            $thumbFileName = $fileInfo['filename'] . "_thumb." . $fileInfo['extension'];

            $sourceFile = $uploadDir . $_FILES['uploadfile']['name'];
            $thumbFile = $uploadDir . $thumbFileName;

            make_thumb($sourceFile, $thumbFile);

            $result = 'Файл ' . $_FILES['uploadfile']['name'] . ' успешно загружен';

        }
    }

    echo $result;
}

function checkKeyInArray($keyNeedle, $fileListArray){

    static $key;

    $key = $keyNeedle;

    if (array_key_exists($keyNeedle, $fileListArray)){
        $keyNeedle++;
        checkKeyInArray($keyNeedle, $fileListArray);
    }
    return $key;
}

?>р к которому нужен доступ,