<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
?>
<head>
    <meta charset="utf-8" />

    <style>
        .fichero{
            width: 150px;
            /*height: 180px;*/
            float: left;
            margin: 0 15px 15px 0;
            border: 1px solid #dadada;
            cursor: pointer;
        }
        .fichero .img{display: block; width: 150px; /*height: 120px;*/ text-align: center; vertical-align: middle}
        .fichero .img img{width: 150px;}
        .fichero p {font-size: 12px; font-family: 'tinymce', Arial; display: inline-block; margin: 4px;}

        .upload_block {position: fixed;top:0;left:0; width:100%; padding: 10px 20px;background: #dadada}
        .upload_block form{margin:0; padding: 0;}
        .file_list {margin-top: 120px;}
        .sorting-options{margin-top: 10px;font-size: 12px; font-family: 'tinymce', Arial;}
        .sorting-options span.sort-group {margin: 0 20px 0 0; display: inline-block}

        pre{ position: fixed; top: 100px; display: inline-block; clear: both;}
    </style>


    <script src="bootstrap/bower_components/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" language="javascript">
        $(document).on("click","div.fichero",function(){
            item_url = $(this).data("src");
            var args = top.tinymce.activeEditor.windowManager.getParams();
            win = (args.window);
            input = (args.input);
            win.document.getElementById(input).value = item_url;
            top.tinymce.activeEditor.windowManager.close();
        });
    </script>
</head>

<div class="upload_block">
    <form name="form" method="post" enctype="multipart/form-data">
        <input type="file" name="uploadfile" />
        <input type="submit" name="uploadbutton" value="загрузить" />
    </form>

    <div class="sorting-options">Сортировка:</div>
    <div class="sorting-options">
        <span class="sort-group">
            По имени (<a href="?modesort=name&typesort=asc">по убыванию</a> | <a href="?modesort=name&typesort=desc">по возрастанию</a>)
        </span>
        <span class="sort-group">
            По дате (<a href="?modesort=date&typesort=asc">по убыванию</a> | <a href="?modesort=date&typesort=desc">по возрастанию</a>)
        </span>
    </div>
</div>


<?php
if ($_POST){
    echo "<p>" . addPhoto() . "</p>";
}
?>


<div class="file_list">
    <?php

    $pathToImage = '../files/image/';
    $dir = opendir($pathToImage);

    $typeSort = 'desc';
    $modeSort = 'name';

    if (isset($_GET['modesort'])){
        $modeSort = $_GET['modesort'];
    }

    if (isset($_GET['typesort'])){
        $typeSort = $_GET['typesort'];
    }

    // для сортировки пишем файлы в массив.
    // в зависимости от типа сортировки (по имени или по дате) ключами массива будут даты либо имена файлов
    while ($fileName = readdir($dir)) {
        if (!is_dir($fileName) && $fileName != '.empty') {

            if (file_exists($pathToImage . $fileName)){

                if (fnmatch("*_thumb.*", $fileName)) {
                    if ($modeSort == 'name'){
                        $fileList[$fileName] = $fileName;
                    }
                    elseif ($modeSort == 'date'){
                        $fileStat = stat($pathToImage . $fileName);
                        $fileMTime = checkKeyInArray($fileStat['mtime'], $fileList);
                        $fileList[$fileMTime] = $fileName;
                    }
                    else{
                        $fileList[$fileName] = $fileName;
                    }
                }
            }
        }
    }

    if ($typeSort == 'asc'){
        ksort($fileList);
    }
    elseif($typeSort == 'desc'){
        krsort($fileList);
    }
    else{
        ksort($fileList);
    }


    foreach ($fileList AS $fileName){

//        $fileNameToDisplay = $fileName;
//
//        if (strlen($fileNameToDisplay) > 12){
//            $fileArr = explode(".", $fileNameToDisplay);
//            $fileNameToDisplay = mb_substr($fileNameToDisplay, 0, 7, mb_internal_encoding()) . "~." . $fileArr[1];
//        }

        $sizeImage = getimagesize($pathToImage . $fileName);

        $insertPathToImage = str_replace('..', '', $pathToImage);

        $port = "";
        if ($_SERVER['HTTP_HOST'] == 'comersant.loc.ru'){
            $port = ":8098";
        }

        $insertPathToImage = 'https://' . $_SERVER['HTTP_HOST'] . $port . $insertPathToImage;

        $fileNameNoThumb = str_replace('_thumb', '', $fileName);

        echo "<div class='fichero' data-src='" . $insertPathToImage . trim($fileNameNoThumb) . "'>";
            echo "<div class='img'>";
                echo "<img src='{$insertPathToImage}{$fileName}' />";
                echo "<p>{$fileName}<br/>width: " . $sizeImage[0] . "px;<br/>height: " . $sizeImage[1] . "px; </p>";
            echo "</div>";
        echo "</div>";
    }
    ?>
</div>


<?php

function make_thumb($src, $dest, $desired_width = 150) {

    $imgType = mime_content_type($src);

    switch ($imgType) {
        case 'image/png':
            $source_image = imagecreatefrompng($src);
            break;
        case 'image/jpeg':
            $source_image = imagecreatefromjpeg($src);
            break;
        case 'image/gif':
            $source_image = imagecreatefromgif($src);
            break;
        default:
            $source_image = imagecreatefromjpeg($src);
    }

    $width = imagesx($source_image);
    $height = imagesy($source_image);

    /* find the "desired height" of this thumbnail, relative to the desired width  */
    $desired_height = floor($height * ($desired_width / $width));

    /* create a new, "virtual" image */
    $virtual_image = imagecreatetruecolor($desired_width, $desired_height);

    /* copy source image at a resized size */
    imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);

    /* create the physical thumbnail image to its destination */
    imagejpeg($virtual_image, $dest);
}


function addPhoto(){
    $result = false;

    $uploadDir = '../files/image/';

    if (isset($_FILES['uploadfile']) && !empty($_FILES['uploadfile']['tmp_name'])) {

        $newFileName = translit($_FILES['uploadfile']['name']);

        if (!move_uploaded_file($_FILES['uploadfile']['tmp_name'], $uploadDir . $newFileName)) {
            $result = 'Ошибка при загрузке файла ' . $_FILES['uploadfile']['name'];
        }
        else {

            $fileInfo = pathinfo($uploadDir . $newFileName);

            $thumbFileName = $fileInfo['filename'] . "_thumb." . $fileInfo['extension'];

            $sourceFile = $uploadDir . $newFileName;
            $thumbFile = $uploadDir . $thumbFileName;

            make_thumb($sourceFile, $thumbFile);

            $result = 'Файл ' . $newFileName . ' успешно загружен';

        }
    }

    echo $result;
}

function checkKeyInArray($keyNeedle, $fileListArray){

    static $key;

    $key = $keyNeedle;

    if (array_key_exists($keyNeedle, $fileListArray)){
        $keyNeedle++;
        checkKeyInArray($keyNeedle, $fileListArray);
    }
    return $key;
}


/**
 * транслит из кир=>лат
 *
 * @param $str
 * @return string
 */
function translit($str)
{
    $str = trim ($str);
    $tr = array(
        "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g",
        "Д"=>"d","Е"=>"e","Ё"=>"E","Ж"=>"j","З"=>"z","И"=>"i",
        "Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
        "О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
        "У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"ts","Ч"=>"ch",
        "Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"yi","Ь"=>"",
        "Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ё"=>"e","ж"=>"j",
        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
        "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
        "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
        " "=> "-", "/"=> "-", "»"=>"", "«"=>"",
        " - "=>"-", " — "=>"-", "&quot;"=>"", "!"=>"", "”"=>"",
        "“"=>"", ","=>"", "("=>"", ")"=>"", "°"=>"", "\""=>"",
        "№"=>"", "*"=>"-", "\""=>"", "'"=>"", "+"=>"", "%"=>"",
        ":"=>"", "&"=>"", "–"=>"", "`"=>""
    );
    return strtr($str,$tr);
}