<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

if ($_POST){
    echo "<p>" . addPhoto() . "</p>";
}
?>

    <head>
        <meta charset="utf-8" />
<!--        <style>-->
<!--            .fichero{-->
<!--                width: 150px;-->
<!--                /*height: 180px;*/-->
<!--                /*float: left;*/-->
<!--                margin: 0 15px 15px 0;-->
<!--                border: 1px solid #dadada;-->
<!--                cursor: pointer;-->
<!--                overflow-x: hidden;-->
<!--                display: inline-block;-->
<!--            }-->
<!--            .fichero .img{display: block; width: 150px; /*height: 120px;*/ text-align: center; vertical-align: middle}-->
<!--            .fichero .img img{width: 150px;}-->
<!--            .fichero p {font-size: 12px; font-family: 'tinymce', Arial; display: inline-block; margin: 4px;}-->
<!---->
<!--            .upload_block {position: fixed;top:0;left:0; width:100%; padding: 10px 20px;background: #dadada}-->
<!--            .upload_block form{margin:0; padding: 0;}-->
<!--            .file_list {margin-top: 120px;}-->
<!--            .sorting-options{margin-top: 10px;font-size: 12px; font-family: 'tinymce', Arial;}-->
<!--            .sorting-options span.sort-group {margin: 0 20px 0 0; display: inline-block}-->
<!---->
<!--            pre{ position: fixed; top: 100px; display: inline-block; clear: both;}-->
<!--        </style>-->

        <style>
            .fichero{
                width: 150px;
                /*height: 180px;*/
                /*float: left;*/
                display: inline-block;
                margin: 0 15px 15px 0;
                border: 1px solid #dadada;
                cursor: pointer;
                overflow-x: hidden;
            }
            .fichero .img{display: block; width: 150px; /*height: 120px;*/ text-align: center; vertical-align: middle}
            .fichero .img img{width: 150px;}
            .fichero p {font-size: 12px; font-family: 'tinymce', Arial; display: inline-block; margin: 4px; text-align: left}

            .upload_block {position: fixed;top:0;left:0; width:100%; padding: 10px 20px;background: #dadada}
            .upload_block form{margin:0; padding: 0;}
            .file_list {margin-top: 120px;}
            .sorting-options{margin-top: 10px;font-size: 12px; font-family: 'tinymce', Arial;}
            .sorting-options span.sort-group {margin: 0 20px 0 0; display: inline-block}

            pre{ position: fixed; top: 100px; display: inline-block; clear: both;}
        </style>


        <script src="bootstrap/bower_components/jquery/dist/jquery.min.js"></script>
        <script type="text/javascript" language="javascript">
            $(document).on("click","div.fichero",function(){
                item_url = $(this).data("src");
                var args = top.tinymce.activeEditor.windowManager.getParams();
                win = (args.window);
                input = (args.input);
                win.document.getElementById(input).value = item_url;
                top.tinymce.activeEditor.windowManager.close();
            });
        </script>
    </head>

    <div class="upload_block">
        <form name="form" method="post" enctype="multipart/form-data">
            <input type="file" name="uploadfile" />
            <input type="submit" name="uploadbutton" value="загрузить" />
        </form>

        <div class="sorting-options">Сортировка:</div>
        <div class="sorting-options">
            <span class="sort-group">
                По имени (<a href="?modesort=name&typesort=asc">по убыванию</a> | <a href="?modesort=name&typesort=desc">по возрастанию</a>)
            </span>
            <span class="sort-group">
            По дате (<a href="?modesort=date&typesort=asc">по убыванию</a> | <a href="?modesort=date&typesort=desc">по возрастанию</a>)
            </span>
        </div>
    </div>

<?php
require_once('FileSystem.admin.php');

// количество изображений на странице
define( 'PERPAGE', 8000);
$directory = '../files/image/';
$dir = new FileSystem($directory);
// Отфильтровываем все файлы, которые не являются изображениями
$dir->filter();

// Сортируем картинки
$dir->indexOrder();

// Общее количество изображений в директории
$totalCount = $dir->getCount();

$page = 1;
// Текущая страница
if (isset($_GET['page'])){
    $page = $_GET['page'];
}

$numPages = ceil($totalCount/PERPAGE);
if ($page < 1) {
    $page = 1;
}

if ($page > $numPages){
    $page = $numPages;
}
// Получаем часть массива
$filearray = $dir->getFileArraySlice( ($page-1)*PERPAGE, PERPAGE);

foreach($filearray as $file) {

    $sizeImage = getimagesize($directory . $file);

    if (fnmatch("*_thumb.*", $file)){

        $fileKey = str_replace('_thumb', '', $file);

        if (array_key_exists($fileKey, $return)){
            $ttt = array(
                'filename_thumb' => $file,
                'imageSize_thumb' => $sizeImage[0] . "x" . $sizeImage[1],
            );
            $return[$fileKey] = array_merge($return[$fileKey], $ttt);
        }
    }
    else{
        $return[$file]/*['origin']*/ = array(
            'filename' => $file,
            'imageSize' => $sizeImage[0] . "x" . $sizeImage[1],
        );
    }

}

$typeSort = 'desc';

$typeSort = 'desc';
if (isset($_GET['typesort'])){
    $typeSort = $_GET['typesort'];
}

if ($typeSort == 'asc'){
    ksort($return);
}
elseif($typeSort == 'desc'){
    krsort($return);
}

//echo "<pre>";
//print_r($return);
//echo "</pre>";

echo "<div class='file_list'>";
foreach ($return  As $r) {

    $fileName = trim($r['filename']);
    $fileNameThumb = trim($r['filename_thumb']);

    echo "<div class='fichero' data-src='" . $directory . $fileName . "'>";
    echo "<div class='img'>";
    echo "<img src='{$directory}{$fileNameThumb}' />";
    echo "<p>{$fileName}<br/>" . $r['imageSize'] . "</p>";
    echo "</div>";
    echo "</div>";
}
echo "</div>";

// Создавать постраничную навигацию есть смысл, только если
// есть больше одной страницы
if($numPages > 1) {
    // Создаем навигатор
    $nav = new PageNavigator($totalCount, PERPAGE, $page);
    echo $nav->getNavigator();
}


function make_thumb($src, $dest, $desired_width = 150) {

    $imgType = mime_content_type($src);

    switch ($imgType) {
        case 'image/png':
            $source_image = imagecreatefrompng($src);
            break;
        case 'image/jpeg':
            $source_image = imagecreatefromjpeg($src);
            break;
        case 'image/gif':
            $source_image = imagecreatefromgif($src);
            break;
        default:
            $source_image = imagecreatefromjpeg($src);
    }

    $width = imagesx($source_image);
    $height = imagesy($source_image);

    /* find the "desired height" of this thumbnail, relative to the desired width  */
    $desired_height = floor($height * ($desired_width / $width));

    /* create a new, "virtual" image */
    $virtual_image = imagecreatetruecolor($desired_width, $desired_height);

    /* copy source image at a resized size */
    imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);

    /* create the physical thumbnail image to its destination */
    imagejpeg($virtual_image, $dest);
}


function addPhoto(){
    $result = false;

    $uploadDir = '../files/image/';

    if (isset($_FILES['uploadfile']) && !empty($_FILES['uploadfile']['tmp_name'])) {

        $newFileName = translit($_FILES['uploadfile']['name']);

        if (!move_uploaded_file($_FILES['uploadfile']['tmp_name'], $uploadDir . $newFileName)) {
            $result = 'Ошибка при загрузке файла ' . $_FILES['uploadfile']['name'];
        }
        else {

            $fileInfo = pathinfo($uploadDir . $newFileName);

            $thumbFileName = $fileInfo['filename'] . "_thumb." . $fileInfo['extension'];

            $sourceFile = $uploadDir . $newFileName;
            $thumbFile = $uploadDir . $thumbFileName;

            make_thumb($sourceFile, $thumbFile);

            $result = 'Файл ' . $newFileName . ' успешно загружен';

        }
    }

    echo $result;
}


/**
 * транслит из кир=>лат
 *
 * @param $str
 * @return string
 */
function translit($str)
{
    $str = trim ($str);
    $tr = array(
        "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g",
        "Д"=>"d","Е"=>"e","Ё"=>"E","Ж"=>"j","З"=>"z","И"=>"i",
        "Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
        "О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
        "У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"ts","Ч"=>"ch",
        "Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"yi","Ь"=>"",
        "Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ё"=>"e","ж"=>"j",
        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
        "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
        "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
        " "=> "-", "/"=> "-", "»"=>"", "«"=>"",
        " - "=>"-", " — "=>"-", "&quot;"=>"", "!"=>"", "”"=>"",
        "“"=>"", ","=>"", "("=>"", ")"=>"", "°"=>"", "\""=>"",
        "№"=>"", "*"=>"-", "\""=>"", "'"=>"", "+"=>"", "%"=>"",
        ":"=>"", "&"=>"", "–"=>"", "`"=>""
    );
    return strtr($str,$tr);
}

class PageNavigator {
    // общее число страниц, необходимых для вывода всего списка изображений
    private $totalpages;
    // число изображений на одной странице
    private $recordsperpage;
    // текущая страница
    private $currentpage;
    // текст для навигации
    private $strfirst = 'Первая';
    private $strlast = 'Последняя';

    public function __construct($totalrecords, $recordsperpage = 10, $currentpage = 1){
        $this->totalrecords = $totalrecords;
        $this->recordsperpage = $recordsperpage;
        $this->currentpage = $currentpage;
        $this->setTotalPages($totalrecords, $recordsperpage);
    }
    // Возвращает HTML код навигатора
    public function getNavigator(){
        $strnavigator = '<div>'."\n";
        // Ссылка "Первая страница"
        if($this->currentpage != 1) {
            $strnavigator .= $this->createLink(1, $this->strfirst);
            $strnavigator .= ' ... ';
        }
        // Две страницы назад + текущая страница + две страницы вперед
        for($i = $this->currentpage - 2; $i <= $this->currentpage + 2; $i++) {
            if($i < 1 or $i > $this->totalpages) continue;
            if($i == $this->currentpage) {
                $strnavigator .= '<b>';
                $strnavigator .= $i;
                $strnavigator .= '</b>'."\n";
            } else {
                $strnavigator .= $this->createLink($i, $i);
            }
            if ($i != $this->totalpages) $strnavigator .= ' | ';
        }
        // Ссылка "Последняя страница"
        if($this->currentpage != $this->totalpages){
            $strnavigator .= ' ... ';
            $strnavigator .= $this->createLink($this->totalpages, $this->strlast);
        }
        $strnavigator .= '</div>'."\n";
        return $strnavigator;
    }

    private function createLink($offset, $strdisplay ){
        $strtemp = '<a href="'.$_SERVER['PHP_SELF'].'?page='.$offset.'">'.$strdisplay.'</a>'."\n";
        return $strtemp;
    }
    // всего страниц
    private function setTotalPages($totalrecords, $recordsperpage){
        $this->totalpages = ceil($totalrecords/$recordsperpage);
    }
}
?>