function initGallery() {
    $(".fotorama").each(function (index, el) {
      var _this = $(this),
        _parent = _this.closest(".fotorama-holder");
      var fotorama = $(this).data("fotorama");
      _this
        .on("fotorama:showend ", function (e, fotorama) {
          $(".numbering", _parent).html(
            fotorama.activeIndex + 1 + "/" + fotorama.size
          );
          var _cotent = $(
            ".fotorama__active .fotorama__html div p",
            _parent
          ).html();
          $(".info p", _parent).html(_cotent);
        })
        .fotorama({
          nav: "thumbs",
          width: 792,
          height: 450,
          // width: '100%',
          // height: '100%',
          fit: "scaledown",
          loop: true,
          margin: 0,
          arrows: true,
          thumbwidth: 72,
          thumbheight: 55,
          thumbmargin: 3,
          allowfullscreen: true
        });
      $(".numbering", _parent).html(
        fotorama.activeIndex + 1 + "/" + fotorama.size
      );
      var _cotent = $(".fotorama__active .fotorama__html div p", _parent).html();
      $(".info p", _parent).html(_cotent);
    });
  
    $(".fotorama__wrap").addClass("fotorama__wrap--no-controls");
  
    $(".fotorama-holder .next").click(function (event) {
      event.preventDefault();
      var fotorama = $(this)
        .closest(".fotorama-holder")
        .find(".fotorama")
        .data("fotorama");
      fotorama.show(">");
    });
    $(".fotorama-holder .prev").click(function (event) {
      event.preventDefault();
      var fotorama = $(this)
        .closest(".fotorama-holder")
        .find(".fotorama")
        .data("fotorama");
      fotorama.show("<");
    });
  }