/**
 * plugin.js
 *
 * Released under LGPL License.
 * Copyright (c) 1999-2015 Ephox Corp. All rights reserved
 *
 * License: http://www.tinymce.com/license
 * Contributing: http://www.tinymce.com/contributing
 */

/*global tinymce:true */
/**
 * plugin.js
 *
 * Released under LGPL License.
 * Copyright (c) 1999-2015 Ephox Corp. All rights reserved
 *
 * License: http://www.tinymce.com/license
 * Contributing: http://www.tinymce.com/contributing
 */

/*jshint unused:false */
/*global tinymce:true */

/**
 * bulletImage plugin that adds a toolbar button and menu item.
 */
tinymce.PluginManager.add('bulletImage', function(editor, url) {
    let templateSelector = '.info-side';
    let lastSelectedTemplate = false;
    let win;
    // Add a button that opens a window
    editor.addButton('bulletImage', {
        text: 'Картинка-втяжка',
        icon: false,
        onclick: showDialogBulletImage
    });

    function showDialogBulletImage(editObj) {
        // Open window
        win = editor.windowManager.open({
            title: 'Картинка-втяжка',
            minWidth: 800,
            // minHeight: 650,
            layout: 'flex',
            direction: 'column',
            align: 'stretch',
            body: [
                {
                    name: 'src',
                    type: 'filepicker',
                    filetype: 'image',
                    label: 'источник фото',
                    autofocus: true,
                    onchange: srcChange,
                    onbeforecall: onBeforeCall,
                    value: editObj.src
                },
                {type: 'textbox', name: 'description', label: 'Описание фотографии', autofocus: true, value: editObj.description},
                {type: 'textbox', name: 'source', label: 'Источник фотографии', value: editObj.source},
                {type: 'textbox', name: 'url', label: 'Сcылка (полностью с http://)', value: editObj.url},
                {type: 'checkbox', name: 'target', label: 'Открывать ссылку в новом окне', checked: editObj.target, value: editObj.target}

            ],
            onsubmit: function(e) {

                let target = "target='_self'";

                if (e.data.target == true){
                    target = "target='_blank'";
                }

                let cont = '' +
                    '<figure class="info-side" contenteditable="false">' +
                    '<div class="pic">' +
                    '<a href="' + e.data.url + '" ' + target + '>' +
                    '<img src="' + e.data.src + '" alt="' + e.data.description + '" title="' + e.data.description + '" ext_url="' + e.data.url + '" caption="true">' +
                    '</a>' +
                    '</div>' +
                    '<p>' +
                    '<a href="' + e.data.url + '" ' + target + '  class="bulletImg" rel="noopener noreferrer">' +
                    e.data.description +
                    '<cite>' + e.data.source + '</cite>' +
                    '</a>' +
                    '</p>' +
                    '</figure>';

                editor.insertContent(cont);
            }
        });
    }
    function deleteBulletImage() {
        return function () { lastSelectedTemplate.remove(); };
    }

    function editBulletImage() {
        let link = lastSelectedTemplate.querySelector('.pic > a');
        let img = lastSelectedTemplate.querySelector('.pic img');
        let editObj = {
            src: img.getAttribute('src'),
            description: img.getAttribute('alt'),
            url: link.getAttribute('href'),
            target: link.getAttribute('target') === '_blank' ? true : false,
            source: lastSelectedTemplate.querySelector('cite').innerText
        };
        showDialogBulletImage(editObj);
    }


    let imageListCtrl = {};
    function srcChange(e) {
        let srcURL, prependURL, absoluteURLPattern, meta = e.meta || {};

        if (imageListCtrl) {
            imageListCtrl.value = editor.convertURL(this.value(), 'src');
        }

        tinymce.each(meta, function(value, key) {
            win.find('#' + key).value(value);
        });

        if (!meta.width && !meta.height) {
            srcURL = editor.convertURL(this.value(), 'src');

            // Pattern test the src url and make sure we haven't already prepended the url
            prependURL = editor.settings.image_prepend_url;
            absoluteURLPattern = new RegExp('^(?:[a-z]+:)?//', 'i');
            if (prependURL && !absoluteURLPattern.test(srcURL) && srcURL.substring(0, prependURL.length) !== prependURL) {
                srcURL = prependURL + srcURL;
            }

            this.value(srcURL);
        }
    }

    function onBeforeCall(e) {
        e.meta = win.toJSON();
    }

    function isEditableTemplate(eventElement) {
        return editor.dom.is(eventElement, templateSelector);
    }


    tinymce.util.Tools.each({
        mceEditBulletImage: editBulletImage,
        mceDeleteBulletImage: deleteBulletImage()
    }, function (fn, cmd) {
        editor.addCommand(cmd, fn);
    });



    function addToolbars() {
        let toolbarItems = false;

        if (!toolbarItems) {
            toolbarItems = 'editBulletImage | deleteBulletImage';
        }

        editor.addContextToolbar(
            isEditableTemplate,
            toolbarItems
        );
    }

    addToolbars();
    function addButtons() {

        editor.addButton('deleteBulletImage', {
            title: 'Удалить',
            text: 'Удалить',
            cmd: 'mceDeleteBulletImage'
        });
        editor.addButton('editBulletImage', {
            title: 'Рредактировать',
            text: 'Редактировать',
            cmd: 'mceEditBulletImage'
        });
    }

    addButtons();

    function addEvents() {
        editor.on('NodeChange', function(e) {
            if(lastSelectedTemplate) {
                lastSelectedTemplate.classList.remove('selected--template');
            }
            lastSelectedTemplate = false;
            //Set up the lastSelectedTemplate
            let isEditable = isEditableTemplate(e.element);
            if (!isEditable) {
                e.parents.forEach(function (el, i) {
                    if (lastSelectedTemplate) { return ;}
                    if (editor.dom.is(el, templateSelector) ) {
                        lastSelectedTemplate = el;
                    }
                });
            } else {
                lastSelectedTemplate = e.element;
            }
            if (lastSelectedTemplate) {
                lastSelectedTemplate.classList.add('selected--template');
            }
        });
    }
    addEvents();
});