/**
 * plugin.js
 *
 * Released under LGPL License.
 * Copyright (c) 1999-2015 Ephox Corp. All rights reserved
 *
 * License: http://www.tinymce.com/license
 * Contributing: http://www.tinymce.com/contributing
 */

/*global tinymce:true */

tinymce.PluginManager.add('embedInsert', function(editor) {
	function showDialog() {
		var win = editor.windowManager.open({
			title: "Embed insert",
			body: {
				type: 'textbox',
				name: 'embed_insert',
				multiline: true,
				minWidth: editor.getParam("code_dialog_width", 600),
				minHeight: editor.getParam("code_dialog_height", Math.min(tinymce.DOM.getViewPort().h - 200, 500)),
				spellcheck: false,
				style: 'direction: ltr; text-align: left'
			},
			onSubmit: function(e) {
				// We get a lovely "Wrong document" error in IE 11 if we
				// don't move the focus to the editor before creating an undo
				// transation since it tries to make a bookmark for the current selection
				editor.focus();

				editor.undoManager.transact(function() {

					let divEmbed = editor.dom.doc.createElement('div');

					divEmbed.classList.add('embed-insert');
					divEmbed.contentEditable = false;
					divEmbed.innerHTML = '<div class="container-video">'
						+ e.data.embed_insert
						+ '</div>';

					Array.from(divEmbed.getElementsByTagName('script')).forEach(function (scr) {
						scr.setAttribute('type', 'mce-no/type');
						scr.setAttribute('data-mce-src', scr.src);
					});
					if(divEmbed.getElementsByTagName('iframe').length === 0) {
                        divEmbed.children[0].classList.add('data-preview');
						let _ID = 'frame_' + (new Date()).getTime();
						let divEmbedIframe = editor.dom.doc.createElement('iframe');
						divEmbedIframe.id = _ID;
						divEmbedIframe.height = 400;
						divEmbedIframe.contentEditable = false;
						divEmbedIframe.style = 'width:100%; border:none;';
						divEmbedIframe.srcdoc = e.data.embed_insert;
						divEmbedIframe.setAttribute('data-preview', '1');
						divEmbed.prepend(divEmbedIframe);
						editor.selection.setContent(divEmbed.outerHTML);
					} else {
						editor.selection.setContent(divEmbed.outerHTML);
					}
				});

				editor.selection.setCursorLocation();
				editor.nodeChanged();
			}
		});

		// Gecko has a major performance issue with textarea
		// contents so we need to set it when all reflows are done
		win.find('#code').value( null);
	}

	editor.addCommand("mceCodeEditor", showDialog);

	editor.addButton('embedInsert', {
		icon: 'code',
		text: 'Embed',
		tooltip: 'Embed',
		onclick: showDialog
	});

	editor.addMenuItem('embedInsert', {
		icon: 'code',
		text: 'Embed',
		context: 'tools',
		onclick: showDialog
	});

});