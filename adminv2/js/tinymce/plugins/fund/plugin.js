/**
 * plugin.js
 *
 * Released under LGPL License.
 * Copyright (c) 1999-2015 Ephox Corp. All rights reserved
 *
 * License: http://www.tinymce.com/license
 * Contributing: http://www.tinymce.com/contributing
 */

/*jshint unused:false */
/*global tinymce:true */

tinymce.PluginManager.add('fund', function(editor) {
    editor.addButton('fund', {
        text: 'Форма пожертвований',
        icon: false,
        onclick: function() {
            var cont = '<p class="donate-form"></p><br />';
            editor.insertContent(cont);
        }
    });
});