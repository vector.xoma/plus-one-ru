{if $Item->group_items }
<script type="application/javascript">
    window._existsContents = {$Item->group_items};
</script>
{/if}
<h4>
{$title}
</h4>
<form name="blogpost" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="pull-right">
                        <div class="btn-group">
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="apply">
                                <i class="fa fa-check"></i> Применить
                            </button>
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="save">
                                <i class="fa fa-check"></i> Сохранить
                            </button>
                            <a class="btn btn-outline btn-warning btn-xs" type="button"
                               href="index.php?section=BlockAnnouncements">
                                <i class="fa fa-ban"></i> Отменить
                            </a>
                        </div>
                    </div>&nbsp;
                </div>
                <div class="panel-body">
                    {if $hasErrors}
                        {foreach item=err from=$Errors name=err}
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="alert alert-danger alert-dismissable">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        {$err}
                                    </div>
                                </div>
                            </div>
                        {/foreach}
                    {/if}
                    <div class="tab-content">
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="well">
                                    <div class="form-group {if !$Item->name}has-warning{/if}">
                                        <label>Название</label>
                                        <input name="name"  type="text" value="{$Item->name}" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="enabled" value="1" {if $Item->enabled==1}checked{/if} />
                                                Отображать на сайте
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label id="created_label" {if !$Item->date_created}class="text-danger"{/if}>Дата</label>
                                        <input id="calendar" name="created" type="text" value='{$Item->date_created|escape}' placeholder="Укажите дату записи" class="form-control">
                                    </div>
                                </div>

                                <div class="well">
                                    <div class="autocomplete-block">
                                        <input type="text" name="query"  class="form-control js_search_posts" placeholder="Поиск поста (новости)....">
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-7">
                                <div class="well">
                                    <label class="{if $Errors->group_items} text-danger{/if}">Связанные посты: <strong class="js_group_items_counter"></strong></label>
                                    <div id="group_items"
                                         {if !$hasErrors}data-group_id="{$Item->id}"{/if}
                                         data-field="block_announcement_id"
                                         data-items-path="ajax/getBlockAnnouncementItems.php">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel-heading">
                    &nbsp;
                    <div class="pull-right">
                        <div class="btn-group">
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="apply">
                                <i class="fa fa-check"></i> Применить
                            </button>
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="save">
                                <i class="fa fa-check"></i> Сохранить
                            </button>
                            <a class="btn btn-outline btn-warning btn-xs" type="button"
                               href="index.php?section=BlockAnnouncements">
                                <i class="fa fa-ban"></i> Отменить
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="type_visual_block" value="block_announcement">
    <input type="hidden" name="item_id" id="item_id" value="{$Item->id}">
    <input type="hidden" name="form_id" value="{$Item->form_id}">
</form>