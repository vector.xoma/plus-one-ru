<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">
            {$title}
        </h2>
    </div>
</div>
<div class="row">
    <!-- /-->
</div>

{if $Error}
    <div class="row">
        <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {$Error}
        </div>
    </div>
{/if}
{if $Errors}
    {foreach item=err from=$Errors name=err}
    <div class="row">
        <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {$err}
        </div>
    </div>
    {/foreach}
{/if}

{literal}
    <script>
        window.EditorBlogpostLead = `{/literal}{$Item->lead|escape|replace:'`':'\`'}{literal}`;
        window.EditorBlogpostBody = `{/literal}{$Item->body|escape|replace:'`':'\`'}{literal}`;
    </script>
{/literal}


<form name="blogpost" method="post" action="blogpost/save/{$Token}/{$Item->id}" class="dropzone" enctype="multipart/form-data">


    <input type="hidden" name="item_id" id="item_id" value="{$Item->id}">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {$Error}&nbsp;
                    <div class="pull-right">
                        <div class="btn-group">
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="apply">
                                <i class="fa fa-check"></i> Применить
                            </button>
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="preview">
                                <i class="fa fa-check"></i> Предпросмотр
                            </button>
                            {if $preview}
                                <script>
                                    window.open('{$root_url}{$articleUrl}?mode=preview', '_blank');
                                </script>
                            {/if}
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="save">
                                <i class="fa fa-check"></i> Сохранить
                            </button>
                            <a class="btn btn-outline btn-warning btn-xs" type="button"
                               href="blogpost/">
                                <i class="fa fa-ban"></i> Отменить
                            </a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="tab-content">
                        <div class="row">
                            <div class="col-lg-6">
                                {include file='include/format_post.tpl'}
                                <div class="well">
                                    <div class="form-group">
                                        <label id="body_color">Цвет фона страницы</label>
                                        <input name="body_color" class="form-control" value="{$Item->body_color}">
                                    </div>
                                    <div class="form-group">
                                        <label id="font_color">Цвет шрифта</label>
                                        <input name="font_color" class="form-control" value="{$Item->font_color}">
                                    </div>

                                    <div class="form-group">
                                        <label id="border_color">Цвет рамки и подчеркивания</label>
                                        <input name="border_color" class="form-control" value="{$Item->border_color}">
                                    </div>

                                    <div class="form-group">
                                        <label id="name_label" {if !$Item->name}class="text-danger"{/if}>Заголовок</label>
                                        <textarea name="name" class="form-control smalleditor">{$Item->name}</textarea>
                                    </div>

                                    <div class="form-group" id="header_on_page_block" style="display: none;">
                                        <label id="header_on_page_label">Заголовок для внутренней страницы</label>
                                        <textarea name="header_on_page" class="form-control smalleditor">{$Item->header_on_page}</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label id="header_rss_label" {if !$Item->header_rss}class="text-danger"{/if}>Заголовок для социальных сетей (max 140 символов) </label>
                                        <input id="header_rss_input" name="header_rss" type="text" value='{$Item->header_rss|escape}' maxlength="140" placeholder="Укажите заголовок для RSS" class="form-control">
                                        {*<textarea name="header_rss" class="form-control smalleditor">{$Item->header_rss}</textarea>*}
                                    </div>

                                    <div class="form-group">
                                        <label id="created_label" {if !$Item->date_created}class="text-danger"{/if}>Дата</label>
                                        <input id="calendar" name="created" type="text" value='{$Item->date_created|escape}' placeholder="Укажите дату записи," class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label id="writers_label" {if !$Item->writers}class="text-danger"{/if}>
                                            Автор, от чьего имени будет опубликована статья
                                        </label>
                                        <select class="form-control" id="writers_input" name="writers">
                                            <option value="0"> -- Выберите автора -- </option>
                                            {foreach item=writer from=$writers name=writer}
                                            <option value="{$writer->id}" {if $Item->writers==$writer->id}selected{/if}>{$writer->name|escape}</option>
                                            {/foreach}
                                        </select>
                                    </div>

{*                                    <div class="form-group">*}
{*                                        <div class="checkbox">*}
{*                                            <label>*}
{*                                                <input type="checkbox" name="anounce_new_author" value="1" {if $Item->anounce_new_author==1}checked{/if} /> Пометить запись как анонс нового автора*}
{*                                            </label>*}
{*                                        </div>*}
{*                                    </div>*}

                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="enabled" value="1"
                                                       {if $Item->enabled==1}checked{/if} /> Отображать на сайте
                                            </label>
                                        </div>
                                    </div>

{*                                    <div class="form-group">*}
{*                                        <div class="checkbox">*}
{*                                            <label>*}
{*                                                <input type="checkbox" name="picture_day" value="1"*}
{*                                                       {if $Item->picture_day==1}checked{/if} /> Материал к блоку "Картина дня"*}
{*                                            </label>*}
{*                                        </div>*}
{*                                    </div>*}

                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="publish_news_aggregators" value="1"
                                                       {if $Item->publish_news_aggregators==1}checked{/if} /> Публикация в новостных агрегаторах
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="dzen" value="1"
                                                       {if $Item->dzen==1}checked{/if} /> Публикация в Дзен/Пульс
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="is_partner_material" value="1"
                                                       {if $Item->is_partner_material==1}checked{/if} /> Партнерский материал
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="well">
                                    <h3>Автор</h3>
                                    <div class="form-group {if !$Item->author_signature}has-danger danger{/if}">
                                        <label class="control-label">Подпись (Автор, Беседовал, etc)</label>
                                        <input name="author_signature" type="text" value="{if $Item->author_signature}{$Item->author_signature|escape}{else}Автор{/if}"
                                               placeholder="" class="form-control">
                                    </div>

                                    <div class="form-group {if !$Item->author_name}has-warning{/if}">
                                        <label class="{if !$Item->author_name}text-danger{/if}">Имя</label>
                                        <input name="author_name" type="text" value="{if $Item->author_name}{$Item->author_name|escape}{/if}"
                                               placeholder="" class="form-control  {if !$Item->author_name}text-danger{/if}">
                                    </div>

                                </div>

                                <div class="well">
                                    <h5>Выбор благотворительного фонда</h5>

                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="show_fund_at_blogpost" value="1" {if $Item->show_fund == 1}checked{/if} /> Отображать блок с благотворительным фондом
                                            </label>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label id="header_donate_form_label" {if !$Item->header_donate_form}class="text-danger"{/if}>Заголовок для формы пожертвований (max 50 символов) </label>
                                        <input id="header_donate_form_input" name="header_donate_form" type="text" value='{$Item->header_donate_form|escape}' maxlength="50" placeholder="Укажите заголовок для формы пожертвований" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label>Укажите благотворительный фонд</label>
                                        <select class="selectpicker form-control" data-max-options="1" data-live-search="true" name="fund_select" title="-- Укажите благотворительный фонд --">
                                            {foreach item=fund from=$funds name=fund}
                                                <option value="{$fund->shop_article_id}" {if $Item->shop_article_id == $fund->shop_article_id}selected{/if}>{$fund->name|escape}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">

                                {include file="include/tagFirstLevel.tpl"}

                                <div class="well">
                                    {include file="include/tagsPost.tpl"}
                                </div>


                                <div class="well">
                                    <div class="form-group">
                                        <label>Спецпроект</label>
                                        <select class="form-control" name="spec_project">
                                            <option value="0"> -- Выберите спецпроект -- </option>
                                            {foreach item=specProject from=$specProjects name=specProject}
                                                <option value="{$specProject->id}" {if $Item->spec_project==$specProject->id}selected{/if}>{$specProject->name|escape}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>


                                <div class="well">
                                    <div class="form-group">
                                        <label>Конференция</label>
                                        <select class="form-control" name="conference">
                                            <option value="0"> -- Выберите конференцию -- </option>
                                            {foreach item=conf from=$conferences name=conf}
                                                <option value="{$conf->id}" {if $Item->conference==$conf->id}selected{/if}>{$conf->name|escape}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>


                                <div class="well">
                                    <div class="form-group">
                                        <label>Тип материала</label>
                                        <select class="form-control" name="type_material">
                                            <option value="0"> -- Выберите тип материала -- </option>
                                            {foreach item=typeMaterial from=$typeMaterials name=typeMaterial}
                                                <option value="{$typeMaterial->id}" {if $Item->type_material==$typeMaterial->id}selected{/if}>{$typeMaterial->name|escape}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>

                                <div class="well">
                                    <div class="form-group">
                                        <label>Партнер записи</label>
                                        <select class="form-control" name="partner">
                                            <option value="0"> -- Выберите партнера -- </option>
                                            {foreach item=partner from=$partners name=partner}
                                                <option value="{$partner->id}" {if $Item->partner==$partner->id}selected{/if}>{$partner->name}</option>
                                            {/foreach}
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>URL оригинала статьи партнера</label>
                                        <input name="partner_url" type="text" value='{$Item->partner_url|escape}'
                                               placeholder="Укажите URL оригинала статьи партнера" class="form-control">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="well">
                                            <div class="form-group">
                                                <label id="url_label" {if !$Item->url}class="text-danger"{/if}>
                                                    URL записи
                                                </label>
                                                <input name="url" type="text" value='{$Item->url|escape}'
                                                       placeholder="Укажите URL записи" class="form-control">
                                            </div>
                                            {include file="include/_meta_tags.tpl"}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: 20px;">
                            <div class="col-lg-12">
                                <div class="well col-lg-2" style="margin: 0 2% 0 0;">
                                    <h4>Блок 1/1</h4>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            {if $Item->image_1_1}
                                                <img id="image_1_1" class="image_preview" src='{$images_uploaddir}{$Item->image_1_1}' alt="" style="width: 100%; height: 100%" />
                                            {else}
                                                <img id="image_1_1" class="image_preview" src='images/no_foto.gif' alt=""/>
                                            {/if}
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Изображение</label>
                                                <input name="image_1_1" type="file" class="upload_button" data-imagetype = "1_1" style="width: 100%; overflow: hidden;"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <div><label>Вид анонса</label></div>
                                                <label class="radio-inline">
                                                    <input type="radio" value="text" {if $Item->type_announce_1_1 == "text" || $Item->type_announce_1_1 == ""}checked="checked"{/if} name="type_announce_1_1"> Текст
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" id="1_1_picture" value="picture" {if $Item->type_announce_1_1 == "picture"}checked="checked"{/if} name="type_announce_1_1"> Иллюстрация
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="well col-lg-2" style="margin: 0 2% 0 0;">
                                    <h4>Блок 1/2</h4>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            {if $Item->image_1_2}
                                                <img id="image_1_2" class="image_preview" src='{$images_uploaddir}{$Item->image_1_2}' alt="" style="width: 100%;  height: 100%"/>
                                            {else}
                                                <img id="image_1_2" class="image_preview" src='images/no_foto.gif' alt=""/>
                                            {/if}
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Изображение</label>
                                                <input name="image_1_2" type="file" class="upload_button" data-imagetype = "1_2" style="width: 100%; overflow: hidden;"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <div><label>Вид анонса</label></div>
                                                <label class="radio-inline">
                                                    <input type="radio" value="text" {if $Item->type_announce_1_2 == "text" || $Item->type_announce_1_2 == ""}checked="checked"{/if} name="type_announce_1_2"> Текст
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" id="1_2_picture" value="picture" {if $Item->type_announce_1_2 == "picture"}checked="checked"{/if} name="type_announce_1_2"> Иллюстрация
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="well col-lg-2" style="margin: 0 2% 0 0;">
                                    <h4>Блок 1/3</h4>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            {if $Item->image_1_3}
                                                <img id="image_1_3" class="image_preview" src='{$images_uploaddir}{$Item->image_1_3}' alt="" style="width: 100%;  height: 100%" />
                                            {else}
                                                <img id="image_1_3" class="image_preview" src='images/no_foto.gif' alt=""/>
                                            {/if}
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Изображение</label>
                                                <input name="image_1_3" type="file" class="upload_button" data-imagetype = "1_3" style="width: 100%; overflow: hidden;"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <div><label>Вид анонса</label></div>
                                                <label class="radio-inline">
                                                    <input type="radio" value="text" {if $Item->type_announce_1_3 == "text" || $Item->type_announce_1_3 == ""}checked="checked"{/if} name="type_announce_1_3"> Текст
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" id="1_3_picture" value="picture" {if $Item->type_announce_1_3 == "picture"}checked="checked"{/if} name="type_announce_1_3"> Иллюстрация
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="well col-lg-2" style="margin: 0 2% 0 0;">
                                    <h4>RSS</h4>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            {if $Item->image_rss}
                                                <img id="image_rss" class="image_preview" src='{$images_uploaddir}{$Item->image_rss}' alt="" style="width: 100%;  height: 100%" />
                                            {else}
                                                <img id="image_rss" class="image_preview" src='images/no_foto.gif' alt=""/>
                                            {/if}
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Изображение</label>
                                                <input name="image_rss" type="file" style="width: 100%; overflow: hidden;"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: 20px;">
                            <div class="well col-lg-12">

                            </div>
                        </div>


                        <div class="row">
                            <div class="col-lg-12">
                                <!-- /-->
                            </div>
                        </div>



                        <div class="row">
                            <div class="col-lg-12">

                                <div>
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active">
                                            <a href="#text-page" aria-controls="text-page" role="tab" data-toggle="tab">
                                                Текстовое наполнение
                                            </a>
                                        </li>
                                        <li role="presentation">
                                            <a href="#partner-links" aria-controls="partner-links" role="tab" data-toggle="tab">
                                                Партнерские ссылки
                                            </a>
                                        </li>
                                        {if $Item->id}
                                            <li role="presentation">
                                                <a href="#linked-post" aria-controls="linked-post" role="tab" data-toggle="tab">
                                                    Связанные статьи
                                                </a>
                                            </li>
                                        {else}
                                            <li role="presentation" class="disabled">
                                                <a href="#linked-post" aria-controls="linked-post" role="tab" onclick="return false;">
                                                    Связанные статьи (сначала сохраните статью)
                                                </a>
                                            </li>
                                        {/if}
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content" style="min-height: 500px;">
                                        <div role="tabpanel" class="tab-pane active" id="text-page">
                                            <div class="well">
                                                <div class="form-group">
                                                    <label id="header_label">Текст подзаголовка</label>
                                                     <textarea rows="5" name="header"  id="header" class="form-control fulleditor">{$Item->header}</textarea>
{*                                                    <div id="reactEditor_blogpost_header"></div>*}
                                                </div>
                                            </div>

                                            <div class="well">
                                                <div class="form-group">
                                                    <label id="header_label">Лид</label>
                                                    {* <textarea rows="20" name="lead" id="lead" class="form-control fulleditor">{$Item->lead}</textarea> *}
                                                    <div id="reactEditor_blogpost_lead"></div>
                                                </div>
                                            </div>

                                            <div class="well">
                                                <div class="form-group">
                                                    <label>Текст статьи</label>
                                                    {* <textarea rows="20" name="body" id="body" class="form-control fulleditor">{$Item->body}</textarea> *}
                                                    <div id="reactEditor_blogpost_body"></div>
                                                </div>
                                            </div>
                                        </div>

                                        {* партнерские ссылки *}
                                        <div role="tabpanel" class="tab-pane" id="partner-links">
                                            {include file="include/partnersLinks.tpl"}
                                        </div>

                                        {* связанные статьи *}
                                        <div role="tabpanel" class="tab-pane" id="linked-post">
                                            <div class="well" style="background: #fff">
                                                <div class="row">

                                                    <div class="col-lg-7">
                                                        <table id="relatedPostDataTable" class="table order-column">
                                                            <thead>
                                                            <tr>
                                                                <th style="width: 100px; cursor: pointer">Создана</th>
                                                                <th style="width: 100px; cursor: pointer">Теги</th>
                                                                <th style="width: 100px; cursor: pointer">Автор</th>
                                                                <th style="width: 100px; cursor: pointer">Партнер</th>
                                                                <th style="width: auto; cursor: pointer">Название</th>
                                                                <th style="width: 100px; cursor: pointer"></th>
                                                            </tr>
                                                            </thead>
                                                        </table>
                                                    </div>

                                                    <div class="col-lg-5">
                                                        <div class="well">
                                                            <div class="form-group">
                                                                <label>Связанные статьи</label>
                                                            </div>

                                                            <div id="already_linked_posts">
                                                                {include file="include/include_already_linked_posts.tpl"}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel-heading">
                    {$Error}&nbsp;
                    <div class="pull-right">
                        <div class="btn-group">
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="apply">
                                <i class="fa fa-check"></i> Применить
                            </button>
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="preview">
                                <i class="fa fa-check"></i> Предпросмотр
                            </button>
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="save">
                                <i class="fa fa-check"></i> Сохранить
                            </button>
                            <a class="btn btn-outline btn-warning btn-xs" type="button"
                               href="blogpost/">
                                <i class="fa fa-ban"></i> Отменить
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="type_post" value="{"Helper::TYPE_MANUAL"|constant}">
    <input type="hidden" name="type_visual_block" value="manual">
    <input type="hidden" name="item_id" id="item_id" value="{$Item->id}">
    <input type="hidden" name="form_id" value="{$Item->form_id}">
</form>

{*{include file='tinymce_init.tpl'}*}
{include file='include/_set_meta.tpl'}
