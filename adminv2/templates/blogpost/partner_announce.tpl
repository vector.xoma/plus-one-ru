<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">
            {$title}
        </h2>
    </div>
</div>
<div class="row">
    <!-- /-->
</div>

{if $Error}
    <div class="row">
        <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {$Error}
        </div>
    </div>
{/if}
{if $Errors}
    {foreach item=err from=$Errors name=err}
    <div class="row">
        <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {$err}
        </div>
    </div>
    {/foreach}
{/if}

{literal}
    <script>
        window.EditorBlogpostLead = `{/literal}{$Item->lead|escape|replace:'`':'\`'}{literal}`;
        window.EditorBlogpostBody = `{/literal}{$Item->body|escape|replace:'`':'\`'}{literal}`;
    </script>
{/literal}


<form name="blogpost" method="post" action="blogpost/save/{$Token}/{$Item->id}" class="" enctype="multipart/form-data">


    <input type="hidden" name="item_id" id="item_id" value="{$Item->id}">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {$Error}&nbsp;
                    <div class="pull-right">
                        <div class="btn-group">
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="apply">
                                <i class="fa fa-check"></i> Применить
                            </button>
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="preview">
                                <i class="fa fa-check"></i> Предпросмотр
                            </button>
                            {if $preview}
                                <script>
                                    window.open('{$root_url}{$articleUrl}?mode=preview', '_blank');
                                </script>
                            {/if}
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="save">
                                <i class="fa fa-check"></i> Сохранить
                            </button>
                            <a class="btn btn-outline btn-warning btn-xs" type="button"
                               href="blogpost/">
                                <i class="fa fa-ban"></i> Отменить
                            </a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="tab-content">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="well">
                                    <div class="form-group">
                                        <label id="name_label" {if !$Item->name}class="text-danger"{/if}>Заголовок (max 80 символов)</label>
                                        <textarea name="name" class="form-control smalleditor">{$Item->name}</textarea>
                                        <small id="name_character_count" class="js_counters text-muted pull-right" data-max-length="80"></small>
                                    </div>

                                    <div class="form-group">
                                        <label id="header_label" {if !$Item->header}class="text-danger"{/if}>Поздаголовок  (max 170 символов)</label>
                                        <textarea name="header" class="form-control smalleditor">{$Item->header}</textarea>
                                        <small id="header_character_count" class="js_counters text-muted pull-right" data-max-length="170"></small>
                                    </div>

                                    <div class="form-group">
                                        <label id="created_label" {if !$Item->date_created}class="text-danger"{/if}>Дата</label>
                                        <input id="calendar" name="created" type="text" value='{$Item->date_created|escape}' placeholder="Укажите дату записи," class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="enabled" value="1"
                                                       {if $Item->enabled==1}checked{/if} /> Отображать на сайте
                                            </label>
                                        </div>
                                    </div>

                                </div>
                                <div class="row image-block" style="margin-bottom: 20px;">
                                    <div class="col-lg-12">
                                        <div class="well col-lg-4" style="margin: 0 2% 0 0; width: 32%">
                                            <h4>Блок 1/1</h4>
                                            <div class="row">
                                                <div class="col-lg-12 image_preview">
                                                    {if $Item->image_1_1}
                                                        <img id="image_1_1" src='{$images_uploaddir}{$Item->image_1_1}' alt=""/>
                                                    {else}
                                                        <img id="image_1_1" src='images/no_foto.gif' alt=""/>
                                                    {/if}
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label>Изображение</label>
                                                        <input name="image_1_1" type="file" class="upload_button" data-imagetype = "1_1" style="width: 100%; overflow: hidden;"/>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <div><label>Вид анонса</label></div>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="text" {if $Item->type_announce_1_1 == "text" || $Item->type_announce_1_1 == ""}checked="checked"{/if} name="type_announce_1_1"> Текст
                                                        </label>
                                                        <label class="radio-inline">
                                                            <input type="radio" id="1_1_picture" value="picture" {if $Item->type_announce_1_1 == "picture"}checked="checked"{/if} name="type_announce_1_1"> Иллюстрация
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="well col-lg-4" style="margin: 0 2% 0 0; width: 32%">
                                            <h4>Блок 1/2</h4>
                                            <div class="row">
                                                <div class="col-lg-12 image_preview">
                                                    {if $Item->image_1_2}
                                                        <img id="image_1_2" src='{$images_uploaddir}{$Item->image_1_2}' alt=""/>
                                                    {else}
                                                        <img id="image_1_2" src='images/no_foto.gif' alt=""/>
                                                    {/if}
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label>Изображение</label>
                                                        <input name="image_1_2" type="file" class="upload_button" data-imagetype = "1_2" style="width: 100%; overflow: hidden;"/>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <div><label>Вид анонса</label></div>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="text" {if $Item->type_announce_1_2 == "text" || $Item->type_announce_1_2 == ""}checked="checked"{/if} name="type_announce_1_2"> Текст
                                                        </label>
                                                        <label class="radio-inline">
                                                            <input type="radio" id="1_2_picture" value="picture" {if $Item->type_announce_1_2 == "picture"}checked="checked"{/if} name="type_announce_1_2"> Иллюстрация
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="well col-lg-4" style="margin: 0 0% 0 0; width: 32%">
                                            <h4>Блок 1/3</h4>
                                            <div class="row">
                                                <div class="col-lg-12 image_preview">
                                                    {if $Item->image_1_3}
                                                        <img id="image_1_3" src='{$images_uploaddir}{$Item->image_1_3}' alt=""  />
                                                    {else}
                                                        <img id="image_1_3" src='images/no_foto.gif' alt=""/>
                                                    {/if}
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label>Изображение</label>
                                                        <input name="image_1_3" type="file" class="upload_button" data-imagetype = "1_3" style="width: 100%; overflow: hidden;"/>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <div><label>Вид анонса</label></div>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="text" {if $Item->type_announce_1_3 == "text" || $Item->type_announce_1_3 == ""}checked="checked"{/if} name="type_announce_1_3"> Текст
                                                        </label>
                                                        <label class="radio-inline">
                                                            <input type="radio" id="1_3_picture" value="picture" {if $Item->type_announce_1_3 == "picture"}checked="checked"{/if} name="type_announce_1_3"> Иллюстрация
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="col-lg-6">

                                {include file="include/tagFirstLevel.tpl"}


                                <div class="well">
                                    <div class="form-group">
                                        <label for="partner">Партнер записи</label>
                                        <select class="form-control" name="partner" id="partner">
                                            <option value="0"> -- Выберите партнера -- </option>
                                            {foreach item=partner from=$partners name=partner}
                                                <option value="{$partner->id}" {if $Item->partner==$partner->id}selected{/if}>{$partner->name}</option>
                                            {/foreach}
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>URL оригинала статьи партнера</label>
                                        <input name="partner_url" type="text" value='{$Item->partner_url|escape}'
                                               placeholder="Укажите URL оригинала статьи партнера" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="url_target" value="_blank"
                                                       {if $Item->url_target=='_blank'}checked{/if} /> Открывать в новой вкладке
                                            </label>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel-heading">
                    {$Error}&nbsp;
                    <div class="pull-right">
                        <div class="btn-group">
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="apply">
                                <i class="fa fa-check"></i> Применить
                            </button>
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="preview">
                                <i class="fa fa-check"></i> Предпросмотр
                            </button>
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="save">
                                <i class="fa fa-check"></i> Сохранить
                            </button>
                            <a class="btn btn-outline btn-warning btn-xs" type="button"
                               href="blogpost/">
                                <i class="fa fa-ban"></i> Отменить
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="type_post" value="{"Helper::TYPE_PARTNER_ANNOUNCE"|constant}">
    <input type="hidden" name="type_visual_block" value="partner_announce">
    <input type="hidden" name="item_id" id="item_id" value="{$Item->id}">
    <input type="hidden" name="type_material" id="type_material" value="0">
    <input type="hidden" name="form_id" value="{$Item->form_id}">
</form>

{*{include file='tinymce_init.tpl'}*}
