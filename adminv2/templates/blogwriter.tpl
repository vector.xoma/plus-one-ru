<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">
            {$title}
        </h2>
    </div>
</div>
<div class="row">
    <!-- /-->
</div>


{literal}
    <script>
        window.EditorWriterBlog = `{/literal}{$Item->body|escape}{literal}`;
        window.EditorHeaderText = `{/literal}{$Item->description|escape}{literal}`;
    </script>
{/literal}

<form name="form" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-12">
            {if $flash}
                <div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    {$flash}
                </div>
            {/if}
            <div class="panel panel-default">
                <div class="panel-heading">
                    {$Error}&nbsp;
                    <div class="pull-right">
                        <div class="btn-group">
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="apply" value="1">
                                <i class="fa fa-check"></i> Применить
                            </button>
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save">
                                <i class="fa fa-save"></i> Сохранить
                            </button>
                            <a class="btn btn-outline btn-warning btn-xs" type="button"
                               href="index.php?section=BlogWriters&token={$Token}">
                                <i class="fa fa-ban"></i> Отменить
                            </a>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="tab-content">

                        {include file="include/seoContent.tpl"}

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="well">
                                    <div class="form-group {if !$Item->name}has-warning{/if}">
                                        <label>Название писателя</label>
                                        <input name="name" type="text" value='{$Item->name|escape}'
                                               placeholder="Укажите название писателя" class="form-control js_writer_name">
                                    </div>

                                    <div class="form-group">
                                        <label>Название писателя в родительном падеже</label>
                                        <input name="name_genitive" type="text" value='{$Item->name_genitive|escape}'
                                               placeholder="Укажите название писателя в родительном падеже" class="form-control">
                                    </div>

                                    <div class="form-group {if !$Item->tag_url}has-warning{/if}">
                                        <label for="tag_url_label" id="tag_url">Писатель в URL</label>
                                        <input id="tag_url" name="tag_url" type="text" value='{$Item->tag_url|escape}'
                                               placeholder="Писатель в URL" class="form-control js_writer_url">
                                    </div>

                                    <div class="form-group">
                                        <label>Текст в шапке</label>
                                        <div id="reactEditor_headerText"></div>
                                        {*<textarea rows="2" name="description" class="form-control">{$Item->description}</textarea>*}
                                        {*<textarea name="description" class="form-control smalleditor">{$Item->description}</textarea>*}
                                    </div>

                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="enabled" value="1"
                                                       {if $Item->enabled==1}checked{/if} /> Отображать на сайте
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="useplatforma" value="1"
                                                       {if $Item->useplatforma==1}checked{/if} /> Участник "Платформы"
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="private" value="1"
                                                       {if $Item->private==1}checked{/if} /> Автор колонки
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="bloger" value="1"
                                                       {if $Item->bloger==1}checked{/if} /> Автор блога
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Лидер направления</label>
                                        <select class="selectpicker form-control" data-live-search="true" name="leader">
                                            <option value="0"
                                                    {if $Item->leader == $tagItem->id}selected{/if}> -- Выберите направление в котором автор - лидер -- </option>
                                            {foreach item=tag from=$blogTags name=tag}
                                                {if $tag->items}
                                                    <optgroup label="{$tag->name}">
                                                        {foreach item=tagItem from=$tag->items name=tagItem}
                                                            <option value="{$tagItem->id}"
                                                                    {if $Item->leader == $tagItem->id}selected{/if}>{$tagItem->name_lead}</option>
                                                        {/foreach}
                                                    </optgroup>
                                                {/if}
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="well">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            {if $Item->image}
                                                <img id="large_image" class="image_preview"
                                                     src='{$images_uploaddir}{$Item->image}?r={math equation="rand(1,1000000)"}'
                                                     alt="" style="width: 92%;"/>
                                                <p>
                                                    <a id="button_delete_largeimage"
                                                       class="btn btn-outline btn-danger btn-xs js-button_delete_largeimage" type="button"
                                                       href="#">
                                                        <i class="fa fa-times-circle"></i> Удалить изображение
                                                    </a>
                                                </p>
                                            {else}
                                                <img id="large_image" class="image_preview" src='images/no_foto.gif' alt=""/>
                                            {/if}
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <label>Изображение для шапки desktop</label>
                                                <input name="large_image" type="file" style="width: 100%; overflow: hidden;"/>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" value="0" id="delete_large_image" name="delete_large_image"/>
                                </div>


                                <div class="well">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            {if $Item->stub_image}
                                                <img id="stub_image" class="image_preview"
                                                     src='{$images_uploaddir}{$Item->stub_image}?r={math equation="rand(1,1000000)"}'
                                                     alt="" style="width: 92%;"/>
                                                <p>
                                                    <a id="button_delete_stub_image"
                                                       class="btn btn-outline btn-danger btn-xs js-button_delete_stub_image" type="button"
                                                       href="#">
                                                        <i class="fa fa-times-circle"></i> Удалить изображение
                                                    </a>
                                                </p>
                                            {else}
                                                <img id="stub_image" class="image_preview" src='images/no_foto.gif' alt=""/>
                                            {/if}
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <label>Изображение для шапки mobile</label>
                                                <input name="stub_image" type="file" style="width: 100%; overflow: hidden;"/>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" value="0" id="delete_stub_image" name="delete_stub_image"/>

                                </div>

                                <div class="well">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            {if $Item->announcement_img}
                                                <img id="announcement_img" class="image_preview"
                                                     src='{$images_uploaddir}{$Item->announcement_img}?r={math equation="rand(1,1000000)"}'
                                                     alt="" style="width: 92%;"/>
                                                <p>
                                                    <a id="button_delete_announcement_img"
                                                       class="btn btn-outline btn-danger btn-xs js-button_delete_announcement_img" type="button"
                                                       href="#">
                                                        <i class="fa fa-times-circle"></i> Удалить изображение
                                                    </a>
                                                </p>
                                            {else}
                                                <img id="announcement_img" class="image_preview" src='images/no_foto.gif' alt=""/>
                                            {/if}
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <label>Лого автора блога</label>
                                                <input name="announcement_img" type="file" style="width: 100%; overflow: hidden;"/>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" value="0" id="delete_announcement_img" name="delete_announcement_img"/>

                                </div>

                                <div class="well">
                                    <div class="row">
                                        <div class="form-group">
                                            <label>Ссылка с изображения в шапке</label>
                                            <input name="stub_link" type="text" style="width: 100%;" value='{$Item->stub_link}'/>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="row" style="display: none;">
                            <div class="col-lg-12">
                                <div class="well">
                                    <div class="form-group">
                                        {*<label>Текст статьи</label>*}
                                        <div id="reactEditor_writerblog_lead"></div>
                                        {* <textarea rows="20" id="body" name="body" class="form-control fulleditor">{$Item->body}</textarea> *}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-heading">
                    {$Error}&nbsp;
                    <div class="pull-right">
                        <div class="btn-group">
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="apply" value="1">
                                <i class="fa fa-check"></i> Применить
                            </button>
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save">
                                <i class="fa fa-save"></i> Сохранить
                            </button>
                            <a class="btn btn-outline btn-warning btn-xs" type="button"
                               href="index.php?section=BlogWriters&token={$Token}">
                                <i class="fa fa-ban"></i> Отменить
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</form>

{include file='tinymce_init.tpl'}
{literal}
    <script>
        var url_touched = true;
        var item_form = document.form;
        // generating translit
        function generate_url(name) {
            return translit(name);
        }

        // Attach events
        function myattachevent(target, eventName, func) {
            if (target.addEventListener)
                target.addEventListener(eventName, func, false);
            else if (target.attachEvent)
                target.attachEvent("on" + eventName, func);
            else
                target["on" + eventName] = func;
        }

        if (window.attachEvent) {
            window.attachEvent("onload", function () {
                setTimeout("autourlgeneration_init();", 1000);
            });
        } else if (window.addEventListener) {
            window.addEventListener("DOMContentLoaded", autourlgeneration_init, false);
        } else {
            document.addEventListener("DOMContentLoaded", autourlgeneration_init, false);
        }
        function autourlgeneration_init() {

            myattachevent(item_form.tag_url, 'change', function () {
                url_touched = true
            });

            myattachevent(item_form.name, 'keyup', set_url);
            myattachevent(item_form.name, 'change', set_url);

            if (item_form.tag_url.value == '' || item_form.tag_url.value == generate_url(name)) {
                url_touched = false;
            }
        }
        function set_url() {
            var name = item_form.name.value.replace(/(<([^>]+)>)/ig, " ").replace(/(\&nbsp;)/ig, " ");

            if (name !='' ){
                $("#name_label").removeClass('text-danger');
                $("#name_label").addClass('text-success');

                if ($("input[name=tag_url]") != ''){
                    $("#tag_url_label").removeClass('text-danger');
                    $("#tag_url_label").addClass('text-success');
                }
                else{
                    $("#url_label").removeClass('text-success');
                    $("#tag_url_label").addClass('text-danger');
                }

            }
            else{
                $("#name_label").removeClass('text-success');
                $("#name_label").addClass('text-danger');

                if ($("input[name=tag_url]").val() == ''){
                    $("#tag_url_label").removeClass('text-success');
                    $("#tag_url_label").addClass('text-danger');
                }
                else{
                    $("#tag_url_label").removeClass('text-danger');
                    $("#tag_url_label").addClass('text-success');
                }
            }

            name = name.trim();
            name = name.replace(/<\/?[^>]+>/g,'');
            name = name.replace('&laquo;', '"');
            name = name.replace('&laquo;', '"');
            name = name.replace('&raquo;', '"');
            name = name.replace('&mdash;', '-');
            name = name.replace('&ndash;', '-');
            name = name.replace('&amp;', '');
            name = name.replace('&reg;', '');
            name = name.replace('&copy;', '');
            name = name.replace('&quot;', '');
            name = name.replace('&lt;', '');
            name = name.replace('&gt;', '');
            name = name.replace('&nbsp;', '');

            // Url
            if (!url_touched)
                item_form.tag_url.value = generate_url(name);
        }

    </script>
{/literal}
