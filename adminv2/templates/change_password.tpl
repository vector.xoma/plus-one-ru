<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">
            {$title}
        </h2>
    </div>
</div>
<div class="row">
    <!-- /-->
</div>

<form name="blogpost" method="post" action="index.php?section=Login&action=changepassword" class="col-lg-12">
    <div class="well well-sm col-lg-3 col-lg-offset-4">

        {if $error}
            <p class="text-danger">{$error}</p>
        {else}
            <p>&nbsp;</p>
        {/if}

        <div class="form-group">
            <label>Старый пароль</label>
            <input name="oldpassword" type="password" value='' placeholder="Укажите старый пароль" class="form-control" autocomplete="off" />
        </div>

        <div class="form-group">
            <label>Новый пароль</label>
            <input name="newpassword" type="password" value='' placeholder="Укажите новый пароль" class="form-control" autocomplete="off" />
        </div>

        <div class="form-group">
            <label>Новый пароль еще раз</label>
            <input name="newpasswordre" type="password" value='' placeholder="Укажите новый пароль еще раз" class="form-control" autocomplete="off" />
        </div>

        <button type="submit" class="btn btn-default btn-success btn-block">Сменить пароль</button>
    </div>
</form>
