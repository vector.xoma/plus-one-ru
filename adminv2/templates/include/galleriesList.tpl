<script src="../bootstrap/bower_components/jquery/dist/jquery.min.js"></script>
{literal}
<script type="text/javascript" language="javascript">
    $(document).on("click","div.gallery",function(){
        galleryId = $(this).data("gallery-id");

        $.ajax({
            url: "/adminv2/ajax/makeFotoramaContent.php",
            data: {galleryId:galleryId},
            type: "GET",
//            dataType: 'json',
            success: function(data)
            {
                top.tinymce.activeEditor.insertContent(data);
                top.tinymce.activeEditor.windowManager.close();
            },
            error: function (file, response) {
                top.tinymce.activeEditor.insertContent(response);
                top.tinymce.activeEditor.windowManager.close();
            }
        });
    });
</script>
{/literal}

    {foreach from=$galeriesList item=gallery name=gallery}
        <div class="col-lg-12">
            <div class="well">
                <div class="row">
                    <div class="col-lg-12 gallery" data-gallery-id="{$gallery->id}">
                        <p>{$gallery->name|escape}</p>
                    </div>
                </div>
            </div>
        </div>
    {/foreach}
