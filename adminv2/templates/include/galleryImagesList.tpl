
<div class="fotorama-holder" contenteditable="false">
    <a href="#" class="prev"></a>
    <a href="#" class="next"></a>
    <div class="fotorama" 
        data-auto="true"
        data-maxwidth="996"
        data-maxheight="525"
        data-arrows="true"
        data-click="true"
        data-swipe="true"
        data-navwidth="755"
        
    >
        {foreach from=$imagesList item=image name=image}
        <div data-img="/files/photogallerys/{$image->filename}" data-thumb="/files/photogallerys/{$image->filename}">
            <p>
                {$image->image_name|escape}
                <span>{$image->image_author|escape}</span>
            </p>
        </div>
        {/foreach}
    </div>
    <div class="info">
        <span class="numbering">1 / {$imagesList|@count}</span>
        <p>
             {$imagesList[0]->image_name|escape}
            <span>{$imagesList[0]->image_author|escape}</span>
        </p>
    </div>
    {* {literal}
        <script>
            $(function () {
                $('.fotorama').fotorama({
                    nav: "thumbs",
                    width: '100%',
                    // height: '100%',
                    // width: '100%',
                    // height: '100%',
                    fit: "scaledown",
                    loop: true,
                    margin: 0,
                    arrows: true,
                    thumbwidth: 88,
                    thumbheight: 70,
                    thumbmargin: 8,
                    allowfullscreen: true,
                    ratio: '800/600'
                });
                $(".fotorama-holder .next").click(function(event) {
                    
                    event.preventDefault();
                    var fotorama = $(this)
                    .closest(".fotorama-holder")
                    .find(".fotorama")
                    .data("fotorama");
                    fotorama.show(">");
                    $('.fotorama').fotorama() = null;
                });
                $(".fotorama-holder .prev").click(function(event) {
                    
                    event.preventDefault();
                    var fotorama = $(this)
                    .closest(".fotorama-holder")
                    .find(".fotorama")
                    .data("fotorama");
                    fotorama.show("<");
                    $('.fotorama').fotorama() = null;
                });
            });
        </script>
    {/literal} *}
</div>
<br>
