{if $linkedPosts}
<table class="table">
    {foreach from=$linkedPosts item=linkedPost name=linkedPost}
        <tr>
            <td style="width: 10%;">
                <img src="{$linkedPost->image_1_3}" style="width: 80px;" />
            </td>
            <td style="width: 75%;">
                {$linkedPost->name}
            </td>
            <td style="width: 15%;">
                <div class="btn-group">
                    <a href="#" type="button" post_id="{$linkedPost->id}" post_type="{$linkedPost->post_type}" onClick="return false;" class="btn btn-success btn-xs unlink_to_linked_post">
                        <i class="fa fa-link"></i> Отвязать
                    </a>
                </div>
                <div class="text-right">
                    [{$linkedPost->postTypeStr}]
                </div>
            </td>
        </tr>
    {/foreach}
</table>
{else}
    <p>Нет связанных статей.</p>
    <p>Для привязки к текущей статье сопутствующих - выберите автора, а затем выберите необходимую статью из списка, и нажмите кнопку "Связать"</p>
{/if}