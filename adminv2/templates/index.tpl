<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <base href="{$root_url}/adminv2/" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>{$Title}</title>
    <link rel="shortcut icon" href="/adminv2/images/favicon6947.png" type="image/png">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="bootstrap/bower_components/bootstrap/dist/css/bootstrap.min.css?{$Config->admin_cache_version}" rel="stylesheet" />
    <link href="bootstrap/bower_components/bootstrap/dist/css/style.css?{$Config->admin_cache_version}" rel="stylesheet" />
    <link href="bootstrap/bower_components/metisMenu/dist/metisMenu.min.css?{$Config->admin_cache_version}" rel="stylesheet" />

    <!--<link href="bootstrap/dist/css/timeline.css" rel="stylesheet">/-->

    <link href="bootstrap/dist/css/sb-admin-2.css?{$Config->admin_cache_version}" rel="stylesheet" />
    <link href="bootstrap/dist/css/dropzone.css?{$Config->admin_cache_version}" rel="stylesheet" />
    <link href="bootstrap/dist/css/bootstrap-select.css?{$Config->admin_cache_version}" rel="stylesheet" />

    {*
    <link href="bootstrap/dist/css/bootstrap-datepicker.css" rel="stylesheet" />
    *}
    <link href="bootstrap/dist/css/jquery.datetimepicker.css?{$Config->admin_cache_version}" rel="stylesheet" />

    <!-- Custom Fonts -->
    <link href="bootstrap/bower_components/font-awesome/css/font-awesome.min.css?{$Config->admin_cache_version}" rel="stylesheet" type="text/css" />
    <!-- DataTables Responsive CSS -->
    <link href="bootstrap/bower_components/datatables-responsive/css/dataTables.responsive.css?{$Config->admin_cache_version}" rel="stylesheet" />

    <!-- React Editor CSS -->
    <link href="/adminv2/editor/build/static/css/2.165b2024.chunk.css" rel="stylesheet">
    <link href="/adminv2/editor/build/static/css/main.0572d226.chunk.css" rel="stylesheet">

<body>
    <div id="user_id" data-value="{$user->id}"></div>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            {include file="include_topnav.tpl"}
            <div class="navbar-default sidebar" role="navigation" style="width: 170px;">
                <div class="sidebar-nav navbar-collapse">
                    {include file="include/include_leftmenu.tpl"}
                </div>
            </div>
        </nav>
        <div id="page-wrapper" style="margin: 0 0 0 170px">
            {if $Errors}
                <div class="row">
                    <br/>
                    <div class="alert alert-danger alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <a href="{$referer}">{$referer}</a> - {$Errors}
                    </div>
                </div>
            {/if}
            {$Body}
        </div>
    </div>

    <script src="bootstrap/bower_components/jquery/dist/jquery.min.js?{$Config->admin_cache_version}"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="application/javascript"></script>
    <script src="bootstrap/bower_components/bootstrap/dist/js/bootstrap.min.js?{$Config->admin_cache_version}"></script>

    {*<script src="bootstrap/bower_components/bootstrap/js/file-input.js"></script>*}
    <script src="bootstrap/bower_components/metisMenu/dist/metisMenu.min.js?{$Config->admin_cache_version}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/r/bs/dt-1.10.20/datatables.min.js?{$Config->admin_cache_version}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.20/sorting/date-de.js?{$Config->admin_cache_version}"></script>

    {*<script src="bootstrap/dist/js/bootstrap-datepicker.js"></script>*} {*<script
        src="bootstrap/dist/js/bootstrap-datepicker.ru.min.js"></script>*}

    <script src="bootstrap/dist/js/jquery.datetimepicker.min.js?{$Config->admin_cache_version}"></script>

    <script src="bootstrap/dist/js/dropzone.js?{$Config->admin_cache_version}"></script>
    <script src="bootstrap/dist/js/bootstrap-select.js?{$Config->admin_cache_version}"></script>
    <script src="bootstrap/dist/js/sb-admin-2.js?{$Config->admin_cache_version}"></script>

    <script src="../design/plusone_v2/js/jquery.plugins.js?{$Config->admin_cache_version}"></script>
{literal}
    <script>
      !(function(a) {
        function e(e) {
          for (
            var r, t, n = e[0], o = e[1], u = e[2], i = 0, l = [];
            i < n.length;
            i++
          )
            (t = n[i]),
              Object.prototype.hasOwnProperty.call(p, t) &&
                p[t] &&
                l.push(p[t][0]),
              (p[t] = 0);
          for (r in o)
            Object.prototype.hasOwnProperty.call(o, r) && (a[r] = o[r]);
          for (s && s(e); l.length; ) l.shift()();
          return c.push.apply(c, u || []), f();
        }
        function f() {
          for (var e, r = 0; r < c.length; r++) {
            for (var t = c[r], n = !0, o = 1; o < t.length; o++) {
              var u = t[o];
              0 !== p[u] && (n = !1);
            }
            n && (c.splice(r--, 1), (e = i((i.s = t[0]))));
          }
          return e;
        }
        var t = {},
          p = { 1: 0 },
          c = [];
        function i(e) {
          if (t[e]) return t[e].exports;
          var r = (t[e] = { i: e, l: !1, exports: {} });
          return a[e].call(r.exports, r, r.exports, i), (r.l = !0), r.exports;
        }
        (i.m = a),
          (i.c = t),
          (i.d = function(e, r, t) {
            i.o(e, r) ||
              Object.defineProperty(e, r, { enumerable: !0, get: t });
          }),
          (i.r = function(e) {
            "undefined" != typeof Symbol &&
              Symbol.toStringTag &&
              Object.defineProperty(e, Symbol.toStringTag, { value: "Module" }),
              Object.defineProperty(e, "__esModule", { value: !0 });
          }),
          (i.t = function(r, e) {
            if ((1 & e && (r = i(r)), 8 & e)) return r;
            if (4 & e && "object" == typeof r && r && r.__esModule) return r;
            var t = Object.create(null);
            if (
              (i.r(t),
              Object.defineProperty(t, "default", { enumerable: !0, value: r }),
              2 & e && "string" != typeof r)
            )
              for (var n in r)
                i.d(
                  t,
                  n,
                  function(e) {
                    return r[e];
                  }.bind(null, n)
                );
            return t;
          }),
          (i.n = function(e) {
            var r =
              e && e.__esModule
                ? function() {
                    return e.default;
                  }
                : function() {
                    return e;
                  };
            return i.d(r, "a", r), r;
          }),
          (i.o = function(e, r) {
            return Object.prototype.hasOwnProperty.call(e, r);
          }),
          (i.p = "editor/build/");
        var r = (this.webpackJsonpdemo = this.webpackJsonpdemo || []),
          n = r.push.bind(r);
        (r.push = e), (r = r.slice());
        for (var o = 0; o < r.length; o++) e(r[o]);
        var s = n;
        f();
      })([]);
    </script>
{/literal}

    <script src="/adminv2/editor/build/static/js/2.3c63b89f.chunk.js"></script>
    <script src="/adminv2/editor/build/static/js/main.95a165c9.chunk.js"></script>
</body>

</html>
