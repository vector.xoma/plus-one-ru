<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">
            {$title}
        </h2>
    </div>
</div>
<div class="row">
    <!-- /-->
</div>
{if $Error}
    <div class="row">
        <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {$Error}
        </div>
    </div>
{/if}
{if $Errors}
    {foreach item=err from=$Errors name=err}
        <div class="row">
            <div class="alert alert-danger alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {$err}
            </div>
        </div>
    {/foreach}
{/if}

{literal}
    <script>
        window.EditorNewspostLead = `{/literal}{$Item->lead|escape|replace:'`':'\`'}{literal}`;
        window.EditorNewspostBody = `{/literal}{$Item->article_text|escape|replace:'`':'\`'}{literal}`;
    </script>
{/literal}

<form name="blogpost" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    &nbsp;
                    <div class="pull-right">
                        <div class="btn-group">
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="apply">
                                <i class="fa fa-check"></i> Применить
                            </button>
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="preview">
                                <i class="fa fa-check"></i> Предпросмотр
                            </button>
                            {if $preview}
                                <script>
                                    window.open('{$root_url}{$articleUrl}?mode=preview', '_blank');
                                </script>
                            {/if}
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="save">
                                <i class="fa fa-check"></i> Сохранить
                            </button>
                            <a class="btn btn-outline btn-warning btn-xs" type="button"
                               href="index.php?section=NewsItems&token={$Token}">
                                <i class="fa fa-ban"></i> Отменить
                            </a>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="tab-content">

                        {*{include file="include/seoContent.tpl"}*}

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="well">
                                    <div class="form-group {if !$Item->header}has-warning{/if}">
                                        <label>Заголовок</label>
                                        {*<input id="name" name="name" type="text" value='{$Item->header|escape}'*}
                                               {*placeholder="Укажите заголовок" class="form-control">*}
                                        <textarea name="name" class="form-control smalleditor">{$Item->header}</textarea>
                                    </div>

                                    <div class="form-group {if !$Item->header_social}has-warning{/if}">
                                        <label>Заголовок для социальных сетей (max 140 символов)</label>
                                        <input id="header_rss_input" name="header_rss" type="text" value='{$Item->header_social|escape}'
                                               placeholder="Укажите заголовок для соц. сетей" class="form-control">
                                    </div>

                                    <div class="form-group {if !$Item->date_created}has-warning{/if}">
                                        <label id="created_label">
                                            Дата
                                        </label>
                                        <input id="calendar" name="created" type="text"
                                               value='{$Item->date_created|escape}'
                                               placeholder="Укажите дату записи" class="form-control">
                                    </div>


                                    <div class="form-group">
                                        <label id="writers_label" {if !$Item->writers}class="text-danger"{/if}>
                                            Автор, от чьего имени будет опубликована статья
                                        </label>
                                        <select class="form-control" id="writers_input" name="writer">
                                            <option value="0"> -- Выберите автора -- </option>
                                            {foreach item=writer from=$writers name=writer}
                                                <option value="{$writer->id}" {if $Item->writers==$writer->id}selected{/if}>{$writer->name|escape}</option>
                                            {/foreach}
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="enabled" value="1"
                                                       {if $Item->enabled==1}checked{/if} /> Отображать на сайте
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="publish_news_aggregators" value="1"
                                                       {if $Item->publish_news_aggregators==1}checked{/if} /> Публикация в новостных агрегаторах
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="publish_news_zen_pulse" value="1"
                                                       {if $Item->publish_news_zen_pulse==1}checked{/if} /> Публикация в Дзен/Пульс
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="is_partner_material" value="1"
                                                       {if $Item->is_partner_material==1}checked{/if} /> Партнерский материал
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="well">
                                    <h3>Подпись</h3>
                                    <div class="form-group {if !$Item->author_signature}has-warning{/if}">
                                        <label>Подпись (Автор, Беседовал, etc)</label>
                                        <input name="author_signature" type="text" value="{if $Item->author_signature}{$Item->author_signature|escape}{else}Автор{/if}"
                                               placeholder="" class="form-control">
                                    </div>

                                    <div class="form-group {if !$Item->author_name}has-warning{/if}">
                                        <label>Имя</label>
                                        <input name="author_name" type="text" value='{if $Item->author_name}{$Item->author_name|escape}{else}{$currentUserName}{/if}'
                                               placeholder="" class="form-control">
                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-6">
                                {include file="include/tagFirstLevel.tpl"}

                                <div class="well">
                                    {include file="include/tagsPost.tpl"}
                                </div>


                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="well">
                                            <div class="form-group">
                                                <label id="url_label" {if !$Item->url}class="text-danger"{/if}>
                                                    URL записи
                                                </label>
                                                <input name="url" type="text" value='{$Item->url|escape}'
                                                       placeholder="Укажите URL записи" class="form-control">
                                            </div>
                                            {include file="include/_meta_tags.tpl"}
                                        </div>
                                    </div>
                                </div>

                                {include file='include/format_post.tpl'}

                            </div>
                        </div>


                        <div class="row" style="margin-bottom: 20px;">
                            <div class="col-lg-12">
                                <div class="well col-lg-2" style="margin: 0 2% 0 0;">
                                    <h4>Основная иллюстрация</h4>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            {if $Item->image_rss}
                                                <img id="image_rss" class="image_preview" src='{$images_uploaddir}{$Item->image_rss}' alt="" style="width: 100%;  height: 100%" />
                                            {else}
                                                <img id="image_rss" class="image_preview" src='images/no_foto.gif' alt=""/>
                                            {/if}
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Изображение</label>
                                                <input name="image_rss" type="file" style="width: 100%; overflow: hidden;"/>
                                            </div>
                                        </div>
                                        {if $Item->origin_image}
                                            <div class="form-group">
                                                <label>Оригинальное изображение</label>
                                                <h5>https://{$smarty.server.HTTP_HOST}/files/news/{$Item->origin_image|escape}</h5>
                                            </div>
                                        {/if}
                                        <div class="form-group">
                                                <label>Описание изображения</label>
                                                <input name="image_rss_description" type="text" value="{if $Item->image_rss_description}{$Item->image_rss_description|escape}{/if}"
                                                       placeholder="" class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label>Источник изображения</label>
                                                <input name="image_rss_source" type="text" value='{if $Item->image_rss_source}{$Item->image_rss_source|escape}{/if}'
                                                       placeholder="" class="form-control">
                                            </div>
                                    </div>
                                </div>


                                <div class="well col-lg-2" style="margin: 0 2% 0 0;">
                                    <h4>Блок 1/1</h4>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            {if $Item->image_1_1}
                                                <img id="image_1_1" class="image_preview" src='{$images_uploaddir}{$Item->image_1_1}' alt="" style="width: 100%; height: 100%" />
                                            {else}
                                                <img id="image_1_1" class="image_preview" src='images/no_foto.gif' alt=""/>
                                            {/if}
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Изображение</label>
                                                <input name="image_1_1" type="file" class="upload_button" data-imagetype = "1_1" style="width: 100%; overflow: hidden;"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <div><label>Вид анонса</label></div>
                                                <label class="radio-inline">
                                                    <input type="radio" value="text" {if $Item->type_announce_1_1 == "text" || $Item->type_announce_1_1 == ""}checked="checked"{/if} name="type_announce_1_1"> Текст
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" id="1_1_picture" value="picture" {if $Item->type_announce_1_1 == "picture"}checked="checked"{/if} name="type_announce_1_1"> Иллюстрация
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="well col-lg-2" style="margin: 0 2% 0 0;">
                                    <h4>Блок 1/2</h4>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            {if $Item->image_1_2}
                                                <img id="image_1_2" class="image_preview" src='{$images_uploaddir}{$Item->image_1_2}' alt="" style="width: 100%;  height: 100%"/>
                                            {else}
                                                <img id="image_1_2" class="image_preview" src='images/no_foto.gif' alt=""/>
                                            {/if}
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Изображение</label>
                                                <input name="image_1_2" type="file" class="upload_button" data-imagetype = "1_2" style="width: 100%; overflow: hidden;"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <div><label>Вид анонса</label></div>
                                                <label class="radio-inline">
                                                    <input type="radio" value="text" {if $Item->type_announce_1_2 == "text" || $Item->type_announce_1_2 == ""}checked="checked"{/if} name="type_announce_1_2"> Текст
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" id="1_2_picture" value="picture" {if $Item->type_announce_1_2 == "picture"}checked="checked"{/if} name="type_announce_1_2"> Иллюстрация
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="well col-lg-2" style="margin: 0 2% 0 0;">
                                    <h4>Блок 1/3</h4>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            {if $Item->image_1_3}
                                                <img id="image_1_3" class="image_preview" src='{$images_uploaddir}{$Item->image_1_3}' alt="" style="width: 100%;  height: 100%" />
                                            {else}
                                                <img id="image_1_3" class="image_preview" src='images/no_foto.gif' alt=""/>
                                            {/if}
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Изображение</label>
                                                <input name="image_1_3" type="file" class="upload_button" data-imagetype = "1_3" style="width: 100%; overflow: hidden;"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <div><label>Вид анонса</label></div>
                                                <label class="radio-inline">
                                                    <input type="radio" value="text" {if $Item->type_announce_1_3 == "text" || $Item->type_announce_1_3 == ""}checked="checked"{/if} name="type_announce_1_3"> Текст
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" id="1_3_picture" value="picture" {if $Item->type_announce_1_3 == "picture"}checked="checked"{/if} name="type_announce_1_3"> Иллюстрация
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>



                        <div class="row">
                            <div class="col-lg-12">

                                <div>
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active">
                                            <a href="#text-page" aria-controls="text-page" role="tab" data-toggle="tab">
                                                Текстовое наполнение
                                            </a>
                                        </li>
                                        <li role="presentation">
                                            <a href="#partner-links" aria-controls="partner-links" role="tab" data-toggle="tab">
                                                Партнерские ссылки
                                            </a>
                                        </li>
                                        {if $Item->id}
                                            <li role="presentation">
                                                <a href="#linked-post" aria-controls="linked-post" role="tab" data-toggle="tab">
                                                    Связанные статьи
                                                </a>
                                            </li>
                                        {else}
                                            <li role="presentation" class="disabled">
                                                <a href="#linked-post" aria-controls="linked-post" role="tab" onclick="return false;">
                                                    Связанные статьи (сначала сохраните статью)
                                                </a>
                                            </li>
                                        {/if}
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content" style="min-height: 500px;">
                                        <div role="tabpanel" class="tab-pane active" id="text-page">
                                            <div class="well">
                                                <div class="form-group">
                                                    <label for="text_content">Текст подзаголовка</label>
                                                     <textarea rows="5" name="header"  id="text_content" class="form-control fulleditor">{$Item->text_content}</textarea>
{*                                                    <div id="reactEditor_newspost_header"></div>*}
                                                </div>
                                            </div>

                                            <div class="well">
                                                <div class="form-group">
                                                    <label id="lead_label">Лид</label>
                                                    <div id="reactEditor_newspost_lead"></div>
                                                    {* <textarea rows="20" name="lead" id="lead" class="form-control fulleditor" placeholder="Лид"> *}
                                                        {* {if $Item->lead}{$Item->lead}{else}<p><strong>&nbsp;</strong></p>{/if} *}
                                                    {* </textarea> *}
                                                </div>
                                            </div>

                                            <div class="well">
                                                <div class="form-group">
                                                    <label>Текст статьи</label>
                                                    <div id="reactEditor_newspost_body"></div>
                                                    {* <textarea rows="20" name="article_text" id="article_text" class="form-control fulleditor">{$Item->article_text}</textarea> *}
                                                </div>
                                            </div>
                                        </div>

                                        {* партнерские ссылки *}
                                        <div role="tabpanel" class="tab-pane" id="partner-links">
                                            {include file="include/partnersLinks.tpl"}
                                        </div>

                                        {* связанные статьи *}
                                        <div role="tabpanel" class="tab-pane" id="linked-post">
                                            <div class="well" style="background: #fff">
                                                <div class="row">

                                                    <div class="col-lg-7">
                                                        <table id="relatedPostDataTable" class="table order-column">
                                                            <thead>
                                                            <tr>
                                                                <th style="width: 100px; cursor: pointer">Создана</th>
                                                                {*<th style="width: 35%; cursor: pointer">Изображение</th>*}
                                                                <th style="width: 100px; cursor: pointer">Теги</th>
                                                                <th style="width: 100px; cursor: pointer">Автор</th>
                                                                <th style="width: 100px; cursor: pointer">Партнер</th>
                                                                <th style="width: auto; cursor: pointer">Название</th>
                                                                <th style="width: 100px; cursor: pointer"></th>
                                                            </tr>
                                                            </thead>
                                                        </table>
                                                    </div>

                                                    <div class="col-lg-5">
                                                        <div class="well">
                                                            <div class="form-group">
                                                                <label>Связанные статьи</label>
                                                            </div>

                                                            <div id="already_linked_posts">
                                                                {include file="include/include_already_linked_posts.tpl"}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="panel-heading">
                    {$Error}&nbsp;
                    <div class="pull-right">
                        <div class="btn-group">
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="apply">
                                <i class="fa fa-check"></i> Применить
                            </button>
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="preview">
                                <i class="fa fa-check"></i> Предпросмотр
                            </button>
                            <button class="btn btn-outline btn-primary btn-xs" type="submit">
                                <i class="fa fa-check"></i> Сохранить
                            </button>
                            <a class="btn btn-outline btn-warning btn-xs" type="button"
                               href="index.php?section=NewsItems&token={$Token}">
                                <i class="fa fa-ban"></i> Отменить
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <input type="hidden" name="type_visual_block" value="news">
    <input type="hidden" name="item_id" id="item_id" value="{$Item->id}">
    <input type="hidden" name="form_id" value="{$Item->form_id}">
</form>

{*{include file='tinymce_init.tpl'}*}
{include file='include/news/_set_meta.tpl'}
