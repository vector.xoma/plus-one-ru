<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">
            <small>{$title|escape}</small>
        </h2>
    </div>
</div>
<div class="row">
    <!-- /-->
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                &nbsp;
                <div class="pull-right">
                    <div class="btn-group">
                        <a class="btn btn-primary btn-xs" type="button" href="index.php?section=NewsItem&token={$Token}">
                            <i class="fa fa-check"></i> Добавить
                        </a>
                    </div>
                </div>
            </div>
            <div class="panel-body">

                <div class="row">
                    <div class="col-lg-8">
                        <form method="get" action="index.php?section=NewsItems" class="form-horizontal row" style="padding-top: 20px;">
                            <input type="hidden" class="form-control" value="NewsItems" name="section">
                            <div class="col-xs-6" >
                                <input type="text" class="form-control pull-left"  placeholder="Поиск" value="{$search}" name="search"/>
                            </div>
                            <div class="col-xs-1">
                                <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-4 text-right">
                        <ul class="pagination">
                            {if !$paginationStructure->startPageLink}
                                <li class="paginate_button previous disabled" tabindex="0">
                                    <a href="#" onclick="return false">В начало</a>
                                </li>
                            {else}
                                <li class="paginate_button previous" tabindex="0">
                                    <a href="{$paginationStructure->startPageLink}">В начало</a>
                                </li>
                            {/if}

                            {if $paginationStructure->page3left}
                                <li class="paginate_button" tabindex="0">
                                    <a href="{$paginationStructure->page3left}">{$page-3}</a>
                                </li>
                            {/if}
                            {if $paginationStructure->page2left}
                                <li class="paginate_button" tabindex="0">
                                    <a href="{$paginationStructure->page2left}">{$page-2}</a>
                                </li>
                            {/if}
                            {if $paginationStructure->page1left}
                                <li class="paginate_button" tabindex="0">
                                    <a href="{$paginationStructure->page1left}">{$page-1}</a>
                                </li>
                            {/if}

                            <li class="paginate_button active" tabindex="0">
                                <a href="#" onclick="return false">{$page}</a>
                            </li>

                            {if $paginationStructure->page1right}
                                <li class="paginate_button" tabindex="0">
                                    <a href="{$paginationStructure->page1right}">{$page+1}</a>
                                </li>
                            {/if}
                            {if $paginationStructure->page2right}
                                <li class="paginate_button" tabindex="0">
                                    <a href="{$paginationStructure->page2right}">{$page+2}</a>
                                </li>
                            {/if}
                            {if $paginationStructure->page3right}
                                <li class="paginate_button" tabindex="0">
                                    <a href="{$paginationStructure->page3right}">{$page+3}</a>
                                </li>
                            {/if}

                            {if !$paginationStructure->lastPageLink}
                                <li class="paginate_button next disabled" tabindex="0">
                                    <a href="#" onclick="return false">Последняя</a>
                                </li>
                            {else}
                                <li class="paginate_button next" tabindex="0">
                                    <a href="{$paginationStructure->lastPageLink}">Последняя</a>
                                </li>
                            {/if}
                        </ul>
                    </div>
                </div>
                {if $Items}
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Показывать</th>
                                <th>Дата публикации</th>
                                <th>RSS</th>
                                <th>Заголовок</th>
                                <th>Автор</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            {foreach item=item from=$Items name=item}
                                <tr class="{if $item->useplatforma == 1}useplatforma{/if} {if $item->deferred}deferred{/if}">
                                    <td nowrap>
                                        <a href="index.php{$item->enable_get}">
                                            <i class="fa fa-toggle-{if $item->enabled}on{else}off{/if} fa-1x"></i>
                                        </a><br/>
                                        <a href="{$item->previewUrl}" target="_blank">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                    </td>
                                    <td>
                                        <p {if !$item->enabled}class="text-muted"{/if}>
                                            <a href="index.php{$item->edit_get}">{$item->date_created|escape}</a>
                                        </p>
                                    </td>
                                    <td width="13%">
                                        <div style="width:80px; height: 50px; display: table-cell; vertical-align: middle; text-align: center;">
                                            <a href="index.php{$item->edit_get}">{$item->image}</a>
                                        </div>
                                    </td>
                                    <td width="45%">
                                        <p {if !$item->enabled}class="text-muted"{/if}>
                                            <a href="index.php{$item->edit_get}">{$item->header|strip_tags}</a>
                                        </p>
                                    </td>
                                    <td>
                                        <p {if !$item->enabled}class="text-muted"{/if}>
                                            <a href="index.php{$item->edit_get}">{$item->author_name|escape}</a>
                                        </p>
                                    </td>
                                    <td nowrap>
                                        <a href="index.php{$item->edit_get}" class="btn btn-success btn-xs"
                                           type="button">
                                            <i class="fa fa-pencil fa-1x"></i>
                                        </a>
                                        <a href="index.php{$item->delete_get}" class="btn btn-danger btn-xs"
                                           type="button"
                                           onclick='if(!confirm("Удалить запись?")) return false;'>
                                            <i class="fa fa-times-circle fa-1x"></i>
                                        </a>
                                    </td>
                                </tr>
                            {/foreach}
                            </tbody>
                        </table>

                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <p align="center">{$PagesNavigation}</p>
                        </div>
                    </div>
                {else}
                    <p>
                        Еще нет ни одной статьи. <a href="index.php?section=NewsItem&token={$Token}">Добавить новость?</a>
                    </p>
                {/if}
            </div>
        </div>
    </div>
</div>
