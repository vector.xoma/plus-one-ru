{literal}
    <script>
        window.EditorPlotLead = `{/literal}{$Item->lead|escape}{literal}`;
    </script>
{/literal}

{if $Item->group_items}
<script type="application/javascript">
    window._existsContents = {$Item->group_items};
</script>
{/if}

{if $Errors}
    {foreach item=err from=$Errors name=err}
        <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-danger alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {$err}
            </div>
        </div>
        </div>
    {/foreach}
{/if}
<br/>
<form name="blogpost" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {$title}
                    <div class="pull-right">
                        <div class="btn-group">
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="apply">
                                <i class="fa fa-check"></i> Применить
                            </button>
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="save">
                                <i class="fa fa-check"></i> Сохранить
                            </button>
                            <a class="btn btn-outline btn-warning btn-xs" type="button"
                               href="groups?token={$Token}">
                                <i class="fa fa-ban"></i> Отменить
                            </a>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="well">
                                    <div class="form-group {if !$Item->name}has-warning{/if}">
                                        <label>Название</label>
                                        <input name="name"  type="text" value="{$Item->name}" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="enabled" value="1" {if $Item->enabled==1}checked{/if} />
                                                Отображать на сайте
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label id="header_rss_label" {if !$Item->header_rss}class="text-danger"{/if}>Заголовок для социальных сетей (max 140 символов) </label>
                                        <input id="header_rss_input" name="header_rss" type="text" value='{$Item->header_rss|escape}' maxlength="140" placeholder="Укажите заголовок для RSS" class="form-control">
                                        {*<textarea name="header_rss" class="form-control smalleditor">{$Item->header_rss}</textarea>*}
                                    </div>

                                    <div class="form-group">
                                        <label id="created_label" {if !$Item->date_created}class="text-danger"{/if}>Дата</label>
                                        <input id="calendar" name="created" type="text" value='{$Item->date_created|escape}' placeholder="Укажите дату записи" class="form-control">
                                    </div>
                                </div>

                                <div class="well">
                                    <div class="form-group">
                                        <label id="header_label">Лид</label>
                                        {* <textarea rows="20" name="lead" id="lead" class="form-control fulleditor">{$Item->lead}</textarea> *}
                                        <div id="reactEditor_plot_lead"></div>
                                    </div>
                                </div>

                                <div class="well">
                                    <div class="autocomplete-block">
                                        <input type="text" name="query"  class="form-control js_search_posts" placeholder="Поиск поста (новости)....">
                                    </div>
                                </div>
                            </div>


                            <div class="col-lg-7">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="well">
                                            <div class="form-group">
                                                <label id="url_label" {if !$Item->url}class="text-danger"{/if}>
                                                    URL записи
                                                </label>
                                                <input name="url" type="text" value='{$Item->url|escape}'
                                                       placeholder="Укажите URL записи" class="form-control">
                                            </div>
                                            {include file="include/_meta_tags.tpl"}
                                        </div>


                                        <div class="well">
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    {if $Item->image}
                                                        <img id="large_image" class="image_preview"
                                                             src='{$images_uploaddir}{$Item->image}?r={math equation="rand(1,1000000)"}'
                                                             alt="" style="width: 92%;"/>
                                                        <p>
                                                            <a id="button_delete_largeimage"
                                                               class="btn btn-outline btn-danger btn-xs js-button_delete_largeimage" type="button"
                                                               href="#">
                                                                <i class="fa fa-times-circle"></i> Удалить изображение
                                                            </a>
                                                        </p>
                                                    {else}
                                                        <img id="large_image" class="image_preview" src='images/no_foto.gif' alt=""/>
                                                    {/if}
                                                </div>
                                                <div class="col-lg-9">
                                                    <div class="form-group">
                                                        <label>Изображение для шапки desktop</label>
                                                        <input name="large_image" type="file" style="width: 100%; overflow: hidden;"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" value="0" id="delete_large_image" name="delete_large_image"/>
                                        </div>


                                        <div class="well">
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    {if $Item->stub_image}
                                                        <img id="stub_image" class="image_preview"
                                                             src='{$images_uploaddir}{$Item->stub_image}?r={math equation="rand(1,1000000)"}'
                                                             alt="" style="width: 92%;"/>
                                                        <p>
                                                            <a id="button_delete_stub_image"
                                                               class="btn btn-outline btn-danger btn-xs js-button_delete_stub_image" type="button"
                                                               href="#">
                                                                <i class="fa fa-times-circle"></i> Удалить изображение
                                                            </a>
                                                        </p>
                                                    {else}
                                                        <img id="stub_image" class="image_preview" src='images/no_foto.gif' alt=""/>
                                                    {/if}
                                                </div>
                                                <div class="col-lg-9">
                                                    <div class="form-group">
                                                        <label>Изображение для шапки mobile</label>
                                                        <input name="stub_image" type="file" style="width: 100%; overflow: hidden;"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" value="0" id="delete_stub_image" name="delete_stub_image"/>

                                        </div>

                                    </div>
                                </div>

                                <div class="well">
                                    <label class="{if $Errors->group_items} text-danger{/if}">Связанные посты: <strong class="js_group_items_counter"></strong></label>
                                    <div id="group_items" {if !$Errors}data-group_id="{$Item->id}" {/if}>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel-heading">
                    &nbsp;
                    <div class="pull-right">
                        <div class="btn-group">
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="apply">
                                <i class="fa fa-check"></i> Применить
                            </button>
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="save">
                                <i class="fa fa-check"></i> Сохранить
                            </button>
                            <a class="btn btn-outline btn-warning btn-xs" type="button"
                               href="groups?token={$Token}">
                                <i class="fa fa-ban"></i> Отменить
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="type_visual_block" value="group">
    <input type="hidden" name="item_id" id="item_id" value="{$Item->id}">
    <input type="hidden" name="form_id" value="{$Item->form_id}">
</form>

{include file='tinymce_init.tpl'}
{include file='include/_set_meta.tpl'}
