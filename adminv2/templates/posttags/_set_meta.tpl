
{if $Settings->meta_autofill}
    <!-- Autogenerating meta tags -->
{literal}
    <script>

        function strip_tags( html){
            let div =  document.createElement('div');
            div.innerHTML =  html;
            return div.innerText.replace(/(\[([^\]]+)\])/ig, " ").replace(/\&shy;/gi, "") ;
        }

        // Templates
        var meta_title_template = '%name';
        var meta_keywords_template = '%name';
        var meta_description_template = '%text';

        var item_form = document.posttag;

        var meta_title_touched = true;
        var meta_keywords_touched = true;
        var meta_description_touched = true;
        var url_touched = true;
        var header_rss_touched = true;
        var name  = "";
        var header_on_page  = "";

        // generating meta_title
        function generate_title(template, name, text) {
            return template.replace('%name', name).replace('%text', text).replace(/^(,\s)+|\s+$/g, "").trim();
        }

        // generating meta_keywords
        function generate_keywords(template, name, text) {
            return template.replace('%name', name).replace('%text', text).replace(/^(,\s)+|\s+$/g, "").trim();
        }

        // generating meta_description
        function generate_description(template, name, text) {
            return template.replace('%name', name).replace('%text', text).replace(/^\s+|\s+$/g, "").trim();
        }

        // generating meta_description
        function generate_header_rss(template, name, text) {
            return template.replace('%name', name).replace('%text', text).replace(/^\s+|\s+$/g, "");
        }

        // generating url
        function generate_url(name) {
            let url = name;
            return translit(url);
        }


        // sel all metatags
        function set_meta() {
            let name = item_form.name.value.replace(/(<([^>]+)>)/ig, " ").replace(/(\&nbsp;)/ig, " ").replace(/\&shy;/gi, "") ;

            name = strip_tags(name);

            if (name !='' ){
                $("#name_label").removeClass('text-danger');
                $("#name_label").addClass('text-success');

                if ($("#header_rss_input").val() != ''){
                    $("#header_rss_label").removeClass('text-danger');
                    $("#header_rss_label").addClass('text-success');
                }
                else{
                    $("#header_rss_label").removeClass('text-success');
                    $("#header_rss_label").addClass('text-danger');
                }

                if ($("input[name=url]") != ''){
                    $("#url_label").removeClass('text-danger');
                    $("#url_label").addClass('text-success');
                }
                else{
                    $("#url_label").removeClass('text-success');
                    $("#url_label").addClass('text-danger');
                }

            }
            else{
                $("#name_label").removeClass('text-success');
                $("#name_label").addClass('text-danger');

                if ($("#header_rss_input").val() == ''){
                    $("#header_rss_label").removeClass('text-success');
                    $("#header_rss_label").addClass('text-danger');
                }
                else{
                    $("#header_rss_label").removeClass('text-danger');
                    $("#header_rss_label").addClass('text-success');
                }

                if ($("input[name=url]").val() == ''){
                    $("#url_label").removeClass('text-success');
                    $("#url_label").addClass('text-danger');
                }
                else{
                    $("#url_label").removeClass('text-danger');
                    $("#url_label").addClass('text-success');
                }
            }

            let text = '';
            if (!!item_form.header) {
                text = strip_tags(item_form.header.innerHTML);
            } else {
                text = strip_tags(item_form.lead.innerHTML);
            }
            var string_for_meta_title = name;
            var string_for_url = name;

            // Meta Title
            if (!meta_title_touched) {
                item_form.seo_title.value = generate_title(meta_title_template, string_for_meta_title, text);
                item_form.seo_title.dispatchEvent(new Event('keyup', {'bubbles': true}));
            }

            // Meta Keywords
            if (!meta_keywords_touched) {
                item_form.seo_keywords.value = generate_keywords(meta_keywords_template, name, text);
                item_form.seo_keywords.dispatchEvent(new Event('keyup', {'bubbles': true}));
            }

            // Meta Description
            if (!meta_description_touched) {
                item_form.seo_description.value = generate_description(meta_description_template, name, text);
                item_form.seo_description.dispatchEvent(new Event('keyup', {'bubbles': true}));
            }

            // Url
            if (!url_touched) {
                item_form.url.value = generate_url(string_for_url);
            }

            // Header RSS
            if (!header_rss_touched && is_quote) {
                item_form.header_rss.value = generate_header_rss(header_on_page);
                item_form.header_rss.dispatchEvent(new Event('keyup', {'bubbles': true}));
            }
        }

        function autometageneration_init() {

            if (!!item_form.header) {
                $(item_form.header).on('keyup' , function(){set_meta()});
                $(item_form.header).on('change' , function(){set_meta()});
            } else if (!!item_form.lead) {
                $(item_form.lead).on('keyup', function () {set_meta()});
                $(item_form.lead).on('change', function () {set_meta()});
            }

            item_form.name && $(item_form.name).on('change keyup', function(){set_meta()});

            var text = '';
            var string_for_meta_title = name;
            var string_for_url = name;

            if (item_form.seo_title.value == '' || item_form.seo_title.value == generate_title(meta_title_template, string_for_meta_title, text))
                meta_title_touched = false;
            if (item_form.seo_keywords.value == '' || item_form.seo_keywords.value == generate_keywords(meta_keywords_template, name, text))
                meta_keywords_touched = false;
            if (item_form.seo_description.value == '' || item_form.seo_description.value == generate_description(meta_description_template, name, text))
                meta_description_touched = false;
            if (item_form.url.value == '' || item_form.url.value == generate_url(string_for_url))
                url_touched = false;
        }

        // Attach events
        function myattachevent(target, eventName, func) {
            if (target.addEventListener)
                target.addEventListener(eventName, func, false);
            else if (target.attachEvent)
                target.attachEvent("on" + eventName, func);
            else
                target["on" + eventName] = func;
        }

        if (window.attachEvent) {
            window.attachEvent("onload", function () {
                setTimeout("autometageneration_init();", 1000)
            });
        } else if (window.addEventListener) {
            window.addEventListener("DOMContentLoaded", autometageneration_init, false);
        } else {
            document.addEventListener("DOMContentLoaded", autometageneration_init, false);
        }


        myattachevent(item_form.url, 'change', function () {
            url_touched = true
        });
        myattachevent(item_form.seo_title, 'change', function () {
            meta_title_touched = true
        });
        myattachevent(item_form.seo_keywords, 'change', function () {
            meta_keywords_touched = true
        });
        myattachevent(item_form.seo_description, 'change', function () {
            meta_description_touched = true
        });
        myattachevent(item_form.name, 'keyup', set_meta);
        myattachevent(item_form.name, 'change', set_meta);


    </script>
{/literal}
    <!-- END Autogenerating meta tags -->
{else}
{/if}
