{literal}
<script>
    window.EditorBlogpostHeader = `{/literal}{$Item->header|escape|replace:'`':'\`'}{literal}`;
    window.EditorBlogpostLead = `{/literal}{$Item->lead|escape|replace:'`':'\`'}{literal}`;
    window.EditorBlogpostBody = `{/literal}{$Item->body|escape|replace:'`':'\`'}{literal}`;
</script>

{/literal}

<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">
            {$title}
        </h2>
    </div>
</div>

{$Error}
{if $Errors}
    {foreach item=err from=$Errors name=err}
        <div class="row">
            <div class="alert alert-danger alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {$err}
            </div>
        </div>
    {/foreach}
{/if}

<form name="posttag" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="pull-right">
                        <div class="btn-group">
{*                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="apply">*}
{*                                <i class="fa fa-check"></i> Применить*}
{*                            </button>*}
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="save">
                                <i class="fa fa-save"></i> Сохранить
                            </button>
                            <a class="btn btn-outline btn-warning btn-xs" type="button"
                               href="blogpost/">
                                <i class="fa fa-ban"></i> Отменить
                            </a>
                        </div>
                    </div>
                    <br/>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="well">
                                    <div class="form-group {if !$Item->name}has-warning{/if}">
                                        <label>Название тега</label>
                                        <input name="name" type="text" value='{$Item->name|escape}'
                                               placeholder="Укажите название тега, например Энергоэффективность"
                                               class="form-control">
                                    </div>

                                    <div class="form-group {if !$Item->name}has-warning{/if}">
                                        <label>Название тега для определения лидера</label>
                                        <input name="name_lead" type="text" value='{$Item->name_lead|escape}'
                                               placeholder="Укажите название тега для определения лидера, например Лидер энергоэффективности"
                                               class="form-control">
                                    </div>

                                    <div class="form-group {if !$Item->name}has-warning{/if}">
                                        <label>Тег первого уровня <i class="fa fa-tag"></i></label>
                                        <select class="form-control" name="parent">
                                            <option value="0"> -- Выберите тег первого уровня --</option>
                                            {foreach item=parentTag from=$parentTags name=parentTag}
                                                {if $parentTag->url != 'main'}
                                                    <option value="{$parentTag->id}"
                                                            {if $Item->parent==$parentTag->id}selected{/if}>{$parentTag->name}</option>
                                                {/if}
                                            {/foreach}
                                        </select>
                                    </div>

                                    <div class="form-group {if !$Item->url}has-warning{/if}">
                                        <label>Латинское название тега (для ссылки)</label>
                                        <input name="url" type="text" value='{$Item->url|escape}'
                                               placeholder="Укажите латинское название тега (для ссылки)"
                                               class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="enabled" value="1"
                                                       {if $Item->enabled==1}checked{/if} /> Отображать на сайте
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                {include file="include/seoContent.tpl"}

                                <div class="well">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <label>Основное изображение</label>
                                            <br/>
                                            <br/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-3">
                                            {if $Item->image}
                                            <div style="text-align: center; height: 160px">
                                                <img id="large_image" class="image_preview"
                                                     src='{$images_uploaddir}{$Item->image}?r={math equation="rand(1,1000000)"}'
                                                     alt=""
                                                     style="width: 100%; height: auto; max-width: 280px; max-height: 280px;"/></div>

                                            {else}
                                                <img id="large_image" class="image_preview" src='images/no_foto.gif'
                                                     alt=""/>
                                            {/if}
                                        </div>
                                        <div class="col-lg-9">
                                            <p>
                                                <a id="button_delete_largeimage"
                                                   class="btn btn-outline btn-danger btn-xs js-button_delete_largeimage"
                                                   type="button"
                                                   href="#">
                                                    <i class="fa fa-times-circle"></i> Удалить изображение
                                                </a>
                                                <br/>
                                                <br/>
                                                <br/>
                                            </p>
                                            <div class="form-group">
                                                <input name="large_image" type="file"
                                                       style="width: 100%; overflow: hidden;"/>
                                            </div>
{*                                            <div class="form-group">*}
{*                                                <label>Описание изображения</label>*}
{*                                                <input name="image_rss_description" type="text" value="{if $Item->image_rss_description}{$Item->image_rss_description|escape}{/if}"*}
{*                                                       placeholder="" class="form-control">*}
{*                                            </div>*}

{*                                            <div class="form-group">*}
{*                                                <label>Источник изображения</label>*}
{*                                                <input name="image_rss_source" type="text" value='{if $Item->image_rss_source}{$Item->image_rss_source|escape}{/if}'*}
{*                                                       placeholder="" class="form-control">*}
{*                                            </div>*}
                                        </div>
                                    </div>

                                    <input type="hidden" value="0" id="delete_large_image" name="delete_large_image"/>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-lg-12">

                            <div>
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active">
                                        <a href="#text-page" aria-controls="text-page" role="tab" data-toggle="tab">
                                            <i class="fa fa-file-text"></i> Текстовое наполнение
                                        </a>
                                    </li>
                                    {if $Item->id}
                                        <li role="presentation">
                                            <a href="#linked-post" aria-controls="linked-post" role="tab"
                                               data-toggle="tab">
                                                <i class="fa fa-link"></i> Связанные статьи
                                            </a>
                                        </li>
                                    {else}
                                        <li role="presentation" class="disabled">
                                            <a href="#linked-post" aria-controls="linked-post" role="tab"
                                               onclick="return false;">
                                                Связанные статьи (сначала сохраните статью)
                                            </a>
                                        </li>
                                    {/if}
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content" style="min-height: 500px;">
                                    <div role="tabpanel" class="tab-pane active" id="text-page">
                                        <div class="well">
                                            <div class="form-group">
                                                <label id="header_label">Текст подзаголовка</label>
                                                <div id="reactEditor_blogpost_header"></div>
                                            </div>
                                        </div>

                                        <div class="well">
                                            <div class="form-group">
                                                <label id="header_label">Лид</label>
                                                <div id="reactEditor_blogpost_lead"></div>
                                            </div>
                                        </div>

                                        <div class="well">
                                            <div class="form-group">
                                                <label>Текст статьи</label>
                                                <div id="reactEditor_blogpost_body"></div>
                                            </div>
                                        </div>
                                    </div>

                                    {* связанные статьи *}
                                    <div role="tabpanel" class="tab-pane" id="linked-post">
                                        <div class="well" style="background: #fff;">
                                            <div class="row">

                                                <div class="col-lg-7">
                                                    <table id="relatedPostDataTable" class="table order-column">
                                                        <thead>
                                                        <tr>
                                                            <th style="width: 100px; cursor: pointer">Создана</th>
                                                            <th style="width: 100px; cursor: pointer">Теги</th>
                                                            <th style="width: 100px; cursor: pointer">Автор</th>
                                                            <th style="width: 100px; cursor: pointer">Партнер</th>
                                                            <th style="width: auto; cursor: pointer">Название</th>
                                                            <th style="width: 100px; cursor: pointer"></th>
                                                        </tr>
                                                        </thead>
                                                    </table>
                                                </div>

                                                <div class="col-lg-5">
                                                    <div class="well">
                                                        <div class="form-group">
                                                            <label>Связанные статьи</label>
                                                        </div>
                                                        <div id="already_linked_posts">
                                                            {include file="include/include_already_linked_posts.tpl"}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>


                {if $Errors}
                    {foreach item=err from=$Errors name=err}
                        <div class="row">
                            <div class="alert alert-danger alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                {$err}
                            </div>
                        </div>
                    {/foreach}
                {/if}
                <div class="panel-heading">
                    <div class="pull-right">
                        <div class="btn-group">
{*                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="apply">*}
{*                                <i class="fa fa-check"></i> Применить*}
{*                            </button>*}
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="save">
                                <i class="fa fa-save"></i> Сохранить
                            </button>
                            <a class="btn btn-outline btn-warning btn-xs" type="button"
                               href="blogpost/">
                                <i class="fa fa-ban"></i> Отменить
                            </a>
                        </div>
                    </div>
                    <br/>
                </div>
                <input type="hidden" name="item_id" id="item_id" value="{$Item->id}"/>
                <input type="hidden" name="type_visual_block" value="post_tag"/>
            </div>
        </div>
    </div>
</form>

{include file='posttags/_set_meta.tpl'}
