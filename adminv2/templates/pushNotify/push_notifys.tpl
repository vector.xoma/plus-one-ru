<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">
            <small>{$title|escape}</small>
        </h2>
    </div>
</div>
<div class="row">
    <!-- /-->
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                &nbsp;
                <div class="pull-right">
                    <div class="btn-group">
                        <a class="btn btn-primary btn-xs" type="button" href="index.php?section=PushMessage&token={$Token}">
                            <i class="fa fa-check"></i> Добавить
                        </a>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                {if $Items}
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Заголовок</th>
                                <th>Текст</th>
                                <th>Ссылка</th>
                                <th>Иконка</th>
                                <th>Дата создания</th>
                            </tr>
                            </thead>
                            <tbody>
                            {foreach item=item from=$Items name=item}
                                <tr>
                                    <td style="width: 25%;" nowrap>
                                        {$item->title|escape}
                                    </td>
                                    <td nowrap style="width: 50%;">
                                        {$item->body|escape}
                                    </td>
                                    <td nowrap style="width: 10%;">
                                        {$item->link|escape}
                                    </td>
                                    <td width="10%">
                                        {if $item->icon}
                                            <img class="img-responsive" src="{$images_uploaddir}{$item->icon}" style="width: 100px;"/>
                                        {else}
                                            <img class="img-responsive" src='images/no_foto.gif' style="width: 100px;" alt=""/>
                                        {/if}
                                    </td>
                                    <td width="5%">
                                        {$item->created|escape}
                                    </td>
                                </tr>
                            {/foreach}
                            </tbody>
                        </table>

                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <p align="center">{$PagesNavigation}</p>
                        </div>
                    </div>
                {else}
                    <p>Еще нет ни одного тега. <a href="index.php?section=PostTag&token={$Token}">Добавить?</a>
                    </p>
                {/if}
            </div>
        </div>
    </div>
</div>