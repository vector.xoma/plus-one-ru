{literal}
<script>
    window.EditorHeaderText = `{/literal}{$Item->header|escape}{literal}`;
</script>
{/literal}

<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">
            {$title}
        </h2>
    </div>
</div>

<form name="form" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-12">
            {if $Errors}
                {foreach item=err from=$Errors name=err}
                    <div class="row">
                        <div class="alert alert-danger alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            {$err}
                        </div>
                    </div>
                {/foreach}
            {/if}
            {if $flash}
                <div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    {$flash}
                </div>
            {/if}
            <div class="panel panel-default">
                <div class="panel-heading">
                    {$Error}&nbsp;
                    <div class="pull-right">
                        <div class="btn-group">
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="apply" value="1">
                                <i class="fa fa-check"></i> Применить
                            </button>
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="1">
                                <i class="fa fa-save"></i> Сохранить
                            </button>
                            <a class="btn btn-outline btn-warning btn-xs" type="button"
                               href="index.php?section=SeoContent&token={$Token}">
                                <i class="fa fa-ban"></i> Отменить
                            </a>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="row">
                            <div class="col-lg-6">
                                {include file="include/seoContent.tpl"}

                                <div class="form-group">
                                    <label>Текст в шапке</label>
                                    <div id="reactEditor_headerText"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="well hidden" >
                                    <div class="form-group">
                                        <label id="name_label" {if !$Item->name}class="text-danger"{/if}>Название </label>
                                        <input id="name" name="name" type="text" value='{$Item->name|escape}' maxlength="255" placeholder="Название" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label id="section_label" {if !$Item->section}class="text-danger"{/if}>Секция </label>
                                        <input id="section" name="section" type="text" value='{$Item->section|escape}' maxlength="255" placeholder="Section" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label id="url_label" {if !$Item->section}class="text-danger"{/if}>Url </label>
                                        <input id="url" name="url" type="text" value='{$Item->url|escape}' maxlength="255" placeholder="Section" class="form-control">
                                    </div>
                                </div>
                                <div class="well">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            {if $Item->large_image}
                                                <img id="large_image" class="image_preview"
                                                     src='{$images_uploaddir}{$Item->large_image}?r={math equation="rand(1,1000000)"}'
                                                     alt="" style="width: 92%;"/>
                                                <p>
                                                    <a id="button_delete_largeimage"
                                                       class="btn btn-outline btn-danger btn-xs js-button_delete_largeimage"
                                                       type="button"
                                                       href="#">
                                                        <i class="fa fa-times-circle"></i> Удалить изображение
                                                    </a>
                                                </p>
                                            {else}
                                                <img id="large_image" class="image_preview" src='images/no_foto.gif'
                                                     alt=""/>
                                            {/if}
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <label>Изображение для шапки desktop</label>
                                                <input name="large_image" type="file"
                                                       style="width: 100%; overflow: hidden;"/>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" value="0" id="delete_large_image" name="delete_large_image"/>
                                </div>


                                <div class="well">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            {if $Item->stub_image}
                                                <img id="stub_image" class="image_preview"
                                                     src='{$images_uploaddir}{$Item->stub_image}?r={math equation="rand(1,1000000)"}'
                                                     alt="" style="width: 92%;"/>
                                                <p>
                                                    <a id="button_delete_stub_image"
                                                       class="btn btn-outline btn-danger btn-xs js-button_delete_stub_image"
                                                       type="button"
                                                       href="#">
                                                        <i class="fa fa-times-circle"></i> Удалить изображение
                                                    </a>
                                                </p>
                                            {else}
                                                <img id="stub_image" class="image_preview" src='images/no_foto.gif'
                                                     alt=""/>
                                            {/if}
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <label>Изображение для шапки mobile</label>
                                                <input name="stub_image" type="file"
                                                       style="width: 100%; overflow: hidden;"/>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" value="0" id="delete_stub_image" name="delete_stub_image"/>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-heading">
                    {$Error}&nbsp;
                    <div class="pull-right">
                        <div class="btn-group">
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="apply" value="1">
                                <i class="fa fa-check"></i> Применить
                            </button>
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="1">
                                <i class="fa fa-save"></i> Сохранить
                            </button>
                            <a class="btn btn-outline btn-warning btn-xs" type="button"
                               href="index.php?section=SeoContent&token={$Token}">
                                <i class="fa fa-ban"></i> Отменить
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</form>

{include file='tinymce_init.tpl'}
