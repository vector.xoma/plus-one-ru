<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">
            {$title}
        </h2>
        <table class="table table-condensed setting-widgets">
            <thead>
            <tr>
                <th><i class="fa fa-sort"></i></th>
                <th>Активен</th>
                <th>Имя виджета</th>
                <th>Wrap class</th>
                <th>Код Виджкта</th>
                <th>Дата создания</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            {foreach item=item from=$Items name=item}
                <tr data-id="{$item->id}">
                    <td nowrap>
                        <div class="handle-sort text-muted" style="cursor: all-scroll"><i class="fa fa-th-list"></i></div>
                    </td>
                    <td nowrap>
                        <a href="index.php{$item->enable_get}">
                            <i class="fa fa-toggle-{if $item->enabled}on{else}off{/if} fa-1x"></i>
                        </a>
                    </td>
                    <td nowrap>
                        {$item->widget_name|escape}
                    </td>
                    <td nowrap>
                        {$item->wrap_class|escape}
                    </td>
                    <td nowrap width="40%">
                        <pre class=".pre-scrollable" style="max-width: 400px; white-space: normal;">
                            {$item->widget_code|escape}
                        </pre>
                    </td>
                    <td nowrap>
                        {$item->created|escape}
                    </td>
                    <td nowrap>
                        <a href="index.php{$item->delete_get}" class="btn btn-danger btn-xs"
                           type="button"
                           onclick='if(!confirm("Удалить запись?")) return false;'>
                            <i class="fa fa-times-circle fa-1x"></i>
                        </a>
                    </td>
                </tr>
            {/foreach}
            </tbody>
        </table>
    </div>
</div>

<br>
<hr>

<form name="form" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {$Error}&nbsp;
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="form-group">
                            <input class="form-control" placeholder="Widget name" name="widget_name"/>
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Wrap class" name="wrap_class"/>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" placeholder="Widget Code" name="widget_code" rows="5"></textarea>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" placeholder="Enabled" name="enabled" value="1"/>Активен
                            </label>
                        </div>
                        <div class="btn-group">
                            <button class="btn btn-outline btn-primary btn-xs" type="submit">
                                <i class="fa fa-check"></i> Добавить
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript">
    {literal}

    {/literal}
</script>