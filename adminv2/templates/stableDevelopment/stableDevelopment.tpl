<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">
            {$title}
        </h2>
    </div>
</div>

{$Error}
{if $Errors}
    {foreach item=err from=$Errors name=err}
        <div class="row">
            <div class="alert alert-danger alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {$err}
            </div>
        </div>
    {/foreach}
{/if}

<form name="stableDevelopment" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-12">
            {if $flash}
                <div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    {$flash}
                </div>
            {/if}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="pull-right">
                        <div class="btn-group">
{*                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="apply">*}
{*                                <i class="fa fa-check"></i> Применить*}
{*                            </button>*}
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="save">
                                <i class="fa fa-save"></i> Сохранить
                            </button>
                            <a class="btn btn-outline btn-warning btn-xs" type="button"
                               href="blogpost/">
                                <i class="fa fa-ban"></i> Отменить
                            </a>
                        </div>
                    </div>
                    <br/>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="row">
                            <div class="col-lg-6">
                                {include file="include/seoContent.tpl"}
                            </div>
                        </div>
                    </div>



                </div>


                {if $Errors}
                    {foreach item=err from=$Errors name=err}
                        <div class="row">
                            <div class="alert alert-danger alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                {$err}
                            </div>
                        </div>
                    {/foreach}
                {/if}
                <div class="panel-heading">
                    <div class="pull-right">
                        <div class="btn-group">
{*                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="apply">*}
{*                                <i class="fa fa-check"></i> Применить*}
{*                            </button>*}
                            <button class="btn btn-outline btn-primary btn-xs" type="submit" name="save" value="save">
                                <i class="fa fa-save"></i> Сохранить
                            </button>
                            <a class="btn btn-outline btn-warning btn-xs" type="button"
                               href="blogpost/">
                                <i class="fa fa-ban"></i> Отменить
                            </a>
                        </div>
                    </div>
                    <br/>
                </div>
                <input type="hidden" name="item_id" id="item_id" value="{$Item->id}"/>
                <input type="hidden" name="type_visual_block" value="post_tag"/>
            </div>
        </div>
    </div>
</form>

{include file='posttags/_set_meta.tpl'}