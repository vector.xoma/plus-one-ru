
{*<script language="javascript" type="text/javascript" src="js/tiny_mce/plugins/smimage/smplugins.js"></script>*}
<script language="javascript" type="text/javascript" src="js/tinymce/tinymce.js?{$Config->admin_cache_version}"></script>

<script language="javascript">

    {literal}
    let charCountIdList = ['name_character_count', 'header_character_count'];
    function charCountOnEvent (ed) {
        charCountIdList.forEach(function (id) {
            var element = document.getElementById(id);
            if (element && ed.id + '_character_count' === id) {
                ed.on('change keyup focus', function (e) {
                    var count = ed.getContent({format : 'text'}).trim().length;
                    element.innerHTML = count + " символов";
                    element.classList.remove('text-danger');
                    console.log(parseInt(element.dataset['maxLength']), count);
                    if (parseInt(element.dataset['maxLength']) < count) {
                        element.classList.add('text-danger');
                    }
                });
            }
        });
    }

    function _loadPreviewEmbed(editor) {
        editor.dom.select('.embed-insert').forEach(function (el) {
            if(editor.dom.select('iframe', el).length > 0) {return;}
            let node  = el.children[0];
            editor.dom.select('script', node).forEach(function (script) {
                script.removeAttribute('data-mce-src');
                script.removeAttribute('type');
            });
            let _ID = 'frame_' + (new Date()).getTime();
            let divEmbedIframe = editor.dom.doc.createElement('iframe');
            divEmbedIframe.id = _ID;
            divEmbedIframe.height = 400;
            divEmbedIframe.contentEditable = false;
            divEmbedIframe.style = 'width:100%; border:none;';
            divEmbedIframe.srcdoc = node.innerHTML;
            divEmbedIframe.setAttribute('data-preview', '1');
            el.prepend(divEmbedIframe);
        });
    }

    function charCountOnInit (ed) {
        charCountIdList.forEach(function (id) {
            var element = document.getElementById(id);
            if (element && ed.id + '_character_count' === id) {
                var count = ed.getContent({format: 'text'}).trim().length;
                element.innerHTML = count + " символов ";
                element.classList.remove('text-danger');
                if (parseInt(element.dataset['maxLength']) < count) {
                    element.classList.add('text-danger');
                }
            }
        });

        ed.serializer.addNodeFilter('iframe', function (e) {
            e.forEach(function (frame) {
                let attribs = Array.from(frame.attributes).map(function (attr) {return attr.name;});
                if (attribs.indexOf('data-preview') !== -1) {
                    frame.remove();
                }
            });
        });

        _loadPreviewEmbed(ed);


        ed.serializer.addAttributeFilter('contenteditable', function (e) {
            e.forEach(function (tmplBlock) {
                tmplBlock.attr('contenteditable', null);
                let classTmpl = tmplBlock.attr('class');
                if (classTmpl) {
                    tmplBlock.attr('class', classTmpl.replace('selected--template', ''));
                }
            });
        });

        ed.dom.select('.info-side,blockquote.citate-block').forEach(function (templ) {
            templ.contentEditable = false;
        });
    }
    {/literal}

    tinymce.init({literal}{{/literal}
        selector: '.smalleditor',
        cache_suffix: '?v=4.1.8&cache=?{$Config->admin_cache_version}',
		height: 100,
		theme: 'modern',
        menubar: false,
        relative_urls: false,
        valid_children: '+a[div|p|h1|h2|h3|h4|h5|h6|span]',
        cleanup_on_startup: false,
        trim_span_elements: false,
        verify_html: false,
        convert_urls: true,
        cleanup: false,
        {*forced_root_block : 'div',*}
        {*forced_root_block_attrs: {literal}{{/literal}*}
            {*'class': 'text-block',*}
        {*{literal}}{/literal},*}
		plugins: [
		'advlist autolink lists link image charmap print preview hr anchor pagebreak',
		'searchreplace visualblocks visualchars code fullscreen',
		'insertdatetime media nonbreaking save table contextmenu directionality',
		'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
		],
		toolbar1: 'undo redo | insert | bold italic | code',
		//toolbar2: 'print preview media | forecolor backcolor emoticons | codesample | code',
		image_advtab: false,
		templates: [
                {literal}{{/literal} title: 'Test template 1', content: 'Test 1' {literal}}{/literal},
                {literal}{{/literal} title: 'Test template 2', content: 'Test 2' {literal}}{/literal}
		],
		{*content_css: ["{$root_url}/design/{$Settings->theme}/css/main_to_adm.css"],*}
        file_browser_callback :
            function(field_name, url, type, win){literal}{{/literal}

                var filebrowser = "filebrowser_2.php";
                filebrowser += (filebrowser.indexOf("?") < 0) ? "?type=" + type : "&type=" + type;
                tinymce.activeEditor.windowManager.open({literal}{{/literal}
                    title : "Выбор изображения",
                    width : 800,
                    height : 600,
                    url : filebrowser
                    {literal}}{/literal}, {literal}{{/literal}
                    window : win,
                    input : field_name
                    {literal}}{/literal});
                    return false;
                {literal}}{/literal},
        setup: 'charCountOnEvent',
        init_instance_callback: 'charCountOnInit',
{literal}
	}
{/literal});

{literal}
 function loaderScript () {
    tinymce.DOM.loadCSS('/adminv2/js/fotorama.css');
    var scriptLoader = new tinymce.dom.ScriptLoader();
        scriptLoader.add('/adminv2/js/jquery-1.8.0.js');
        scriptLoader.add('/adminv2/js/fotorama.js');

    scriptLoader.loadQueue(function () {
        console.log('All scripts are now loaded.');
    });
 }
    {/literal}

    {literal}
    function appendScript(){
     frames.onload = function(){

        var editor;
        var iframe = frames;
        var jqueryLib;
        var fotorama;
        var fotoramaStyle;
        var fotoramaInit;
        for(var i = 0 ; i< iframe.length; i++){

            editor = tinymce.editors[i].getBody();
            editor.designMode = 'off';
            fotoramaStyle = iframe[i].frameElement.contentDocument.createElement('link');
            fotoramaStyle.setAttribute('href', "/adminv2/js/fotorama.css" );
            fotoramaStyle.setAttribute('rel', "stylesheet" );

            editor.appendChild(fotoramaStyle);

            jqueryLib = iframe[i].frameElement.contentDocument.createElement('script');
            jqueryLib.setAttribute('src', "/adminv2/js/jquery-1.8.0.js" );

            editor.appendChild(jqueryLib);

            fotorama = iframe[i].frameElement.contentDocument.createElement('script');
            fotorama.setAttribute('src', "/adminv2/js/fotorama.js" );
            editor.appendChild(fotorama);




        }
        editor.designMode = 'on';
     }

    }
    {/literal}

            tinymce.init({literal}{{/literal}
                        selector: '.fulleditor',
                        setup: 'charCountOnEvent',
                        init_instance_callback: 'charCountOnInit',
                        cache_suffix: '?v=4.1.8&cache=?{$Config->admin_cache_version}',
                        height: 500,
                        theme: 'modern',
                        menubar: false,
                        relative_urls: false,
                        valid_children: '+a[div|p|h1|h2|h3|h4|h5|h6|span]',
                        cleanup_on_startup: false,
                        trim_span_elements: false,
                        extended_valid_elements : "img[*],style[*],list[*]",
                        verify_html: false,
                        convert_urls: true,
                        cleanup: false,
                        templates: [
                                    {literal}{{/literal}
                                "title": "Текстовый врез выделеный линиями",
                                // "description": "Some desc 2",
                                "url": "templates/textInserts/_newTextWithLines.html"
                                    {literal}}{/literal}
                        ],
                        {*forced_root_block : 'div',*}
                        {*forced_root_block_attrs: {literal}{{/literal}*}
                        {*'class': 'text-block',*}
                        {*{literal}}{/literal},*}
                        plugins: [
                            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                            'searchreplace visualblocks visualchars code fullscreen',
                            'insertdatetime media nonbreaking save table contextmenu directionality',
                            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc bulletImage imageVert imageFullWidth imageGallery embedInsert signLine headerTwoLevel citateNew dialogBox textColoredImg fullscreen responsivefilemanager fund'
                        ],
                        toolbar1: 'fullscreen | undo redo | insert  template | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | image bulletImage imageVert imageFullWidth imageGallery | embedInsert | signLine | headerTwoLevel | citateNew | dialogBox | textColoredImg',
                        toolbar2: 'responsivefilemanager | print preview media | forecolor backcolor emoticons | codesample | code | fund',
                        image_advtab: false,
                        content_css: [
                            "{$root_url}/design/{$Settings->theme}/css/fonts.css?{$Config->admin_cache_version}",
                            "{$root_url}/design/{$Settings->theme}/css/main_to_adm.css?{$Config->admin_cache_version}"

                         ],
                        importcss_append: true,
                    external_filemanager_path:"/filemanager/",
                    filemanager_title:"Файловый менеджер" ,
                    filemanager_sort_by: "date",
                    filemanager_descending: 1,
                    external_plugins: {literal}{{/literal} "filemanager" : "/filemanager/plugin.min.js"{literal}}{/literal}
                        {*file_browser_callback :*}
                                {*function(field_name, url, type, win){literal}{{/literal}*}

                                    {*var filebrowser = "filebrowser_2.php";*}
                                    {*filebrowser += (filebrowser.indexOf("?") < 0) ? "?type=" + type : "&type=" + type;*}
                                    {*tinymce.activeEditor.windowManager.open({literal}{{/literal}*}
                                        {*title : "Выбор изображения",*}
                                        {*width : 800,*}
                                        {*height : 600,*}
                                        {*url : filebrowser*}
                                        {*{literal}}{/literal}, {literal}{{/literal}*}
                                        {*window : win,*}
                                        {*input : field_name*}
                                        {*{literal}}{/literal});*}
                                    {*return false;*}
                                    {*{literal}}{/literal}*}
                        {literal}
                    }
                    {/literal});

</script>


{*<script language="javascript" type="text/javascript" src="js/tiny_mce/plugins/smimage/smplugins.js"></script>*}
{*<script language="javascript" type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>*}
{*<script language="javascript">*}

  {*tinyMCE.init({literal}{{/literal}*}
	{*// General options*}
	{*mode : "specific_textareas",*}
	{*editor_selector : /(editor|editor_big|editor_small|form-control)/,*}
	{*theme : "advanced",*}
	{*language : "ru",*}
	{*theme_advanced_path : false,*}
	{*apply_source_formatting : false,*}
	{*plugins : "smimage,safari,style,table,advimage,advlink,inlinepopups,media,contextmenu,paste,noneditable,visualchars,xhtmlxtras",*}
	{*relative_urls : false,*}
	{*remove_script_host : true,*}
	{*convert_urls : true,*}
	{*verify_html: false,*}
    {*remove_linebreaks : false,*}
    {*content_css :"../../../../design/{$Settings->theme}/css/main_to_adm.css",*}
    {*spellchecker_languages : "+Russian=ru,+English=en",*}
            {**}
	{*// Theme options*}
	{*theme_advanced_buttons1 : "pastetext,pasteword,|,bold,italic,underline,strikethrough,|,bullist,numlist,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,fontsizeselect",*}
	{*theme_advanced_buttons2 : "tablecontrols,|,link,unlink,anchor,smimage,charmap,|,removeformat,cleanup,|,code",*}
	{*theme_advanced_buttons3 : "",*}
	{*theme_advanced_buttons4 : "",*}
	{*theme_advanced_toolbar_location : "top",*}
	{*theme_advanced_toolbar_align : "left",*}
	{*theme_advanced_statusbar_location : "bottom",*}
	{*theme_advanced_resizing : true,*}

                {*var filebrowser = "filebrowser.php";*}
                {*filebrowser += (filebrowser.indexOf("?") < 0) ? "?type=" + type : "&type=" + type;*}
                {*tinymce.activeEditor.windowManager.open({literal}{{/literal}*}
                    {*title : "Выбор изображения",*}
                    {*width : 800,*}
                    {*height : 600,*}
                    {*url : filebrowser*}
                    {*{literal}}{/literal}, {literal}{{/literal}*}
                    {*window : win,*}
                    {*input : field_name*}
                    {*{literal}}{/literal});*}
                    {*return false;*}
                {*{literal}}{/literal}*}
{*{literal}*}
	{*}*}
{*{/literal});*}


{*</script>*}
