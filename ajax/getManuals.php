<?php

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

session_start();
chdir('..');

require_once('publicControllers/ManualController.php');

$a=null;
$manualController = new ManualController($a);
session_write_close();

$tag = @$_GET['tag'];
$page = @$_GET['page'] ? $_GET['page'] : 1;

if (!$tag) {
    $result = $manualController->getManual($page);
} else {
    $result = $manualController->getItem($tag);
}

//echo "<pre>";
//print_r($result);
//echo "</pre>";
//die();

header("Content-type: application/json; charset=UTF-8");
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");

echo json_encode($result);
