<?php
session_start();
chdir('..');
require_once('Widget.class.php');
$Widget = new Widget();

require_once('Blog.class.php');
$blogClass = new Blog();

$article_url = $_GET['url'];
$rubrika_url = $_GET['rubrika_url'];

$result = $blogClass->fetchNewsItem($article_url, $rubrika_url, true);

header("Content-type: application/json; charset=UTF-8");
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");
print json_encode($result);
?>
