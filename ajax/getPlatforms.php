<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

session_start();
chdir('..');

require_once('publicControllers/PlatformController.php');

$a=null;
$platformController = new PlatformController($a);
session_write_close();

$tag = @$_GET['tag'];
$page = @$_GET['page'] ? $_GET['page'] : 1;

if (!$tag) {
    $result = $platformController->getPlatform($page);
} else {
    $result = $platformController->getItems($tag);
}

//echo "<pre>";
//print_r($result);
//echo "</pre>";
//die();

header("Content-type: application/json; charset=UTF-8");
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");

echo json_encode($result);
