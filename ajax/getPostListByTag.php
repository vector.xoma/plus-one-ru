<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);

session_start();
chdir('..');
require_once('Widget.class.php');
$Widget = new Widget();

require_once('Blog.class.php');
$blogClass = new Blog();

$rubrika = $_GET['rubrika_url'];
$tagUrl = $_GET['tag_url'];
$page = 1;

if ($_GET['page']){
    $page = $_GET['page'];
}

$result = $blogClass->fetchListByTag($rubrika, $tagUrl, $page);

header("Content-type: application/json; charset=UTF-8");
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");
print json_encode($result);

//
//header("Content-type: application/jsonp; charset=UTF-8");
//header("Cache-Control: must-revalidate");
//header("Pragma: no-cache");
//header("Expires: -1");
//
//print json_encode($result);
?>
