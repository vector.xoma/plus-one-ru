<?php
session_start();
chdir('..');
require_once('Widget.class.php');
$Widget = new Widget();

require_once('Blog.class.php');
$blogClass = new Blog();

$rubrika = $_GET['rubrika'];
$page = $_GET['page'];

if (!$page){
    $page = 1;
}

if ($rubrika === 'news') {
    $items = $blogClass->getNews($rubrika, $page, true, true);
} else {
    $items = $blogClass->getPosts($rubrika, $page, $usedids, true, true);
}

header("Content-type: application/json; charset=UTF-8");
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");
print json_encode($items);
?>
