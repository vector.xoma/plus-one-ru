<?php
session_start();
chdir('..');
require_once('Widget.class.php');
$Widget = new Widget();

require_once('Blog.class.php');
$blogClass = new Blog();

$query = $_GET['query'];
$page = 1;
if ($_GET['page']){
    $page = $_GET['page'];
}


//var_dump($query);
//var_dump($page);

$items = $blogClass->getSearch($query, $page, 15, true);


header("Content-type: application/json; charset=UTF-8");
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");
print json_encode($items);
?>
