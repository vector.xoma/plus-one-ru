<?php

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

session_start();
chdir('../adminv2');

require_once('StableDevelopments.admin.php');

$a=null;
$stableDevClass = new StableDevelopments($a);
session_write_close();

$tag = @$_GET['tag'];

if (!$tag) {
    $result = $stableDevClass->getTagsForPublic();
} else {
    $result = $stableDevClass->getOnceTagForPublic($tag);
}

//echo "<pre>";
//print_r($result);
//echo "</pre>";
//die();

header("Content-type: application/json; charset=UTF-8");
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");

echo json_encode($result);
