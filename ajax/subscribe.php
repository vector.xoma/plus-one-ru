<?php

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

session_start();
chdir('..');

require_once('Widget.class.php');

$Widget = new Widget();

$result = "";

$hash = $Widget->setSubscribeCheck(@$_GET['email']);

if ($hash) {
    $result = $Widget->sendConfirmSubscribeEmail(@$_GET['email'], $hash);
}

header("Content-type: text/html; charset=UTF-8");
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");

echo $result;
