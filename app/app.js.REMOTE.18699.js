/**
 * Created by xoma on 07.11.16.
 */
'use strict';
// Ссылка на серверную часть приложения
var serviceBase = 'http://plus-one-kom-dev.truemanpride.ru/api/';
// Основной модуль приложения и его компоненты
var yii2AngApp = angular.module('yii2AngApp', [
    'ngRoute',
    //'yii2AngApp.site',
    'yii2AngApp.film',
    'ngSanitize',
    'infinite-scroll',
    'angular.vertilize'
]);
// рабочий модуль
//var yii2AngApp_site = angular.module('yii2AngApp.site', ['ngRoute']);
var yii2AngApp_film = angular.module('yii2AngApp.film', ['ngRoute']);

yii2AngApp.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    // Маршрут по-умолчанию
    $routeProvider.otherwise({redirectTo: '/'});

    // use the HTML5 History API
    $locationProvider.html5Mode(true);
}]);
