<main id="main">
    <div class="about">
        <img class="main-img" src="/files/about/{$about->image_name}" width="2000" height="1018" />
        <h1>{$about->header}</h1>
        <span>{$about->body}</span>
    </div>
</main>
