<main id="main">
{if $writerInfo}
    {if $writerInfo->private == 1}
        <div class="blog__title-holder">
            <div class="blog__title-holder__img">
                <img src="files/writers/{$writerInfo->writerImage}" alt="Колонка {$writerInfo->name_genitive}" width="266" height="265" />
            </div>
            <div class="blog__title-holder__box">
                <h1 class="blog__title">Колонка {$writerInfo->name_genitive}</h1>
                <h2 class="blog__title-2">{$writerInfo->description}</h2>
            </div>
        </div>
    {elseif $writerInfo->bloger == 1}
        <div class="blog__title-holder">
            <div class="blog__title-holder__img">
                <img src="files/writers/{$writerInfo->writerImage}" alt="Блог {$writerInfo->name_genitive}" width="266" height="265" />
            </div>
            <div class="blog__title-holder__box">
                <h1 class="blog__title">Блог {$writerInfo->name_genitive}</h1>
                <h2 class="blog__title-2">{$writerInfo->description}</h2>
            </div>
        </div>
    {else}
    <div class="head" style="margin-top: 35px;">
        <strong class="header">
            <strong class="h1">{$writerInfo->name}</strong>

            <span class="description">
                <span class="svg {$writerInfo->url}">
                    <svg viewBox="0 0 30 30" height="30" width="30" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1">
                        <title>{$writerInfo->url}</title>
                        <circle r="14.68" cy="14.68" cx="14.68"/>
                        {if $writerInfo->url == 'ecology'}
                        <path d="M19.71,10.19c-1.73,0-3.85,1.87-5,3-1.15-1.16-3.27-3-5-3a4.48,4.48,0,1,0,0,9c1.73,0,3.85-1.87,5-3,1.15,1.16,3.27,3,5,3a4.48,4.48,0,1,0,0-9Zm-10,7a2.48,2.48,0,1,1,0-5c1.48,0,2.46,1.31,3.63,2.48C12.16,15.85,11.12,17.16,9.7,17.16Zm10,0c-1.38,0-2.46-1.31-3.63-2.48,1.17-1.17,2.42-2.48,3.63-2.48a2.48,2.48,0,1,1,0,5Z" />
                        {/if}
                        {if $writerInfo->url == 'economy'}
                        <polygon points="24.18 11.22 22.75 9.79 15.55 16.99 10.38 11.82 5.37 16.84 6.8 18.26 10.38 14.68 15.55 19.85 24.18 11.22" />
                        {/if}
                        {if $writerInfo->url == 'community'}
                        <path d="M19.37,11.68a4.67,4.67,0,1,0-9.33,0c0,1.82,2,3.87,3.19,5L9,20.93l1.41,1.41L14.7,18,19,22.29l1.41-1.41-4.22-4.22C17.41,15.55,19.37,13.51,19.37,11.68Zm-4.61,3.56-.06-.06-.06.06C13.42,14.14,12,12.56,12,11.68a2.67,2.67,0,1,1,5.33,0C17.37,12.56,16,14.14,14.76,15.24Z" />
                        {/if}
                    </svg>
                </span>{$writerInfo->name_lead}
            </span>

            {if $writerData}
            <a class="btn-filter" ng-click="getAnalitics('');" href="#">
                <i class="i-graph"></i>
                Аналитика
            </a>
            {/if}

        </strong>
    </div>
    {/if}
{/if}
    <div class="posts">
        <div class="items" style="width: calc(100% + 24px);">

            {foreach item=item from=$items name=item}
                {foreach item=p from=$item name=p}

                    {foreach item=post from=$p name=post}
                        {if $post->blocks == '1/1'}
                            {include file="include/posts_1-1.tpl"}
                        {/if}
                        {if $post->blocks == '1/2'}
                            {include file="include/posts_1-2.tpl"}
                        {/if}
                        {if $post->blocks == '1/3'}

                            {if $p|@count == 1 && $post->dopBlock_1_3 == 2 && $post->stub_image}
                                {include file="include/posts_1-3.tpl"}

                                {* дополнительные блоки выводящиеся когда не хватает основных блоков для забивки строки *}
                                {if $post->dopBlock_1_3}
                                    <div id="fb-root"></div>
                                {literal}
                                    <script>(function (d, s, id) {
                                            var js, fjs = d.getElementsByTagName(s)[0];
                                            if (d.getElementById(id)) return;
                                            js = d.createElement(s);
                                            js.id = id;
                                            js.src = 'https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v3.0&appId=287353758002479&autoLogAppEvents=1';
                                            fjs.parentNode.insertBefore(js, fjs);
                                        }(document, 'script', 'facebook-jssdk'));
                                    </script>
                                {/literal}
                                {/if}

                                {if $post->dopBlock_1_3 == 1 && $isRenderDopBlock != 1}

                                    {assign var="isRenderDopBlock" value=1}

                                    <div class="item trio gray">
                                        <div class="fb-page" data-href="https://www.facebook.com/ProjectPlus1Official/" data-tabs="timeline"
                                             data-width="364" data-height="560" data-small-header="false" data-adapt-container-width="true"
                                             data-hide-cover="false" data-show-facepile="true">
                                            <blockquote cite="https://www.facebook.com/ProjectPlus1Official/" class="fb-xfbml-parse-ignore"><a
                                                        href="https://www.facebook.com/ProjectPlus1Official/">Проект +1 Official</a></blockquote>
                                        </div>
                                    </div>
                                {/if}

                                {if $post->dopBlock_1_3 == 2 && $isRenderDopBlock != 1}

                                    {assign var="isRenderDopBlock" value=1}

                                    <div class="item trio gray">
                                        <div class="fb-page" data-href="https://www.facebook.com/ProjectPlus1Official/" data-tabs="timeline"
                                             data-width="364" data-height="560" data-small-header="false" data-adapt-container-width="true"
                                             data-hide-cover="false" data-show-facepile="true">
                                            <blockquote cite="https://www.facebook.com/ProjectPlus1Official/" class="fb-xfbml-parse-ignore"><a
                                                        href="https://www.facebook.com/ProjectPlus1Official/">Проект +1 Official</a></blockquote>
                                        </div>
                                    </div>

                                    {if $post->stub_image}
                                        <a href="{$post->stub_link}">
                                            <div class="item trio blue no-image" style="background-position: 0 0; background-image: url('/files/writers/{$post->stub_image}');">
                                                <!-- /-->
                                            </div>
                                        </a>
                                    {/if}
                                {/if}
                            {elseif $p|@count == 3}
                                {include file="include/posts_1-3.tpl"}
                            {/if}

                        {/if}
                        {if $post->blocks == '1/4' || $post->blocks == '2/4' || $post->blocks == '3/4' || $post->blocks == '4/4'}
                            {include file="include/posts_1-4.tpl"}
                        {/if}
                    {/foreach}
                {/foreach}
            {/foreach}

            {if $items}
            <div style="display: none">

                {if $tagUrl}
                    {if $page}
                        <a href="api/getposttags/{$rubrika}/{$tagUrl}/{$page}">next page</a>
                    {else}
                        <a href="api/getposttags/{$rubrika}/{$tagUrl}/0">next page</a>
                    {/if}
                {elseif $getsimpleblogs == 1}

                    {if $page}
                        <a href="map/{$page}">next page</a>
                    {else}
                        <a href="map/0">next page</a>
                    {/if}

                {elseif $writerId}
                    {if $page}
                        <a href="api/getpostauthor/{$writerId}/{$page}">next page</a>
                    {else}
                        <a href="api/getpostauthor/{$writerId}/0">next page</a>
                    {/if}
                {else}
                    {if $page}
                        <a href="blogs/{$rubrika}/{$page}">next page</a>
                        {*<a href="api/getpost/{$rubrika}/{$page}">next page</a>*}
                    {else}
                        <a href="blogs/main/0">next page</a>
                        {*<a href="api/getpost/main/0">next page</a>*}
                    {/if}
                {/if}

            </div>
            {/if}
        </div>
    </div>
</main>


