{literal}
<script>
    document.addEventListener('DOMContentLoaded', () => {
        const siteForm = document.querySelector('#new_payment');

        siteForm.addEventListener('submit', (e) => {
            e.preventDefault();

            const paymentSum = document.querySelector('#payment_sum').value;
            const paymentType = document.querySelector('#payment_type').value;
            const paymentEmail = document.querySelector('#payment_email').value;

            const yaMoneyForm = document.querySelector('#yaMoneyForm');
            const yaSum = document.querySelector('#ya_sum');
            const yaCustomerNumber = document.querySelector('#ya_customerNumber');
            const yaPaymentType = document.querySelector('#ya_paymentType');
            const yaEmail = document.querySelector('#ya_email');

            // Не отправляем форму если нет данных
            if (!paymentSum || !paymentType) {
                return;
            }

            // Получаем данные формы
            const formData = new FormData(e.target);

            fetch('https://ivsezaodnogo.ru/payments', {
                method: 'POST',
                mode: 'cors',
                body: formData,
            })
                .then(response => response.json())
                .then(json => {
                    const id = json.id;

                    // Заполняем яндекс и сабмитим форму
                    yaSum.value = paymentSum;
                    yaCustomerNumber.value = id;
                    yaPaymentType.value = paymentType;
                    yaEmail.value = paymentEmail;

                    yaMoneyForm.submit();
                })
        });
    });
</script>
{/literal}