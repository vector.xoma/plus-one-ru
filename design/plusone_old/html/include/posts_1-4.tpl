{* 1/4 *}
{if $post->blocks == '1/4'}
<div class="item half quarter">
    <a href="/blogs/{$post->tags->url}" class="category-link">
        {include file="include/iconTagFirstLevelWoLink.tpl"}
    </a>
    {if $post->url == 'kak-ocenit-i-kompensirovat-ekologicheskiy-sled-meropriyatiya'}
        <a href="case-1.html">
            <strong class="h3">{$post->name}</strong>
            <p class="d">{$post->header}</p>
        </a>
    {else}
        <a href="/blog/{$post->tags->url}/{$post->url}">
            <strong class="h3">{$post->name}</strong>
            <p class="d">{$post->header}</p>
        </a>
    {/if}
    {include file="include/tagsOnPostTileOnlyDate.tpl"}
</div>
{/if}

{*2/4 *}
{if $post->blocks == '2/4'}
<div class="item half quarter_two">
    <a href="/blogs/{$post->tags->url}" class="category-link">
        {include file="include/iconTagFirstLevelWoLink.tpl"}
    </a>

    {if $post->url == 'kak-ocenit-i-kompensirovat-ekologicheskiy-sled-meropriyatiya'}
        <a href="case-1.html">
            <strong class="h3">{$post->name}</strong>
            <p class="d">{$post->header}</p>
        </a>
    {else}
        <a href="/blog/{$post->tags->url}/{$post->url}">
            <strong class="h3">{$post->name}</strong>
            <p class="d">{$post->header}</p>
        </a>
    {/if}

    {include file="include/tagsOnPostTileOnlyDate.tpl"}
</div>
{/if}

{* 3/4 *}
{if $post->blocks == '3/4'}
<div class="item half quarter_trio">
    <a href="/blogs/{$post->tags->url}" class="category-link">
        {include file="include/iconTagFirstLevelWoLink.tpl"}
    </a>

    {if $post->url == 'kak-ocenit-i-kompensirovat-ekologicheskiy-sled-meropriyatiya'}
        <a href="case-1.html">
            <strong class="h3">{$post->name}</strong>
            <p class="d">{$post->header}</p>
        </a>
    {else}
        <a href="/blog/{$post->tags->url}/{$post->url}">
            <strong class="h3">{$post->name}</strong>
            <p class="d">{$post->header}</p>
        </a>
    {/if}

    {include file="include/tagsOnPostTileOnlyDate.tpl"}
</div>
{/if}

{* 4/4 *}
{if $post->blocks == '4/4'}
<div class="item half quarter_quarter">
    <div class="visual_4_4">
        <a href="/blogs/{$post->tags->url}/{$post->url}">
            <img src="/files/blogposts/{$post->id}-1_3.jpg" alt="image description"/>
        </a>
    </div>
    <div class="info_4_4">
        <a href="/blog/{$post->tags->url}" class="category-link">
            {include file="include/iconTagFirstLevelWoLink.tpl"}
        </a>

        {if $post->url == 'kak-ocenit-i-kompensirovat-ekologicheskiy-sled-meropriyatiya'}
            <a href="case-1.html">
                <strong class="h3">{$post->name}</strong>
                <p class="d">{$post->header}</p>
            </a>
        {else}
            <a href="/blog/{$post->tags->url}/{$post->url}">
                <strong class="h3">{$post->name}</strong>
                <p class="d">{$post->header}</p>
            </a>
        {/if}

        {include file="include/tagsOnPostTileWoDate.tpl"}
    </div>
</div>
{/if}