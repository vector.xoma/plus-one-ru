<ul class="tags">
    {*<li>*}
        {*<span class="date">*}
            {*{$post->postDateStr}*}
        {*</span>*}
    {*</li>*}
    {if $post->partner}
        <li>
            <a href="{$post->partner_url}" target="_blank">
                <div style="color: {$post->partner_color}; font-size: 26px; top: 0px; left:0px; position: relative;">
                    &bull;
                    <span style="font-size: 11px; position: relative; top: -5px; left: 0; color: #000;">{$post->partner}</span>
                </div>
            </a>
        </li>
    {/if}
    <li>
        <a href="/author/{$post->writer_id}">
            {$post->writer}
        </a>
    </li>
    {if $post->postTagMainPage->name}
        <li>
            <a href="/posttags/{$post->postTagMainPage->blogtag_url}/{$post->postTagMainPage->url}">
                {$post->postTagMainPage->name}
            </a>
        </li>
    {/if}
</ul>