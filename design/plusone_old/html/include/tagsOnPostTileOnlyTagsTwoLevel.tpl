<ul class="tags">
    {if $post->postTagMainPage->name}
        <li>
            <a href="/posttags/{$post->postTagMainPage->blogtag_url}/{$post->postTagMainPage->url}">
                {$post->postTagMainPage->name}
            </a>
        </li>
    {/if}
</ul>