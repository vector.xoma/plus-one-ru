<ul class="tags">
    {*<li>*}
        {*<span class="date">*}
            {*{$post->postDateStr}*}
        {*</span>*}
    {*</li>*}
    <li>
        <a href="/author/{$post->writer_id}">
            {$post->writer}
        </a>
    </li>
    {if $post->postTagMainPage->name}
        <li>
            <a href="/posttags/{$post->postTagMainPage->blogtag_url}/{$post->postTagMainPage->url}">
                {$post->postTagMainPage->name}
            </a>
        </li>
    {/if}
</ul>