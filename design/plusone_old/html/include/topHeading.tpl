    <div id="header">
        <div class="container">
            <a href="#" class="icon-menu"><span></span></a>
            <ul class="short-menu">
                {*<li><a href="/about/">О проекте</a></li>*}
                <li><a href="http://conf.plus-one.ru/" target="_blank">Конференции</a></li>
                <li><a href="/special-projects/">Спецпроекты</a></li>
                <li><a href="https://полезныйгород.рф" target="_blank">Полезный город</a></li>
                <li><a href="https://award.plus-one.ru" target="_blank">Премия</a></li>
            </ul>
            <strong class="logo"><a href="./">+1</a></strong>
            <ul class="social">
                <li><a href="https://www.facebook.com/ProjectPlus1Official/ " target="_blank">FB</a></li>
                <li><a href="https://vk.com/plus1ru " target="_blank">VK</a></li>
                <li><a href="https://twitter.com/plus1_official" target="_blank">TW</a></li>
            </ul>
            <a href="#" class="search-opener">
                <img class="ico-search" src="/design/plusone_old/images/search.svg" alt="image description" width="20" height="20" />
            </a>
            <ul class="sites">
                <li><a href="http://plus-one.rbc.ru" target="_blank">+1 РБК</a></li>
                <li><a href="http://tass.ru/plus-one" target="_blank">+1 ТАСС</a></li>
                <li><a href="http://plus-one.vedomosti.ru" target="_blank">+1 ВЕДОМОСТИ</a></li>
                <li><a href="http://www.forbes.ru/native/plus-one" target="_blank">+1 FORBES</a></li>
                <li><a href="https://www.bfm.ru/special/visioners" target="_blank">+1 BFM</a></li>
            </ul>
            <div id="nav">
                <div class="container">
                    <div class="col">
                        <span class="title">&nbsp;</span>
                        <ul>
                            <li><a href="/about/">О проекте</a></li>
                            <li><a href="http://conf.plus-one.ru/" target="_blank">Конференции</a></li>
                            <li><a href="/special-projects/">Спецпроекты</a></li>
                            <li><a href="https://полезныйгород.рф/" target="_blank">Полезный город</a></li>
                        </ul>
                    </div>
                    <div class="col">
                        <span class="title">Площадки</span>
                        <ul class="sites">
                            <li><a href="http://plus-one.rbc.ru" target="_blank">+1 РБК</a></li>
                            <li><a href="http://tass.ru/plus-one" target="_blank">+1 ТАСС</a></li>
                            <li><a href="http://plus-one.vedomosti.ru" target="_blank">+1 ВЕДОМОСТИ</a></li>
                            <li><a href="http://www.forbes.ru/native/plus-one" target="_blank">+1 FORBES</a></li>
                            <li><a href="https://www.bfm.ru/special/visioners" target="_blank">+1 BFM</a></li>
                        </ul>
                    </div>
                    <div class="col">
                        <span class="title">Следите за новостями</span>
                        <ul>
                            <li><a href="https://www.facebook.com/ProjectPlus1Official/ " target="_blank">Facebook</a></li>
                            <li><a href="https://twitter.com/plus1_official" target="_blank">Twitter</a></li>
                            <li><a href="https://vk.com/plus1ru " target="_blank">VK</a></li>
                            {*<li><a href="#">Telegram</a></li>*}
                        </ul>
                    </div>
                    <ul class="header-contact-list">
                        <li>
                            <a href="tel:+79296302407">+7(929)630-24-07</a>
                        </li>
                        <li>
                            <a class="header-contact-list__email" href="mailto:info@plus-one.ru">info@plus-one.ru</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="open-close_slide">
                <div class="container">
                    <form action="/search/" method="get" class="search-form">
                        <div class="search-form_input-holder autocomplete-block">
                            <input type="text" class="autocomplete" name="query" placeholder="Поиск" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>