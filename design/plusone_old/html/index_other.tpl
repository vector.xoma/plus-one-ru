<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width">

    <title>{$title|escape}</title>
    <meta name="description" content="{if ($description)}{$description|escape}{else}{$descriptionDefault|escape}{/if}" />
    <meta name="keywords" content="{$keywords|escape}" />
    <meta name="title" content="{$title|escape}" />
    <base href="/">

    <meta property="og:title" content="{$title|escape}" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="{if ($description)}{$description|escape}{else}{$descriptionDefault|escape}{/if}" />
    {if $ogImage}
        <meta property="og:image" content="{$root_url}/{$ogImage}" />
    {else}
        <meta property="og:image" content="{$root_url}/design/plusone/images/og-plus-one.ru.png" />
    {/if}

    <meta property="og:site_name" content="{$ogSiteName|escape}" />
    <meta property="og:url" content="{$root_url}/{$ogUrl}" />


    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="{$title|escape}" />
    <meta name="twitter:description" content="{if ($description)}{$description|escape}{else}{$descriptionDefault|escape}{/if}" />
    <meta name="twitter:url" content="{$root_url}/{$ogUrl}" />
    <meta name="twitter:image" content="{$root_url}/{if ($ogImage)}{$ogImage}{else}{$ogImageDefault}{/if}" />
    <meta name="twitter:image:alt" content="{$title|escape}" />
    <meta name="twitter:site" content="{$ogSiteName|escape}" />

    <link rel="image_src" href="{$root_url}/{if ($ogImage)}{$ogImage}{else}{$ogImageDefault}{/if}">

    <link rel="apple-touch-icon" sizes="57x57" href="/design/plusone_old/images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/design/plusone_old/images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/design/plusone_old/images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/design/plusone_old/images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/design/plusone_old/images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/design/plusone_old/images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/design/plusone_old/images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/design/plusone_old/images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/design/plusone_old/images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/design/plusone_old/images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/design/plusone_old/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/design/plusone_old/images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/design/plusone_old/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="/design/plusone_old/images/favicon/manifest.json">

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/design/plusone/images/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <script type="text/javascript" src="/design/plusone_old/js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="/design/plusone_old/js/jquery.plugins.js"></script>
    <script type="text/javascript" src="/design/plusone_old/js/jquery.jscroll.js?{$smarty.now}"></script>
    <script type="text/javascript" src="/design/plusone_old/js/jquery.mainScroll.js?{$smarty.now}"></script>

    <script src="https://www.gstatic.com/firebasejs/5.2.0/firebase.js"></script>

    <script type="text/javascript" src="/design/plusone_old/js/jquery.main.js?{$smarty.now}"></script>
    <script type="text/javascript" src="/design/plusone_old/js/main.js?{$smarty.now}"></script>

    {* ADFox *}
    <script src="https://yastatic.net/pcode/adfox/loader.js" crossorigin="anonymous"></script>

    <link media="all" rel="stylesheet" href="/design/plusone_old/css/header.css?{$smarty.now}">
    <link media="all" rel="stylesheet" href="/design/plusone_old/css/main.css?{$smarty.now}">
    <link media="all" rel="stylesheet" href="/design/plusone_old/css/main-new.css?{$smarty.now}">
    <link media="all" rel="stylesheet" href="/design/plusone_old/css/fotorama.css?{$smarty.now}">

    <!-- Google Tag Manager -->
    {literal}
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-W7L3VJ7');</script>
    {/literal}
    <!-- End Google Tag Manager -->

    <meta property="fb:pages" content="916512591820179" />
</head>
<body>

    {include file="include/counters.tpl"}

    <div id="wrapper">

        {include file="include/topHeading.tpl"}

        <div class="container">

            {include file="banners/980x90.tpl"}

            <nav class="category-details">
                {include file="include/menu.tpl"}
            </nav>
        </div>
        <div class="container">

            {include file="banners/1140x290.tpl"}

            <main id="main">
                <div class="posts">
                    <div div class="items" style="width: calc(100% + 24px);">
                        {$content}
                    </div>
                </div>
            </main>
        </div>
    </div>

    <input type="hidden" id="rubrikaUrl" value="{$rubrikaUrl}">
    <input type="hidden" id="typeUrl" value="{$typeUrl}">

    {include file="include/retargetingСode.tpl"}

</body>
</html>