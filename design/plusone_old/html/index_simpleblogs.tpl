<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width">

    <title>{$title|escape}</title>
    <meta name="description" content="{$description|escape}" />
    <meta name="keywords" content="{$keywords|escape}" />
    <meta name="title" content="{$title|escape}" />
    <base href="/">

    <meta property="og:title" content="+1 — Полезный город" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="Карта «Полезный город» — сервис, созданный командой +1 и НП «И ВСЕ ЗА ОДНОГО» с целью развития городской среды и сообществ ответственных горожан." />
    <meta name="og:image" content="{$root_url}/design/plusone/images/pg-og.png" />
    <meta property="og:site_name" content="+1 — Полезный город" />
    <meta property="og:url" content="http://plus-one.ru/map/" />

    <meta name="twitter:title" content="+1 — Полезный город" />
    <meta name="twitter:description" content="Карта «Полезный город» — сервис, созданный командой +1 и НП «И ВСЕ ЗА ОДНОГО» с целью развития городской среды и сообществ ответственных горожан." />
    <meta name="twitter:url" content="https://plus-one.ru/map/" />
    <meta name="twitter:image" content="{$root_url}/design/plusone/images/pg-og.png" />
    <meta name="twitter:image:alt" content="+1 — Полезный город" />

    <link rel="apple-touch-icon" sizes="57x57" href="/design/plusone_old/images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/design/plusone_old/images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/design/plusone_old/images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/design/plusone_old/images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/design/plusone_old/images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/design/plusone_old/images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/design/plusone_old/images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/design/plusone_old/images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/design/plusone_old/images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/design/plusone_old/images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/design/plusone_old/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/design/plusone_old/images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/design/plusone_old/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="/design/plusone_old/images/favicon/manifest.json">

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/design/plusone/images/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <script type="text/javascript" src="/design/plusone_old/js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="/design/plusone_old/js/jquery.plugins.js"></script>
    <script type="text/javascript" src="/design/plusone_old/js/jquery.jscroll.js?{$smarty.now}"></script>
    <script type="text/javascript" src="/design/plusone_old/js/jquery.mainScroll.js?{$smarty.now}"></script>

    <script src="https://www.gstatic.com/firebasejs/5.2.0/firebase.js"></script>

    <script type="text/javascript" src="/design/plusone_old/js/jquery.main.js?{$smarty.now}"></script>
    <script type="text/javascript" src="/design/plusone_old/js/main.js?{$smarty.now}"></script>

    <link media="all" rel="stylesheet" href="/design/plusone_old/css/header.css?{$smarty.now}">
    <link media="all" rel="stylesheet" href="/design/plusone_old/css/main.css?{$smarty.now}">
    <link media="all" rel="stylesheet" href="/design/plusone_old/css/fotorama.css?{$smarty.now}">
    <link media="all" rel="stylesheet" href="/design/plusone_old/css/main-new.css?{$smarty.now}">

    {literal}
    <style>
        html, body {height:100vh; margin: 0; padding: 0;}
        .row-top { width: 100%; height: 70vh; overflow:hidden}
        .row-bottom { padding-top: 5px; border-top: 2px solid #C0C0C0; width: 100%; height: 29vh; height: 29%;}
    </style>
    {/literal}

    <!-- Google Tag Manager -->
    {literal}
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-W7L3VJ7');</script>
    {/literal}
    <!-- End Google Tag Manager -->

    <meta property="fb:pages" content="916512591820179" />
</head>
<body>
    {include file="include/counters.tpl"}
    <div id="wrapper" style="padding-top: 125px;">
        {include file="include/topHeading.tpl"}
        <div class="container">
            {include file="banners/980x90.tpl"}
        </div>
        <div class="container">
            <div class="row-top">
                <iframe id="map" name="map" style="border: none; height:100%; width:100%;" src="https://map.plus-one.ru"></iframe>
            </div>
            <div class="row-bottom">

                {include file="banners/1140x290.tpl"}

                <div class="scroll">
                    {$content}
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="rubrikaUrl" value="{$rubrikaUrl}">
    <input type="hidden" id="typeUrl" value="{$typeUrl}">
    <input type="hidden" id="writerId" value="{$writerId}">

    {include file="include/retargetingСode.tpl"}

</body>
</html>