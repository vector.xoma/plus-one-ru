    <div class="clear-zone">

        {include file="banners/300x600.tpl"}

        {if $blog->show_fund == 1}
        <div class="zone zone--form">
            <form id="new_payment" class="donate-form form-validation">
                <div class="donate-form_input-holder">
                    <input type="text" name="payment[amount]" id="payment_sum" placeholder="* Сумма" data-required="true" />
                    <input name="payment[resource_id]" value="{$blog->fund_resource_id}" type="hidden"/>
                    <input name="payment[resource_type]" value="Foundation" type="hidden"/>
                    <input name="commit" value="Перечислить" type="hidden"/>
                    <input name="payment[terms]" value="0" type="hidden"/>
                    <input name="payment[terms]" value="1" type="hidden"/>
                    <input name="g-recaptcha-response" value="" type="hidden"/>
                    <input name="utf8" value="&#x2713;" type="hidden"/>
                    <div class="donate-form_rur"></div>
                    <div class="donate-form_note">
                        Введите сумму вашего пожертвования. Это необходимо для сбора и проверки отчетов от благотворительных организаций.
                    </div>
                    <div class="donate-form_error-text">не может быть пустым</div>
                </div>
                <div class="donate-form_input-holder">
                    <select name="payment[pay_system]" id="payment_type" class="select" data-required="true" data-interactive="true" data-placeholder="* Способ оплаты">
                        <option value=""></option>
                        <option value="pc">Яндекс.Деньги</option>
                        <option value="ac">Банковская карта</option>
                        <option value="gp">Через кассы и терминалы</option>
                        <option value="wm">WebMoney</option>
                        <option value="sb">Сбербанк</option>
                        <option value="ab">Альфа-Клик</option>
                        <option value="ma">MasterPass</option>
                    </select>
                    <div class="donate-form_error-text">не может быть пустым</div>
                </div>
                <div class="donate-form_input-holder">
                    <input id="payment_email" type="email" name="payment[email]" placeholder="Email (опционально)" />
                    <div class="donate-form_note">
                        Введите E-mail для регистрации на нашем сайте. Это необходимо для уведомления Вас о публикации отчета благотварительной организации по денежным средствам, собранным при помощи сайта PLUS-ONE.RU
                    </div>
                </div>
                <div class="donate-form_input-holder">
                    <label for="check-1" name="donate-form_field-4" class="custom-check">
                        <input id="check-1" type="checkbox" data-required="true">
                        <span class="fake-input"></span>
                        <span class="fake-label donate-form_note">Я принимаю условия Положения о благотворительной программе <a href="#">&laquo;Поможем вместе&raquo;</a></span>
                    </label>
                    <div class="donate-form_error-text">Вы должны принять условия оферты</div>
                </div>
                <div class="donate-form_text-center">
                    <button>Перечислить</button>
                </div>
            </form>
        </div>
        <form action="https://money.yandex.ru/eshop.xml" id="yaMoneyForm" method="post" class="js-yandex-money-form">
            <input name="sum" id="ya_sum" type="hidden"/>
            <input name="shopId" id="shopId" value="19785" type="hidden"/>
            <input name="scid" id="scid" value="10422" type="hidden"/>
            <input name="customerNumber" id="ya_customerNumber" value="1" type="hidden"/>
            <input name="ShopArticleID" id="ShopArticleID" value="{$blog->shop_article_id}" type="hidden"/>
            <input name="paymentType" id="ya_paymentType" type="hidden" />
            <input name="cps_email" id="ya_email" type="hidden"/>
        </form>
        {/if}

        <div class="text-block">

            {if $blog->spec_project != 0}
            <div class="special">
                <a style="background-color: #{$blog->specProjectColor};" href="{$blog->specProjectUrl}" target="_blank">
                    {$blog->specProjectName}
                </a>
            </div>
            {/if}

            {if $blog->conference != 0}
                <div class="special">
                    <a style="background-color: #{$blog->confColor};" href="{$blog->confUrl}" target="_blank">
                        {$blog->confName}
                    </a>
                </div>
            {/if}

            {if $blog->header_on_page != ''}
                <strong class="h1">
                    {$blog->header_on_page}
                </strong>
            {else}
                <strong class="h1">
                    {$blog->name}
                </strong>
            {/if}

            {if $blog->private == 1}
            <div class="blockquote-box">
                <div class="blockquote-box_cite blockquote-box_cite--2">
                    <div class="blockquote-box_cite_img blockquote-box_cite_img--2">
                        <img src="files/writers/{$blog->writerImage}" alt="image description" width="128" height="128" />
                    </div>
                    <span class="blockquote-text">
                        Колонка {$blog->name_genitive}
                    </span>
                </div>
            </div>
            {/if}

            {if $blog->bloger == 1}
                <div class="blockquote-box">
                    <div class="blockquote-box_cite blockquote-box_cite--2">
                        <div class="blockquote-box_cite_img blockquote-box_cite_img--2">
                            <img src="files/writers/{$blog->writerImage}" alt="image description" width="128" height="128" />
                        </div>
                        <span class="blockquote-text">
                            Блог {$blog->name_genitive}
                        </span>
                    </div>
                </div>
            {/if}

            <span>
                {$blog->lead}
            </span>

            <div class="meta">
                {if $blog->partner_name}
                <a href="{$blog->partner_url}" target="_blank" class="date" style="background-color: transparent; border: 1px solid #b1b1b1" ng-if="post.partner_name">
                    <div style="color: {$blog->partner_color}; font-size: 26px; top: 0px; left:0px; position: relative;">
                        &bull;
                        <span style="font-size: 11px; position: relative; top: -5px; left: 0; color: #000;">{$blog->partner_name}</span>
                    </div>
                </a>
                {/if}
                <a class="{$blog->tags->url}" href="/blogs/{$blog->tags->url}">
                    {$blog->tags->name}
                </a>
                <time datetime="{$blog->postDateStr}">{$blog->postDateStr}</time>

                <span class="btn-time-read">{$totalReadTime} мин на чтение</span>
                <a href="{$urlToFavorite}" title="{$titleToFavorite}" class="btn-add-to-favorite" onclick="AddToBookmark(this);" rel="sidebar" >Добавить в закладки</a>
            </div>

            {$blog->body}

        </div>
    </div>
    {*<div class="text-block">*}
        {**}
    {*</div>*}

    <div class="text-block">
        <ul class="tags static">
            {if $blog->writer_id}
            <li>
                <a href="/author/{$blog->writer_id}">
                    {$blog->writer}
                </a>
            </li>
            {/if}
            {if $blog->postTagMainPage->name}
            <li>
                <a href="/posttags/{$blog->postTagMainPage->blogtag_url}/{$blog->postTagMainPage->url}">
                    {$blog->postTagMainPage->name}
                </a>
            </li>
            {/if}
            {if $blog->postTags}
            {foreach item=postTag from=$blog->postTags name=postTag}
            <li>
                <a href="/posttags/{$blog->tags->url}/{$postTag->url}">
                    {$postTag->name}
                </a>
            </li>
            {/foreach}
            {/if}
        </ul>
    </div>