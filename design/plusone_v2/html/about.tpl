


<div class="plusoneAboutLogo" style='background: url("/design/plusone_v2/img/plusOneLogo_white.svg") no-repeat 0 0'></div>


<h1 class="rbc_tape">
    {$about->header}
</h1>

<div class="rbcParagraphW about">
    {$about->body}
{*</div>*}

{*<div class="rbcParagraphW">*}
    {*<p class="rbcParagraph">+1&nbsp;&mdash; коммуникационный проект, рассказывающий о&nbsp;лидерских практиках в&nbsp;области социальной и&nbsp;экологической ответственности. Мы&nbsp;сотрудничаем с&nbsp;крупнейшими медиаплощадками России ТАСС, РБК, &laquo;Ведомости&raquo; и&nbsp;Forbes, на&nbsp;которых мы&nbsp;представлены как <a href="http://tass.ru/plus-one" target="_blank" rel="noopener noreferrer" title="ТАСС +1">tass.ru/plus-one</a>, <a href="http://plus-one.rbc.ru/" target="_blank" rel="noopener noreferrer" title="РБК +1">plus-one.rbc.ru</a>, <a href="http://plus-one.vedomosti.ru/" target="_blank" rel="noopener noreferrer" title="Ведомости +1">plus-one.vedomosti.ru</a> и <a href="http://www.forbes.ru/native/plus-one" target="_blank" rel="noopener noreferrer" title="Forbes +1">forbes.ru/native/plus-one</a>.&nbsp;+1&nbsp;объединяет бизнес, некоммерческие организации, социальных предпринимателей и&nbsp;городские сообщества с&nbsp;целью повысить информационную прозрачность и&nbsp;увеличить привлекательность рынка ответственных инвестиций.</p>*}
{*</div>*}
{*<div class="rbcParagraphW">*}
    {*<p class="rbcParagraph">Платформа +1&nbsp;&mdash; агрегатор рынка устойчивого развития. Мы&nbsp;создаем площадку для прямой коммуникации и&nbsp;обмена ресурсами между бизнесом, НКО, государством и&nbsp;обществом. В&nbsp;формате блогов участники проекта рассказывают о&nbsp;лучших практиках экологических и&nbsp;социальных преобразований. Мы&nbsp;помогаем формировать лояльность всех заинтересованных сторон посредством разделения общих ценностей и&nbsp;вовлечения в&nbsp;совместную деятельность.</p>*}
{*</div>*}
{*<div class="rbcParagraphW">*}
    {*<p class="rbcParagraph">Миссия Проекта +1&nbsp;&mdash; содействовать развитию инфраструктуры рынка устойчивого развития. Наши партнеры создают социально значимые проекты, мы&nbsp;рассказываем о&nbsp;них массовой аудитории и&nbsp;формируем культуру ответственного производства, потребления и&nbsp;инвестирования.</p>*}
{*</div>*}
{*<div class="rbcParagraphW">*}
    {*<p class="rbcParagraph">Партнеры: Unilever, Citibank, PepsiCo, QIWI, Amway, &laquo;Вымпелком&raquo;, &laquo;Норильский никель&raquo;, ГК&nbsp;&laquo;Сегежа&raquo;, &laquo;Оптиком&raquo;, WWF, &laquo;Вера&raquo;, &laquo;Подари жизнь&raquo;, &laquo;Игры будущего&raquo;.</p>*}
{*</div>*}
{*<div class="rbcParagraphW">*}
{*<p class="rbcParagraph">Контакты:</p>*}
    {*<p class="rbcParagraph">Мероприятия +1<span>&nbsp;</span><span>&mdash;&nbsp;</span>Вероника Васильева, <a href="mailto:v.vasilyeva@plus-one.ru" title="Вероника Васильева">v.vasilyeva@plus-one.ru</a></p>*}
{*</div>*}
{*<div class="rbcParagraphW">*}
    {*<p class="rbcParagraph">Редакция +1 <span>&mdash;&nbsp;</span>Евгений Арсюхин, <a href="mailto:e.arsyuhin@plus-one.ru" title="Евгений Арсюхин">e.arsyuhin@plus-one.ru</a></p>*}
{*</div>*}
{*<div class="rbcParagraphW">*}
    {*<p class="rbcParagraph">Клиентский отдел +1&nbsp;<span>&mdash;&nbsp;</span>Наталия Павлова, <a href="mailto:n.pavlova@plus-one.ru" title="Наталия Павлова">n.pavlova@plus-one.ru</a></p>*}
{*</div>*}

{*<div class="rbcParagraphW">*}
    {*<p class="rbcParagraph">+1 — коммуникационный проект, рассказывающий о лидерских практиках в области социальной и*}
        {*экологической*}
        {*ответственности. Мы сотрудничаем с крупнейшими медиаплощадками России ТАСС, РБК, «Ведомости» и Forbes, на которых*}
        {*мы*}
        {*представлены как tass.ru/plus-one, plus-one.rbc.ru, plus-one.vedomosti.ru и forbes.ru/native/plus-one. +1*}
        {*объединяет*}
        {*бизнес, некоммерческие организации, социальных предпринимателей и городские сообщества с целью повысить*}
        {*информационную*}
        {*прозрачность и увеличить привлекательность рынка ответственных инвестиций.</p>*}
{*</div>*}
{*<div class="rbcParagraphW">*}
    {*<p class="rbcParagraph">Платформа +1 — агрегатор рынка устойчивого развития. Мы создаем площадку для прямой*}
        {*коммуникации и обмена ресурсами*}
        {*между бизнесом, НКО, государством и обществом. В формате блогов участники проекта рассказывают о лучших практиках*}
        {*экологических и социальных преобразований. Мы помогаем формировать лояльность всех заинтересованных сторон*}
        {*посредством*}
        {*разделения общих ценностей и вовлечения в совместную деятельность.</p>*}
{*</div>*}
{*<div class="rbcParagraphW">*}
    {*<p class="rbcParagraph">Миссия Проекта +1 — содействовать развитию инфраструктуры рынка устойчивого развития. Наши*}
        {*партнеры создают социально*}
        {*значимые проекты, мы рассказываем о них массовой аудитории и формируем культуру ответственного производства,*}
        {*потребления и инвестирования.</p>*}
{*</div>*}
{*<div class="rbcParagraphW">*}
    {*<p class="rbcParagraph">Партнеры: Unilever, Citibank, PepsiCo, QIWI, Amway, «Вымпелком», «Норильский никель», ГК*}
        {*«Сегежа», «Оптиком», WWF,*}
        {*«Вера», «Подари жизнь», «Игры будущего».</p>*}
{*</div>*}
{*<div class="rbcParagraphW">*}
    {*<p class="rbcParagraph">Контакты:*}
    {*<p class="rbcParagraph">Мероприятия +1</p>*}
    {*<p class="rbcParagraph">*}
        {*Вероника Васильева,*}
    {*</p>*}
    {*<p class="rbcParagraph">*}
        {*v.vasilyeva@plus-one.ru</p>*}
    {*</p>*}
{*</div>*}
{*<div class="rbcParagraphW">*}
    {*<p class="rbcParagraph">Редакция +1</p>*}
    {*<p class="rbcParagraph">Елена Белоусова-Байковская,</p>*}
    {*<p class="rbcParagraph">e.baikovskaya@plus-one.ru</p>*}
{*</div>*}
{*<div class="rbcParagraphW">*}
    {*<p class="rbcParagraph">Клиентский отдел +1</p>*}
    {*<p class="rbcParagraph">*}
        {*имНаталия Павлова,*}
    {*</p>*}
    {*<p class="rbcParagraph">*}
        {*n.pavlova@plus-one.ru*}
    {*</p>*}
{*</div>*}