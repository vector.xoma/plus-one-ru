<h1 class="plusOneHeader">{$header}</h1>

{foreach item=authorObject from=$authors name=authorObject}
    <div class="plusOneListPosW">
        <div class="plusOneList {if $rubrikaUrl == 'platform'}platforma{else}$rubrikaUrl{/if}">
            <a href="/author/{$authorObject->id}">
                <p class="plusOneList_title">
                    {$authorObject->name}
                </p>
            </a>
            <div class="plusOneListCount">
                <p class="plusOneListCount_numb" title="{$authorObject->countPost}">{$authorObject->countPost}</p>
                <p class="plusOneListCount_descr">{$authorObject->countPostText}</p>
            </div>
        </div>
    </div>
{/foreach}
