<link rel="apple-touch-icon" sizes="57x57" href="/design/plusone_v2/img/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/design/plusone_v2/img/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/design/plusone_v2/img/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/design/plusone_v2/img/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/design/plusone_v2/img/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/design/plusone_v2/img/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/design/plusone_v2/img/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/design/plusone_v2/img/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/design/plusone_v2/img/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/design/plusone_v2/img/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/design/plusone_v2/img/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/design/plusone_v2/img/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/design/plusone_v2/img/favicon/favicon-16x16.png">
<link rel="manifest" href="{assets url='/design/plusone_v2/img/favicon/manifest.json'}">

<link rel="shortcut icon" href="/design/plusone_v2/img/favicon/favicon.ico" type="image/x-icon"/>

<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>

<link rel="stylesheet" href="{assets url='/design/plusone_v2/css/fonts.css'}"/>
<link rel="stylesheet" href="{assets url='/design/plusone_v2/css/main.css'}" />
<link rel="stylesheet" href="{assets url='/design/plusone_v2/css/footer.css'}" />
<link rel="stylesheet" href="{assets url='/design/plusone_v2/css/header.css'}" />
<link rel="stylesheet" href="{assets url='/design/plusone_v2/css/header_media.css'}" />
<link rel="stylesheet" href="{assets url='/design/plusone_v2/css/footer_media.css'}" />
<link rel="stylesheet" href="{assets url='/design/plusone_v2/css/media.css'}" />
<link rel="stylesheet" href="{assets url='/design/plusone_v2/css/animate.css'}" />
<link rel="stylesheet" href="{assets url='/design/plusone_v2/libs/scrollbar/scrollbar.css'}" />
<link rel="stylesheet" href="/design/plusone_v2/css/fotorama.css" />
<link rel="stylesheet" href="/design/plusone_v2/css/donate_form.css" />