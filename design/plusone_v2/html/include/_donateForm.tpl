
    <div class="form-wrap">
        <div class="form-img form-img__left"></div>
        <div class="form-header">
            <div class="form-logo"><img class="form-img__logo" src="/design/plusone_v2/img/form-title.svg" alt=""></div>
            <h2 class="form-title">{$blog->header_donate_form|escape}</h2>
            <div class="header-img"></div>
        </div>
        <div class="form-img form-img__right"></div>
        <form id="new_payment" method="post" class="form">
            <div class="form-block">
                <div class="form-grid-item form-sum">
                    <input name="payment[amount]" id="payment_sum" type="text" class="form-inp" placeholder="Сумма пожертвования" required>
                    <input name="payment[resource_id]" value="{$blog->fund_resource_id}" type="hidden"/>
                    <input name="payment[resource_type]" value="Foundation" type="hidden"/>
                    <input name="commit" value="Перечислить" type="hidden"/>
                    <input name="payment[terms]" value="0" type="hidden"/>
                    <input name="payment[terms]" value="1" type="hidden"/>
                    <input name="g-recaptcha-response" value="" type="hidden"/>
                    <input name="utf8" value="&#x2713;" type="hidden"/>
                    <span class="money-symbol">₽</span>
                    
                    <div class="form-text_small">
                       Мы не берем комиссию за пожертвования и делаем все, чтобы комиссия оператора становилась ниже
                    </div>
                </div>
            </div>
            <div class="form-block">
                <div class=" form-pay form-grid-item form-select-item">
                    <select name="payment[pay_system]" id="payment_type" class="form-select">
                        <option value="ac">Банковская карта</option>
                        <option value="sb">Сбербанк</option>
                        <option value="pc">Яндекс.Деньги</option>
                        <option value="gp">Через кассы и терминалы</option>
                        <option value="ab">Альфа-Клик</option>
                        <option value="wm">WebMoney</option>
                        <option value="ma">MasterPass</option>
                    </select>
                    <div class="form-text_small">
                        Выберите способ оплаты
                    </div>
                </div>
            </div>
            <div class="form-block">
                <div class="form-mail">
                    <input id="payment_email" type="email" name="payment[email]"  class="form-inp" placeholder="Почта" required>
                    <div class="form-text_small">
                        Введите e-mail, чтобы получить подтверждение о зачислении пожертвования и отчет о его расходовании
                    </div>
                </div>
            </div>
            <div class="form-block">
                <div class="form-grid-item form-buttons">
                    <button class="form-btn">Поддержать</button>
                    <div class="form-text_small">
                        Нажимая на кнопку «Поддержать», я принимаю условия Положения о благотворительной программе  «<a
                            href="#" class="form-link">Поможем вместе</a>».
                    </div>
                </div>
            </div>
            <div class="form-text_small form-text__active">
                <span class="text-item">Мы не берем комиссию за пожертвования и делаем все, чтобы комиссия оператора становилась ниже.</span>
                    
                <span class="text-item">Нажимая на кнопку «Поддержать», я принимаю условия Положения о благотворительной программе «Поможем вместе».</span>
            </div>



        </form>

        <form action="https://money.yandex.ru/eshop.xml" id="yaMoneyForm" method="post" class="js-yandex-money-form">
            <input name="sum" id="ya_sum" type="hidden"/>
            <input name="shopId" id="shopId" value="19785" type="hidden"/>
            <input name="scid" id="scid" value="10422" type="hidden"/>
            <input name="customerNumber" id="ya_customerNumber" value="1" type="hidden"/>
            <input name="ShopArticleID" id="ShopArticleID" value="{$blog->shop_article_id}" type="hidden"/>
            <input name="paymentType" id="ya_paymentType" type="hidden" />
            <input name="cps_email" id="ya_email" type="hidden"/>
        </form>
    </div>