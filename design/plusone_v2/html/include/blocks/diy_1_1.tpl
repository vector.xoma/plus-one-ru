<div class="mem1_1 scaleElem">
<a href="{$post->postUrl}" {if $post->linkTypeTag == 'parent'} target="_blank" {/if} class="wrap-all"></a>
    <div class="mem1_1_img" style="background: url('{$post->image}') no-repeat 50%; background-size: cover; background-color: #000;"></div>
    {* <div class="mem1_1_img mem1_1_img_mobile image_1_3_mobile" style="background: url('/files/blogposts/{$post->image_1_3}') no-repeat 50%; background-size: cover;"></div> *}
    <div class="mem1_1_img mem1_1_img_mobile image_1_3_mobile" style="background: url('{$post->imageMobile}') no-repeat 50%; background-size: cover;"></div>
    {if $post->bloger == 1}
        {include file="include/blogger_tag.tpl"}
    {else}
        <a href="{$post->tagUrl}" {if $post->linkTypeTag == 'parent'} target="_blank" {/if} class="mem1_1_logoWrapper">
            {include file='include/blocks/spec_project_for_mem.tpl' classPrefix = 'mem1_1'}
        </a>
        {if $post->is_partner_material == 1}
            <div class="partner-material eco_taccBlock">
                <span class="partner-text eco_tacc">Партнерский материал</span>
                <span class="partner-text_mobile eco_tacc">Партнерский</span>
            </div>
        {elseif $post->partnerName && $post->showPartner}
            <a href="{$post->partner_url}" class="eco_taccBlock">
                <span class="eco_tacc">{$post->partnerName}</span>
            </a>
        {/if}
    {/if}
    <a href="{$post->postUrl}" {if $post->linkTypeTag == 'parent'} target="_blank" {/if} class="mem1_1_header">{$post->name} {$post->header}</a>
</div>
