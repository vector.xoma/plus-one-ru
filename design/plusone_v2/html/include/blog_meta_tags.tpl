<!-- Cache control with meta tags -->
<meta http-equiv="cache-control" content="no-cache, must-revalidate, post-check=0, pre-check=0" />
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />

<meta name="title" content="{$blog->meta->title|escape}" />
<meta name="description" content="{if ($blog->meta->description)}{$blog->meta->description|escape}{else}{$descriptionDefault|escape}{/if}" />
<meta name="keywords" content="{$blog->meta->keywords|escape}" />

<meta property="og:title" content="{$blog->meta->title|escape}" />
<meta property="og:type" content="article" />
<meta property="og:description" content="{if ($blog->meta->description)}{$blog->meta->description|escape}{else}{$descriptionDefault|escape}{/if}" />
{if $ogImage}
    <meta property="og:image" content="{$root_url}/{$ogImage}" />
{else}
    <meta property="og:image" content="{$root_url}/og-plus-one.ru.png" />
{/if}

<meta property="og:site_name" content="{$ogSiteName|escape}" />
<meta property="og:url" content="{$root_url}/{$ogUrl}?v2" />


<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:title" content="{$blog->meta->title|escape}" />
<meta name="twitter:description" content="{if ($blog->meta->description)}{$blog->meta->description|escape}{else}{$descriptionDefault|escape}{/if}" />
<meta name="twitter:url" content="{$root_url}/{$ogUrl}?v2" />
<meta name="twitter:image" content="{$root_url}/{if ($ogImage)}{$ogImage}{else}{$ogImageDefault}{/if}" />
<meta name="twitter:image:alt" content="{$blog->meta->title|escape}" />
<meta name="twitter:site" content="{$ogSiteName|escape}" />

<link rel="image_src" href="{$root_url}/{if ($ogImage)}{$ogImage}{else}{$ogImageDefault}{/if}">