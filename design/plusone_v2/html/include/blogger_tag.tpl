<a href="{$post->writerPageUrl}" class="blog_sign_wrap">
    {if $post->blogImg}
        <div class="blog_sign_img_wrap">
            <img src="{$post->blogImg}" class="blog_sign_img">
        </div>
    {/if}
    <div>
        <span class="blog_signature">БЛОГ {$post->writerName}</span>
    </div>
</a>
