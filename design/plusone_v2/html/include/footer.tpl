<footer class="rbcFooter">
    <div class="container">
        <div class="rbcFooterTop">
            <a href="./">
                <div class="plusOneLogo_footer plusOneLogo_gray">
                    <!-- <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewbox="0 0 161.65 91.02">
                        <defs>
                            <style>
                                {literal}
                                .cls-1{fill: none;clip-rule: evenodd;}.cls-2{clip-path: url("#clip-path");}.cls-3{fill: #4a4a4a;}
                                {/literal}
                            </style>
                            <clippath id="clip-path">
                                <path class="cls-1" d="M132.18,62.83h-24V55.28h7.65v-15c0-1.36,0-2.77,0-2.77h-.1a6.76,6.76,0,0,1-1.41,1.91l-2.18,2L107.16,36l9.72-9.12h7.59V55.28h7.7Zm-105-22H40v-14h8.53v14H61.39v8.16H48.53V62.87H40V48.94H27.14ZM88.43,77.85h62.46a1.81,1.81,0,0,0,1.81-1.8V13.61a1.81,1.81,0,0,0-1.81-1.8H88.43a1.81,1.81,0,0,0-1.8,1.8V76.05A1.81,1.81,0,0,0,88.43,77.85ZM44.27,9h0A35.32,35.32,0,0,0,9,44.27v2.48a35.32,35.32,0,0,0,70.63,0h0V44.27A35.31,35.31,0,0,0,44.27,9Z"/>
                            </clippath>
                        </defs>
                        <title>Проект +1</title>
                        <g id="Layer_2" data-name="Layer 2">
                            <g id="background">
                                <g class="cls-2">
                                    <rect class="cls-3" width="161.65" height="91.02"/>
                                </g>
                            </g>
                        </g>
                    </svg> -->
                </div>
            </a>
        </div>
        <div class="rbcFooterBtm">
            <ul class="rbcFnav">
                <li>
                    <p class="rbcFnavLink_header">Редакция</a>
                </li>
                <li>
                    <a href="/about/" class="rbcFnavLink">О проекте</a>
                </li>
                <li>
                    <a href="/special-projects/" class="rbcFnavLink">Спецпроекты</a>
                </li>
                <li>
                    <a href="#" class="rbcFnavLink_date">© 2016—{$smarty.now|date_format:"%Y"}</a>
                </li>
            </ul>

            <ul class="rbcFnav">
                <li>
                    <p class="rbcFnavLink_header">Площадки</p>
                </li>
                <li>
                    <a href="http://plus-one.rbc.ru/" target="_blank" class="rbcFnavLink rbcFnavLink--borderGrn">РБК+1</a>
                <li>
                    <a href="http://plus-one.forbes.ru" target="_blank" class="rbcFnavLink rbcFnavLink--borderBlue">Forbes+1</a >
                </li>
                <li>
                    <a href="http://plus-one.vedomosti.ru/" target="_blank" class="rbcFnavLink rbcFnavLink--borderWhite">Ведомости+1</a >
                </li>
                <li>
                    <a href="https://www.bfm.ru/special/visioners" target="_blank" class="rbcFnavLink rbcFnavLink--borderOrange">BFM+1</a >
                </li>

            </ul>

            <ul class="rbcFnav">
                <li>
                    <p class="rbcFnavLink_header">Рубрики</p>
                </li>
                <li>
                    <a href="/blogs/ecology/" class="rbcFnavLink">Экология</a>
                </li>
                <li>
                    <a href="/blogs/community/" class="rbcFnavLink">Общество</a>
                </li>
                <li>
                    <a href="/blogs/economy/" class="rbcFnavLink">Экономика</a>
                </li>
                <li>
                    <a href="https://plus-one.ru/platform" class="rbcFnavLink">Платформа</a>
                </li>
                
            </ul>

            <ul class="rbcFnav">
                <li>
                    <p class="rbcFnavLink_header">Проекты</a>
                </li>
                <li>
                    <a href=" https://platform.plus-one.ru" target="_blank" class="rbcFnavLink">+1Платформа</a>
                </li>
                
                <li>
                    <a href="https://полезныйгород.рф" target="_blank" class="rbcFnavLink">+1Город</a>
                </li>
                <li>
                    <a href="https://ivsezaodnogo.ru/" target="_blank" class="rbcFnavLink">+1Люди</a>
                </li>
                <li>
                    <a href="https://award.plus-one.ru" target="_blank" class="rbcFnavLink">+1Премия</a>
                </li>
                
                
                <li>
                    <a href="https://conf.plus-one.ru" target="_blank" class="rbcFnavLink">+1Conf</a>
                </li>
                
            </ul>

            <ul class="rbcFnav">
                <li>
                    <p class="rbcFnavLink_header">Соцсети</p>
                </li>
                <li>
                    <a href="https://www.facebook.com/ProjectPlus1Official/" target="_blank" class="rbcFnavLink">Facebook</a>
                </li>
                <li>
                    <a href="https://vk.com/plus1ru" target="_blank" class="rbcFnavLink">Вконтакте</a>
                </li>
                <li>
                    <a href="https://www.youtube.com/c/Plusoneru" target="_blank" class="rbcFnavLink">YouTube</a>
                </li>

            </ul>

        </div>
    </div>
</footer>