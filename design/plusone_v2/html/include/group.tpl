<div class="container-group scaleElem">
    <h2 class="group-title">{$post->name}</h2>
    <div class="group-slider">
        {foreach item=groupItem from=$post->postList key=k name=groupItem}
            <div class="group-item">
                {include file="include/groupItem.tpl"}
            </div>
        {/foreach}
    </div>
</div>
