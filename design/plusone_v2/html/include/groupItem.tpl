{if $groupItem->format != 'picture'}
    <div class="grid-item">
            <div class="blType1_3StringHalf {$groupItem->frontClass} scaleElem">
                    <div class="blType1_3_contentW">
                        <a href="{$groupItem->postUrl}" {if $groupItem->linkTypePost == 'parent'} target="_blank" {/if} class="wrap-all"></a>
                        <div class="blType1_3StringTop">
                            <a href="{$groupItem->tagUrl}" {if $groupItem->linkTypeTag == 'parent'} target="_blank" {/if} class="blType1_3StringH link_to_redirect">
                                {if !$groupItem->specProject && !$groupItem->body_color}
                                <div class="blType1_3StringH_logo">
                                    {if $groupItem->tagCode == $blogTags->ecology}
                                            {*ecology*}
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30" class="logoBl">
                                                <g id="Layer_2" data-name="Layer 2" class="logoBl_el">
                                                    <g id="background">
                                                        <path d="M30,15A15,15,0,1,1,15,0,15,15,0,0,1,30,15Z"/>
                                                        <path class="cls-1 lightning_detail" d="M20.19,10.37c-1.79,0-4,1.93-5.16,3.12-1.19-1.19-3.38-3.12-5.17-3.12a4.63,4.63,0,1,0,0,9.25c1.79,0,4-1.93,5.17-3.12,1.18,1.19,3.37,3.12,5.16,3.12a4.63,4.63,0,1,0,0-9.25ZM9.86,17.56a2.57,2.57,0,0,1,0-5.13c1.53,0,2.54,1.35,3.75,2.57C12.4,16.21,11.32,17.56,9.86,17.56Zm10.33,0c-1.43,0-2.54-1.35-3.75-2.56,1.21-1.22,2.5-2.57,3.75-2.57a2.57,2.57,0,1,1,0,5.13Z"/>
                                                    </g>
                                                </g>
                                            </svg>
                                        {/if}
                                    {if $groupItem->tagCode == $blogTags->community}
                                            {*community*}
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40" class="logoBl">
                                                <g id="Layer_2" data-name="Layer 2" class="logoBl_el">
                                                    <g id="background">
                                                        <path d="M40,20A20,20,0,1,1,20,0,20,20,0,0,1,40,20Z"></path>
                                                        <path class="cls-1 lightning_detail" d="M23.67,15.91c0,1.2-1.88,3.34-3.56,4.85L20,20.68l-.08.08c-1.68-1.5-3.56-3.65-3.56-4.85a3.64,3.64,0,0,1,7.27,0Zm4.12,12.54L22,22.7c1.68-1.52,4.35-4.31,4.35-6.79a6.36,6.36,0,1,0-12.71,0c0,2.48,2.67,5.27,4.34,6.79l-5.81,5.81,1.93,1.93,5.9-5.9,5.83,5.83Z"></path>

                                                    </g>
                                                </g>
                                            </svg>
                                        {/if}
                                    {if $groupItem->tagCode == $blogTags->economy}
                                            {*community*}
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30" class="logoBl">
                                                <g id="Layer_2" data-name="Layer 2" class="logoBl_el">
                                                    <g id="background">
                                                        <circle class="cls-1" cx="15" cy="15" r="15"></circle>
                                                        <polygon points="15.89 20.29 10.61 15 6.95 18.66 5.49 17.2 10.61 12.08 15.89 17.37 23.25 10.01 24.71 11.47 15.89 20.29"></polygon>
                                                        <circle cx="15" cy="15" r="15"></circle>
                                                        <polygon class="lightning_detail" points="15.89 20.29 10.61 15 6.95 18.66 5.49 17.2 10.61 12.08 15.89 17.37 23.25 10.01 24.71 11.47 15.89 20.29"></polygon>
                                                    </g>
                                                </g>
                                            </svg>
                                        {/if}
                                </div>
                                {/if}
                                <p class="blType1_3StringH_title">{$groupItem->tagName}</p>
                            </a>
                            {if $groupItem->partnerName && $groupItem->showPartner}
                                <a href="{$groupItem->partner_url}" class="eco_taccBlock">
                                    <span class="eco_tacc">{$groupItem->partnerName}</span>
                                </a>
                            {/if}
                        </div>
                        <div>
                            <a href="{$groupItem->postUrl}" {if $groupItem->linkTypePost == 'parent'} target="_blank" {/if} class="blHalfDescr">
                                <div class="blHalfDescr_title">{$groupItem->name}</div>
                                <div class="blHalfDescr_subtitle">{$groupItem->header}</div>
                            </a>
                        </div>
                    </div>
                <div class="blType1_3StringDays">{$groupItem->dateStr}</div>
            </div>
    </div>
{else}
    <div class="grid-item">
        <div class="blType1_3_bgImgStringHalf bgImg {$groupItem->frontClass} scaleElem" style="background: url('{$groupItem->image}') no-repeat 50%; background-size: cover; background-color: #000;">
                <div class="blType1_3_bgImg_contentW">
                    <a href="{$groupItem->postUrl}" {if $groupItem->linkTypePost == 'parent'} target="_blank" {/if} class="wrap-all"></a>
                    <div class="blType1_3StringTop">
                    <a href="{$groupItem->tagUrl}" {if $groupItem->linkTypeTag == 'parent'} target="_blank" {/if} class="blType1_3_bgImgStringH link_to_redirect">
                    {if !$groupItem->specProject && ! $groupItem->body_color}

                        <div class="blType1_3_bgImgStringH_logo">
                            {if $groupItem->tagCode == $blogTags->ecology}
                                        {*ecology*}
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30" class="logoBl">
                                            <g id="Layer_2" data-name="Layer 2" class="logoBl_el">
                                                <g id="background">
                                                    <path d="M30,15A15,15,0,1,1,15,0,15,15,0,0,1,30,15Z"/>
                                                    <path class="cls-1 lightning_detail" d="M20.19,10.37c-1.79,0-4,1.93-5.16,3.12-1.19-1.19-3.38-3.12-5.17-3.12a4.63,4.63,0,1,0,0,9.25c1.79,0,4-1.93,5.17-3.12,1.18,1.19,3.37,3.12,5.16,3.12a4.63,4.63,0,1,0,0-9.25ZM9.86,17.56a2.57,2.57,0,0,1,0-5.13c1.53,0,2.54,1.35,3.75,2.57C12.4,16.21,11.32,17.56,9.86,17.56Zm10.33,0c-1.43,0-2.54-1.35-3.75-2.56,1.21-1.22,2.5-2.57,3.75-2.57a2.57,2.57,0,1,1,0,5.13Z"/>
                                                </g>
                                            </g>
                                        </svg>
                                    {/if}
                            {if $groupItem->tagCode == $blogTags->community}
                                {*community*}
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40" class="logoBl">
                                            <g id="Layer_2" data-name="Layer 2" class="logoBl_el">
                                                <g id="background">
                                                    <path d="M40,20A20,20,0,1,1,20,0,20,20,0,0,1,40,20Z"></path>
                                                    <path class="cls-1 lightning_detail" d="M23.67,15.91c0,1.2-1.88,3.34-3.56,4.85L20,20.68l-.08.08c-1.68-1.5-3.56-3.65-3.56-4.85a3.64,3.64,0,0,1,7.27,0Zm4.12,12.54L22,22.7c1.68-1.52,4.35-4.31,4.35-6.79a6.36,6.36,0,1,0-12.71,0c0,2.48,2.67,5.27,4.34,6.79l-5.81,5.81,1.93,1.93,5.9-5.9,5.83,5.83Z"></path>

                                                </g>
                                            </g>
                                        </svg>
                                    {/if}
                            {if $groupItem->tagCode == $blogTags->economy}
                                        {*community*}
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30" class="logoBl">
                                            <g id="Layer_2" data-name="Layer 2" class="logoBl_el">
                                                <g id="background">
                                                    <circle class="cls-1" cx="15" cy="15" r="15"></circle>
                                                    <polygon points="15.89 20.29 10.61 15 6.95 18.66 5.49 17.2 10.61 12.08 15.89 17.37 23.25 10.01 24.71 11.47 15.89 20.29"></polygon>
                                                    <circle cx="15" cy="15" r="15"></circle>
                                                    <polygon class="lightning_detail" points="15.89 20.29 10.61 15 6.95 18.66 5.49 17.2 10.61 12.08 15.89 17.37 23.25 10.01 24.71 11.47 15.89 20.29"></polygon>
                                                </g>
                                            </g>
                                        </svg>
                                    {/if}
                        </div>
                        {/if}
                        <p class="blType1_3_bgImgStringH_title">{$groupItem->tagName}</p>
                    </a>
                    {if $groupItem->partnerName && $groupItem->showPartner}
                        <a href="{$groupItem->partner_url}" class="eco_taccBlock">
                            <span class="eco_tacc">{$groupItem->partnerName}</span>
                        </a>
                    {/if}
                    </div>
                    <div class="blType1_3_bgImgStringHalfImg" style="background: url('{$groupItem->image}') no-repeat 50%; background-size: cover;"></div>

                    <div class="blHalfDescr">
                        <a href="{$groupItem->postUrl}" {if $groupItem->linkTypePost == 'parent'} target="_blank" {/if} >
                            <div class="blHalfDescr_title">{$groupItem->name}</div>
                            <div class="blHalfDescr_subtitle">{$groupItem->header}</div>
                        </a>
                        <div>
                            <p class="blType1_3_bgImgStringDays">{$groupItem->dateStr}</p>
                        </div>
                    </div>
                </div>
        </div>
    </div>
{/if}
