<div class="container">
    {*<div class="plusOneDropDown">*}
    <div class="plusOneHambCloseW">
        <span class="plusOneHambClose"></span>
    </div>
    <div class="informer">
            {include file="include/header_informer.tpl"}
        </div>
    <ul class="headerNav">
        <li>
            <a href="/about/" class="headerNav_link">О проекте</a>
        </li>
        <li>
            <a href="/special-projects/" class="headerNav_link">Спецпроекты</a>
        </li>
        <li>
            <a href="https://award.plus-one.ru/" target="_blank" class="headerNav_link">+1Премия</a>
        </li>
        <li>
            <a href="https://conf.plus-one.ru/" target="_blank" class="headerNav_link">+1Conf</a>
        </li>

        <li>
            <a href="https://полезныйгород.рф/" target="_blank" class="headerNav_link">+1Город</a>
        </li>
        <li>
            <a href="https://platform.plus-one.ru" target="_blank" class="headerNav_link">+1Платформа</a>
        </li>
        <li>
            <a href="https://ivsezaodnogo.ru/" target="_blank" class="headerNav_link">+1Люди</a>
        </li>
    </ul>
    <ul class="headerNav ">
        <li>
            <a href="http://plus-one.rbc.ru/" target="_blank" class="headerNav_link headerNav_link--green">РБК+1</a >
        </li>
        <!-- <li>
            <a href="https://tass.ru/plus-one" target="_blank" class="headerNav_link headerNav_link--yellow">+1 ТАСС</a >
        </li> -->
        <li>
            <a href="http://plus-one.forbes.ru" target="_blank" class="headerNav_link headerNav_link--blue">Forbes+1</a >
        </li>
        <li>
            <a href="http://plus-one.vedomosti.ru/" target="_blank" class="headerNav_link headerNav_link--white">Ведомости+1</a >
        </li>
        <li>
            <a href="https://www.bfm.ru/special/visioners" target="_blank" class="headerNav_link headerNav_link--orange">BFM+1</a >
        </li>
    </ul>
    <ul class="headerNav">
        <li>
            <a href="https://www.facebook.com/ProjectPlus1Official/" target="_blank" class="headerNav_link">Facebook</a>
        </li>
        <li>
            <a href="https://vk.com/plus1ru" target="_blank" class="headerNav_link">Вконтакте</a>
        </li>
        <li>
            <a href="https://twitter.com/plus1_official" target="_blank" class="headerNav_link">Twitter</a>
        </li>
        <li>
            <a href="https://www.youtube.com/c/Plusoneru" target="_blank" class="headerNav_link">YouTube</a>
        </li>
    </ul>
    {*</div>*}
</div>