
        <div class="headerNavWrapper">
            <ul class="headerNav">
                <li class="hidemobile">
                    <a href="/about/" class="headerNav_link">О проекте</a>
                </li>
                <li class="hidemobile">
                    <a href="/special-projects/" class="headerNav_link">Спецпроекты</a>
                </li>
                <li>
                    <a href="https://award.plus-one.ru/" target="_blank" class="headerNav_link">+1Премия</a>
                </li>
                <li class="hidemobile">
                    <a href="https://conf.plus-one.ru/" target="_blank" class="headerNav_link">+1CONF</a>
                </li>
                <li>
                    <a href="https://полезныйгород.рф/" target="_blank" class="headerNav_link">+1город</a>
                </li>
                 <li>
                    <a href="https://platform.plus-one.ru" class="headerNav_link">+1платформа</a>
                </li>
                <li>
                    <a href="https://ivsezaodnogo.ru" target="_blank" class="headerNav_link">+1ЛЮДИ</a>
                </li>
            </ul>
            <div class="headerNavWrapper headerNavWrapper--hidden">
                <ul class="headerNav  headerNav--mobile ">
                    <li>
                        <a href="http://plus-one.rbc.ru/" target="_blank" class="headerNav_link headerNav_link--green">РБК+1</a >
                    </li>
                    <li>
                        <a href="http://plus-one.forbes.ru" target="_blank" class="headerNav_link headerNav_link--blue">Forbes+1</a >
                    </li>
                    <li>
                        <a href="http://plus-one.vedomosti.ru/" target="_blank" class="headerNav_link headerNav_link--white">Ведомости+1</a >
                    </li>
                    <li>
                        <a href="https://www.bfm.ru/special/visioners" target="_blank" class="headerNav_link headerNav_link--orange">BFM+1</a >
                    </li>
                </ul>
                <div class="headerNav_line"></div>
                <ul class="headerNav">
                    <li>
                        <a href="https://www.facebook.com/ProjectPlus1Official/" target="_blank" class="headerNav_link">fb</a>
                    </li>
                    <li>
                        <a href="https://vk.com/plus1ru" target="_blank" class="headerNav_link">vk</a>
                    </li>
                    <li>
                        <a href="https://www.youtube.com/c/Plusoneru" target="_blank" class="headerNav_link">yt</a>
                    </li>
                    
                </ul>
            </div>
        </div>
