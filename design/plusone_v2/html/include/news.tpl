<div class="container desktop-slider">
    <div class="slick-slider_wrap">
        <a href="news" class="news-link">НОВОСТИ</a>
        <div class="slider-block">
            {foreach item=newsListPage from=$newsList key=keyNewsListPage name=newsListPage}
                <div class="slider-item">
                    <div class="item-wrap">
                {foreach item=news from=$newsListPage key=key name=news}
                    {if $news|@count == 1}
                        <div class="item item__1-1">
                    {elseif $news|@count == 3}
                        <div class="item item__1-3">
                    {else}
                        <div class="item item__1-3">
                    {/if}
                    {foreach item=newsItem from=$news key=keyItem name=newsItem}
                            <a href="{$newsItem->postUrl}" class="item-link">
                                <p class="item-description">{$newsItem->header|strip_tags}</p>
                            </a>
                    {/foreach}
                        </div>
                {/foreach}
                    </div>
                </div>
            {/foreach}
        </div>
    </div>
</div>

<div class="container mobile-slider">
    <div class="slick-slider_wrap">
        <a href="news" class="news-link">НОВОСТИ</a>
        <div class="slider-block">
            {foreach item=mobileNewsListPage from=$mobileNewsList key=keyMobileNewsListPage name=mobileNewsListPage}
                <div class="slider-item">
                    <div class="item-wrap">
                {foreach item=mobileNews from=$mobileNewsListPage key=keyMobile name=mobileNews}
                        <div class="item item__1-3">
                            <a href="{$mobileNews->postUrl}" class="item-link">
                                <p class="item-description">{$mobileNews->header|strip_tags}</p>
                            </a>
                        </div>
                {/foreach}
                    </div>
                </div>
            {/foreach}
        </div>
    </div>
</div>
