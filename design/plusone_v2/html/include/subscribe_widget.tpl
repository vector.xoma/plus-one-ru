<div class="subscribe-wrap animated"   style="margin-bottom: 20px;">
    <div class="subscribe-block">
        <h2 class="subscribe-title">Подпишитесь на&nbsp рассылку +1</h2>
        <div class="subscribe-email ">Ваш email</div>
        <form action="" class="subscribe-form">
            <input class="subscribe-form-inp" type="text" placeholder="Ваш email">
            <input class="subscribe-form-inp subscribe-form-inp__main" type="text" placeholder="Подпишитесь на рассылку +1">
            <button class="subscribe-form-btn" type="submit">Подписаться</button>
        </form>
    </div>
    <div class="message-block">
        <h2 class="subscribe-title">Подпишитесь на&nbsp рассылку +1</h2>
        <h2 class="message-title">На вашу почту отправлено письмо со ссылкой
            для подтверждения подписки
        </h2>
        <button class="message-btn" type="submit">Ok</button>
    </div>
</div>