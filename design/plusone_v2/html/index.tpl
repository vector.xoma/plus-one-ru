<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8"/>

    <base href="/">

    <title>{$title|escape}</title>

    {include file="include/meta_tags.tpl"}
    {include file="include/_assets.tpl"}

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/design/plusone_v2/css/slick-theme.css">
    <link rel="stylesheet" href="/design/plusone_v2/css/slick.css" />
    <link rel="stylesheet" type="text/css" href="/design/plusone_v2/css/group.css" />

    {* ADFox *}
    <script src="https://yastatic.net/pcode/adfox/loader.js" crossorigin="anonymous"></script>

    <!-- Google Tag Manager -->
    {literal}
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-W7L3VJ7');</script>
    {/literal}
    <!-- End Google Tag Manager -->

    <meta property="fb:pages" content="916512591820179" />
    <script src="https://code.jquery.com/jquery-1.12.4.js" type="application/javascript"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="application/javascript"></script>
</head>
<body>

{include file="include/counters.tpl"}


<div class="mainWrapper mainWrapper--plusOne mainPage">
    <div class="banners-wrapper">
        {include file="banners/1140x140.tpl"}
        {include file="banners/mobile_320x70.tpl"}
    </div>
    <header class="headerPlusOne">
        <div class="container">
            {include file="include/header.tpl"}
        </div>
    </header>

    <!-- Шторка -->
    <div class="plusOneDropDown">
        {include file="include/hamb_dropdown.tpl"}
    </div>


    <div class="container">
        {include file="include/header_hamb.tpl"}
        <div class="informer">
            {include file="include/header_informer.tpl"}
        </div>
    </div>

    {$content}

    {include file="include/footer.tpl"}

    <div class="bgforCloseEl"></div>
</div>

<script src="{assets url='/design/plusone_v2/libs/masonry/masonry.pkgd.min.js'}"></script>
<script src="{assets url='/design/plusone_v2/js/common.js'}"></script>
<script src="{assets url='/design/plusone_v2/libs/scrollbar/jquery.scrollbar.min.js'}"></script>
<script src="{assets url='/design/plusone_v2/js/scrollString.js'}"></script>
<script src="{assets url='/design/plusone_v2/js/slick.min.js'}"></script>
<script src="{assets url='/design/plusone_v2/js/news.js'}"></script>
<script src="{assets url='/design/plusone_v2/js/group.js'}"></script>

{if $confirmSubscribe}
    {literal}
        <script>
            $(function(){
                var $alert = $(
                '<div class="confirm-alert animated fadeInDown">' +
                    '<div class="confirm-alert__box">' +
                        '<div class="confirm-alert__logo"></div>'+
                        '<a class="close-alert" href="#"></a>' +
                        '<div class="confirm-alert__text">Вы подписались на рассылку +1</div>' +
                    '</div>'+
                '</div>');
                $alert.prependTo($('body'));
                $('.close-alert').click(function(e){
                    e.preventDefault();
                    $alert.addClass("fadeOutUp").hide(2000);
                });
                setTimeout(function(){
                    $alert.addClass("fadeOutUp").hide(2000);
                },5000);
            });
        </script>
    {/literal}
{/if}

{literal}
<script>
    (function($){
        $('.scrollbar-macosx').scrollbar();
    })(jQuery);
</script>
{/literal}


</body>
</html>
