<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8"/>

    <base href="/">

    <title>{$title|escape}</title>

    {include file="include/meta_tags.tpl"}
    {include file="include/_assets.tpl"}

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    {* ADFox *}
    <script src="https://yastatic.net/pcode/adfox/loader.js" crossorigin="anonymous"></script>

    <!-- Google Tag Manager -->
    {literal}
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-W7L3VJ7');</script>
    {/literal}
    <!-- End Google Tag Manager -->

    <meta property="fb:pages" content="916512591820179" />
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>
<body>

{include file="include/counters.tpl"}

<div class="mainWrapper mainWrapper--plusOne">
    <header class="headerPlusOne">
        <div class="container">
            {include file="include/header.tpl"}
        </div>
    </header>

    <!-- Шторка -->
    <div class="plusOneDropDown">
        {include file="include/hamb_dropdown.tpl"}
    </div>

    <div class="container">
        {include file="include/header_hamb.tpl"}
        <div class="informer">
            {include file="include/header_informer.tpl"}
        </div>
    </div>

    <div class="scroll">
        {$content}
    </div>

    {include file="include/footer.tpl"}

    <div class="bgforCloseEl"></div>
</div>

<script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js"></script>
<script src="{assets url='/design/plusone_v2/libs/masonry/masonry.pkgd.min.js'}"></script>
<script src="{assets url='/design/plusone_v2/js/common.js'}"></script>
<script src="{assets url='/design/plusone_v2/js/tails.js'}"></script>

</body>
</html>