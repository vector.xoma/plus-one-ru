<div class="clear-zone">

    {include file="banners/300x600.tpl"}

    <div class="text-block">
        <strong class="h1">{$blog->name}</strong>
        <span>{$blog->lead}</span>
        <div class="meta">
            {if $blog->partner_name}
                <a href="{$blog->partner_url}" target="_blank" class="date" style="background-color: transparent; border: 1px solid #b1b1b1" ng-if="post.partner_name">
                    <div style="color: {$blog->partner_color}; font-size: 26px; top: 0px; left:0px; position: relative;">
                        &bull;
                        <span style="font-size: 11px; position: relative; top: -5px; left: 0; color: #000;">{$blog->partner_name}</span>
                    </div>
                </a>
            {/if}
            <a class="{$blog->tags->url}" href="/blogs/{$blog->tags->url}">
                {$blog->tags->name}
            </a>
            <time datetime="{$blog->postDateStr}">{$blog->postDateStr}</time>

            <span class="btn-time-read">{$totalReadTime} мин на чтение</span>
            <a href="{$urlToFavorite}" title="{$titleToFavorite}" class="btn-add-to-favorite" onclick="AddToBookmark(this);" rel="sidebar">Добавить в закладки</a>
        </div>

        {*<div class="item {if $blog->tags->color=="f9d606"}yellow{/if}{if $blog->tags->color=="18dc46"}green{/if}{if $blog->tags->color=="00bae9"}blue{/if} analytics fact fact--2">
            <a href="#" class="category-link">Факт недели</a>
            <div class="info" style="width: 100%;">
                {if $blog->amount_to_displaying|count_characters > 6 && $blog->amount_to_displaying|count_characters <= 13}
                    <strong class="digit" style="font-size: 80px; line-height: 90px">
                        {$blog->amount_to_displaying}
                    </strong>
                {elseif $blog->amount_to_displaying|count_characters > 14}
                    <strong class="digit" style="font-size: 60px; line-height: 65px">
                        {$blog->amount_to_displaying}
                    </strong>
                {else}
                    <strong class="digit">
                        {$blog->amount_to_displaying}
                    </strong>
                {/if}
                <p><span class="mln">{$blog->signature_to_sum}</p>
                {$blog->header_on_page}
            </div>
        </div>*}
    </div>
</div>
<div class="text-block">
    {$blog->body}
</div>

<div class="text-block">
    <ul class="tags static">
        <li>
            <a href="/author/{$blog->writer_id}">
                {$blog->writer}
            </a>
        </li>
        {if $blog->postTags}
        {foreach item=postTag from=$blog->postTags name=postTag}
            <li>
                <a href="/tag/{$blog->tags->url}/{$postTag->url}">
                    {$postTag->name}
                </a>
            </li>
        {/foreach}
        {/if}
    </ul>
</div>