<div id="{$blog->id}" data-url="{$blog->fullUrl}" class="post-item"  {if $bodyColor}style="background: {$bodyColor}"{/if} data-id="{$blog->id}">
{if $customColors}
{literal}
    <style>
        .insert-wrap {
            border-bottom-color: {/literal}{$blog->border_color}{literal}!important;
            border-top-color: {/literal}{$blog->border_color}{literal}!important;
        }

        .rbcDescr p a{
            border-bottom-color: {/literal}{$blog->border_color}{literal}!important;
        }

        .fotorama__active .fotorama__loaded--img::after {
            border: 4px solid;
            border-color: {/literal}{$blog->border_color}{literal}!important;
        }

        .customColor-{/literal}{$blog->id}{literal} .pattern{
            border-color: {/literal}{$blog->border_color}{literal}!important;
            color:{/literal}{$blog->font_color}{literal}!important;
        }

        -{/literal}{$blog->id}{literal} .circle_article{
            fill:{/literal}{$blog->font_color}{literal}!important;
        }
        .customColor-{/literal}{$blog->id}{literal} .polygon_article{
            fill:{/literal}{$blog->font_color}{literal}!important;
        }

        .customColor-{/literal}{$blog->id}{literal} .topic-item,
        .customColor-{/literal}{$blog->id}{literal} .pattern span,
        .customColor-{/literal}{$blog->id}{literal} strong{
            color:{/literal}{$blog->font_color}{literal}!important;
        }

        .customColor-{/literal}{$blog->id}{literal} p,
        .customColor-{/literal}{$blog->id}{literal} h1,
        .customColor-{/literal}{$blog->id}{literal} h2,
        .customColor-{/literal}{$blog->id}{literal} p a,
        .customColor-{/literal}{$blog->id}{literal} .rbc_tape,
        .customColor-{/literal}{$blog->id}{literal} .articleAuthor_h,
        .customColor-{/literal}{$blog->id}{literal} cite,
        .customColor-{/literal}{$blog->id}{literal} .topic-item span a,
        .customColor-{/literal}{$blog->id}{literal} p cite,
        .customColor-{/literal}{$blog->id}{literal} .cls-1,
        .customColor-{/literal}{$blog->id}{literal} .rbcTopic,
        .customColor-{/literal}{$blog->id}{literal} .numbering,
        .customColor-{/literal}{$blog->id}{literal} .dialog-block,
        .customColor-{/literal}{$blog->id}{literal} .rbcDescr h2,
        .customColor-{/literal}{$blog->id}{literal} .rbcDescr blockquote .holder cite,
        .customColor-{/literal}{$blog->id}{literal} .fotorama-holder .info p span,
        .customColor-{/literal}{$blog->id}{literal} figure figcaption span.name,
        .customColor-{/literal}{$blog->id}{literal} .rbcDescr figcaption,
        .customColor-{/literal}{$blog->id}{literal} .info-side > p a.bulletImg {
            color:{/literal}{$blog->font_color}{literal}!important;
        }

        .customColor-{/literal}{$blog->id}{literal} .info-side  {
            background: {/literal}{$blog->bodyColor}{literal}!important;
        }
        .customColor-{/literal}{$blog->id}{literal} .topic-wrap a, {
        .customColor-{/literal}{$blog->id}{literal} .topic-wrap a:hover, {
            border-bottom: none!important;
        }
        .customColor-{/literal}{$blog->id}{literal} a {
            color:{/literal}{$blog->font_color}{literal}!important;
            border-bottom: 2px solid {/literal}{$blog->border_color}{literal}!important;
        }
        .customColor-{/literal}{$blog->id}{literal} a:hover {
            color:{/literal}{$blog->font_color}{literal}!important;
            border-bottom-color: {/literal}{$blog->border_color}{literal}!important;
        }

        .mainWrapper--material .articleAuthor::before,
        .mainWrapper--special .articleAuthor::before {
            background-color: {/literal}{$blog->font_color}{literal}!important;
        }
    </style>
{/literal}
{/if}

    <div class="banner-over-topic">
        {include file="banners/1140x200.tpl"}
        {include file="banners/mobile_600x200.tpl"}
    </div>
{if $blog->platformPost == 1}<p class="platform_h">МАТЕРИАЛ УЧАСТНИКА ПЛАТФОРМЫ +1</p>{/if}

    <div class="secondContainer customColor-{$blog->id} {if $blog->platformPost == 1}platform{/if}">

        <div class="topic-wrap">
            <div class="topic-header">
                {if ($blog->specProjectUrl && $blog->specProjectName) || $bodyColor}
                    <div class="topic-item"><span style="text-transform: uppercase">
                    {if $blog->specProjectUrl && $blog->specProjectName }
                        <a href="{$blog->specProjectUrl}" target="_blank">СПЕЦПРОЕКТ {$blog->specProjectName}</a>
                    {else}
                        {if $bodyColor }
                            СПЕЦПРОЕКТ {$blog->specProjectName}
                        {/if}
                    {/if}
                </span></div>
                {/if}
                <div class="topic-item topic-item--date">{$blog->dateStr}</div>
                {*<div class="topic-item">{$totalReadTime} {$totalReadTimeCaption} на чтение</div>*}
            </div>
            <div class="topic-tags-wrap">
                {if $blog->is_partner_material == 1}
                <div class="partner-material_inside eco_taccBlock">
                    <span class="partner-text_inside eco_tacc">Партнерский материал</span>
                </div>
            {/if}<a href="/{$blog->tags->url}"><div class="topic-tags__item topic-tags__item--{$blog->tags->url}">{$blog->tags->name}</div></a>
                {foreach  item=postTag from=$blog->postTags name=postTag}
                    <a href="/tag/{$blog->tags->url}/{$postTag->url}"> <div class="topic-tags__item">{$postTag->name}</div></a>
                {/foreach}
            </div>
        </div>

        {if $blog->typePost == 'citate' || $blog->typePost == 'diypost' }
            <h1 class="rbc_tape">{$blog->header_on_page}</h1>
        {elseif $blog->typePost == 'imageday'|| $blog->typePost == 'post' || $blog->typePost == 'factday'}
            <h1 class="rbc_tape">{$blog->name}</h1>
        {else}
            <h1 class="rbc_tape">{$blog->name}</h1>
        {/if}
        <span class="rbcDescr lead">
        {if $blog->fund_resource_id}
            {include file="include/_donateForm.tpl" assign="donateForm"}
            {include file="include/payForm.tpl" assign="payForm"}
            {assign var=bodyDonateForm value="$donateForm$payForm"}
            {$blog->lead|regex_replace:"/(\<p class=\"donate-form\"\>)+(.)*(\<\/p\>)+/":$bodyDonateForm}
        {else}
            {$blog->lead}
        {/if}
        </span>

        <div class="rbcDescr">
            {if $blog->mainImage}
                <figure contenteditable="false">
                    <img src="{$blog->mainImage}" alt="" title="" />
                    <figcaption contenteditable="false">
                        {if $blog->image_rss_description}
                            {$blog->image_rss_description}<br />
                        {/if}
                        {if $blog->image_rss_source}
                            <span class="name">{$blog->image_rss_source}</span>
                        {/if}
                    </figcaption>
                </figure>
            {/if}
            {if $blog->fund_resource_id}
                {include file="include/_donateForm.tpl" assign="donateForm"}
                {include file="include/payForm.tpl" assign="payForm"}
                {assign var=bodyDonateForm value="$donateForm$payForm"}
                {$blog->body|regex_replace:"/(\<p class=\"donate-form\"\>)+(.)*(\<\/p\>)+/":$bodyDonateForm}
            {else}
                {$blog->body}
            {/if}
            {if $blog->author_signature || $blog->author_name}
            <div class="articleAuthor">
                <p class="articleAuthor_h">{$blog->author_signature}</p>
                <p class="articleAuthor_name">{$blog->author_name}</p>
            </div>
            {/if}

        </div>
        <div class="share-block">
            <script src="https://yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
            <script src="https://yastatic.net/share2/share.js"></script>
            <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,twitter,evernote,viber,whatsapp,skype,telegram"></div>
        </div>
    </div>

{if $blog->show_fund == 1}
    <div class="secondContainer">
        {include file="include/_donateForm.tpl"}
        {include file="include/payForm.tpl"}
    </div>
{else}
    {if !$hasCookieSbscr }
        {include file="include/subscribe_widget.tpl"}
    {/if}
{/if}

{if $blog->relatedPosts}
<div class="container" style="margin-top: 100px;">
    <div class="grid_flex">
        {foreach item=post from=$blog->relatedPosts name=post}
            {if $post->typePost == 'post'}
                {include file="include/blocks/post_1_3.tpl"}
            {elseif $post->typePost == 'factday'}
                {include file="include/blocks/factWeek_1_3.tpl"}
            {elseif $post->typePost == 'citate'}
                {include file="include/blocks/citate_1_3.tpl"}
            {elseif $post->typePost == 'diypost'}
                {include file="include/blocks/diy_1_3.tpl"}
            {else}
                {include file="include/blocks/post_1_3.tpl"}
            {/if}
        {/foreach}
    </div>
</div>
{/if}

{if $blog->partnersPosts}
<div class="container" style="margin-top: 20px;">
    <div class="grid_flex">
        {foreach item=post from=$blog->partnersPosts name=post}
            {if $post->typePost == 'post'}
                {include file="include/blocks/post_1_3.tpl"}
            {elseif $post->typePost == 'factday'}
                {include file="include/blocks/factWeek_1_3.tpl"}
            {elseif $post->typePost == 'citate'}
                {include file="include/blocks/citate_1_3.tpl"}
            {elseif $post->typePost == 'diypost'}
                {include file="include/blocks/diy_1_3.tpl"}
            {else}
                {include file="include/blocks/post_1_3.tpl"}
            {/if}
        {/foreach}
    </div>
</div>
{/if}

    {if $settingWidget}
        {if $settingWidget->wrap_class}<div class="{$settingWidget->wrap_class}">{/if}
                {$settingWidget->widget_code}
        {if $settingWidget->wrap_class}</div>{/if}
    {/if}

    {*    <div class="lenta-widget">
            <div id="M500643ScriptRootC753510">
            <div id="M500643PreloadC753510"> Çàãðóçêà...
            </div>
            {literal}<script> (function(){ var D=new Date(),d=document,b='body',ce='createElement',ac='appendChild',st='style',ds='display',n='none',gi='getElementById',lp=d.location.protocol,wp=lp.indexOf('http')==0?lp:'https:'; var i=d[ce]('iframe');i[st][ds]=n;d[gi]("M500643ScriptRootC753510")[ac](i);try{var iw=i.contentWindow.document;iw.open();iw.writeln("<ht"+"ml><bo"+"dy></bo"+"dy></ht"+"ml>");iw.close();var c=iw[b];} catch(e){var iw=d;var c=d[gi]("M500643ScriptRootC753510");}var dv=iw[ce]('div');dv.id="MG_ID";dv[st][ds]=n;dv.innerHTML=753510;c[ac](dv); var s=iw[ce]('script');s.async='async';s.defer='defer';s.charset='utf-8';s.src=wp+"//jsc.lentainform.com/p/l/plus-one.ru.753510.js?t="+D.getUTCFullYear()+D.getUTCMonth()+D.getUTCDate()+D.getUTCHours();c[ac](s);})();
            </script> {/literal}
            </div>
        </div>
        <div class="pulse-widget" data-sid="partners_widget_horizontal_plus_one_ru"> </div>
        {literal}<script async src="https://static.pulse.mail.ru/pulse-widget.js"> </script>{/literal}
        <div class="smi-widget">
            <div id="unit_94748"><a href="http://smi2.ru/">Новости СМИ2</a></div>
            {literal}
             <script type="text/javascript" async="" src="//smi2.ru/data/js/94748.js" charset="utf-8"></script>
             <script type="text/javascript" charset="utf-8">
                (function() {
                    var sc = document.createElement('script'); sc.type = 'text/javascript'; sc.async = true;
                    sc.src = '//smi2.ru/data/js/94748.js'; sc.charset = 'utf-8';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(sc, s);
                }());
            </script>
            {/literal}
        </div>
        <div class="mediamtric-widget" style="display: block;">
                    <div id="DivID"></div>
                   {literal} <script type="text/javascript" src="//news.mediametrics.ru/cgi-bin/b.fcgi?ac=b;m=js;v=2;n=10;id=DivID" charset="UTF-8"></script>{/literal}
            </div>
    *}

</div>
