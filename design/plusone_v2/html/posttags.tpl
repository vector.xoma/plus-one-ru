<h1 class="plusOneHeader">Тэги</h1>

{foreach item=tag from=$tags name=tag}
    <div class="plusOneListPosW">
        <div class="plusOneList">
            <a href="/tag/{$tagUrl}/{$tag->url}">
                <p class="plusOneList_title">
                    {$tag->name}
                </p>
            </a>
            <div class="plusOneListCount">
                <p class="plusOneListCount_numb" title="{$tag->countPosts->count}">{$tag->countPosts->count}</p>
                <p class="plusOneListCount_descr">{$tag->countPosts->text}</p>
            </div>
        </div>
    </div>
{/foreach}
