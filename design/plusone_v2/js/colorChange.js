$(document).ready(function () {

    const pageUrl = window.location.href;
    const quantity = $('.plusOneList').length;

    const greenGradient = [24, 214, 0, quantity, 191, 244, 199];

    const yellowGradient = [250, 214, 0, quantity, 250, 245, 215];

    const blueGradient = [0, 186, 233, quantity, 201, 238, 247];

    const grayGradient = [143, 143, 143, quantity, 54, 54, 54];

    // функция для сафари
    // на старых сафари не перерендорит
    bruteForceRepaint = () => {
        const ss = document.styleSheets[0];
        try {
          ss.addRule('.xxxxxx', 'position: relative');
          ss.removeRule(ss.rules.length - 1);
        } catch (e) {
          console.error('safari rapaint error');
        }
      };

    // Функция для передачи цвета
    function* colorChange(r, g, b, quantity, rEnd, gEnd, bEnd, gray = false) {
        bruteForceRepaint();
        // находим дапозон цвета между начало и концом и / на quantity
        const rangeColorR = (rEnd - r) / (quantity - 1);
        const rangeColorG = (gEnd - g) / (quantity - 1);
        let rangeColorB = (bEnd - b) / (quantity - 1);
        if(gray) {
           rangeColorB = ((bEnd - b) / (quantity - 1))  * -1;
        }

        const toStr = ({r, g, b}) => `rgb(${parseInt(r)}, ${parseInt(g)}, ${parseInt(b)})`;
        // const toStr = ({r, g, b}) => `rgb(${r}, ${g}, ${b})`;

        const current = {
            r,
            g,
            b
        };
        const stepG = Math.round(20 / quantity);
        const stepB = Math.round(130 / quantity);

        while (true) {
            yield toStr(current);
            current.r += rangeColorR;
            current.g += rangeColorG;
            current.b += rangeColorB;
            // current.g += stepG;
            // current.b += stepB;
        }
    }

    // функция принимает цвет и запускает colorChange
    function setColor(rgbColor) {
        let generateColor = colorChange(...rgbColor);
        // если активный и повторный клик , то не обрабатываем
        if ($(this).hasClass('active')) 
            return;
        
        // если нет, то убираем активность со всех
        $('.statisticsBl ').each(function (i, el) {
            $(el).removeClass('active');
        });

        // и устанавливаем только для текущего
        $(this).addClass('active');
        $('.plusOneList_title').each(function (i, el) {
            let currentColor = generateColor.next().value;
            $(el).css('color', currentColor);
        });
    }

    // Смотрим на URL и в завиимости от урла устанавливаем цвет

    if (pageUrl.indexOf('/leaders/ecology') !== -1 || pageUrl.indexOf('/tag/ecology') !== -1) {
        setColor(greenGradient);
        return;
    }
    if (pageUrl.indexOf('/leaders/society') !== -1 || pageUrl.indexOf('/tag/society') !== -1) {
        setColor(yellowGradient);
        return;
    }
    if (pageUrl.indexOf('/leaders/economy') !== -1 || pageUrl.indexOf('/tag/economy') !== -1) {
        setColor(blueGradient);
        return;
    }
    if (pageUrl.indexOf('/authors/platform') !== -1) {
        setColor(grayGradient);

    }

});
