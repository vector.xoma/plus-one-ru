$(document).ready(function() {
  $(".rbcTopHHamb").click(function() {
    $(".rbcNavSide_rbcNavElWrapper").toggleClass("show");
  });

  // Для кастомного плеера
  $(".customPlayer_wrapper").click(function(e) {
    if (!$(".customPlayer_wrapper").hasClass("active"))
      $(".customPlayer_wrapper").toggleClass("active");
  });
  // при клике на эл-ты списка меню.
  $(".customPlayer_descr").each(function(index, el) {
    $(el).click(function() {
      $(".customPlayer_screen--img").css("display", "none");
      $(".customPlayer_screen > div.iframe").css("display", "block");
      const url = $(this).attr("data-video-url");
      $(".customPlayer_screen > div.iframe").html(url);
      // для кастомного видео
      calculateWHCustomPlayer(
        $(".customPlayer_screen iframe"),
        customPlayerRatio
      );
    });
  });
  // для кастомного видео/ Первая инициализация
  calculateWHCustomPlayer($(".customPlayer_screen iframe"), customPlayerRatio);
  // при закрытии плеера
  $(".closeCustomPlayer").click(function(e) {
    e.stopPropagation();
    // Срос вопроизведения видео в iframe
    $(".customPlayer_screen > div.iframe > iframe").attr(
      "src",
      $(".customPlayer_screen > div.iframe > iframe").attr("src")
    );
    $(".customPlayer_screen--img").css("display", "block");
    $(".customPlayer_wrapper").removeClass("active");
  });
  // конец

  // Для фиксированного хедера

  var headerWhite = document.querySelector(".headerPlusOneBtmW");
  var headerSearched = document.querySelector(".headerPlusOneBtm");
  var headerPlusOne = document.querySelector(".headerPlusOne");
  var headerPlusOneBtmWWrapper = document.querySelector(
    ".headerPlusOneBtmW--wrapper"
  );
  var validUrlsPattern = /^\/(economy|ecology|society|platform|news)\/.*/i;
  window.addEventListener("scroll", fixHeader);

  function fixHeader() {
    let margin = 0;
    if (headerPlusOneBtmWWrapper) {
      margin =
        parseInt(getComputedStyle(headerPlusOneBtmWWrapper).marginTop) +
        parseInt(getComputedStyle(headerPlusOneBtmWWrapper).marginBottom);
    }

    if (headerWhite === null) {
      return;
    }

    if (
      headerWhite.getBoundingClientRect().height +
        headerWhite.getBoundingClientRect().height +
        headerPlusOne.getBoundingClientRect().height +
        margin <=
      window.scrollY
    ) {
      headerWhite.classList.remove("headerPlusOneBtmW__static");
      headerWhite.classList.add("headerPlusOneBtmW__fixed");
      headerWhite.style.top = "0px";
    } else {
      headerWhite.classList.remove("headerPlusOneBtmW__fixed");
      headerWhite.classList.add("headerPlusOneBtmW__static");
      headerWhite.style.top = null;
    }
  }

  //Для инпута поиска
  var logo = $(".logoSearchAfter");

  logo.click(function(e) {
    var me = this;
    e.stopPropagation();
    $(".inputPlusOneSearch").removeClass("openAutocompleteMenu");
    if (
      $("body")
        .find(".inputPlusOneSearch")
        .hasClass("show")
    ) {
      $("body")
        .find(".inputPlusOneSearch")
        .removeClass("show");
      logo.css({ right: "0" });
      logo.removeClass("logo-white");
      $(".hambPlusOne").css({ opacity: "1" });
      $(".plusOneLogo_main").css({ opacity: "1" });
      headerSearched.classList.remove("search-active");
      if (headerWhite) headerWhite.classList.remove("search-active");
    } else {
      $("body")
        .find(".inputPlusOneSearch")
        .addClass("show");

      // $(this).parent().find(".inputPlusOneSearch").focus();
      setTimeout(function() {
        //$(".inputPlusOneSearch.show").focus();
        $(me)
          .parent()
          .find(".inputPlusOneSearch.show")
          .focus();
      }, 100);
      logo.css({ right: "80px" });
      $(".hambPlusOne").css({ opacity: "0" });
      $(".plusOneLogo_main").css({ opacity: "0" });
      logo.addClass("logo-white");
      headerSearched.classList.add("search-active");
      if (headerWhite) headerWhite.classList.add("search-active");
    }
  });

  $(".hambPlusOne").click(function() {
    $(".plusOneDropDown").addClass("show");
    $(".bgforCloseEl").addClass("show");
  });

  $(".plusOneHambCloseW").click(function() {
    $(".plusOneDropDown").removeClass("show");
    $(".bgforCloseEl").removeClass("show");
    // $(".logoSearchAfter").css({"right": "80px"})
  });

  $(".bgforCloseEl").click(function() {
    var logo = $(".logoSearchAfter");
    $(".plusOneDropDown").removeClass("show");
    $(this).removeClass("show");
    logo.css({ right: "0" });
    logo.removeClass("logo-white");
    $(".hambPlusOne").css({ opacity: "1" });
    // $(".logoSearchAfter").removeClass("show");
  });

  $(window).click(function(e) {
    var logo = $(".logoSearchAfter");
    if (e.target === $(".inputPlusOneSearch")[0]) return;
    $(".inputPlusOneSearch").removeClass("show");
    $(".inputPlusOneSearch").val("");
    logo.removeClass("show");
    logo.css({ right: "0" });
    logo.removeClass("logo-white");
    $(".hambPlusOne").css({ opacity: "1" });
    $(".plusOneLogo_main").css({ opacity: "1" });
    if (headerSearched) {
      headerSearched.classList.remove("search-active");
    }
    if (headerWhite) {
      headerWhite.classList.remove("search-active");
    }
  });

  $(".inputPlusOneSearch ").on("input", function(event) {
    var inputSearch = $(this);

    var uiMenu = $(".ui-menu");

    uiMenu.each(function(i, elem) {
      if (inputSearch[i].value != "" && elem.display != "none") {
        inputSearch[i].classList.add("openAutocompleteMenu");
      } else {
        inputSearch[i].classList.remove("openAutocompleteMenu");
      }
    });

    // if(!uiMenu.is(':visible')){
    //   inputSearch.addClass('openAutocompleteMenu');
    // }
    // else {
    //   inputSearch.removeClass('openAutocompleteMenu');
    // }
  });
  // initOpenClose();

  // =============================================Галерея инитится в шаблонах приходящих с бэка !!!!!!!!!!!!!!! ================================================
  // initGallery();

  initAutocomplete();

  // getMenu();

  // Скрипты для Iframe'ов с видео
  const ratioVimeo = 360 / 640; //соотношение высоты к ширине для Vimeo
  // const ratioYoutube = 315 / 734; //соотношение высоты к ширине для Youtube
  const ratioYoutube = 315 / 560; //соотношение высоты к ширине для Youtube
  if (!$(".mainPage")) widthAndHeightVideo(ratioVimeo, ratioYoutube);

  $(window).resize(function() {
    // для кастомного видео
    calculateWHCustomPlayer(
      $(".customPlayer_screen iframe"),
      customPlayerRatio
    );
    // TODO нужно понять на каких страницах хапускать скрипт
    // пока что он запускается на всех кроме главной
    if (!$(".mainPage")) widthAndHeightVideo(ratioVimeo, ratioYoutube);
  });
  //  end $(document).ready

  function responsiveVideo() {
    var container = document.querySelectorAll(".container-video");
    for (var i = 0; i < container.length; i++) {
      if (container[i].firstElementChild.getAttribute("src")) {
        if (
          container[i].firstElementChild
            .getAttribute("src")
            .toLowerCase()
            .includes("youtube") ||
          container[i].firstElementChild
            .getAttribute("src")
            .toLowerCase()
            .includes("vimeo")
        ) {
          container[i].style.height = "0";
          container[i].style.paddingBottom = "56.25%";
          container[i].firstElementChild.style.position = "absolute";
          container[i].firstElementChild.style.height = "100%";
        }
      }
    }
  }
  responsiveVideo();

  // Для бесконечного скролла

  /* Переменная-флаг для отслеживания того, происходит ли в данный момент ajax-запрос. В самом начале даем ей значение false, т.е. запрос не в процессе выполнения */
  var inProgress = false;

  var locString = window.location.pathname;
  var localStringArray = locString.split("/");
  console.log(localStringArray[1]);
  var timer = null;
  // Сюда падает разметка при запросе
  var dataBody;
  // Распаршеная разметка внутри которой ищем подходящий виджет
  var dataWidget;
  var widgetCounter = 1;

  function appendWidget() {
    // var widgetPulse = dataWidget.querySelector(".pulse-widget");
    // var widgetSmi = dataWidget.querySelector(".smi-widget");
    // var widgetLenta = dataWidget.querySelector(".lenta-widget");
    // var widgetMediametric = dataWidget.querySelector(".lenta-widget");

    // widgetMediametric.style.display = "block";
    if (window.hasOwnProperty('settingWidgets') && window.settingWidgets) {
      let widgetCounterLength = window.settingWidgets.length;
      if (widgetCounter >= widgetCounterLength) {
        widgetCounter = 0;
      }
      let appendElement = dataWidget.querySelector('.post-item');
      if (window.settingWidgets[widgetCounter]) {
        if(window.settingWidgets[widgetCounter].wrap_class) {
          let div = document.createElement('div');
          div.classList.add(window.settingWidgets[widgetCounter].wrap_class);
          div.innerHTML = window.settingWidgets[widgetCounter].widget_code;
          appendElement.append(div);
        } else {
          appendElement.innerHTML = appendElement.innerHTML + window.settingWidgets[widgetCounter].widget_code;
        }

      }
      widgetCounter++;
    }
    // if (widgetCounter == 0) {
    //   widgetPulse.style.display = "block";
    //   widgetSmi.style.display = "none";
    //   widgetLenta.style.display = "none";
    //   widgetMediametric.style.display = "none";
    // } else if (widgetCounter == 1) {
    //   widgetSmi.style.display = "block";
    //   widgetPulse.style.display = "none";
    //   widgetLenta.style.display = "none";
    //   widgetMediametric.style.display = "none";
    // } else if (widgetCounter == 2) {
    //   widgetLenta.style.display = "block";
    //   widgetPulse.style.display = "none";
    //   widgetSmi.style.display = "none";
    //   widgetMediametric.style.display = "none";
    // } else if (widgetCounter == 3) {
    //   widgetMediametric.style.display = "block";
    //   widgetPulse.style.display = "none";
    //   widgetSmi.style.display = "none";
    //   widgetLenta.style.display = "none";
    // }
    //
    // if (widgetCounter >= 4) {
    //   widgetCounter = 0;
    // }
    // widgetCounter++;
  }
  function appendData(data) {
    // Отрисовываем разметку

    var parser = document.createElement("div");
    parser.classList.add("parser-wrap");
    parser.innerHTML = data;
    dataWidget = parser;
    //console.log(dataWidget)
    appendWidget(dataWidget);
    //  getRandomId(".banner_1140x200");
    $("#scrollPostsInfinity").append(dataBody);
    responsiveVideo();
    subscribeWidgetUpdate();
    //  И делаем следущий запрос чтобы успел прийти респонс
    dataBody = parser;
    inProgress = false;
  }

  function getFotoramaCollection(selector) {
    var fotoramaClass;
    var arrs = [];
    var classPrefix = 0;
    var fotoramaCollection = document.querySelectorAll(selector);
    fotoramaCollection = Array.from(fotoramaCollection);
    // = 'fotoramaId_' + classPrefix;
    fotoramaCollection.forEach(function(fotoramaElem) {
      fotoramaClass = "fotoramaId_" + classPrefix;

      arrs = [fotoramaClass];
      fotoramaElem.classList.add(arrs[0]);
      if (arrs.indexOf(fotoramaClass) != -1) {
        // console.log(fotoramaClass);
        classPrefix++;

        initGallery(fotoramaClass);
      }
    });
    console.log("getFotoramaCollection", fotoramaClass);
  }

  function getRandomId(selector) {
    let collection = document.querySelectorAll(selector);

    collection.forEach(item => {
      let rand = Math.random() * 10000;
      let randId = `adfoxID_${+rand.toFixed(0)}`;
      item.children[0].id = randId;

      let banner = document.createElement("div");
      banner.classList.add("banner_adfox_wrap");
      banner.innerHTML = `div id="${randId}" class="zone banner_1140x200"
      window.Ya.adfoxCode.create({
                 ownerId: 260854,
                 containerId: '${randId}',
                 params: {
                     pp: 'g',
                     ps: 'cock',
                     p2: 'ghgl'
                 }
             });`;
    });
  }

  // Делаем запрос при загрузке
  $.ajax({
    xhrFields: { withCredentials: true },
    url: `api/getPost/${localStringArray[1]}`,
    dataType: "json",
    beforeSend: function() {
      inProgress = true;
    }
  }).done(function(data) {
    appendData(data.body);
    getFotoramaCollection(".fotorama");
  });

  let previousRoute = "/";
  let triggerAnalytics = (pathname, title) => {
    ym(40580670, "hit", pathname, title); // Метрика засчитывает переход при скроле и изменении урла;
    ga("create", "UA-86584410-1", "auto"); // Гугл аналитика засчитывает переход при скроле и изменении урла
    ga("send", "pageview", pathname);

    // initGallery()
  };

  $(window).scroll(function() {
    var posts = document.querySelectorAll(".post-item");
    clearTimeout(timer);
    for (let i = 0; i < posts.length; i++) {
      if (posts[i].getBoundingClientRect().bottom >= 0) {
        let options = { title: posts[i].querySelector(".rbc_tape").innerText };
        // Для сафари пришлось юзать таймаут
        timer = setTimeout(function() {
          // перезаписываем url в адресной строке
          window.history.replaceState(
            posts[i].body,
            "plus-one.ru",
            posts[i].dataset.url
          );
          document.title = options.title;
          if (previousRoute !== "/" + posts[i].dataset.url) {
            triggerAnalytics(location.pathname, options.title);
            previousRoute = location.pathname;
          }
        }, 200);
        break;
      }
    }
    /* Если высота окна + высота прокрутки больше или равны высоте всего документа и ajax-запрос в настоящий момент не выполняется, то запускаем ajax-запрос */
    if (
      $(window).scrollTop() + $(window).height() >=
        $(document).height() - 400 &&
      !inProgress &&
      window.location.pathname.match(validUrlsPattern)
    ) {
      $.ajax({
        xhrFields: { withCredentials: true },
        /* адрес файла-обработчика запроса */
        url: `api/getPost/${localStringArray[1]}`,
        /* метод отправки данных */
        method: "POST",
        dataType: "json",
        /* что нужно сделать до отправки запрса */
        beforeSend: function() {
          /* меняем значение флага на true, т.е. запрос сейчас в процессе выполнения */
          inProgress = true;
        }
        /* что нужно сделать по факту выполнения запроса */
      }).done(function(data) {
        appendData(data.body);
        getFotoramaCollection(".fotorama");
        if (window._oldCitateCorrector && data.id) {
          window._oldCitateCorrector(document.getElementById(data.id));
        }
      });
    }
  });
});

// пропорции для кастомного плеера
const customPlayerRatio = 246 / 420;
// Для пропорционального сжатия кастомного плеера
function calculateWHCustomPlayer(player, ratio) {
  // для кастомного видео
  if (
    document.documentElement.clientWidth < 1140 &&
    document.documentElement.clientWidth > 609
  ) {
    $(".customPlayer_screen > .iframe > iframe").attr("height", 333);
    $(".customPlayer_screen").css({ height: "333px" });
  }

  if (
    document.documentElement.clientWidth <= 609 &&
    document.documentElement.clientWidth > 440
  ) {
    $(".customPlayer_screen > .iframe > iframe").attr("height", 242);
    $(".customPlayer_screen").css({ height: "238px" });
  }

  if (document.documentElement.clientWidth > 1140) {
    $(".customPlayer_screen > .iframe > iframe").attr("height", 480);
    $(".customPlayer_screen").css({ height: "480px" });
  }
  if (document.documentElement.clientWidth < 440) {
    const widthParent = $(player).width();
    const currentHeight = Math.floor(widthParent * ratio);
    const currentHeightParent = `${currentHeight - 6}px`;
    // Устанавливаем значения
    $(".customPlayer_screen").css({ height: currentHeightParent });
    $(".customPlayer_screen > .iframe > iframe").attr("height", currentHeight);
  }
}

// Для пропорционального сжатия Iframe video
function calculateWHIrame(iframe, ratio) {
  iframe.closest("p").style.width = "100%";
  iframe.closest("p").style.position = "relative";
  const widthParent = $(iframe)
    .parent()
    .width();
  let currentHeight = Math.floor(widthParent * ratio);
  // Устанавливаем значения
  iframe.closest("p").style.height = `${currentHeight}px`;
  $(iframe).attr("width", widthParent);
  $(iframe).attr("height", currentHeight);
  iframe.style.height = `${currentHeight}px`;
  iframe.style.position = "absolute";
}
// Для пропорционального сжатия Iframe video
function widthAndHeightVideo(ratioVimeo, ratioYoutube) {
  let allIrame = $("iframe");
  for (let i = 0; i < allIrame.length; i++) {
    if (allIrame[i].src.indexOf("player.vimeo") !== -1) {
      calculateWHIrame(allIrame[i], ratioVimeo);
    }
    if (allIrame[i].src.indexOf("www.youtube") !== -1) {
      calculateWHIrame(allIrame[i], ratioYoutube);
    }
  }
}
var _isMobile = window.innerWidth < 768 ? true : false;
window.addEventListener("resize", function(e) {
  if (_isMobile && window.innerWidth > 767) {
    initGallery("fotorama", { thumbwidth: 88, thumbheight: 70 });
    _isMobile = false;
  } else if (!_isMobile && window.innerWidth < 768) {
    initGallery("fotorama", { thumbwidth: 62, thumbheight: 48 });
    _isMobile = true;
  }
});
function initGallery(fotoramaClass, params) {
  let thumbWidth, thumbHeight;
  if (params) {
    thumbWidth = params.thumbwidth;
    thumbHeight = params.thumbheight;
  } else {
    thumbWidth = window.innerWidth < 768 ? 62 : 88;
    thumbHeight = window.innerWidth < 768 ? 48 : 70;
  }

  $("." + fotoramaClass).each(function(index, el) {
    var _this = $(this),
      _parent = _this.closest(".fotorama-holder");
    var fotorama = $(this).data("fotorama");
    _this
      .on("fotorama:showend ", function(e, fotorama) {
        $(".numbering", _parent).html(
          fotorama.activeIndex + 1 + " / " + fotorama.size + ".&nbsp;"
        );
        var _cotent = $(
          ".fotorama__active .fotorama__html div p",
          _parent
        ).html();
        $(".info p", _parent).html(_cotent);
      })
      .fotorama({
        clicktransition: "dissolve",
        nav: "thumbs",
        width: "100%",
        maxwidth: 996,
        maxheight: 525,
        fit: "scaledown",
        loop: true,
        swipe: true,
        click: true,
        arrows: true,
        margin: 7,
        keyboard: { space: true },
        allowfullscreen: true,
        thumbwidth: thumbWidth,
        thumbheight: thumbHeight,
        thumbmargin: 7,
        navwidth: Math.min(755, window.innerWidth),
        ratio: "800/600",
        thumbfit: "cover"
      });
    // $(".numbering", _parent).html(
    //   fotorama.activeIndex + 1 + " / " + fotorama.size + ".&nbsp;"
    // );
    // var _cotent = $(".fotorama__active .fotorama__html div p", _parent).html();
    // $(".info p", _parent).html(_cotent);
  });

  $(".fotorama__wrap").addClass("fotorama__wrap--no-controls");
}

function getMenu() {
  var rubrikaUrl = $("#rubrikaUrl").val();
  var typeUrl = $("#typeUrl").val();

  $.ajax({
    url: "/ajax/getMenu.php",
    data: {
      rubrikaUrl: rubrikaUrl,
      typeUrl: typeUrl
    },
    type: "POST",
    //dataType: 'json',
    success: function(data) {
      $(".informer").html(data);
    }
  });
}

// open-close init
function initOpenClose() {
  var flag = "active";

  if ($(window).width() <= 767) {
    console.log("desctop");
    flag = false;
  }

  $("#header").openClose({
    activeClass: flag,
    opener: ".search-opener",
    slider: ".open-close_slide",
    animSpeed: 400,
    hideOnClickOutside: true,
    effect: "slide"
  });

  $(".search-opener").click(function() {
    $(".autocomplete").focus();
  });
}

function initAutocomplete() {
  let $input = $("input.autocomplete");
  $input.autocomplete({
    source: "api/autocomplette",
    appendTo: ".autocomplete-block",
    position: { my: "top", at: "bottom" },
    highlight: true,
    minLength: 2,
    autoFocus: false,
    select: function(event, ui) {
      if (ui.item.url != "") {
        document.location.href = ui.item.url;
      }
    }
  });
  $input.data("ui-autocomplete")._renderItem = function(ul, item) {
    return $("<li>")
      .attr("data-value", item.value)
      .append("<div>" + (item.label || "") + "</div>")
      .appendTo(ul);
  };
  $input.data("ui-autocomplete")._renderMenu = function(ul, items) {
    var that = this;
    if (
      items.filter(function(_it) {
        if (!_it.hasOwnProperty("label")) return false;
        return true;
      }).length === 0
    ) {
      that._renderItemData(ul, {
        label: "<em>Поиск не дал результатов...</em>",
        url: "",
        value: ""
      });
    } else {
      $.each(items, function(index, item) {
        that._renderItemData(ul, item);
      });
    }
  };
}
// Фикс высоты блоков подписей для цитат
function editCiteHeight() {
  var pics = document.querySelectorAll(".pic");
  var cites = document.querySelectorAll(".holder cite");
  // var picIndex;
  // var citeIndex;
  for (var i = 0; i < cites.length; i++) {
    /* picIndex = i;
    citeIndex = picIndex; */
    var citeHeight = parseInt(getComputedStyle(cites[i]).height);

    if (citeHeight >= 100) {
      pics[i].style.height = citeHeight + "px";
    }
  }
}
window.addEventListener("load", editCiteHeight);
window.addEventListener("resize", editCiteHeight);

window.addEventListener("load", function() {
  // Добавление уникального класса для AMP - страниц
  var images = this.document.querySelectorAll(".rbcDescr figure img");
  var rbcDescr = this.document.querySelectorAll(".rbcDescr");
  if (rbcDescr != undefined && rbcDescr[1] != undefined) {
    rbcDescr[1].classList.add("rbc-AMP-id");
  }
  if (images != undefined && images[0] != undefined) {
    images[0].classList.add("AMP-id");
  }

  // Удаление пустых item-link
  var itemLink = $(".item-link").find(".item-description");
  for (var i = 0; i < itemLink.length; i++) {
    if (itemLink[i].textContent.trim() === "") {
      itemLink[i].style.display = "none";
    }
  }
});

$(".form-select").each(function() {
  // Variables
  var $this = $(this),
    selectOption = $this.find("option"),
    selectOptionLength = selectOption.length,
    selectedOption = selectOption.filter(":selected"),
    dur = 500;

  $this.hide();
  // Wrap all in select box
  $this.wrap('<div class="select"></div>');
  // Style box
  $("<div>", {
    class: "select__gap",
    text: "Способ оплаты"
  }).insertAfter($this);

  var selectGap = $this.next(".select__gap"),
    caret = selectGap.find(".caret");
  // Add ul list
  $("<ul>", {
    class: "select__list"
  }).insertAfter(selectGap);

  var selectList = selectGap.next(".select__list");
  // Add li - option items
  for (var i = 0; i < selectOptionLength; i++) {
    $("<li>", {
      class: "select__item",
      html: $("<span>", {
        text: selectOption.eq(i).text()
      })
    })
      .attr("data-value", selectOption.eq(i).val())
      .appendTo(selectList);
  }
  // Find all items
  var selectItem = selectList.find("li");

  selectList.slideUp(0);
  selectList.addClass("nano");
  selectGap.on("click", function() {
    if (!$(this).hasClass("on")) {
      $(this).addClass("on");
      selectList.slideDown(dur);

      selectItem.on("click", function() {
        var chooseItem = $(this).data("value");

        $("select")
          .val(chooseItem)
          .attr("selected", "selected");
        selectGap.text(
          $(this)
            .find("span")
            .text()
        );

        selectList.slideUp(dur);
        selectGap.removeClass("on");
      });
    } else {
      $(this).removeClass("on");
      selectList.slideUp(dur);
    }
  });

  $(document).mouseup(function(e) {
    // событие клика по веб-документу
    if (!selectGap.is(e.target)) {
      selectList.slideUp(dur);
      selectGap.removeClass("on");
    }
  });
});

$(".form-sum .form-inp").on("keydown", function(e) {
  if (e.key.length == 1 && e.key.match(/[^0-9'".]/)) {
    return false;
  }
});

var subscribeWidgetUpdate = null;
window.subscribeWidget = function() {

  function getSbscrInput(formSubmited) {
    if (window.innerWidth < 1140) {
      return formSubmited.querySelector(".subscribe-form-inp");
    } else {
      return formSubmited.querySelector(".subscribe-form-inp__main");
    }
  }

  // window.addEventListener('load', showButton);
  window.addEventListener("load", getSubscrCookie);
  // sbscrInput.addEventListener('input', showButton);
  window.addEventListener("submit", setSubscrCookie);
  window.addEventListener("click", hideSubscribeWrapOnClick);

  // возвращает cookie если есть или undefined
  function getCookie(name) {
    var matches = document.cookie.match(
      new RegExp(
        "(?:^|; )" +
          name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, "\\$1") +
          "=([^;]*)"
      )
    );
    return matches ? decodeURIComponent(matches[1]) : undefined;
  }

  // уcтанавливает cookie
  function setCookie(name, value, props) {
    props = props || {};
    var exp = props.expires;
    if (typeof exp == "number" && exp) {
      var d = new Date();
      d.setTime(d.getTime() + exp * 1000);
      exp = props.expires = d;
    }

    if (exp && exp.toUTCString) {
      props.expires = exp.toUTCString();
    }
    value = encodeURIComponent(value);
    var updatedCookie = name + "=" + value;
    for (var propName in props) {
      updatedCookie += "; " + propName;

      var propValue = props[propName];

      if (propValue !== true) {
        updatedCookie += "=" + propValue;
      }
    }
    document.cookie = updatedCookie;
  }

  // удаляет cookie
  function deleteCookie(name) {
    setCookie(name, null, { expires: -1 });
  }

  // function showButton(){
  //     if(sbscrInput.value !== ""){
  //         sbscrButton.classList.add("subscribe-form-btn__show");
  //     }
  //     else{
  //         sbscrButton.classList.remove("subscribe-form-btn__show");
  //     }
  // }

  var subscribeCookie = "sbscr";
  function setSubscrCookie(event) {
    if (event.target.classList.contains("subscribe-form")) {
      let subscribeWrap = event.target.closest(".subscribe-wrap");
      let subscribeEmail = subscribeWrap.querySelector(".subscribe-email");
      let subscribeBlock = subscribeWrap.querySelector(".subscribe-block");
      let messageBlock = subscribeWrap.querySelector(".message-block");

      event.preventDefault();
      let reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
      let sbscrInput = getSbscrInput(event.target);
      if (sbscrInput.value.match(reg)) {
        jQuery.ajax({
          xhrFields: { withCredentials: true },
          url: "/ajax/subscribe.php",
          type: "post",
          data: { email: sbscrInput.value }
        });
        setCookie(subscribeCookie, true, { path: "/" });
        subscribeBlock.classList.add("hide");
        messageBlock.classList.add("show");
      } else {
        subscribeEmail.style.opacity = "1";
        subscribeEmail.innerHTML = "Ошибка! Введите правильный email";
        subscribeWrap.classList.add("shake");
        setTimeout(() => subscribeWrap.classList.remove("shake"), 2000);
      }
    }
    return false;
  }

  function getSubscrCookie() {
    if (getCookie(subscribeCookie)) {
      hideSubscribeWrap();
    } else {
      showSubscribeWrap();
    }
  }

  subscribeWidgetUpdate = getSubscrCookie;

  function informMessage() {

  }

  function hideSubscribeWrapOnClick(event) {
    if (event.target.classList.contains("message-btn")) {
      // event.target.parentElement.parentElement.classList.add('hide');
      Array.from(document.querySelectorAll(".subscribe-wrap")).forEach(function(
        el
      ) {
        el.classList.add("hide");
      });
    }
  }

  function showSubscribeWrap() {
    Array.from(document.querySelectorAll(".subscribe-wrap")).forEach(function(
      el
    ) {
      el.classList.remove("hide");
    });
  }

  function hideSubscribeWrap() {
    Array.from(document.querySelectorAll(".subscribe-wrap")).forEach(function(
      el
    ) {
      el.classList.add("hide");
    });
  }
};
window.subscribeWidget();
