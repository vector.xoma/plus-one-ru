$('.group-slider').slick({
  centerMode: true,
  centerPadding: '60px',
  slidesToShow: 3,
  swipe: true,
  dots: true,
  speed: 300,
  waitForAnimate: false,
  responsive: [
    {
      breakpoint: 767,
      settings: {
        arrows: false,
        swipe: true,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1,
        touchMove: true
      }
    }
  ]
});
