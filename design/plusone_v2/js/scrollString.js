$(document).ready(function() {
  var element = document.querySelectorAll(".parallaxTxt");
  for (let i = 0; i < element.length; i++) {
    $(element).css({ right: `${-$(window).width()}px` });
  }
  // Получаем нужный элемент

  var visible = function(target) {
    // Все позиции элемента
    var targetPosition = {
        top: window.pageYOffset + target.getBoundingClientRect().top,
        left: window.pageXOffset + target.getBoundingClientRect().left,
        right: window.pageXOffset + target.getBoundingClientRect().right,
        bottom: window.pageYOffset + target.getBoundingClientRect().bottom
      },
      // Получаем позиции окна
      windowPosition = {
        top: window.pageYOffset,
        left: window.pageXOffset,
        right: window.pageXOffset + document.documentElement.clientWidth,
        bottom: window.pageYOffset + document.documentElement.clientHeight
      };

    if (
      targetPosition.top + $(window).height() * 0.1 >
      windowPosition.bottom - $(target).height()
    ) {
      $(target).css({ right: 0 });
    }
    if (
      targetPosition.bottom - $(target).height() - $(window).height() * 0.1 <
      windowPosition.top
    ) {
      $(".parallaxTxt").css({
        right: `${$(".parallaxTxt_flex > a > p").width() - $(window).width()}px`
      });
    }
    if (
      targetPosition.bottom - $(target).height() - $(window).height() * 0.1 >=
        windowPosition.top &&
      // Если позиция нижней части элемента больше позиции верхней чайти окна, то элемент виден сверху
      targetPosition.top + $(window).height() * 0.1 <=
        windowPosition.bottom - $(target).height()
    ) {
      // Если элемент полностью видно, то запускаем следующий код
      let k =
        ($(".parallaxTxt_flex > a > p").width() - $(window).width()) /
        ($(window).height() * 0.8 - $(".parallaxTxt_flex > a > p").height());

      let resultPx =
        ($(window).height() * 0.9 - target.getBoundingClientRect().bottom) * k;

      $(".parallaxTxt").css({ right: `${resultPx}px` });
    }
  };

  for (let i = 0; i < element.length; i++) {
    // А также запустим функцию сразу. А то вдруг, элемент изначально видно
    visible(element[i]);
  }

  // Запускаем функцию при прокрутке страницы
  window.addEventListener("scroll", function() {
    for (let i = 0; i < element.length; i++) {
      visible(element[i]);
    }
  });
});
