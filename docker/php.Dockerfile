FROM php:7.2-apache
RUN a2enmod rewrite headers \
  && docker-php-source extract \
  && printf "\n" | pecl install redis \
  && docker-php-ext-install mysqli \
  && docker-php-source delete \
  && echo "extension=redis.so" > /usr/local/etc/php/conf.d/redis.ini \
  && echo "display_errors = On" > /usr/local/etc/php/conf.d/errors.ini \
  && echo "error_reporting = E_ERROR | E_PARSE" >> /usr/local/etc/php/conf.d/errors.ini
COPY . /var/www/html/
