FROM nginx:stable-alpine
COPY . /usr/share/nginx/html
RUN mv /usr/share/nginx/html/nginx.react.conf /etc/nginx/conf.d/default.conf
CMD ["nginx", "-g", "daemon off;"]