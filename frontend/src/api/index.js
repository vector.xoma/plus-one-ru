import { getContents, getContentRowTypes, postContentList, uploadPage,getGroups } from './content';
import { checkAuth } from './utils';

export default {
	getContents,
	getContentRowTypes,
	postContentList,
	uploadPage,
	checkAuth,
	getGroups,
};