import React, { Component } from 'react';

class Button extends Component {
	render() {
		const { classes, onClickHandler, children, style } = this.props;
		return (
			<div
				className={`adminHBtn ${classes}`}
				onClick={onClickHandler}
				style={style}
			>
				{children}
			</div>
		);
	}
}

export default Button;
