import React, { Component } from "react";

class Citate extends Component {
  render() {
    const { rowType, contentPic, importedPresetStyle, clearText } = this.props;

    switch (rowType) {
      case "1_1":
        return (
          <div className="quoteBlWrap" style={{ ...importedPresetStyle, display: "flex", flexDirection: "column", justifyContent: "space-between", color: '#000', background: '#eaeaea' }}>
            <div>
              <div style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
              }}>
                <p className="mainQuote" style={{ lineHeight: '39px' }}>{clearText(contentPic.name)}</p>
              </div>
              {contentPic.header && <p style={{ marginTop: '50px' }} className="qouteDescr">{clearText(contentPic.header)}</p>}
            </div>
          </div>
        );

      case "1_2":
        return (
          <div className="quoteBlWrap"
            style={{ ...importedPresetStyle, height: "100%", minHeight: '365px', width: "560px", display: "flex", flexDirection: "column", justifyContent: "space-between", color: '#000', background: '#eaeaea' }}
          >
            <p style={{ fontSize: "34px" }} className="mainQuote">
              {clearText(contentPic.name)}
            </p>
            <p className="qouteDescr">{clearText(contentPic.header)}</p>
          </div>
        );

      case "1_3":
        return (
          <div
            className="quoteBlWrap"
            style={{
              ...importedPresetStyle,
              height: "510px",
              margin: "0",
              textAlign: "center",
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-between",
              background: '#eaeaea',
            }}
          >
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                justifyContent: 'space-between',
                height: '100%',
                color: '#000',
                textAlign: 'left',
              }}
            >
              <p style={{ fontSize: "26px", lineHeight: "27px"}} className="mainQuote">
                {clearText(contentPic.name)}
              </p>
              <p style={{ fontSize: "19px", lineHeight: "24px" }} className="qouteDescr">
                {clearText(contentPic.header)}
              </p>
            </div>
          </div>
        );

      default:
        return null;
    }
  }
}

export default Citate;
