import React, { Component } from 'react';

import Tag from '../Tag';

class DefaultPost extends Component {

	render() {
		const { rowType, contentPic, importedPresetStyle, clearText } = this.props;
		const needUppercase = importedPresetStyle.fontFamily === 'Geometric';

		switch (rowType) {
			case '1_1': return (
				<div style={{ ...importedPresetStyle, height: "480px" }} className="newsBl">
					<div />
					<div className="newsMain" style={{ overflow: "hidden" }}>
						{contentPic.name && <p style={{ fontSize: "66px", lineHeight: "66px", marginBottom: "20px", fontFamily: 'CSTM' }} className="newsMain_title">{clearText(contentPic.name)}</p>}
						{contentPic.header && <p style={{ fontSize: "24px", lineHeight: "32px", fontFamily: 'MullerRegular' }} className="newsMain_newsMain_subtitle">{clearText(contentPic.header)}</p>}
					</div>
					<Tag text={contentPic.iconName} iconType={contentPic.iconCode} {...importedPresetStyle} />
				</div>
			);

			case '1_2': return (
				<div style={{
					...importedPresetStyle, padding: "20px 30px 30px", display: 'flex', flexDirection: 'column', justifyContent: 'space-between', height: '365px'
				}}>
					<div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-between', height: '100%' }}>
						<div className="blTypeStringTop" style={{ padding: "0" }}>
							<div className="blTypeStringH">
								<div className="blTypeStringH_logo">
									<Tag text={contentPic.iconName} iconType={contentPic.iconCode} {...importedPresetStyle} />
								</div>
							</div>
						</div>
						<div className="blHalfDescr" style={{ padding: "0", maxWidth: '400px' }}>
							{contentPic.name && <p style={{ fontFamily: 'MullerBold', lineHeight: "36px", fontSize: '30px', textTransform: needUppercase ? 'uppercase' : 'none' }} className="blHalfDescr_title">{clearText(contentPic.name)}</p>}
							{contentPic.header && <p className="blHalfDescr_subtitle">{clearText(contentPic.header)}</p>}
						</div>
					</div>
				</div >
			)

			case '1_3': return (
				<div className="article center" style={{ ...importedPresetStyle }}>
					<div className="articleDescrW noPic">
						<div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-between', height: '100%' }}>
							<div style={{ marginBottom: '30px' }}>
								<Tag text={contentPic.iconName} iconType={contentPic.iconCode} {...importedPresetStyle} />
							</div>
							<div>
								{contentPic.name && <p style={{ fontFamily: 'MullerBold', lineHeight: "36px", fontSize: '30px' }} className="articleDefaultNoPic">{clearText(contentPic.name)}</p>}
								{contentPic.header && <p className="articleBottom_subtitle">{clearText(contentPic.header)}</p>}
							</div>
						</div>
					</div>
				</div>
			)

			default: return null;
		}


	}
}

export default DefaultPost;
