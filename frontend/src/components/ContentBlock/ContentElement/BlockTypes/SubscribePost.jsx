import React, { Component } from 'react';

class SubscribePost extends Component {

	render() {
		return (
			<div className="subscribe-wrap">
				<div className="subscribe-block">
					<h2 className="subscribe-title">Подпишитесь на нашу рассылку</h2>
					<form action="" className="subscribe-form">
						<input className="subscribe-form-inp" type="email" placeholder="введите ваш email"/>
						<button className="subscribe-form-btn" type="submit">Подписаться</button>
					</form>
				</div>
				<div className="message-block">
					<h2 className="message-title">На вашей почте письмо со ссылкой для подтверждения подписки</h2>
					<button className="message-btn" type="submit">ок</button>
				</div>
			</div>
		);
	}
}

export default SubscribePost;
