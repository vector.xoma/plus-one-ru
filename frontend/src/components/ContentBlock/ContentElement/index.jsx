import React, { Component } from 'react';

import { isEmpty, clearText } from '../../../utils';

import defaultBlock from '../../../img/defaulBlock.png';
import DiyPost from './BlockTypes/DiyPost';
import FactDay from './BlockTypes/FactDay';
import Citate from './BlockTypes/Citate';
import DefaultPost from './BlockTypes/DefaultPost';
import DefaultPostPic from './BlockTypes/DefaultPostPic';

class ContentElement extends Component {

	state = {
		color: null,
		background: null,
		fontFamily: null,
	};

	componentWillUnmount() {
		if (this.span) this.span.remove();
	}

	render() {
		const { contentPic, rowTypeId } = this.props;

		const importedPresetStyle = {
			background: contentPic && contentPic.postFormat ? contentPic.postFormat.backgroundColor : '#000',
			color: contentPic && contentPic.postFormat ? contentPic.postFormat.fontColor : '#fff',
			fontFamily: contentPic ? contentPic.font : 'inherit',
		};

		const backgroundImage = (contentPic && contentPic.imageUrl) ? { backgroundImage: `url(${contentPic.imageUrl})`, backgroundSize: 'cover' } : { backgroundImage: `url(${defaultBlock})`, backgroundSize: '100% 100%' };


		const blockData = {
			rowType: rowTypeId,
			clearText: clearText,
			importedPresetStyle,
			backgroundImage,
			contentPic,
		};

		if (rowTypeId && !isEmpty(contentPic) && contentPic) {
			switch (contentPic.typePost) {
				case 'factday': return <FactDay {...blockData} />;
				case 'imageday':
				case 'diypost': 
					return <DiyPost {...blockData} />;
				case 'citate': return <Citate {...blockData} />;
				case 'post':
					// с картинкой
					if (contentPic.format === 'picture') return <DefaultPostPic {...blockData} />;
					// без картинки
					return <DefaultPost {...blockData} />;

				default: return <DefaultPostPic {...blockData} />
			}
		}

		return (
			<div style={{
				backgroundImage: `url(${defaultBlock})`,
				backgroundSize: '100% 100%',
				height: '100%',
				width: '100 %',
				minHeight: '250px',
			}} />
		)
	}
}

export default ContentElement;
