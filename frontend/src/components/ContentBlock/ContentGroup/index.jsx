import React from "react";
import Slider from "react-slick";

import ContentElement from "../ContentElement";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./styles.scss";

const ContentGroup = ({ content: { picture } }) => {
  const { name, data } = picture;

  const rows = [];
  let tempRows = [];
  data &&
    data.forEach((p, i) => {
      tempRows.push(p);
      if (tempRows.length === 3 || i + 1 === data.length) {
        rows.push(tempRows);
        tempRows = [];
      }
    });

  const settings = {
    dots: true,
    infinite: true,
    speed: 500
  };
  return (
    <div className="sliderWrapper">
      <h2>{name}</h2>
      <Slider {...settings}>
        {rows.length
          ? rows.map((row, i) => (
              <div key={i} className="slideWrapper" style={{ display: "flex" }}>
                {row.map((el, j) => (
                  <div key={j} className="blTypeStringThird">
                    <ContentElement
                      rowTypeId="1_3"
                      contentPic={{
                        ...el,
                        imageUrl: el.image,
                        typePost: el.typePost === "news" ? "block" : el.typePost
                      }}
                    />
                  </div>
                ))}
              </div>
            ))
          : null}
      </Slider>
    </div>
  );
};

export default ContentGroup;
