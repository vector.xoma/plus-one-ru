import React, { Component } from 'react';

import BigInput from './BigInput';

class ContentStretch extends Component {

	render() {
		const { onChange, value } = this.props;

		return (
			<div className="stretchContainer">
				<BigInput onChange={onChange} value={value} />
			</div>
		);
	}
}

export default ContentStretch;
