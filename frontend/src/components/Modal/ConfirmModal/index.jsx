import React, { Component } from 'react';

class ConfirmModal extends Component {
	render() {
		const { removeRow, rejectRemove, title } = this.props;

		return (
			<div className="delModalW show">
				<div className="delModalBg" onClick={rejectRemove} />
				<div className="delModal">
					<div className="delMessage">
						<p>{title}</p>
						<p>(Материалы не удаляются)</p>
					</div>
					<div className="delBtnModalWrap">
						<button className="delBtnModal alert" onClick={removeRow}>Сбросить</button>
						<button className="delBtnModal white" onClick={rejectRemove}>Отмена</button>
					</div>
				</div>
			</div>
		);
	}
}

export default ConfirmModal;
