import React, { Component } from 'react';

import Button from '../../Button';
import { clearText } from '../../../utils';

class ContentModal extends Component {
	state = {
		findValue: '',
	};

	componentDidMount() {
		this.nameInput.focus();
	}

	componentWillUnmount() {
		if (this.span) this.span.remove();
	}

	span = document.createElement('span');

	convertHTMLEntity = (text) => {
		return text
			.replace(/&[#A-Za-z0-9]+;/gi, (entity, position, text) => {
				this.span.innerHTML = entity;
				return this.span.innerText;
			});
	};

	clearText = (text) => text ? this.convertHTMLEntity(text.replace(/<\/?.+?>/g, '')) : '';

	listRow = (element, type) => {
		let choosen = null;

		if (type === 'group') {
			choosen = element.enabled
				? (<Button classes="adminHBtn--white chooseContentBtn" onClickHandler={() => {this.props.selectContentFromModal(element, type)}}>Выбрать</Button>)
				: (<div className="adminHBtn--disabled" style={{textAlign: 'center'}}>Отключена</div>);
		} else {
			choosen = element.writersEnabled === "1"
				? (<Button classes="adminHBtn--white chooseContentBtn" onClickHandler={() => {this.props.selectContentFromModal(element, type)}}>Выбрать</Button>)
				: (<div className="adminHBtn--disabled">Автор неактивен</div>);
		}

		let searchClass = 'searchElemW';
		searchClass += element.writersEnabled === "1" ? '' : ' author--disabled';
		return (
			<li className={searchClass} key={element.id}>
				<p className="searchDescr">{clearText(element.name)}</p>
				<p className="searchData">{element.date}</p>
				{choosen}
			</li>
		)
	};

	render() {
		const { hideContentModal, contentList, contentType } = this.props;
		const { findValue } = this.state;

		// фильтрация списка по введённому значению
		const newContentList = contentList ? contentList.filter(content => content.name.toLowerCase().includes(findValue.toLowerCase())) : [];

		return (

			<div className="modalAddContentW show">
				<div className="modalAddContenBg" onClick={hideContentModal} />
				<div className="searchWrapper">
					<div className="modalAddContent">
						<div className="searchInputW">
							<input
								type="text"
								className="searchInput"
								placeholder="Введите значение"
								value={findValue}
								onChange={(e) => { this.setState({ findValue: e.target.value }) }}
								ref={(input) => { this.nameInput = input; }}
							/>
						</div>
						<ul className="resultSerch">
							{contentType === 'block' && newContentList.map(el => this.listRow(el, 'block'))}
							{contentType === 'group' && newContentList.map(el => this.listRow(el, 'group'))}
						</ul>
					</div>
					<p className="modalContentCloseBtn" onClick={hideContentModal} />
				</div>
			</div>
		);
	}
}

export default ContentModal;
