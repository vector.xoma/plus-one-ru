export const arrayMove = (arr, oldIndex, newIndex) => {
	if (newIndex >= arr.length) {
		let k = newIndex - arr.length + 1;
		while (k--) { arr.push(undefined); }
	}
	arr.splice(newIndex, 0, arr.splice(oldIndex, 1)[0]);
};


export const isEmpty = (obj) => {
	for (var key in obj) return false;
	return true;
};

// заполнение данными контента в строке
export const setFieldsOnPic = (from) => {
	if (!from) return {};

	if (from.block === 'ticker') return {
		stretchValue: clearText(from.name),
		typePost: 'ticker',
		id: from.id
	};

	return {
		name: from.name,
		header: from.header,
		url: from.url,
		date: from.date,
		imageUrl: from.image,
		postFormat: from.postFormat,
		typePost: from.typePost || 'post',
		font: from.font,
		iconName: from.iconName,
		iconCode: from.iconCode,
		id: from.id,
		format: from.format
	};
};

export const  convertHTMLEntity = (text) => {
	return text
		.replace(/&[#A-Za-z0-9]+;/gi, (entity, position, text) => {
			let span = document.createElement('span');
			span.innerHTML = entity;
			return span.innerText;
		});
};

export const  clearText = (text) => {
	return text ? convertHTMLEntity(text.replace(/<\/?.+?>/g, '')) : '';
};