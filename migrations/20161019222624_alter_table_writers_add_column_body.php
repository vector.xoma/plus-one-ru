<?php

use Phinx\Migration\AbstractMigration;

class AlterTableWritersAddColumnBody extends AbstractMigration
{
    private $tablename = 'blogwriters';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('body', 'text', ['null' => true, 'default' => null])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('body')
            ->save();
    }
}
