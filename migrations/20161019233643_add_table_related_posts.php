<?php

use Phinx\Migration\AbstractMigration;

class AddTableRelatedPosts extends AbstractMigration
{
    private $tablename = 'related_posts';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('parent_id', 'integer', ['limit'=>11, 'null' => false])
            ->addColumn('post_id', 'integer', ['limit'=>11, 'null' => false])
            ->addIndex(array('parent_id', 'post_id'), array('unique'=>true))
            ->save();
    }

    public function down()
    {
        $this->dropTable($this->tablename);
    }
}
