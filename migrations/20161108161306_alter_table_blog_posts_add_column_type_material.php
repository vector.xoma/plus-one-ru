<?php

use Phinx\Migration\AbstractMigration;

class AlterTableBlogPostsAddColumnTypeMaterial extends AbstractMigration
{
    private $tablename = 'blogposts';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('type_material', 'integer', ['null' => false, 'default' => 0])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('type_material')
            ->save();
    }
}
