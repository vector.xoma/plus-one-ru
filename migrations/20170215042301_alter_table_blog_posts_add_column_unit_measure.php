<?php

use Phinx\Migration\AbstractMigration;

class AlterTableBlogPostsAddColumnUnitMeasure extends AbstractMigration
{
    private $tablename = 'blogposts';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('unit_measure', 'integer', ['null' => false, 'default' => 0])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('unit_measure')
            ->save();
    }
}
