<?php

use Phinx\Migration\AbstractMigration;

class CreateTableEvents extends AbstractMigration
{
    private $tablename = 'events';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('tag', 'integer', ['null' => false])
            ->addColumn('header', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('url', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('lead', 'text', ['null' => false])
            ->addColumn('body', 'text', ['null' => true])
            ->addColumn('image', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('enabled', 'integer', ['null' => false, 'default' => 0])
            ->addColumn('created', 'datetime', ['null' => false, 'default' => null])
            ->addColumn('modified', 'datetime', ['null' => false, 'default' => null])
            ->save();
    }

    public function down()
    {
        $this->dropTable($this->tablename);
    }
}
