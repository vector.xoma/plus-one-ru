<?php

use Phinx\Migration\AbstractMigration;

class AddFieldHeaderOnPostTableBlogs extends AbstractMigration
{
    private $tablename = 'blogposts';

    public function up()
    {
        $this->table($this->tablename)
                ->addColumn('header_on_page', 'text', ['null' => true, 'default' => null])
                ->save();
    }

    public function down()
    {
        $this->table($this->tablename)->removeColumn('header_on_page')->save();
    }
}
