<?php

use Phinx\Migration\AbstractMigration;

class AlterTableBlogpostsAddUsefulCity extends AbstractMigration
{
    private $tablename = 'blogposts';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('useful_city', 'integer', ['null' => false, 'default' => 0])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('useful_city')
            ->save();
    }
}
