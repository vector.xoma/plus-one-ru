<?php

use Phinx\Migration\AbstractMigration;

class AddTableFund extends AbstractMigration
{
    private $tablename = 'fund';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('name', 'string', array('limit'=>255, 'null' => false))
            ->addColumn('shop_article_id', 'integer', array('limit'=>11, 'null' => false))
            ->addColumn('created', 'datetime', array('null' => false, 'default' => null))
            ->addColumn('modified', 'datetime', array('null' => false, 'default' => null))
            ->addIndex('shop_article_id', array('unique'=>true))
            ->save();
    }

    public function down()
    {
        $this->dropTable($this->tablename);
    }
}
