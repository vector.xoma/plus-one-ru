<?php

use Phinx\Migration\AbstractMigration;

class AlterTableBlogPostsAddColumnShopArticleId extends AbstractMigration
{
    private $tablename = 'blogposts';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('shop_article_id', 'integer', array('limit'=>11))
            ->addColumn('show_fund', 'boolean', array('default' => false))
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('shop_article_id')
            ->removeColumn('show_fund')
            ->save();
    }
}
