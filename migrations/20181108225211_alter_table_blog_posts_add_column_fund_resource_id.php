<?php

use Phinx\Migration\AbstractMigration;

class AlterTableBlogPostsAddColumnFundResourceId extends AbstractMigration
{
    private $tablename = 'blogposts';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('fund_resource_id', 'integer', array('limit'=>11))
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('fund_resource_id')
            ->save();
    }
}
