<?php

use Phinx\Migration\AbstractMigration;

class AlterTableFundAddColumnResourceId extends AbstractMigration
{
    private $tablename = 'fund';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('resource_id', 'integer', array('limit'=>11))
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('resource_id')
            ->save();
    }
}
