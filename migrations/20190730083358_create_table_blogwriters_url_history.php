<?php


use Phinx\Migration\AbstractMigration;

class CreateTableBlogwritersUrlHistory extends AbstractMigration
{

    private $tablename = 'blogwriters_url_history';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('url', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('writers', 'integer', array('limit'=>11, 'null' => false))
            ->addColumn('users_admin_id', 'integer', ['limit' => 11, 'null' => true])
            ->addColumn('created', 'datetime', ['null' => true, 'default' => null])
            ->addColumn('modified', 'datetime', ['null' => true, 'default' => null])
            ->create();
    }

    public function down()
    {
        $this->dropTable($this->tablename);
    }
}
