<?php


use Phinx\Migration\AbstractMigration;

class AddFieldBlogwriters extends AbstractMigration
{

    private $tablename = 'blogwriters';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('tag_url', 'string', ['limit' => 255, 'null' => true])
            ->save();
    }

    public function down()
    {
        $table = $this->table($this->tablename);
        $table->removeColumn('tag_url')
            ->save();
    }
}
