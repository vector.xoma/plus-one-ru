<?php


use Phinx\Migration\AbstractMigration;

class CreateTableBlogpostsUrlHistory extends AbstractMigration
{
    private $tablename = 'blogposts_url_history';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('url', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('blogpost_id', 'integer', ['limit' => 11, 'null' => false])
            ->addColumn('users_admin_id', 'integer', ['limit' => 11, 'null' => true])
            ->addColumn('enabled', 'integer', ['limit' => 1, 'default' => 0])
            ->addColumn('created', 'datetime', ['null' => true, 'default' => null])
            ->addColumn('modified', 'datetime', ['null' => true, 'default' => null])
            ->addIndex('blogpost_id', array('name' => 'ind_blogposts_url_history_blogpost_id'))
            ->addIndex('url', array('name' => 'ind_blogposts_url_history_url', 'unique' => true))
            ->addIndex('enabled', array('name' => 'ind_blogposts_url_history_enabled'))
            ->create();
    }

    public function down()
    {
        $this->dropTable($this->tablename);
    }
}
