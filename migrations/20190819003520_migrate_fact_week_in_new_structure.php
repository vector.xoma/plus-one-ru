<?php


use Phinx\Migration\AbstractMigration;

class MigrateFactWeekInNewStructure extends AbstractMigration
{

    public function up()
    {
        $this->table('blogposts')
            ->changeColumn('name', 'string', ['limit'=>511, 'null' => false])
            ->save();
        $this->execute("
            UPDATE blogposts bp 
            SET bp.name = CONCAT(bp.amount_to_displaying,' ', bp.signature_to_sum, ' ', bp.text_body)
            WHERE bp.type_post = 7
        ");
    }
    public function down()
    {
        $this->table('blogposts')
            ->changeColumn('header', 'string', ['limit'=>255, 'null' => false])
            ->save();
    }
}
