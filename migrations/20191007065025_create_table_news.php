<?php


use Phinx\Migration\AbstractMigration;

class CreateTableNews extends AbstractMigration
{
    private $tablename = 'news';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('url', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('tags', 'integer', ['limit' => 1, 'null' => false])

            ->addColumn('header', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('header_social', 'string', ['limit' => 140, 'null' => false])
            ->addColumn('created', 'datetime', ['null' => true, 'default' => null])
            ->addColumn('modified', 'datetime', ['null' => true, 'default' => null])
            ->addColumn('writers', 'integer', ['limit' => 11, 'null' => false])
            ->addColumn('enabled', 'integer', ['limit' => 1, 'default' => 0])
            ->addColumn('publish_news_aggregators', 'integer', ['limit' => 1, 'default' => 1])
            ->addColumn('publish_news_zen_pulse', 'integer', ['limit' => 1, 'default' => 1])

            ->addColumn('author_signature', 'string', ['limit' => 255, 'null' => false, 'default' => 'Автор'])
            ->addColumn('author_name', 'string', ['limit' => 255, 'null' => true])

            ->addColumn('text_content', 'text', ['null' => true])
            ->addColumn('lead', 'text', ['null' => true])
            ->addColumn('article_text', 'text', ['null' => true])

            ->addColumn('meta_title', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('meta_description', 'text', ['null' => true])
            ->addColumn('meta_keywords', 'string', ['limit' => 255, 'null' => true])

            ->addIndex('url', array('name' => 'ind_blogposts_url_history_url', 'unique' => true))
            ->addIndex('enabled', array('name' => 'ind_blogposts_url_history_enabled'))
            ->create();
    }

    public function down()
    {
        $this->dropTable($this->tablename);
    }
}
