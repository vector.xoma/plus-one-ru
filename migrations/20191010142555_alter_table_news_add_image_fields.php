<?php


use Phinx\Migration\AbstractMigration;

class AlterTableNewsAddImageFields extends AbstractMigration
{
    private $tablename = 'news';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('image_rss', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('image_1_1', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('image_1_2', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('image_1_3', 'string', ['limit' => 255, 'null' => true])
            ->save();
    }

    public function down()
    {
        $table = $this->table($this->tablename);
        $table->removeColumn('image_rss')->save();
        $table->removeColumn('image_1_1')->save();
        $table->removeColumn('image_1_2')->save();
        $table->removeColumn('image_1_3')->save();
    }
}
