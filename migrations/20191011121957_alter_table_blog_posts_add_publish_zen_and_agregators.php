<?php


use Phinx\Migration\AbstractMigration;

class AlterTableBlogPostsAddPublishZenAndAgregators extends AbstractMigration
{
    private $tablename = 'blogposts';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('publish_news_aggregators', 'string', ['limit' => 255, 'null' => true, 'default' => 0])
            ->addColumn('publish_news_zen_pulse', 'string', ['limit' => 255, 'null' => true, 'default' => 1])
            ->save();
    }

    public function down()
    {
        $table = $this->table($this->tablename);
        $table->removeColumn('publish_news_aggregators')->save();
        $table->removeColumn('publish_news_zen_pulse')->save();
    }
}
