<?php


use Phinx\Migration\AbstractMigration;

class CreateTableRelationsNewsToTags extends AbstractMigration
{
    private $tablename = 'news_to_tags_relations';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('post_tag_id', 'integer', ['limit'=>11, 'null' => false])
            ->addColumn('news_id', 'integer', ['limit'=>11, 'null' => false])
            ->save();
    }

    public function down()
    {
        $this->dropTable($this->tablename);
    }
}
