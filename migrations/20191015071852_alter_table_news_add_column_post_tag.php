<?php


use Phinx\Migration\AbstractMigration;

class AlterTableNewsAddColumnPostTag extends AbstractMigration
{
    private $tablename = 'news';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('post_tag', 'integer', ['limit' => 11, 'null' => true])->save();
    }

    public function down()
    {
        $table = $this->table($this->tablename);
        $table->removeColumn('post_tag')->save();
    }
}
