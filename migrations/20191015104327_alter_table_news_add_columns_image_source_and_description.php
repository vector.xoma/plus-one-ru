<?php


use Phinx\Migration\AbstractMigration;

class AlterTableNewsAddColumnsImageSourceAndDescription extends AbstractMigration
{
    private $tablename = 'news';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('image_rss_description', 'string', ['limit'=>255, 'null' => true, 'default' => null])
            ->addColumn('image_rss_source', 'string', ['limit'=>255, 'null' => true, 'default' => null])
            ->save();
    }

    public function down()
    {
        $table = $this->table($this->tablename);
        $table->removeColumn('image_rss_source')->save();
        $table->removeColumn('image_rss_description')->save();
    }
}
