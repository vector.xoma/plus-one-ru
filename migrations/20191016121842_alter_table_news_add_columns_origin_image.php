<?php


use Phinx\Migration\AbstractMigration;

class AlterTableNewsAddColumnsOriginImage extends AbstractMigration
{
    private $tablename = 'news';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('origin_image', 'string', ['limit'=>255, 'null' => true, 'default' => null])
            ->save();
    }

    public function down()
    {
        $table = $this->table($this->tablename);
        $table->removeColumn('origin_image')->save();
    }
}
