<?php


use Phinx\Migration\AbstractMigration;

class AlterTableBlogpostsUpdateFontForCitate extends AbstractMigration
{
    private $tablename = 'blogposts';

    public function up()
    {
        $this->execute("UPDATE {$this->tablename} SET font = NULL WHERE type_post = 8");
    }

    public function down()
    {
        $this->execute("UPDATE {$this->tablename} SET font = 'MullerBold' WHERE type_post = 8");
    }
}
