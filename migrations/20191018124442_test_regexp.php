<?php


use Phinx\Migration\AbstractMigration;

class TestRegexp extends AbstractMigration
{
    private $tablename = 'blogposts';
    private $logTable = 'images_posts_replaced';

    public function up()
    {
        $replacedData = [];

        $idList = $this->query("SELECT id
            FROM {$this->tablename}
            WHERE type_post != 10
              AND created < '2019-09-05 00:00:00'
              AND image_rss IS NOT NULL")
            ->fetchAll(PDO::FETCH_ASSOC);

        foreach ($idList as $postId) {
            $postBody = $this->query("SELECT body FROM {$this->tablename} WHERE id = {$postId['id']}")->fetch(PDO::FETCH_ASSOC);
            // Нахожу первое изображение
            if (preg_match('/<figure (?:[^>]*)><img src="(?P<url>(?:.[^"]*?))"/i', $postBody['body'],$matches)) {
                $stringToReplace = $matches['url'];
                // Проверяю что изображение нуждается в замене
                if (preg_match('/(?P<url>(?:.[^"]*?)(?:[а-яА-Я+_)(*&^%$#@!~ ]+)(?:[^"]*))/i', $stringToReplace) &&
                    !stristr($stringToReplace, 'files/photogallerys/') &&
                    $stringToReplace != 'https://plus-one.ru/files/image/ivsezaodnogo.ru.png') {
                    $imageRss = $this->query("SELECT image_rss FROM {$this->tablename} WHERE id = {$postId['id']}")->fetch(PDO::FETCH_ASSOC);
                    $imageRssPath = '/files/blogposts/' . $imageRss['image_rss'];
                    // Замена изображения в статье
                    $this->execute("UPDATE {$this->tablename}
                      SET body = REPLACE(body, '{$stringToReplace}', '{$imageRssPath}')
                      WHERE id = {$postId['id']};");
                    //Логирование
                    $replacedData[] = ['post_id' => $postId['id'], 'replaced_url' => $stringToReplace, 'new_url' => $imageRssPath];
                }
            }
        }
        $this->table($this->logTable)
            ->addColumn('post_id', 'integer', ['limit'=>11, 'null' => false])
            ->addColumn('replaced_url', 'string', ['limit'=>255, 'null' => false])
            ->addColumn('new_url', 'string', ['limit'=>255, 'null' => false])
            ->save();
        $this->table($this->logTable)->insert($replacedData)->save();
    }

    public function down()
    {
        $dataList = $this->query("SELECT *
            FROM {$this->logTable}")
            ->fetchAll(PDO::FETCH_ASSOC);

        foreach ($dataList as $item) {
            $this->execute("UPDATE {$this->tablename}
              SET body = REPLACE(body, '{$item['new_url']}', '{$item['replaced_url']}')
              WHERE id = {$item['post_id']};");
        }
        $this->dropTable($this->logTable);
    }
}
