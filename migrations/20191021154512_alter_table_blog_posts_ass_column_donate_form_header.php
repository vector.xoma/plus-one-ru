<?php


use Phinx\Migration\AbstractMigration;

class AlterTableBlogPostsAssColumnDonateFormHeader extends AbstractMigration
{
    private $tablename = 'blogposts';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('header_donate_form', 'string', ['limit'=>50, 'null' => false])
            ->save();
    }

    public function down()
    {
        $table = $this->table($this->tablename);
        $table->removeColumn('origin_image')->save();
    }
}
