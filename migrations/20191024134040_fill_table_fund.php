<?php


use Phinx\Migration\AbstractMigration;

class FillTableFund extends AbstractMigration
{
    private $tablename = 'fund';

    public function up()
    {
        $dateNow = date('Y-m-d H:i:s', time());

        $data = [
            ['shop_article_id' => 132671, 'name' => 'Улыбка ребёнка', 'resource_id' => 277, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132743, 'name' => 'Счастливый мир', 'resource_id' => 352, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132829, 'name' => 'АНО "Центр социальной помощи Опека"', 'resource_id' => 440, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132613, 'name' => 'Надежда, Ивановская организация инвалидов-опорников', 'resource_id' => 217, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132586, 'name' => 'ДетскиеДомики', 'resource_id' => 180, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132623, 'name' => 'Фонд благотворительной помощи детям-сиротам и инвалидам "Димина Мечта"', 'resource_id' => 229, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 183714, 'name' => 'Елизаветинский детский хоспис', 'resource_id' => 529, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 183726, 'name' => 'Православная служба помощи "Милосердие"', 'resource_id' => 560, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132430, 'name' => 'Одухотворение', 'resource_id' => 18, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132576, 'name' => 'Иллюстрированные книжки для маленьких слепых детей', 'resource_id' => 170, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132629, 'name' => 'Подари мне жизнь', 'resource_id' => 235, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132686, 'name' => 'Домик детства', 'resource_id' => 292, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132756, 'name' => 'Мир для всех', 'resource_id' => 366, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132833, 'name' => 'Благотворительный фонд "Неопалимая Купина"', 'resource_id' => 444, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 151299, 'name' => 'Художники и Дети', 'resource_id' => 504, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 152216, 'name' => 'БФ "Ваша надежда"', 'resource_id' => 507, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 151303, 'name' => 'РООИ "Перспектива"', 'resource_id' => 526, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 151300, 'name' => 'Владимирские дети-ангелы', 'resource_id' => 509, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 183717, 'name' => 'Центр "Мастер ОК"', 'resource_id' => 542, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132562, 'name' => 'АиФ. Доброе сердце', 'resource_id' => 156, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132638, 'name' => 'Партнерство каждому ребенку', 'resource_id' => 244, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132826, 'name' => 'Благотворительный фонд "Теплое детство"', 'resource_id' => 437, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132421, 'name' => 'Десятина', 'resource_id' => 9, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132790, 'name' => 'МРБООИ "Союз пациентов и пациентских организаций по редким заболеваниям"', 'resource_id' => 400, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132514, 'name' => 'Солнце', 'resource_id' => 105, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132723, 'name' => 'Старость в радость', 'resource_id' => 330, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132772, 'name' => 'Шаг вместе', 'resource_id' => 382, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132729, 'name' => 'Седьмой лепесток, Благотворительный фонд', 'resource_id' => 337, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 151298, 'name' => 'Благотворительный фонд "Наши Дети"', 'resource_id' => 492, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 204203, 'name' => 'НБФ "Дети и Родители" против рака"', 'resource_id' => 579, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 151302, 'name' => 'Кузбасский благотворительный фонд "Детское сердце"', 'resource_id' => 525, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 152235, 'name' => 'Благотворительный фонд "Социально-правовой щит"', 'resource_id' => 538, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 183718, 'name' => 'БФ "Помоги ребенку.ру"', 'resource_id' => 546, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 183724, 'name' => 'Благотворительный фонд "Арифметика добра"', 'resource_id' => 558, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 809020, 'name' => 'Благотворительный фонд помощи животным «Всем Миром»', 'resource_id' => 3641, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 808520, 'name' => 'Благотворительный фонд "Нижегородский"', 'resource_id' => 684, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132526, 'name' => 'Благотворительный Фонд "ГАЛЧОНОК"', 'resource_id' => 651, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132721, 'name' => 'Благотворительный фонд "Шередарь"', 'resource_id' => 672, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132566, 'name' => 'БФ "Шаг навстречу"', 'resource_id' => 160, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 183734, 'name' => 'Фонд продовольствия "Русь"', 'resource_id' => 565, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132481, 'name' => 'Дети без мам', 'resource_id' => 71, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 151297, 'name' => 'Общественная организация "Надежда"', 'resource_id' => 470, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132543, 'name' => 'Б.Э.Л.А. Дети-бабочки', 'resource_id' => 134, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132724, 'name' => 'Возрождение, МБОО"', 'resource_id' => 331, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132596, 'name' => 'Благотворительный фонд "Родительский мост"', 'resource_id' => 679, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 151305, 'name' => 'Благотворительный фонд "Протяни Руку"', 'resource_id' => 535, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132604, 'name' => 'Ночлежка', 'resource_id' => 199, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132607, 'name' => 'Благотворительный фонд помощи хосписам "Вера"', 'resource_id' => 206, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132601, 'name' => 'Новая Надежда', 'resource_id' => 196, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132670, 'name' => 'Настенька', 'resource_id' => 276, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132815, 'name' => 'Фонд "ОРБИ"', 'resource_id' => 425, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132825, 'name' => 'Дом трудолюбия Ной', 'resource_id' => 436, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 151306, 'name' => 'Благотворительный фонд "Большая Перемена"', 'resource_id' => 536, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132781, 'name' => 'Благотворительный фонд "Дети наши"', 'resource_id' => 391, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132504, 'name' => 'Благотворительный фонд ЦФО', 'resource_id' => 95, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 152233, 'name' => 'Благотворительный фонд "ЖИВИ"', 'resource_id' => 533, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 151304, 'name' => 'Благотворительный центр помощи детям "Радуга"', 'resource_id' => 534, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132634, 'name' => 'Адели', 'resource_id' => 240, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132432, 'name' => 'Международный благотворительный центр "Надежда"', 'resource_id' => 20, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132714, 'name' => 'Благотворительный фонд "София"', 'resource_id' => 320, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132488, 'name' => 'Дарина', 'resource_id' => 79, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132688, 'name' => 'Берег надежды', 'resource_id' => 294, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132818, 'name' => 'Бюро добрых дел', 'resource_id' => 428, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132528, 'name' => 'Жизнь как чудо', 'resource_id' => 119, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132587, 'name' => 'Добросердие', 'resource_id' => 181, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132508, 'name' => 'Точка опоры', 'resource_id' => 99, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 204210, 'name' => 'Благотворительный детский фонд "Мы вместе"', 'resource_id' => 585, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 151301, 'name' => 'Старшие Братья Страшие Сёстры', 'resource_id' => 523, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132800, 'name' => 'Благотворительный фонд "Берегиня"', 'resource_id' => 410, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132799, 'name' => 'Некоммерческая организация "Детский проект"', 'resource_id' => 409, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 152234, 'name' => 'БФ "Пища жизни"', 'resource_id' => 537, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 808540, 'name' => 'БФ "В твоих руках"', 'resource_id' => 696, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 808400, 'name' => 'БФ "ПОДАРОК АНГЕЛУ"', 'resource_id' => 631, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 151296, 'name' => 'Общественная организация "Метелица"', 'resource_id' => 467, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 132608, 'name' => 'Благо Дарю', 'resource_id' => 209, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 808325, 'name' => 'Фонд поддержки слепоглухих "Со-единение"', 'resource_id' => 596, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 808345, 'name' => 'БФ "СТРАНА ЧУДЕС"', 'resource_id' => 613, 'created' => $dateNow, 'modified' => $dateNow],
            ['shop_article_id' => 152217, 'name' => 'Цвет Жизни ', 'resource_id' => 508, 'created' => $dateNow, 'modified' => $dateNow],
        ];

        $table = $this->table($this->tablename);
        $table->truncate();
        $table->insert($data)->save();
    }

    public function down()
    {

    }
}
