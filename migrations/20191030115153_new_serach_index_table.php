<?php


use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class NewSerachIndexTable extends AbstractMigration
{
    private $tablename = 'blogposts_search_index';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('content_id', 'integer', ['limit' => 11, 'null' => false])
            ->addColumn('content_type', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('created', 'datetime', ['null' => true, 'default' => null])
            ->addColumn('modified', 'datetime', ['null' => true, 'default' => null])
            ->addColumn('clear_full_text', 'text', ['limit' => MysqlAdapter::TEXT_LONG, 'null' => true, 'default' => null, ])
            ->addIndex('content_id', array('name' => 'ind_search_index_content_id'))
            ->create();
    }

    public function down()
    {
        $this->dropTable($this->tablename);
    }
}
