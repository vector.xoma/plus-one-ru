<?php

use Phinx\Migration\AbstractMigration;

class CreateTableGroups extends AbstractMigration
{
    private $tablename = 'groups';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('name', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('created', 'datetime', ['null' => true, 'default' => null])
            ->addColumn('modified', 'datetime', ['null' => true, 'default' => null])
            ->addColumn('creator_id', 'integer', ['limit' => 11, 'null' => true, 'default' => null, ])
            ->addColumn('enabled', 'integer', ['limit' => 1, 'null' => true, 'default' => null, ])
            ->create();
    }

    public function down()
    {
        $this->dropTable($this->tablename);
    }
}
