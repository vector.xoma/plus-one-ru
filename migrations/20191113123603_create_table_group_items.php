<?php


use Phinx\Migration\AbstractMigration;

class CreateTableGroupItems extends AbstractMigration
{
    private $tablename = 'group_items';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('content_type', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('group_id', 'integer', ['limit' => 11, 'null' => false ])
            ->addColumn('content_id', 'integer', ['limit' => 11, 'null' => false ])
            ->addColumn('created', 'datetime', ['null' => true, 'default' => null])
            ->addColumn('modified', 'datetime', ['null' => true, 'default' => null])
            ->addColumn('enabled', 'integer', ['limit' => 1, 'null' => true, 'default' => null, ])
            ->addColumn('order_num', 'integer', ['limit' => 11, 'null' => true, 'default' => null, ])
            ->addIndex('content_id', array('name' => 'ind_group_item_content_id'))
            ->addIndex('group_id', array('name' => 'ind_group_item_group_id'))
            ->create();
    }

    public function down()
    {
        $this->dropTable($this->tablename);
    }
}
