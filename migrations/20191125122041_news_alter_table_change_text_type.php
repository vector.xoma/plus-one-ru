<?php


use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;


class NewsAlterTableChangeTextType extends AbstractMigration
{
    private $tablename = 'news';

    public function up()
    {
        $this->table($this->tablename)
            ->changeColumn('article_text', 'text', [
                'encoding' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'limit'=>MysqlAdapter::TEXT_LONG, 'null' => false])
            ->changeColumn('lead', 'text', [
                'encoding' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'limit'=>MysqlAdapter::TEXT_LONG, 'null' => false])
            ->changeColumn('text_content', 'text', [
                'encoding' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'limit'=>MysqlAdapter::TEXT_LONG, 'null' => false])
            ->changeColumn('header', 'string', [
                'encoding' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'limit'=>255,
                'null' => false])
            ->save();

        $this->table('blogposts')
            ->changeColumn('body', 'text', [
                'encoding' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'limit'=>MysqlAdapter::TEXT_LONG, 'null' => false])
            ->changeColumn('lead', 'text', [
                'encoding' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'limit'=>MysqlAdapter::TEXT_LONG, 'null' => false])
            ->changeColumn('header', 'text', [
                'encoding' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'null' => false])
            ->save();
    }

    public function down()
    {
    }

}
