<?php


use Phinx\Migration\AbstractMigration;

class AlterTableBlogPostsAddColumnPartnerMaterial extends AbstractMigration
{
    private $tablename = 'blogposts';
    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('is_partner_material', 'integer', ['limit' => 1, 'default' => 0])
            ->save();
    }
    public function down()
    {
        $table = $this->table($this->tablename);
        $table->removeColumn('is_partner_material')->save();
    }
}
