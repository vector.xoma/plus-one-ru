<?php


use Phinx\Migration\AbstractMigration;

class AlterTableBlogwritersAddColumnAnnouncementImg extends AbstractMigration
{
    private $tablename = 'blogwriters';
    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('announcement_img', 'string', ['limit'=>255, 'null' => true, 'default' => null])
            ->save();
    }
    public function down()
    {
        $table = $this->table($this->tablename);
        $table->removeColumn('announcement_img')->save();
    }
}
