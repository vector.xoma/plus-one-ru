<?php


use Phinx\Migration\AbstractMigration;

class AlterTableRelatedPosts extends AbstractMigration
{
    private $tablename = 'related_posts';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('post_type', 'integer', ['null' => true, 'default' => null])
            ->addColumn('parent_type', 'string', ['limit'=>11, 'null' => true, 'default' => null])
            ->save();

        $this->query("UPDATE {$this->tablename} SET parent_type = 'post' WHERE parent_type IS NULL ;");
    }

    public function down()
    {
        $table = $this->table($this->tablename);
        $table->removeColumn('post_type')->save();
        $table->removeColumn('parent_type')->save();
    }
}
