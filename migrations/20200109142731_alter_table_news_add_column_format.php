<?php


use Phinx\Migration\AbstractMigration;

class AlterTableNewsAddColumnFormat extends AbstractMigration
{
    protected $tablename = 'news';
    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('format', 'string', ['limit'=>20, 'null' => true, 'default' => null])
            ->save();
    }
    public function down()
    {
        $table = $this->table($this->tablename);
        $table->removeColumn('format')->save();
    }
}
