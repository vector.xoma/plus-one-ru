<?php


use Phinx\Migration\AbstractMigration;

class AlterTableNewsAddTypesAnnounce extends AbstractMigration
{
    protected $tablename = 'news';
    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('type_announce_1_1', 'string', ['limit'=>10, 'null' => true, 'default' => null])
            ->addColumn('type_announce_1_2', 'string', ['limit'=>10, 'null' => true, 'default' => null])
            ->addColumn('type_announce_1_3', 'string', ['limit'=>10, 'null' => true, 'default' => null])
            ->save();
    }
    public function down()
    {
        $table = $this->table($this->tablename);
        $table
            ->removeColumn('type_announce_1_1')
            ->removeColumn('type_announce_1_2')
            ->removeColumn('type_announce_1_3')
            ->save();
    }
}
