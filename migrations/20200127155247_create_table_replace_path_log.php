<?php


use Phinx\Migration\AbstractMigration;

class CreateTableReplacePathLog extends AbstractMigration
{
    private $tablename = 'replace_path_log';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('old_path', 'string', ['limit' => 511, 'null' => false ])
            ->addColumn('new_path', 'string', ['limit' => 511, 'null' => false ])
            ->addColumn('item_id', 'integer', ['limit' => 11, 'null' => false])
            ->addColumn('item_type', 'string', ['limit' => 255, 'null' => false ])
            ->addColumn('status', 'integer', ['limit' => 11, 'null' => true, 'default' => null, ])
            ->addColumn('created', 'datetime', ['null' => true, 'default' => null])
            ->addColumn('params', 'blob', ['null' => true, 'default' => null])
            ->addIndex('old_path', array('name' => 'ind_old_path_path_id'))
            ->create();
    }

    public function down()
    {
        $this->dropTable($this->tablename);
    }
}
