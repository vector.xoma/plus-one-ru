<?php


use Phinx\Migration\AbstractMigration;

class CreateTableOptimizedLog extends AbstractMigration
{
    private $tablename = 'optimizer_log';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('item_id', 'integer', ['limit' => 11, 'null' => false])
            ->addColumn('item_type', 'string', ['limit' => 255, 'null' => false ])
            ->addColumn('image', 'string', ['limit' => 511, 'null' => false ])
            ->addColumn('image_orig', 'string', ['limit' => 511, 'null' => true ])
            ->addColumn('size', 'integer', ['limit' => 11, 'null' => false ])
            ->addColumn('size_orig', 'integer', ['limit' => 11, 'null' => true ])
            ->addColumn('status', 'integer', ['limit' => 11, 'null' => true, 'default' => null, ])
            ->addColumn('created', 'datetime', ['null' => true, 'default' => null])
            ->addIndex('image', array('name' => 'ind_image_path_id'))
            ->create();
    }

    public function down()
    {
        $this->dropTable($this->tablename);
    }
}
