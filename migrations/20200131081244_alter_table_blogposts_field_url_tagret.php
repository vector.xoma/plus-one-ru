<?php


use Phinx\Migration\AbstractMigration;

class AlterTableBlogpostsFieldUrlTagret extends AbstractMigration
{
    protected $tablename = 'blogposts';
    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('url_target', 'string', ['limit'=>127, 'null' => true, 'default' => null])
            ->save();
    }
    public function down()
    {
        $table = $this->table($this->tablename);
        $table
            ->removeColumn('url_target')
            ->save();
    }
}
