<?php


use Phinx\Migration\AbstractMigration;

class CreateTableSettingWidgets extends AbstractMigration
{
    private $tablename = 'setting_widgets';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('widget_name', 'string', ['limit' => 511, 'null' => false])
            ->addColumn('wrap_class', 'string', ['limit' => 511, 'null' => false])
            ->addColumn('widget_code', 'text', [ 'null' => true ])
            ->addColumn('enabled', 'integer', ['limit' => 1, 'null' => true, 'default' => 0 ])
            ->addColumn('created', 'datetime', ['null' => true, 'default' => null])
            ->addColumn('order_num', 'integer', ['limit' => 11, 'null' => true ])
            ->create();
    }

    public function down()
    {
        $this->dropTable($this->tablename);
    }
}
