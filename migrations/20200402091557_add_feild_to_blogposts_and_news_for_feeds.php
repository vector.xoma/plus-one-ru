<?php

use Phinx\Db\Adapter\MysqlAdapter;
use Phinx\Migration\AbstractMigration;

class AddFeildToBlogpostsAndNewsForFeeds extends AbstractMigration
{
    protected $tablename = 'blogposts';

    public function up()
    {
        $this->table('blogposts')
            ->addColumn('feed_header_html', 'text', ['limit' => MysqlAdapter::TEXT_LONG, 'null' => true, 'default' => null, 'encoding' => 'utf8mb4', 'collation' => 'utf8mb4_unicode_ci',])
            ->addColumn('feed_lead_html', 'text', ['limit' => MysqlAdapter::TEXT_LONG, 'null' => true, 'default' => null, 'encoding' => 'utf8mb4', 'collation' => 'utf8mb4_unicode_ci',])
            ->addColumn('feed_body_html', 'text', ['limit' => MysqlAdapter::TEXT_LONG, 'null' => true, 'default' => null, 'encoding' => 'utf8mb4', 'collation' => 'utf8mb4_unicode_ci',])
            ->save();
//        $this->execute("
//            UPDATE blogposts
//            SET
//                feed_header_html =    header,
//                feed_lead_html =      lead,
//                feed_body_html =      body
//            WHERE 1=1
//        ");

        $this->table('news')
            ->addColumn('feed_header_html', 'text', ['limit' => MysqlAdapter::TEXT_LONG, 'null' => true, 'default' => null, 'encoding' => 'utf8mb4', 'collation' => 'utf8mb4_unicode_ci',])
            ->addColumn('feed_lead_html', 'text', ['limit' => MysqlAdapter::TEXT_LONG, 'null' => true, 'default' => null, 'encoding' => 'utf8mb4', 'collation' => 'utf8mb4_unicode_ci',])
            ->addColumn('feed_body_html', 'text', ['limit' => MysqlAdapter::TEXT_LONG, 'null' => true, 'default' => null, 'encoding' => 'utf8mb4', 'collation' => 'utf8mb4_unicode_ci',])
            ->save();
//
//        $this->execute("
//            UPDATE news
//            SET
//                feed_header_html =    text_content,
//                feed_lead_html =      lead,
//                feed_body_html =      article_text
//            WHERE 1=1
//        ");
    }

    public function down()
    {
        $this->table('blogposts')
            ->removeColumn('feed_header_html')
            ->removeColumn('feed_lead_html')
            ->removeColumn('feed_body_html')
            ->save();
        $this->table('news')
            ->removeColumn('feed_header_html')
            ->removeColumn('feed_lead_html')
            ->removeColumn('feed_body_html')
            ->save();

    }
}
