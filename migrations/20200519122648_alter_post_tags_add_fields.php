<?php


use Phinx\Db\Adapter\MysqlAdapter;
use Phinx\Migration\AbstractMigration;

class AlterPostTagsAddFields extends AbstractMigration
{
    public function up()
    {
        $this->table('post_tags')
            ->addColumn('header', 'text', ['limit' => MysqlAdapter::TEXT_LONG, 'null' => true, 'default' => null, 'encoding' => 'utf8mb4', 'collation' => 'utf8mb4_unicode_ci',])
            ->addColumn('lead', 'text', ['limit' => MysqlAdapter::TEXT_LONG, 'null' => true, 'default' => null, 'encoding' => 'utf8mb4', 'collation' => 'utf8mb4_unicode_ci',])
            ->addColumn('body', 'text', ['limit' => MysqlAdapter::TEXT_LONG, 'null' => true, 'default' => null, 'encoding' => 'utf8mb4', 'collation' => 'utf8mb4_unicode_ci',])
            ->addColumn('image_rss_description', 'string', ['limit' => 255, 'null' => true, 'default' => null])
            ->addColumn('image_rss_source', 'string', ['limit' => 255, 'null' => true, 'default' => null])
            ->save();
    }

    public function down()
    {
        $this->table('post_tags')
            ->removeColumn('header')
            ->removeColumn('lead')
            ->removeColumn('body')
            ->removeColumn('image_rss_source')
            ->removeColumn('image_rss_description')
            ->save();
    }
}
