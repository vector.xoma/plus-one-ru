<?php


use Phinx\Migration\AbstractMigration;

class CreateTableStableDevelopment extends AbstractMigration
{
    private $tablename = "stable_development";

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('seo_title', 'string', ['limit' => 255, 'null' => false, 'default' => null])
            ->addColumn('seo_description', 'string', ['limit' => 500, 'null' => false, 'default' => null])
            ->addColumn('seo_keywords', 'string', ['limit' => 500, 'null' => false, 'default' => null])
            ->save();

        $this->execute("
        INSERT INTO `modules` (`class`, `name`, `valuable`, `url`, `type_module`, `icon`, `order_num`) VALUES ('StableDevelopment', 'Устойчивое развитие', '0', '', 'admin', 'fa fa-save', '111');
        INSERT INTO `stable_development` (id, seo_title,  seo_description, seo_keywords) VALUES (1, '', '' , ''); 
        ");
    }

    public function down()
    {
        $this->dropTable($this->tablename);
    }
}
