<?php


use Phinx\Db\Adapter\MysqlAdapter;
use Phinx\Migration\AbstractMigration;

class AlterKeywords1024Length extends AbstractMigration
{
    public function up()
    {
        $this->table('blogtags')
            ->changeColumn('seo_keywords', 'string', ['limit' => 1024, 'null' => true, 'default' => null])
            ->save();

        $this->table('post_tags')
            ->changeColumn('seo_keywords', 'string', ['limit' => 1024, 'null' => true, 'default' => null])
            ->save();

        $this->table('blogwriters')
            ->changeColumn('seo_keywords', 'string', ['limit' => 1024, 'null' => true, 'default' => null])
            ->save();

        $this->table('seo_content')
            ->changeColumn('seo_keywords', 'string', ['limit' => 1024, 'null' => true, 'default' => null])
            ->save();

        $this->table('stable_development')
            ->changeColumn('seo_keywords', 'string', ['limit' => 1024, 'null' => true, 'default' => null])
            ->save();

        $this->table('news')
            ->changeColumn('meta_keywords', 'string', ['limit' => 1024, 'null' => true, 'default' => null])
            ->save();

        $this->table('groups')
            ->changeColumn('meta_keywords', 'string', ['limit' => 1024, 'null' => true, 'default' => null])
            ->changeColumn('meta_description', 'text', ['limit' =>  MysqlAdapter::TEXT_REGULAR, 'null' => true, 'default' => null])
            ->save();
    }
}
