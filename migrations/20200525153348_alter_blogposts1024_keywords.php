<?php


use Phinx\Migration\AbstractMigration;

class AlterBlogposts1024Keywords extends AbstractMigration
{

    public function up()
    {
        $this->table('blogposts')
            ->changeColumn('meta_keywords', 'string', ['limit' => 1024, 'null' => true, 'default' => null])
            ->save();

    }
}
