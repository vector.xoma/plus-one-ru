<?php

use Phinx\Migration\AbstractMigration;

class NewTableBlockAnnouncement extends AbstractMigration
{

    private $tablename =  "block_announcements";
    private $tableItems = "block_announcement_items";

    public function up()
    {

        $this->table($this->tablename)
            ->addColumn('name', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('enabled', 'integer', ['limit' => 1, 'null' => true, 'default' => null, ])
            ->addColumn('creator_id', 'integer', ['limit' => 11, 'null' => true, 'default' => null, ])
            ->addColumn('created', 'datetime', ['null' => true, 'default' => null])
            ->addColumn('modified', 'datetime', ['null' => true, 'default' => null])
            ->save();

        $this->table($this->tableItems)
            ->addColumn('block_announcement_id', 'integer', ['limit' => 11, 'null' => false ])
            ->addColumn('content_id', 'integer', ['limit' => 11, 'null' => false ])
            ->addColumn('content_type', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('created', 'datetime', ['null' => true, 'default' => null])
            ->addColumn('modified', 'datetime', ['null' => true, 'default' => null])
            ->addColumn('enabled', 'integer', ['limit' => 1, 'null' => true, 'default' => null, ])
            ->addColumn('order_num', 'integer', ['limit' => 11, 'null' => true, 'default' => null, ])
            ->addIndex('content_id', array('name' => 'ind_block_announcement_item_content_id'))
            ->addIndex('block_announcement_id', array('name' => 'ind_block_announcement_items_block_announcement_id'))
            ->create();

        $this->execute("
    INSERT INTO `modules` (`class`, `name`, `valuable`, `url`, `type_module`, `icon`, `order_num`) 
         VALUES ('BlockAnnouncement', 'Блок анонсов', '0', '', 'admin', 'fa fa-th', '3');
        ");

    }

    public function down()
    {
        $this->dropTable($this->tablename);
        $this->dropTable($this->tableItems);
    }
}
