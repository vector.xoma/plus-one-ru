<?php


use Phinx\Migration\AbstractMigration;

class AlterTableBlogpostsAddAuthorSignature extends AbstractMigration
{
    private $tablename = 'blogposts';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('author_signature', 'string', ['limit' => 255, 'null' => false, 'default' => 'Автор'])
            ->addColumn('author_name', 'string', ['limit' => 255, 'null' => true])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('author_signature')
            ->removeColumn('author_name')
        ;
    }
}
