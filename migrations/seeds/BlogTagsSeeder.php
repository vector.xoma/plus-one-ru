<?php

use Phinx\Seed\AbstractSeed;

class BlogTagsSeeder extends AbstractSeed
{
    private $tablename = 'blogtags';

    public function run()
    {
        $data = array(
            array(
                'url' => 'economy',
                'name' => 'Экономика',
                'enabled' => 1,
                'isshow' => 1,
                'created' => date('Y-m-d H:i:s', time()),
                'modified' => date('Y-m-d H:i:s', time()),
            ),
            array(
                'url' => 'eco',
                'name' => 'Экология',
                'enabled' => 1,
                'isshow' => 1,
                'created' => date('Y-m-d H:i:s', time()),
                'modified' => date('Y-m-d H:i:s', time()),
            ),
            array(
                'url' => 'community',
                'name' => 'Общество',
                'enabled' => 1,
                'isshow' => 1,
                'created' => date('Y-m-d H:i:s', time()),
                'modified' => date('Y-m-d H:i:s', time()),
            ),
            array(
                'url' => 'main',
                'name' => 'Главная страница',
                'enabled' => 1,
                'isshow' => 0,
                'created' => date('Y-m-d H:i:s', time()),
                'modified' => date('Y-m-d H:i:s', time()),
            ),
        );

        $table = $this->table($this->tablename);
        $table->insert($data)->save();
    }
}
