<?php

use Phinx\Seed\AbstractSeed;

class SectionsSeeder extends AbstractSeed
{
    private $tablename = 'sections';

    public function run()
    {
        $data = array(
            array('url'=>'mainpage', 'parent'=>null, 'name'=>'ГЛАВНАЯ', 'header'=>'ГЛАВНАЯ', 'meta_title'=>'ГЛАВНАЯ', 'meta_description'=>'', 'meta_keywords'=>'', 'body'=>'', 'menu_id'=>'2', 'order_num'=>'1', 'module_id'=>'1', 'enabled'=>'1', 'created'=>'2012-06-28 13:19:01', 'modified'=>'2016-06-09 15:58:23', ),
            array('url'=>'404', 'parent'=>0, 'name'=>'Страница не найдена', 'header'=>'Страница не найдена', 'meta_title'=>'Страница не найдена', 'meta_description'=>'', 'meta_keywords'=>'', 'body'=>'<p>Страница не найдена</p>', 'menu_id'=>'1', 'order_num'=>'12', 'module_id'=>'1', 'enabled'=>'1', 'created'=>'0000-00-00 00:00:00', 'modified'=>'0000-00-00 00:00:00', ),
        );

        $table = $this->table($this->tablename);
        $table->insert($data)->save();
    }
}
