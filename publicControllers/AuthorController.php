<?php
require_once('Widget.class.php');
require_once('PresetFormat.class.php');
require_once('Helper.class.php');
require_once('QueryProvider.php');

class AuthorController extends Widget
{
    private $numItemsOnPage = 25;

    function getAuthors($page = 1)
    {
        $this->db->query(sql_placeholder("SELECT * FROM seo_content WHERE section = 'author'"));
        $seo = $this->db->result();

        return array(
            'items' => $this->getAllAuthors($page),
            'description' => $seo->header,
            'imageDesktop' => $seo->large_image ? "/files/seo_content/" . $seo->large_image : null,
            'imageMobile' => $seo->stub_image ? "/files/seo_content/" . $seo->stub_image : null,
            'rubrikaUrl' => 'author',
            'header' => $seo->seo_title,
            'page' => $page,
            'meta' => [
                'title' => $seo->seo_title,
                'description' => $seo->seo_description,
                'keywords' => $seo->seo_keywords,
                'ogTitle' => $seo->seo_title,
                'ogType' => 'article',
                'ogDescription' => $seo->seo_description,
                'ogImage' => $seo->large_image ? "/files/seo_content/" . $seo->large_image : '/og-plus-one.ru.png',
                'ogSiteName' => '',
                'ogUrl' => '',
                'twitterCard' => 'summary_large_image',
                'twitterTitle' => $seo->seo_title,
                'twitterDescription' => $seo->seo_description,
                'twitterUrl' => '',
                'twitterImage' => $seo->large_image ? "/files/seo_content/" . $seo->large_image : '/og-plus-one.ru.png',
                'twitterImageAlt' => $seo->seo_title,
                'twitterSite' => ''
            ]
        );
    }


    function getAuthorPosts($writerUrl = '', $page = 1)
    {
        $query = sql_placeholder("SELECT bw.* FROM blogwriters AS bw WHERE bw.enabled=1 AND bw.tag_url = ?", $writerUrl);
        $this->db->query($query);
        $user = $this->db->result();

        $nowDate = new DateTime();
        $query = sql_placeholder(" SELECT COUNT(b.id) AS `count` FROM blogposts AS b WHERE b.enabled = 1 AND b.writers = ? AND b.created <= ?"
            , $user->id
            , $nowDate->format('Y-m-d H:i:s'));
        $this->db->query($query);
        $resultCount = $this->db->result();
        $totalPages = ceil($resultCount->count / $this->numItemsOnPage);

        // метатеги, информация об авторе
        $meta = $this->getMetaData($user->id);
        $this->title = $meta['metaTitle'];
        $this->description = $meta['metaDescription'];
        $this->keywords = $meta['metaKeyword'];

        if ($page > $totalPages) {
            $return = array(
                'posts' => null,
                'page' => $page,

            );
            return $return;
        }

        if ($user->id != "") {
        $start = ($page - 1) * $this->numItemsOnPage;

        $result = $this->getListPosts(null, null, $user->id, $start, $this->numItemsOnPage);

        $result = Helper::formatPostList($result, '1_3', $this->getHost());

        $page = intval($page + 1);

            $return = array(
                'posts' => $result,
                'writerInfo' => $meta['writer'],
                'page' => $page,
                'meta' => [
                    'title' => $this->title,
                    'description' => $this->description,
                    'keywords' => $this->keywords,
                    'ogTitle' => $this->title,
                    'ogType' => 'article',
                    'ogDescription' => $this->description,
                    'ogImage' => '/og-plus-one.ru.png',
                    'ogSiteName' => '',
                    'ogUrl' => '',
                    'twitterCard' => 'summary_large_image',
                    'twitterTitle' => $this->title,
                    'twitterDescription' =>  $this->description,
                    'twitterUrl' => '',
                    'twitterImage' => '/og-plus-one.ru.png',
                    'twitterImageAlt' => '+1 — Проект об устойчивом развитии',
                    'twitterSite' => ''
                ]
            );


            return $return;
        }
        return false;
    }


    /**
     *
     * @param int $page
     * @return array|int
     */
    function getAllAuthors($page = 1)
    {
        $limit = 5000;
        $offset = ($page -1) * $limit;

        $tags = [Helper::TAG_ECONOMY, Helper::TAG_ECOLOGY, Helper::TAG_COMMUNITY];
        $tagsStr = "'".join("','", $tags)."'";
        $query = "SELECT bt.id, bt.color, bt.url FROM blogtags as bt WHERE bt.url IN ({$tagsStr}) AND bt.enabled = 1" ;
        $this->db->query($query);
        $blogTags = $this->db->results();

        $blogTagsIdsStr = join(',', array_map(function ($item) {return $item->id;}, $blogTags));

        $query = "
        SELECT pt.name_lead, bw.name, bw.tag_url, bw.id, count(bp.id) AS total
          FROM post_tags AS pt
          JOIN blogwriters AS bw ON bw.leader = pt.id
          JOIN blogposts AS bp ON bp.writers = bw.id
         WHERE pt.enabled = 1 AND pt.parent IN ($blogTagsIdsStr) AND bw.private = 0 AND bw.bloger = 0 AND bw.enabled = 1 AND bp.enabled = 1 AND bw.useplatforma <> 1
      GROUP BY bw.id
      ORDER BY total DESC
      LIMIT {$offset}, {$limit}
      ";

        $this->db->query($query);
        $authors = $this->db->results();

        if (is_array($authors)) {
            foreach ($authors as $k => $author) {
                $authors[$k]->lenAuthor = strlen($author->name);
                $authors[$k]->countPosts = new stdClass();
                $authors[$k]->countPosts->count = $author->total;
                $authors[$k]->countPosts->text = Helper::getHeaderMenuSigns('материалов', 'материал', 'материала', $author->total);
                $authors[$k]->url = $author->tag_url;

            }
        }

        return $authors;
    }

    private function getMetaData($id)
    {
        $query = "SELECT w.*, t.name_lead, bt.url, w.image AS imageDesktop, w.stub_image AS imageMobile, w.stub_link, w.description FROM blogwriters AS w
                                  LEFT JOIN post_tags AS t ON t.id=w.leader
                                  LEFT JOIN blogtags AS bt ON bt.id=t.parent
                                  WHERE w.id=" . $id . " LIMIT 1";
        $this->db->query($query);
        $writer = $this->db->result();

        $writer->header = $writer->name;
        unset($writer->name);

        if ($writer->private == 1){
            $writerName = "Колонка " . $writer->name_genitive;
        }
        elseif ($writer->bloger == 1){
            $writerName = "Блог " . $writer->name;
        }
        else{
            $writerName = $writer->name;
        }

        $title = $writer->seo_title ? "+1 — " . $writer->seo_title : "+1 — " . $writerName;
        $description = $writer->seo_description ? $writer->seo_description : "";
        $keywords = $writer->seo_keywords ? $writer->seo_keywords : "";

        if ($writer->image){
            $writer->image = "/files/writers/" . $writer->image;
        }
        else {
            $writer->image = null;
        }

        if ($writer->imageDesktop){
            $writer->imageDesktop = "/files/writers/" . $writer->imageDesktop;
        }
        else {
            $writer->imageDesktop = null;
        }

        if ($writer->imageMobile){
            $writer->imageMobile = "/files/writers/" . $writer->imageMobile;
        }
        else {
            $writer->imageMobile = null;
        }


        return array(
            'metaTitle' => $title,
            'metaDescription' => $description,
            'metaKeyword' => $keywords,
            'writer' => $writer
        );
    }

    private function getHost()
    {
        return $this->config->protocol . $this->config->host;

    }

    private function getListPosts($rubrika_url, $rubrikaId, $writerId = null, $start, $limit = null )
    {
        $nowDate = new DateTime('now');

        $query = sql_placeholder("SELECT b.id, b.blocks_author AS blocks, b.tags, b.name, b.header, b.url, b.type_post, b.is_partner_material,
                            b.type_post, b.post_tag, b.signature_to_sum, b.text_body, b.amount_to_displaying, b.video_on_main, b.state, b.type_video,
                            b.partner_url, b.body_color, bw.name AS writer, bw.id AS writer_id, bw.private, bw.bloger, bw.name_genitive,
                            bw.image AS writerImage, bw.stub_image, bw.stub_link, p.name AS partner,
                            p.color AS partner_color, b.anounce_new_author, b.spec_project, b.conference,
                            sp.name AS specProjectName, sp.external_url AS specProjectUrl, sp.color AS marker_color,
                            sp.font_color AS font_color, b.image_1_1, b.image_1_2, b.image_1_3, b.image_rss,
                            conf.name AS confName, conf.external_url AS confUrl, conf.color AS confColor,
                            conf.font_color AS confFontColor, b.citate_author_name,
                            DATE_FORMAT(b.created, '%d.%m.%Y') AS date_created,
                            bt.name AS tagName, bt.url AS tagUrl, b.format, b.font, p.name AS partnerName
                            ,b.type_announce_1_1, b.type_announce_1_2, b.type_announce_1_3
                            ,sp.name AS sp_name, sp.header AS sp_header, sp.external_url AS sp_url
                            ,sp.image_1_1 AS sp_image_1_1, sp.image_1_2 AS sp_image_1_2, sp.image_1_3 AS sp_image_1_3
                            ,sp.type_announce_1_1 AS sp_type_announce_1_1
                            ,sp.type_announce_1_2 AS sp_type_announce_1_2
                            ,sp.type_announce_1_3 AS sp_type_announce_1_3,
                            bw.bloger, bw.name as writerName, bw.announcement_img as blogImg, bw.id as writerId, bw.tag_url as writerTag
                            FROM blogposts AS b
                          INNER JOIN blogwriters AS bw ON bw.id=b.writers
                          LEFT JOIN partners AS p ON p.id=b.partner
                          LEFT JOIN blogtags AS bt ON bt.id = b.tags
                          LEFT JOIN spec_projects AS sp ON sp.id=b.spec_project
                          LEFT JOIN conferences AS conf ON conf.id=b.conference
                          WHERE
                          b.enabled = 1 AND (bw.enabled = 1 OR b.spec_project ) AND bt.enabled = 1
                          AND b.created <= ?
                          AND b.blocks != 'ticker'
                          AND b.writers=?
                          ORDER BY b.created DESC
                          LIMIT ?, ?",
            $nowDate->format('Y-m-d H:i:s'),
            $writerId,
            $start,
            $limit);

        if (!empty($query)){
            $this->db->query($query);
            $result = $this->db->results();
        }
        else{
            $result = null;
        }
        return $result;
    }
}
