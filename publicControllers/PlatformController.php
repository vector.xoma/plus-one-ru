<?php
require_once('Widget.class.php');
require_once('Helper.class.php');

class PlatformController extends Widget
{
    function getPlatform($page)
    {
        $this->db->query(sql_placeholder("SELECT * FROM seo_content WHERE section = 'platform'"));
        $seo = $this->db->result();

        return array(
            'items' => $this->getPosts($page),
            'header' => $seo->seo_title,
            'description' => $seo->header,
            'imageDesktop' => $seo->large_image ? "/files/seo_content/" . $seo->large_image : null,
            'imageMobile' => $seo->stub_image ? "/files/seo_content/" . $seo->stub_image : null,
            'rubrikaUrl' => 'platform',
            'page' => $page,
            'meta' => [
                'title' => $seo->seo_title,
                'description' => $seo->seo_description,
                'keywords' => $seo->seo_keywords,
                'ogTitle' => $seo->seo_title,
                'ogType' => 'article',
                'ogDescription' => $seo->seo_description,
                'ogImage' => $seo->large_image ? "/files/seo_content/" . $seo->large_image : '/og-plus-one.ru.png',
                'ogSiteName' => '',
                'ogUrl' => '',
                'twitterCard' => 'summary_large_image',
                'twitterTitle' => $seo->seo_title,
                'twitterDescription' => $seo->seo_description,
                'twitterUrl' => '',
                'twitterImage' => $seo->large_image ? "/files/seo_content/" . $seo->large_image : '/og-plus-one.ru.png',
                'twitterImageAlt' => $seo->seo_title,
                'twitterSite' => ''
            ]
        );
    }

    function getItems($tag)
    {
        return [];
    }


    /**
     *
     * @param int $page
     * @return array|int
     */
    function getPosts($page = 1)
    {

        $limit = 25;
        $offset = ($page -1) * $limit;
        $nowDate = new \DateTime('now');

        $query = sql_placeholder("SELECT b.id, b.blocks_blog AS blocks, b.tags, b.name, b.header, b.url, b.type_post, b.is_partner_material,
                            b.type_post, b.partner_url, b.post_tag, b.signature_to_sum, b.text_body, b.video_on_main, b.state, b.type_video,
                            b.amount_to_displaying, b.body_color, bw.name AS writer, bw.id AS writer_id, bw.private, bw.bloger, bw.name_genitive,
                            bw.image AS writerImage, bw.stub_image, bw.stub_link, p.name AS partner,
                            p.color AS partner_color, b.anounce_new_author, b.spec_project,  b.conference,
                            sp.name AS specProjectName, sp.external_url AS specProjectUrl, sp.color AS marker_color,
                            sp.font_color AS font_color,  b.image_1_1, b.image_1_2, b.image_1_3, b.image_rss,
                            sp.image_1_1 AS specProjectImage, sp.header AS specProjectHeader,
                            conf.name AS confName, conf.external_url AS confUrl, conf.color AS confColor,
                            conf.font_color AS confFontColor,  b.citate_author_name,
                            DATE_FORMAT(b.created, '%d.%m.%Y') AS date_created,
                            bt.name AS tagName, bt.url AS tagUrl, b.format, b.font, p.name AS partnerName
                            ,b.type_announce_1_1, b.type_announce_1_2, b.type_announce_1_3
                            ,sp.name AS sp_name, sp.header AS sp_header, sp.external_url AS sp_url
                            ,sp.image_1_1 AS sp_image_1_1, sp.image_1_2 AS sp_image_1_2, sp.image_1_3 AS sp_image_1_3
                            ,sp.type_announce_1_1 AS sp_type_announce_1_1
                            ,sp.type_announce_1_2 AS sp_type_announce_1_2
                            ,sp.type_announce_1_3 AS sp_type_announce_1_3,
                            bw.bloger, bw.name as writerName, bw.announcement_img as blogImg, bw.id as writerId, bw.tag_url as writerTag
                              FROM blogposts AS b
                            INNER JOIN blogwriters AS bw ON bw.id=b.writers
                            LEFT JOIN partners AS p ON p.id=b.partner
                            LEFT JOIN blogtags AS bt ON bt.id = b.tags
                            LEFT JOIN spec_projects AS sp ON sp.id=b.spec_project
                            LEFT JOIN conferences AS conf ON conf.id=b.conference
                            WHERE bw.useplatforma = 1
                            AND b.enabled = 1 AND (bw.enabled = 1 OR b.spec_project ) AND bt.enabled = 1
                            AND b.created <= ?
                            AND b.blocks != 'ticker'
                            ORDER BY b.created DESC
                          LIMIT ?, ?",
            $nowDate->format('Y-m-d H:i:s'),
            $offset,
            $limit
        );



        $this->db->query($query);
        $authors = $this->db->results();


        return Helper::formatPostList($authors, '1_3', $this->getHost());
    }

    private function getHost()
    {
        $host = $this->config->protocol . $this->config->host;

        return $host;
    }
}
