<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

ini_set('memory_limit', '512M');
require_once('./../Config.class.php');
require_once('./../placeholder.php');
require_once('./../Database.class.php');

$config = new Config();
$db = new Database($config->dbname, $config->dbhost, $config->dbuser, $config->dbpass);

if (!$db->connect()) {
    print "Не могу подключиться к базе данных. Проверьте настройки подключения";
    exit();
}

$db->query('SET NAMES utf8;');

$tablename = 'blogposts';
$localeFile = 'replaceUrlAbsoluteToRelative.csv';

$db->query("SELECT id, header, lead, meta_title, meta_description FROM news");

$newsList = $db->results();


$GLOBALS['news_description'] = array();
$GLOBALS['news_title'] = array();


foreach ($newsList as $i => $post) {
    if (trim($post->meta_description) === "" || trim($post->meta_description) === "undefined") {
        $GLOBALS['news_description'][$post->id] = html_entity_decode(strip_tags($post->lead)) ;
    }
    if (trim($post->meta_title) === "" || trim($post->meta_title) === "undefined") {
        $GLOBALS['news_title'][$post->id] = html_entity_decode(strip_tags($post->header));
    }
}


if (@$argv[1] == '--update') {
    foreach ($GLOBALS['news_description'] as $id => $meta_descr) {
        $db->query(sql_placeholder("UPDATE news SET meta_description = ? WHERE id = ?", $meta_descr, $id));
    }
    foreach ($GLOBALS['news_title'] as $id => $meta_title) {
        $db->query(sql_placeholder("UPDATE news SET meta_title = ? WHERE id = ?", $meta_title, $id));
    }
} else {
    print_r($GLOBALS['news_title']);
    print_r($GLOBALS['news_description']);
}
