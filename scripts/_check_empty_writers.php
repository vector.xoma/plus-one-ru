<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

ini_set('memory_limit', '512M');
require_once('./../Config.class.php');
require_once('./../placeholder.php');
require_once('./../Database.class.php');

$config = new Config();
$db = new Database($config->dbname, $config->dbhost, $config->dbuser, $config->dbpass);

if(!$db->connect())
{
    print "Не могу подключиться к базе данных. Проверьте настройки подключения";
    exit();
}
$db->query('SET NAMES utf8;');

$tablename = 'blogposts';
$localeFile = 'replaceUrlAbsoluteToRelative.csv';

$db->query("SELECT id, url, tags, name FROM blogposts WHERE writers = 0");
$postList = $db->results();
$localeFilePath = __DIR__ .DIRECTORY_SEPARATOR. $localeFile;


function sanitize($str)
{
    return strip_tags(htmlspecialchars($str));
}

function fix_strtolower($str)
{
    if (function_exists('mb_strtoupper'))
    {
        return mb_strtolower($str);
    }
    else
    {
        return strtolower($str);
    }
}

function fix_filename($str, $config, $is_folder = false)
{
    $str = sanitize($str);
    if ($config['convert_spaces'])
    {
        $str = str_replace(' ', $config['replace_with'], $str);
    }

    if ($config['transliteration'])
    {
        if (!mb_detect_encoding($str, 'UTF-8', true))
        {
            $str = utf8_encode($str);
        }
        if (function_exists('transliterator_transliterate'))
        {
            $str = transliterator_transliterate('Russian-Latin/BGN', $str);
        }
        else
        {
//			$str = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $str);
            $cyr = array(
                'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п',
                'р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',
                'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П',
                'Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я'
            );
            $lat = array(
                'a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p',
                'r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya',
                'A','B','V','G','D','E','Io','Zh','Z','I','Y','K','L','M','N','O','P',
                'R','S','T','U','F','H','Ts','Ch','Sh','Sht','A','I','Y','e','Yu','Ya'
            );
            $str = str_replace($cyr, $lat, $str);
        }

        $str = preg_replace("/[^a-zA-Z0-9\.\[\]_| -]/", '', $str);
    }

    if ($config['lower_case'])
    {
        $str = fix_strtolower($str);
    }
    $str = str_replace(array( '"', "'", "/", "\\" ), "", $str);
    $str = strip_tags($str);

    // Empty or incorrectly transliterated filename.
    // Here is a point: a good file UNKNOWN_LANGUAGE.jpg could become .jpg in previous code.
    // So we add that default 'file' name to fix that issue.
    if (!$config['empty_filename'] && strpos($str, '.') === 0 && $is_folder === false)
    {
        $str = 'file' . $str;
    }

    return trim($str);
}
$GLOBALS['counterPictures'] = 0;
function replace_path($fieldList, $postId, $tablename, $postUrl, Database $db,   $HOST_REPLACE)
{
    $replace = "/shared/httpd/plus-one.ru";
    $matches = array();
    foreach ($fieldList as $field) {
        preg_match_all('/src="(https?:\/\/plus-one.rbc.ru\/files\/\w+\/([^"]+))"/', $field, $matches);
        if ($matches[1]){
            foreach ($matches[1] as $ind => $fileName) {
                echo   $postUrl, '; ', $fileName, '; ' , PHP_EOL;
                $GLOBALS['counterPictures'] ++;
            }
        }
    }
}

$full = count($postList);
$countChanged = 0;
$round = 1;
$countPerRound = round($full/100);
$oneRoundPercent = 1;
$HOST_REPLACE = $config->protocol.$config->host;
$ff = fopen('ffff.csv', 'w+');
foreach ($postList as $i => $post) {
    $tags = array (
        '',
        'economy/',
        'ecology/',
        'society/'
    );
    $admin = "/adminv2/blogpost/edit/post/{$post->id}/token";
    $postUrl = $config->protocol.$config->host. $admin ;
    $userUrl = $config->protocol.$config->host. '/' . $tags[$post->tags] . $post->url;

    echo   $postUrl, '; ', $userUrl, '; ', str_replace(';', '\;', $post->name), '; ' , $post->id, '; ', PHP_EOL;

    fputcsv($ff, array($postUrl, $userUrl, $post->name, $post->id));
    $GLOBALS['counterPictures']++;
}
fclose($ff);
echo PHP_EOL;
echo 'Posts: '. $GLOBALS['counterPictures'];
echo PHP_EOL;
