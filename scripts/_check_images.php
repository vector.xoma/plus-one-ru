<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

ini_set('memory_limit', '512M');
require_once('./../Config.class.php');
require_once('./../placeholder.php');
require_once('./../Database.class.php');
$GLOBALS['config'] = require_once('./../filemanager/config/config.php');

$configApp = new Config();
$db = new Database($configApp->dbname, $configApp->dbhost, $configApp->dbuser, $configApp->dbpass);

if(!$db->connect())
{
    print "Не могу подключиться к базе данных. Проверьте настройки подключения";
    exit();
}
$db->query('SET NAMES utf8;');

$tablename = 'blogposts';

$db->query("SELECT 'blogposts' as type, id, url, tags, body, header, lead, created FROM blogposts ORDER BY id ASC");
$postList = $db->results();


$GLOBALS['root_path'] = realpath(__DIR__ . '/../');

function check_image($fieldList, $post, $tablename, $postUrl, Database $db)
{
    $result = false;
    $matches = array();
    foreach ($fieldList as $index => $field) {
        preg_match_all('/="(\/files\/[^"]+)"/', $field, $matches);
        if ($matches[1]){
            foreach ($matches[1] as $ind => $fileName) {
                $fileName = preg_replace('/\?\d+$/u', '', $fileName);
                if (!file_exists($GLOBALS['root_path'].$fileName)) {

                    if (!isset($GLOBALS['images'][$fileName]))                      $GLOBALS['images'][$fileName] = array();
                    if (!isset($GLOBALS['images'][$fileName]['created']))           $GLOBALS['images'][$fileName]['created'] = $post->created;
                    if (!isset($GLOBALS['images'][$fileName]['vars']))              $GLOBALS['images'][$fileName]['vars'] = array();
                    if (!isset($GLOBALS['images'][$fileName][$post->type]))         $GLOBALS['images'][$fileName][$post->type] = array();
                    if (!isset($GLOBALS['images'][$fileName][$post->type]['ids']))  $GLOBALS['images'][$fileName][$post->type]['ids'] = array();

                    $GLOBALS['images'][$fileName]['fieldImage'] = $index;
                    $GLOBALS['images'][$fileName]['postUrl'] = $postUrl;
                    $GLOBALS['images'][$fileName][$post->type]['ids'][] = $post->id;

                    $GLOBALS['images'][$fileName]['vars'][] = basename($fileName);

                    $GLOBALS['config']['replace_with'] = "-";
                    $GLOBALS['config']['lower_case'] = true;
                    $GLOBALS['images'][$fileName]['vars'][] = fix_filename(basename($fileName), $GLOBALS['config']);
                    $GLOBALS['config']['replace_with'] = "_";
                    $GLOBALS['config']['lower_case'] = false;
                    $GLOBALS['images'][$fileName]['vars'][] = fix_filename(basename($fileName), $GLOBALS['config']);

                    $clearRussian = preg_replace('/[а-яА-Я]+/u', '', basename($fileName));
                    $GLOBALS['images'][$fileName]['vars'][] = $clearRussian;
                    $GLOBALS['images'][$fileName]['vars'][] = str_replace(' ', '_', $clearRussian);
                    $GLOBALS['images'][$fileName]['vars'][] = str_replace(['+', ',', '(', ')'], '', str_replace(' ', '_', $clearRussian));
                    $clearRussian = preg_replace('/[а-яА-Я]+/u', '_', basename($fileName));
                    $clearRussian = str_replace(' ', '_',  $clearRussian);
                    $GLOBALS['images'][$fileName]['vars'][] =  str_replace(array( '"', "'", '“', "”", "/", "\\" ), "", $clearRussian);
                    $GLOBALS['images'][$fileName]['vars'][] =
                        str_replace(' ', '_', str_replace(array( '"', "'", '“', "”", "/", "\\" ), "", basename($fileName)));
                    $result = true;
                }
            }
        }
    }
    return $result;
}


$full = count($postList);
$countChanged = 0;
$round = 1;
$countPerRound = round($full/100);
$oneRoundPercent = 1;


$GLOBALS['images'] = array();
$GLOBALS['urls'] = array();


foreach ($postList as $i => $post) {
    $fieldList = [
        'body' => htmlspecialchars_decode(html_entity_decode($post->body)),
        'header' => htmlspecialchars_decode(html_entity_decode($post->header)),
        'lead' => htmlspecialchars_decode(html_entity_decode($post->lead)),
    ];

    $tags = [
        '',
        'economy/',
        'ecology/',
        'society/'
    ];
    if ($post->type == 'blogposts') {
        $postUrl = $configApp->protocol . $configApp->host . '/' . $tags[$post->tags] . $post->url;
    } else {
        $date = new DateTime($post->created);
        $postUrl = $configApp->protocol . $configApp->host . '/news.' . $date->format('Y/m/d') .'/'. $post->url;
    }
    if (check_image($fieldList, $post, $tablename, $postUrl, $db)) {}
}

echo "Count: ", count($GLOBALS['images']) , PHP_EOL;

foreach ($GLOBALS['images'] as $image => $params) {

}



//--------------------------------------------------------------
// Функция рекурсивного сканирования каталога
//--------------------------------------------------------------
// Параметры:
//   $directory - начальный каталог
//   $callback - функция для обработки найденных файлов
//--------------------------------------------------------------
function scan_recursive($directory, $callback = null)
{
    // Привести каталог в канонизированный абсолютный путь
    $directory = realpath($directory);

    if ($d = opendir($directory)) {
        while ($fname = readdir($d)) {
            if ($fname == '.' || $fname == '..') {
                continue;
            } else {
                // Передать путь файла в callback-функцию
                if ($callback != null && is_callable($callback)) {
                    $callback($directory . DIRECTORY_SEPARATOR . $fname);
                }
            }

            if (is_dir($directory . DIRECTORY_SEPARATOR . $fname)) {
                scan_recursive($directory . DIRECTORY_SEPARATOR . $fname, $callback);
            }
        }
        closedir($d);
    }
}

function mkdir_custom($path) {

    if (file_exists($path)) {
        return true;
    }
    return @mkdir($path, 0775, true);
}

$GLOBALS['unused'] = array();

// Callback-функция, которая будет принимать имена файлов
function scan_callback($fname) {
    if (is_dir($fname) || preg_match('/.empty$/', $fname)) return;
    if (preg_match('/\.xml$/', $fname)) return;
    $GLOBALS['unused'][$fname] = basename($fname);
}

// Вывести список файлов и каталогов
scan_recursive('/var/www/unused-files/__un_used/image', 'scan_callback');

$GLOBALS['founded_clear'] = array();

foreach ($GLOBALS['unused'] as $fname => $fbName) {
    foreach ($GLOBALS['images'] as $image => $ibNames) {
        foreach ($ibNames['vars'] as $varName)
        {
            if ($fbName === $varName) {
                if (!isset($GLOBALS['founded_clear'][$fbName])) {
                    $GLOBALS['founded_clear'][$fbName] = [];
                    $GLOBALS['founded_clear'][$fbName]['replace_paths'] = array();
                    $GLOBALS['founded_clear'][$fbName]['post_ids'] = array();
                };
                $GLOBALS['founded_clear'][$fbName]['replace_paths'][] = $image;
                $GLOBALS['founded_clear'][$fbName]['real_path'] = $fname;
                $GLOBALS['founded_clear'][$fbName]['post_ids'] = array_merge($GLOBALS['founded_clear'][$fbName]['post_ids'], $ibNames['blogposts']['ids']);
            }
        }
    }
}

foreach ($GLOBALS['founded_clear'] as & $founded) {
    $founded['replace_paths'] = array_unique($founded['replace_paths']);
    $founded['post_ids'] = array_unique($founded['post_ids']);
}

echo count( $GLOBALS['founded_clear']), PHP_EOL, PHP_EOL;

$GLOBALS['founded_clear'] = array_filter($GLOBALS['founded_clear'], function ($item){return count($item['replace_paths']) < 2;});

print_r($GLOBALS['founded_clear']);
echo count( $GLOBALS['founded_clear']), PHP_EOL, PHP_EOL;

$GLOBALS['config']['replace_with'] = "-";
$GLOBALS['config']['lower_case'] = true;

function generateNewPath($pathFrom,  $created) {
    $ROOT_PATH = $GLOBALS['root_path'];
    $oldPath = $ROOT_PATH.$pathFrom;
    $dirName = dirname($oldPath);

    $dateFolder = '' . (new DateTime($created))->format('Y/m');

    if (preg_match("/(\d{4})\/(\d{2})/", $dirName)) {
        return $pathFrom;
    }

    $newPath = $ROOT_PATH . '/image_' . $dateFolder . '/' . fix_filename(basename($oldPath), $GLOBALS['config']);

    $pattern = preg_quote($ROOT_PATH, '/');

    return preg_replace("/^{$pattern}/", '', $newPath);
}


function grab_image($pathFrom, $post, $created, Database $db, $fieldImage) {
    $newPath = generateNewPath($pathFrom, $created);
    if ($pathFrom !== $newPath) {
        $params = array('field_image' => $fieldImage);
        unset($GLOBALS['images'][$pathFrom]['vars']);
        if (isset($GLOBALS['images'][$pathFrom])) {
            $params = array_merge($params, $GLOBALS['images'][$pathFrom]);
        }
        $db->query(sql_placeholder("INSERT INTO replace_path_log (old_path, new_path, item_id, item_type, created, status, params) VALUE (?, ?, ?, ?, ?, ?, ?)",
            $pathFrom, $newPath, $post->id, $post->type, date('Y-m-d H:i:s'), 0, serialize($params)));
    }
    return $pathFrom !== $newPath;
}


foreach ($GLOBALS['founded_clear'] as $fname => $options) {
    $stdObj = new stdClass();
    $stdObj->id = 0;
    $stdObj->type = 'filemanager';
    $pathFrom = $options['replace_paths'][0];
    $GLOBALS['images'][$pathFrom]['real_path'] = $options['real_path'];
    grab_image($options['replace_paths'][0], $stdObj, $GLOBALS['images'][$pathFrom]['created'], $db, $GLOBALS['images'][$pathFrom]['fieldImage']);
}
