<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set('memory_limit', '128M');
ini_set('max_execution_time', 180);


$startTime = microtime(true);


require_once('./../../Config.class.php');
require_once('./../../placeholder.php');
require_once('./../../Database.class.php');

$GLOBALS['root_path'] = realpath(__DIR__ . '/../../');

$config = new Config();
$db = new Database($config->dbname, $config->dbhost, $config->dbuser, $config->dbpass);

if (!$db->connect()) {
    print "Не могу подключиться к базе данных. Проверьте настройки подключения";
    exit();
} else {
    echo 'Подключение к БД успешно', PHP_EOL;
}
$db->query('SET NAMES utf8;');
$db->query("SELECT  id, name, header, created FROM blogposts ORDER BY id DESC");
$postList = $db->results();
echo PHP_EOL, "Count POSTS: " , count($postList), PHP_EOL;

foreach ($postList as $i => $post) {
    $post->header = html_entity_decode(strip_tags($post->header));
    $post->name = html_entity_decode(strip_tags($post->name));
    $db->query(sql_placeholder("UPDATE blogposts SET name = ?, header = ? WHERE id = ?", $post->name, $post->header, $post->id));
}


$db->query("SELECT  id, header, text_content FROM news ORDER BY id DESC");
$postList = $db->results();

echo PHP_EOL, "Count NEWS: " , count($postList), PHP_EOL;

foreach ($postList as $i => $post) {
    $post->header = html_entity_decode(strip_tags($post->header));
    $post->text_content = html_entity_decode(strip_tags($post->text_content));
    $db->query(sql_placeholder("UPDATE news SET header = ?, text_content = ? WHERE id = ?", $post->header, $post->text_content, $post->id));
}
echo PHP_EOL, "ALL DONE", PHP_EOL;
