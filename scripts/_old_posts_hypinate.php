<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set('memory_limit', '128M');
ini_set('max_execution_time', 180);

require_once('./../Config.class.php');
require_once('./../placeholder.php');
require_once('./../Database.class.php');
require_once('./../adminv2/BlogPost.admin.php');


$config = new Config();
$db = new Database($config->dbname, $config->dbhost, $config->dbuser, $config->dbpass);

if(!$db->connect())
{
    print "Не могу подключиться к базе данных. Проверьте настройки подключения";
    exit();
} else {
    echo 'Подключение к БД успешно', PHP_EOL;
}

$a = 0;
$blogPost = new BlogPost($a);

$db->query("SELECT id, name FROM blogposts ORDER BY id DESC");
$posts = $db->results();


foreach ($posts as $i => $b) {
    $hyphenateText = $blogPost->hyphenate($b->name);
    if ($hyphenateText !== $b->name) {
        $db->query(sql_placeholder("UPDATE blogposts SET name = ? WHERE id = ?", $hyphenateText, $b->id));
    }
}

$db->query("SELECT id, header FROM news ORDER BY id DESC");
$posts = $db->results();


foreach ($posts as $i => $b) {
    $hyphenateText = $blogPost->hyphenate($b->header);
    if ($hyphenateText !== $b->header) {
        $db->query(sql_placeholder("UPDATE news SET header = ? WHERE id = ?", $hyphenateText, $b->id));
    }
}

