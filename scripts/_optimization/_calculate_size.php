<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set('memory_limit', '128M');

$startTime = microtime(true);

$scirptPath = realpath(__DIR__. '/../');

require_once($scirptPath.'/../Config.class.php');
require_once($scirptPath.'/../placeholder.php');
require_once($scirptPath.'/../Database.class.php');
require_once($scirptPath.'/../adminv2/TinyPngImageOptimizer.php');


$config = new Config();
$db = new Database($config->dbname, $config->dbhost, $config->dbuser, $config->dbpass);


if(!$db->connect())
{
    print "Не могу подключиться к базе данных. Проверьте настройки подключения";
    exit();
} else {
    echo 'Подключение к БД успешно';
}
$db->query('SET NAMES utf8;');

$GLOBALS['config'] = $config;
$GLOBALS['root_path'] = realpath(__DIR__ . '/../../');
$GLOBALS['excludePathImages'] = array(
    'photogallerys', 'news', 'blogposts', 'banners', 'spec_project', 'tags', 'writers', 'events', 'conferences', 'about'
);


$db->query("SELECT *
FROM `optimizer_log` WHERE (`size_orig` = 0 OR `size_orig` IS NULL) AND image_orig IS NOT NULL
ORDER BY `optimizer_log`.`id` DESC");
$logs = $db->results();



foreach ($logs as $i => $log) {
    $decodedName = $GLOBALS['root_path'].$log->image;
    $decodedOriginalName = $GLOBALS['root_path'].$log->image_orig;

    $db->query(sql_placeholder("UPDATE optimizer_log SET  size_orig = ?, `size` = ?  WHERE id = ?"
        , (int)@filesize($decodedOriginalName), (int)@filesize($decodedName), $log->id));
}

$endTime = microtime(true);
echo PHP_EOL, 'Execution time: ', $endTime - $startTime;