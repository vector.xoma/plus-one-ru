<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set('memory_limit', '128M');

require_once('./../../Config.class.php');
require_once('./../../placeholder.php');
require_once('./../../Database.class.php');

$config = new Config();
$db = new Database($config->dbname, $config->dbhost, $config->dbuser, $config->dbpass);

if(!$db->connect())
{
    print "Не могу подключиться к базе данных. Проверьте настройки подключения";
    exit();
} else {
    echo 'Подключение к БД успешно', PHP_EOL;
}
$db->query('SET NAMES utf8;');

$tablename = 'blogposts';
$localeFile = 'replaceUrlAbsoluteToRelative.csv';

$GLOBALS['root_path'] = realpath(__DIR__ . '/../../');

$GLOBALS['blogposts_upload_path'] = realpath($GLOBALS['root_path'] . '/files/blogposts/');
$GLOBALS['news_upload_path'] = realpath($GLOBALS['root_path'] . '/files/news/');

$db->query("SELECT id, url, tags, body, header, lead, created FROM blogposts ORDER BY id DESC");
$postList = $db->results();
$localeFilePath = __DIR__ .DIRECTORY_SEPARATOR. $localeFile;

$GLOBALS['excludePathImages'] = array(
    'photogallerys', 'news', 'blogposts', 'banners', 'spec_project', 'tags', 'writers', 'events', 'conferences', 'about'
);

function grab_path_images($entity, $fieldList, $itemType, Database $db)
{
    $matches = array();
    foreach ($fieldList as $field) {
        preg_match_all('/src="(\/files\/[^"]+\/[^"]+)"/', $field, $matches);
        if ($matches[1]){
            foreach ($matches[1] as $ind => $fileName) {
                $found = false;
                foreach($GLOBALS['excludePathImages'] as $folder) {
                    if (mb_strpos($fileName, '/files/'.$folder) !== false) {
                        $found = true;
                        break;
                    };
                }
                if ($found) continue;

                if (!isset($GLOBALS['images'][$fileName])) $GLOBALS['images'][$fileName] = array();
                if (!isset($GLOBALS['images'][$fileName]['ids'])) $GLOBALS['images'][$fileName]['ids'] = array();
                $GLOBALS['images'][$fileName]['ids'][] = $entity->id;
                $GLOBALS['images'][$fileName]['created'] = $entity->created;
                $GLOBALS['images'][$fileName]['item_type'] = $itemType;
            }
        }
    }
    return;
}

$GLOBALS['images'] = array();
foreach ($postList as $i => $post) {
    $fieldList = [
        'body' => html_entity_decode($post->body),
        'header' => html_entity_decode($post->header),
        'lead' => html_entity_decode($post->lead),
    ];
    grab_path_images($post,  $fieldList, 'post', $db);
}


$db->query("SELECT id, url, tags, text_content, article_text, lead, created FROM news ORDER BY id DESC");
$newsList = $db->results();

foreach ($newsList as $i => $news) {
    $fieldList = [
        'article_text' => html_entity_decode($news->article_text),
        'text_content' => html_entity_decode($news->text_content),
        'lead' => html_entity_decode($news->lead),
    ];
    grab_path_images($news, $fieldList, 'news', $db);
}

$ROOT_PATH = $GLOBALS['root_path'];

foreach ($GLOBALS['images'] as $pathFrom => $arr) {
    $nowDate = date('Y-m-d H:i:s');
    $decodedName = urldecode($pathFrom);
    $db->query(sql_placeholder("INSERT INTO optimizer_log (item_id, item_type, image, status, created, `size`) VALUE (?, ?, ?, 0, ?, ?)"
        , $arr['ids'][0], $arr['item_type'], $decodedName, $nowDate, (int)@filesize($ROOT_PATH.$pathFrom)));

}