<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set('memory_limit', '128M');
ini_set('max_execution_time', 180);

require_once('./../../Config.class.php');
require_once('./../../placeholder.php');
require_once('./../../Database.class.php');

$config = new Config();
$db = new Database($config->dbname, $config->dbhost, $config->dbuser, $config->dbpass);

if(!$db->connect())
{
    print "Не могу подключиться к базе данных. Проверьте настройки подключения";
    exit();
} else {
    echo 'Подключение к БД успешно', PHP_EOL;
}
$db->query('SET NAMES utf8;');

$tablename = 'blogposts';
$localeFile = 'replaceUrlAbsoluteToRelative.csv';

$GLOBALS['root_path'] = realpath(__DIR__ . '/../../');
$GLOBALS['photogallerys_upload_path'] = realpath($GLOBALS['root_path'] . '/files/photogallerys/');

$db->query("SELECT id, filename FROM images ORDER BY id DESC");
$gallerysList = $db->results();

$GLOBALS['excludePathImages'] = array(
    'photogallerys', 'news', 'blogposts', 'banners', 'spec_project', 'tags', 'writers', 'events', 'conferences', 'about'
);

$GLOBALS['images'] = array();
foreach ($gallerysList as $i => $gallery) {
    $photogallerys =  '/files/photogallerys/';
    $fieldList = ['filename' => true];

    foreach ($fieldList as $field => $val) {
        if ($gallery->$field) {
            $fileName = $photogallerys . $gallery->$field;
            $GLOBALS['images'][$fileName] = $gallery->id;
        }
    }
}

foreach ($GLOBALS['images'] as $pathFrom => $itemId) {
    $nowDate = date('Y-m-d H:i:s');
    $decodedName = urldecode($pathFrom);
    $db->query(sql_placeholder("INSERT INTO optimizer_log (item_id, item_type, image, status, created, `size`) VALUE (?, 'gallery', ?, 0, ?, ?)"
        , $itemId, $decodedName, $nowDate, (int)@filesize($GLOBALS['root_path'].$pathFrom)));
}