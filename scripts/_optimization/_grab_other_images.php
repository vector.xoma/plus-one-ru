<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set('memory_limit', '128M');
ini_set('max_execution_time', 180);

require_once('./../../Config.class.php');
require_once('./../../placeholder.php');
require_once('./../../Database.class.php');

$GLOBALS['root_path'] = realpath(__DIR__ . '/../../');

$config = new Config();
$db = new Database($config->dbname, $config->dbhost, $config->dbuser, $config->dbpass);

if(!$db->connect())
{
    print "Не могу подключиться к базе данных. Проверьте настройки подключения";
    exit();
} else {
    echo 'Подключение к БД успешно', PHP_EOL;
}



$db->query("SELECT * FROM blogwriters ORDER BY id DESC");
$writers = $db->results();


$spec_projectPATH =  '/files/writers/';
foreach ($writers as $i => $wr) {
    $nowDate = date('Y-m-d H:i:s');
    $fieldList = ['image' => true, 'stub_image' => true];

    foreach ($fieldList as $field => $val) {
        if ($wr->$field) {
            $fileName = $spec_projectPATH . $wr->$field;
            $decodedName = urldecode($fileName);
            $db->query(sql_placeholder("INSERT INTO optimizer_log (item_id, item_type, image, status, created, `size`) VALUE (?, 'writer', ?, 0, ?, ?)"
                , $wr->id, $decodedName, $nowDate, (int)@filesize($GLOBALS['root_path'].$fileName)));
        }
    }
}
