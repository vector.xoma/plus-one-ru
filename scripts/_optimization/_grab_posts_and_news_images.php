<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set('memory_limit', '128M');
ini_set('max_execution_time', 180);

require_once('./../../Config.class.php');
require_once('./../../placeholder.php');
require_once('./../../Database.class.php');

$config = new Config();
$db = new Database($config->dbname, $config->dbhost, $config->dbuser, $config->dbpass);

if(!$db->connect())
{
    print "Не могу подключиться к базе данных. Проверьте настройки подключения";
    exit();
} else {
    echo 'Подключение к БД успешно', PHP_EOL;
}
$db->query('SET NAMES utf8;');

$tablename = 'blogposts';
$localeFile = 'replaceUrlAbsoluteToRelative.csv';

$GLOBALS['root_path'] = realpath(__DIR__ . '/../../');
$GLOBALS['blogposts_upload_path'] = realpath($GLOBALS['root_path'].'/files/blogposts/');

$db->query("SELECT id, url, tags, image_1_1, image_1_1_c, image_1_2, image_1_3, image_1_4, image_rss, thumbnail, created 
                 FROM blogposts 
             ORDER BY id DESC");
$postList = $db->results();

$GLOBALS['excludePathImages'] = array(
    'photogallerys', 'news', 'blogposts', 'banners', 'spec_project', 'tags', 'writers', 'events', 'conferences', 'about'
);

$GLOBALS['images'] = array();
foreach ($postList as $i => $post) {
    $blogpostsPath =  '/files/blogposts/';
    $fieldList = ['image_1_1' => true, 'image_1_1_c' => true, 'image_1_2' => true, 'image_1_3' => true, 'image_1_4' => true, 'image_rss' => true];

    foreach ($fieldList as $field => $val) {
        if ($post->$field) {
            $fileName = $blogpostsPath . $post->$field;
            $GLOBALS['images'][$fileName] = $post->id;
        }
    }
}
foreach ($GLOBALS['images'] as $pathFrom => $itemId) {
    $nowDate = date('Y-m-d H:i:s');
    $decodedName = urldecode($pathFrom);
    $db->query(sql_placeholder("INSERT INTO optimizer_log (item_id, item_type, image, status, created, `size`) VALUE (?, 'post', ?, 0, ?, ?)"
        , $itemId, $decodedName, $nowDate, (int)@filesize($GLOBALS['root_path'].$pathFrom)));
}

$GLOBALS['images'] = array();

$db->query("SELECT id, url, image_1_1, image_1_2, image_1_3, image_rss,  created 
                 FROM news 
             ORDER BY id DESC");
$newsList = $db->results();

foreach ($newsList as $i => $news) {
    $newsPath =  '/files/news/';
    $fieldList = ['image_1_1' => true, 'image_1_2' => true, 'image_1_3' => true,  'image_rss' => true];

    foreach ($fieldList as $field => $val) {
        if ($post->$field) {
            $fileName = $newsPath . $news->$field;
            $GLOBALS['images'][$fileName] = $news->id;
        }
    }
}

foreach ($GLOBALS['images'] as $pathFrom => $itemId) {
    $nowDate = date('Y-m-d H:i:s');
    $decodedName = urldecode($pathFrom);
    $db->query(sql_placeholder("INSERT INTO optimizer_log (item_id, item_type, image, status, created, `size`) VALUE (?, 'news', ?, 0, ?, ?)"
        , $itemId, $decodedName, $nowDate, (int)@filesize($GLOBALS['root_path'].$pathFrom)));
}