<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set('memory_limit', '128M');

$startTime = microtime(true);

$scirptPath = realpath(__DIR__. '/../');

require_once($scirptPath.'/../Config.class.php');
require_once($scirptPath.'/../placeholder.php');
require_once($scirptPath.'/../Database.class.php');
require_once($scirptPath.'/../adminv2/TinyPngImageOptimizer.php');


$config = new Config();
$db = new Database($config->dbname, $config->dbhost, $config->dbuser, $config->dbpass);


if(!$db->connect())
{
    print "Не могу подключиться к базе данных. Проверьте настройки подключения";
    exit();
} else {
    echo 'Подключение к БД успешно';
}
$db->query('SET NAMES utf8;');

$GLOBALS['config'] = $config;
$GLOBALS['root_path'] = realpath(__DIR__ . '/../../');
$GLOBALS['excludePathImages'] = array(
    'photogallerys', 'news', 'blogposts', 'banners', 'spec_project', 'tags', 'writers', 'events', 'conferences', 'about'
);


$db->query("SELECT * FROM optimizer_log WHERE status = 0 ORDER BY item_id DESC LIMIT 15");
$logs = $db->results();


function optimizeImage($fileFrom, $fileTo = false) {

    $result = false;
    if (TinyPngImageOptimizer::optimize($fileFrom, $fileTo)) {
        chown($fileTo, 'www-data');
        $result = true;
    }
    return $result;
}

function mkdir_custom($path) {

    if (file_exists($path)) {
        return true;
    }
    return @mkdir($path, 0775, true);
}


$ROOT_PATH = $GLOBALS['root_path'];

TinyPngImageOptimizer::init();
TinyPngImageOptimizer::setApiKey($GLOBALS['config']->tinypng['api_key']);

/** Errors :
 *  2  => Не найден файл
 *  3  => Api не смог оптимизировать
 *  4  => Не удалось переместить файл оригинала
 *  5  => Не удалось создать каталог для файла оригинала
*/
foreach ($logs as $i => $log) {
    $decodedName = $log->image;
    if (!file_exists($GLOBALS['root_path']. $decodedName)) {
        $db->query(sql_placeholder('UPDATE optimizer_log SET status = 2 WHERE id = ?', $log->id));
        continue;
    }

    $decodedOriginalName = preg_replace('/^\/files\//', '/files_original/', $decodedName);
    if (mkdir_custom(dirname($ROOT_PATH.$decodedOriginalName))) {
        if (rename($ROOT_PATH . $decodedName, $ROOT_PATH . $decodedOriginalName)) {
            $nowDate = date('Y-m-d H:i:s');
            if (optimizeImage($ROOT_PATH . $decodedOriginalName, $ROOT_PATH . $decodedName)) {
                $db->query(sql_placeholder("UPDATE optimizer_log SET status = 1, image_orig = ?, size_orig = `size`, `size` = ?  WHERE id = ?"
                    , $decodedOriginalName, (int)@filesize($ROOT_PATH.$decodedName), $log->id));
                continue;
            } else {
                $db->query(sql_placeholder("UPDATE optimizer_log SET status = 3 WHERE id = ?", $log->id));
                rename($ROOT_PATH . $decodedOriginalName, $ROOT_PATH . $decodedName);
            }
        } else {
            $db->query(sql_placeholder("UPDATE optimizer_log SET status = 4 WHERE id = ?", $log->id));
        }
    } else {
        $db->query(sql_placeholder("UPDATE optimizer_log SET status = 5 WHERE id = ?", $log->id));
    }
}

$endTime = microtime(true);
echo PHP_EOL, 'Execution time: ', $endTime - $startTime;