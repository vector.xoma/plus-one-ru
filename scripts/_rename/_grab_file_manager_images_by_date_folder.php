<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set('memory_limit', '128M');

require_once('./../../Config.class.php');
require_once('./../../placeholder.php');
require_once('./../../Database.class.php');
$GLOBALS['root_path'] = realpath(__DIR__ . '/../../');

$config = new Config();
$db = new Database($config->dbname, $config->dbhost, $config->dbuser, $config->dbpass);

if(!$db->connect())
{
    print "Не могу подключиться к базе данных. Проверьте настройки подключения";
    exit();
} else {
    echo 'Подключение к БД успешно';
}
$db->query('SET NAMES utf8;');
$db->query("  
                (SELECT 'blogposts' as type, id, url, tags, body, header, lead, created FROM blogposts ORDER BY id DESC)
                    UNION ALL
                (SELECT 'news' as type, id, url, tags, article_text as body, text_content as header, lead, created FROM news ORDER BY id DESC)
                ");

$postList = $db->results();

$GLOBALS['excludePathImages'] = array(
    'photogallerys', 'news', 'blogposts', 'banners', 'spec_project', 'tags', 'writers', 'events', 'conferences', 'about'
);

$fieldMap = array(
    'news' => array(
        'lead' => 'lead',
        'body' => 'article_text',
        'header' => 'text_content'
    ),
    'blogposts' => array(
        'lead' => 'lead',
        'body' => 'body',
        'header' => 'header'
    )
);

function grab_path_images($post,  Database $db)
{
    $fieldList = [
        'body' => html_entity_decode($post->body),
        'header' => html_entity_decode($post->header),
        'lead' => html_entity_decode($post->lead),
    ];
    $matches = array();
    foreach ($fieldList as $field) {
        preg_match_all('/src="(\/files\/[^"]+\/[^"]+)"/', $field, $matches);
        if ($matches[1]){
            foreach ($matches[1] as $ind => $fileName) {
                $fileName = urldecode($fileName);
                $found = false;
                foreach($GLOBALS['excludePathImages'] as $folder) {
                    if (mb_strpos($fileName, '/files/' . $folder) !== FALSE) {
                        $found = true;
                        break;
                    };
                }

                if ($found || preg_match("/(\d{4})\/(\d{2})/", dirname($fileName))) {continue;}

                if (!isset($GLOBALS['images'][$fileName]))                      $GLOBALS['images'][$fileName] = array();
                if (!isset($GLOBALS['images'][$fileName]['created']))           $GLOBALS['images'][$fileName]['created'] = $post->created;
                if (!isset($GLOBALS['images'][$fileName][$post->type]))         $GLOBALS['images'][$fileName][$post->type] = array();
                if (!isset($GLOBALS['images'][$fileName][$post->type]['ids']))  $GLOBALS['images'][$fileName][$post->type]['ids'] = array();

                $GLOBALS['images'][$fileName][$post->type]['ids'][] = $post->id;
            }
        }
    }

    return;
}

$GLOBALS['images'] = array() ;
foreach ($postList as $i => $post) {
    grab_path_images($post,  $db);
}


function generateNewPath($pathFrom,  $created) {
    $ROOT_PATH = $GLOBALS['root_path'];
    $oldPath = $ROOT_PATH.$pathFrom;

    $dateFolder = 'image_' . (new DateTime($created))->format('Y/m');

    $newPath = $ROOT_PATH . '/files/' . $dateFolder . '/' . basename($oldPath);

    $pattern = preg_quote($ROOT_PATH, '/');
    return preg_replace("/^{$pattern}/", '', $newPath);
}

$ROOT_PATH = $GLOBALS['root_path'];

foreach ($GLOBALS['images'] as $pathFrom => $arr) {
    $nowDate = date('Y-m-d H:i:s');
    $pathTo = generateNewPath($pathFrom, $arr['created']);
    if ($pathTo !== $pathFrom) {
        $db->query(sql_placeholder(
            "INSERT INTO replace_path_log (old_path, new_path, item_id, item_type, created, status, params) VALUE (?, ?, 0, 'file_manager', ?, 0, ?)"
            , $pathFrom, $pathTo, $nowDate, serialize($arr)
        ));
    }
}

echo PHP_EOL, "Images: ", count($GLOBALS['images']), PHP_EOL;
