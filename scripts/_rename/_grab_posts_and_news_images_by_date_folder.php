<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set('memory_limit', '128M');
ini_set('max_execution_time', 180);


$startTime = microtime(true);


require_once('./../../Config.class.php');
require_once('./../../placeholder.php');
require_once('./../../Database.class.php');

$GLOBALS['root_path'] = realpath(__DIR__ . '/../../');

$config = new Config();
$db = new Database($config->dbname, $config->dbhost, $config->dbuser, $config->dbpass);

if(!$db->connect())
{
    print "Не могу подключиться к базе данных. Проверьте настройки подключения";
    exit();
} else {
    echo 'Подключение к БД успешно', PHP_EOL;
}
$db->query('SET NAMES utf8;');
$db->query("      
          (SELECT 'blogposts' as type, id, url, tags, body, header, lead, created, image_1_1, image_1_1_c, image_1_2, image_1_3, image_1_4, image_rss, thumbnail FROM blogposts ORDER BY id DESC)
                    UNION ALL
          (SELECT 'news' as type, id, url, tags, article_text as body, text_content as header, lead, created, image_1_1, null as image_1_1_c, image_1_2, image_1_3, null as image_1_4, image_rss, null as thumbnail FROM news ORDER BY id DESC)
             ");
$postList = $db->results();

function generateNewPath($pathFrom,  $created) {
    $ROOT_PATH = $GLOBALS['root_path'];
    $oldPath = $ROOT_PATH.$pathFrom;
    $dirName = dirname($oldPath);

    $dateFolder = '' . (new DateTime($created))->format('Y/m');

    if (preg_match("/(\d{4})\/(\d{2})/", $dirName)) {
        return $pathFrom;
    }

    $newPath = $dirName . '/' . $dateFolder . '/' .basename($oldPath);

    $pattern = preg_quote($ROOT_PATH, '/');
    return preg_replace("/^{$pattern}/", '', $newPath);
}

function mkdir_custom($path) {

    if (file_exists($path)) {
        return true;
    }
    return @mkdir($path, 0775, true);
}

function grab_image($pathFrom, $post, $created, Database $db, $fieldImage) {
    $newPath = generateNewPath($pathFrom, $created);
    if ($pathFrom !== $newPath) {
        $params = array('field_image' => $fieldImage);
        if (isset($GLOBALS['images'][$pathFrom])) {
            $params = array_merge($params, $GLOBALS['images'][$pathFrom]);
        }
        $db->query(sql_placeholder("INSERT INTO replace_path_log (old_path, new_path, item_id, item_type, created, status, params) VALUE (?, ?, ?, ?, ?, ?, ?)",
            $pathFrom, $newPath, $post->id, $post->type, date('Y-m-d H:i:s'), 0, serialize($params)));
    }
    return $pathFrom !== $newPath;
}

function grab_path_images($entity, Database $db)
{
    $rootSitePath = "/files/{$entity->type}/";
    switch ($entity->type) {
        case "blogposts" :
            $fieldImages = ['image_1_1' => true, 'image_1_1_c' => true, 'image_1_2' => true, 'image_1_3' => true, 'image_1_4' => true, 'image_rss' => true, 'thumbnail' => true];
            foreach ($fieldImages as $field => $val) {
                if ($entity->$field) {
                    $fileName = $rootSitePath . $entity->$field;
                    grab_image($fileName, $entity, $entity->created, $db, $field);
                }
            }
            break;
        case "news" :
            $newsPath = '/files/news/';
            $fieldImages = ['image_1_1' => true, 'image_1_2' => true, 'image_1_3' => true,  'image_rss' => true];
            foreach ($fieldImages as $field => $val) {
                if ($entity->$field) {
                    $fileName = $newsPath . $entity->$field;
                    grab_image($fileName, $entity, $entity->created, $db, $field);
                }
            }
            break;
        case "photogallerys":
            $fieldImages = ['filename' => true,];
            foreach ($fieldImages as $field => $val) {
                if ($entity->$field) {
                    $fileName = $rootSitePath . $entity->$field;
                    grab_image($fileName, $entity, $entity->created, $db, $field);
                }
            }
            break;
        case "writers":
            $fieldImages = ['image' => true,'stub_image' => true, 'announcement_img' => true];
            foreach ($fieldImages as $field => $val) {
                if ($entity->$field) {
                    $fileName = $rootSitePath . $entity->$field;
                    grab_image($fileName, $entity, $entity->created, $db, $field);
                }
            }
            break;
        case "spec_project":
            $fieldImages = ['image_1_1' => true, 'image_1_2' => true, 'image_1_3' => true,];
            foreach ($fieldImages as $field => $val) {
                if ($entity->$field) {
                    $fileName = $rootSitePath . $entity->$field;
                    grab_image($fileName, $entity, $entity->created, $db, $field);
                }
            }
            break;
        case "conferences":
            $fieldImages = ['image_1_1' => true];
            foreach ($fieldImages as $field => $val) {
                if ($entity->$field) {
                    $fileName = $rootSitePath . $entity->$field;
                    grab_image($fileName, $entity, $entity->created, $db, $field);
                }
            }
            break;
        case "events":
            $fieldImages = ['image' => true];
            foreach ($fieldImages as $field => $val) {
                if ($entity->$field) {
                    $fileName = $rootSitePath . $entity->$field;
                    grab_image($fileName, $entity, $entity->created, $db, $field);
                }
            }
            break;
        default:
            return false;
    }
    return true;
}


$GLOBALS['images'] = array();

function check_image($fieldList, $post, $folder)
{
    $matches = array();
    foreach ($fieldList as $field) {
        preg_match_all('/="(\/files\/'.$folder.'\/[^"]+)"/', $field, $matches);
        if ($matches[1]){
            foreach ($matches[1] as $ind => $fileName) {
                $fileName = urldecode($fileName);
                if (!isset($GLOBALS['images'][$fileName]))                      $GLOBALS['images'][$fileName] = array();
                if (!isset($GLOBALS['images'][$fileName][$post->type]))         $GLOBALS['images'][$fileName][$post->type] = array();
                if (!isset($GLOBALS['images'][$fileName][$post->type]['ids']))  $GLOBALS['images'][$fileName][$post->type]['ids'] = array();

                $GLOBALS['images'][$fileName][$post->type]['ids'][] = $post->id;
            }
        }
    }
}

echo PHP_EOL, 'Grab articles: ',   PHP_EOL;
foreach ($postList as $i => $post) {
    $replaceFieldList = [
        'header' => html_entity_decode($post->header),
        'lead' => html_entity_decode($post->lead),
        'body' => html_entity_decode($post->body)
    ];
    check_image($replaceFieldList, $post,  $post->type);
    check_image($replaceFieldList, $post,  'photogallerys');
}

echo PHP_EOL, 'Grab blogposts and news: ',   PHP_EOL;
foreach ($postList as $i => $post) {
  grab_path_images($post, $db);
}
unset($postList);

echo PHP_EOL, 'Grab photogallerys: ',   PHP_EOL;
$db->query("SELECT 'photogallerys' as type, i.filename, pg.created, pg.id FROM photo_galleries pg JOIN images i ON i.gallery_id = pg.id ");
$galleries = $db->results();
foreach ($galleries as $i => $gallery) {
  grab_path_images($gallery, $db);
}
unset($galleries);

echo PHP_EOL, 'Grab writers: ',   PHP_EOL;
$db->query("SELECT 'writers' as type, bw.*, CONCAT(CURRENT_DATE(), ' ' ,CURRENT_TIME()) as created  FROM blogwriters bw ");
$writers = $db->results();
foreach ($writers as $i => $writer) {
  grab_path_images($writer, $db);
}
unset($writers);

echo PHP_EOL, 'Grab spec_project: ',   PHP_EOL;
$db->query("SELECT 'spec_project' as type, sp.*  FROM spec_projects sp ");
$spec_projects = $db->results();
foreach ($spec_projects as $i => $spec_project) {
  grab_path_images($spec_project, $db);
}
unset($spec_project);

echo PHP_EOL, 'Grab conferences: ',   PHP_EOL;
$db->query("SELECT 'conferences' as type, c.*  FROM conferences c ");
$conferences = $db->results();
foreach ($conferences as $i => $con) {
  grab_path_images($con, $db);
}
unset($conferences);

echo PHP_EOL, 'Grab events: ',   PHP_EOL;
$db->query("SELECT 'events' as type, e.*  FROM events e ");
$events = $db->results();
foreach ($events as $i => $e) {
  grab_path_images($e, $db);
}
unset($events);

$endTime = microtime(true);
echo PHP_EOL, 'Execution time: ', $endTime - $startTime , PHP_EOL;
