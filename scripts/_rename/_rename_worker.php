<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set('memory_limit', '128M');

$startTime = microtime(true);

require_once('./../../Config.class.php');
require_once('./../../placeholder.php');
require_once('./../../Database.class.php');
require_once('./../../adminv2/TinyPngImageOptimizer.php');

$GLOBALS['root_path'] = realpath(__DIR__ . '/../../');

$config = new Config();
$db = new Database($config->dbname, $config->dbhost, $config->dbuser, $config->dbpass);

if(!$db->connect())
{
    print "Не могу подключиться к базе данных. Проверьте настройки подключения";
    exit();
} else {
    echo 'Подключение к БД успешно';
}
$db->query('SET NAMES utf8;');

$GLOBALS['excludePathImages'] = array(
    'photogallerys', 'news', 'blogposts', 'banners', 'spec_project', 'tags', 'writers', 'events', 'conferences', 'about'
);

$db->query("SELECT * FROM replace_path_log WHERE status = 0 ORDER BY id DESC LIMIT 1000");
$logs = $db->results();

function mkdir_custom($path) {

    if (file_exists($path)) {
        return true;
    }
    return @mkdir($path, 0775, true);
}


function move_image($pathFrom, $pathTo) {
    $ROOT_PATH = $GLOBALS['root_path'];
    $oldPath = $ROOT_PATH.$pathFrom;
    if (file_exists($oldPath)) {
        if ($pathTo !== $pathFrom && mkdir_custom(dirname($ROOT_PATH.$pathTo))) {
            if (rename($oldPath, $ROOT_PATH.$pathTo)) {
                return true;
            }
        }
    }
    return false;
}


function replace_path($pathFrom, $pathTo, $postIds, $itemsType, Database $db)
{

    switch ($itemsType) {
        case "blogposts" :
            $fieldList = [
                'body' => true,
                'header' => true,
                'lead' => true,
            ];

            $postIds = join(',' , array_merge($postIds, [0]));
            foreach ($fieldList as $fieldName => $text) {
                $db->query(sql_placeholder(
                    "UPDATE blogposts SET {$fieldName} = REPLACE({$fieldName}, ?, ?) WHERE id IN ({$postIds})",
                    $pathFrom, $pathTo
                ));
            }
            break;
        case "news" :
            $fieldList = [
                'article_text' => true,
                'text_content' => true,
                'lead' => true,
            ];

            $postIds = join(',' , array_merge($postIds, [0]));
            foreach ($fieldList as $fieldName => $text) {
                $db->query(sql_placeholder(
                    "UPDATE news SET {$fieldName} = REPLACE({$fieldName}, ?, ?) WHERE id IN ({$postIds})",
                    $pathFrom, $pathTo
                ));
            }
            break;
        default:
            return false;
    }

    return true;
}

$GLOBALS['images'] = array();


$tableNameMap = array(
    'blogposts' => 'blogposts',
    'news' => 'news',
    'photogallerys' => 'photo_galleries',
    'spec_project' => 'spec_projects',
    'writers' => 'blogwriters',
    'events' => 'events',
    'conferences' => 'conferences',
);

/**
 * 1 => Success
 * 2 => File not exists
 * 3 => Not moved file
 */
foreach ($logs as $i => $log) {
    if (!file_exists($GLOBALS['root_path']. $log->old_path)) {
        $db->query(sql_placeholder('UPDATE replace_path_log SET status = 2 WHERE id = ?', $log->id));
        continue;
    }
    $arr = $log->params ? unserialize($log->params) : null;

    if (move_image($log->old_path, $log->new_path)) {
        if ($log->item_id && $log->item_type) {
            if (isset($arr['field_image']) && $arr['field_image']) {
                $newPath = preg_replace("/^\/files\/{$log->item_type}\//", '',$log->new_path);
                $tableName = @$tableNameMap[$log->item_type];
                $db->query(sql_placeholder("UPDATE {$tableName} SET {$arr['field_image']} = ? WHERE id = ?", $newPath, $log->item_id));
            }
        }

        if (is_array($arr)) {
            foreach ($arr as $type => $params) {
                if (in_array($type, array('blogposts', 'news'))) {
                    if (isset($params['ids']) && count($params['ids']) > 0) {
                        replace_path($log->old_path, $log->new_path, $params['ids'], $type, $db);
                    }
                }
            }
        }

        $db->query(sql_placeholder('UPDATE replace_path_log SET status = 1 WHERE id = ?', $log->id));
    } else {
        $db->query(sql_placeholder('UPDATE replace_path_log SET status = 3 WHERE id = ?', $log->id));
    }
}

$endTime = microtime(true);
echo PHP_EOL, 'Execution time: ', $endTime - $startTime , PHP_EOL;
