<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set('memory_limit', '128M');
ini_set('max_execution_time', 180);


$startTime = microtime(true);


require_once('./../../Config.class.php');
require_once('./../../placeholder.php');
require_once('./../../Database.class.php');

$GLOBALS['root_path'] = realpath(__DIR__ . '/../../');

$config = new Config();
$db = new Database($config->dbname, $config->dbhost, $config->dbuser, $config->dbpass);

if(!$db->connect())
{
    print "Не могу подключиться к базе данных. Проверьте настройки подключения";
    exit();
} else {
    echo 'Подключение к БД успешно', PHP_EOL;
}
$db->query('SET NAMES utf8;');
$db->query("      
          (SELECT 'blogposts' as type, id, url, tags, body, header, lead, created, image_1_1, image_1_1_c, image_1_2, image_1_3, image_1_4, image_rss, thumbnail FROM blogposts ORDER BY id DESC)
                    UNION ALL
          (SELECT 'news' as type, id, url, tags, article_text as body, text_content as header, lead, created, image_1_1, null as image_1_1_c, image_1_2, image_1_3, null as image_1_4, image_rss, null as thumbnail FROM news ORDER BY id DESC)
             ");
$postList = $db->results();


function grab_image($pathFrom, $post, $created, Database $db, $fieldImage) {
    $GLOBALS['images'][$GLOBALS['root_path'] . urldecode($pathFrom)] = $post->type . '_' . $post->id;
}

function grab_path_images($entity, Database $db)
{
    $rootSitePath = "/files/{$entity->type}/";
    switch ($entity->type) {
        case "blogposts" :
            $fieldImages = ['image_1_1' => true, 'image_1_1_c' => true, 'image_1_2' => true, 'image_1_3' => true, 'image_1_4' => true, 'image_rss' => true, 'thumbnail' => true];
            foreach ($fieldImages as $field => $val) {
                if ($entity->$field) {
                    $fileName = $rootSitePath . $entity->$field;
                    grab_image($fileName, $entity, $entity->created, $db, $field);
                }
            }
            break;
        case "news" :
            $newsPath = '/files/news/';
            $fieldImages = ['image_1_1' => true, 'image_1_2' => true, 'image_1_3' => true,  'image_rss' => true];
            foreach ($fieldImages as $field => $val) {
                if ($entity->$field) {
                    $fileName = $newsPath . $entity->$field;
                    grab_image($fileName, $entity, $entity->created, $db, $field);
                }
            }
            break;
        case "photogallerys":
            $fieldImages = ['filename' => true,];
            foreach ($fieldImages as $field => $val) {
                if ($entity->$field) {
                    $fileName = $rootSitePath . $entity->$field;
                    grab_image($fileName, $entity, $entity->created, $db, $field);
                }
            }
            break;
        case "writers":
            $fieldImages = ['image' => true,'stub_image' => true, 'announcement_img' => true];
            foreach ($fieldImages as $field => $val) {
                if ($entity->$field) {
                    $fileName = $rootSitePath . $entity->$field;
                    grab_image($fileName, $entity, $entity->created, $db, $field);
                }
            }
            break;
        case "spec_project":
            $fieldImages = ['image_1_1' => true, 'image_1_2' => true, 'image_1_3' => true,];
            foreach ($fieldImages as $field => $val) {
                if ($entity->$field) {
                    $fileName = $rootSitePath . $entity->$field;
                    grab_image($fileName, $entity, $entity->created, $db, $field);
                }
            }
            break;
        case "conferences":
            $fieldImages = ['image_1_1' => true];
            foreach ($fieldImages as $field => $val) {
                if ($entity->$field) {
                    $fileName = $rootSitePath . $entity->$field;
                    grab_image($fileName, $entity, $entity->created, $db, $field);
                }
            }
            break;
        case "events":
            $fieldImages = ['image' => true];
            foreach ($fieldImages as $field => $val) {
                if ($entity->$field) {
                    $fileName = $rootSitePath . $entity->$field;
                    grab_image($fileName, $entity, $entity->created, $db, $field);
                }
            }
            break;
        default:
            return false;
    }
    return true;
}


$GLOBALS['images'] = array();

function check_image($fieldList, $post)
{
    $matches = array();
    foreach ($fieldList as $field) {
        preg_match_all('/="(\/files\/[^"]+)"/', $field, $matches);
        if ($matches[1]){
            foreach ($matches[1] as $ind => $fileName) {
                $fileName = $GLOBALS['root_path'] . urldecode($fileName);
                $GLOBALS['images'][$fileName] = $post->type . '_' . $post->id;
            }
        }
    }
}

foreach ($postList as $i => $post) {
    $replaceFieldList = [
        'header' => html_entity_decode($post->header),
        'lead' => html_entity_decode($post->lead),
        'body' => html_entity_decode($post->body)
    ];
    check_image($replaceFieldList, $post);
}

foreach ($postList as $i => $post) {
    grab_path_images($post, $db);
}
unset($postList);

$db->query("SELECT 'photogallerys' as type, i.filename, pg.created, pg.id FROM photo_galleries pg JOIN images i ON i.gallery_id = pg.id ");
$galleries = $db->results();
foreach ($galleries as $i => $gallery) {
    grab_path_images($gallery, $db);
}
unset($galleries);

$db->query("SELECT 'writers' as type, bw.*, CONCAT(CURRENT_DATE(), ' ' ,CURRENT_TIME()) as created  FROM blogwriters bw ");
$writers = $db->results();
foreach ($writers as $i => $writer) {
    grab_path_images($writer, $db);
}
unset($writers);

$db->query("SELECT 'spec_project' as type, sp.*  FROM spec_projects sp ");
$spec_projects = $db->results();
foreach ($spec_projects as $i => $spec_project) {
    grab_path_images($spec_project, $db);
}
unset($spec_project);

$db->query("SELECT 'conferences' as type, c.*  FROM conferences c ");
$conferences = $db->results();
foreach ($conferences as $i => $con) {
    grab_path_images($con, $db);
}
unset($conferences);

$db->query("SELECT 'events' as type, e.*  FROM events e ");
$events = $db->results();
foreach ($events as $i => $e) {
    grab_path_images($e, $db);
}
unset($events);

$endTime = microtime(true);
echo PHP_EOL, 'Execution time: ', $endTime - $startTime , PHP_EOL;

echo PHP_EOL, 'Images: ', count($GLOBALS['images']), PHP_EOL;




//--------------------------------------------------------------
// Функция рекурсивного сканирования каталога
//--------------------------------------------------------------
// Параметры:
//   $directory - начальный каталог
//   $callback - функция для обработки найденных файлов
//--------------------------------------------------------------
function scan_recursive($directory, $callback = null)
{
    // Привести каталог в канонизированный абсолютный путь
    $directory = realpath($directory);

    if ($d = opendir($directory)) {
        while ($fname = readdir($d)) {
            if ($fname == '.' || $fname == '..') {
                continue;
            } else {
                // Передать путь файла в callback-функцию
                if ($callback != null && is_callable($callback)) {
                    $callback($directory . DIRECTORY_SEPARATOR . $fname);
                }
            }

            if (is_dir($directory . DIRECTORY_SEPARATOR . $fname)) {
                scan_recursive($directory . DIRECTORY_SEPARATOR . $fname, $callback);
            }
        }
        closedir($d);
    }
}

function mkdir_custom($path) {

    if (file_exists($path)) {
        return true;
    }
    return @mkdir($path, 0775, true);
}


$GLOBALS['unused'] = array();

// Callback-функция, которая будет принимать имена файлов
function scan_callback($fname) {
    if (is_dir($fname) || preg_match('/.empty$/', $fname)) return;
    if (preg_match('/\.xml$/', $fname)) return;
    if (!isset($GLOBALS['images'][$fname])) {
        $GLOBALS['unused'][$fname] = true;
    }
}

$ROOT_FILES_PATH = $GLOBALS['root_path'] . '/files/';
$ROOT_UNUSED_FILES_PATH = $GLOBALS['root_path'] . '/files/__un_used/';

mkdir_custom($ROOT_UNUSED_FILES_PATH);
// Вывести список файлов и каталогов
scan_recursive($ROOT_FILES_PATH, 'scan_callback');

echo PHP_EOL, 'UNUSED: ', count($GLOBALS['unused']), PHP_EOL;

if (@$argv[1] == '--move') {
    echo '--move'; die;
    foreach ($GLOBALS['unused'] as $file => $flag) {

        $pattern = preg_quote($ROOT_FILES_PATH, '/');

        $newName = preg_replace("/^{$pattern}/", $ROOT_UNUSED_FILES_PATH, $file);

        $newDirname = dirname($newName) . '/updated__' . date("Y-m", filemtime($file)) . '/';

        $newName = $newDirname . basename($newName);

        mkdir_custom($newDirname);

        rename($file, $newName);

    }
} else if(@$argv[1] == '--print'){
    print_r($GLOBALS['unused']);
} else {
    $size = 0;
    foreach ($GLOBALS['unused'] as $path => $item) {
        $size += (int)@filesize($path);
    }
    echo PHP_EOL, "File sizes: " , number_format($size / 1024 / 1024, 2, '.', ' ') , ' Mb', PHP_EOL;
}

