<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set('memory_limit', '128M');

$startTime = microtime(true);

require_once('./../../Config.class.php');
require_once('./../../placeholder.php');
require_once('./../../Database.class.php');
require_once('./../../adminv2/TinyPngImageOptimizer.php');

$GLOBALS['root_path'] = realpath(__DIR__ . '/../../');

$config = new Config();
$db = new Database($config->dbname, $config->dbhost, $config->dbuser, $config->dbpass);

if(!$db->connect())
{
    print "Не могу подключиться к базе данных. Проверьте настройки подключения";
    exit();
} else {
    echo 'Подключение к БД успешно';
}
$db->query('SET NAMES utf8;');

$GLOBALS['excludePathImages'] = array(
    'photogallerys', 'news', 'blogposts', 'banners', 'spec_project', 'tags', 'writers', 'events', 'conferences', 'about'
);

$db->query("SELECT * FROM replace_path_log WHERE status = 1002 LIMIT 1500");
$logs = $db->results();

function mkdir_custom($path) {

    if (file_exists($path)) {
        return true;
    }
    return @mkdir($path, 0775, true);
}


function move_image($pathFrom, $pathTo) {
    $ROOT_PATH = $GLOBALS['root_path'];
    $oldPath = $ROOT_PATH.$pathFrom;
    if (file_exists($oldPath)) {
        if ($pathTo !== $pathFrom && mkdir_custom(dirname($ROOT_PATH.$pathTo))) {
            if (rename($oldPath, $ROOT_PATH.$pathTo)) {
                return true;
            }
        }
    }
    return false;
}

function getOriginalPath($path) {
    return preg_replace('/^\/files\//', '/files_original/', $path);
}


/**
 * 111 => Success
 * 222 => File not exists
 * 333 => Not moved file
 */
foreach ($logs as $i => $log) {
    $db->query(sql_placeholder("SELECT * FROM optimizer_log WHERE image = ?", $log->old_path));
    $opts = $db->results();

    $status = 1001;
    if (file_exists($GLOBALS['root_path'] . $log->new_path)) {
        foreach ($opts as $opt) {

            $status = 1002;
            if ($opt->image_orig) {
                $newOrig = getOriginalPath($log->new_path);
                if (move_image($opt->image_orig, $newOrig)) {
                    $db->query(sql_placeholder('UPDATE optimizer_log SET image = ?,  image_orig = ? WHERE id = ?', $log->new_path, $newOrig, $opt->id));
                    $status = 1003;
                }
            }
        }
    } else {
        $status = 1004;
    }
    $db->query(sql_placeholder("UPDATE replace_path_log SET status = ? WHERE id = ?", $status, $log->id));
}

$endTime = microtime(true);
echo PHP_EOL, 'Execution time: ', $endTime - $startTime , PHP_EOL;
