<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set('memory_limit', '128M');

$startTime = microtime(true);

require_once('./../Config.class.php');
require_once('./../placeholder.php');
require_once('./../Database.class.php');
require_once('./../adminv2/TinyPngImageOptimizer.php');

$GLOBALS['root_path'] = realpath(__DIR__ . '/../');

$config = new Config();
$db = new Database($config->dbname, $config->dbhost, $config->dbuser, $config->dbpass);

if(!$db->connect())
{
    print "Не могу подключиться к базе данных. Проверьте настройки подключения";
    exit();
} else {
    echo 'Подключение к БД успешно';
}
$db->query('SET NAMES utf8;');

$GLOBALS['excludePathImages'] = array(
    'photogallerys', 'news', 'blogposts', 'banners', 'spec_project', 'tags', 'writers', 'events', 'conferences', 'about'
);

$db->query("SELECT * FROM replace_path_log WHERE status = 0 ORDER BY id DESC LIMIT 1");
$logs = $db->results();

function mkdir_custom($path) {

    if (file_exists($path)) {
        return true;
    }
    return @mkdir($path, 0775, true);
}

function move_image($pathFrom, $pathTo) {
    if (file_exists($pathFrom)) {
        if ($pathTo !== $pathFrom && mkdir_custom(dirname($GLOBALS['root_path'].$pathTo))) {
            if (rename($pathFrom, $GLOBALS['root_path'].$pathTo)) {
                return true;
            }
        }
    }
    return false;
}

$GLOBALS['images'] = array();


$tableNameMap = array(
    'blogposts' => 'blogposts',
    'news' => 'news',
    'photogallerys' => 'photo_galleries',
    'spec_project' => 'spec_projects',
    'writers' => 'blogwriters',
    'events' => 'events',
    'conferences' => 'conferences',
);

/**
 * 1 => Success
 * 2 => File not exists
 * 3 => Not moved file
 */
foreach ($logs as $i => $log) {

    $arr = $log->params ? unserialize($log->params) : null;

    $realPath  = $arr['real_path'];
    if (move_image($realPath, $log->new_path)) {
        if (is_array($arr)) {
            $fieldName = $arr['field_image'];
            foreach ($arr as $type => $params) {
                if (in_array($type, array('blogposts'))) {
                    if (isset($params['ids']) && count($params['ids']) > 0) {
                        $postIds = join(',', array_merge($params['ids'], [0]));
                        $db->query(sql_placeholder(
                            "SELECT id, {$fieldName} FROM blogposts WHERE id IN ({$postIds})"
                        ));
                        $posts = $db->results();
                        foreach ($posts as $post) {
                            $newBody = str_replace(htmlentities(htmlspecialchars($log->old_path)), $log->new_path, $post->$fieldName);
                            if ($newBody !== $post->$fieldName) {
                                $db->query(sql_placeholder(
                                    "UPDATE blogposts SET {$fieldName} = ? WHERE id IN ({$postIds})", $newBody
                                ));
                                var_dump($postIds);
                            }
                        }
                    }
                }
            }
        }
        $db->query(sql_placeholder('UPDATE replace_path_log SET status = 11 WHERE id = ?', $log->id));
    }
}

$endTime = microtime(true);
echo PHP_EOL, 'Execution time: ', $endTime - $startTime , PHP_EOL;
