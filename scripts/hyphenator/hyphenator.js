const createHyphenator = require("hyphen");
const patterns = require("hyphen/patterns/ru");
const express = require('express');

const hyphenate = createHyphenator(patterns, { async: false });
const app = express();

app.use(express.json());

const _symbol = '&shy;';

app.post('/', function (req, res) {
    let word = hyphenate(req.body.text || '', { hyphenChar: _symbol });
    let index = word.indexOf(_symbol);
    while (index !== -1 && index <= 11 ) {
        word = word.replace(_symbol, '');
        index = word.indexOf(_symbol);
    }
    res.send({ok:true, result: word});
});

app.listen(8071, '127.0.0.1');


