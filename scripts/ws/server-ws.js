const WebSocket = require('ws');

const wss = new WebSocket.Server({port: 8080});

var editablePosts = [];
var connections = [];
var connectionCounter = 1;

wss.on('connection', function connection(ws) {
    let id = connectionCounter++;
    connections[id] = {_edit: false, ws: false};
    ws.on('message', function incoming(message) {
        let obj = false;
        try {
            obj = JSON.parse(message);
        } catch (e) {
            obj = false;
        }
        if (obj && obj.hasOwnProperty('postID') && obj.postID) {
            connections[id] = { _edit:false, ws: ws};
            let postID = obj.host + '-' + obj.postID;
            let locked = false;
            if (!editablePosts[postID]) {
                connections[id]._edit = obj;
                editablePosts[postID] = obj;
                editablePosts[postID]._timeout = false;
            } else {
                if (editablePosts[postID]['author'] === obj.author) {
                    if (editablePosts[postID]._timeout) {
                        clearTimeout(editablePosts[postID]._timeout);
                        editablePosts[postID]._timeout = false;
                        connections[id]._edit = obj;
                    } else {
                        locked = true;
                    }
                } else {
                    locked = true;
                }
            }
            if (locked) {
                ws.send(JSON.stringify({ok: false, err: 'Уже редактируется', user: editablePosts[postID].author}));
            }
        }
    });

    ws.on('close', function (event) {
        onCloseConnection(event)
    });

    ws.on('error', function (event) {
        onCloseConnection(event)
    });

    function onCloseConnection(event) {
        if (connections[id]._edit) {
            let postID = connections[id]._edit['host'] + '-' + connections[id]._edit['postID'];
            if (editablePosts[postID]) {
                if (editablePosts[postID]['author'] === connections[id]._edit['author']) {
                    editablePosts[postID]._timeout =
                        setTimeout(function () {
                            delete editablePosts[postID];
                        }, 15000);
                }
            }
        }
        delete  connections[id];
    }
});
