### Билд:

### Редактор

1. в `src/index.js` комментим всё, что связано с `AppUserFront`, расскоменчиваем всё, что связано с `AppEditor`
2. в `package.json` изменяем параметр `"homepage": "/adminv2/editor/build"`
3. `yarn build`
4. переносим билд в ветку `api_v1`
5. в папку `adminv2/editor/`
6. `adminv2/templates/index.tpl` - обоновить пути до js и css

### Главная (Основное приложение)

1. в `package.json` проверяем параметр `"homepage": "/sp/build"`
2. `yarn build`
3. копируем `/sp/build/index.html` в `/index.html`
