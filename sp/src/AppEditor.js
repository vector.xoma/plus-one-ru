import React, { useState, useEffect } from 'react';
import { BrowserRouter } from 'react-router-dom';

import Editor from 'pages/Editor';
import { clearText } from 'clearText';

import 'antd/dist/antd.css';
import './reset.css';
import './styles.scss';
import 'fonts/fonts.scss';

function App({ globalVar, areaName }) {
  const [serverData, setServerData] = useState(
    window[globalVar] ? { text: clearText(window[globalVar]) } : null,
  );

  useEffect(() => {
    setServerData({ text: clearText(window[globalVar]) });
  }, [window[globalVar]]);

  return (
    <BrowserRouter>
      <Editor
        serverData={serverData}
        updateData={setServerData}
        areaName={areaName}
      />
    </BrowserRouter>
  );
}

export default App;
