import React, { useEffect } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import { routes } from './routes';
import { MenuProvider } from 'contexts/menuContext';
import { RowsBlocksProvider } from 'contexts/rowsBlocksContext';
import { RubricProvider } from 'contexts/rubricsContext';
import { NewsBlocksProvider } from 'contexts/newsBlocksContext';
import { NewsSliderProvider } from 'contexts/newsSliderContext';
import { SearchInputProvider } from 'contexts/searchInputContext';

import './reset.css';
import './styles.scss';
import 'fonts/fonts.scss';

export default () => (
  <BrowserRouter>
    <NewsSliderProvider>
      <RowsBlocksProvider>
        <RubricProvider>
          <MenuProvider>
            <NewsBlocksProvider>
              <SearchInputProvider>
                <Switch>
                  {routes.map((params, key) => (
                    <Route key={key} {...params} />
                  ))}
                </Switch>
              </SearchInputProvider>
            </NewsBlocksProvider>
          </MenuProvider>
        </RubricProvider>
      </RowsBlocksProvider>
    </NewsSliderProvider>
  </BrowserRouter>
);
