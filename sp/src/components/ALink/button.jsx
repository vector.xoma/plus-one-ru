import React, { useState, useEffect } from 'react';
import { Popover, Button, Input, Icon, Checkbox } from 'antd';
import { splitBySelection } from 'utils';
import { Wrap } from '../Grid/Grid';
import styled from 'styled-components/macro';

const LinkButtonWrap = styled.div`
  width: 500px;
`;

const LinkButton = ({ setALink, textData }) => {
  const { middle } = splitBySelection(textData.text, textData.area);

  const [visible, setVisible] = useState(false);
  const [url, setUrl] = useState('');
  const [urlText, setUrlText] = useState(middle || '');
  const [targetBlank, setTargetBlank] = useState(false);

  useEffect(() => {
    setUrlText(middle);
  }, [middle]);

  const Content = (
    <LinkButtonWrap>
      <Wrap>
        <Input
          placeholder="Url"
          value={url || ''}
          onChange={e => setUrl(e.target.value)}
        />
      </Wrap>
      <Wrap>
        <Input
          placeholder="Текст ссылки"
          value={urlText || ''}
          onChange={e => setUrlText(e.target.value)}
        />
      </Wrap>
      <Wrap>
        <Checkbox
          checked={targetBlank}
          onChange={e => setTargetBlank(e.target.checked)}
        >
          В новом окне
        </Checkbox>
      </Wrap>
      <Wrap>
        <Button
          onClick={() => {
            setUrl('');
            setALink({ url, urlText, targetBlank });
            setTargetBlank(false);
            setVisible(false);
          }}
        >
          Добавить
        </Button>
      </Wrap>
    </LinkButtonWrap>
  );

  return (
    <Popover
      content={Content}
      title="Ссылка"
      trigger="click"
      placement="right"
      visible={visible}
      onVisibleChange={visible => {
        setVisible(visible);
      }}
    >
      <Button>
        <Icon type="link" />
      </Button>
    </Popover>
  );
};

export default LinkButton;
