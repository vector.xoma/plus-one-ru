import React, { useContext } from 'react';
import styled, { ThemeContext } from 'styled-components/macro';

const A = styled.a`
  display: inline;
  border-bottom: 2px solid;
  border-bottom-color: ${({ theme }) =>
    theme.font_color ? theme.border_color : '#f8c946'};
  -webkit-transition: all 0.4s;
  transition: all 0.4s;
  text-decoration: none;
  color: ${({ theme, themeContext }) =>
    theme.font_color ? theme.font_color : themeContext.color};
  :hover {
    color: ${({ theme, themeContext }) =>
      theme.font_color ? theme.font_color : themeContext.color};
    border-bottom: 2px solid #fff;
  }
`;

const ALink = ({ data }) => {
  const themeContext = useContext(ThemeContext);

  return (
    <A
      href={data.url}
      target={data.targetBlank === 'true' ? '_blank' : '_self'}
      themeContext={themeContext}
    >
      {data.text}
    </A>
  );
};

export default ALink;
