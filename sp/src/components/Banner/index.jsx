import React, { useState, useEffect, forwardRef } from 'react';
import useDesktop from 'hooks/useDesktop';
import _uniqueId from 'lodash/uniqueId';
import styled from 'styled-components/macro';

const BannersWrapper = styled.div`
  ${({ bannerStyle }) => bannerStyle}
`;
const BannerElem = styled.div`
  max-width: 1140px;
  margin: 0 auto;
`;

// Баннеры подключены через ГТМ и
// при первой инициализации компонент срабатывает быстрее
// чем появляется window.Ya.adfoxCode
function checkAdFox({ IsAdFoxScript, setIsAdFoxScript }) {
  let counter = 0;
  const timerId = setInterval(() => {
    counter += 1;
    if (counter === 10) {
      console.group(
        '%cADFOX INFO',
        'color: white; background-color: red; padding: 3px;',
      );
      console.error('Похоже window.Ya.adfox не найден');
      console.error('Значение window.Ya: ', window.Ya);
      console.groupEnd();
      clearInterval(timerId);
    }
    if (window.Ya && window.Ya.adfoxCode && !IsAdFoxScript) {
      setIsAdFoxScript(true);
      clearInterval(timerId);
    }
  }, 500);
}

const Banner = (
  { elemId, bannerStyle = {}, p2Desctop = '', p2Mobile = '' },
  ref,
) => {
  const desktop = useDesktop();
  const [IsAdFoxScript, setIsAdFoxScript] = useState(false);
  const [id, setId] = useState(elemId);

  useEffect(() => {
    checkAdFox({ IsAdFoxScript, setIsAdFoxScript });
    !id && setId(_uniqueId());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  useEffect(() => {
    if (desktop) {
      IsAdFoxScript &&
        window.Ya.adfoxCode.create({
          ownerId: 260854,
          containerId: `adfox_${id}`,
          params: {
            pp: 'g',
            ps: 'cock',
            p2: p2Desctop,
          },
        });

      return;
    }
    if (!desktop) {
      IsAdFoxScript &&
        window.Ya.adfoxCode.create({
          ownerId: 260854,
          containerId: `adfox_m_${id}`,
          params: {
            pp: 'g',
            ps: 'cock',
            p2: p2Mobile,
          },
        });
      return;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [desktop, IsAdFoxScript]);

  return (
    <BannersWrapper bannerStyle={bannerStyle} ref={ref}>
      {desktop ? (
        <BannerElem id={`adfox_${id}`} key={`adfox_${id}`} />
      ) : (
        <BannerElem id={`adfox_m_${id}`} key={`adfox_m_${id}`} />
      )}
    </BannersWrapper>
  );
};

export default forwardRef(Banner);
