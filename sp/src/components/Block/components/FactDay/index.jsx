import React from 'react';
import { useHistory } from 'react-router-dom';
import _get from 'lodash/get';
import useDesktop from 'hooks/useDesktop';
import { clearText } from 'clearText';
import TagsTitle from '../TagsTitle';
import { checkBlank } from '../../helpers';

import { AWrap } from 'components/Grid/Grid';
import {
  FactDay1,
  FactDay2,
  FactDay3,
  FactDay1Text,
  FactDay2Text,
  FactDay3Text,
  FactDay1Title,
  FactDay2Title,
  FactDay3Title,
  FactGroupItem,
} from './style';

const FactDay = ({
  masonryMobileLayout = {},
  articlePageRow,
  isMasonryPage,
  setDragging,
  styles = {},
  style = {},
  dragging,
  data,
}) => {
  const isDesktop = useDesktop();
  const { linkParentWindowForPost: postBlank, url, block: size } = data;
  const blankValue = postBlank ? '_blank' : '_self';
  const history = useHistory();
  const fact1 = (
    <FactDay1
      bgc={_get(data, 'postFormat.backgroundColor')}
      customstyle={style.container}
      iconName={data.iconName}
    >
      <TagsTitle data={data} />
      <AWrap target={blankValue} to={`/${url}`}></AWrap>
      <FactDay1Text href={url}>
        <FactDay1Title
          fontColor={_get(data, 'postFormat.fontColor')}
          customStyleTitle={style.textTitle}
          factStyles={styles.fact}
          iconName={data.iconName}
        >
          {clearText(data.name)}
        </FactDay1Title>
      </FactDay1Text>
    </FactDay1>
  );

  const fact2 = (
    <FactDay2
      bgc={_get(data, 'postFormat.backgroundColor')}
      customstyle={style.container}
      iconName={data.iconName}
      className="post2"
    >
      <TagsTitle data={data} />
      <AWrap target={blankValue} to={`/${url}`}></AWrap>
      <FactDay2Text href={url}>
        <FactDay2Title
          fontColor={_get(data, 'postFormat.fontColor')}
          customStyleTitle={style.textTitle}
          factStyles={styles.fact}
        >
          {clearText(data.name)}
        </FactDay2Title>
      </FactDay2Text>
    </FactDay2>
  );

  const fact3 = (
    <FactDay3
      masonryMobileLayout={isDesktop ? {} : masonryMobileLayout}
      bgc={_get(data, 'postFormat.backgroundColor')}
      customstyle={style.container}
      iconName={data.iconName}
      className="post3"
    >
      <TagsTitle data={data} block3 />
      <AWrap target={blankValue} to={url[0] === '/' ? url : `/${url}`}></AWrap>
      <FactDay3Text href={url[0] === '/' ? url : `/${url}`}>
        <FactDay3Title
          fontColor={_get(data, 'postFormat.fontColor')}
          customStyleTitle={style.textTitle}
          isMasonryPage={isMasonryPage}
          factStyles={styles.fact}
        >
          {clearText(data.name)}
        </FactDay3Title>
      </FactDay3Text>
    </FactDay3>
  );

  if (articlePageRow) {
    return fact3;
  }
  switch (size) {
    case '1_1': {
      return fact1;
    }
    case '1_2': {
      return fact2;
    }
    case '1_3': {
      return fact3;
    }
    default: {
      return (
        <FactGroupItem bgc={_get(data, 'postFormat.backgroundColor')}>
          {data.iconName && (
            <TagsTitle
              setDragging={setDragging}
              dragging={dragging}
              data={data}
              isGroup
            />
          )}
          <AWrap
            to={url}
            onDragStart={e => {
              e.preventDefault();
              setDragging(true);
            }}
            onClick={e => {
              e.preventDefault();
              setDragging(false);
              if (!dragging) {
                checkBlank(blankValue, url, history);
              }
            }}
            draggable={false}
          />
          <FactDay3Text href={url}>
            <FactDay3Title
              fontColor={_get(data, 'postFormat.fontColor')}
              factStyles={styles.fact}
            >
              {clearText(data.name)}
            </FactDay3Title>
          </FactDay3Text>
        </FactGroupItem>
      );
    }
  }
};

export default FactDay;
