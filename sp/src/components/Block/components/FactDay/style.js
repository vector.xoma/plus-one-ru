import styled from 'styled-components/macro';
import {
  GridItem1,
  GridItem2,
  GridItem3,
  GridItemGroup,
} from 'components/Grid/Grid';

const FactDay1 = styled(GridItem1)`
  height: 100%;
  min-height: auto;
  background-color: ${({ bgc }) => bgc};
  display: flex;
  align-items: ${({ iconName }) => (iconName ? 'inherit' : 'center')};
  flex-direction: column;
  ${({ customstyle }) => customstyle}
  justify-content: flex-start;
  @media (max-width: 767px) {
    padding: 20px;
    min-height: auto;
  }
`;
const FactDay2 = styled(GridItem2)`
  height: 100%;
  background-color: ${({ bgc }) => bgc};
  display: flex;
  justify-content: ${({ iconName }) => (iconName ? 'flex-start' : 'center')};
  align-items: ${({ iconName }) => (iconName ? 'inherit' : 'center')};
  flex-direction: column;
  ${({ customstyle }) => customstyle}
  justify-content: flex-start;
  @media (max-width: 767px) {
    padding: 20px;
    min-height: auto;
  }
`;

const FactDay3 = styled(GridItem3)`
  height: 100%;
  background-color: ${({ bgc }) => bgc};
  display: flex;
  justify-content: ${({ iconName }) => (iconName ? 'flex-start' : 'center')};
  align-items: ${({ iconName }) => (iconName ? 'inherit' : 'center')};
  flex-direction: column;
  ${({ customstyle }) => customstyle}
  justify-content: flex-start;
  @media (max-width: 767px) {
    padding: 20px;
    min-height: auto;
    ${({ masonryMobileLayout }) => masonryMobileLayout};
  }
`;
const FactDay1Text = styled.a`
  margin: auto;
`;
const FactDay2Text = styled.a`
  margin: auto;
`;
const FactDay3Text = styled.a`
  margin: auto;
`;

const FactDay1Title = styled.div`
  font-size: 42px;
  font-family: "GeometricSansSerifv1";
  text-align: center;
  line-height: 42px;
  color: ${({ fontColor }) => fontColor};
  ${({ customStyleTitle }) => customStyleTitle}
  font-size: ${({ fz }) => fz};
  @media (max-width: 767px) {
    font-size: 28px;
    line-height: 1.07;
  }
`;
const FactDay2Title = styled.div`
  font-size: 42px;
  font-family: "GeometricSansSerifv1";
  text-align: center;
  line-height: 42px;
  color: ${({ fontColor }) => fontColor};
  ${({ customStyleTitle }) => customStyleTitle}
  font-size: ${({ fz }) => fz};
  @media (max-width: 767px) {
    font-size: 28px;
    line-height: 1.07;
  }
`;
const FactDay3Title = styled.div`
  font-family: 'GeometricSansSerifv1';
  line-height: 1;
  color: ${({ fontColor }) => fontColor};
  ${({ customStyleTitle }) => customStyleTitle}
  /* ${({ factStyles }) => factStyles}; TODO: Возможно понадобится*/
  text-align: center;
  /* font-size: ${({ isMasonryPage }) =>
    isMasonryPage ? '28px' : '36px'}; TODO: Возможно понадобится*/
    font-size: 36px;
  word-break: ${({ isMasonryPage }) =>
    isMasonryPage ? 'break-word' : 'inherit'};
  @media (max-width: 767px) {
    font-size: 28px;
    line-height: 1.07;
  }
`;

const FactGroupItem = styled(GridItemGroup)`
  background-color: ${({ bgc }) => bgc};
  display: flex;
  justify-content: ${({ iconName }) => (iconName ? 'flex-start' : 'center')};
  align-items: ${({ iconName }) => (iconName ? 'inherit' : 'center')};
  flex-direction: column;
  @media (max-width: 767px) {
    padding: 20px;
    ${FactDay3Title} {
      word-break: break-word;
    }
  }
`;

export {
  FactDay1,
  FactDay2,
  FactDay3,
  FactDay1Text,
  FactDay2Text,
  FactDay3Text,
  FactDay1Title,
  FactDay2Title,
  FactDay3Title,
  FactGroupItem,
};
