import React, { useState, useRef, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Slider from 'react-slick';
import Block from 'components/Block';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { SliderContainer, CustomLeftArrow, CustomRightArrow } from './style.js';
import useDesktop from 'hooks/useDesktop';

const SampleNextArrow = ({
  onClick,
  className,
  computedPositionY,
  arrowPositionX,
  arrowPositionY,
}) => {
  return (
    <CustomRightArrow
      className={className}
      onClick={onClick}
      arrowPositionX={arrowPositionX}
      arrowPositionY={arrowPositionY}
      computedPositionY={computedPositionY}
    />
  );
};

const SamplePrevArrow = ({
  onClick,
  className,
  computedPositionY,
  arrowPositionX,
  arrowPositionY,
}) => {
  return (
    <CustomLeftArrow
      className={className}
      onClick={onClick}
      arrowPositionX={arrowPositionX}
      arrowPositionY={arrowPositionY}
      computedPositionY={computedPositionY}
    />
  );
};

const styles = {
  fact: { fontSize: '30px', textAlign: 'left' },
};

const styleAlign = {
  justifyContent: 'space-between',
};
// isDesktop обновляет занеово слайдер.
// Это необходимо для правильного центрирования
// активного блока слайдера на мобилках и десктопе

const Group = ({ data = [], groupName, url = '' }) => {
  const isDesktop = useDesktop();
  const dotsRef = useRef();
  const [arrowPositionY, setArrowPositionY] = useState(null);
  useEffect(() => {
    dotsRef &&
      dotsRef.current &&
      setArrowPositionY(
        `${
          dotsRef.current.offsetTop +
          dotsRef.current.getBoundingClientRect().height / 2
        }px`,
      );
  }, []);

  // Адаптивная позиция стрелок в слайдере
  const dotsInLine = 40;
  const arrowPositionX =
    data.length >= dotsInLine
      ? `120px`
      : `${(1140 - data.length * 20 - 90) / 2}px`;

  const [dragging, setDragging] = useState(false);

  const settings = {
    className: 'center',
    centerMode: true,
    initialSlide: isDesktop ? 1 : 0,
    infinite: true,
    centerPadding: '50px',
    slidesToShow: 3,
    speed: 500,
    lazyLoad: true,
    // waitForAnimate: false,  ---- Не дожидается окончания перелистывания слайда , работает хуже чем в slick jquery
    dots: true,
    swipeToSlide: true,
    nextArrow: (
      <SampleNextArrow
        arrowPositionX={arrowPositionX}
        arrowPositionY={arrowPositionY}
        computedPositionY={data.length >= dotsInLine}
      />
    ),
    prevArrow: (
      <SamplePrevArrow
        arrowPositionX={arrowPositionX}
        arrowPositionY={arrowPositionY}
        computedPositionY={data.length >= dotsInLine}
      />
    ),
    beforeChange: () => setDragging(true),
    afterChange: () => setDragging(false),
    initialSlide: isDesktop ? 1 : 0,
    appendDots: dots => (
      <div
        style={{
          position: 'static',
          marginTop: '20px',
        }}
      >
        <ul style={{ margin: '0px' }} ref={dotsRef}>
          {dots}
        </ul>
      </div>
    ),
    responsive: [
      {
        breakpoint: 767,
        settings: {
          initialSlide: isDesktop ? 1 : 0,
          arrows: false,
          swipe: true,
          centerMode: true,
          centerPadding: '20px',
          slidesToShow: 1,
          touchMove: true,
          dots: false,
        },
      },
    ],
  };

  return (
    <SliderContainer key={isDesktop ? '1' : '2'}>
      <Link to={`/story/${url}`}>
        <h2>{groupName}</h2>
      </Link>
      <Slider {...settings}>
        {data.map(({ id, typePost, ...other }) => (
          <Block
            setDragging={setDragging}
            iconCode={data.iconCode}
            styleAlign={styleAlign}
            dragging={dragging}
            type={typePost}
            styles={styles}
            data={other}
            key={id}
            isGroup
          />
        ))}
      </Slider>
    </SliderContainer>
  );
};

export default Group;
