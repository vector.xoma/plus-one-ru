import styled from 'styled-components/macro';
import leftArrowImg from 'img/arrowLeft.svg';
import rightArrowImg from 'img/arrowRight.svg';

const SliderContainer = styled.div`
  width: 100%;
  margin: 0 auto;
  background-color: #eaeaea;
  padding: 30px 0 30px;
  margin-bottom: 20px;
  border-radius: 5px;
  h2 {
    margin-bottom: 20px;
    font-family: 'MullerBold';
    font-size: 30px;
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.2;
    letter-spacing: normal;
    text-align: center;
    color: #000000;
  }
  .slick-dots {
    ul {
      max-width: 800px;
      width: 100%;
      margin: 0 auto !important;
    }
    li {
      margin: 0;
      button {
        ::before {
          font-size: 8px;
        }
      }
    }
  }
`;

const CustomArrows = styled.div`
  font-size: 0;
  line-height: 0;
  position: absolute;
  bottom: -8px;
  top: auto;
  display: block;
  width: auto;
  height: 20px;
  padding: 0;
  cursor: pointer;
  color: transparent;
  border: none;
  outline: none;
  background: transparent;
  top: ${({ arrowPositionY, computedPositionY }) =>
    computedPositionY && arrowPositionY};
`;

const CustomLeftArrow = styled(CustomArrows)`
  left: ${({ arrowPositionX }) => arrowPositionX};

  ::before {
    left: ${({ arrowPositionX }) => arrowPositionX};
    top: ${({ arrowPositionY, computedPositionY }) =>
      computedPositionY && arrowPositionY};

    content: url(${leftArrowImg});
  }
  ${({ arrowPositionY, computedPositionY, arrowPositionX }) => {}};
`;

const CustomRightArrow = styled(CustomArrows)`
  right: ${({ arrowPositionX }) => arrowPositionX};
  ::before {
    right: ${({ arrowPositionX }) => arrowPositionX};
    top: ${({ arrowPositionY, computedPositionY }) =>
      computedPositionY && arrowPositionY};
    content: url(${rightArrowImg});
  }
`;

export { SliderContainer, CustomArrows, CustomLeftArrow, CustomRightArrow };
