import React from 'react';
import { useHistory } from 'react-router-dom';
import _get from 'lodash/get';
import useDesktop from 'hooks/useDesktop';
import { clearText } from 'clearText';
import TagsTitle from '../TagsTitle';
import { checkBlank } from '../../helpers';
import { AWrap } from 'components/Grid/Grid';
import {
  NewsAfter,
  NewsWrap,
  NewsText,
  NewsTitle,
  NewsSubTitle,
} from './style';

const News = ({
  data,
  style = {},
  masonryMobileLayout = {},
  dragging,
  setDragging = false,
  articlePageRow,
  isMasonryPage,
  isGroup,
}) => {
  const isDesktop = useDesktop();
  const { linkParentWindowForPost: postBlank, url } = data;
  const blankValue = postBlank ? '_blank' : '_self';
  const history = useHistory();

  return (
    <NewsWrap
      backgroundColor={_get(data, 'postFormat.backgroundColor')}
      masonryMobileLayout={isDesktop ? {} : masonryMobileLayout}
      className={`news ${articlePageRow && 'post3'}`}
      articlePageRow={articlePageRow}
      customstyle={style.container}
      isGroup={isGroup}
    >
      <TagsTitle
        announceType={data.type_announce_1_3}
        setDragging={setDragging}
        dragging={dragging}
        data={data}
        isGroup
        block3
      />
      {articlePageRow || isMasonryPage ? (
        <AWrap to={url[0] === '/' ? url : `/${url}`} />
      ) : (
        <AWrap
          to={url}
          onDragStart={e => {
            e.preventDefault();
            setDragging(true);
          }}
          onClick={e => {
            e.preventDefault();
            setDragging(false);
            if (!dragging) {
              checkBlank(blankValue, url, history);
            }
          }}
          draggable={false}
        />
      )}
      {data.image ? <img src={data.image} alt="" /> : null}
      <NewsText href={url}>
        <NewsTitle
          fontColor={_get(data, 'postFormat.fontColor')}
          customstyle={style.textTitle}
          image={data.image}
        >
          {clearText(data.name)}
        </NewsTitle>
        {data.header ? (
          <NewsSubTitle
            fontColor={_get(data, 'postFormat.fontColor')}
            customstyle={style.textSubTitle}
            className="newsSubtitle"
            image={data.image}
          >
            {clearText(data.header)}
          </NewsSubTitle>
        ) : null}
      </NewsText>
      {data.image && <NewsAfter />}
    </NewsWrap>
  );
};

export default News;
