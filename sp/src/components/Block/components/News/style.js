import { GridItemGroup } from 'components/Grid/Grid';
import styled from 'styled-components/macro';

const NewsAfter = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background-image: linear-gradient(
    to bottom,
    rgba(0, 0, 0, 0.22),
    rgba(0, 0, 0, 0.41) 48%,
    rgba(0, 0, 0, 0.8)
  );
  z-index: 5;
  border-radius: 5px;
`;

const NewsWrap = styled(GridItemGroup)`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  transition: transform ease 1s;
  ${({ customstyle }) => customstyle}
  ${({ articlePageRow }) =>
    articlePageRow && {
      marginLeft: '0',
      minHeight: '505px',
      transform: 'scale(1)',
      marginBottom: '20px',
    }}
    :hover {
    ${({ articlePageRow }) =>
      articlePageRow && {
        transform: 'scale(1.01)',
      }}
  }
  img {
    z-index: auto;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    display: block;
    width: 100%;
    height: 100%;
    border-radius: 5px;
    object-fit: cover;
  }
  background-color: #fff;
  @media (max-width: 767px) {
    width: ${({ isGroup }) => (isGroup ? 'calc(100% - 20px)' : '100%')};
    padding: 20px;
    min-height: 410px;

    ${({ masonryMobileLayout }) => masonryMobileLayout};
    ${({ articlePageRow }) =>
      articlePageRow && {
        marginRight: '0 !important',
        marginBottom: '20px',
      }}
  }
`;
const NewsText = styled.a`
  min-height: initial;
  text-align: left;
  z-index: 6;
  position: relative;
  text-decoration: none;
`;

const NewsTitle = styled.div`
  font-size: 30px;
  line-height: 1.2;
  font-family: 'MullerBold', sans-serif;
  word-break: break-word;
  color: ${({ image }) => (image ? '#fff' : '#000')};
  ${({ customstyle }) => customstyle}
  @media (max-width: 767px) {
    font-size: 20px;
    font-family: 'MullerBold';
    line-height: 1.3;
  }
`;

const NewsSubTitle = styled.div`
  display: flex;
  margin-top: 20px;
  justify-content: center;
  font-size: 19px;
  font-family: 'MullerRegular';
  line-height: 1.26;
  color: ${({ image }) => (image ? '#fff' : '#000')};
  ${({ customstyle }) => customstyle}

  @media (max-width: 767px) {
    font-size: 16px;
    line-height: 1.33;
  }
`;

export { NewsAfter, NewsWrap, NewsText, NewsTitle, NewsSubTitle };
