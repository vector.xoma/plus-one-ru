import React from 'react';

import _get from 'lodash/get';

import useDesktop from 'hooks/useDesktop';
import { clearText } from 'clearText';

import PreparedLink from 'components/PreparedLink';
import TagsTitle from '../TagsTitle';
import { checkBlank } from '../../helpers';
import parser from 'bbcode-to-react';
import LinkGroupType from 'components/LinkGroupType';
import {
  Post1SubTitle,
  Post2SubTitle,
  Post3SubTitle,
  PostGroupItem,
  Post1Title,
  Post2Title,
  Post3Title,
  Post1Text,
  Post2Text,
  Post3Text,
  linkStyle,
  PostAfter,
  Post2,
  Post3,
  Post1,
} from './style';

const Post = ({
  masonryMobileLayout = {},
  isSpecProjectPage,
  articlePageRow,
  isMasonryPage,
  setDragging,
  isMainPage,
  style = {},
  imageMobile,
  dragging,
  image,
  data,
}) => {
  const isDesktop = useDesktop();
  const { linkParentWindowForPost: postBlank = true, url, block: size } = data;
  const blankValue = postBlank ? '_blank' : '_self';

  const imgSize = isDesktop ? data.image : data.imageMobile;

  const post1 = (
    <Post1
      backgroundColor={_get(data, 'postFormat.backgroundColor')}
      isSpecProjectPage={isSpecProjectPage}
      announceType={data.type_announce_1_1}
      customstyle={style.container}
    >
      <TagsTitle
        announceType={data.type_announce_1_1}
        isSpecProjectPage={isSpecProjectPage}
        isMainPage={isMainPage}
        bgi={data.image}
        data={data}
        post1
      />

      <PreparedLink
        target={blankValue}
        url={url && url[0] === '/' ? url : `/${url}`}
        style={linkStyle}
      />
      {data.image && !isSpecProjectPage ? <img src={imgSize} alt="" /> : null}
      {isSpecProjectPage ? <img src={image} alt="" /> : null}

      <Post1Text href={url} customstyle={style.textWrap}>
        <Post1Title
          fontColor={_get(data, 'postFormat.fontColor')}
          announceType={data.type_announce_1_1}
          isSpecProjectPage={isSpecProjectPage}
          customstyle={style.textTitle}
          bgi={data.image}
        >
          {clearText(data.name)}
        </Post1Title>
        {data.header ? (
          <Post1SubTitle
            fontColor={_get(data, 'postFormat.fontColor')}
            announceType={data.type_announce_1_1}
            isSpecProjectPage={isSpecProjectPage}
            customstyle={style.textSubTitle}
            bgi={data.image}
          >
            {parser.toReact(clearText(data.header))}
          </Post1SubTitle>
        ) : null}
      </Post1Text>
      {(data.type_announce_1_1 === 'picture' || isSpecProjectPage) && (
        <PostAfter />
      )}
    </Post1>
  );
  const post2 = (
    <Post2
      backgroundColor={_get(data, 'postFormat.backgroundColor')}
      announceType={data.type_announce_1_2}
      customstyle={style.container}
      className="post2"
    >
      <TagsTitle
        isSpecProjectPage={isSpecProjectPage}
        announceType={data.type_announce_1_2}
        bgi={data.image}
        data={data}
      />
      <PreparedLink
        url={url && url[0] === '/' ? url : `/${url}`}
        target={blankValue}
        style={linkStyle}
      />
      {data.type_announce_1_2 === 'picture' ? (
        <img src={imgSize} alt="" />
      ) : null}
      <Post2Text href={url}>
        <Post2Title
          fontColor={_get(data, 'postFormat.fontColor')}
          announceType={data.type_announce_1_2}
          customstyle={style.textTitle}
          bgi={data.image}
        >
          {clearText(data.name)}
        </Post2Title>
        {data.header ? (
          <Post2SubTitle
            fontColor={_get(data, 'postFormat.fontColor')}
            announceType={data.type_announce_1_2}
            customstyle={style.textSubTitle}
            bgi={data.image}
          >
            {clearText(data.header)}
          </Post2SubTitle>
        ) : null}
      </Post2Text>
      {data.type_announce_1_2 === 'picture' && <PostAfter />}
    </Post2>
  );

  const post3 = (
    <Post3
      backgroundColor={_get(data, 'postFormat.backgroundColor')}
      masonryMobileLayout={isDesktop ? {} : masonryMobileLayout}
      isSpecProjectPage={isSpecProjectPage}
      announceType={data.type_announce_1_3}
      customstyle={style.container}
      isMasonryPage={isMasonryPage}
      className="post3"
    >
      <TagsTitle
        isSpecProjectPage={isSpecProjectPage}
        announceType={data.type_announce_1_3}
        bgi={data.image}
        data={data}
        block3
      />
      {
        <PreparedLink
          url={url && url[0] === '/' ? url : `/${url}`}
          articlePageRow={articlePageRow}
          target={blankValue}
          style={linkStyle}
        />
      }
      {data.type_announce_1_3 === 'picture' && !isSpecProjectPage ? (
        <img src={data.image} alt="" />
      ) : null}
      {isSpecProjectPage ? <img src={imageMobile} alt="" /> : null}
      <Post3Text href={url} isSpecProjectPage={isSpecProjectPage}>
        <Post3Title
          fontColor={_get(data, 'postFormat.fontColor')}
          announceType={data.type_announce_1_3}
          isSpecProjectPage={isSpecProjectPage}
          customstyle={style.textTitle}
          bgi={data.image}
        >
          {clearText(data.name)}
        </Post3Title>
        {data.header ? (
          <Post3SubTitle
            fontColor={_get(data, 'postFormat.fontColor')}
            isSpecProjectPage={isSpecProjectPage}
            announceType={data.type_announce_1_3}
            customstyle={style.textSubTitle}
            bgi={data.image}
          >
            {parser.toReact(clearText(data.header))}
            {/* {clearText(data.header)} */}
          </Post3SubTitle>
        ) : null}
      </Post3Text>
      {(data.type_announce_1_3 === 'picture' || isSpecProjectPage) && (
        <PostAfter />
      )}
    </Post3>
  );

  if (articlePageRow) {
    return post3;
  }

  switch (size) {
    case '1_1': {
      return post1;
    }
    case '1_2': {
      return post2;
    }
    case '1_3': {
      return post3;
    }
    default: {
      return (
        <PostGroupItem
          backgroundColor={_get(data, 'postFormat.backgroundColor')}
          announceType={data.type_announce_1_3}
        >
          <TagsTitle
            announceType={data.type_announce_1_3}
            setDragging={setDragging}
            dragging={dragging}
            bgi={data.image}
            data={data}
            isGroup
          />
          <LinkGroupType
            checkBlank={checkBlank}
            setDragging={setDragging}
            dragging={dragging}
            url={url}
          />
          {data.type_announce_1_3 === 'picture' ? (
            <img src={data.image} alt="" />
          ) : null}

          <Post3Text href={url}>
            <Post3Title
              fontColor={_get(data, 'postFormat.fontColor')}
              announceType={data.type_announce_1_3}
              bgi={data.image}
            >
              {clearText(data.name)}
            </Post3Title>
            {data.header ? (
              <Post3SubTitle
                fontColor={_get(data, 'postFormat.fontColor')}
                announceType={data.type_announce_1_3}
                bgi={data.image}
              >
                {parser.toReact(clearText(data.header))}
              </Post3SubTitle>
            ) : null}
          </Post3Text>
          {data.type_announce_1_3 === 'picture' && <PostAfter />}
        </PostGroupItem>
      );
    }
  }
};

export default Post;
