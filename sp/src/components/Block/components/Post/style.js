import styled from 'styled-components/macro';
import {
  GridItem1,
  GridItem2,
  GridItem3,
  GridItemGroup,
} from 'components/Grid/Grid';

const PostAfter = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background-image: linear-gradient(
    to bottom,
    rgba(0, 0, 0, 0.22),
    rgba(0, 0, 0, 0.41) 48%,
    rgba(0, 0, 0, 0.8)
  );
  z-index: 5;
  border-radius: 5px;
`;

const Post1 = styled(GridItem1)`
  display: flex;
  flex-direction: column;
  margin-bottom: 20px;
  
  img {
    z-index: auto;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    display: block;
    width: 100%;
    height: 100%;
    border-radius: 5px;
    object-fit: cover;
  }

  ${({ customstyle }) => customstyle}
  background-color: ${({ backgroundColor, announceType }) =>
    announceType === 'picture' ? 'transparent' : backgroundColor};
  @media (max-width: 767px) {
    justify-content: space-between;
    padding: 20px;
    min-height: 410px;
  }
`;

const Post1Text = styled.a`
  min-height: initial;
  text-align: center;
  z-index: 6;
  position: relative;
  margin-bottom: 20px;
  order: 1;
  margin: auto;
  text-decoration: none;
  ${({ customstyle }) => customstyle}
  @media (max-width: 767px) {
    margin: 0;
    text-align: left;
  }
`;

const Post1Title = styled.div`
  font-family: 'CSTMXprmntl02-Bold', sans-serif;
  font-size: 66px;
  line-height: 1;
  word-break: break-word;
  color: ${({ fontColor, announceType, isSpecProjectPage }) =>
    announceType === 'picture' || isSpecProjectPage ? '#fff' : fontColor};

  ${({ customstyle }) => customstyle}
  @media (max-width: 767px) {
    font-size: 20px;
    font-family: 'MullerBold';
    line-height: 1.3;
  }
`;

const Post1SubTitle = styled.div`
  margin-top: 20px;
  font-size: 24px;
  font-family: 'MullerRegular';
  display: flex;
  justify-content: center;
  /* justify-content: ${({ isSpecProjectPage }) =>
    isSpecProjectPage ? 'flex-start' : 'center'}; */
  line-height: 1.33;
  color: ${({ fontColor, announceType, isSpecProjectPage }) =>
    announceType === 'picture' || isSpecProjectPage ? '#fff' : fontColor};
  ${({ customstyle }) => customstyle}

  @media (max-width: 767px) {
    font-size: 15px;
    display: block;
  }
`;
// Post2 -----------------------------------------------

const Post2 = styled(GridItem2)`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  img {
    z-index: auto;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    display: block;
    width: 100%;
    height: 100%;
    border-radius: 5px;
    object-fit: cover;
  }
  background-color: ${({ backgroundColor, announceType }) =>
    announceType === 'picture' ? 'transparent' : backgroundColor};
  ${({ customstyle }) => customstyle}
  @media (max-width: 767px) {
    padding: 20px;
    min-height: 410px;
  }
`;

const Post2Text = styled.a`
  max-width: 400px;
  min-height: initial;
  text-align: left;
  z-index: 6;
  position: relative;
  text-decoration: none;
`;

const Post2Title = styled.div`
  font-size: 30px;
  line-height: 1.2;
  font-family: 'MullerBold', sans-serif;
  color: ${({ fontColor, announceType, isSpecProjectPage }) =>
    announceType === 'picture' || isSpecProjectPage ? '#fff' : fontColor};
  @media (max-width: 767px) {
    line-height: 1.3;
    font-size: 20px;
    font-family: 'MullerBold';
  }
`;

const Post2SubTitle = styled.div`
  margin-top: 20px;
  font-family: 'MullerRegular', sans-serif;
  font-size: 19px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.26;
  letter-spacing: normal;
  text-align: left;
  color: ${({ fontColor, announceType, isSpecProjectPage }) =>
    announceType === 'picture' || isSpecProjectPage ? '#fff' : fontColor};
  @media (max-width: 767px) {
    line-height: 1.33;
    font-size: 15px;
  }
`;

//  Post3 ---------------------------------------

const Post3 = styled(GridItem3)`
  display: flex;
  flex-direction: column;

  img {
    z-index: auto;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    display: block;
    width: 100%;
    height: 100%;
    border-radius: 5px;
    object-fit: cover;
  }

  background-color: ${({ backgroundColor = '#eaeaea', announceType }) =>
    announceType === 'picture' ? 'transparent' : backgroundColor};
  ${({ customstyle }) => customstyle}
  justify-content: space-between;
  min-height: ${({ announceType, isMasonryPage }) =>
    announceType === 'text' && isMasonryPage && 'auto'};

  @media (max-width: 767px) {
    padding: 20px;
    min-height: 410px;
    ${({ masonryMobileLayout }) => masonryMobileLayout};
  }
`;

const Post3Text = styled.a`
  min-height: initial;
  text-align: left;
  z-index: 6;
  position: relative;
  text-decoration: none;
`;

const Post3Title = styled.div`
  font-size: 30px;
  line-height: 1.2;
  font-family: 'MullerBold', sans-serif;
  word-break: break-word;
  color: ${({ fontColor = '#000', announceType, isSpecProjectPage }) =>
    announceType === 'picture' || isSpecProjectPage ? '#fff' : fontColor};
  @media (max-width: 767px) {
    font-size: 20px;
    font-family: 'MullerBold';
    line-height: 1.3;
  }
`;

const Post3SubTitle = styled.div`
  /* display: flex; */
  margin-top: 20px;
  /* justify-content: center; */
  font-size: 19px;
  font-family: 'MullerRegular';
  line-height: 1.26;
  color: ${({ fontColor = '#000', announceType, isSpecProjectPage }) =>
    announceType === 'picture' || isSpecProjectPage ? '#fff' : fontColor};
  ${({ customstyle }) => customstyle}
  @media (max-width: 767px) {
    font-size: 15px;
    line-height: 1.33;
  }
`;

const PostGroupItem = styled(GridItemGroup)`
  display: flex;
  flex-direction: column;
   background-color: ${({ backgroundColor = '#eaeaea', announceType }) =>
     announceType === 'picture' ? 'transparent' : backgroundColor};
  img {
    z-index: auto;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    display: block;
    width: 100%;
    height: 100%;
    border-radius: 5px;
    object-fit: cover;
  }
  ${Post3Title},${Post3SubTitle}{
    color: ${({ fontColor, announceType, isSpecProjectPage }) =>
      announceType === 'picture' || isSpecProjectPage ? '#fff' : fontColor};
  }
`;

const linkStyle = {
  zIndex: '10',
  position: 'absolute',
  top: '0',
  left: '0',
  right: '0',
  bottom: '0',
  display: 'block',
  cursor: 'pointer',
};

export {
  PostAfter,
  Post1,
  Post1Text,
  Post1Title,
  Post1SubTitle,
  Post2,
  Post2Text,
  Post2Title,
  Post2SubTitle,
  Post3,
  Post3Text,
  Post3Title,
  Post3SubTitle,
  PostGroupItem,
  linkStyle,
};
