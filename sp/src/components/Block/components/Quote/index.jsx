import React from 'react';
import { useHistory } from 'react-router-dom';
import useDesktop from 'hooks/useDesktop';
import { clearText } from 'clearText';
import { AWrap } from 'components/Grid/Grid';
import { checkBlank } from '../../helpers';

import { Quote1, Quote2, Quote3, Quote3Group, H2, Header } from './style';

const Quote = ({
  data,
  style = {},
  masonryMobileLayout = {},
  dragging,
  setDragging,
  articlePageRow,
}) => {
  const isDesktop = useDesktop();
  const { container, textTitle } = style;
  const { linkParentWindowForPost: postBlank, url, block: size } = data;
  const blankValue = postBlank ? '_blank' : '_self';
  const history = useHistory();

  const quote1 = (
    <Quote1
      bgc={data.postFormat.backgroundColor}
      fontColor={data.postFormat.fontColor}
      customStyleH2={textTitle}
      customstyle={container}
      size={size}
    >
      <AWrap target={blankValue} to={url[0] === '/' ? url : `/${url}`}></AWrap>
      <H2>{clearText(data.name)}</H2>
      <Header fontColor={data.postFormat.fontColor}>
        {clearText(data.header)}
      </Header>
    </Quote1>
  );
  const quote2 = (
    <Quote2
      className="post2"
      size={size}
      customstyle={container}
      customStyleH2={textTitle}
      bgc={data.postFormat.backgroundColor}
      fontColor={data.postFormat.fontColor}
    >
      <AWrap target={blankValue} to={url[0] === '/' ? url : `/${url}`}></AWrap>
      <H2 fontColor={data.postFormat.fontColor}>{clearText(data.name)}</H2>
      <Header fontColor={data.postFormat.fontColor}>
        {clearText(data.header)}
      </Header>
    </Quote2>
  );
  const quote3 = (
    <Quote3
      className="post3"
      size={size}
      customstyle={container}
      customStyleH2={textTitle}
      masonryMobileLayout={isDesktop ? {} : masonryMobileLayout}
      bgc={data.postFormat.backgroundColor}
      fontColor={data.postFormat.fontColor}
      articlePageRow={articlePageRow}
    >
      <AWrap target={blankValue} to={url[0] === '/' ? url : `/${url}`}></AWrap>
      <H2 fontColor={data.postFormat.fontColor}>{clearText(data.name)}</H2>
      <Header fontColor={data.postFormat.fontColor}>
        {clearText(data.header)}
      </Header>
    </Quote3>
  );
  if (articlePageRow) {
    return quote3;
  }
  switch (size) {
    case '1_1':
      return quote1;
    case '1_2':
      return quote2;
    case '1_3':
      return quote3;
    default:
      return (
        <Quote3Group size={size} bgc={data.postFormat.backgroundColor}>
          <AWrap
            onDragStart={e => {
              e.preventDefault();
              setDragging(true);
            }}
            onClick={e => {
              e.preventDefault();
              setDragging(false);
              if (!dragging) {
                checkBlank(blankValue, url, history);
              }
            }}
            draggable={false}
            to=""
          />
          <H2 fontColor={data.postFormat.fontColor}>{clearText(data.name)}</H2>
          <Header fontColor={data.postFormat.fontColor}>
            {clearText(data.header)}
          </Header>
        </Quote3Group>
      );
  }
};

export default Quote;
