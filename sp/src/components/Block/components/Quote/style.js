import {
  GridItem1,
  GridItem2,
  GridItem3,
  GridItemGroup,
} from 'components/Grid/Grid';
import styled from 'styled-components/macro';

const H2 = styled.h2`
  color: #000;
`;
const Quote1 = styled(GridItem1)`
  width: 100%;
  min-height: auto;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 20px 30px;
  background-color: #fff;
  ${({ customstyle }) => customstyle}

  ${H2} {
    font-size: 28px;
    line-height: 1.21;
    font-family: 'Krok', sans-serif;
    text-align: left;
    ${({ customStyleH2 }) => customStyleH2}
  }
  div {
    font-family: 'MullerRegular', sans-serif;
    font-size: 19px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.26;
    letter-spacing: normal;
    text-align: left;
    @media (max-width: 767px) {
      font-size: 15px;
      line-height: 1.33;
      letter-spacing: normal;
      text-align: left;
      color: #070707;
    }
  }
  @media (max-width: 767px) {
    padding: 20px;
    ${H2} {
      font-size: 23px;
      line-height: 1.26;
    }
  }
`;
const Quote2 = styled(GridItem2)`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 20px 30px;
  background-color: #fff;
  /* background-color: #eaeaea; */

  ${({ customstyle }) => customstyle}
  ${H2} {
    font-size: 34px;
    line-height: 1.12;
    font-family: 'Krok', sans-serif;
    text-align: left;
    ${({ customStyleH2 }) => customStyleH2}
  }
  div {
    font-family: 'MullerRegular', sans-serif;
    font-size: 19px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.26;
    letter-spacing: normal;
    text-align: left;
    color: #000000;
    @media (max-width: 767px) {
      font-size: 15px;
      line-height: 1.33;
      letter-spacing: normal;
      text-align: left;
      color: #070707;
    }
  }
  @media (max-width: 767px) {
    padding: 20px;
    min-height: auto;
    ${H2} {
      font-size: 23px;
      line-height: 1.26;
    }
  }
`;

const Quote3 = styled(GridItem3)`
  width: 100%;
  display: flex;

  flex-direction: column;
  justify-content: space-between;
  background-color: #fff;
  ${({ customstyle }) => customstyle}
  ${H2} {
    text-align: left;
    font-size: 27px;
    font-family: 'Krok', sans-serif;
    text-align: left;
    ${({ customStyleH2 }) => customStyleH2}
    color: #000;
    margin-bottom: 45px;
    line-height: 1.07;
  }
  ${({ articlePageRow }) => articlePageRow && { backgroundColor: '#fff' }}
  div {
    font-family: 'MullerRegular', sans-serif;
    font-size: 19px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.26;
    letter-spacing: normal;
    text-align: left;
    color: #000;
    @media (max-width: 767px) {
      font-size: 15px;
      line-height: 1.33;
      letter-spacing: normal;
      text-align: left;
      color: #070707;
    }
  }
  @media (max-width: 767px) {
    padding: 20px;
    min-height: auto;
    ${({ masonryMobileLayout }) => masonryMobileLayout};
    ${H2} {
      font-size: 23px;
      line-height: 1.26;
    }
  }
`;

const Quote3Group = styled(GridItemGroup)`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  background-color: #fff;

  ${H2} {
    font-size: 27px;
    font-family: 'Krok', sans-serif;
    text-align: left;
    font-weight: normal;
    ${({ customStyleH2 }) => customStyleH2}
    color: #000;
    margin-bottom: 45px;
    line-height: 1.07;
  }
  div {
    font-family: 'MullerRegular', sans-serif;
    font-size: 19px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.26;
    letter-spacing: normal;
    text-align: left;
    color: #000;
    @media (max-width: 767px) {
      font-size: 15px;
      line-height: 1.33;
      letter-spacing: normal;
      text-align: left;
      color: #070707;
    }
  }
`;

const Header = styled.div`
  /* color: ${({ fontColor = '#eaeaea' }) => fontColor}; */
  color: #000;
`;

export { Quote1, Quote2, Quote3, Quote3Group, H2, Header };
