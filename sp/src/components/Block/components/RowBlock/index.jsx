import React from 'react';
import _uniqueId from 'lodash/uniqueId';

import Block from 'components/Block';
import { Container } from 'components/Grid/Grid';
import LongRow from 'components/LongRow';
import Group from 'components/Block/components/Group';
import Banner from 'components/Banner';

const RowBlock = ({
  content = [{}],
  rowType,
  isMainPage,
  groupName,
  articlePageRow,
}) => {
  const exeptions = ['group', 'ticker', 'banner'];

  return (
    <Container isMainPage={isMainPage}>
      {rowType && rowType === 'group' && (
        <Group
          data={content[0].postList}
          groupName={groupName}
          url={content[0].url}
        />
      )}

      {rowType && rowType === 'ticker' && (
        <LongRow data={content && content.length && content[0]} />
      )}
      {rowType && rowType === 'banner' && (
        <Banner
          p2Desctop="ghgl"
          p2Mobile="ghgm"
          bannerStyle={{ marginBottom: '20px' }}
        />
      )}

      {content &&
        !exeptions.includes(rowType) &&
        content.map(({ id, typePost, ...other }, i) => (
          <Block
            key={id ? id : i}
            type={typePost}
            data={other}
            isMainPage={isMainPage}
            articlePageRow={articlePageRow}
          />
        ))}
    </Container>
  );
};

export default RowBlock;
