import React, { useEffect, useState } from 'react';
import closeImg from 'img/closeCross.svg';
import {
  SubscribeConfirmMessageWrap,
  SubscribeConfirmMessageCloseWrap,
} from './style';
import _get from 'lodash/get';
import { useParams } from 'react-router-dom';
import { Subscribe } from 'connectors/query/Subscribe';

const SubscribeConfirmMessage = () => {
  const { hashConfirmSubscribe } = useParams();
  const [isShow, setIsShow] = useState(false);

  const closeMessageDelay = () => {
    setTimeout(() => setIsShow(false), 5000);
  };

  useEffect(() => {
    async function confirmSubscribe() {
      const dataPage = await Subscribe.confirm(hashConfirmSubscribe);

      const confirmSubscribe = _get(dataPage, 'confirmSubscribe', []);

      if (confirmSubscribe) {
        setIsShow(true);
      }
    }

    confirmSubscribe();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [hashConfirmSubscribe]);

  useEffect(() => {
    closeMessageDelay();
  }, []);

  return (
    <>
      {isShow ? (
        <SubscribeConfirmMessageWrap isShow={isShow}>
          <SubscribeConfirmMessageCloseWrap
            className="сonfirmSubscribe"
            onClick={() => setIsShow(false)}
          >
            <img src={closeImg} />
          </SubscribeConfirmMessageCloseWrap>
          Вы подписались на рассылку +1
        </SubscribeConfirmMessageWrap>
      ) : null}
    </>
  );
};

export default SubscribeConfirmMessage;
