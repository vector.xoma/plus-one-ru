import styled, { keyframes } from 'styled-components/macro';

const SubscribeConfirmMessageWrap = styled.div`
  right: 25px;
  top: ${({ isShow }) => (isShow ? '43px' : '-100%')};
  position: fixed;
  min-width: 300px;
  box-sizing: border-box;
  z-index: 50;
  padding: 31px 27px;
  background-color: #fff;
  font-size: 15px;
  line-height: 1.2;
  letter-spacing: normal;
  text-align: left;
  color: #000000;
  font-family: 'MullerRegular', sans-serif;
  transition: all ease 0.3s;
`;

const SubscribeConfirmMessageCloseWrap = styled.div`
  right: 10px;
  top: 10px;
  position: absolute;
  cursor: pointer;
`;

export { SubscribeConfirmMessageWrap, SubscribeConfirmMessageCloseWrap };
