import React from 'react';
import {
  SubscribeContainer,
  SubscribeEmailTitle,
  FormContaier,
  InformMessageContainer,
  Input,
  Button,
} from './style';

const SubscribFormContainer = ({
  placeholder,
  onFocus,
  onBlur,
  onChanghe,
  onSubmit,
  value,
  mailValid,
  informMessage,
  onClick,
  title,
}) => {
  return !informMessage ? (
    <SubscribeContainer>
      <SubscribeEmailTitle>{`${
        mailValid ? title : 'Ошибка! Введите правильный email'
      }`}</SubscribeEmailTitle>

      <FormContaier>
        <Input
          placeholder={placeholder}
          onFocus={onFocus}
          onBlur={onBlur}
          onChange={onChanghe}
          value={value}
        />
        <Button onClick={onSubmit}>Подписаться</Button>
      </FormContaier>
    </SubscribeContainer>
  ) : (
    <InformMessageContainer>
      <h2>
        На вашу почту отправлено письмо со ссылкой для подтверждения подписки
      </h2>
      <Button onClick={onClick}>Ok</Button>
    </InformMessageContainer>
  );
};

export default SubscribFormContainer;
