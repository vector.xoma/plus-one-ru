import React from 'react';
import { Button } from 'antd';

const SubscribeButton = ({ setSubscribeForm }) => {
  return <Button onClick={setSubscribeForm}>Форма подписки</Button>;
};

export default SubscribeButton;
