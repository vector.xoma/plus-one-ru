import React, { useState, useEffect } from 'react';
import { Subscribe } from 'connectors/query/Subscribe';
import SubscribeFormContainer from './SubscribeFormContainer/';
import { SubscribeWrap } from './style';

// возвращает куки с указанным name,
// или undefined, если ничего не найдено
function getCookie(name) {
  let matches = document.cookie.match(
    new RegExp(
      '(?:^|; )' + name.replace(/([.$?*|{}()[\]\\/+^])/g, '\\$1') + '=([^;]*)',
    ),
  );
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

const SubscribeForm = ({ mainPage }) => {
  const regMailValidator = /^([A-Za-z0-9_\-.])+@([A-Za-z0-9_\-.])+\.([A-Za-z]{2,4})$/;
  const [isDesktop, setIsDesktop] = useState(window.outerWidth >= 767);

  const oldPhVisible = isDesktop ? 'Подпишитесь на рассылку +1' : 'Ваш Email';
  const title = !isDesktop ? 'Подпишитесь на рассылку +1' : 'Ваш Email';
  const [informMessage, setInformMessage] = useState(false);
  const [phVisible, setPhVisible] = useState(oldPhVisible);
  const [closeMessage, setCloseMessage] = useState(false);
  const [isSubscribe, setIsSubscribe] = useState(false);
  const [mailValid, setMailValid] = useState(true);
  const [animate, setAnimate] = useState(false);
  const [mail, setMail] = useState('');

  const placehoalderUnVisible = () => setPhVisible('');
  const placeholderVisible = () => setPhVisible(oldPhVisible);
  const closeInformMessage = () => setCloseMessage(true);

  useEffect(() => {
    const resizeHandler = () => setIsDesktop(window.outerWidth >= 767);
    window.addEventListener('resize', resizeHandler);
    setIsSubscribe(getCookie('subscribe'));
    return () => window.removeEventListener('resize', resizeHandler);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    setPhVisible(oldPhVisible);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isDesktop]);

  const validEmail = event => {
    const currentMail = event.target.value.trim();
    setMail(currentMail);
    if (currentMail.match(regMailValidator)) {
      setMailValid(true);
    }
  };

  const onSubmitForm = async event => {
    event.preventDefault();
    if (!mail.match(regMailValidator)) {
      setMailValid(false);
      setAnimate(true);
      setTimeout(() => setAnimate(false), 1000);
      return;
    }
    // отправляем почту, если всё ок, то устанавливаем флаг в куки
    // далее по флагу показываем или нет форму подписки
    await Subscribe.postMail(mail).then(() => {
      document.cookie = 'subscribe=true';
      setMail('');
      setInformMessage(true);
    });
  };
  return !closeMessage && !isSubscribe ? (
    <SubscribeWrap mailValid={mailValid} mainPage={mainPage} animate={animate}>
      <SubscribeFormContainer
        onFocus={placehoalderUnVisible}
        informMessage={informMessage}
        onClick={closeInformMessage}
        onBlur={placeholderVisible}
        onSubmit={onSubmitForm}
        placeholder={phVisible}
        onChanghe={validEmail}
        mailValid={mailValid}
        title={title}
        value={mail}
      />
    </SubscribeWrap>
  ) : null;
};

export default SubscribeForm;
