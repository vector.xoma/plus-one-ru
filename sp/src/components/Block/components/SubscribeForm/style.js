import styled, { keyframes } from 'styled-components/macro';

const shake = keyframes`
  from,
  to {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }

  10%,
  30%,
  50%,
  70%,
  90% {
    -webkit-transform: translate3d(-10px, 0, 0);
    transform: translate3d(-10px, 0, 0);
  }

  20%,
  40%,
  60%,
  80% {
    -webkit-transform: translate3d(10px, 0, 0);
    transform: translate3d(10px, 0, 0);
  }
`;

const SubscribeWrap = styled.div`
  max-width: 1140px;
  width: 100%;
  height: 1px;
  min-height: 210px;
  padding: 42px 108px 26px;
  background-color: ${({ mainPage }) => (mainPage ? '#eaeaea' : '#fff')};
  border-radius: 5px;
  margin: 0 auto;
  margin-bottom: 20px;
  animation-name: ${({ animate }) => {
    return animate ? shake : 'none';
  }};
  animation-duration: 1s;
  animation-iteration-count: 1;
  @media (max-width: 767px) {
    height: 1px;
    min-height: 410px;
    padding: 18px 20px 30px;
  }
`;

export { SubscribeWrap };
