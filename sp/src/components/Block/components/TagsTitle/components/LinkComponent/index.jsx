import React from 'react';
import { Link } from 'react-router-dom';
import { checkBlank } from 'components/Block/helpers';
import { useHistory } from 'react-router-dom';

const LinkOutSideSlide = ({ tagUrl, children, isSpecProjectPage }) => {
  const tagLinkUrl = tagUrl && tagUrl.includes('http') && new URL(tagUrl);

  const linkBlank = (
    <a href={tagUrl} target={'_blank'}>
      {children}
    </a>
  );
  const linkSelf = (
    <Link to={`${!tagLinkUrl ? tagUrl : tagLinkUrl.pathname}`}>{children}</Link>
  );
  const specProjectMark = <>{children}</>;
  if (isSpecProjectPage) {
    return specProjectMark;
  }
  return tagUrl &&
    tagUrl.includes('http') &&
    !tagUrl.includes('https://plus-one.ru')
    ? linkBlank
    : linkSelf;
};

const LinkComponent = ({
  tagBlank,
  tagUrl,
  isGroup = false,
  dragging,
  setDragging,
  specProject,
  isSpecProjectPage,
  ...props
}) => {
  const history = useHistory();
  const blankValue = tagBlank ? '_blank' : '_self';
  return isGroup ? (
    <div
      draggable
      onDragStart={e => {
        e.preventDefault();
        setDragging(true);
      }}
      onClick={() => {
        !dragging && checkBlank(blankValue, tagUrl, history);
        setDragging(false);
      }}
      style={{ cursor: 'pointer' }}
    >
      {props.children}
    </div>
  ) : (
    <LinkOutSideSlide
      tagBlank={tagBlank}
      tagUrl={tagUrl}
      children={props.children}
      specProject={specProject}
      isSpecProjectPage={isSpecProjectPage}
    />
  );
};

export default LinkComponent;
