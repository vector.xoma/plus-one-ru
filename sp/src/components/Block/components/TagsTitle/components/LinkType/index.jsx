import React from 'react';
import { Link } from 'react-router-dom';
import _get from 'lodash/get';
import { PartnerLink, PartnerRoute } from '../../style';
import useDesktop from 'hooks/useDesktop';

const LinkType = ({
  isSpecProjectPage = false,
  announceType,
  block3,
  data: {
    postFormat = {},
    is_partner_material: isPartnerMaterial,
    partnerName = '',
    partnerUrl,
    partner_url,
    block,
    url,
  } = {},
}) => {
  const isDesktop = useDesktop();
  const fontColor = _get(postFormat, 'fontColor', '');
  const linkColor =
    announceType === 'picture' || isSpecProjectPage ? '#fff' : fontColor;
  const partnerText =
    block === '1_3' || !isDesktop || block3
      ? 'Партнерский'
      : 'Партнерский материал';

  const partnerRoute = text => (
    <PartnerRoute
      isPartnerMaterial={isPartnerMaterial}
      isSpecProjectPage={isSpecProjectPage}
      announceType={announceType}
      isPartnerName={partnerName}
      bordercolor={fontColor}
      fontColor={fontColor}
    >
      <Link
        to={url}
        style={{
          color: linkColor,
        }}
      >
        {text}
      </Link>
    </PartnerRoute>
  );

  const linkType =
    partnerUrl || partner_url ? (
      <PartnerLink
        isSpecProjectPage={isSpecProjectPage}
        isPartnerName={partnerName}
        announceType={announceType}
        bordercolor={fontColor}
        fontColor={fontColor}
        href={partnerUrl || partner_url}
        target="_blank"
      >
        {partnerName}
      </PartnerLink>
    ) : (
      partnerRoute(partnerName)
    );
  return isPartnerMaterial === '1' ? partnerRoute(partnerText) : linkType;
};

export default LinkType;
