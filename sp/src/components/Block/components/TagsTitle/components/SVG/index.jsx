import React, { useEffect, useState } from 'react';

const SVG = ({
  backgroundColor = '#eaeaea',
  fontColor = '#000',
  announceType,
  iconCode,
  typePost,
}) => {
  const [fontFill, setFontFill] = useState();
  const [backgroundFill, setBackgroundFill] = useState();
  useEffect(() => {
    const checkImg = () => {
      if (announceType === 'picture' && typePost !== 'citate') {
        setBackgroundFill('#fff');
        setFontFill('#000');
        return;
      } else if (typePost === 'citate' && !fontColor && !backgroundColor) {
        setBackgroundFill('#000');
        setFontFill('#eaeaea');
        return;
      } else {
        setBackgroundFill(fontColor);
        setFontFill(backgroundColor);
        return;
      }
    };
    checkImg();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const icon = {
    ecology: (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 40 40"
        width="100%"
        height="100%"
      >
        <g id="Layer_2" data-name="Layer 2">
          <g id="background">
            <path
              fill={backgroundFill}
              className="backgroundFill"
              d="M40,20A20,20,0,1,1,20,0,20,20,0,0,1,40,20Z"
            />
            <path
              fill={fontFill}
              className="fontFill"
              d="M26.91,13.82c-2.38,0-5.29,2.58-6.88,4.17-1.58-1.59-4.49-4.17-6.89-4.17a6.17,6.17,0,0,0,0,12.34c2.4,0,5.31-2.57,6.89-4.17,1.59,1.6,4.5,4.17,6.88,4.17a6.17,6.17,0,1,0,0-12.34ZM13.14,23.41a3.42,3.42,0,0,1,0-6.83c2,0,3.4,1.8,5,3.42C16.54,21.61,15.09,23.41,13.14,23.41Zm13.77,0c-1.89,0-3.38-1.8-5-3.41,1.61-1.62,3.33-3.42,5-3.42a3.42,3.42,0,1,1,0,6.83Z"
            />
          </g>
        </g>
      </svg>
    ),
    society: (
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
        <g id="Layer_2" data-name="Layer 2">
          <g id="background">
            <path
              fill={backgroundFill}
              className="backgroundFill"
              d="M40,20A20,20,0,1,1,20,0,20,20,0,0,1,40,20Z"
            />
            <path
              fill={fontFill}
              className="fontFill"
              d="M23.67,15.91c0,1.2-1.88,3.34-3.56,4.85L20,20.68l-.08.08c-1.68-1.5-3.56-3.65-3.56-4.85a3.64,3.64,0,0,1,7.27,0Zm4.12,12.54L22,22.7c1.68-1.52,4.35-4.31,4.35-6.79a6.36,6.36,0,1,0-12.71,0c0,2.48,2.67,5.27,4.34,6.79l-5.81,5.81,1.93,1.93,5.9-5.9,5.83,5.83Z"
            />
          </g>
        </g>
      </svg>
    ),
    platform: (
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30">
        <g id="Layer_2" data-name="Layer 2">
          <g id="background">
            <circle cx="15" cy="15" r="15" />
            <circle
              cx="15"
              cy="15"
              r="15"
              fill={backgroundFill}
              className="backgroundFill"
            />
            <polygon
              fill={fontFill}
              className="fontFill"
              points="22.33 13.63 16.1 13.63 16.1 7.4 13.9 7.4 13.9 13.63 7.67 13.63 7.67 15.83 13.9 15.83 13.9 22.06 16.1 22.06 16.1 15.83 22.33 15.83 22.33 13.63"
            />
          </g>
        </g>
      </svg>
    ),
    economy: (
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30">
        <g id="Layer_2" data-name="Layer 2">
          <g id="background">
            <circle cx="15" cy="15" r="15" />
            <polygon points="15.89 20.29 10.61 15 6.95 18.66 5.49 17.2 10.61 12.08 15.89 17.37 23.25 10.01 24.71 11.47 15.89 20.29" />
            <circle
              cx="15"
              cy="15"
              r="15"
              fill={backgroundFill}
              className="backgroundFill"
            />
            <polygon
              fill={fontFill}
              className="fontFill"
              points="15.89 20.29 10.61 15 6.95 18.66 5.49 17.2 10.61 12.08 15.89 17.37 23.25 10.01 24.71 11.47 15.89 20.29"
            />
          </g>
        </g>
      </svg>
    ),
  };
  return <>{icon[iconCode]}</>;
};

export default SVG;
