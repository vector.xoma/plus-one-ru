import React from 'react';
import SVG from './components/SVG';
import LinkComponent from './components/LinkComponent';
import { Link } from 'react-router-dom';
import _get from 'lodash/get';
import LinkType from './components/LinkType';
import { TagsWrap, TagLeft, ImgWrap, TagName, TagRight, Img } from './style';

const TagsTitle = ({
  defaultBackgroundFillCitate,
  defaultFontFillCitate,
  isSpecProjectPage,
  announceType,
  setDragging,
  isMainPage,
  typePost,
  dragging,
  post1,
  isGroup,
  data,
  block3,
}) => {
  const {
    linkParentWindowForTag: tagBlank,
    iconName = '',
    iconCode = '',
    tagUrl,
  } = data;
  const iconType = ['ecology', 'economy', 'society', 'platform'];
  const matchIcon = iconType.includes(iconCode);
  //Для переноса "СПЕЦПРОЕКТ" в метке на следущую строку
  const isSpecprojectTitle = iconCode && iconName.search(/^спецпроект ./gimu);
  const specProjectDescr =
    iconCode && iconName.replace(/^спецпроект/gimu, '').trim();
  //-------------------------------------------------------
  return (
    <TagsWrap
      isMainPage={isMainPage}
      post1={post1}
      isSpecProjectPage={isSpecProjectPage}
    >
      {data.iconBlogger === '1' ? (
        <Link to={data.iconBloggerUrl}>
          <TagLeft iconBlogger>
            {data.iconBloggerImage && (
              <ImgWrap iconBlogger>
                <Img src={data.iconBloggerImage} />
              </ImgWrap>
            )}
            <TagName
              announceType={announceType}
              fontColor={_get(data, 'postFormat.fontColor')}
              className="tag-text"
              isSpecProjectPage={isSpecProjectPage}
              iconBlogger
            >
              БЛОГ {data.iconBloggerName}
            </TagName>
          </TagLeft>
        </Link>
      ) : (
        <TagLeft>
          {matchIcon && (
            <ImgWrap>
              <SVG
                iconCode={iconCode}
                fontColor={_get(data, 'postFormat.fontColor')}
                backgroundColor={_get(data, 'postFormat.backgroundColor')}
                announceType={announceType}
                defaultBackgroundFillCitate={defaultBackgroundFillCitate}
                defaultFontFillCitate={defaultFontFillCitate}
                typePost={typePost}
              />
            </ImgWrap>
          )}
          {iconName && (
            <LinkComponent
              tagBlank={tagBlank}
              tagUrl={tagUrl}
              iconName={iconName}
              isGroup={isGroup}
              dragging={dragging}
              setDragging={setDragging}
              specProject={data.specProject}
              isSpecProjectPage={isSpecProjectPage}
            >
              <TagName
                fontColor={_get(data, 'postFormat.fontColor')}
                className="tag-text"
                isSpecProjectPage={isSpecProjectPage}
                announceType={announceType}
                isSpecprojectTitle={isSpecprojectTitle}
              >
                {!isSpecprojectTitle ? (
                  <>
                    спецпроект <br /> {specProjectDescr}
                  </>
                ) : (
                  iconName
                )}
              </TagName>
            </LinkComponent>
          )}
        </TagLeft>
      )}
      {data.iconBlogger !== '1' && (
        <TagRight className="tag-text">
          <LinkType
            announceType={announceType}
            isSpecProjectPage={isSpecProjectPage}
            data={data}
            block3={block3}
          ></LinkType>
        </TagRight>
      )}
      <SVG />
    </TagsWrap>
  );
};

export default TagsTitle;
