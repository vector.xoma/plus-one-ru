import styled, { css } from 'styled-components/macro';

const TagsWrap = styled.div`
  width: 100%;
  position: relative;
  display: flex;
  justify-content: space-between;
  align-items: center;
  z-index: ${({ isSpecProjectPage }) => (isSpecProjectPage ? '6' : '15')};
  cursor: pointer;
  margin-bottom: ${({ isMainPage, post1, isSpecProjectPage }) =>
    (isMainPage || isSpecProjectPage) && post1 ? '0' : '30px'};
  min-height: 36px;
  order: ${({ isMainPage, isSpecProjectPage, post1 }) =>
    (isMainPage || isSpecProjectPage) && post1 ? '2' : '0'};
  @media (max-width: 767px) {
    order: 0;
  }
`;

const TagLeft = styled.div`
  display: flex;
  align-items: center;
  max-width: ${({ iconBlogger }) => (iconBlogger ? '300px' : '190px')};
  width: ${({ iconBlogger }) => (iconBlogger ? 'auto' : '100%')};
  background-color: ${({ iconBlogger }) =>
    iconBlogger ? 'rgba(255,255,255, 0.8)' : 'transparent'};
  border-radius: ${({ iconBlogger }) => (iconBlogger ? '5px' : '0')};
  padding: ${({ iconBlogger }) => (iconBlogger ? '5px 12px;' : '0')};
`;
const Img = styled.img``;

const ImgWrap = styled.div`
  width: ${({ iconBlogger }) => (iconBlogger ? '24px' : '30px')};
  height: ${({ iconBlogger }) => (iconBlogger ? '24px' : '30px')};
  position: static;
  margin-right: 8px;
  ${Img} {
    width: 100%;
    height: 100%;
    object-fit: contain;
    display: block;
    position: static;
    border-radius: 0;
  }
`;

const TagName = styled.div`
  &.tag-text {
    text-transform: uppercase;
    font-family: 'MullerMedium';
    font-size: 13px;
    display: inline-block;
    vertical-align: middle;
    color: ${({ fontColor = '#000', announceType, isSpecProjectPage }) =>
      announceType === 'picture' || isSpecProjectPage ? '#fff' : fontColor};
    ${({ iconBlogger }) =>
      iconBlogger && {
        color: '#000',
        lineHeight: '1.2',
        height: '13px',
        marginLeft: '6px',
      }}
      line-height:${({ isSpecprojectTitle }) => !isSpecprojectTitle && '1.2'};
  }
`;
const TagRight = styled.div``;

const styleLinks = css`
  padding: 4px 8px 2px;
  ${({ bordercolor, isPartnerName, isPartnerMaterial }) =>
    isPartnerName || isPartnerMaterial === '1'
      ? { border: '1px solid', borderColor: bordercolor }
      : {}};
  border-color: ${({ announceType, bordercolor }) =>
    announceType === 'picture' ? '#fff' : bordercolor};
  text-transform: ${({ isPartnerMaterial }) =>
    isPartnerMaterial ? 'none' : 'uppercase'};
  font-family: 'MullerRegular';
  font-size: 14px;
  letter-spacing: 0.24px;
  color: ${({ fontColor = '#000', announceType, isSpecProjectPage }) =>
    announceType === 'picture' || isSpecProjectPage ? '#fff' : fontColor};
  :hover {
    color: ${({ fontColor = '#000', announceType, isSpecProjectPage }) =>
      announceType === 'picture' || isSpecProjectPage ? '#fff' : fontColor};
  }
`;

const PartnerRoute = styled.span`
  ${styleLinks}
  display:block;
`;

const PartnerLink = styled.a`
  ${styleLinks}
`;

export {
  PartnerRoute,
  PartnerLink,
  TagsWrap,
  TagRight,
  TagLeft,
  ImgWrap,
  TagName,
  Img,
};
