export const checkBlank = (blankValue, url, history) => {
  if (blankValue === '_blank') window.open(url);
  if (blankValue === '_self') history.push(`/${url}`);
};
