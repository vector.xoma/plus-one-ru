import React from 'react';
import Quote from './components/Quote';
import Post from './components/Post';
import News from './components/News';
import FactDay from './components/FactDay';
import SubscribeForm from './components/SubscribeForm';

const Block = ({
  masonryMobileLayout = {},
  rubricValues,
  articlePageRow,
  isMasonryPage,
  setDragging,
  isMainPage,
  dragging,
  styles,
  style,
  type,
  data,
  isGroup,
}) => {
  switch (type) {
    case 'post':
    case 'imageday':
    case 'diypost': {
      return (
        <Post
          masonryMobileLayout={masonryMobileLayout}
          rubricValues={rubricValues}
          articlePageRow={articlePageRow}
          isMasonryPage={isMasonryPage}
          setDragging={setDragging}
          isMainPage={isMainPage}
          dragging={dragging}
          typePost={type}
          style={style}
          data={data}
        />
      );
    }
    case 'citate': {
      return (
        <Quote
          masonryMobileLayout={masonryMobileLayout}
          articlePageRow={articlePageRow}
          setDragging={setDragging}
          dragging={dragging}
          typePost={type}
          style={style}
          data={data}
        />
      );
    }
    case 'factday': {
      return (
        <FactDay
          masonryMobileLayout={masonryMobileLayout}
          articlePageRow={articlePageRow}
          isMasonryPage={isMasonryPage}
          setDragging={setDragging}
          dragging={dragging}
          typePost={type}
          styles={styles}
          style={style}
          data={data}
        />
      );
    }
    case 'subscribe': {
      return <SubscribeForm mainPage></SubscribeForm>;
    }
    case 'news': {
      return (
        <News
          masonryMobileLayout={masonryMobileLayout}
          isMasonryPage={isMasonryPage}
          articlePageRow={articlePageRow}
          setDragging={setDragging}
          dragging={dragging}
          typePost={type}
          style={style}
          data={data}
          isGroup={isGroup}
        />
      );
    }
    default:
      return '';
  }
};

export default Block;
