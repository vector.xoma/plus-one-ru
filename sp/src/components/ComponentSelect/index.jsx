import React from 'react';
import { Select } from 'antd';
import { useComponents } from 'contexts/componentsContext';

const { Option } = Select;
const ComponentSelect = ({ value, onChange, showEmpty = false }) => {
  const { components } = useComponents();

  return (
    <Select
      labelId="demo-simple-select-label"
      id="demo-simple-select"
      className="header__selectComponent"
      value={value}
      onChange={onChange}
    >
      {showEmpty && (
        <Option value={0} className="select_emptyRow">
          Новый
        </Option>
      )}
      {components &&
        components.map(c => (
          <Option key={c.id} value={c.id}>
            {c.name}
          </Option>
        ))}
    </Select>
  );
};

export default ComponentSelect;
