import React from 'react';

import { SmallTextDescription } from '../style';
import { DonateButtonWrap, Btn } from './style';

const DonateButton = ({ text, submit }) => {
  return (
    <DonateButtonWrap>
      <Btn onClick={submit}>Поддержать</Btn>
      <SmallTextDescription>{text}</SmallTextDescription>
    </DonateButtonWrap>
  );
};

export default DonateButton;
