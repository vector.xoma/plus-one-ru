import styled from 'styled-components/macro';

const DonateButtonWrap = styled.div`
  width: 100%;
  max-width: 230px;
  @media (max-width: 767px) {
    max-width: 100%;
  }
`;

const Btn = styled.button`
  width: 100%;
  height: 55px;
  background-color: #e7442b;
  border: 2px solid #e7442b;
  font-family: 'MabryPro-Medium', sans-serif;
  color: #fff;
  cursor: pointer;
  border-radius: 5px;
`;

export { DonateButtonWrap, Btn };
