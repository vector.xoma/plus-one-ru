import React from 'react';

import { SmallTextDescription, ErrorMessage } from '../style';
import { DonateInputContainer, DonateInputWrap } from './style';

const DonateInput = ({ text, touched, valid, errorText, ...inputProps }) => {
  return (
    <DonateInputContainer>
      <DonateInputWrap>
        <input {...inputProps} />
        {!valid && touched ? <ErrorMessage>{errorText}</ErrorMessage> : null}
      </DonateInputWrap>
      <SmallTextDescription>{text}</SmallTextDescription>
    </DonateInputContainer>
  );
};

export default DonateInput;
