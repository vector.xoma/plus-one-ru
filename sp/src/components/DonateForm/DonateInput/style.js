import styled from 'styled-components/macro';

const DonateInputContainer = styled.div`
  width: 100%;
  max-width: 250px;
  @media (max-width: 767px) {
    max-width: 100%;
  }
`;

const DonateInputWrap = styled.div`
  width: 100%;
  height: 100%;
  border: solid 2px #000000;
  font-size: 18px;
  background-color: #fff;
  outline: none;
  border-radius: 5px;
  font-family: 'MabryPro-Regular', sans-serif;
  appearance: none;
  height: 55px;
  input {
    width: 100%;
    display: block;
    height: 100%;
    padding: 15px 25px 15px 14px;
    color: #000;
    box-sizing: border-box;
    outline: none;
    border: none;
    ::placeholder {
      color: #000;
    }
  }
`;

export { DonateInputContainer, DonateInputWrap };
