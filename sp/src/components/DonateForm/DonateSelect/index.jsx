import React from 'react';
import { Select } from 'antd';
import { SmallTextDescription, ErrorMessage } from '../style';
import { SelectContainer, SelectWrap, CustomSelect } from './style';

import 'antd/es/select/style/css';

const { Option } = Select;
// TODO: Option застилизованы спомощью scss т.к. не получается стилизовать с помощью styled.Component(нельзя обратиться к option);

const paymentMethod = [
  { value: 'ac', txt: 'Банковская карта' },
  { value: 'sb', txt: 'Сбербанк' },
  { value: 'pc', txt: 'Яндекс.Деньги' },
  { value: 'gp', txt: 'Через кассы и терминалы' },
  { value: 'ab', txt: 'Альфа-Клик' },
  { value: 'wm', txt: 'WebMoney' },
  { value: 'ma', txt: 'MasterPass' },
];

const DonateSelect = ({
  text,
  selectHandler,
  value,
  errorText,
  touched,
  zoom,
}) => {
  return (
    <SelectContainer>
      <SelectWrap>
        <CustomSelect
          // suffixIcon={arrowSelect}
          value={value}
          defaultValue="Способ оплаты"
          placeholder="Способ оплаты"
          dropdownStyle={{ transform: `scale(${zoom})` }}
          onChange={selectHandler}
        >
          {paymentMethod.map(({ txt, value }, i) => (
            <Option key={i} className="custom-option" value={value}>
              {txt}
            </Option>
          ))}
        </CustomSelect>
        {value === 'Способ оплаты' && touched ? (
          <ErrorMessage>{errorText}</ErrorMessage>
        ) : null}
        <SmallTextDescription>{text}</SmallTextDescription>
      </SelectWrap>
    </SelectContainer>
  );
};

export default DonateSelect;
