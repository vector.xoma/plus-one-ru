import styled from 'styled-components/macro';
import { Select } from 'antd';

const SelectContainer = styled.div`
  width: 100%;
  max-width: 260px;
  @media (max-width: 767px) {
    max-width: 100%;
  }
`;

const SelectWrap = styled.div`
  width: 100%;
  height: 100%;
`;
const CustomSelect = styled(Select)`
  height: 55px;
  border: solid 2px #000000;
  borderradius: 5px;
  width: 100%;

  outline: none;
  .ant-select-selection {
    height: 100%;
    outline: none;
    border: none;
    :active {
      border: none;
    }
    :focus {
      border: none;
    }
    :hover {
      border: none;
    }
    :active {
      border: none;
    }
    .ant-select-selection__rendered {
      height: 100%;
      outline: none;
      .ant-select-selection-selected-value {
        height: 100%;
        display: flex !important;
        align-items: center;
        font-family: 'MabryPro-Regular', sans-serif;
        font-size: 18px;
        color: #000;
        outline: none;
      }
    }
  }
`;

export { SelectContainer, SelectWrap, CustomSelect };
