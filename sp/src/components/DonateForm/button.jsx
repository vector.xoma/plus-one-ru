import React, { useState } from 'react';
import { Button } from 'antd';

const DonateButton = ({ setDonateForm }) => {
  const [text, setText] = useState('');

  return (
    <Button
      onClick={() => {
        setDonateForm({ header: text });
        setText('');
      }}
    >
      Форма пожертвований
    </Button>
  );
};

export default DonateButton;
