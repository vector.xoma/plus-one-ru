import { Payments } from 'connectors/query/Payments';

const enterOnlyNumbers = (event, { setSum, setSumValid }) => {
  const currentSum = event.target.value;
  if (currentSum[0] === '0') {
    return;
  }
  setSum(currentSum.replace(/[^\d,]/g, ''));
  if (currentSum.replace(/[^\d,]/g, '')) {
    setSumValid(true);
  }
};

const txt = {
  textSum:
    'Мы не берем комиссию за пожертвования и делаем все, чтобы комиссия оператора становилась ниже',
  textMail:
    'Введите e-mail, чтобы получить подтверждение о зачислении пожертвования и отчет о его расходовании',
  textPayment: 'Выберите способ оплаты',
  textButton:
    'Нажимая на кнопку «Поддержать», я принимаю условия Положения о благотворительной программе «Поможем вместе».',
};

const submit = (
  event,
  {
    setSumTouched,
    setMailTouched,
    setSelectTouched,
    mail,
    regMailValidator,
    setMailValid,
    setSumValid,
    setSum,
    select,
    setSelectValid,
    sum,
    resourceId,
    setMail,
    setSelect,
    dataValue,
    shopId,
  },
) => {
  event.preventDefault();
  let _mailValid = true;
  let _sumValid = true;
  let _selectValid = true;

  setSumTouched(true);
  setMailTouched(true);
  setSelectTouched(true);

  if (!mail.match(regMailValidator)) {
    setMailValid(false);
    _mailValid = false;
  }

  if (!(sum && sum > 0)) {
    setSumValid(false);
    _sumValid = false;
    setSum('');
  }

  if (select === 'Способ оплаты') {
    _selectValid = false;
    setSelectValid(false);
  }

  if (_mailValid && _sumValid && _selectValid) {
    setMail('');
    setSum('');
    setSelect('Способ оплаты');
    setSumTouched(false);
    setMailTouched(false);
    setSelectTouched(false);
    dataValue();

    const formData = new FormData();
    formData.append('payment[amount]', sum);
    formData.append('payment[resource_id]', resourceId);
    formData.append('payment[resource_type]', 'Foundation');
    formData.append('payment[terms]', '0');
    formData.append('payment[terms]', '1');
    formData.append('payment[pay_system]', select);
    formData.append('payment[email]', mail);
    formData.append('utf8', '&#x2713;');
    formData.append('g-recaptcha-response', '');
    formData.append('commit', 'Перечислить');

    const sendPaymant = async () => {
      const { id = 1, pay_system, sum } = await Payments.postPayments(formData);
      const formDataYa = new FormData();
      formDataYa.append('sum', sum);
      formDataYa.append('shopId', 19785);
      formDataYa.append('scid', 10422);
      formDataYa.append('customerNumber', id);
      formDataYa.append('paymentType', pay_system);
      formDataYa.append('cps_email', mail);
      formDataYa.append('ShopArticleID', shopId);

      const {
        request: { responseURL },
      } = await Payments.yandexPayments(formDataYa);
      if (responseURL) {
        window.open(responseURL);
        return;
      }
      console.error(`responseURL is - ${responseURL}`);
    };
    sendPaymant();
  }
};

export { enterOnlyNumbers, txt, submit };
