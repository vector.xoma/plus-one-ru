import React, { useState } from 'react';
import useDesktop from 'hooks/useDesktop';

import DonateInput from './DonateInput';
import DonateSelect from './DonateSelect';
import DonateButton from './DonateButton';
import { enterOnlyNumbers, txt, submit } from './helpers';

import {
  TextBottomMobile,
  DonateFormWrap,
  DonateControls,
  FormImgRight,
  FormLogoImg,
  FormImgLeft,
  FormHeader,
  FormTitle,
  FormLogo,
} from './style';

const DonateForm = ({ zoom, data: { header, resourceId, shopId } = {} }) => {
  const { textSum, textMail, textPayment, textButton } = txt;
  const isDesktop = useDesktop();
  const [sum, setSum] = useState('');
  const [mail, setMail] = useState('');
  const [select, setSelect] = useState('Способ оплаты');

  const [sumValid, setSumValid] = useState(false);
  const [selectValid, setSelectValid] = useState(false);
  const [mailValid, setMailValid] = useState(true);

  const [sumTouched, setSumTouched] = useState(false);
  const [mailTouched, setMailTouched] = useState(false);
  const [selectTouched, setSelectTouched] = useState(false);

  const regMailValidator = /^([A-Za-z0-9_\-.])+@([A-Za-z0-9_\-.])+\.([A-Za-z]{2,4})$/;
  const dataValue = () => {};

  const validEmail = event => {
    const currentMail = event.target.value.trim();

    setMail(currentMail);
    setMailTouched(true);

    if (currentMail.match(regMailValidator)) {
      setMailValid(true);
    }
  };

  const selectHandler = value => {
    setSelect(value);
    setSelectTouched(true);
    setSelectValid(true);
  };

  const handleSubmit = e => {
    submit(e, {
      setSelectTouched,
      regMailValidator,
      setSelectValid,
      setMailTouched,
      setSumTouched,
      setMailValid,
      setSumValid,
      resourceId,
      dataValue,
      setSelect,
      setMail,
      shopId,
      setSum,
      select,
      mail,
      sum,
    });
  };

  return (
    <DonateFormWrap>
      {isDesktop && <FormImgLeft />}
      <FormHeader>
        <FormLogo>
          <FormLogoImg />
        </FormLogo>
        <FormTitle>{header}</FormTitle>
        {/* <FormTitle>Заголовок для формы пожертвования</FormTitle> */}
        {!isDesktop && <FormImgLeft />}
      </FormHeader>
      <DonateControls>
        <DonateInput
          errorText="Введите сумму"
          placeholder="Сумма пожертвования"
          onChange={e => {
            enterOnlyNumbers(e, { setSum, setSumValid });
          }}
          text={isDesktop ? textSum : null}
          value={sum}
          name="sum"
          touched={sumTouched}
          valid={sumValid}
        />
        <DonateSelect
          errorText="Выберите способ оплаты"
          text={isDesktop ? textPayment : null}
          selectHandler={selectHandler}
          selectValid={selectValid}
          touched={selectTouched}
          valid={selectValid}
          value={select}
          zoom={zoom}
        />
        <DonateInput
          text={isDesktop ? textMail : null}
          errorText="E-mail некорректен"
          onChange={validEmail}
          touched={mailTouched}
          placeholder="Email"
          valid={mailValid}
          value={mail}
          name="email"
        />
        <DonateButton
          text={isDesktop ? textButton : null}
          submit={handleSubmit}
        />
      </DonateControls>
      {!isDesktop && (
        <div>
          <TextBottomMobile>
            Мы не берем комиссию за пожертвования и делаем все, чтобы комиссия
            оператора становилась ниже.
          </TextBottomMobile>
          <TextBottomMobile>
            Нажимая на кнопку «Поддержать», я принимаю условия Положения о
            благотворительной программе «Поможем вместе».
          </TextBottomMobile>
        </div>
      )}
      {isDesktop && <FormImgRight />}
    </DonateFormWrap>
  );
};

export default DonateForm;
