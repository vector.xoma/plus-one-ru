import styled from 'styled-components/macro';
import peopleSvg from 'img/people-short.svg';

const SmallTextDescription = styled.div`
  width: 100%;
  font-family: 'MabryPro-Regular', sans-serif;
  font-size: 13px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.23;
  letter-spacing: normal;
  text-align: left;
  color: #868686;
  margin-top: 20px;
`;

const ErrorMessage = styled.div`
  font-size: 10px;
  color: #e7442b;
  position: absolute;
`;

const FormImg = styled.div`
  position: absolute;
  background-image: url(${peopleSvg});
  background-repeat: no-repeat;
  background-size: auto;
  max-width: 319px;
  width: 100%;
  height: 125px;
  top: 0;
`;

const FormImgLeft = styled(FormImg)`
  left: 0;
  background-position: bottom right;
  @media (max-width: 767px) {
    position: static;
    width: calc(100% + 40px);
    margin-left: -20px;
    margin-right: -20px;
    margin-bottom: 10px;
    height: 100px;
    background-size: auto;
    background-position: center;
  }
`;

const FormImgRight = styled(FormImg)`
  right: 0;
  background-position: bottom left;
`;

const FormHeader = styled.div`
  position: relative;
`;

const FormLogo = styled.div`
  max-width: 106px;
  margin: 0 auto;
`;

const FormLogoImg = styled.img.attrs({
  src: require('img/form-title.svg'),
  alt: 'logo',
})`
  text-align: center;
  margin-bottom: 30px;
  @media (max-width: 767px) {
    margin-bottom: 25px;
  }
`;

const FormTitle = styled.h2`
  min-height: 60px;
  max-width: 500px;
  padding: 0 20px;
  margin: 0 auto;
  margin-bottom: 35px;
  text-align: center;
  font-family: 'MabryPro-Bold', sans-serif;
  font-size: 24px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 30px;
  letter-spacing: normal;
  text-align: center;
  color: #000000;
  @media (max-width: 767px) {
    font-size: 18px;
    line-height: 1.33;
    margin-bottom: 10px;
    padding: 0;
  }
`;

const DonateFormWrap = styled.form`
  max-width: 1140px;
  margin: 0 auto;
  background-color: #fff;
  padding: 12px 42px 24px;
  border-radius: 5px;
  position: relative;
  margin-bottom: 20px;
  @media (max-width: 767px) {
    padding-left: 20px;
    padding-right: 20px;
  }
`;

const DonateControls = styled.div`
  display: flex;
  justify-content: space-between;
  @media (max-width: 767px) {
    flex-wrap: wrap;
  }
`;

const TextBottomMobile = styled.p`
  font-family: 'MabryPro-Regular', sans-serif;
  font-size: 13px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.23;
  letter-spacing: normal;
  text-align: left;
  color: #868686;
`;

export {
  SmallTextDescription,
  ErrorMessage,
  FormImg,
  FormImgLeft,
  FormImgRight,
  FormHeader,
  FormLogo,
  FormLogoImg,
  FormTitle,
  DonateFormWrap,
  DonateControls,
  TextBottomMobile,
};
