import React from 'react';
import { ButtonWrap, ButtonLineFirst, ButtonLineSecond } from './style';
const CloseButton = ({ setVisible, isVisible }) => (
  <ButtonWrap onClick={() => setVisible(!isVisible)}>
    <ButtonLineFirst />
    <ButtonLineSecond />
  </ButtonWrap>
);

export default CloseButton;
