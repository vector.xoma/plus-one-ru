import styled, { keyframes } from 'styled-components/macro';

const ButtonWrap = styled.div`
  position: absolute;
  left: 0;
  top: 32px;
  width: 37px;
  height: 37px;
  cursor: pointer;

  @media (max-width: 767px) {
    width: 18px;
    height: 18px;
    top: 25px;
    left: 28px;
  }
`;
const ButtonLine = styled.div`
  position: absolute;
  left: 50%;
  top: 50%;
  width: 40px;
  height: 4px;
  background: #fff;
  transform-origin: center;
  @media (max-width: 767px) {
    width: 22px;
    height: 3px;
  }
`;
const ButtonLineFirst = styled(ButtonLine)`
  transform: translateX(-50%) rotate(45deg);
`;
const ButtonLineSecond = styled(ButtonLine)`
  transform: translateX(-50%) rotate(-45deg);
`;

export { ButtonWrap, ButtonLineFirst, ButtonLineSecond };
