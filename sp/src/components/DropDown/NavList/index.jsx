import React, { useRef, useEffect } from 'react';

import { LinkWrap, A, LinkRoute, NavLIstCol } from './style';

const CustomLink = ({ bgColor, url, title, margin = false }) => {
  const linkRef = useRef(null);

  useEffect(() => {
    linkRef.current.style.setProperty('--bgColor', bgColor);
  }, [bgColor]);

  return url.includes('http') ? (
    <A ref={linkRef} target="_blank" href={url} rel="noopener noreferrer">
      {title}
    </A>
  ) : (
    <LinkRoute ref={linkRef} to={url}>
      {title}
    </LinkRoute>
  );
};

const NavList = ({ contentType }) => (
  <NavLIstCol>
    {contentType.map((el, i) => (
      <LinkWrap key={i} margin={el.margin ? true : false}>
        <CustomLink url={el.src} title={el.title} bgColor={el.borderColor} />
      </LinkWrap>
    ))}
  </NavLIstCol>
);

export default NavList;
