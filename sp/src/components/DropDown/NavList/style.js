import styled, { css } from 'styled-components/macro';
import { Link } from 'react-router-dom';

const NavLIstCol = styled.div`
  font-size: 30px;

  @media (max-width: 767px) {
    font-size: 20px;
    :first-child {
      margin-top: 50px;
    }
  }
`;
const LinkWrap = styled.div`
  margin-bottom: ${({ margin }) => margin && '60px'};
  @media (max-width: 767px) {
    margin-bottom: ${({ margin }) => margin && '40px'};
  }
`;

const LinkStyle = css`
  font-size: 30px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.33;
  letter-spacing: normal;
  text-align: left;
  color: #ffffff;
  font-family: 'MullerMedium';
  display: inline-block;
  &:hover {
    border-bottom-width: inherit;
    color: inherit;
  }

  @media (max-width: 767px) {
    font-size: 20px;
    line-height: 1.45;
  }
`;

const A = styled.a`
  ${LinkStyle}
`;

const LinkRoute = styled(Link)`
  ${LinkStyle}
`;

export { LinkWrap, A, LinkRoute, NavLIstCol };
