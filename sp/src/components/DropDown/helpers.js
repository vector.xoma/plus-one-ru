const contacts = [
  { title: 'О проекте', src: '/about' },
  {
    title: 'Яндекс.Дзен',
    src: 'https://zen.yandex.ru/id/5b333225146cd600a920df8d',
  },
  { title: 'Facebook', src: 'https://www.facebook.com/ProjectPlus1Official/' },
  { title: 'ВКонтакте', src: 'https://vk.com/plus1ru' },
  { title: 'Instagram', src: 'https://www.instagram.com/projectplus1official' },
  { title: 'YouTube', src: 'https://www.youtube.com/c/Plusoneru' },
  { title: 'Telegram', src: 'https://t.me/projectplusone' },
];

const itemsLeft = [
  { title: 'Новости', src: '/news' },
  { title: 'Сюжеты', src: '/story' },
  { title: 'Инструкции', src: '/manual' },
  { title: 'Коронавирус', src: '/story/glavnoe-o-koronaviruse', margin: true },
  { title: 'Экология', src: '/ecology' },
  { title: 'Экономика', src: '/economy' },
  { title: 'Общество', src: '/society', margin: true },
  { title: 'Устойчивое развитие', src: '/устойчивое-развитие' },
  { title: 'Спецпроекты', src: '/special-projects' },
  { title: 'Блоги', src: '/platform' },
  { title: 'Участники', src: '/author', margin: true },
];

const itemsCenter = [
  { title: 'РБК+1', src: 'http://plus-one.rbc.ru' },
  { title: 'Forbes+1', src: 'http://plus-one.forbes.ru' },
  { title: 'Ведомости+1', src: 'http://plus-one.vedomosti.ru', margin: true },
  { title: '+1Город', src: 'https://полезныйгород.рф' },
  { title: '+1Платформа', src: 'https://platform.plus-one.ru' },
  { title: '+1Люди', src: 'https://people.plus-one.ru/' },
  { title: '+1Премия', src: 'https://award.plus-one.ru/' },
];

const itemsMobileProjects = [
  { title: '+1Город', src: 'https://полезныйгород.рф' },
  { title: '+1Платформа', src: 'https://platform.plus-one.ru' },
  { title: '+1Люди', src: 'https://people.plus-one.ru/' },
  { title: '+1Премия', src: 'https://award.plus-one.ru/', margin: true },
];
const itemsMobilePartners = [
  { title: 'РБК+1', src: 'http://plus-one.rbc.ru' },
  { title: 'Forbes+1', src: 'http://plus-one.forbes.ru' },
  { title: 'Ведомости+1', src: 'http://plus-one.vedomosti.ru', margin: true },
];

export {
  itemsLeft,
  itemsCenter,
  contacts,
  itemsMobileProjects,
  itemsMobilePartners,
};
