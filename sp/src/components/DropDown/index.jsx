import React, { useState, useRef } from 'react';
import { Transition } from 'react-transition-group';

import CloseButton from './CloseButton';
import NavList from './NavList';
import useDesktop from 'hooks/useDesktop';
import useOnClickOutside from 'hooks/useOnClickOutside';

import { DropDownWrap, DropDownContainer, DropdownNavWrap } from './style';
import {
  itemsLeft,
  itemsCenter,
  contacts,
  itemsMobileProjects,
  itemsMobilePartners,
} from './helpers';
import { useEffect } from 'react';

const DropDown = ({ visible, setVisible, isHeaderFixedOpen, headerHeight }) => {
  const isDesktop = useDesktop();

  const dropDownRef = useRef();

  useOnClickOutside(dropDownRef, () => {
    return setVisible(false);
  });

  useEffect(() => {
    const checkVisible = () => {
      if (visible) {
        setVisible(false);
      }
    };

    document.addEventListener('scroll', checkVisible);
    return () => document.removeEventListener('scroll', checkVisible);
  }, [visible]);

  return (
    <Transition
      in={visible}
      timeout={280}
      unmountOnExit
      setVisible={setVisible}
      isVisible={visible}
    >
      <DropDownWrap
        isVisible={visible}
        isHeaderFixedOpen={isHeaderFixedOpen}
        ref={dropDownRef}
      >
        <DropDownContainer>
          <CloseButton setVisible={setVisible} isVisible={visible} />
          <DropdownNavWrap>
            {isDesktop ? (
              <>
                <NavList contentType={itemsLeft} />
                <NavList contentType={itemsCenter} />
                <NavList contentType={contacts} />
              </>
            ) : (
              <>
                <NavList contentType={itemsLeft} />
                <NavList contentType={itemsMobileProjects} />
                <NavList contentType={itemsMobilePartners} />
                <NavList contentType={contacts} />
              </>
            )}
          </DropdownNavWrap>
        </DropDownContainer>
      </DropDownWrap>
    </Transition>
  );
};

export default DropDown;
