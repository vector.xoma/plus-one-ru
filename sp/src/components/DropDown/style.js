import styled, { keyframes } from 'styled-components/macro';

const dropAnimation = keyframes`
    from {
      top: -100%;
    }
    to {
      top:0;
    }
  `;

const dropAnimationReverse = keyframes`
    from {
      top: 0;
    }
    to {
      top: -100%;
    }
  `;

const DropDownWrap = styled.div`
  position: fixed;
  left: 0;
  max-height: 100vh;
  top: 0;
  min-height: 200px;
  width: 100%;
  background: #000;
  z-index: 999;
  animation: ${({ isVisible }) =>
      isVisible ? dropAnimation : dropAnimationReverse}
    0.4s;
  animation-timing-function: ease-in-out;
  @media (max-width: 767px) {
    overflow-y: auto;
    min-height: -webkit-fill-available;
  }
`;

const DropDownContainer = styled.div`
  max-width: 1024px;
  margin: 0 auto;
  color: #fff;
  position: relative;
`;

const DropdownNavWrap = styled.nav`
  width: 100%;
  display: flex;
  justify-content: space-between;
  padding: 125px 0 100px;
  @media (max-width: 767px) {
    flex-direction: column;
    padding-left: 28px;
    padding-top: 30px;
    padding-bottom: 60px;
    min-height: -webkit-fill-available;
  }
`;

const styles = {
  container: {
    flexDirection: 'column',
    margin: '20px 0 0 0 ',
    display: 'block',
  },
  item: {
    justifyContent: 'flex-start',
    borderBottom: '1px solid #333333',
  },
  block: {
    paddingLeft: '0',
  },
};

export { DropDownWrap, DropDownContainer, DropdownNavWrap };
