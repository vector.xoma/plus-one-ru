import React, {useRef, useEffect, useState} from 'react';
import styled from 'styled-components/macro';

const youtubeStyles = {
  iframe: {
    position: 'absolute',
    top: '0',
    left: '0',
    width: '100%',
    height: '100%',
    borderWidth: '0',
    outlineWidth: '0',
  },
  container: {
    position: 'relative',
    paddingBottom: '56.25%',
    height: '0',
    width: '100%',
    overflow: 'hidden',
  },
};
const EmbedWrap = styled.div`
  display: flex;
  justify-content: center;
  color: #000;
  margin-bottom: 55px;
  iframe {
    @media (max-width: 767px) {
      ${({ type, styles }) => (type === 'youtube' ? styles : { width: '100%' })}
    }
    ${({ type, styles }) => (type === 'youtube' ? styles : {})}

    ${({ type }) =>
      type === 'instagram' ? { margin: '0 auto !important' } : {}}
  }
`;

const EmbedContainer = styled.div`
  margin: 0 auto;
  ${({ type, styles }) => (type === 'youtube' ? styles : {})};
  ${({ type }) => (type === 'instagram' ? { width: '100%' } : {})}
  ${({ type }) =>
    type === 'вконтакте'
      ? { width: '100%', maxWidth: '500px' }
      : {}}
  .twitter-tweet {
    width: 100% !important;
  }
  @media (max-width: 767px) {
    ${({ type, styles }) => (type === 'youtube' ? styles : { width: '100%' })};
  }
`;

const twitterTypes = ['twitter-tweet', 'twitter-embed'];
const instagramTypes = ['instagram-media'];

const Embed = ({ data }) => {
  // data.type - хранит инфу о типе эмбеда. устанавливается пользователем.
  const type = data.type.toLowerCase();
  const emb = useRef(null);
  const [isVideo, setIsVideo] = useState(false);

  useEffect(() => {
    if (type === 'twitter') {
      const scriptTwitter = document.createElement('script');
      scriptTwitter.src = 'https://platform.twitter.com/widgets.js';
      twitterTypes.forEach(t => {
        if (document.getElementsByClassName(t)[0]) {
          document.getElementsByClassName(t)[0].appendChild(scriptTwitter);
        }
      });
    }

    if (type === 'instagram') {
      const scriptInstagram = document.createElement('script');
      scriptInstagram.src = '//www.instagram.com/embed.js';
      instagramTypes.forEach(t => {
        if (document.getElementsByClassName(t)[0]) {
          document.getElementsByClassName(t)[0].appendChild(scriptInstagram);
        }
      });
      if (window.instgrm) {
        window.instgrm.Embeds.process();
      }
    }

    const setVkEmbed = async function () {
      if (data.embedInsert.match(/<iframe[^>]*>/gm)) {
        setIsVideo(true);
        return;
      }

      const scriptsInEmbed = data.embedInsert.match(
        /<script[^>]*>([\s\S]*?)<\/script>/gm,
      );
      const withSrc = [];
      const withoutSrc = [];

      const scriptVK = document.createElement('script');
      scriptVK.src = 'https://vk.com/js/api/openapi.js?167';
      emb.current.appendChild(scriptVK);

      // разбиваем скрипты на группы с SRC и без. 1е нужно добавить до запуска вторых
      scriptsInEmbed &&
        scriptsInEmbed.forEach(async function (s) {
          const regex = /<script[^>]*>([\s\S]*?)<\/script>/gm;
          let m;
          if ((m = regex.exec(s)) !== null) {
            if (!m[1]) {
              const matchSrc = m[0].match(
                /<script[^>]*src="([^>]*)"><\/script>/,
              );
              withSrc.push({ row: m[0], src: matchSrc[1] });
            }
            if (m[1]) withoutSrc.push({ func: m[1] });
          }
        });

      if (withSrc.length) {
        await withSrc.forEach(s => {
          const scriptVk = document.createElement('script');
          scriptVk.src = s.src;
          emb.current.appendChild(scriptVk);
          scriptVk.onload = () => {
            if (withoutSrc.length) {
              withoutSrc.forEach(({ func }) => {
                try {
                  /*eslint-disable no-eval */
                  eval(func);
                } catch (e) {
                  console.error('Ошибка в эмбеде VK.');
                }
              });
            }
          };
        });
      }

      if (!withSrc.length) {
        withoutSrc.forEach(({ func }) => {
          try {
            /*eslint-disable no-eval */
            eval(func);
          } catch (e) {
            console.error('Ошибка в эмбеде VK.');
          }
        });
      }
    };

    if (type === 'вконтакте') setVkEmbed();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data.embedInsert, isVideo]);

  return (
    <EmbedWrap type={isVideo ? 'youtube' : type} styles={youtubeStyles.iframe}>
      <EmbedContainer
        ref={emb}
        type={isVideo ? 'youtube' : type}
        styles={youtubeStyles.container}
        dangerouslySetInnerHTML={{
          __html: data.embedInsert,
        }}
      />
    </EmbedWrap>
  );
};
export default Embed;
