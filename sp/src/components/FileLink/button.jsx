import React, { useState, useEffect } from 'react';
import { Popover, Button, Input } from 'antd';
import styled from 'styled-components/macro';

import { splitBySelection } from 'utils';
import FileManager from 'containers/FileManager';
import { Wrap } from '../Grid/Grid';
import { BtnFindImg, FilesControl } from '../_styles/buttonsStyles';

const LinkButtonWrap = styled.div`
  width: 500px;
`;

const FileLinkButton = ({ setFileLink, textData }) => {
  const { middle } = splitBySelection(textData.text, textData.area);

  const [visible, setVisible] = useState(false);
  const [showFM, setShowFM] = useState(false);
  const [url, setUrl] = useState('');
  const [urlText, setUrlText] = useState(middle || '');
  const [targetBlank, setTargetBlank] = useState(false);

  useEffect(() => {
    setUrlText(middle);
  }, [middle]);

  const Content = (
    <LinkButtonWrap>
      <Wrap>
        <FilesControl>
          <Input
            placeholder="Url картинки"
            value={url || ''}
            onChange={e => setUrl(e.target.value)}
          />
          <BtnFindImg onClick={() => setShowFM(true)}></BtnFindImg>
        </FilesControl>
      </Wrap>
      <Wrap>
        <Input
          placeholder="Текст ссылки"
          value={urlText || ''}
          onChange={e => setUrlText(e.target.value)}
        />
      </Wrap>
      <Wrap>
        <Button
          onClick={() => {
            setUrl('');
            setFileLink({ url, urlText, targetBlank });
            setTargetBlank(false);
            setVisible(false);
          }}
        >
          Добавить
        </Button>
      </Wrap>
    </LinkButtonWrap>
  );

  return (
    <>
      <Popover
        content={Content}
        title="Ссылка"
        trigger="click"
        placement="right"
        visible={visible}
        onVisibleChange={visible => {
          if (!showFM) setVisible(visible);
        }}
      >
        <Button>Файл</Button>
      </Popover>

      <FileManager
        visible={showFM}
        setPicUrl={setUrl}
        hideModal={() => setShowFM(false)}
      />
    </>
  );
};

export default FileLinkButton;
