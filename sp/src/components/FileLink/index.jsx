import React, { useContext } from 'react';
import styled, { ThemeContext } from 'styled-components/macro';

const A = styled.a`
  display: inline;
  border-bottom: 2px solid #f8c946;
  -webkit-transition: all 0.4s;
  transition: all 0.4s;
  text-decoration: none;
  color: ${({ themeContext }) => themeContext.color};
  :hover {
    color: ${({ themeContext }) => themeContext.color};
    border-bottom: 2px solid #fff;
  }
`;

const ALink = ({ data }) => {
  const themeContext = useContext(ThemeContext);

  const matchPdf = data.url.includes('.pdf');
  return matchPdf ? (
    <A href={data.url} themeContext={themeContext} target="_blank">
      {data.text}
    </A>
  ) : (
    <A
      href={data.url}
      themeContext={themeContext}
      target="_blank"
      download={data.text}
    >
      {data.text}
    </A>
  );
};

export default ALink;
