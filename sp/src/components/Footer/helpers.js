const currentYear = new Date().getFullYear();

export const data = [
  [
    { title: 'Редакция', url: null },
    { title: 'О проекте', url: '/about', target: '_self' },
    { title: 'Спецпроекты', url: '/special-projects', target: '_self' },
    {
      title: `© 2016 - ${currentYear}`,
      url: null,
      mt: '35px',
    },
  ],
  [
    { title: 'Площадки', url: null },
    {
      title: 'РБК+1',
      url: 'http://plus-one.rbc.ru/',
      borderColor: '#2ad546',
    },
    {
      title: 'Forbes+1',
      url: 'http://plus-one.forbes.ru',
      borderColor: '#00bcee',
    },
    {
      title: 'Ведомости+1',
      url: 'http://plus-one.vedomosti.ru/',
      borderColor: '#fff',
    },
    {
      title: 'BFM+1',
      url: 'https://www.bfm.ru/special/visioners',
      borderColor: '#b98b00',
    },
  ],
  [
    { title: 'Рубрики', url: null },
    { title: 'Экология', url: '/ecology', target: '_self' },
    { title: 'Общество', url: '/society', target: '_self' },
    { title: 'Экономика', url: '/economy', target: '_self' },
    {
      title: 'Платформа',
      url: 'https://platform.plus-one.ru',
      target: '_blank',
    },
  ],
  [
    { title: 'Проекты', url: null },
    {
      title: '+1Платформа',
      url: 'https://platform.plus-one.ru',
    },
    { title: '+1Город', url: 'https://xn--c1acbikjqegacu3l.xn--p1ai/' },
    { title: '+1Люди', url: 'https://people.plus-one.ru/' },
    { title: '+1Премия', url: 'https://award.plus-one.ru/' },
    { title: '+1CONF', url: 'https://conf.plus-one.ru/' },
  ],
  [
    { title: 'Соцсети', url: null },
    {
      title: 'Яндекс.Дзен',
      url: 'https://zen.yandex.ru/id/5b333225146cd600a920df8d',
      target: '_blank',
    },
    {
      title: 'Facebook',
      url: 'https://www.facebook.com/ProjectPlus1Official/',
      target: '_blank',
    },
    {
      title: 'Вконтакте',
      url: 'https://vk.com/plus1ru',
      target: '_blank',
    },
    {
      title: 'Instagram',
      url: 'https://www.instagram.com/projectplus1official',
      target: '_blank',
    },

    {
      title: 'Youtube',
      url: 'https://www.youtube.com/c/Plusoneru',
      target: '_blank',
    },
    {
      title: 'Telegram',
      url: 'https://t.me/projectplusone',
      target: '_blank',
    },
  ],
];
