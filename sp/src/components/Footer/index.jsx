import React from 'react';

import PreparedLink from 'components/PreparedLink';
import footerLogo from 'img/PlusOneLogoFooterLogo.svg';
import useDesktop from 'hooks/useDesktop';
import {
  FooterWrap,
  FooterLogoWrap,
  FooterLogoImg,
  FooterContent,
  FooterList,
  FooterItem,
  FooterItemTitle,
} from './style';
import { data } from './helpers';

const Footer = () => {
  const isDesktop = useDesktop();
  return (
    <FooterWrap>
      <FooterLogoWrap>
        <FooterLogoImg src={footerLogo} />
      </FooterLogoWrap>
      {isDesktop && (
        <FooterContent>
          {data.map((elem, index) => {
            return (
              <FooterList key={index}>
                {elem.map((el, i) => {
                  return (
                    <FooterItem key={i}>
                      {el.url ? (
                        <PreparedLink
                          bordercolor={el.borderColor}
                          url={el.url}
                          title={el.title}
                          target={el.target}
                        />
                      ) : (
                        <FooterItemTitle mt={el.mt}>{el.title}</FooterItemTitle>
                      )}
                    </FooterItem>
                  );
                })}
              </FooterList>
            );
          })}
        </FooterContent>
      )}
    </FooterWrap>
  );
};

export default Footer;
