import styled from 'styled-components/macro';

const FooterWrap = styled.footer`
  width: 100%;
  /* margin-top: 170px; */
  padding: 45px 0 85px;
  background-color: #000000;
`;

const FooterLogoWrap = styled.div`
  width: 80px;
  height: 42px;
  margin: 0 auto 45px;
`;

const FooterLogoImg = styled.img`
  display: block;
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

const FooterContent = styled.div`
  display: flex;
  max-width: 1140px;
  margin: 0 auto;
  justify-content: center;
`;

const FooterList = styled.ul`
  display: flex;
  flex-direction: column;
  margin: 0 70px 0 0;
  :last-child {
    margin-right: 0;
  }
`;

const FooterItem = styled.li`
  line-height: normal;
  position: relative;
`;

const FooterItemTitle = styled.p`
  font-size: 16px;
  color: #5c5c5c;
  font-family: 'MullerMedium';
  margin: 0;
  padding-bottom: 15px;
  line-height: normal;
  margin-top: ${({ mt }) => (mt ? mt : 0)};
`;

export {
  FooterWrap,
  FooterLogoWrap,
  FooterLogoImg,
  FooterContent,
  FooterList,
  FooterItem,
  FooterItemTitle,
};
