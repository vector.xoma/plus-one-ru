import React, { useState, useEffect } from 'react';
import { Radio, Button, Modal } from 'antd';

import { Galleries } from 'connectors/query/Galleries';
import { Wrap } from '../Grid/Grid';

const FotoRamaButton = ({ setFotoRama }) => {
  const [galleriesList, setGalleriesList] = useState([]);
  const [selectedGal, setSelectedGal] = useState(null);
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    async function getGalleries() {
      const result = await Galleries.getList();
      if (result && result.length) setGalleriesList(result);
    }
    getGalleries();
  }, []);

  return (
    <>
      <Modal
        visible={visible}
        onCancel={() => setVisible(false)}
        footer={null}
        centered
      >
        <Wrap>
          <Radio.Group
            onChange={e => setSelectedGal(e.target.value)}
            value={selectedGal}
          >
            {galleriesList.map(g => (
              <Radio
                key={g.id}
                value={g.id}
                style={{ display: 'block' }}
                disabled={g.enabled !== '1'}
              >
                {g.name}
              </Radio>
            ))}
          </Radio.Group>
        </Wrap>
        <Wrap>
          <Button
            onClick={() => {
              setVisible(false);
              setFotoRama({ id: selectedGal });
            }}
          >
            Добавить
          </Button>
        </Wrap>
      </Modal>
      <Button onClick={() => setVisible(true)}>Фотогалерея</Button>
    </>
  );
};

export default FotoRamaButton;
