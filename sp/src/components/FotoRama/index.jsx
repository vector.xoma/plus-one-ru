import React, { useState, useEffect, useContext } from 'react';
import _get from 'lodash/get';
import ImageGallery from 'react-image-gallery';
import { ThemeContext } from 'styled-components/macro';

import { Galleries } from 'connectors/query/Galleries';
import 'react-image-gallery/styles/scss/image-gallery.scss';

import {
  ContainerGallery,
  DescriptionIndex,
  DescriptionWrap,
  DescriptionLine,
  FullScreenIcon,
  ArrowRight,
  ArrowLeft,
} from './style';

import useDesktop from 'hooks/useDesktop';

const FotoRama = ({ data }) => {
  const [gallery, setGallery] = useState([]);
  const [slideIndex, setSlideIndex] = useState(0);
  const isDesktop = useDesktop();
  const themeContext = useContext(ThemeContext);
  const [fullScreenActive, setFullScreenActive] = useState(true);

  useEffect(() => {
    async function getGallery() {
      const result = await Galleries.getGallery(data.id);
      setGallery(result);
    }
    getGallery();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const images = gallery.map(g => ({
    original: g.filename,
    thumbnail: g.filename,
    author: g.image_author,
    descriptionName: g.image_name,
  }));

  const options = {
    showBullets: false,
    showPlayButton: false,
    infinite: true,
    thumbnailPosition: 'bottom',

    renderFullscreenButton: isDesktop
      ? (onClick, isFullscreen) => {
          setFullScreenActive(isFullscreen);
          return (
            <FullScreenIcon
              className={`image-gallery-fullscreen-button${
                isFullscreen ? ' active' : ''
              }`}
              onClick={onClick}
              isFullscreen={isFullscreen}
            />
          );
        }
      : () => false,
    renderLeftNav: isDesktop
      ? (onClick, disabled) => {
          return (
            <ArrowLeft
              isFullscreen={fullScreenActive}
              className="image-gallery-custom-left-nav"
              disabled={disabled}
              onClick={e => {
                onClick();
                e.preventDefault();
              }}
            />
          );
        }
      : () => false,
    renderRightNav: isDesktop
      ? (onClick, disabled) => {
          return (
            <ArrowRight
              isFullscreen={fullScreenActive}
              className="image-gallery-custom-right-nav"
              disabled={disabled}
              onClick={e => {
                onClick();
                e.preventDefault();
              }}
            />
          );
        }
      : () => false,
  };
  return (
    <ContainerGallery className="containerGallery">
      <ImageGallery onSlide={setSlideIndex} items={images} {...options} />
      <DescriptionWrap>
        <DescriptionLine themeContext={themeContext}>
          <DescriptionIndex themeContext={themeContext}>
            {slideIndex + 1} / {images.length}.&nbsp;
          </DescriptionIndex>
          {_get(images, `[${slideIndex}].descriptionName`)}
        </DescriptionLine>
        <DescriptionLine className="text--gray" themeContext={themeContext}>
          {_get(images, `[${slideIndex}].author`)}
        </DescriptionLine>
      </DescriptionWrap>
    </ContainerGallery>
  );
};

export default FotoRama;
