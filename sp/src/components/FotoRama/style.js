import styled from 'styled-components/macro';
import fotoramaArrowLeft from 'img/fotoramaArrowLeft.svg';
import fotoramaArrowRight from 'img/fotoramaArrowRight.svg';
import fotorama from 'img/fotorama.png';

const ContainerGallery = styled.div`
  margin: 20px 0 46px;
  overflow: hidden;
  padding: 0 38px;
  :hover .image-gallery-fullscreen-button {
    transform: translate(0);
    opacity: 1;
  }
  .image-gallery-slide {
    height: 525px;
    @media (max-width: 767px) {
      height: auto;
      /* min-height: 228px; */
      height: 300px;
    }

    div {
      height: 100%;
      img {
        object-fit: cover;
        display: block;
        height: 100%;
      }
    }
  }
  .image-gallery-thumbnails-wrapper {
    max-width: 755px;
    margin: 0 auto;
  }
  .image-gallery-content .image-gallery-slide .image-gallery-image {
    max-height: 100%;
  }
  .fullscreen .image-gallery-slide {
    height: calc(100vh - 80px);
  }
  .image-gallery-thumbnails {
    padding: 0;
  }
  .image-gallery-thumbnails .image-gallery-thumbnails-container {
    text-align: left;
  }
  .image-gallery-thumbnail-inner {
    width: 88px;
    height: 70px;
    box-sizing: border-box;
    margin: 0;
    @media (max-width: 767px) {
      width: 62px;
      height: 48px;
    }
    .image-gallery-thumbnail-image {
      width: 100%;
      height: 100%;
      object-fit: cover;
    }
  }
  .image-gallery-thumbnail.active,
  .image-gallery-thumbnail:hover,
  .image-gallery-thumbnail:focus {
    border: none;
    width: auto;
  }
  .image-gallery-thumbnail {
    width: auto;
    border: none;
    margin: 7px 0;
    margin-right: 7px;
  }
  .image-gallery-thumbnail.active {
    position: relative;
    ::after {
      content: '';
      position: absolute;
      left: 0;
      top: 0;
      width: 100%;
      height: 100%;
      display: block;
      border: 4px solid;
      border-color: ${({ theme }) =>
        theme.border_color && theme.border_color !== ''
          ? theme.border_color
          : '#fdbe0f'};
    }
  }
  @media (max-width: 767px) {
    padding: 0;
  }
`;
const Arrows = styled.button`
  position: absolute;
  top: 50%;
  width: 26px;
  height: 30px;
  display: block;
  background-repeat: no-repeat;
  background-color: transparent;
  transform: translateY(-50%);
  z-index: 50;
  border: none;
  outline: none;
  cursor: pointer;
`;
const ArrowLeft = styled(Arrows)`
  background-image: url(${fotoramaArrowLeft});
  left: ${({ isFullscreen }) => (isFullscreen ? '32px' : '-32px')};
`;
const ArrowRight = styled(Arrows)`
  right: ${({ isFullscreen }) => (isFullscreen ? '32px' : '-32px')};
  background-image: url(${fotoramaArrowRight});
`;

const FullScreenIcon = styled.button`
  &.image-gallery-fullscreen-button {
    position: absolute;
    right: 0;
    top: 0;
    width: 32px;
    height: 32px;
    transform: translate(32px, -32px);
    opacity: 0;
    transition: all ease 0.3s;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 0;
  }
  background-image: url(${fotorama});
  ${({ isFullscreen }) =>
    !isFullscreen
      ? { backgroundPosition: '0px -32px' }
      : { backgroundPosition: '-32px -32px' }};
  background-color: transparent;
`;

const DescriptionWrap = styled.div`
  min-height: 56px;
  max-width: 755px;
  margin: 0 auto;
`;
const DescriptionLine = styled.div`
  line-height: 1.54;
  min-height: 20px;
  font-size: 12px;
  font-family: 'helvetica', sans-serif;
  :first-child {
    color: ${({ themeContext, theme }) =>
      theme.font_color !== '' ? theme.font_color : themeContext.color};
  }

  &.text--gray {
    color: #929292;
    color: ${({ theme }) =>
      theme.font_color !== '' ? theme.font_color : '#929292'};
  }
`;
const DescriptionIndex = styled.span`
  display: inline-block;
  line-height: 1.54;
  vertical-align: baseline;
  color: ${({ themeContext, theme }) =>
    theme.font_color !== '' ? theme.font_color : themeContext.color};
`;

export {
  ContainerGallery,
  ArrowLeft,
  ArrowRight,
  FullScreenIcon,
  DescriptionWrap,
  DescriptionLine,
  DescriptionIndex,
};
