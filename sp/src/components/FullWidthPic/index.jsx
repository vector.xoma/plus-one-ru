import React, { useContext } from 'react';
import styled, { ThemeContext } from 'styled-components/macro';
import { clearText } from 'clearText';

const FullWidthPicContainer = styled.div`
  /* margin-bottom: ${({ marginBottom }) => (!marginBottom ? '14px' : '0')}; */
  margin-bottom: 30px;
`;
const FullWidthPicImgWrap = styled.div`
  width: 100%;
  height: auto;
  position: relative;

  @media (max-width: 767px) {
    width: calc(100% + 20px);
    margin: 0 -10px;
  }
`;

const FullWidthPicImg = styled.img`
  max-width: 100%;
`;

const FullWidthPicTextWrap = styled.div`
  max-width: 755px;
  margin: 14px auto;
`;

const FullWidthPicTextTitle = styled.div`
  margin: 5px 0 0;
  display: block;
  color: ${({ themeContext, isarticlepage, isAdminPage, theme }) => {
    if (theme.font_color && theme.font_color !== '') {
      return theme.font_color;
    }
    return isarticlepage || isAdminPage ? '#999' : themeContext.color;
  }};
  font-size: 12px;
  font-family: 'MullerRegular', sans-serif;
  @media (max-width: 767px) {
    font-size: 10px;
    color: ${({ themeContext, isarticlepage, isAdminPage, theme }) => {
      if (theme.font_color && theme.font_color !== '') {
        return theme.font_color;
      }
      return isarticlepage || isAdminPage ? '#000' : themeContext.color;
    }};
  }
`;

const FullWidthPicText = styled(FullWidthPicTextTitle)`
  margin: 5px 0 0;
  color: ${({ themeContext, isarticlepage, isAdminPage, theme }) => {
    if (theme.font_color !== '') {
      return theme.font_color;
    }
    return isarticlepage || isAdminPage ? '#999' : themeContext.color;
  }};
`;

const FullWidthDevider = styled.div`
  position: absolute;
`;

const FullWidthPic = ({ data, isarticlepage, isAdminPage }) => {
  const themeContext = useContext(ThemeContext);

  return (
    <FullWidthPicContainer
    // marginBottom={!data.source && data.source === 'null' && !data.title}
    >
      <FullWidthDevider />
      <FullWidthPicImgWrap>
        <FullWidthPicImg
          src={data.url}
          alt={data.title ? clearText(data.title) : ''}
          title={data.title ? clearText(data.title) : ''}
          className="full-width-pic_img"
        />
      </FullWidthPicImgWrap>
      <FullWidthPicTextWrap>
        {data.title && (
          <FullWidthPicTextTitle
            themeContext={themeContext}
            isarticlepage={isarticlepage}
            isAdminPage={isAdminPage}
          >
            {clearText(data.title)}
          </FullWidthPicTextTitle>
        )}
        {data.source && data.source !== 'null' && (
          <FullWidthPicText
            themeContext={themeContext}
            isarticlepage={isarticlepage}
            isAdminPage={isAdminPage}
          >
            {clearText(data.source)}
          </FullWidthPicText>
        )}
      </FullWidthPicTextWrap>
      <FullWidthDevider />
    </FullWidthPicContainer>
  );
};

export default FullWidthPic;
