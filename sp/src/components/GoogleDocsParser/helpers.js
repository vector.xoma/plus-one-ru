const style = {
  height: 300,
  maxHeight: 300,
  backgroundColor: '#fff',
  border: '1px solid #b5aaaa',
  borderRadius: 5,
  overflowY: 'auto',
  padding: '4px',
};

// Отдает отформатированный текст
const fn = {
  i: el => `[i]${el.textContent}[/i]`,
  b: el => `[b]${el.textContent}[/b]`,
  u: el => `[u]${el.textContent}[/u]`,
  br: () => '\n',
  a: el => `[alink url="${el.href}" blank="true"]${el.textContent}[/alink]`,
  ibu: el => `[i][b][u]${el.textContent}[/u][/b][/i]`,
  iu: el => `[i][u]${el.textContent}[/u][/i]`,
  ib: el => `[i][b]${el.textContent}[/b][/i]`,
  bu: el => `[b][u]${el.textContent}[/u][/b]`,
  // варианты с sup
  sup: el => `[sup]${el.textContent}[/sup]`,
  isup: el => `[sup][i]${el.textContent}[/i][/sup]`,
  bsup: el => `[sup][b]${el.textContent}[/b][/sup]`,
  usup: el => `[sup][u]${el.textContent}[/u][/sup]`,
  ibusup: el => `[sup][i][b][u]${el.textContent}[/u][/b][/i][/sup]`,
  iusup: el => `[sup][i][u]${el.textContent}[/u][/i][/sup]`,
  ibsup: el => `[sup][i][b]${el.textContent}[/b][/i][/sup]`,
  busup: el => `[sup][b][u]${el.textContent}[/u][/b][/sup]`,
  // варианты с sub
  sub: el => `[sub]${el.textContent}[/sub]`,
  isub: el => `[sub][i]${el.textContent}[/i][/sub]`,
  bsub: el => `[sub][b]${el.textContent}[/b][/sub]`,
  usub: el => `[sub][u]${el.textContent}[/u][/sub]`,
  ibusub: el => `[sub][i][b][u]${el.textContent}[/u][/b][/i][/sub]`,
  iusub: el => `[sub][i][u]${el.textContent}[/u][/i][/sub]`,
  ibsub: el => `[sub][i][b]${el.textContent}[/b][/i][/sub]`,
  busub: el => `[sub][b][u]${el.textContent}[/u][/b][/sub]`,
};

const cssCode = {
  i: 'font-style:italic',
  b: 'font-weight:700',
  u: 'text-decoration-line:underline',
  sub: 'vertical-align:sub',
  sup: 'vertical-align:super',
};

// Совпадения
const getMatch = (elStyle = '') => {
  const el = elStyle || '';
  const cleanSpace = el.replace(/\s/g, '');
  const iMatch = cleanSpace.includes(cssCode.i);
  const bMatch = cleanSpace.includes(cssCode.b);
  const uMatch = cleanSpace.includes(cssCode.u);
  const subMatch = cleanSpace.includes(cssCode.sub);
  const supMatch = cleanSpace.includes(cssCode.sup);
  return { iMatch, bMatch, uMatch, subMatch, supMatch };
};

// устанавливает переносы если это не явный br
const setLineBreak = ({ el, text }) => {
  if (!el) return text;
  const parent = el.parentElement;
  const lastEl = el.parentElement.lastElementChild;
  const nextEl = el => el.nextElementSibling;
  const parentMarginBtm = getComputedStyle(parent).marginBottom;
  const parentMarginTop = getComputedStyle(parent).marginTop;
  let { subMatch, supMatch } = getMatch(el.getAttribute('style'));
  // Кейс s sup И sub
  /*
    <p>
      <span>
        <span style:bold,italic...>CO</span>
        <span style:sub/sup>2</span>
      </span>
    </p>
  */
  if (subMatch || supMatch) {
    const wrap = el.parentElement.parentElement;
    const parent = el.parentElement;
    const lastEl = wrap.lastElementChild;
    if (lastEl === parent) {
      return `${text}${fn.br()}`;
    }
  }
  // В случае когда родитель P и после него идет не BR
  // устанавливать два переноса. Это кейс красной строки
  if (
    parent &&
    parent.tagName === 'P' &&
    el === lastEl &&
    parentMarginBtm !== '0' &&
    parentMarginTop !== '0' &&
    nextEl(parent) &&
    nextEl(parent).tagName !== 'BR'
  ) {
    return `${text}${fn.br()}${fn.br()}`;
  }

  if (parent && parent.tagName === 'P' && el === lastEl) {
    return `${text}${fn.br()}`;
  }

  if (nextEl(el) && nextEl(el).tagName === 'P') {
    return `${text}${fn.br()}`;
  }
  /* кейс
    <p>
      <a>
        <span>TXT</span>
      </a>
    </p>
  */
  if (
    el.tagName === 'SPAN' &&
    parent.tagName === 'A' &&
    parent.parentElement.tagName === 'P' &&
    parent.parentElement.lastElementChild === parent
  ) {
    return `${text}${fn.br()}`;
  }

  return text;
};

// устанавливает в зависимости от стиля - шорткод
const matchElem = (el, link = false) => {
  let shortCode = '';
  const elStyle = el.getAttribute('style');
  const returnValue = (el, fn) => {
    if (link) {
      return el.text;
    }
    return setLineBreak(el);
  };
  //Начало: кейс если содержимое эл-та пустая строка
  let spaceTxt =
    el.textContent === '\u00A0' || el.textContent === ' ' ? true : false;
  if (el.tagName === 'BR') return fn.br();
  if (spaceTxt) return el.textContent;
  // Конец
  if (elStyle) {
    let match = getMatch(elStyle);

    let isLink = false;
    // Если это ссылка, игнорируем underline
    if (el.tagName === 'A' || el.parentElement.tagName === 'A') {
      isLink = true;
    }
    // Начало: кейс
    // <span style: bold, italic, underline > <span style:sub/sup>TEXT</span> </span>
    if (
      (match.subMatch || match.supMatch) &&
      !match.iMatch &&
      !match.bMatch &&
      !match.uMatch
    ) {
      const parentStyle = el.parentElement.getAttribute('style');
      el.setAttribute('style', `${parentStyle}${elStyle}`);
      match = getMatch(el.getAttribute('style'));
    }
    // конец
    let { iMatch, bMatch, uMatch, subMatch, supMatch } = match;

    if (iMatch && bMatch && uMatch && !isLink) {
      // Больше одного совпадение i,b,u
      return returnValue({ el, text: fn.ibu(el) });
    }
    if (iMatch && uMatch && !isLink)
      return returnValue({ el, text: fn.iu(el) });
    if (bMatch && uMatch && !isLink)
      return returnValue({ el, text: fn.bu(el) });
    if (iMatch && bMatch) return returnValue({ el, text: fn.ib(el) });

    // Больше одного совпадение i,b,u, sub
    if (iMatch && bMatch && uMatch && subMatch && !isLink) {
      return returnValue({ el, text: fn.ibusub(el) });
    }
    if (iMatch && uMatch && subMatch && !isLink) {
      return returnValue({ el, text: fn.iusub(el) });
    }
    if (bMatch && uMatch && subMatch && !isLink) {
      return returnValue({ el, text: fn.busub(el) });
    }
    if (uMatch && subMatch && !isLink)
      return returnValue({ el, text: fn.usub(el) });
    if (iMatch && bMatch && subMatch)
      return returnValue({ el, text: fn.ibsub(el) });
    if (bMatch && subMatch) return returnValue({ el, text: fn.bsub(el) });
    if (iMatch && subMatch) return returnValue({ el, text: fn.isub(el) });

    // Больше одного совпадение i,b,u, sup
    if (iMatch && bMatch && uMatch && supMatch && !isLink) {
      return returnValue({ el, text: fn.ibusub(el) });
    }
    if (iMatch && uMatch && supMatch && !isLink) {
      return returnValue({ el, text: fn.iusup(el) });
    }
    if (bMatch && uMatch && supMatch && !isLink) {
      return returnValue({ el, text: fn.busup(el) });
    }
    if (uMatch && supMatch && !isLink)
      return returnValue({ el, text: fn.usup(el) });

    if (iMatch && bMatch && supMatch)
      return returnValue({ el, text: fn.ibsup(el) });
    if (bMatch && supMatch) return returnValue({ el, text: fn.bsup(el) });
    if (iMatch && supMatch) return returnValue({ el, text: fn.isup(el) });
    // по одному совпаднию
    if (iMatch || bMatch || uMatch || subMatch || supMatch) {
      shortCode = iMatch
        ? returnValue({ el, text: fn.i(el) })
        : bMatch
        ? returnValue({ el, text: fn.b(el) })
        : uMatch && !isLink
        ? returnValue({ el, text: fn.u(el) })
        : subMatch
        ? returnValue({ el, text: fn.sub(el) })
        : supMatch
        ? returnValue({ el, text: fn.sup(el) })
        : returnValue({ el, text: el.textContent });
      return shortCode;
    }
  }

  return returnValue({ el, text: el.textContent });
};

// Оборачивает ссыллкой
const linkWrap = ({ href, c, link = false }) => {
  if (link) {
    return setLineBreak(
      c,
      `[alink url="${href}" blank="true"]${matchElem(c, link)}[/alink]`,
    );
  }
  return `[alink url="${href}" blank="true"]${matchElem(c, link)}[/alink]`;
};

// TODO: отказались от Списков
// const listWrap = (el, text) => {
//   // кейс <ol><li><p><span><a>TXT</a></span></p></li></ol>
//   const findOL = el.closest('li') && el.closest('li').parentElement === 'OL';
//   const findUL = el.closest('li') && el.closest('li').parentElement === 'LL';
//   const ulList = `[listRows numeric="false" start="1"/][li]${text}[/li][/listRows]`;
//   const olList = `[listRows numeric="false" start="1"/][li]${text}[/li][/listRows]`;
//   if (findOL) return olList;
//   if (findUL) return ulList;
//   return text;
// };

// функция чистит переносы в конце готовой строки
const clearEndText = txt => {
  if (txt.slice(-1) !== '\n') return txt;
  return clearEndText(txt.substr(0, txt.length - 1));
};

export { matchElem, linkWrap, style, fn, clearEndText };
