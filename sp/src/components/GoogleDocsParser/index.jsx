import React, { useState, useRef } from 'react';
import { Button, Modal } from 'antd';

import { matchElem, linkWrap, style, fn, clearEndText } from './helpers';

const GoogleDocsParser = ({ setAction }) => {
  const [visible, setVisible] = useState(false);
  const [value, setValue] = useState(false);
  const wrapper = useRef();

  const getElem = el => {
    let text = '';
    let href = null;
    const test = [];
    console.log('start', el);
    const tree = [];
    const lookingForElem = el => {
      Array.prototype.forEach.call(el.children, c => {
        const isParentLink = c.parentElement.tagName === 'A';
        let spaceTxt = c.textContent === '\u00A0' || c.textContent === ' ';

        if (c.children.length > 0) {
          // строка свзяна с блоком => обработка вариантов с link
          if (c.tagName === 'A') href = c.href;
          // кейс для link
          // <span style=.../><a>txt<a/></span>
          if (c.tagName === 'SPAN' && c.getAttribute('style')) {
            const children = c.children[0];
            if (children.tagName === 'A') {
              text = linkWrap({ href: children.href, c, link: true });
              test.push(text);
              return;
            }
          }
          // конец кейса
          lookingForElem(c);
          return;
        }

        // обработка вариантов с link
        if (href) {
          // кейс если в <a><span style.../>txt</span></a>
          // первый эл-нт оказался пустым span
          if (spaceTxt) {
            test.push(c.textContent);
            href = null;
            return;
          }
          // кейс <a><span style.../>txt</span></a>
          text = linkWrap({ href, c });
          test.push(text);
          href = null;
          return;
        }

        if (c.tagName === 'A') {
          // кейс <a style... />txt</a>
          if (c.getAttribute('style')) {
            text = linkWrap({ href: el.href, c });
            test.push(text);
            href = null;
            return;
          }
          text += fn.a(c);
          return;
        }

        // Начало: когда в <a> лежит несколько детей, один из них тест, а остальные могут быть пустые span
        // в таком случае мы выше не оборачиваем пустые спаны в линки
        // далее падаем сюда и оборачиваем текст линком
        if (isParentLink && !spaceTxt) {
          text = linkWrap({
            href: c.parentElement.href,
            c,
          });
          test.push(text);
          return;
        }
        // конец
        text = matchElem(c);
        tree.push({ parent: c.parentElement, txt: text });
        test.push(text);
      });
    };
    lookingForElem(el);
    return test;
  };

  // TODO: Попытка удалить свдоенныешорткоды
  // Удалить, если откажутся от затеи
  // Нужно на моменте чтения гугл структуры формировать свою

  // const text = `asdasd[i][b]Все, что у нас осталось, [/b][/i][i][b]—[/b][/i][i][b] наши гости[/b][/i][b]. [/b]У нас очень много друзей`;
  // const text = `[i][b]Все, что у нас осталось, [/b][/i][i][b]—[/b][/i][i][b] наши гости[/b][/i][b].[/b]У нас очень много друзей. Мы продолжаем радовать их едой, а они[i][b], в свою очередь, охотно рекомендуют нас своим знакомым. Среди наших гостей нема[/b][/i]ло`;
  // const shortCodes = ["i", "b"];

  // const preparedText = (str, code) => {
  //   let next = false;
  //   const arr = str.split(`[/${code}]`);

  //   const z = arr.map((el, i) => {
  //     if (i === 0 && el.includes(`[${code}]`)) {
  //       next = true;
  //       return el;
  //     }
  //     if (el.slice(0, 3) === `[${code}]`) {
  //       return el.substr(3);
  //     }
  //     if (i !== arr.length - 1) return `[/${code}]${el}`;
  //     return `${el}[/${code}]`;
  //   });
  //   return z;
  // };

  // const clearDoubleShortCode = (text) => {
  //   let preparedStr;

  //   shortCodes.forEach((code) => {
  //     if (!preparedStr) {
  //       preparedStr = preparedText(text, code).join("");
  //       return;
  //     }

  //     preparedStr = preparedText(preparedStr, code);
  //     console.log("z", preparedStr);
  //     console.log("1---", preparedStr.join(""));
  //   });
  // };

  // clearDoubleShortCode(text);

  // ещё один вариант. Нужно несколько итерации для чистки
  // const text1 =
  //   "[i][b]Все, что у нас осталось, [/b][/i][i][b]—[/b][/i][i][b] наши гости[/b][/i][b]. [/b]У нас очень много друзей. Мы продолжаем радовать их едой, а они[i][b], в свою очередь, охотно рекомендуют нас своим знакомым. Среди наших [/b][/i][i][b][u]гостей нема[/u][/b][/i][u]ло изве[/u]стных людей, и с их стороны мы получаем большую поддержку. По сути, наши акции сейчас — это большая благодарность и поклон гостям. Хоть мы зарабатываем совсем не те деньги, что до пандемии, но в любом случае это лучше, чем ничего. Для нас очень важно, что мы остаемся в контакте с нашими завсегдатаями.";

  // const shortCodes1 = ["[/i][i]", "[/b][b]", "[/u][u]"];
  // const matrix = ["i", "b", "u"];
  // let newText;
  // const rep = (text1) => {
  //   shortCodes1.forEach((c) => {
  //     if (!newText) newText = text1;
  //     newText = newText.split(c).join("");
  //   });
  //   matrix.forEach((a) => {
  //     matrix.forEach((b) => {
  //       if (newText.includes(`[/${a}][/${b}][${a}]`)) {
  //         newText = newText.split(`[/${a}][/${b}][${a}]`).join(`[/${b}]`);
  //       }
  //       matrix.forEach((c) => {
  //         if (newText.includes(`[/${a}][/${b}][/${c}][${a}]`)) {
  //           newText = newText
  //             .split(`[/${a}][/${b}][/${c}][${a}]`)
  //             .join(`[/${b}][/${c}]`);
  //         }
  //       });
  //     });
  //   });
  // };

  // for (let i = 0; i < 10; i++) {
  //   rep(text1);
  // }

  // console.log(newText);

  // let newText;

  // function clearDoubleShortCode(shortCodes1, text) {
  //   shortCodes1.forEach(c => {
  //     if (text.includes(c)) {
  //       text = clearDoubleShortCode(shortCodes1, text.split(c).join(''));
  //     }
  //   });
  //   return text;
  // }

  // function handleDoubleShortCode(text) {
  //   const shortCodes1 = ['[/i][i]', '[/b][b]', '[/u][u]'];
  //   const matrix = ['i', 'b', 'u'];
  //   let newText;
  //   newText = clearDoubleShortCode(shortCodes1, text);
  //   // matrix.forEach(a => {
  //   //   matrix.forEach(b => {
  //   //     // кейс [/u][/i][u]
  //   //     if (newText.includes(`[/${a}][/${b}][${a}]`)) {
  //   //       newText = newText.split(`[/${a}][/${b}][${a}]`).join(`[/${b}]`);
  //   //     }
  //   //     matrix.forEach(c => {
  //   //       // кейс [/u][/b][/i][u]
  //   //       if (newText.includes(`[/${a}][/${b}][/${c}][${a}]`)) {
  //   //         newText = newText
  //   //           .split(`[/${a}][/${b}][/${c}][${a}]`)
  //   //           .join(`[/${b}][/${c}]`);
  //   //       }
  //   //     });
  //   //   });
  //   // });
  //   return newText;
  // }

  return (
    <>
      <Modal
        visible={visible}
        width={700}
        centered
        title="Вставьте текст"
        onOk={() => {
          setVisible(true);
          if (wrapper.current.textContent === '') return;
          setAction({ print: clearEndText(value) });
          setVisible(false);
          // Очистка содержимого
          setValue('');
          wrapper.current.innerHTML = '';
        }}
        onCancel={() => {
          setVisible(false);
          setVisible(false);
          // Очистка содержимого
          setValue('');
          wrapper.current.innerHTML = '';
        }}
        cancelText="Закрыть"
        okText="Добавить"
      >
        <div
          contentEditable
          ref={wrapper}
          style={style}
          onInput={e => {
            const el = e.target;
            let text = '';
            text = getElem(el).join('');
            setValue(text);
          }}
        ></div>
      </Modal>
      <Button onClick={() => setVisible(true)}>Вставить из Google Docs </Button>
    </>
  );
};

export default GoogleDocsParser;
