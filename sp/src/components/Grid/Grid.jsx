import styled, { css } from 'styled-components/macro';
import { Link } from 'react-router-dom';

export const Container = styled.div`
  display: flex;
  justify-content: ${({ isMainPage }) =>
    isMainPage ? 'flex-start' : 'space-between'};
  margin: 0 auto;
  max-width: 1140px;
  .post3,
  .post2 {
    ${({ isMainPage }) => (isMainPage ? { marginRight: '22px' } : {})};
    :last-child {
      ${({ isMainPage }) => (isMainPage ? { marginRight: '0' } : {})};
    }
  }
  @media (max-width: 767px) {
    max-width: 365px;
    flex-direction: column;
  }
`;
export const Grid1_1 = styled.div`
  width: 100%;
  position: relative;
`;

export const Grid1_2 = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  position: relative;
`;

export const Grid1_3 = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
`;

export const GridItem1 = styled.div`
  position: relative;
  width: 100%;
  min-height: 480px;
  padding: 30px 25px;
  margin-bottom: 20px;
  border-radius: 5px;
  transform: scale(1);
  transition: transform 1s;
  justify-content: space-between;

  :hover {
    transform: scale(1.01);
  }
`;

export const GridItem2 = styled.div`
  position: relative;
  width: 100%;
  max-width: 560px;
  min-height: 365px;
  margin-bottom: 20px;
  padding: 20px 30px 30px;
  border-radius: 5px;
  transform: scale(1);
  transition: transform 1s;
  justify-content: space-between;
  :hover {
    transform: scale(1.01);
  }
`;
export const GridItem3 = styled.div`
  position: relative;
  width: 100%;
  max-width: 365px;
  min-height: 505px;
  padding: 20px 30px 30px;
  margin-bottom: 20px;
  border-radius: 5px;
  transform: scale(1);
  transition: transform 1s;
  justify-content: space-between;
  :hover {
    transform: scale(1.01);
  }
`;

export const GridItemGroup = styled.div`
  position: relative;
  /* width: 100%; */
  margin: 0 10px;
  max-width: 365px;
  height: 410px;
  padding: 20px 30px 30px;
  border-radius: 5px;
  justify-content: space-between;
`;

// Компонент для кликабельности всего поста
const linkStyles = `
  z-index: 10;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  display: block;
  cursor: pointer;
  user-select: none;
  -webkit-user-drag: none;
  -webkit-user-select: none; /* для Chrome и Safari */
  -moz-user-select: none; /* для Firefox */
  -ms-user-select: none; /* для IE 10+ */
  -o-user-select: none;
  -khtml-user-select: none;
`;
export const AWrap = styled(Link)`
  ${linkStyles}
`;

export const AWrapOutside = styled.a`
  ${linkStyles}
`;

export const SlideLink = styled.div`
  z-index: 10;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  display: block;
  cursor: pointer;
`;

//------------- Обёртка для модалок в админке
export const Wrap = styled.div`
  width: 100%;
  margin: 5px 0;
`;
