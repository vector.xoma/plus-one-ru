import React, { useEffect, useRef } from 'react';

import { AutoCompleteItem } from './style';

const AutoCompleteElWrap = ({ setRefArrayList, ...props }) => {
  const refElement = useRef(null);

  useEffect(() => {
    setRefArrayList(prev => [...prev, refElement]);
    return () => setRefArrayList([]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <AutoCompleteItem ref={refElement} {...props}>
      {props.children}
    </AutoCompleteItem>
  );
};
export default AutoCompleteElWrap;
