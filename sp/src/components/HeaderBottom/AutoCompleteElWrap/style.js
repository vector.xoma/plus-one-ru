import styled from 'styled-components/macro';

const AutoCompleteItem = styled.div`
  padding-left: 57px;
  cursor: pointer;
  background: ${({ activeColor }) => activeColor};
  :first-child {
    margin-top: 22px;
  }
  :last-child {
    margin-bottom: 22px;
  }
  :hover {
    background: #242424;
  }
  @media (max-width: 767px) {
    padding-left: 20px;
    padding-right: 20px;
  }
`;

export { AutoCompleteItem };
