import React from 'react';

import { AutoCompleteLink } from './style';

const AutoCompleteElement = ({ title, ...props }) => {
  return <AutoCompleteLink {...props}>{title}</AutoCompleteLink>;
};
export default AutoCompleteElement;
