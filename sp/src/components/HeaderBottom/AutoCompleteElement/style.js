import styled from 'styled-components/macro';

const AutoCompleteLink = styled.div`
  width: 100%;
  display: block;
  line-height: 2.5;
  text-align: left;
  color: #fff;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  letter-spacing: normal;
  text-align: left;
  font-family: 'MullerRegular', sans-serif;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  :hover {
    color: #fff;
  }
`;

export { AutoCompleteLink };
