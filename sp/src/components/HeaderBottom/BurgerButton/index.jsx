import React from 'react';

import {
  BurgerItemThird,
  BurgerItemFirst,
  BurgerItemSecond,
  BurgerWrap,
} from './style';

const BurgerButton = ({
  articlePageStyles = {},
  activeAnimationHeader,
  animationstyles = {},
  customstyle = {},
  isarticlepage,
  isAboutPage,
  headerFixed,
  setVisible,
  isVisible,
  showed,
  isblackpage,
  burgerLeft,
}) => {
  return (
    <BurgerWrap
      activeAnimationHeader={activeAnimationHeader}
      articlePageStyles={articlePageStyles.btn}
      onClick={() => setVisible(!isVisible)}
      animationstyles={animationstyles.btn}
      isarticlepage={isarticlepage}
      headerFixed={headerFixed}
      showed={showed}
      burgerLeft={burgerLeft}
    >
      <BurgerItemFirst
        articlePageStyles={articlePageStyles.btn}
        isarticlepage={isarticlepage}
        customstyle={customstyle}
        isAboutPage={isAboutPage}
        isblackpage={isblackpage}
      />
      <BurgerItemSecond
        articlePageStyles={articlePageStyles.btn}
        isarticlepage={isarticlepage}
        customstyle={customstyle}
        isAboutPage={isAboutPage}
        isblackpage={isblackpage}
      />
      <BurgerItemThird
        articlePageStyles={articlePageStyles.btn}
        isarticlepage={isarticlepage}
        customstyle={customstyle}
        isAboutPage={isAboutPage}
        isblackpage={isblackpage}
      />
    </BurgerWrap>
  );
};

export default BurgerButton;
