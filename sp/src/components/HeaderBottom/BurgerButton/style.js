import styled from 'styled-components/macro';
import { Link } from 'react-router-dom';

const BurgerWrap = styled.div`
  width: 22px;
  height: 18px;
  position: relative;
  cursor: pointer;
  opacity: ${({ activeAnimationHeader }) =>
    activeAnimationHeader ? '0' : '1'};
  margin-right: ${({ isblackpage, burgerLeft }) =>
    !burgerLeft && !isblackpage && '20px'};
  transition: opacity 0.3s;
`;

const BurgerItem = styled.div`
  position: absolute;
  left: 0;
  width: 100%;
  height: 3px;
  background-color: ${({ isblackpage }) =>
    !isblackpage ? '#000' : 'rgba(255,255,255, 0.9)'};
  ${({ customstyle }) => customstyle};
  ${({ articlePageStyles }) => articlePageStyles}
`;

const BurgerItemFirst = styled(BurgerItem)`
  top: 1px;
`;
const BurgerItemSecond = styled(BurgerItem)`
  top: 50%;
  transform: translateY(-50%);
`;
const BurgerItemThird = styled(BurgerItem)`
  bottom: 1px;
`;

export { BurgerWrap, BurgerItemFirst, BurgerItemSecond, BurgerItemThird };
