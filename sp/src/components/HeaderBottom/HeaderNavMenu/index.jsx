import React from 'react';
import { Link } from 'react-router-dom';
import styled, { css } from 'styled-components/macro';

const HeaderItem = styled.div`
  display: flex;
  align-items: center;
  opacity: ${({ activeAnimationHeader }) =>
    activeAnimationHeader ? '0' : '1'};
  flex-basis: 50%;
  flex-grow: 1;
  justify-content: ${({ leftMenu }) => !leftMenu && 'flex-end'};
`;
const Styles = css`
  margin-right: 19px;
  font-family: 'MullerMedium';
  font-size: 13px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.11;
  letter-spacing: 0.27px;
  text-align: left;

  :hover {
    color: #9b9b9b;
  }
`;
const LinkRoute = styled(Link)`
  ${Styles}
   color: ${({ isblackpage }) => (isblackpage ? '#fff' : '#000')};
  :last-child{
    margin-right: 0;
  }
`;
const A = styled.a`
  color: ${({ isblackpage }) => (isblackpage ? '#fff' : '#000')};
  ${Styles}
  :last-of-type {
    margin-right: 26px;
  }
  :hover {
    color: #9b9b9b;
  }
`;

const HeaderNavMenu = ({
  data,
  isarticlepage,
  isblackpage,
  activeAnimationHeader,
  leftMenu,
}) => {
  return (
    <HeaderItem
      activeAnimationHeader={activeAnimationHeader}
      className="header-item"
      leftMenu={leftMenu}
    >
      {data.map((item, index) =>
        item.url.includes('http') ? (
          <A
            href={item.url}
            target="_blank"
            rel="noopener noreferrer"
            key={index}
            isblackpage={isblackpage}
          >
            {item.name}
          </A>
        ) : (
          <LinkRoute
            to={item.url}
            key={index}
            isarticlepage={isarticlepage}
            isblackpage={isblackpage}
          >
            {item.name}
          </LinkRoute>
        ),
      )}
    </HeaderItem>
  );
};

export default React.memo(HeaderNavMenu);
