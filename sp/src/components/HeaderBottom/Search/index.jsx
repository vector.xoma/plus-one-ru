import React from 'react';
import { Link } from 'react-router-dom';
import useDesktop from 'hooks/useDesktop';

import styled from 'styled-components/macro';
import logoSearchBlack from 'img/navSearchBlack.svg';
import logoSearch from 'img/navSearch.svg';
import BurgerButton from '../BurgerButton';
import HeaderNavMenu from '../HeaderNavMenu';

const HeaderItem = styled.div`
  display: flex;
  align-items: center;
  flex-basis: 50%;
  flex-grow: 1;
  justify-content: flex-start;
  @media (max-width: 767px) {
    flex-basis: auto;
    flex-grow: 0;
  }
`;
const A = styled.a`
  margin-right: 21px;
  font-family: 'MullerMedium';
  font-size: 13px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.11;
  letter-spacing: 0.27px;
  text-align: left;
  color: ${({ isblackpage }) => (isblackpage ? '#fff' : '#000')};
  :last-of-type {
    margin-right: 26px;
  }
  :hover {
    color: #9b9b9b;
  }
`;

const SearchWrap = styled.div`
  z-index: 20;
  position: relative;
  z-index: 21;
  transition: right 0.3s;
  right: ${({ showed }) => (!showed ? '30px' : '0')};
  transition: right 0.3s;
  ${({ customstyle }) => customstyle};
  width: 23px;
  height: 22px;
  ${({ activeAnimationHeader, animationstyles }) =>
    activeAnimationHeader ? animationstyles : { transform: 'translateY(0)' }};
  @media (max-width: 767px) {
    ${({ activeAnimationHeader }) =>
      activeAnimationHeader ? { right: '10px' } : { right: '0' }};
  }
`;

const SearchImg = styled.img`
  width: 100%;
  height: 100%;
  cursor: pointer;
  object-fit: contain;
  ${({ activeAnimationHeader, animationstyles }) =>
    activeAnimationHeader ? animationstyles : { transform: 'translateY(0)' }};
  opacity: ${({ isOpacity }) => (isOpacity ? '0.8' : '1')};
`;

const Search = ({
  setActiveAnimationHeader,
  articlePageStyles = {},
  activeAnimationHeader,
  animationstyles,
  isHeaderFixed,
  setSearchStr,
  setLinksList,
  customstyle,
  setToggle,
  isblackpage,
  showed,
  setVisible,
  data = { data },
}) => {
  const isDesktop = useDesktop();
  return (
    <HeaderItem>
      {isDesktop && (
        <HeaderNavMenu
          data={data}
          isblackpage={isblackpage}
          activeAnimationHeader={activeAnimationHeader}
        />
      )}
      {!isblackpage && isDesktop && (
        <BurgerButton
          isblackpage={isblackpage}
          setVisible={setVisible}
          activeAnimationHeader={activeAnimationHeader}
        />
      )}
      <div
        onClick={() => {
          setToggle(!showed);
          setLinksList([]);
          setSearchStr('');
          setActiveAnimationHeader(!activeAnimationHeader);
        }}
      >
        <SearchWrap
          showed={showed}
          customstyle={customstyle}
          animationstyles={animationstyles.input}
          activeAnimationHeader={activeAnimationHeader}
        >
          {isblackpage ? (
            <SearchImg
              className="SearchImg"
              src={logoSearch}
              isOpacity={!showed}
            />
          ) : articlePageStyles.logos && !showed ? (
            <SearchImg
              className="SearchImg"
              src={logoSearch}
              isOpacity={true}
            />
          ) : (
            <SearchImg
              className="SearchImg"
              src={isHeaderFixed && !showed ? logoSearch : logoSearchBlack}
              isOpacity={!showed}
            />
          )}
        </SearchWrap>
      </div>
    </HeaderItem>
  );
};

export default Search;
