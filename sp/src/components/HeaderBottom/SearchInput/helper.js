const validValue = (activeIndex, linkArrayList, searchStr) => {
  if (activeIndex < 0) {
    return searchStr;
  }
  if (linkArrayList.length > 0 && activeIndex <= linkArrayList.length - 1) {
    return linkArrayList[activeIndex];
  }
  return searchStr;
};

// В linksList лежит объект
const objToArray = arr => {
  const linksArray = [];
  for (let value in arr) {
    linksArray.push(arr[value]);
  }
  return linksArray;
};

const handlerMoveArrow = (
  refWrapperList,
  activeIndex,
  refArrayList,
  step,
  setStep,
) => {
  const refWrap = refWrapperList && refWrapperList.current;
  const offsetTop = refWrap && refWrap.getBoundingClientRect().top;
  const condition = refWrap && activeIndex >= 0;
  const currentEl =
    condition && refArrayList[activeIndex] && refArrayList[activeIndex].current;
  const rect = condition && currentEl.getBoundingClientRect();
  const offsetBottom =
    refWrap &&
    refWrap.getBoundingClientRect().top +
      refWrap.getBoundingClientRect().height;
  const marginCurrent = {
    top: currentEl && parseInt(getComputedStyle(currentEl).marginTop),
    btm: currentEl && parseInt(getComputedStyle(currentEl).marginBottom),
  };
  //  down
  if (condition && rect.top + rect.height >= offsetBottom) {
    refWrap.scrollTo({
      top: step + rect.height + marginCurrent.top + marginCurrent.btm,
      behavior: 'smooth',
    });
    setStep(step + rect.height + marginCurrent.top + marginCurrent.btm);
  }
  // up
  if (condition && rect.top <= offsetTop) {
    refWrap.scrollTo({
      top: step - (rect.height + marginCurrent.top),
      behavior: 'smooth',
    });

    setStep(step - (rect.height + marginCurrent.top));
  }
};

const arrowHandler = (
  e,
  activeIndex,
  setActiveIndex,
  setMoveArrow,
  linkArrayList,
) => {
  if (e.key === 'ArrowUp') {
    if (activeIndex > -1) {
      setActiveIndex(activeIndex - 1);
      setMoveArrow(true);
      return;
    }
  }

  if (e.key === 'ArrowDown') {
    if (activeIndex < linkArrayList.length - 1) {
      setActiveIndex(activeIndex + 1);
      setMoveArrow(true);
      return;
    }
  }
};

export { validValue, objToArray, handlerMoveArrow, arrowHandler };
