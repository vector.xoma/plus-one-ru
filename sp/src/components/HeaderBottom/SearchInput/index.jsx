import React, { useEffect, useState, useRef, forwardRef } from 'react';
import { useHistory } from 'react-router-dom';
import useDebounce from 'hooks/useDebounce';
import { Transition } from 'react-transition-group';
import { Search } from 'connectors/query/Search';
import {
  SearchInputWrapAnimateReverse,
  SearchInputWrapAnimate,
  AutoCompleteWrap,
  SearchInp,
} from './style';
import AutoCompleteElement from '../AutoCompleteElement';
import AutoCompleteElWrap from '../AutoCompleteElWrap';
import {
  handlerMoveArrow,
  arrowHandler,
  objToArray,
  validValue,
} from './helper';

const SearchInput = (
  {
    setActiveAnimationHeader,
    setIsVisibleResultList,
    activeAnimationHeader,
    isVisibleResultList,
    setPage = () => {},
    animationstyles,
    setRefArrayList,
    setActiveIndex,
    linksList = [],
    isHeaderFixed,
    searchStr = '',
    setLinksList,
    refArrayList,
    setSearchStr,
    headerFixed,
    activeIndex,
    setOldValue,
    setToggle,
    oldValue,
    showed,
  },
  ref,
) => {
  const debouncedValue = useDebounce(searchStr, 500);
  const history = useHistory();
  const [moveArrow, setMoveArrow] = useState(false);
  const [linkArrayList, setLinkArrayList] = useState([]);
  const [linkURL, setLinURL] = useState('/');
  const refWrapperList = useRef();
  const [step, setStep] = useState(0);

  useEffect(() => {
    if (debouncedValue.length > 1 && !moveArrow) {
      Search.getList({
        term: debouncedValue,
      }).then(res => {
        if (!res) return;
        setLinksList(res);
        setIsVisibleResultList(true);
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [debouncedValue]);

  useEffect(() => {
    if (!moveArrow) {
      setLinkArrayList(objToArray(linksList));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [moveArrow, linksList]);

  // устанавливаем значение value при нажатии на стрелки
  useEffect(() => {
    if (activeIndex !== -1 && linksList.length > 0) {
      setSearchStr(validValue(activeIndex, linkArrayList, searchStr).label);
      setLinURL(validValue(activeIndex, linkArrayList, searchStr).url);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [activeIndex]);

  // для скролла
  useEffect(() => {
    if (activeIndex === -1 && oldValue) {
      setSearchStr(oldValue);
      setMoveArrow(false);
      setLinURL('/');
    }
    handlerMoveArrow(refWrapperList, activeIndex, refArrayList, step, setStep);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [activeIndex, refWrapperList.current]);

  const handleEnter = e => {
    if (e.key === 'Enter') {
      if (!moveArrow) {
        history.push(`/search?${searchStr}`);
        setIsVisibleResultList(false);
        setPage(1);
        setToggle(showed); //Скрытие строки поиска
      }
      if (moveArrow) {
        history.push(`${linkURL}`);
        handleEnterAndClick();
      }
    }
  };

  const ComponentType = showed
    ? SearchInputWrapAnimate
    : SearchInputWrapAnimateReverse;
  // при клике по урлу или enter скидывать все данные
  const handleEnterAndClick = () => {
    setOldValue('');
    setSearchStr('');
    setLinkArrayList([]);
    setActiveIndex(-1);
    setStep(0);
    setLinksList([]);
    setToggle(true);
  };

  return (
    <Transition in={showed} timeout={280} unmountOnExit>
      <ComponentType headerFixed={headerFixed} ref={ref}>
        <SearchInp
          type="text"
          placeholder="Поиск"
          className="search__input"
          autoFocus={showed}
          onChange={e => {
            setSearchStr(e.target.value);
            if (activeIndex === -1) setOldValue(e.target.value);
            setMoveArrow(false);
            setStep(0);
            setActiveIndex(-1);
          }}
          onKeyDown={e => {
            handleEnter(e);
            arrowHandler(
              e,
              activeIndex,
              setActiveIndex,
              setMoveArrow,
              linkArrayList,
            );
          }}
          activeAnimationHeader={activeAnimationHeader}
          animationstyles={animationstyles.input}
          activeStyle={linkArrayList.length > 0}
          headerFixed={headerFixed}
          value={searchStr}
          isHeaderFixed
        />

        {isVisibleResultList && (
          <AutoCompleteWrap
            activeStyle={linkArrayList.length > 0}
            activeAnimationHeader={activeAnimationHeader}
            headerFixed={headerFixed}
            ref={refWrapperList}
          >
            {linkArrayList.length > 0 &&
              linkArrayList.map((l, i) => (
                <AutoCompleteElWrap
                  key={i}
                  onClick={() => {
                    setToggle(true);
                    isHeaderFixed &&
                      setActiveAnimationHeader(!activeAnimationHeader);
                    handleEnterAndClick();
                  }}
                  setRefArrayList={setRefArrayList}
                  activeColor={activeIndex === i ? '#242424' : ''}
                >
                  <AutoCompleteElement
                    onClick={() => history.push(`${l.url}`)}
                    setLinkArrayList={setLinkArrayList}
                    refWrapperList={refWrapperList}
                    title={l.label}
                  />
                </AutoCompleteElWrap>
              ))}
          </AutoCompleteWrap>
        )}
      </ComponentType>
    </Transition>
  );
};

export default forwardRef(SearchInput);
