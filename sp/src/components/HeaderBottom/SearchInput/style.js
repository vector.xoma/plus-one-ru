import styled, { keyframes } from 'styled-components/macro';

const inputAnimate = keyframes`
    from {
    width:20px;
  }
  to {
    width: 100%;
  }
`;

const inputAnimateMobile = keyframes`
    from {
    width:20px;
  }
  to {
    width: 100%;
  }
`;

const inputAnimateReverse = keyframes`
   from {
     width: 100%;
  }
  to {
    width: 0;
  }
`;

const inputAnimateReverseMobile = keyframes`
   from {
     width: 100%;
  }
  to {
    width: 0;
  }
`;

const SearchInputWrapAnimate = styled.div`
  position: absolute;
  right: 50%;
  top: 50%;
  transform: translate(50%, -50%);
  width: 100%;
  height: 64px;
  animation-name: ${inputAnimate};
  animation-duration: 0.3s;
  z-index: 20;
  box-sizing: border-box;
  @media (max-width: 767px) {
    animation-name: ${inputAnimateMobile};
    width: calc(100% + 10px);
    ${({ headerFixed }) => (headerFixed ? { height: '50px' } : {})};
  }
`;
const SearchInputWrapAnimateReverse = styled.div`
  position: absolute;
  right: 0;
  top: 50%;
  transform: translateY(-50%);
  width: 100%;
  height: 64px;
  animation-name: ${inputAnimateReverse};
  animation-duration: 0.3s;
  /*Ну вот прям ну очень надо что бы строка скрывалась*/
  display: none;
  /* ------------------------------------------------------- */
  @media (max-width: 767px) {
    animation-name: ${inputAnimateReverseMobile};
  }
`;

const SearchInp = styled.input`
  color: #fff;
  height: 100%;
  width: 100%;
  background-color: #000;
  border-radius: ${({ activeStyle }) => (activeStyle ? '5px 5px 0 0 ' : '5px')};
  padding-left: 57px;
  transition: all ease 0.3s;
  outline: none;
  border: none;
  font-family: 'MullerMedium';
  font-size: 30px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.3;
  letter-spacing: normal;
  text-align: left;
  ::placeholder {
    color: #9b9b9b;
  }

  ${({ activeAnimationHeader, animationstyles }) =>
    activeAnimationHeader ? animationstyles : { transform: 'translateY(0)' }};
  @media (max-width: 767px) {
    padding-left: 20px;
  }
`;

const AutoCompleteWrap = styled.div`
  width: 100%;
  max-height: 380px;
  background-color: #000;
  /* padding: 22px 0; */
  border-radius: 0 0 5px 5px;
  border-top: ${({ activeStyle }) =>
    activeStyle ? '1px solid #c5c5c5' : 'none'};
  /* overflow-y: ${({ activeStyle }) => (activeStyle ? 'scroll' : 'auto')}; */
  overflow-y: auto;
  box-sizing: border-box;
  transform: ${({ headerFixed }) =>
    headerFixed ? 'translateY(80px)' : 'translateY(0)'};
`;

export {
  inputAnimate,
  inputAnimateReverse,
  SearchInputWrapAnimate,
  SearchInputWrapAnimateReverse,
  SearchInp,
  AutoCompleteWrap,
};
