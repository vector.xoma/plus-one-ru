const itemsLeft = [
  { name: 'НОВОСТИ', url: '/news' },
  { name: 'СЮЖЕТЫ', url: '/story' },
  { name: 'ИНСТРУКЦИИ', url: '/manual' },
  { name: 'КОРОНАВИРУС', url: '/story/glavnoe-o-koronaviruse' },
];

const itemsRight = [
//  { name: 'О ПРОЕКТЕ', url: '/about', target: '_blank' },
  { name: '+1ПРЕМИЯ', url: 'https://award.plus-one.ru/', target: '_blank' },
  { name: 'РБК+1', url: 'http://plus-one.rbc.ru' },
  { name: 'ВЕДОМОСТИ+1', url: 'http://plus-one.vedomosti.ru' },
  { name: 'FORBES+1', url: 'http://plus-one.forbes.ru' },
];

export { itemsLeft, itemsRight };
