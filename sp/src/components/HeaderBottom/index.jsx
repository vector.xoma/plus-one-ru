import React, { useState, useRef, useEffect } from 'react';
import { Link } from 'react-router-dom';

import useOnclickOutside from 'react-cool-onclickoutside';
import BurgerButton from './BurgerButton';
import Search from './Search';
import SearchInput from './SearchInput/index';
import useDesktop from 'hooks/useDesktop';
import HeaderNavMenu from './HeaderNavMenu';
import { itemsLeft, itemsRight } from './helpers';

import {
  HeaderBottomContainer,
  HeaderBottomStyle,
  LogoWrap,
  Logo,
  Devider,
} from './style';
import logoPlusOne from 'img/plus_one_logo.svg';
import logoPlusOneBlack from 'img/plus_one_logoBlackFix.svg';

const HeaderBottom = ({
  setActiveAnimationHeader,
  articlePageStyles = {},
  activeAnimationHeader,
  animationstyles = {},
  isHeaderFixed,
  setHeaderRef,
  styles = {},
  isarticlepage,
  articleRubric,
  isAboutPage,
  headerFixed,
  isMainPage,
  setVisible,
  isblackpage,
  isVisible,
  setPage,
}) => {
  const [toggle, setToggle] = useState(true);
  const [linksList, setLinksList] = useState([]);
  const [searchStr, setSearchStr] = useState('');
  const [isVisibleResultList, setIsVisibleResultList] = useState(false);
  const [activeIndex, setActiveIndex] = useState(-1);
  const [refArrayList, setRefArrayList] = useState([]);
  const [oldValue, setOldValue] = useState('');
  const isDesktop = useDesktop();
  const refHeader = useRef(null);
  const registerRef = useRef(null);

  useEffect(() => {
    setHeaderRef && setHeaderRef(refHeader.current);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [refHeader]);

  useOnclickOutside(registerRef, e => {
    if (!e.target.classList.contains('SearchImg')) {
      setToggle(true);
      setLinksList([]);
      setSearchStr('');
      isHeaderFixed && setActiveAnimationHeader(false);
      setIsVisibleResultList(false);
      setActiveIndex(-1);
      setRefArrayList([]);
      setOldValue('');
    }
  });

  return (
    <HeaderBottomContainer headerFixed={headerFixed}>
      <HeaderBottomStyle
        customstyle={styles.headerWrap}
        isarticlepage={isarticlepage}
        headerFixed={headerFixed}
        ref={refHeader}
      >
        {isDesktop ? (
          <HeaderNavMenu
            isarticlepage={isarticlepage}
            data={itemsLeft}
            activeAnimationHeader={activeAnimationHeader}
            leftMenu
            isblackpage={isblackpage}
          />
        ) : (
          <BurgerButton
            setActiveAnimationHeader={setActiveAnimationHeader}
            activeAnimationHeader={activeAnimationHeader}
            articlePageStyles={articlePageStyles}
            animationstyles={animationstyles}
            isHeaderFixed={isHeaderFixed}
            isarticlepage={isarticlepage}
            customstyle={styles.line}
            isAboutPage={isAboutPage}
            headerFixed={headerFixed}
            setVisible={setVisible}
            isVisible={isVisible}
            showed={toggle}
            isblackpage={isblackpage}
            burgerLeft
          />
        )}
        <LogoWrap
          activeAnimationHeader={activeAnimationHeader}
          animationstyles={animationstyles.btn}
          customstyle={styles.logo}
          headerfixed={headerFixed}
        >
          <Link to="/">
            {isHeaderFixed || isarticlepage ? (
              <Logo src={logoPlusOneBlack} />
            ) : (
              <Logo src={logoPlusOne} />
            )}
          </Link>
        </LogoWrap>
        <Search
          setActiveAnimationHeader={setActiveAnimationHeader}
          activeAnimationHeader={activeAnimationHeader}
          articlePageStyles={articlePageStyles}
          animationstyles={animationstyles}
          customstyle={styles.logoSearch}
          isHeaderFixed={isHeaderFixed}
          setLinksList={setLinksList}
          setSearchStr={setSearchStr}
          isAboutPage={isAboutPage}
          isMainPage={isMainPage}
          setToggle={setToggle}
          isblackpage={isblackpage}
          showed={toggle}
          setVisible={setVisible}
          data={itemsRight}
        />
        <SearchInput
          setActiveAnimationHeader={setActiveAnimationHeader}
          setIsVisibleResultList={setIsVisibleResultList}
          activeAnimationHeader={activeAnimationHeader}
          isVisibleResultList={isVisibleResultList}
          setRefArrayList={setRefArrayList}
          animationstyles={animationstyles}
          setActiveIndex={setActiveIndex}
          articleRubric={articleRubric}
          isHeaderFixed={isHeaderFixed}
          setLinksList={setLinksList}
          setSearchStr={setSearchStr}
          refArrayList={refArrayList}
          headerFixed={headerFixed}
          activeIndex={activeIndex}
          setOldValue={setOldValue}
          setToggle={setToggle}
          linksList={linksList}
          searchStr={searchStr}
          oldValue={oldValue}
          ref={registerRef}
          setPage={setPage}
          showed={!toggle}
        />
      </HeaderBottomStyle>
      {!headerFixed && (
        <Devider
          isarticlepage={isarticlepage}
          activeAnimationHeader={activeAnimationHeader}
        />
      )}
    </HeaderBottomContainer>
  );
};

// export default forwardRef(HeaderBottom);
export default React.memo(HeaderBottom);
