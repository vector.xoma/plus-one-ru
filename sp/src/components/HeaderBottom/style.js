import styled from 'styled-components/macro';

const HeaderBottomContainer = styled.div`
  padding: ${({ headerFixed }) => (headerFixed ? '12px 0 0' : ' 18px 0 0')};
`;
const HeaderBottomStyle = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  max-width: 1140px;
  box-sizing: border-box;
  position: relative;
  ${({ customstyle }) => customstyle}
  padding: 0;
  margin: ${({ headerFixed }) =>
    headerFixed ? '0 auto 10px auto' : ' 0 auto 19px auto'};

  @media (max-width: 767px) {
    ${({ headerFixed }) =>
      headerFixed ? { margin: ' 0 20px 12px' } : { margin: ' 0 12px 10px' }};
  }
`;

const LogoWrap = styled.span`
  ${({ headerfixed }) =>
    headerfixed
      ? { minWidth: '80px', flexBasis: '80px', height: '41px' }
      : { minWidth: '102px', flexBasis: '102px', height: '52px' }}
  border: 0;
  ${({ activeAnimationHeader, animationstyles }) =>
    activeAnimationHeader ? animationstyles : { opacity: '1' }};

  /* ${({ customstyle }) => customstyle}; */
  :hover {
    border: 0;
  }
  @media (max-width: 767px) {
    ${({ headerfixed }) => headerfixed && { width: '66px', height: '35px' }}
  }
`;

const Logo = styled.img`
  width: 100%;
  height: 100%;
  display: block;
`;
const Devider = styled.div`
  width: 100%;
  height: 1px;
  background-color: ${({ isarticlepage }) =>
    isarticlepage ? '#d3d3d3' : '#242424'};
  opacity: ${({ activeAnimationHeader }) =>
    activeAnimationHeader ? '0' : '1'};
`;
export { HeaderBottomContainer, HeaderBottomStyle, LogoWrap, Logo, Devider };
