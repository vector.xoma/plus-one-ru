import React from 'react';
import { NavContainer, Nav, NavItem, LinkA, LinkRoute } from './style';

const nav = [
  { title: 'О проекте', src: '/about' },
  { title: 'Спецпроекты', src: '/special-projects' },
  { title: '+1Премия', src: 'https://award.plus-one.ru/' },
  { title: '+1CONF', src: 'https://conf.plus-one.ru/' },
  { title: '+1Город', src: 'https://xn--c1acbikjqegacu3l.xn--p1ai/' },
  { title: '+1Платформа', src: 'https://platform.plus-one.ru' },
  { title: '+1Люди', src: 'https://people.plus-one.ru/' },
];

const NavComponent = () => {
  return (
    <Nav>
      <NavContainer>
        {nav.map((elem, i) => {
          if (elem.src.includes('http')) {
            return (
              <NavItem key={i}>
                <LinkA
                  href={elem.src}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {elem.title}
                </LinkA>
              </NavItem>
            );
          }
          return (
            <NavItem key={i} className="nav-item">
              <LinkRoute to={elem.src} key={i} rel="noopener noreferrer">
                {elem.title}
              </LinkRoute>
            </NavItem>
          );
        })}
      </NavContainer>
    </Nav>
  );
};
export default NavComponent;
