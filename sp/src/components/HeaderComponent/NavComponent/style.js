import styled from 'styled-components/macro';
import { Link } from 'react-router-dom';

const NavContainer = styled.ul`
  display: flex;
  max-width: 1140px;
  margin: 0 auto;
  @media (max-width: 767px) {
    justify-content: space-between;
    padding: 2px 20px;
  }
`;
const Nav = styled.nav`
  @media (max-width: 767px) {
    width: 100%;
  }
`;
const NavItem = styled.li`
  margin-right: 20px;
  cursor: pointer;
  text-transform: uppercase;
  font-family: 'MullerMedium', sans-serif;
  font-size: 12px;
  line-height: normal;
  :last-child {
    margin-right: 0;
  }
  @media (max-width: 767px) {
    margin-right: 0;
    :first-child {
      display: none;
    }
    :nth-child(2) {
      display: none;
    }
    :nth-child(4) {
      display: none;
    }
  }
`;

const LinkA = styled.a`
  padding: 12px 0;
  display: block;
  color: #ffffff;
  border: none;
  @media (max-width: 767px) {
    font-size: 12px;
  }
  :hover {
    border: none;
    color: #ffffff;
  }
`;

const LinkRoute = styled(Link)`
  padding: 12px 0;
  display: block;
  color: #ffffff;
  border: none;
  :hover {
    border: none;
    color: #ffffff;
  }
`;

export { NavContainer, Nav, NavItem, LinkA, LinkRoute };
