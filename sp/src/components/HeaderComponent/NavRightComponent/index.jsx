import React from 'react';
import { Nav, NavContainer, NavItem, A } from './style';

const nav = [
  { title: 'РБК+1', src: 'http://plus-one.rbc.ru/' },
  { title: 'Forbes+1', src: 'http://plus-one.forbes.ru' },
  { title: 'Ведомости+1', src: 'http://plus-one.vedomosti.ru/' },
  { title: 'BFM+1', src: 'https://www.bfm.ru/special/visioners' },
  { title: 'fb', src: 'https://www.facebook.com/ProjectPlus1Official/' },
  { title: 'vk', src: 'https://vk.com/plus1ru' },
  { title: 'yt', src: 'https://www.youtube.com/c/Plusoneru' },
];

const NavRightComponent = () => {
  return (
    <Nav>
      <NavContainer>
        {nav.map((el, i) => {
          return (
            <NavItem key={i}>
              <A href={el.src} target="_blank">
                {el.title}
              </A>
            </NavItem>
          );
        })}
      </NavContainer>
    </Nav>
  );
};

export default NavRightComponent;
