import styled from 'styled-components/macro';

const Nav = styled.nav`
  @media (max-width: 767px) {
    display: none;
  }
`;
const NavContainer = styled.ul`
  display: flex;
  max-width: 1140px;
  margin: 0 auto;
`;

const NavItem = styled.li`
  position: relative;
  margin-right: 20px;
  cursor: pointer;
  text-transform: uppercase;
  font-family: 'MullerMedium', sans-serif;
  font-size: 12px;
  line-height: normal;
  :last-child {
    margin-right: 0;
  }
  ::after {
    position: absolute;
    content: '';
    left: 0;
    bottom: 7px;
    width: 100%;
    height: 2px;
  }
  :nth-child(1) {
    ::after {
      background-color: #02c418;
    }
  }
  :nth-child(2) {
    ::after {
      background-color: #00bcee;
    }
  }
  :nth-child(3) {
    ::after {
      background-color: #fff;
    }
  }
  :nth-child(4) {
    ::after {
      background-color: #b98b00;
    }
  }
  :nth-child(5) {
    margin-left: 20px;
    ::after {
      background-color: #575757;
      height: 20px;
      top: 50%;
      transform: translateY(-50%);
      width: 1px;
      left: -20px;
    }
  }
`;

const A = styled.a`
  padding: 12px 0;
  display: block;
  line-height: normal;
  list-style: none;
  color: #fff;
  :hover {
    color: #fff;
  }
`;

export { Nav, NavContainer, NavItem, A };
