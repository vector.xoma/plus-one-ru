import React, { useEffect, memo } from 'react';
import { Transition } from 'react-transition-group';
import HeaderBottom from '../HeaderBottom';
import { HeaderFixedWrap, styles } from './style';

const HeaderFixed = ({
  setActiveAnimationHeader,
  activeAnimationHeader,
  animationstyles = {},
  setVisible,
  visible,
}) => {
  useEffect(() => {
    if (!visible) {
      setActiveAnimationHeader(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [visible]);

  return (
    <Transition
      setVisible={setVisible}
      isVisible={visible}
      unmountOnExit
      timeout={280}
      in={visible}
      mountOnEnter
    >
      <HeaderFixedWrap
        animationstyles={animationstyles.headerWrap}
        activeAnimationHeader={activeAnimationHeader}
        className="headerFixedWrap"
        visible={visible}
      >
        <HeaderBottom
          setActiveAnimationHeader={setActiveAnimationHeader}
          activeAnimationHeader={activeAnimationHeader}
          animationstyles={animationstyles}
          setVisible={setVisible}
          styles={styles}
          isHeaderFixed
          isarticlepage={1}
          headerFixed
        />
      </HeaderFixedWrap>
    </Transition>
  );
};

export default memo(HeaderFixed);
