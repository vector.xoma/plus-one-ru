import styled, { keyframes } from 'styled-components/macro';

const headerDownAnimation = keyframes`
    from {
      top: -100%;
    }
    to {
      top:0;
    }
  `;

const headerUpAnimation = keyframes`
    from {
      top: 0;
    }
    to {
      top: -100%;
    }
  `;

const HeaderFixedWrap = styled.div`
  position: fixed;
  width: 100%;
  margin: 0 auto;
  z-index: 100;
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
  background-color: #eaeaea;
  transition: all ease 0.5s;
  animation: ${({ visible }) =>
      visible ? headerDownAnimation : headerUpAnimation}
    ease 0.3s;

  ${({ activeAnimationHeader, animationstyles }) =>
    activeAnimationHeader ? animationstyles : { background: '#eaeaea' }};
  top: ${({ visible }) => (visible ? '0' : '-100%')};
`;

const styles = {
  headerWrap: {
    padding: '10px 0',
  },
  burgerButton: {
    height: '25px',
    width: '37px',
    position: 'relative',
    transition: ' all 0.8s',
  },
  logoSearch: {
    width: '27px',
    height: '26px',
  },
  logo: {
    width: '79px',
    height: '40px',
  },
  line: {
    background: '#000',
  },
};

export { HeaderFixedWrap, styles };
