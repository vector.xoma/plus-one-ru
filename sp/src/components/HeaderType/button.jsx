import React, { useState } from 'react';
import { Menu, Popover, Button } from 'antd';

const headers = [
  { type: 1 },
  { type: 2 },
  { type: 3 },
  { type: 4 },
  { type: 5 },
  { type: 6 },
];

const HeaderType = ({ setHeaders }) => {
  const [visible, setVisible] = useState(false);

  const Content = (
    <Menu>
      {headers.map((elem, i) => (
        <Menu.Item
          onClick={() => {
            setHeaders({ type: elem.type });
            setVisible(false);
          }}
          key={i}
          style={{ width: 256 }}
        >
          {`Заголовок ${i + 1} уровня`}
        </Menu.Item>
      ))}
    </Menu>
  );

  return (
    <Popover
      content={Content}
      title="Заголовок"
      trigger="hover"
      placement="right"
      visible={visible}
      onVisibleChange={visible => {
        setVisible(visible);
      }}
    >
      <Button>Заголовок</Button>
    </Popover>
  );
};

export default HeaderType;
