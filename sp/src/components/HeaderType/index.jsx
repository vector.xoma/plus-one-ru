import React, { useContext } from 'react';
import styled, { ThemeContext } from 'styled-components/macro';

const HeaderTitleWrap = styled.span`
  line-height: 35px;
  font-weight: bold;
  font-family: 'MullerRegular', sans-serif;
  color: ${({ theme, themeContext = { color: '#fff' } }) =>
    theme.font_color ? theme.font_color : themeContext.color};
  display: block;
`;

const HeaderTitleWrap1 = styled(HeaderTitleWrap)`
  font-size: 40px;
  margin-top: 0;
  margin-bottom: 14px;
`;
const HeaderTitleWrap2 = styled(HeaderTitleWrap)`
  font-size: 26px;
  margin-top: 0;
  margin-bottom: 14px;
  font-family: 'MullerBold';
`;
const HeaderTitleWrap3 = styled(HeaderTitleWrap)`
  font-size: 23.4px;
  margin-top: 0;
  margin-bottom: 14px;
`;

const HeaderTitleWrap4 = styled(HeaderTitleWrap)`
  font-size: 20px;
  margin-top: 0;
  margin-bottom: 14px;
`;

const HeaderTitleWrap5 = styled(HeaderTitleWrap)`
  font-size: 16.6px;
  margin-top: 0;
  margin-bottom: 14px;
`;
const HeaderTitleWrap6 = styled(HeaderTitleWrap)`
  font-size: 13.4px;
  margin-top: 0;
  margin-bottom: 14px;
`;

const types = {
  1: HeaderTitleWrap1,
  2: HeaderTitleWrap2,
  3: HeaderTitleWrap3,
  4: HeaderTitleWrap4,
  5: HeaderTitleWrap5,
  6: HeaderTitleWrap6,
};

const HeaderType = ({ data }) => {
  const themeContext = useContext(ThemeContext);
  const HeaderTitleType = types[data.type] || HeaderTitleWrap6;
  // const headerTypeRef = useRef(null);
  // У обёртки TextAll margin: 28px;
  // useEffect(() => {
  //   if (
  //     headerTypeRef.current &&
  //     headerTypeRef.current.classList.contains('headerTypes')
  //   ) {
  //     headerTypeRef.current.parentNode.style.marginBottom = '0';
  //   }
  // }, []);
  // const className = `header-title header-title__${data.size}`;
  return (
    <HeaderTitleType
      // ref={headerTypeRef}
      className="headerTypes"
      themeContext={themeContext}
    >
      {data.text}
    </HeaderTitleType>
  );
};
export default HeaderType;
