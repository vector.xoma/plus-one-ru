import React from 'react';
import { useMenuData } from 'contexts/menuContext';
import _get from 'lodash/get';

import logos from './logos.jsx';

import {
  InformerText,
  InformerTitle,
  InformerNum,
  InformerDescription,
  InformerBlockOther,
  InformerContainer,
  InformerItem,
  InformerBlock,
  InformerLogoWrap,
} from './style';

const translate = {
  society: 'Общество',
  economy: 'Экономика',
  ecology: 'Экология',
  platform: 'Платформа',
};

const rightArr = ['ecology', 'society', 'economy', 'platform'];
const Informer = ({ styles = {} }) => {
  const { data: menuData } = useMenuData();
  const data = Object.values(_get(menuData, 'menu', {}));

  const sortData = rightArr.map(
    ra => data.length && Object.values(data).find(l => l.url && l.url === ra),
  );

  return (
    <InformerContainer customstyle={styles.container}>
      {sortData.map((item = {}, index) => (
        <InformerItem key={index} customstyle={styles.item}>
          <InformerBlock
            to={`/${item.url}`}
            customstyle={styles.block}
            activeClassName="activeElem"
          >
            <InformerLogoWrap>{logos[item.url]}</InformerLogoWrap>
            <InformerText>
              <InformerTitle>{translate[item.url]}</InformerTitle>
              <InformerNum>{_get(item, 'posts.cnt')}</InformerNum>
              <InformerDescription>
                {_get(item, 'posts.text')}
              </InformerDescription>
            </InformerText>
          </InformerBlock>
          {_get(item, 'postTags.cnt', 0) > 0 && (
            <InformerBlockOther
              to={`/tag/${item.url}`}
              activeClassName="activeElem"
            >
              <InformerNum>{item.postTags.cnt}</InformerNum>
              <InformerDescription>{item.postTags.text}</InformerDescription>
            </InformerBlockOther>
          )}
          {_get(item, 'leaders.cnt', 0) > 0 && (
            <InformerBlockOther
              to={
                item.url === 'platform'
                  ? `/authors/${item.url}`
                  : `/leaders/${item.url}`
              }
              activeClassName="activeElem"
            >
              <InformerNum>{item.leaders.cnt}</InformerNum>
              <InformerDescription>{item.leaders.text}</InformerDescription>
            </InformerBlockOther>
          )}
        </InformerItem>
      ))}
    </InformerContainer>
  );
};

export default Informer;
