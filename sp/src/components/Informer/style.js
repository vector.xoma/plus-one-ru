import styled from 'styled-components/macro';
import { NavLink } from 'react-router-dom';

const InformerContainer = styled.div`
  max-width: 1140px;
  margin: 34px auto 20px;
  display: flex;
  justify-content: space-between;
  cursor: pointer;
  @media (max-width: 767px) {
    ${({ customstyle }) => customstyle};
  }
`;
const InformerItem = styled.div`
  display: flex;
  justify-content: space-between;
  ${({ customstyle }) => customstyle}
`;

const InformerBlock = styled(NavLink)`
  display: flex;
  padding: 20px 15px;
  border-radius: 8px;
  transition: background-color ease 0.2s;
  align-items: flex-end;
  :first-child {
    ${({ customstyle }) => customstyle};
  }
  :hover {
    background-color: rgba(255, 255, 255, 0.2);
  }

  &.activeElem {
    background-color: rgba(255, 255, 255, 0.2);
  }
`;

const InformerLogoWrap = styled.div`
  width: 42px;
  height: 42px;
  margin-right: 12px;
  circle {
    fill: #fff;
  }
  path:first-child,
  path:first-child {
    fill: #fff;
  }
`;

const InformerText = styled.div`
  color: #fff;
`;
const InformerTitle = styled.div`
  font-size: 9px;
  font-family: 'MullerMedium';
  text-transform: uppercase;
  color: #fff;
  line-height: normal;
`;

const InformerNum = styled.div`
  font-size: 21px;
  font-family: 'MullerMedium';
  color: #fff;
  line-height: 1;
`;

const InformerDescription = styled.div`
  font-size: 9px;
  font-family: 'MullerMedium';
  text-transform: uppercase;
  color: #fff;
  line-height: 1.2;
`;

const InformerBlockOther = styled(NavLink)`
  padding: 20px 15px;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  cursor: pointer;
  border-radius: 8px;
  transition: background-color ease 0.2s;
  :hover {
    background-color: rgba(255, 255, 255, 0.2);
  }
  &.activeElem {
    background-color: rgba(255, 255, 255, 0.2);
  }
`;

export {
  InformerText,
  InformerTitle,
  InformerNum,
  InformerDescription,
  InformerBlockOther,
  InformerContainer,
  InformerItem,
  InformerBlock,
  InformerLogoWrap,
};
