import React, { useState } from 'react';
import { Popover, Button } from 'antd';
import styled from 'styled-components/macro';
import { Input } from 'antd';
import { Wrap } from '../Grid/Grid';

const InterviewWrap = styled.div`
  width: 500px;
`;
const InterviewButton = ({ setInterview }) => {
  const [visible, setVisible] = useState(false);
  const [question, setQuestion] = useState('');
  const [answer, setAnswer] = useState('');

  const Content = (
    <InterviewWrap>
      <Wrap>
        <Input
          placeholder="Вопрос"
          value={question || ''}
          onChange={e => setQuestion(e.target.value)}
        />
      </Wrap>
      <Wrap>
        <Input
          placeholder="Ответ"
          value={answer || ''}
          onChange={e => setAnswer(e.target.value)}
        />
      </Wrap>

      <Wrap>
        <Button
          onClick={() => {
            setInterview({ question, answer });
            setVisible(false);
            setQuestion('');
            setAnswer('');
          }}
        >
          Добавить
        </Button>
      </Wrap>
    </InterviewWrap>
  );

  return (
    <Popover
      content={Content}
      title="Интервью"
      trigger="click"
      placement="right"
      visible={visible}
      onVisibleChange={visible => {
        setVisible(visible);
      }}
    >
      <Button>Интервью</Button>
    </Popover>
  );
};

export default InterviewButton;
