import React, { useContext } from 'react';
import styled, { ThemeContext } from 'styled-components/macro';
import { parserCustomBBCode } from '../Text/parser';

const InterviewWrap = styled.div`
  max-width: 755px;
  margin: 0 auto;
  color: ${({ themeContext }) => themeContext.color};
  font-family: 'MullerRegular', sans-serif;
  line-height: 35px;
  position: relative;
`;

const InterviewTitle = styled.div`
  font-size: 26px;
  font-weight: bold;
  .textAll.TEXT-WORD {
    font-size: 26px;
    font-weight: bold;
    margin-bottom: 0;
    line-height: 35px;
  }
`;

const InterviewAuthor = styled.div`
  font-size: 20px;
  margin-bottom: 28px;
`;

const InterviewDevider = styled.div`
  position: absolute;
`;

const Interview = ({ data }) => {
  const themeContext = useContext(ThemeContext);
  return (
    <InterviewWrap themeContext={themeContext}>
      <InterviewDevider />
      <InterviewTitle themeContext={themeContext}>
        {parserCustomBBCode({ r: data.question }).component}
      </InterviewTitle>
      <InterviewAuthor themeContext={themeContext}>
        {parserCustomBBCode({ r: data.answer }).component}
      </InterviewAuthor>
      <InterviewDevider />
    </InterviewWrap>
  );
};

export default Interview;
