import React, { useState } from 'react';
import { Popover, Button } from 'antd';
import { Input } from 'antd';
import { splitBySelection } from 'utils';

const LinkButton = ({ setLink, textData }) => {
  const { middle } = splitBySelection(textData.text, textData.area);

  const [visible, setVisible] = useState(false);
  const [url, setUrl] = useState('');
  const [urlText, setUrlText] = useState(middle);

  const Content = (
    <div className="editorTextInsert">
      <div className="editorSign_name">
        <Input
          placeholder="Url картинки"
          value={url || ''}
          onChange={e => setUrl(e.target.value)}
        />
      </div>
      <div className="editorSign_name">
        <Input
          placeholder="Текст ссылки"
          value={urlText || ''}
          onChange={e => setUrlText(e.target.value)}
        />
      </div>
      <Button
        onClick={() => {
          setUrl('');
          setLink({ url, urlText });
          setVisible(false);
        }}
      >
        Добавить
      </Button>
    </div>
  );

  return (
    <Popover
      content={Content}
      title="Ссылка"
      trigger="click"
      placement="right"
      visible={visible}
      onVisibleChange={visible => {
        setVisible(visible);
      }}
    >
      <Button>Ссылка</Button>
    </Popover>
  );
};

export default LinkButton;
