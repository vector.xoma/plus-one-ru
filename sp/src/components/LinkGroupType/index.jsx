import React from 'react';
import { useHistory } from 'react-router-dom';
import { AWrap, AWrapOutside } from 'components/Grid/Grid';

const LinkGroupType = ({ checkBlank, setDragging, dragging, url }) => {
  const history = useHistory();
  return url.includes('http') ? (
    <AWrapOutside
      href={url}
      onDragStart={e => {
        e.preventDefault();
        setDragging(true);
      }}
      onClick={e => {
        e.preventDefault();
        setDragging(false);
        if (!dragging) {
          checkBlank('_blank', url, history);
        }
      }}
      draggable={false}
    />
  ) : (
    <AWrap
      to={url}
      onDragStart={e => {
        e.preventDefault();
        setDragging(true);
      }}
      onClick={e => {
        e.preventDefault();
        setDragging(false);
        if (!dragging) {
          checkBlank('_self', url, history);
        }
      }}
      draggable={false}
    />
  );
};

export default LinkGroupType;
