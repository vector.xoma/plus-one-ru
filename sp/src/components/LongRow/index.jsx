import React, { useRef } from 'react';
import _get from 'lodash/get';
import useScrollPosition from 'utils/hooks/useScroll';
import { clearText } from 'clearText';

import {
  ParallaxTxtW,
  ParallaxTxtBorder,
  ParallaxTxt,
  ParallaxTxtFlex,
  ParallaxRow,
} from './style';

const LongRow = ({ data = {} }) => {
  const { url, name } = data;
  let rightMove = 0;
  const rowRef = useRef(null);
  const textRef = useRef(null);

  // позиционирование строки по скроллу
  const scroll = useScrollPosition(rowRef);
  // ширина, насколько строка превышает размер окна
  const overflowX = textRef.current
    ? textRef.current.offsetWidth - window.innerWidth + 30
    : 0;
  // отступ слева от края экрана
  const leftSideMargin = rowRef.current ? rowRef.current.offsetLeft : 0;
  // высота блока
  const blockHeight = textRef.current ? textRef.current.offsetHeight + 50 : 0;
  // "рабочая" высота, где происходит анимация
  const workHeight = window.innerHeight - blockHeight;
  if (scroll && scroll.top >= 0 && scroll.top < workHeight) {
    const heightPercent = scroll.top / (workHeight / 100);
    rightMove = (100 - heightPercent) * (overflowX / 100) + leftSideMargin;
  }
  if (scroll && scroll.top < 30) rightMove = overflowX + leftSideMargin;
  if (scroll && scroll.top > workHeight) rightMove = leftSideMargin;

  return (
    <ParallaxTxtW ref={rowRef} href={url} target="_blank">
      <ParallaxTxtBorder className="ParallaxTxtBorder" />
      <ParallaxTxt style={{ right: `${rightMove}px` }}>
        <ParallaxTxtFlex>
          <ParallaxRow ref={textRef} color={_get(data, 'postFormat.fontColor')}>
            {clearText(name)}
          </ParallaxRow>
        </ParallaxTxtFlex>
      </ParallaxTxt>
    </ParallaxTxtW>
  );
};

export default LongRow;
