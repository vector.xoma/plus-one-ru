import styled from 'styled-components/macro';

const ParallaxTxtW = styled.a`
  position: relative;
  height: 148px;
  margin-bottom: 22px;
  margin-top: 30px;
  width: 100%;
  :hover .ParallaxTxtBorder {
    border-color: #fdbe0f;
  }
`;

const ParallaxTxtBorder = styled.div`
  position: absolute;
  left: 0;
  right: 0;
  bottom: 26px;
  border: 3px solid #fff;
  -webkit-transition: all 0.4s;
  transition: all 0.4s;
`;
const ParallaxTxt = styled.div`
  font-size: 136px;
  line-height: 100px;
  color: #fff;
  font-family: 'GeometricSansSerifv1';
  position: absolute;
  width: 100%;
  white-space: nowrap;
  position: absolute;
  text-transform: uppercase;
  transition: all 0.8s;
`;
const ParallaxTxtFlex = styled.div`
  display: flex;
`;
const ParallaxRow = styled.p`
  color: ${({ color }) => color};
`;

export {
  ParallaxTxtW,
  ParallaxTxtBorder,
  ParallaxTxt,
  ParallaxTxtFlex,
  ParallaxRow,
};
