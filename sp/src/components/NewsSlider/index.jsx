import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import Slider from 'react-slick';
import _isEmpty from 'lodash/isEmpty';

import useDesktop from 'hooks/useDesktop';
import { clearText } from 'clearText';
import { useNewsSliderData } from 'contexts/newsSliderContext';

import {
  CustomRightArrow,
  CustomLeftArrow,
  SliderContainer,
  SliderWrapper,
  SingleGroup,
  SlideItem,
  LinkTo,
  LinkToName,
} from './style';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

const SampleNextArrow = ({ onClick, className }) => {
  return <CustomRightArrow className={className} onClick={onClick} />;
};

const SamplePrevArrow = ({ onClick, className }) => {
  return <CustomLeftArrow className={className} onClick={onClick} />;
};

const NewsSlider = () => {
  const isDesktop = useDesktop();
  const history = useHistory();
  const { data, setData, getSliderData } = useNewsSliderData();
  // Необходимо стягивать данные, когда бек обновился.
  // контекст стягивает данные единожды
  // В идеале бы использовать webSocket
  useEffect(() => {
    // данные ранее были стянуты
    if (!_isEmpty(data)) getSliderData(setData);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const settings = {
    className: 'center',
    infinite: false,
    slidesToShow: 1,
    speed: 500,
    // waitForAnimate: false,  ---- Не дожидается окончания перелистывания слайда , работает хуже чем в slick jquery
    dots: true,
    swipeToSlide: true,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    beforeChange: () => setDragging(true),
    afterChange: () => setDragging(false),
    appendDots: dots => (
      <div
        style={{
          position: 'static',
          marginTop: '20px',
        }}
      >
        <ul style={{ margin: '0px' }}> {dots} </ul>
      </div>
    ),
    responsive: [
      {
        breakpoint: 767,
        settings: {
          arrows: false,
          swipe: true,
          centerMode: false,
          slidesToShow: 1,
          touchMove: true,
        },
      },
    ],
  };
  const [dragging, setDragging] = useState(false);

  return (
    <SliderWrapper>
      <SliderContainer>
        {!isDesktop && (
          <LinkToName to={`/news`}>
            <h2>Все Новости</h2>
          </LinkToName>
        )}
        {data.newsList && data.mobileNewsList ? (
          <Slider {...settings}>
            {isDesktop
              ? data.newsList.map((elem, index) => (
                  <SingleGroup key={index} className="slideGroup">
                    {elem.map((el, idx) => (
                      <SlideItem key={idx} className="slideItem">
                        {el.map((slide, i) => (
                          <LinkTo
                            onDragStart={e => {
                              e.preventDefault();
                              setDragging(true);
                            }}
                            key={i}
                            // https://github.com/styled-components/styled-components/issues/1198
                            dragging={dragging ? 1 : 0}
                            className={el.length === 1 ? 'firstSlide' : ''}
                            to={`/${slide.postUrl}`}
                            onClick={e => {
                              e.preventDefault();
                              setDragging(false);
                              if (!dragging) {
                                history.push(
                                  slide.postUrl[0] === '/'
                                    ? slide.postUrl
                                    : `/${slide.postUrl}`,
                                );
                              }
                            }}
                          >
                            <span>{clearText(slide.name)}</span>
                          </LinkTo>
                        ))}
                      </SlideItem>
                    ))}
                  </SingleGroup>
                ))
              : data.mobileNewsList.map((elem, index) => (
                  <SingleGroup key={index}>
                    {elem.map((el, i) => {
                      return (
                        <LinkTo
                          key={i}
                          to={`/${el.postUrl}`}
                          // onClick={() => history.push(el.postUrl)}
                        >
                          <span>{clearText(el.name)}</span>
                        </LinkTo>
                      );
                    })}
                  </SingleGroup>
                ))}
          </Slider>
        ) : null}
        {isDesktop && (
          <LinkToName to={`/news`}>
            <h2>Все Новости</h2>
          </LinkToName>
        )}
      </SliderContainer>
    </SliderWrapper>
  );
};

export default NewsSlider;
