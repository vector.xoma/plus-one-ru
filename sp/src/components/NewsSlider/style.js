import styled from 'styled-components/macro';
import { Link } from 'react-router-dom';
import leftArrowImg from 'img/arrowLeft.svg';
import rightArrowImg from 'img/arrowRight.svg';

const SliderWrapper = styled.div`
  margin: 20px auto 0;
  @media (max-width: 767px) {
    max-width: 100%;
    padding: 0 10px;
  }
`;

const SliderContainer = styled.div`
  max-width: 1140px;
  margin: 0 auto;
  margin-bottom: 20px;
  padding: 30px 33px;
  border-radius: 5px;
  position: relative;
  background-color: #eaeaea;
  box-sizing: border-box;

  .slick-arrow {
    /* opacity: 0; */
    transition: opacity ease 0.3s;
  }
  :hover .slick-arrow {
    opacity: 1;
  }
  h2 {
    /* position: absolute; */
    /* top: 30px;
    left: 33px; */
    font-family: 'MullerRegular', sans-serif;
    font-weight: 500;
    font-size: 13px;
    line-height: 1.2;
    text-transform: uppercase;
    text-decoration: none;
    color: #000;
    /* z-index: 10; */
  }
  .slick-dots {
    li {
      margin: 0;
      button {
        ::before {
          font-size: 8px;
        }
      }
    }
  }
  @media (max-width: 767px) {
    max-width: 365px;
  }
`;

const SingleGroup = styled.div`
  margin-top: 21px;
  justify-content: space-between;
  display: flex !important;
  @media (max-width: 767px) {
    flex-direction: column;
  }
`;

const SlideItem = styled.div`
  padding: 0 30px;
  flex: 1 1;

  :nth-child(2) {
    border-left: 1px solid #d3d3d3;
    border-right: 1px solid #d3d3d3;
  }
  :first-child {
    padding-left: 0;
  }
  :last-child {
    padding-right: 0;
  }
`;

const LinkTo = styled(Link)`
  cursor: pointer;
  text-decoration: none;
  font-family: 'MullerRegular', sans-serif;
  color: #000;
  line-height: 1.29;
  margin-bottom: 16px;
  display: block;
  user-select: none;
  -webkit-user-drag: none;
  font-size: 17px;
  pointer-events: ${({ dragging }) => (dragging ? 'none' : 'auto')};
  :focus {
    color: #000;
  }
  &.firstSlide {
    font-size: 28px;
  }
  :hover {
    color: #000;
  }
  :last-child {
    margin-bottom: 0;
  }

  @media (max-width: 767px) {
    margin-top: 10px;
    padding-bottom: 10px;
    font-size: 15px;
    border-bottom: 1px solid #d3d3d3;
  }
`;
const LinkToName = styled(LinkTo)`
  position: absolute;
  right: 33px;
  bottom: 25px;
  font-family: 'MullerMedium';
  font-size: 13px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.2;
  letter-spacing: 0.73px;
  text-align: left;
  color: #000000;
  h2 {
    font-family: inherit;
  }
  @media (max-width: 767px) {
    padding-bottom: 0;
    margin: 0;
    position: static;
  }
`;

const CustomArrows = styled.div`
  font-size: 0;
  line-height: 0;
  position: absolute;
  bottom: -8px;
  top: auto;
  display: block;
  width: 20px;
  height: 20px;
  padding: 0;
  cursor: pointer;
  color: transparent;
  border: none;
  outline: none;
  background: transparent;
`;

const CustomLeftArrow = styled(CustomArrows)`
  left: 40%;
  ::before {
    left: 40%;
    content: url(${leftArrowImg});
  }
`;

const CustomRightArrow = styled(CustomArrows)`
  right: 40%;
  ::before {
    right: 40%;
    content: url(${rightArrowImg});
  }
`;

export {
  SliderWrapper,
  SliderContainer,
  SingleGroup,
  SlideItem,
  // SlideLink,
  CustomLeftArrow,
  CustomRightArrow,
  LinkTo,
  LinkToName,
};
