import React from 'react';
import { Input, Button } from 'antd';
import styled from 'styled-components/macro';

const ListItemWrap = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 10px 0;
  border-radius: 5px;
  align-items: center;
  span {
    display: inline-block;
    margin-right: 10px;
  }
  .ant-btn {
    margin-left: 5px;
  }
`;

const ListInp = styled(Input)`
  padding: 5px 10px;
  border-radius: 5px;
  font-weight: 600;
`;
const ListItem = ({
  numericList,
  editItem,
  delItem,
  index,
  addItem,
  item,
  num,
}) => {
  return (
    <ListItemWrap>
      {numericList && <span>{num}</span>}
      <ListInp
        className="inp list-inp"
        onChange={e => editItem(index, e.target.value)}
        onKeyDown={({ key }) => {
          if (key === 'Enter') addItem();
        }}
        value={item.name}
        autoFocus={index !== 0 && true}
      />
      {index !== 0 && <Button onClick={() => delItem(index)}>Удалить</Button>}
    </ListItemWrap>
  );
};
export default ListItem;
