import React, { useState } from 'react';
import ListItem from './ListItem/';
// import "./NumberingListComponent.scss";
import { Popover, Button, Checkbox, Icon, Input } from 'antd';
import { Wrap } from '../Grid/Grid';
import styled from 'styled-components/macro';

const NumberingListWrap = styled.div`
  width: 500px;
`;

const NumberingListComponent = ({ setList }) => {
  const defaultItem = { id: 1, name: '' };
  const [items, setItems] = useState([defaultItem]);
  const [count, setCount] = useState(2);
  const [firstNum, setFirstNum] = useState('');
  const [numericList, setNumericList] = useState(false);
  const [visible, setVisible] = useState(false);

  const addItem = () => {
    const _items = items;
    _items.push({ id: count, name: '' });
    setItems(_items);
    setCount(count + 1);
  };

  const delItem = i => {
    const _items = items;
    _items.splice(i, 1);
    setItems([..._items]);
  };

  const editItem = (index, name) => {
    const _items = items.map((el, i) => {
      return i === index ? { id: el.id, name } : el;
    });
    setItems(_items);
  };

  const setNumberRow = i => (parseInt(firstNum, 10) || 1) + i;
  const Content = (
    <NumberingListWrap>
      <Checkbox
        onChange={() => setNumericList(!numericList)}
        checked={numericList}
      >
        Нумерация
      </Checkbox>
      <Button onClick={addItem}>Добавить элемент</Button>
      {numericList && (
        <Wrap>
          <Input
            placeholder="Стартовый номер"
            value={firstNum || ''}
            onChange={e => setFirstNum(e.target.value)}
          />
        </Wrap>
      )}
      {items.map((el, i) => (
        <ListItem
          key={el.id}
          item={el}
          index={i}
          num={setNumberRow(i)}
          delItem={delItem}
          numericList={numericList}
          editItem={editItem}
          addItem={addItem}
        />
      ))}
      <Button
        onClick={() => {
          setList({
            elements: items,
            numeric: numericList,
            firstNum: firstNum || 1,
          });
          setVisible(false);
          setItems([defaultItem]);
        }}
      >
        Добавить на страницу
      </Button>
    </NumberingListWrap>
  );
  return (
    <Popover
      content={Content}
      title="Список"
      trigger="click"
      placement="right"
      visible={visible}
      onVisibleChange={visible => {
        setVisible(visible);
      }}
    >
      <Button>
        <Icon type="ordered-list" />
      </Button>
    </Popover>
  );
};

export default NumberingListComponent;
