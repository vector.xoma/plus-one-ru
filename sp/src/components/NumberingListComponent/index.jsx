import React, { useContext } from 'react';
import styled, { ThemeContext } from 'styled-components/macro';
import { parserCustomBBCode } from '../Text/parser';

const NumListWrap = styled.div`
  margin: 28px 0;
`;
const NumericContainer = styled.div`
  display: flex;
  max-width: 755px;
  margin: 0 auto;
  font-family: 'MullerRegular', sans-serif;
  font-size: 20px;
  margin: 16px auto;
  color: #000000;
  position: relative;
  padding-left: 29px;
  margin: 18px auto;
  .textAll.TEXT-WORD {
    margin-bottom: 0;
  }
`;

const Num = styled.div`
  font-family: 'MullerRegular', sans-serif;
  color: #fdbe0f;
  font-size: 20px;
  text-align: left;
  line-height: normal;
  position: absolute;
  top: 5px;
  left: 0;
  @media (max-width: 767px) {
    top: 3px;
  }
`;

const UnNum = styled.div`
  background-color: #fdbe0f;
  width: 7px;
  height: 7px;
  display: block;
  align-self: center;
  margin-right: 29px;
  border-radius: 50%;
  position: absolute;
  top: 10px;
  left: 0;
  @media (max-width: 767px) {
    top: 8px;
  }
`;

const ListTitle = styled.div`
  color: ${({ themeContext }) => themeContext.color};
  line-height: 1.54;
  &.numeric {
    margin-left: 5px;
  }
  .textAll {
    min-height: auto;
    margin-bottom: 0;
  }
`;

const ListDevider = styled.div`
  position: absolute;
`;

const List = ({ data }) => {
  const themeContext = useContext(ThemeContext);

  return (
    <NumListWrap>
      <ListDevider />
      {data.elements &&
        data.elements.map((el, index) => (
          <NumericContainer key={index}>
            {data.numeric ? (
              <Num>{index + parseInt(data.firstNum, 10)}</Num>
            ) : (
              <UnNum />
            )}
            <ListTitle
              themeContext={themeContext}
              className={data.numeric && 'numeric'}
            >
              {parserCustomBBCode({ r: el }).component}
            </ListTitle>
          </NumericContainer>
        ))}
      <ListDevider />
    </NumListWrap>
  );
};

export default List;
