import React from 'react';
import styled from 'styled-components/macro';

const Img = styled.img`
  max-width: 100%;
  object-fit: contain;
`;

const ParsedImg = ({ imgUrl }) => <Img src={imgUrl} />;

export default ParsedImg;
