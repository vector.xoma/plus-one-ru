import React, { useState } from 'react';
import styled from 'styled-components/macro';
import { Popover, Button, Input, Checkbox } from 'antd';

import FileManager from 'containers/FileManager';
import { Wrap } from '../Grid/Grid';
import { BtnFindImg, FilesControl } from '../_styles/buttonsStyles';

const { TextArea } = Input;

const PicRetraction = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  textarea {
    width: 500px;
  }
`;

const PicRetractionButton = ({ setPicRetraction }) => {
  const [visible, setVisible] = useState(false);
  const [showFM, setShowFM] = useState(false);
  const [title, setTitle] = useState('');
  const [url, setUrl] = useState('');
  const [text, setText] = useState('');
  const [link, setLink] = useState('');
  const [targetBlank, setTargetBlank] = useState(false);

  const Content = (
    <PicRetraction>
      <FilesControl>
        <Input
          placeholder="Url картинки"
          value={url || ''}
          onChange={e => setUrl(e.target.value)}
        />
        <BtnFindImg onClick={() => setShowFM(true)}></BtnFindImg>
      </FilesControl>
      <Wrap>
        <Input
          placeholder="Заголовок"
          value={title || ''}
          onChange={e => setTitle(e.target.value)}
        />
      </Wrap>
      <Wrap>
        <TextArea
          placeholder="Подзаголовок"
          value={text || ''}
          rows={4}
          onChange={e => setText(e.target.value)}
        />
      </Wrap>
      <Wrap>
        <Input
          placeholder="Ссылка"
          value={link || ''}
          onChange={e => setLink(e.target.value)}
        />
      </Wrap>
      <Wrap>
        <Checkbox
          checked={targetBlank}
          onChange={e => setTargetBlank(e.target.checked)}
        >
          В новом окне
        </Checkbox>
      </Wrap>

      <Wrap>
        <Button
          onClick={() => {
            setPicRetraction({ url, title, textPic: text, link, targetBlank });
            setUrl('');
            setText('');
            setTitle('');
            setLink('');
            setTargetBlank(false);
            setVisible(false);
          }}
        >
          Добавить
        </Button>
      </Wrap>
    </PicRetraction>
  );

  return (
    <>
      <Popover
        content={Content}
        title="Картинка втяжка"
        trigger="click"
        placement="right"
        visible={visible}
        onVisibleChange={(visible, a, b) => {
          if (!showFM) setVisible(visible);
        }}
      >
        <Button>Картинка втяжка</Button>
      </Popover>

      <FileManager
        visible={showFM}
        setPicUrl={setUrl}
        hideModal={() => setShowFM(false)}
      />
    </>
  );
};

export default PicRetractionButton;
