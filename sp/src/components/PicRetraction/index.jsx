import React from 'react';
import { Link } from 'react-router-dom';
import { parserCustomBBCode } from '../Text/parser';
import useDesktop from 'hooks/useDesktop';
import { clearText } from 'clearText';
import {
  PicRetractionWrap,
  PicRetractionImgWrap,
  PicRetractionImg,
  PicRetractionTextWrap,
  PicRetractionTitle,
  PicRetractionText,
  PicRetractionDivider,
} from './style';

const PicRetraction = ({ data }) => {
  const rows = data.textPic.split('\n');
  const isDesktop = useDesktop();

  // Забираем только pathname из data.link что бы не завязываться на location
  const linkString = () => {
    try {
      return new URL(data.link).pathname;
    } catch (e) {
      return data.link;
    }
  };

  const validTitle = data.title.replace(/\[[\S\s ]+?\]/gi, '');

  const PicRetract = () => (
    <>
      <PicRetractionDivider />
      {isDesktop && (
        <PicRetractionImgWrap>
          <PicRetractionImg
            src={data.url}
            alt={data.title ? clearText(validTitle) : ''}
            title={data.title ? clearText(validTitle) : ''}
          />
        </PicRetractionImgWrap>
      )}
      <PicRetractionTextWrap>
        <PicRetractionTitle>
          {parserCustomBBCode({ r: data.title }).component}
        </PicRetractionTitle>
        {rows.length ? (
          <PicRetractionText>
            {rows.map(
              (r, i) =>
                r &&
                r !== 'null' && (
                  <p key={i}>{parserCustomBBCode({ r }).component}</p>
                ),
            )}
          </PicRetractionText>
        ) : (
          <PicRetractionText>
            {parserCustomBBCode({ r: data.textPic }).component}
          </PicRetractionText>
        )}
      </PicRetractionTextWrap>
      <PicRetractionDivider />
    </>
  );

  return (
    <PicRetractionWrap>
      {data.targetBlank === 'true' ? (
        <a
          href={data.link}
          target="_blank"
          className="pic-retraction_wrap"
          rel="noopener noreferrer"
        >
          <PicRetract />
        </a>
      ) : (
        <Link to={linkString()} className="pic-retraction_wrap">
          <PicRetract />
        </Link>
      )}
    </PicRetractionWrap>
  );
};

export default PicRetraction;
