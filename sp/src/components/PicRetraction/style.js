import styled from 'styled-components/macro';

const PicRetractionWrap = styled.div`
  max-width: 755px;
  margin: 0 auto 35px;
  display: flex;
  min-height: 128px;
  background-color: #ffffff;
  position: relative;
  .pic-retraction_wrap {
    display: flex;
    width: 100%;
  }
`;

const PicRetractionImgWrap = styled.div`
  max-width: 175px;
  width: 100%;
`;

const PicRetractionImg = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
  display: block;
`;

const PicRetractionTextWrap = styled.div`
  font-family: 'MullerBold', sans-serif;
  border: none;
  font-style: normal;
  padding: 20px 47px 30px 40px;
  margin: 0;
  color: #000;
  p {
    color: #000;
  }
  .textAll.TEXT-WORD {
    margin-bottom: 0;
  }
`;

const PicRetractionTitle = styled.div`
  .textAll {
    margin-bottom: 0;
    font-family: 'MullerBold', sans-serif;
    font-size: 19px;
    line-height: 24px;
    color: #000 !important;
    a {
      color: #000;
    }
  }
  a {
    color: #000;
  }
`;

const PicRetractionText = styled.div`
  p {
    margin-bottom: 0;
  }
  .textAll {
    margin-top: 5px;
    margin-bottom: 0;
    font-size: 15px;
    line-height: 20px;
    font-family: 'MullerRegular', sans-serif;
    color: #000 !important;
    a {
      color: #000;
    }
  }
`;
const PicRetractionDivider = styled.div`
  position: absolute;
`;

export {
  PicRetractionWrap,
  PicRetractionImgWrap,
  PicRetractionImg,
  PicRetractionTextWrap,
  PicRetractionTitle,
  PicRetractionText,
  PicRetractionDivider,
};
