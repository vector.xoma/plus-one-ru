import React, { useState } from 'react';
import { Popover, Button, Input, Icon } from 'antd';
import styled from 'styled-components/macro';

import FileManager from 'containers/FileManager';
import { BtnFindImg, FilesControl } from '../_styles/buttonsStyles';
import { Wrap } from '../Grid/Grid';

const EditorTextInsert = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 500px;
  div {
    margin: 5px 0;
  }
`;

const PictureButton = ({ setPicture }) => {
  const [visible, setVisible] = useState(false);
  const [showFM, setShowFM] = useState(false);
  const [url, setUrl] = useState('');
  const [title, setTitle] = useState('');
  const [source, setSource] = useState('');

  const Content = (
    <EditorTextInsert className="editorTextInsert">
      <Wrap>
        <FilesControl>
          <Input
            placeholder="Url картинки"
            value={url || ''}
            onChange={e => setUrl(e.target.value)}
          />
          <BtnFindImg onClick={() => setShowFM(true)}></BtnFindImg>
        </FilesControl>
        <Wrap>
          <Input
            placeholder="Описание фото"
            value={title || ''}
            onChange={e => setTitle(e.target.value)}
          />
        </Wrap>
        <Wrap>
          <Input
            placeholder="Источник фото"
            value={source || ''}
            onChange={e => setSource(e.target.value)}
          />
        </Wrap>

        <Button
          onClick={() => {
            setPicture({ url, title, source });
            setVisible(false);
            setUrl('');
            setSource('');
            setTitle('');
          }}
        >
          Добавить
        </Button>
      </Wrap>
    </EditorTextInsert>
  );

  return (
    <>
      <Popover
        content={Content}
        title="Картинка"
        trigger="click"
        placement="right"
        visible={visible}
        onVisibleChange={visible => {
          if (!showFM) setVisible(visible);
        }}
      >
        <Button>
          <Icon type="picture" />
        </Button>
      </Popover>

      <FileManager
        visible={showFM}
        setPicUrl={setUrl}
        hideModal={() => setShowFM(false)}
      />
    </>
  );
};

export default PictureButton;
