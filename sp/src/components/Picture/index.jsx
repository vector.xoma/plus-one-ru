import React, { useContext } from 'react';
import styled, { ThemeContext } from 'styled-components/macro';
import { clearText } from 'clearText';

const FullWidthPicContainer = styled.div`
  margin-bottom: 30px;
`;
const FullWidthPicImgWrap = styled.div`
  width: 100%;
  height: auto;
  position: relative;
  @media (max-width: 767px) {
    width: calc(100% + 20px);
    margin: 0 -10px;
  }
`;

const FullWidthPicImg = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

const FullWidthPicTextWrap = styled.div`
  max-width: 755px;
  margin: 0 auto;
`;

const FullWidthPicTextTitle = styled.div`
  display: block;
  color: ${({ themeContext, isarticlepage, isAdminPage, theme }) => {
    if (theme.font_color && theme.font_color !== '') {
      return theme.font_color;
    }
    return isarticlepage || isAdminPage ? '#999' : themeContext.color;
  }};
  font-size: 12px;
  font-family: 'MullerRegular', sans-serif;
  @media (max-width: 767px) {
    font-size: 10px;
    color: ${({ themeContext, isarticlepage, isAdminPage, theme }) => {
      if (theme.font_color && theme.font_color !== '') {
        return theme.font_color;
      }
      return isarticlepage || isAdminPage ? '#000' : themeContext.color;
    }};
  }
`;

const FullWidthPicText = styled(FullWidthPicTextTitle)``;

const FullWidthDevider = styled.div`
  position: absolute;
`;

const Picture = ({ data, isarticlepage }) => {
  // TODO: переписать под обычную картинку
  const themeContext = useContext(ThemeContext);
  return (
    <FullWidthPicContainer>
      <FullWidthDevider />
      <FullWidthPicImgWrap>
        <FullWidthPicImg src={data.url} alt="неверный url" />
      </FullWidthPicImgWrap>
      <FullWidthPicTextWrap>
        {data.title && (
          <FullWidthPicTextTitle
            themeContext={themeContext}
            isarticlepage={isarticlepage}
          >
            {clearText(data.title)}
          </FullWidthPicTextTitle>
        )}
        <FullWidthPicText themeContext={themeContext}>
          {clearText(data.source)}
        </FullWidthPicText>
      </FullWidthPicTextWrap>
      <FullWidthDevider />
    </FullWidthPicContainer>
  );
};

export default Picture;
