import React from 'react';
import { LinkRoute, LinkInitialRoute } from './style';

const PreparedLink = ({
  url = '',
  target = '_blank',
  borderColor = '',
  title = '',
  style = {},
}) => {
  return url.includes('http') ? (
    <LinkInitialRoute
      target={target}
      href={url[0] === '/' ? url.slice(1) : url}
      bordercolor={borderColor}
      style={style}
    >
      {title}
    </LinkInitialRoute>
  ) : (
    <LinkRoute
      target={target}
      bordercolor={borderColor}
      to={url[0] === '/' ? url : `/${url}`}
      style={style}
    >
      {title}
    </LinkRoute>
  );
};

export default PreparedLink;
