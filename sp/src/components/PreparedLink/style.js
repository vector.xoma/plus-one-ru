import styled from 'styled-components/macro';
import { Link } from 'react-router-dom';

const linkStyle = `
  display: inline-block;
  opacity: 0.85;
  transition: all 0.4s;
  margin-bottom: 16px;
  color: #fff;
  font-size: 16px;
  font-family: 'MullerMedium';
  line-height: normal;
  position: relative;
  ::after {
    content: '';
    position: absolute;
    width: 100%;
    height: 2px;
    bottom: -5px;
    left: 0;
    background-color: ${({ bordercolor }) =>
      bordercolor ? bordercolor : 'transparent'};
  }
  :hover {
    color: #fff;
  }`;

const LinkRoute = styled(Link)`
  ${linkStyle}
`;
const LinkInitialRoute = styled.a`
  ${linkStyle}
`;

export { LinkRoute, LinkInitialRoute };
