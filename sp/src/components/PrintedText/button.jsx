import React, { useState } from 'react';
import { Popover, Button } from 'antd';
import { Input } from 'antd';
import styled from 'styled-components/macro';
import { Wrap } from '../Grid/Grid';

const EditorPrintedText = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  textarea {
    width: 500px;
    margin-bottom: 10px;
  }
`;

const { TextArea } = Input;

const PrintedTextButton = ({ setPrintedText }) => {
  const [text, setText] = useState('');
  const [visible, setVisible] = useState(false);

  const Content = (
    <EditorPrintedText>
      <Wrap>
        <div>
          <TextArea
            placeholder="Текст"
            value={text || ''}
            rows={4}
            onChange={e => setText(e.target.value)}
          />
        </div>
        <Button
          onClick={() => {
            setPrintedText({ print: text });
            setVisible(false);
            setText('');
          }}
        >
          Добавить
        </Button>
      </Wrap>
    </EditorPrintedText>
  );

  return (
    <Popover
      content={Content}
      title="Типографный текст"
      trigger="click"
      placement="right"
      visible={visible}
      onVisibleChange={visible => {
        setVisible(visible);
      }}
    >
      <Button>Типографный текст</Button>
    </Popover>
  );
};

export default PrintedTextButton;
