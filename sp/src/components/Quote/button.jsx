import React, { useState } from 'react';
import { Popover, Button, Input } from 'antd';
import styled from 'styled-components/macro';

import FileManager from 'containers/FileManager';
import { BtnFindImg, FilesControl } from '../_styles/buttonsStyles';
import { Wrap } from '../Grid/Grid';

const { TextArea } = Input;

const QuoteContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  textarea {
    width: 500px;
  }
`;
const QuoteButton = ({ setQuote }) => {
  const [visible, setVisible] = useState(false);
  const [showFM, setShowFM] = useState(false);
  const [url, setUrl] = useState('');
  const [text, setText] = useState('');
  const [author, setAuthor] = useState('');

  const Content = (
    <QuoteContainer>
      <FilesControl>
        <Input
          placeholder="Url картинки"
          value={url || ''}
          onChange={e => setUrl(e.target.value)}
        />
        <BtnFindImg onClick={() => setShowFM(true)}></BtnFindImg>
      </FilesControl>
      <Wrap>
        <TextArea
          placeholder="Текст цитаты"
          value={text || ''}
          rows={4}
          onChange={e => setText(e.target.value)}
        />
      </Wrap>
      <Wrap>
        <Input
          placeholder="Подпись"
          value={author || ''}
          onChange={e => setAuthor(e.target.value)}
        />
      </Wrap>
      <Wrap>
        <Button
          onClick={() => {
            setQuote({ url, author, textQuote: text });
            setVisible(false);
            setUrl('');
            setText('');
            setAuthor('');
          }}
        >
          Добавить
        </Button>
      </Wrap>
    </QuoteContainer>
  );

  return (
    <>
      <Popover
        content={Content}
        title="Цитата"
        trigger="click"
        placement="right"
        visible={visible}
        onVisibleChange={visible => {
          if (!showFM) setVisible(visible);
        }}
      >
        <Button>Цитата</Button>
      </Popover>

      <FileManager
        visible={showFM}
        setPicUrl={setUrl}
        hideModal={() => setShowFM(false)}
      />
    </>
  );
};

export default QuoteButton;
