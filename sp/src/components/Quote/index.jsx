import React, { useContext } from 'react';
import styled, { ThemeContext } from 'styled-components/macro';
import { parserCustomBBCode } from '../Text/parser';
import { clearText } from 'clearText';

const QuoteWrap = styled.div`
  max-width: 755px;
  margin: 0 auto 55px;
  .textAll.TEXT-WORD {
    margin-bottom: 0;
  }
`;

const QuoteTextWrap = styled.div`
  display: flex;
  align-items: center;
  max-width: 460px;
`;

const QuoteImgWrap = styled.div`
  width: 45px;
  min-width: 45px;
  height: 45px;
  margin-right: 18px;
`;

const QuoteImg = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

const QuoteTitle = styled.div`
  .textAll.TEXT-WORD {
    font-size: 33px;
    font-family: 'Krok';
    margin-bottom: 18px;
    line-height: 1.2;
    color: ${({ themeContext }) => themeContext.color};
    min-height: auto;
  }
`;

const QuoteAuthor = styled.div`
  .textAll {
    width: 100%;
    font-size: 17px;
    font-family: 'MullerRegular';
    font-style: normal;
    line-height: 25px;
    color: ${({ themeContext }) => themeContext.color};
  }
  p {
    margin: 0;
    min-height: auto;
  }
`;

const Quote = ({ data }) => {
  const themeContext = useContext(ThemeContext);

  return (
    <QuoteWrap>
      <QuoteTitle themeContext={themeContext}>
        {parserCustomBBCode({ r: data.text }).component}
      </QuoteTitle>
      <QuoteTextWrap>
        <QuoteImgWrap>
          <QuoteImg
            src={data.url}
            alt={data.author ? clearText(data.author) : ''}
          />
        </QuoteImgWrap>
        <QuoteAuthor themeContext={themeContext}>
          {parserCustomBBCode({ r: data.author }).component}
        </QuoteAuthor>
      </QuoteTextWrap>
    </QuoteWrap>
  );
};

export default Quote;
