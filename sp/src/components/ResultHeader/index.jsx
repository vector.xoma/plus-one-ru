import React from 'react';
import styled from 'styled-components/macro';

const Container = styled.div`
  width: 1140px;
  max-width: 100%;
  margin: 0 auto;
  @media (max-width: 767px) {
    padding: 0 10px;
    width: 100%;
  }
`;
const SearchResultHeader = styled.div`
  display: flex;
  flex-flow: column wrap;
  align-content: space-between;
  flex-direction: row;
  font-size: 28px;
  line-height: 37px;
  font-family: MullerBold;
  margin-bottom: 34px;
  margin-top: 78px;
  color: #fff;
  @media (max-width: 767px) {
    margin: 20px auto 26px;
    max-width: 365px;
  }
`;
const LeftTitle = styled.span`
  flex: 4;
  @media (max-width: 767px) {
    font-size: 20px;
    flex: auto;
  }
`;
const RightTitle = styled.span`
  flex: 1;
  text-align: right;
  @media (max-width: 767px) {
    font-size: 20px;
    flex: auto;
  }
`;

const ResultHeader = ({ searchTitle = '', listLength = '' }) => {
  return (
    <Container>
      <SearchResultHeader>
        <LeftTitle>Поиск: {`"${searchTitle}"`}</LeftTitle>
        <RightTitle>найдено: {listLength}</RightTitle>
      </SearchResultHeader>
    </Container>
  );
};

export default ResultHeader;
