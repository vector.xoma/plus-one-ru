import React, { useState } from 'react';
import { Popover, Button, Input } from 'antd';
import styled from 'styled-components/macro';
import { Wrap } from '../Grid/Grid';

const SignWrap = styled.div`
  display: flex;
  flex-direction: column;
  width: 500px;
`;
const SignName = styled.div``;

const SignButton = ({ setSign }) => {
  const titleDefault = 'Автор';
  const [title, setTitle] = useState(titleDefault);
  const [name, setName] = useState('');
  const [visible, setVisible] = useState(false);

  const Content = (
    <SignWrap>
      <Wrap>
        <Input
          placeholder="Заголовок"
          value={title || ''}
          onChange={e => setTitle(e.target.value)}
        />
      </Wrap>
      <SignName>
        <Wrap>
          <Input
            placeholder="Имя"
            value={name || ''}
            onChange={e => setName(e.target.value)}
          />
        </Wrap>
      </SignName>
      <Wrap>
        <Button
          onClick={() => {
            setSign({ title, name });
            setVisible(false);
            setTitle(titleDefault);
            setName('');
          }}
        >
          Добавить
        </Button>
      </Wrap>
    </SignWrap>
  );

  return (
    <Popover
      content={Content}
      title="Подпись"
      trigger="click"
      visible={visible}
      onVisibleChange={visible => {
        setVisible(visible);
      }}
    >
      <Button>Подпись</Button>
    </Popover>
  );
};

export default SignButton;
