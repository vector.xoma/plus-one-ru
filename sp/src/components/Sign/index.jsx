import React, { useContext } from 'react';
import { ThemeContext } from 'styled-components/macro';
import { parserCustomBBCode } from '../Text/parser';
import { SignContainer, SignDevider, SignTitle, SignAuthor } from './style';

const Sign = ({ data }) => {
  const themeContext = useContext(ThemeContext);

  return (
    <SignContainer>
      <SignDevider themeContext={themeContext} />
      <SignTitle themeContext={themeContext}>
        {parserCustomBBCode({ r: data.title }).component}
      </SignTitle>
      <SignAuthor themeContext={themeContext}>
        {parserCustomBBCode({ r: data.name }).component}
      </SignAuthor>
    </SignContainer>
  );
};

export default Sign;
