import styled from 'styled-components/macro';

const SignContainer = styled.div`
  max-width: 755px;
  margin: 0px auto 28px auto;
  .textAll {
    min-height: auto;
  }
  .textAll.TEXT-WORD {
    margin-bottom: 0;
  }
`;

const SignDevider = styled.div`
  width: 110px;
  height: 2px;
  background: ${({ themeContext, theme }) =>
    theme.font_color && theme.font_color !== ''
      ? theme.font_color
      : themeContext.color};
  margin-bottom: 28px;
`;

const SignTitle = styled.div`
  .textAll {
    font-family: 'MullerBold', sans-serif;
    line-height: 1.54;
  }
  font-size: 20px;
  color: ${({ themeContext }) => themeContext.color};
  line-height: 35px;
`;

const SignAuthor = styled.div`
  font-size: 20px;
  font-family: 'MullerRegular', sans-serif;
  line-height: 25px;
  color: ${({ themeContext }) => themeContext.color};
`;

export { SignContainer, SignDevider, SignTitle, SignAuthor };
