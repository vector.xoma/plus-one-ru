const rubrics = [
  { name: 'ЭКОЛОГИЯ', url: '/ecology' },
  { name: 'ЭКОНОМИКА', url: '/economy' },
  { name: 'ОБЩЕСТВО', url: '/society' },
];

const specArticles = [
  { name: 'УСТОЙЧИВОЕ РАЗВИТИЕ', url: '/устойчивое-развитие' },
  { name: 'СПЕЦПРОЕКТЫ', url: '/special-projects' },
  { name: 'БЛОГИ', url: '/platform' },
  { name: 'УЧАСТНИКИ', url: '/author' },
];

const plusOnePartners = [
  { name: '+1ГОРОД', url: 'https://полезныйгород.рф' },
  { name: '+1ПЛАТФОРМА', url: 'https://platform.plus-one.ru' },
  { name: '+1ЛЮДИ', url: 'https://people.plus-one.ru/' },
];

export { rubrics, specArticles, plusOnePartners };
