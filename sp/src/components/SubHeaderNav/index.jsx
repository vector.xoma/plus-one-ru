import React from 'react';

import { NawWrap, NavItemCol, LinkRoute, A, NavItemLi } from './style';
import { rubrics, specArticles, plusOnePartners } from './helpers';

const SubHEaderNav = () => {
  return (
    <div>
      <NawWrap>
        <NavItemCol>
          {rubrics.map((rubric, i) => (
            <NavItemLi key={i}>
              <LinkRoute to={rubric.url} activeClassName="activeElem">
                {rubric.name}
              </LinkRoute>
            </NavItemLi>
          ))}
        </NavItemCol>
        <NavItemCol>
          {specArticles.map((rubric, i) => (
            <NavItemLi key={i}>
              <LinkRoute to={rubric.url} activeClassName="activeElem">
                {rubric.name}
              </LinkRoute>
            </NavItemLi>
          ))}
        </NavItemCol>
        <NavItemCol>
          {plusOnePartners.map((rubric, i) => (
            <NavItemLi key={i}>
              <A href={rubric.url} target="_blank" rel="noopener noreferrer">
                {rubric.name}
              </A>
            </NavItemLi>
          ))}
        </NavItemCol>
      </NawWrap>
    </div>
  );
};
export default SubHEaderNav;
