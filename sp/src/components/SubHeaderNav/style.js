import styled, { css } from 'styled-components/macro';
import { NavLink } from 'react-router-dom';

const NawWrap = styled.nav`
  max-width: 1140px;
  margin: 0 auto;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 26px 0 25px;
  .activeElem {
    color: #9b9b9b;
  }
`;

const NavItemCol = styled.ul`
  display: flex;
  align-items: center;
  margin-bottom: 0;
  padding: 0 24px;
  border-left: solid 1px #4a4a4a;
  border-right: solid 1px #4a4a4a;
  :first-child {
    padding-left: 0;
    border-left: none;
    border-right: none;
  }
  :nth-child(2) {
    padding: 0 28px;
  }
  :last-child {
    padding-right: 0;
    border-left: none;
    border-right: none;
  }
`;
const NavItemLi = styled.li`
  margin-right: 24px;
  :last-child {
    margin-right: 0;
  }
`;

const StylesLink = css`
  font-family: 'MullerMedium';
  font-size: 13px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.11;
  /* letter-spacing: 0.27px; */
  text-align: left;
  color: #ffffff;
  :hover {
    color: #9b9b9b;
  }
`;

const LinkRoute = styled(NavLink)`
  ${StylesLink}
`;
const A = styled.a`
  ${StylesLink}
`;

export { NawWrap, NavItemCol, LinkRoute, A, NavItemLi };
