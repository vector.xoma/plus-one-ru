import { splitBySelection } from 'utils';
import { clearText } from 'clearText';

export const setBold = ({ changeText, text, area }) => {
  const { start, middle, end } = splitBySelection(text, area);
  changeText(`${start}[b]${middle}[/b]${end}`);
};

export const setUnder = ({ changeText, text, area }) => {
  const { start, middle, end } = splitBySelection(text, area);
  changeText(`${start}[u]${middle}[/u]${end}`);
};

export const setItalic = ({ changeText, text, area }) => {
  const { start, middle, end } = splitBySelection(text, area);
  changeText(`${start}[i]${middle}[/i]${end}`);
};

export const setLink = ({ changeText, text, area }, { url, urlText }) => {
  const { start, end } = splitBySelection(text, area);
  changeText(`${start}[url="${url}"]${urlText}[/url]${end}`);
};

export const setSub = ({ changeText, text, area }) => {
  const { start, middle, end } = splitBySelection(text, area);
  changeText(`${start}[sub]${middle}[/sub]${end}`);
};

export const setSup = ({ changeText, text, area }) => {
  const { start, middle, end } = splitBySelection(text, area);
  changeText(`${start}[sup]${middle}[/sup]${end}`);
};

export const setSign = (
  { changeText, text, area },
  { title = '', name = '' },
) => {
  const { start, end } = splitBySelection(text, area);
  changeText(
    `${
      start ? `${start}\n\n` : ''
    }[sign title="${title}" name="${name}"]\n\n${end}`,
  );
};

export const setTextInsert = (
  { changeText, text, area },
  { textInsert = '', type },
) => {
  const { start, end } = splitBySelection(text, area);
  changeText(
    `${
      start ? `${start}\n\n` : ''
    }[textInsert text="${textInsert}" type="${type}"]\n\n${end}`,
  );
};

export const setList = (
  { changeText, text, area },
  { elements, numeric = false, firstNum = 1 },
) => {
  const { start, end } = splitBySelection(text, area);
  const elementsRow = elements.map(el => `[li]${el.name}[/li]`).join('');
  changeText(
    `${
      start ? `${start}\n\n` : ''
    }[listRows numeric="${numeric}" start="${firstNum}"/]${elementsRow}[/listRows]\n\n${end}`,
  );
};

export const setHeaders = ({ changeText, text, area }, { type }) => {
  const { start, middle, end } = splitBySelection(text, area);
  changeText(
    `${start}[headerTitle type="${type}"/]${middle}[/headerTitle]${end}`,
  );
};

export const setPicRetraction = (
  { changeText, text, area },
  { url, title, textPic = '', link = '', targetBlank },
) => {
  const { start, end } = splitBySelection(text, area);
  changeText(
    `${
      start ? `${start}\n\n` : ''
    }[picRetraction url="${url}" title="${title}" textPic="${textPic}" link="${link}" blank="${targetBlank}"]\n\n${end}`,
  );
};

export const setQuote = (
  { changeText, text, area },
  { url = '', author = '', textQuote = '' },
) => {
  const { start, end } = splitBySelection(text, area);
  changeText(
    `${
      start ? `${start}\n\n` : ''
    }[quote url="${url}" author="${author}" text="${textQuote}"]\n\n${end}`,
  );
};

export const setInterview = (
  { changeText, text, area },
  { question = '', answer = '' },
) => {
  const { start, end } = splitBySelection(text, area);
  changeText(
    `${
      start ? `${start}\n\n` : ''
    }[interview question="${question}" answer="${answer}"]\n\n${end}`,
  );
};

export const setFullWidthPic = (
  { changeText, text, area },
  { url = '', title = '', source = '' },
) => {
  const { start, end } = splitBySelection(text, area);
  changeText(
    `${
      start ? `${start}\n\n` : ''
    }[fullWidthPic url="${url}" title="${title}" source="${source}"]\n\n${end}`,
  );
};

export const setPicture = (
  { changeText, text, area },
  { url = '', title = '', source = '' },
) => {
  const { start, end } = splitBySelection(text, area);
  changeText(
    `${
      start ? `${start}\n\n` : ''
    }[picture url="${url}" title="${title}" source="${source}"]\n\n${end}`,
  );
};

export const setEmbed = ({ changeText, text, area }, { embedInsert, type }) => {
  const { start, end } = splitBySelection(text, area);
  changeText(
    `${
      start ? `${start}\n\n` : ''
    }[embed embedInsert="${embedInsert}" type="${type}"]\n\n${end}`,
  );
};

export const setALink = (
  { changeText, text, area },
  { url, urlText, targetBlank },
) => {
  const { start, end } = splitBySelection(text, area);
  changeText(
    `${start}[alink url="${url}" blank="${targetBlank}"]${urlText}[/alink]${end}`,
  );
};

export const setFileLink = ({ changeText, text, area }, { url, urlText }) => {
  const { start, end } = splitBySelection(text, area);
  changeText(`${start}[filelink url="${url}"]${urlText}[/filelink]${end}`);
};

export const setFotoRama = ({ changeText, text, area }, { id }) => {
  const { start, end } = splitBySelection(text, area);
  changeText(`${start ? `${start}\n\n` : ''}[fotorama id="${id}"]\n\n${end}`);
};

export const setDonateForm = ({ changeText, text, area }, { header = '' }) => {
  const { start, end } = splitBySelection(text, area);
  changeText(
    `${start ? `${start}\n\n` : ''}[donateForm header="${header}"]\n\n${end}`,
  );
};

export const setSubscribeForm = ({ changeText, text, area }) => {
  const { start, end } = splitBySelection(text, area);
  changeText(`${start ? `${start}\n\n` : ''}[subscribeForm]\n\n${end}`);
};

export const setPrintedText = ({ changeText, text, area }, { print = '' }) => {
  const { start, end } = splitBySelection(text, area);
  changeText(`${start ? `${start}\n\n` : ''}${clearText(print)}\n\n${end}`);
};

export const setGooDocACTION = ({ changeText, text, area }, { print = '' }) => {
  const { start, end } = splitBySelection(text, area);
  changeText(`${start ? `${start}\n` : ''}${print}\n${end}`);
};
