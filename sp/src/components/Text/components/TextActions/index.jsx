import React, { useEffect } from 'react';
import { Button, Icon } from 'antd';
import styled from 'styled-components/macro';

import SignButton from 'components/Sign/button';
import TextInsertButton from 'components/TextInsert/button';
import PicRetractionButton from 'components/PicRetraction/button';
import LinkButton from 'components/ALink/button';
import FileLinkButton from 'components/FileLink/button';
import QuoteButton from 'components/Quote/button';
import InterviewButton from 'components/Interview/button';
import FullWidthPicButton from 'components/FullWidthPic/button';
import PictureButton from 'components/Picture/button';
import NumberingListComponent from 'components/NumberingListComponent/button';
import HeaderType from 'components/HeaderType/button';
import EmbedButton from 'components/Embed/button';
import FotoRamaButton from 'components/FotoRama/button';
import DonateButton from 'components/DonateForm/button';
// import PrintedTextButton from 'components/PrintedText/button';
import SubscribeFormButton from 'components/Block/components/SubscribeForm/button';
import GoogleDocsParser from 'components/GoogleDocsParser';

import {
  setBold,
  setUnder,
  setItalic,
  setALink,
  setFileLink,
  setSub,
  setSup,
  setSign,
  setTextInsert,
  setPicRetraction,
  setQuote,
  setInterview,
  setFullWidthPic,
  setList,
  setHeaders,
  setEmbed,
  setFotoRama,
  setDonateForm,
  setSubscribeForm,
  // setPrintedText,
  setPicture,
  setGooDocACTION,
} from './actions';

const ButtonContainerWrap = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-bottom: 10px;
  > * {
    margin: 2px;
  }
`;

const Btn = styled(Button)``;

const TextActions = ({
  changeText,
  text,
  area,
  textHistory,
  setTextHistory,
}) => {
  // TODO: недоделан хот кей
  const previos = () => {
    const newHistory = [...textHistory];
    const newText = newHistory[newHistory.length - 2];
    newHistory.splice(-2, 2);
    setTextHistory(newHistory);
    changeText(newText);
  };

  const propsForActions = { changeText, text, area };
  const _setBold = () => setBold(propsForActions);
  const _setUnder = () => setUnder(propsForActions);
  const _setItalic = () => setItalic(propsForActions);
  const _setSub = () => setSub(propsForActions);
  const _setSup = () => setSup(propsForActions);
  const _setSign = data => setSign(propsForActions, data);
  const _setTextInsert = data => setTextInsert(propsForActions, data);
  const _setPicRetraction = data => setPicRetraction(propsForActions, data);
  const _setQuote = data => setQuote(propsForActions, data);
  const _setInterview = data => setInterview(propsForActions, data);
  const _setFullWidthPic = data => setFullWidthPic(propsForActions, data);
  const _setList = data => setList(propsForActions, data);
  const _setHeaders = data => setHeaders(propsForActions, data);
  const _setEmbed = data => setEmbed(propsForActions, data);
  const _setALink = data => setALink(propsForActions, data);
  const _setFileLink = data => setFileLink(propsForActions, data);
  const _setFotoRama = data => setFotoRama(propsForActions, data);
  const _setDonateForm = data => setDonateForm(propsForActions, data);
  const _setSubscribeForm = () => setSubscribeForm(propsForActions);
  // const _setPrintedText = data => setPrintedText(propsForActions, data);
  const _setPicture = data => setPicture(propsForActions, data);
  const _setGooDocACTION = data => setGooDocACTION(propsForActions, data);

  const buttons = [
    { title: <Icon type="arrow-left" />, action: previos },
    { title: <Icon type="bold" />, action: _setBold },
    { title: <Icon type="underline" />, action: _setUnder },
    { title: <Icon type="italic" />, action: _setItalic },
  ];

  useEffect(() => {
    const handler = event => {
      if ((event.ctrlKey || event.metaKey) && area === document.activeElement) {
        if (event.keyCode === 90) previos(event);
        if (event.keyCode === 66) _setBold();
        if (event.keyCode === 73) _setItalic();
        if (event.keyCode === 85) {
          event.preventDefault();
          _setUnder();
        }
      }
      return;
    };
    window.addEventListener('keydown', handler);

    return () => window.removeEventListener('keydown', handler);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [textHistory]);

  return (
    <ButtonContainerWrap>
      {buttons.map((b, i) => (
        <Btn key={i} className="button" onClick={b.action}>
          {b.title}
        </Btn>
      ))}
      <Btn className="button" onClick={_setSub}>
        <span style={{ color: 'black' }}>X</span>
        <sub>2</sub>
      </Btn>
      <Btn className="button" onClick={_setSup}>
        <span style={{ color: 'black' }}>X</span>
        <sup>2</sup>
      </Btn>
      <LinkButton setALink={_setALink} textData={propsForActions} />
      <NumberingListComponent setList={_setList} />
      <PictureButton setPicture={_setPicture} />
      <FullWidthPicButton setFullWidthPic={_setFullWidthPic} />
      <SignButton setSign={_setSign} />
      <TextInsertButton setTextInsert={_setTextInsert} />
      <PicRetractionButton setPicRetraction={_setPicRetraction} />
      <QuoteButton setQuote={_setQuote} />
      <InterviewButton setInterview={_setInterview} />
      <DonateButton setDonateForm={_setDonateForm} />
      <HeaderType setHeaders={_setHeaders} />
      <EmbedButton setEmbed={_setEmbed} />
      <FotoRamaButton setFotoRama={_setFotoRama} />
      <SubscribeFormButton setSubscribeForm={_setSubscribeForm} />
      {/* TODO:
       ** по задаче 990, сказали что потребности нет в компоненте
       ** если не будет изменений - удалить
       */}
      {/* <PrintedTextButton setPrintedText={_setPrintedText} /> */}
      <FileLinkButton setFileLink={_setFileLink} textData={propsForActions} />
      <GoogleDocsParser setAction={_setGooDocACTION} />
    </ButtonContainerWrap>
  );
};

export default TextActions;
