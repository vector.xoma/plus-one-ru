import React, { useEffect, useState, useRef, useContext } from 'react';
import _isEmpty from 'lodash/isEmpty';
import styled, { ThemeContext } from 'styled-components/macro';
// import { clearTextSpaces } from "utils";
import TextActions from './components/TextActions';
import { parserCustomBBCode } from './parser';
import { splitBySelection } from 'utils';

const FullWidth = styled.div`
  width: 100%;
  .full-width-pic_img {
    width: 100%;
  }
`;
const CenterWidth = styled.div`
  max-width: ${({ w }) => w};
  margin: 0 auto;
`;

const ShowTextField = styled.div`
  line-height: 1.54;
  width: 100%;
  margin-left: auto;
  margin-right: auto;
  font-size: 20px;
  .textAll {
    /* color: ${({ themeContext }) => themeContext.color}; */
    color: ${({ theme, themeContext }) =>
      theme.font_color ? theme.font_color : themeContext.color};
  }
`;

const EditorTextField = styled.textarea`
  outline: none;
  width: 100%;
  font-family: Consolas;
  line-height: 1.5;
  font-size: 18px;
  color: #000;
  ${({ name }) =>
    name === 'body' ? { minHeight: '300px' } : { maxHeight: '250px' }};
  overflow-y: auto;
`;

// счётчик открытых/закрытых тегов
const bracketDifference = string => {
  const openBracket = string.split('[').length - 1;
  const closeBracket = string.split(']').length - 1;
  const difference = openBracket - closeBracket;

  return difference;
};

// разбиваем весь текст по переносам. затем, если тег открылся, но не был закрыт - склеивает несколько строк
// сделано для возможности поддрежки тегов с переносами строк в контенте
const SplitOnRows = text => {
  const splitted = text && text.split('\n');
  const parsedByRows = [];
  if (splitted) {
    for (let i = 0; i < splitted.length; i++) {
      let row = splitted[i];

      // смотрим на разницу скобок в строке, если есть разница - склеиваем переносы до погашения разницы
      const diffMainRow = bracketDifference(row);
      let dinamicDiff = diffMainRow;
      if (diffMainRow !== 0) {
        const currentRow = i;
        for (let y = currentRow + 1; y < splitted.length; y++) {
          row = `${row}\n${splitted[y]}`;
          const diffSubRow = bracketDifference(splitted[y]);
          i++;
          dinamicDiff = dinamicDiff + diffSubRow;
          if (dinamicDiff === 0) break;
        }
      }

      parsedByRows.push(row);
    }
  }

  // тут происходит склейка строк. если следующая строка не пустая "", то мы склеиваем её с текущей
  // это нужно что бы не отделять каждый абзац отступом, как параграф, а иметь возоможность делать просто перенос строк
  // из-за этого кастомные, многострочные теги (такие как [textInsrt ...]) требуется отделять дополнительными переносами
  const parsedByRows2 = [];
  for (let i = 0; i < parsedByRows.length; i++) {
    let row = parsedByRows[i];

    if (i + 1 < parsedByRows.length && parsedByRows[i + 1] !== '') {
      for (let y = i + 1; y < parsedByRows.length; y++) {
        if (parsedByRows[y] === '') break;
        row = row === '' ? parsedByRows[y] : `${row}\n${parsedByRows[y]}`;
        i++;
      }
    }

    parsedByRows2.push(row);
  }

  return parsedByRows2;
};

const WidthComponent = ({ component, fullWidth, width }, i) =>
  fullWidth ? (
    <FullWidth key={i}>{component}</FullWidth>
  ) : (
    <CenterWidth key={i} w={width}>
      {component}
    </CenterWidth>
  );

const Text = ({
  data,
  isAboutPage,
  isarticlepage,
  zoom,
  isAdminPage,
  lead,
  donateProps = {},
}) => {
  const themeContext = useContext(ThemeContext);
  const parsedByRows = SplitOnRows(data.text);
  const { donateHeader } = donateProps;
  return (
    <ShowTextField
      isAboutPage={isAboutPage}
      isarticlepage={isarticlepage}
      themeContext={themeContext}
      isAdminPage={isAdminPage}
      zoom={zoom}
      donateHeader={donateHeader}
    >
      {data.text &&
        parsedByRows.map((r, i) =>
          WidthComponent(
            parserCustomBBCode({
              r,
              i,
              zoom,
              isarticlepage,
              isAdminPage,
              lead,
              ...donateProps,
            }),
            i,
          ),
        )}
    </ShowTextField>
  );
};

const TextEditor = ({ updateField, data, areaName }) => {
  const textareaRef = useRef();
  const area = textareaRef.current;

  const [textHistory, setTextHistory] = useState([]);

  useEffect(() => {
    if (_isEmpty(data)) updateField({ text: '' });
  }, [data, updateField]);

  useEffect(() => {
    if (typeof data.text === 'string')
      setTextHistory([...textHistory, data.text]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data.text]);

  const changeText = val => {
    updateField({
      text: val, //clearTextSpaces(val)
    });
  };

  // при нажитии на Enter в некоторых случаях курсор устанавливается
  // в 1ю строку textarea (поведение по умолчанию)
  // отменяем любое действие при нажатии на энетр и устанавливаем курсор
  // на нужную позицию
  // таск 1042
  const stopDefaultActionOnEnter = e => {
    if (e.key === 'Enter') {
      e.preventDefault();
      const { start, end } = splitBySelection(data.text, area);
      updateField({
        text: `${start}\n${end}`, //clearTextSpaces(val)
      });
      setTimeout(() => {
        area.selectionStart = start.length + 1;
        area.selectionEnd = start.length + 1;
      }, 0);
    }
  };

  return (
    <>
      <TextActions
        area={area}
        textHistory={textHistory}
        setTextHistory={setTextHistory}
        text={data.text}
        changeText={changeText}
      />

      <EditorTextField
        ref={textareaRef}
        name={areaName}
        rows={areaName === 'body' ? 30 : 20}
        value={data.text}
        onChange={e => changeText(e.target.value)}
        onKeyPress={stopDefaultActionOnEnter}
      />
    </>
  );
};

Text.Editor = TextEditor;

export default Text;
