import React from 'react';
import parser, { Tag } from 'bbcode-to-react';
import styled from 'styled-components/macro';
import { clearText } from 'clearText';

import HeaderTypes from '../HeaderType';
import Sign from '../Sign';
import FotoRama from '../FotoRama';
import TextInsert from '../TextInsert';
import PicRetraction from '../PicRetraction';
import Quote from '../Quote';
import Interview from '../Interview';
import FullWidthPic from '../FullWidthPic';
import List from '../NumberingListComponent';
import Embed from '../Embed';
import ALink from '../ALink';
import FileLink from '../FileLink';
import ParsedImg from '../ParsedImg';
import DonateForm from '../DonateForm';
import SubscribeForm from 'components/Block/components/SubscribeForm';

const TextAll = styled.p`
  margin: 0;
  white-space: pre-line;
  font-family: 'MullerRegular', sans-serif;
  font-size: 20px;
  color: ${({ theme }) => (theme && theme.color ? theme.color : '#000')};
  line-height: 1.54;
  /* min-height: 35px; Оставил 30 ибо разная высота отступов , если текст в одну строку*/
  min-height: 28px;

  &.TEXT-WORD {
    margin-bottom: 28px;
  }
  &.TEXT-EMPTY {
    margin-bottom: 0;
  }

  &.lead {
    font-size: 26px;
    line-height: 1.3;
    margin-bottom: 28px;
    strong {
      font-weight: 700;
    }
    @media (max-width: 767px) {
      font-size: 20px;
      line-height: 1.24;
    }
  }
  strong {
    font-weight: 700;
  }

  sup,
  sub {
    font-size: 16px;
    line-height: 0;
  }
  @media (max-width: 767px) {
    line-height: 1.34;
    strong {
      font-size: ${() => window.location.pathname.includes('/news/') && '20px'};
    }
  }
`;

class SubTag extends Tag {
  toReact() {
    return <sub>{this.getComponents()}</sub>;
  }
}
parser.registerTag('sub', SubTag);

class SupTag extends Tag {
  toReact() {
    return <sup>{this.getComponents()}</sup>;
  }
}
parser.registerTag('sup', SupTag);

// [leftpadded px="30px" textalign="center"]123123[/leftpadded]
class LeftPadded extends Tag {
  toReact() {
    const { px, textalign } = this.params;
    return (
      <p style={{ paddingLeft: px, textAlign: textalign }}>
        {this.getComponents()}
      </p>
    );
  }
}
parser.registerTag('leftpadded', LeftPadded);

// [decoration color="#0f0" bgcolor="#00f"]123[/decoration]
class TextDecorationTag extends Tag {
  toReact() {
    const { color, bgcolor } = this.params;

    return (
      <span style={{ color, backgroundColor: bgcolor }}>
        {this.getComponents()}
      </span>
    );
  }
}
parser.registerTag('decoration', TextDecorationTag);

// [img src="..."][/img]
class ImageTag extends Tag {
  toReact() {
    const { src } = this.params;

    return <ParsedImg imgUrl={src} />;
  }
}
parser.registerTag('img', ImageTag);

// [alink url="..." blank="false"]123[/alink]
class ALinkTag extends Tag {
  toReact() {
    const { url, blank } = this.params;

    return (
      <ALink
        data={{
          url,
          targetBlank: blank,
          text: this.getComponents(),
        }}
      />
    );
  }
}
parser.registerTag('alink', ALinkTag);

// [filelink url="..."]123[/filelink]
class FileLinkTag extends Tag {
  toReact() {
    const { url, blank } = this.params;

    return (
      <FileLink
        data={{
          url,
          targetBlank: blank,
          text: this.getComponents(),
        }}
      />
    );
  }
}
parser.registerTag('filelink', FileLinkTag);

// [headerTitle type="1"]2134243[/headerTitle]
class HeaderTitleTag extends Tag {
  toReact() {
    const { type } = this.params;

    return (
      <HeaderTypes
        data={{
          type,
          text: this.getComponents(),
        }}
      />
    );
  }
}
parser.registerTag('headertitle', HeaderTitleTag);
// TODO: поправить. Передавать объект
export const parserCustomBBCode = ({
  r,
  i = 0,
  zoom,
  isarticlepage = 0,
  isAdminPage,
  lead,
  donateHeader = '',
  show_fund,
  resourceId,
  shopId,
}) => {
  // иногда с бэка прилетают строки с undefined. т.е. это строка, но она не '', а именно undefined
  if (!r && r !== '')
    return {
      component: <span></span>,
      fullWidth: false,
      width: '755px',
    };

  const sign = r.match(/\[sign title="([\s\S]*)" name="([\s\S]*)"]/);

  if (sign) {
    return {
      component: <Sign key={i} data={{ title: sign[1], name: sign[2] }} />,
      fullWidth: false,
      width: '755px',
    };
  }

  const insert = r.match(
    /\[textInsert text="([\s\S]*||\n)" type="([\s\S]*)"]/m,
  );
  if (insert)
    return {
      component: (
        <TextInsert key={i} data={{ text: insert[1], type: insert[2] }} />
      ),
      fullWidth: false,
      width: '755px',
    };

  const interview = r.match(
    /\[interview question="([\s\S]*||\n)" answer="([\s\S]*||\n)"]/m,
  );
  if (interview)
    return {
      component: (
        <Interview
          key={i}
          data={{ question: interview[1], answer: interview[2] }}
        />
      ),
      fullWidth: false,
      width: '755px',
    };

  // const headerTitle = r.match(
  //   /\[headerTitle type="([\s\S]*)"\/\]([\s\S]*)\[\/headerTitle\]/,
  // );
  // if (headerTitle)
  //   return {
  //     component: (
  //       <HeaderTypes
  //         key={i}
  //         data={{
  //           text: headerTitle[2],
  //           type: headerTitle[1],
  //         }}
  //       />
  //     ),
  //     fullWidth: false,
  //     width: '755px',
  //   };

  const retraction = r.match(
    /\[picRetraction url="([\s\S]*||\n)" title="([\s\S]*||\n)" textPic="([\s\S]*||\n)" link="([\s\S]*||\n)" blank="([\s\S]*||\n)"]/m,
  );
  if (retraction)
    return {
      component: (
        <PicRetraction
          key={i}
          data={{
            url: retraction[1],
            title: retraction[2],
            textPic: retraction[3],
            link: retraction[4],
            targetBlank: retraction[5],
          }}
        />
      ),
      fullWidth: false,
      width: '755px',
    };

  const quote = r.match(
    /\[quote url="([\s\S]*||\n)" author="([\s\S]*||\n)" text="([\s\S]*||\n)"]/m,
  );
  if (quote)
    return {
      component: (
        <Quote
          key={i}
          data={{
            url: quote[1],
            author: quote[2],
            text: quote[3],
          }}
        />
      ),
      fullWidth: false,
      width: '755px',
    };

  const fwp = r.match(
    /\[fullWidthPic url="([\s\S]*||\n)" title="([\s\S]*||\n)" source="([\s\S]*||\n)"]/m,
  );
  if (fwp)
    return {
      component: (
        <FullWidthPic
          key={i}
          isarticlepage={isarticlepage}
          isAdminPage={isAdminPage}
          data={{
            url: fwp[1],
            title: fwp[2],
            source: fwp[3],
          }}
        />
      ),
      fullWidth: true,
      width: '100%',
    };

  const picture = r.match(
    /\[picture url="([\s\S]*||\n)" title="([\s\S]*||\n)" source="([\s\S]*||\n)"]/m,
  );
  if (picture)
    return {
      component: (
        <FullWidthPic
          isarticlepage={isarticlepage}
          isAdminPage={isAdminPage}
          key={i}
          data={{
            url: picture[1],
            title: picture[2],
            source: picture[3],
          }}
        />
      ),
      fullWidth: false,
      width: '755px',
    };

  const list = r.match(
    /\[listRows numeric="([\s\S]*)" start="([\s\S]*)"\/\]([\s\S]*)\[\/listRows]/m,
  );
  if (list) {
    const elements = list[3];
    const regex = /\[li\](.+?)\[\/li\]/gim;
    let listss = [];
    let m;

    while ((m = regex.exec(elements)) !== null) listss.push(m[1]);

    return {
      component: (
        <List
          key={i}
          data={{
            numeric: list[1] === 'true',
            elements: listss,
            firstNum: list[2],
          }}
        />
      ),
      fullWidth: false,
      width: '755px',
    };
  }

  const embed = r.match(
    /\[embed embedInsert="([\s\S]*||\n)" type="([\s\S]*||\n)"]/m,
  );

  if (embed) {
    return {
      component: (
        <Embed
          key={i}
          data={{
            embedInsert: embed[1],
            type: embed[2],
          }}
        />
      ),
      fullWidth: false,
      width: '755px',
    };
  }

  const fotorama = r.match(/\[fotorama id="([\s\S]*)"]/m);
  if (fotorama)
    return {
      component: <FotoRama key={i} data={{ id: fotorama[1] }} />,
      fullWidth: false,
      width: '996px',
    };

  const donateForm = r.match(/\[donateForm header="([\s\S]*)"]/m);

  if (donateForm) {
    if (show_fund === 0) {
      return <span />;
    }
    return {
      component: (
        <DonateForm
          zoom={zoom}
          key={i}
          data={{ header: donateHeader, resourceId, shopId }}
        />
      ),
      fullWidth: false,
      width: '1140px',
    };
  }

  const subscribeForm = r.match(/\[subscribeForm]/m);
  if (subscribeForm)
    return {
      component: <SubscribeForm key={i} />,
      fullWidth: false,
      width: '1140px',
    };

  // const regexLinkForData = /\[alink url="([\s\S]*)" blank="([\s\S]*)"\]([\s\S]*)\[\/alink\]/m;
  // const regexLinkForSplit = /(\[alink url=".+?" blank=".+?"\].+?\[\/alink])/gm;
  // const link = r.match(regexLinkForData);

  // if (link) {
  //   const linksAndText = r
  //     .split(regexLinkForSplit)
  //     .map(item => ({ row: item, link: !!item.match(regexLinkForSplit) }));

  //   const parseLink = (linkRow, index) => {
  //     const linkData = linkRow.match(regexLinkForData);
  //     return (
  //       <ALink
  //         key={index}
  //         data={{
  //           url: linkData[1],
  //           targetBlank: linkData[2],
  //           text: linkData[3],
  //         }}
  //       />
  //     );
  //   };

  //   return {
  //     component: (
  //       <TextAll className="textAll" key={i}>
  //         {linksAndText.map((el, index) => {
  //           return el.link ? parseLink(el.row, index) : parser.toReact(el.row);
  //         })}
  //       </TextAll>
  //     ),
  //     fullWidth: false,
  //     width: '755px',
  //   };
  // }

  const headerTypes = r.match(
    /\[headerTitle type="([\s\S]*)"]([\s\S]*)\[\/headerTitle]/m,
  );
  const headerTypes2 = r.match(
    /\[headerTitle type="([\s\S]*)"\/]([\s\S]*)\[\/headerTitle]/m,
  );

  const empty = r === '';

  const textWord = () => {
    if (
      !headerTypes &
      !headerTypes2 &
      !sign &
      !fotorama &
      !insert &
      !retraction &
      !quote &
      !interview &
      !fwp &
      !list &
      !embed &
      !donateForm &
      !subscribeForm
    ) {
      return true;
    }
    return false;
  };
  const cssClass = () => {
    if (empty) {
      return 'TEXT-EMPTY';
    }
    return textWord() ? 'TEXT-WORD' : 'TEXT-NOT-WORD';
  };
  const TextAllWrap = () => {
    return (
      <TextAll
        className={`textAll ${lead ? 'lead' : ''} ${cssClass()}`}
        key={i}
      >
        {parser.toReact(clearText(r))}
      </TextAll>
    );
  };

  return {
    component: <TextAllWrap />,
    fullWidth: false,
    width: '755px',
  };
};
