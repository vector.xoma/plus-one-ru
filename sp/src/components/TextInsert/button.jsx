import React, { useState } from 'react';
import { Popover, Button } from 'antd';
import { Input, Radio } from 'antd';
import styled from 'styled-components/macro';
import { Wrap } from '../Grid/Grid';

const EditorTextInsert = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  textarea {
    width: 500px;
    margin-bottom: 10px;
  }
`;

const { TextArea } = Input;

const TextInsertButton = ({ setTextInsert }) => {
  const [text, setText] = useState('');
  const [visible, setVisible] = useState(false);
  const [type, setType] = useState('frame');

  const Content = (
    <EditorTextInsert>
      <Wrap>
        <Radio.Group onChange={e => setType(e.target.value)} value={type}>
          <Radio value={'frame'}>Текст в рамке</Radio>
          <Radio value={'lines'}>Текст выделенный линиями</Radio>
        </Radio.Group>
      </Wrap>
      <Wrap>
        <div>
          <TextArea
            placeholder="Текст"
            value={text || ''}
            rows={4}
            onChange={e => setText(e.target.value)}
          />
        </div>
        <Button
          onClick={() => {
            setTextInsert({ textInsert: text, type });
            setVisible(false);
            setText('');
          }}
        >
          Добавить
        </Button>
      </Wrap>
    </EditorTextInsert>
  );

  return (
    <Popover
      content={Content}
      title="Текстовый врез"
      trigger="click"
      placement="right"
      visible={visible}
      onVisibleChange={visible => {
        setVisible(visible);
      }}
    >
      <Button>Текстовый врез</Button>
    </Popover>
  );
};

export default TextInsertButton;
