import React, { useContext } from 'react';
import styled, { ThemeContext } from 'styled-components/macro';
import { parserCustomBBCode } from '../Text/parser';

const styleTypeLine = {
  borderLeft: 'none',
  borderRight: 'none',
  paddingRight: '0',
  paddingLeft: '0',
  textAlign: 'left',
};

const TextInsertTitle = styled.div`
  max-width: 755px;
  margin: 0 auto;
  text-align: center;
  border: 3px solid;
  border-color: ${({ theme }) =>
    theme.border_color && theme.border_color !== ''
      ? theme.border_color
      : '#f8c946'};
  padding: 48px 80px;
  margin: 45px auto;
  font-family: 'MullerMedium';
  color: ${({ theme, themeContext }) =>
    theme.font_color !== '' ? theme.font_color : themeContext.color};
  .text-insert {
    margin: 0;
    p {
      margin: 0;
      font-weight: bold;
      line-height: 1.42;
      font-size: ${({ line }) => (line === 'lines' ? '24px' : '38px')};
      @media (max-width: 767px) {
        font-size: 21px;
        line-height: 26px;
      }
    }
    a {
      @media (max-width: 767px) {
        font-size: 21px;
        line-height: 26px;
      }
    }
  }
  ${({ line }) => (line === 'lines' ? styleTypeLine : {})}
  @media (max-width: 767px) {
    padding: 20px 40px;
    padding-left: ${({ line }) => (line === 'lines' ? '0' : '40px')};
    padding-right: ${({ line }) => (line === 'lines' ? '0' : '40px')};
    word-break: break-word;
    line-height: 24px;
  }
`;

const TextInsert = ({ data }) => {
  const rows = data.text.split('\n');
  const themeContext = useContext(ThemeContext);

  // data.type === lines - текст с линиями
  // data.type === frame - текст с рамкой

  if (rows.length)
    return (
      <div>
        <TextInsertTitle themeContext={themeContext} line={data.type}>
          {rows.map((r, i) => {
            return (
              <p key={i} className="text-insert">
                {parserCustomBBCode({ r }).component}
              </p>
            );
          })}
        </TextInsertTitle>
      </div>
    );

  return (
    <div>
      <TextInsertTitle>{parserCustomBBCode({ r: data.text })}</TextInsertTitle>
    </div>
  );
};

export default TextInsert;
