import React from 'react';
import { parserCustomBBCode } from 'components/Text/parser';
import {
  TitleDescription,
  TitleContainer,
  ImgElem,
  ImgWrap,
  Title,
} from './style';

const TitleBlock = ({
  info: { header, description } = {},
  rubric,
  staticTitle,
  src,
}) => {
  return (
    <>
      {rubric || staticTitle ? (
        <Title className="rubric-name">{rubric || staticTitle}</Title>
      ) : (
        <>
          <TitleContainer>
            <Title withDescription>{header}</Title>
            <TitleDescription>
              {parserCustomBBCode({ r: description }).component}
            </TitleDescription>
          </TitleContainer>
          {src && (
            <ImgWrap>
              <ImgElem src={src} />
            </ImgWrap>
          )}
        </>
      )}
    </>
  );
};

export default TitleBlock;
