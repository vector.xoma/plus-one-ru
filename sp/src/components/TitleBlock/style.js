import styled from 'styled-components/macro';

const TitleDescription = styled.div`
  color: #fff;
  max-width: 600px;
  font-family: 'MullerRegular';
  font-size: 19px;
  line-height: 24px;
  .textAll {
    color: inherit;
  }
`;

const Title = styled.h1`
  font-size: 68px;
  color: #fff;
  font-family: 'MullerBold';
  margin-bottom: 50px;
  margin-top: 45px;
  margin-right: 30px;
  line-height: 1;
  max-width: 500px;
  @media (max-width: 767px) {
    max-width: 365px;
    margin: ${({ withDescription }) =>
      withDescription ? '45px 0 50px' : '45px auto 50px'};
    font-size: 32px;
  }
`;
const TitleContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  margin: 0 auto;
  align-items: flex-start;
  margin: 50px 0 20px 0;
  ${Title} {
    margin-top: 0;
    margin-bottom: 0;
  }
`;
const ImgWrap = styled.div`
  width: 100%;
  height: 300px;
  margin-bottom: 20px;
  @media (max-width: 767px) {
    height: auto;
  }
`;
const ImgElem = styled.img`
  display: block;
  object-fit: cover;
  width: 100%;
  height: 100%;
  @media (max-width: 767px) {
    object-fit: fill;
  }
`;

export { TitleDescription, Title, TitleContainer, ImgElem, ImgWrap };
