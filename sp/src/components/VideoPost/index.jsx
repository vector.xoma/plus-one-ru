import React, { useEffect, useState } from 'react';
import { Transition } from 'react-transition-group';
import {
  CustomPlayerWrapper,
  CustomPlayerHeader,
  CustomPlayerImgWrap,
  CustomPlayerImg,
  CustomPlayerContentWrap,
  CustomPlayerVideoContainer,
  CustomPlayerControlsContainer,
  CustomPlayerControlsElem,
  CloseButton,
} from './style';

const VideoPost = ({ posts, img }) => {
  const [frameIndex, setFrameIndex] = useState(0);
  const [showPlayer, setShowPlayer] = useState(true);

  return (
    <Transition in={showPlayer} timeout={1000}>
      <CustomPlayerWrapper onClick={() => !showPlayer && setShowPlayer(true)}>
        <CustomPlayerHeader showPlayer={showPlayer}>
          {showPlayer && <CloseButton onClick={() => setShowPlayer(false)} />}
          <CustomPlayerImgWrap>
            <CustomPlayerImg src={img}></CustomPlayerImg>
          </CustomPlayerImgWrap>
        </CustomPlayerHeader>
        {showPlayer && (
          <CustomPlayerContentWrap>
            <CustomPlayerVideoContainer
              dangerouslySetInnerHTML={{
                __html: posts[frameIndex].frame,
              }}
            ></CustomPlayerVideoContainer>
            <CustomPlayerControlsContainer>
              {posts.map((fr, i) => {
                return (
                  <CustomPlayerControlsElem
                    key={i}
                    onClick={() => setFrameIndex(i)}
                  >
                    {fr.name}
                  </CustomPlayerControlsElem>
                );
              })}
            </CustomPlayerControlsContainer>
          </CustomPlayerContentWrap>
        )}
      </CustomPlayerWrapper>
    </Transition>
  );
};

export default VideoPost;
