import styled, { keyframes } from 'styled-components/macro';
import closeButton from '../../img/closePlayer.svg';

export const closeAnimation = keyframes`
    from {
      opacity: 1
    }
    to {
      opacity: 0;
    }
  `;

export const openAnimationReverse = keyframes`
    from {
       opacity: 0;
    }
    to {
       opacity: 1;
    }
  `;

export const CustomPlayerWrapper = styled.div`
  width: 100%;
  max-width: 1140px;
  margin: 20px auto;
  @media (max-width: 767px) {
    padding: 0 10px;
  }
`;
export const CustomPlayerHeader = styled.div`
  position: relative;
  cursor: ${({ showPlayer }) => !showPlayer && 'pointer'};
`;
export const CustomPlayerImgWrap = styled.div`
  width: 100%;
  height: 83px;
`;
export const CustomPlayerImg = styled.img`
  display: block;
  width: 100%;
  height: 100%;
  object-fit: cover;
`;
export const CustomPlayerContentWrap = styled.div`
  display: flex;
  flex-wrap: wrap;
  animation: ${({ showPlayer }) =>
      showPlayer ? closeAnimation : openAnimationReverse}
    0.4s;
  @media (max-width: 767px) {
    flex-direction: column;
  }
`;
export const CustomPlayerVideoContainer = styled.div`
  height: 480px;
  width: 100%;
  flex: 3;
  iframe {
    width: 100%;
    height: 100%;
  }
  @media (max-width: 767px) {
    height: auto;
  }
`;

export const CustomPlayerControlsContainer = styled.div`
  color: #fff;
  width: 100%;
  flex: 1;
  background-color: #2f2f2f;
`;

export const CustomPlayerControlsElem = styled.div`
  padding: 20px 14px 20px 28px;
  font-size: 16px;
  line-height: 20px;
  color: #fff;
  font-family: 'MullerRegular';
  transition: background-color ease 0.3s;
  cursor: pointer;
  :hover {
    background-color: #00000024;
  }
`;
export const CloseButton = styled.div`
  width: 18px;
  height: 18px;
  position: absolute;
  background-image: url(${closeButton});
  background-repeat: no-repeat;
  background-position: center;
  top: 32px;
  right: 42px;
  z-index: 10;
  cursor: pointer;
`;
