import styled from 'styled-components/macro';
import icon from '../../img/iconFolder.png';

export const BtnFindImg = styled.button`
  background-image: url(${icon});
  width: 50px;
  height: 32px;
  background-position: center;
  background-repeat: no-repeat;
  margin-left: 20px;
  border-radius: 4px;
  border: 1px solid #d9d9d9;
  background-color: #fff;
  transition: background-color 0.3s;
  :hover {
    background-color: #d9d9d9;
  }
`;

export const FilesControl = styled.div`
  display: flex;
  width: 100%;
  margin: 5px 0;
`;
