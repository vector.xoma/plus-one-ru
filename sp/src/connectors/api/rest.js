import axios from 'axios';

const instance = axios.create({
  // baseURL: "/api"
});

const axiosApi = async () => instance;

export const APIService = {
  async logError(message, err) {
    const { response: { status = '' } = {} } = err;
    console.group(
      `%c${message}`,
      'color: white; background-color: red; padding: 3px;',
    );
    console.error(err);
    if (status === 401) {
      console.error('response error status: 401');
    }
    if (status === 403) {
      console.error('response error status: 403');
    }
    if (status === 500) {
      console.error('response error status: 500');
    }
    if (status === '') {
      console.error('Response error status does not available');
    }
    console.groupEnd();
  },

  async GET(route, params) {
    try {
      const API = await axiosApi();
      const { data } = await API.get(route, { params });
      return data;
    } catch (err) {
      this.logError(`Ошибка в GET запросе: ${route}`, err);
    }
  },

  async POST(route, params, { YApay = false } = {}) {
    try {
      const API = await axiosApi();
      if (YApay) {
        const { request } = await API.post(route, params);
        return { request };
      }
      const { data } = await API.post(route, params);
      return data;
    } catch (err) {
      this.logError(`Ошибка в POST запросе: ${route}`, err);
      throw err;
    }
  },

  async DELETE(route, params) {
    try {
      const API = await axiosApi();
      const { data } = await API.delete(route, params);
      return data;
    } catch (err) {
      this.logError(`Ошибка в DELETE запросе: ${route}`, err);
    }
  },
  async PUT(route, params) {
    try {
      const API = await axiosApi();
      const { data } = await API.put(route, params);
      return data;
    } catch (err) {
      this.logError(`Ошибка в PUT запросе: ${route}`, err);
    }
  },
};
