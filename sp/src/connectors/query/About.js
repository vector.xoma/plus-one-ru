import { APIService } from 'connectors/api/rest';
import { DOMAIN, API_V1 } from '../constants';

export const About = {
  async getPage() {
    try {
      return await APIService.GET(`${DOMAIN}/${API_V1}/get-content/about`);
    } catch (e) {
      throw e;
    }
  },
};
