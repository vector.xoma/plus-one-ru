import { APIService } from 'connectors/api/rest';
import { DOMAIN, API_V1 } from '../constants';

export const Article = {
  async getPage(path) {
    try {
      let _path = path;
      //убираем '/' с конца пути, если после него ничего не идёт
      if (_path[_path.length - 1] === '/') _path = _path.slice(0, -1);

      return await APIService.GET(`${DOMAIN}/${API_V1}/get-content${_path}`);
    } catch (e) {
      throw e;
    }
  },
  async getNextPage(path) {
    try {
      return await APIService.GET(`${DOMAIN}/${API_V1}/${path}`);
    } catch (e) {
      throw e;
    }
  },
  async getNews(path) {
    try {
      let _path = path;
      //убираем '/' с конца пути, если после него ничего не идёт
      if (_path[_path.length - 1] === '/') _path = _path.slice(0, -1);

      return await APIService.GET(`${DOMAIN}/${API_V1}/${_path}`);
    } catch (e) {
      throw e;
    }
  },
  async getStableDevelopment(path) {
    try {
      let _path = path;
      //убираем '/' с конца пути, если после него ничего не идёт
      if (_path[_path.length - 1] === '/') _path = _path.slice(0, -1);

      return await APIService.GET(
        `${DOMAIN}/${API_V1}/stable-development/${_path}`,
      );
    } catch (e) {
      throw e;
    }
  },
};
