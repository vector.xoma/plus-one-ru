import { APIService } from 'connectors/api/rest';
import { DOMAIN, API_V1 } from '../constants';

export const Blocks = {
  async getList(rubric, page = 1) {
    try {
      return await APIService.GET(`${DOMAIN}/${API_V1}/${rubric}/${page}`);
    } catch (e) {
      throw e;
    }
  },
  async getTagList(rubric, page = 1) {
    try {
      return await APIService.GET(`${DOMAIN}/${API_V1}/${rubric}/${page}`);
    } catch (e) {
      throw e;
    }
  },
  async getRows() {
    try {
      return await APIService.GET(`${DOMAIN}/${API_V1}/main-page`);
    } catch (e) {
      throw e;
    }
  },
  async getNews(page) {
    try {
      return await APIService.GET(`${DOMAIN}/${API_V1}/news/${page}`);
    } catch (e) {
      throw e;
    }
  },

  async getCurrentNews(path) {
    try {
      return await APIService.GET(`${DOMAIN}/${API_V1}/news${path}`);
    } catch (e) {
      throw e;
    }
  },
};
