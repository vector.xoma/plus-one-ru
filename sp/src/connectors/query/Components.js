import { APIService } from 'connectors/api/rest';

export const Components = {
  async getList(params) {
    try {
      return await APIService.GET('components', params);
    } catch (e) {
      throw e;
    }
  },

  async saveData(params) {
    try {
      return await APIService.POST('components', params);
    } catch (e) {
      throw e;
    }
  },

  async updateData({ id, ...params }) {
    try {
      return await APIService.PUT(`components/${id}`, params);
    } catch (e) {
      throw e;
    }
  },

  async remove(id) {
    try {
      return await APIService.DELETE(`components/${id}`);
    } catch (e) {
      throw e;
    }
  },
};
