import { APIService } from 'connectors/api/rest';
import { DOMAIN, API_V1, ADMIN } from '../constants';

export const Files = {
  async getFolder(params) {
    try {
      return await APIService.GET(
        `${DOMAIN}/${ADMIN}/${API_V1}/file-manager`,
        params,
      );
    } catch (e) {
      throw e;
    }
  },
  async PostFile(params) {
    try {
      return await APIService.POST(
        `${DOMAIN}/${ADMIN}/${API_V1}/file-manager`,
        params,
      );
    } catch (e) {
      throw e;
    }
  },
};
