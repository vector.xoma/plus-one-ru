import { APIService } from 'connectors/api/rest';
import { DOMAIN, API_V1, ADMIN } from '../constants';

export const Galleries = {
  async getList() {
    try {
      return await APIService.GET(`${DOMAIN}/${ADMIN}/${API_V1}/galleries`);
    } catch (e) {
      throw e;
    }
  },

  async getGallery(id) {
    try {
      return await APIService.GET(
        `${DOMAIN}/${ADMIN}/${API_V1}/galleries/${id}`,
      );
    } catch (e) {
      throw e;
    }
  },
};
