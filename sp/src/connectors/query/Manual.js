import { APIService } from 'connectors/api/rest';
import { DOMAIN, API_V1 } from '../constants';

export const Manual = {
  async getList() {
    try {
      return await APIService.GET(`${DOMAIN}/${API_V1}/manual`);
    } catch (e) {
      throw e;
    }
  },
};
