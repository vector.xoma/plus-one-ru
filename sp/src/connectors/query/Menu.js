import { APIService } from 'connectors/api/rest';
import { DOMAIN, API_V1 } from '../constants';

export const Menu = {
  async getList() {
    try {
      return await APIService.GET(`${DOMAIN}/${API_V1}/main-menu`);
    } catch (e) {
      throw e;
    }
  },
};
