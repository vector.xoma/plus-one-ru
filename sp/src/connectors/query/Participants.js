import { APIService } from 'connectors/api/rest';
import { DOMAIN, API_V1 } from '../constants';

export const Participants = {
  async getList() {
    try {
      return await APIService.GET(`${DOMAIN}/${API_V1}/author`);
    } catch (e) {
      throw e;
    }
  },
};
