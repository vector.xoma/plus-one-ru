import { APIService } from 'connectors/api/rest';

export const Payments = {
  async postPayments(values) {
    try {
      return await APIService.POST(
        `https://platform.plus-one.rupayments`,
        values,
      );
    } catch (e) {
      throw e;
    }
  },
  async yandexPayments(values) {
    try {
      return await APIService.POST(
        `https://money.yandex.ru/eshop.xml`,
        values,
        { YApay: true },
      );
    } catch (e) {
      throw e;
    }
  },
};
