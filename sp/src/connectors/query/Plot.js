import { APIService } from 'connectors/api/rest';
import { DOMAIN, API_V1 } from '../constants';
export const Plot = {
  async getPlot() {
    try {
      return await APIService.GET(`${DOMAIN}/${API_V1}/story`);
    } catch (e) {
      throw e;
    }
  },
  // TODO:Запрос тестовый
  async getInnerPlot(plotName) {
    try {
      return await APIService.GET(`${DOMAIN}/${API_V1}/story/${plotName}`);
    } catch (e) {
      throw e;
    }
  },
};
