import { APIService } from 'connectors/api/rest';
import { DOMAIN, API_V1 } from '../constants';

export const Search = {
  async getList(params) {
    try {
      return await APIService.GET(`${DOMAIN}/api/autocomplette`, params);
    } catch (e) {
      throw e;
    }
  },
  async getSearchResult(params, page) {
    try {
      return await APIService.GET(
        `${DOMAIN}/${API_V1}/search/${params}/${page}`,
        params,
      );
    } catch (e) {
      throw e;
    }
  },
};
