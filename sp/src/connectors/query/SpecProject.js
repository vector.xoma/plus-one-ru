import { APIService } from 'connectors/api/rest';
import { DOMAIN, API_V1 } from '../constants';

export const SpecProject = {
  async get() {
    try {
      return await APIService.GET(`${DOMAIN}/${API_V1}/special-projects`);
    } catch (e) {
      throw e;
    }
  },
};
