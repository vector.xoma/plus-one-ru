import { APIService } from 'connectors/api/rest';
import { DOMAIN, API_V1 } from '../constants';

export const StableDevelopment = {
  async getList() {
    try {
      return await APIService.GET(`${DOMAIN}/${API_V1}/stable-development`);
    } catch (e) {
      throw e;
    }
  },
};
