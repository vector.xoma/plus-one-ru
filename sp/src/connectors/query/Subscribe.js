import { APIService } from 'connectors/api/rest';
import { DOMAIN, API_V1 } from '../constants';

export const Subscribe = {
  async postMail(mail) {
    try {
      return await APIService.POST(`${DOMAIN}/${API_V1}/subscribe?email=${mail}`, {email: mail});
    } catch (e) {
      throw e;
    }
  },
  async confirm(hash) {
    try {
      return await APIService.POST(`${DOMAIN}/${API_V1}/confirm-subscribe/${hash}`, {hash: hash});
    } catch (e) {
      throw e;
    }
  },
};
