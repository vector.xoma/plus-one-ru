import { APIService } from 'connectors/api/rest';
import { DOMAIN, API_V1 } from '../constants';

export const Tags = {
  async getList(type, page) {
    try {
      return await APIService.GET(`${DOMAIN}/${API_V1}/${type}/${page}`);
    } catch (e) {
      throw e;
    }
  },
};
