import { APIService } from 'connectors/api/rest';
import { DOMAIN, API_V1 } from '../constants';

export const Widget = {
  async getWidget() {
    try {
      return await APIService.GET(`${DOMAIN}/${API_V1}/news-widget`);
    } catch (e) {
      throw e;
    }
  },
};
