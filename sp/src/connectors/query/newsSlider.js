import { APIService } from 'connectors/api/rest';
import { DOMAIN, API_V1 } from '../constants';

export const News = {
  async getList() {
    try {
      return await APIService.GET(`${DOMAIN}/${API_V1}/main-news`);
    } catch (e) {
      throw e;
    }
  },
};
