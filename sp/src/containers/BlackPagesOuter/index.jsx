import React, { useState, useEffect } from 'react';
import _uniqueId from 'lodash/uniqueId';
import styled from 'styled-components/macro';

import useCleanParamsUrl from 'hooks/useCleanParamsUrl';
import useDesktop from 'hooks/useDesktop';
import DropDown from 'components/DropDown';
import HeaderComponent from 'components/HeaderComponent/HeaderComponent';
import SubHeaderNav from 'components/SubHeaderNav';
import HeaderBottom from 'components/HeaderBottom';
import Informer from 'components/Informer';
import Footer from 'components/Footer';
import Banner from 'components/Banner';
import ResultHeader from 'components/ResultHeader';
import { useSearchInput } from 'contexts/searchInputContext';

const MainContainer = styled.div`
  height: auto;
  background: ${({ colorPage }) => colorPage};
  overflow: hidden;
  min-height: 100vh;
  .post3 {
    margin-left: auto;
    margin-right: auto;
  }
`;

const BlackPagesOuter = ({
  children,
  isMainPage,
  isErrorPage,
  isAboutPage,
  isRubricPage,
  isSearchPage = false,
  searchTitle,
  listLength,
  setPage,
}) => {
  //  useCleanParamsUrl(['?fbclid=', '?utm_']);

  const isDesktop = useDesktop();
  const [visible, setVisible] = useState(false);
  const [colorPage, setColorPage] = useState('#212121');
  const isFooter =
    isAboutPage || isErrorPage || isMainPage || isRubricPage || isSearchPage;
  useEffect(() => {
    if (isErrorPage) {
      setColorPage('#121212');
    } else if (!isAboutPage && !isMainPage && !isRubricPage && !isSearchPage) {
      setColorPage('#EAEAEA');
    } else {
      setColorPage('#212121');
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isAboutPage, isMainPage, isErrorPage]);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const { activeAnimationHeader, setActiveAnimationHeader } = useSearchInput();

  return (
    <MainContainer isAboutPage colorPage={colorPage}>
      <Banner
        bannerStyle={{ backgroundColor: '#212121' }}
        p2Desctop="gokf"
        p2Mobile="gokg"
      />
      {!isErrorPage && (
        <>
          <DropDown visible={visible} setVisible={setVisible} />
          <HeaderBottom
            visible={visible}
            setVisible={setVisible}
            isMainPage={isMainPage}
            isAboutPage={isAboutPage}
            isRubricPage={isRubricPage}
            isblackpage={1}
            setPage={setPage}
            activeAnimationHeader={activeAnimationHeader}
            setActiveAnimationHeader={setActiveAnimationHeader}
          />
          {isDesktop &&
            (isAboutPage || isMainPage || isRubricPage || isSearchPage) && (
              <SubHeaderNav />
            )}

          {searchTitle && (
            <ResultHeader searchTitle={searchTitle} listLength={listLength} />
          )}
        </>
      )}
      {children}
      {isFooter && <Footer />}
    </MainContainer>
  );
};

export default BlackPagesOuter;
