import React from 'react';
import { Link } from 'react-router-dom';
import { ThemeProvider } from 'styled-components/macro';
import { useLocation } from 'react-router-dom';

import useMetaTags from 'utils/hooks/useMetaTags';
import TitleBlock from 'components/TitleBlock';

import {
  CountersContainer,
  CounterNumWrap,
  CountersTitle,
  CounterDescr,
  CounterWrap,
  CounterText,
  CounterNum,
} from './style';
// TODO: логика заграшивания в градиент списка
// если не нужна, убрать
const toStr = ({ r, g, b }) =>
  `rgb(${parseInt(r)}, ${parseInt(g)}, ${parseInt(b)})`;

const CountersPage = ({
  header = '',
  data = [],
  meta = {},
  titleBlock,
  showCount,
  textColor,
  src,
  rubric,
  writerInfo,
}) => {
  useMetaTags({ meta });
  const { pathname } = useLocation();
  const isGrowth = pathname.includes('устойчивое-развитие');
  const theme = { color: '#fff' };
  const quantity = data.length;
  const greenGradient = [24, 214, 0, quantity, 191, 244, 199];
  const yellowGradient = [250, 214, 0, quantity, 250, 245, 215];
  const blueGradient = [0, 186, 233, quantity, 201, 238, 247];
  const grayGradient = [143, 143, 143, quantity, 54, 54, 54];
  const colorSet = {
    ecology: greenGradient,
    society: yellowGradient,
    economy: blueGradient,
    platform: grayGradient,
  };

  const colorChange = (r, g, b, q, rEnd, gEnd, bEnd, gray = false) => {
    const rangeColorR = (rEnd - r) / (q - 1);
    const rangeColorG = (gEnd - g) / (q - 1);
    let rangeColorB = (bEnd - b) / (q - 1);
    if (gray) {
      rangeColorB = ((bEnd - b) / (q - 1)) * -1;
    }
    let colors = [];

    for (let i = 0; i < q; i++) {
      colors.push({
        r: r + rangeColorR * i,
        g: g + rangeColorG * i,
        b: b + rangeColorB * i,
      });
    }
    return colors;
  };

  let colors = colorSet[rubric]
    ? colorChange(...colorSet[rubric])
    : colorChange(...greenGradient);

  return (
    <>
      <CountersContainer>
        <ThemeProvider theme={theme}>
          {!titleBlock ? (
            <CountersTitle>{header}</CountersTitle>
          ) : (
            <TitleBlock info={writerInfo} src={src} />
          )}
          {data &&
            data.map(
              ({ url, total, countPosts: { text } = {}, name, id }, index) => {
                return (
                  <Link
                    to={
                      isGrowth
                        ? `/${decodeURI('устойчивое-развитие')}/${url}`
                        : `/author/${url}`
                    }
                    key={id}
                  >
                    <CounterWrap>
                      <CounterText color={textColor || toStr(colors[index])}>
                        {name}
                      </CounterText>
                      {showCount && (
                        <CounterNumWrap>
                          <CounterNum>{total}</CounterNum>
                          <CounterDescr>{text}</CounterDescr>
                        </CounterNumWrap>
                      )}
                    </CounterWrap>
                  </Link>
                );
              },
            )}
        </ThemeProvider>
      </CountersContainer>
    </>
  );
};

export default CountersPage;
