import styled from 'styled-components/macro';

const CountersContainer = styled.div`
  width: 100%;
  max-width: 1140px;
  margin: 0 auto;
  @media (max-width: 767px) {
    width: calc(100% - 20px);
  }
`;

const CountersTitle = styled.h1`
  font-size: 68px;
  color: ${({ theme }) => theme && theme.color};
  font-family: 'MullerBold', sans-serif;
  margin-bottom: 50px;
  margin-top: 65px;
  @media (max-width: 767px) {
    font-size: 32px;
    padding: 0 20px;
  }
`;

const CounterWrap = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-wrap: wrap;
  padding-bottom: 40px;
  margin-bottom: 40px;
  border-bottom: 1px solid #252525;
  /* padding: 0 10px; */
  :last-child {
    margin: 0;
  }
  @media (max-width: 767px) {
    padding: 10px 0;
    margin-bottom: 0;
    align-items: center;
  }
`;

const CounterText = styled.span`
  font-size: 63px;
  text-transform: uppercase;
  font-family: 'GeometricSansSerifv1';
  max-width: 900px;
  color: ${({ color }) => color};
  :hover {
    color: ${({ color }) => color};
  }
  @media (max-width: 767px) {
    max-width: 210px;
    font-size: 18px;
    word-break: break-word;
  }
`;
const CounterNumWrap = styled.div`
  font-family: 'GeometricSansSerifv1';
`;

const CounterNum = styled.p`
  font-size: 91px;
  color: #fff;
  margin: 0;
  @media (max-width: 767px) {
    font-size: 31px;
  }
`;

const CounterDescr = styled.p`
  font-size: 19px;
  font-family: 'MullerRegular';
  text-transform: uppercase;
  color: #fff;
  margin: 0;
  text-align: right;
  @media (max-width: 767px) {
    font-size: 7px;
  }
`;

export {
  CountersContainer,
  CountersTitle,
  CounterWrap,
  CounterText,
  CounterNumWrap,
  CounterNum,
  CounterDescr,
};
