import React from 'react';
import Block from 'components/Block';
import Masonry from 'react-masonry-component';
import InfiniteScroll from 'react-infinite-scroll-component';
import useDesktop from 'hooks/useDesktop';
import TitleBlock from 'components/TitleBlock';

import {
  masonryMobileLayout,
  styleFact,
  styleNews,
  stylePost,
  styleQuote,
  TagTitle,
} from './style';

const styleConfig = {
  post: stylePost,
  citate: styleQuote,
  factday: styleFact,
  news: styleNews,
};

const rebricsTranslate = {
  society: 'Общество',
  ecology: 'Экология',
  economy: 'Экономика',
};

const imgSrc = (rubricValues, isDesktop) => {
  if (rubricValues) {
    return isDesktop ? rubricValues.imageDesktop : rubricValues.imageMobile;
  }
};

// в случае если пришел post с картинкой - заменяем min-height
const styleSetter = (typePost = 'post', image = false) => {
  if (typePost === 'post' && image)
    return {
      ...styleConfig[typePost],
      container: { ...stylePost.container, minHeight: '505px' },
    };
  if (typePost === 'news' && image)
    return {
      ...styleConfig[typePost],
      container: { ...styleNews.container, minHeight: '505px' },
    };

  return styleConfig[typePost];
};

const MasonryPage = ({
  incrementPage,
  rubricValues,
  blocks = [],
  staticTitle,
  tagHeader,
  rubric,
}) => {
  const isDesktop = useDesktop();
  return (
    <>
      {/* TODO: уточнить, где выводится данный заголовок */}
      {tagHeader && <TagTitle className="rubric-name">{tagHeader}</TagTitle>}

      <InfiniteScroll
        dataLength={blocks && blocks.length}
        next={incrementPage}
        hasMore={true}
        style={{
          overflowY: 'hidden ',
        }}
        endMessage={''}
      >
        <TitleBlock
          src={imgSrc(rubricValues, isDesktop)}
          rubric={rebricsTranslate[rubric]}
          staticTitle={staticTitle}
          info={rubricValues}
        />
        <Masonry
          options={{
            gutter: 22,
          }}
        >
          {blocks.map(({ id, typePost, ...other }) => (
            <Block
              key={id}
              size={'1_3'}
              type={typePost}
              data={{
                ...other,
                block: '1_3',
              }}
              style={styleSetter(typePost, other.image)}
              masonryMobileLayout={masonryMobileLayout}
              rubricValues={rubricValues}
              isMasonryPage
            />
          ))}
        </Masonry>
      </InfiniteScroll>
    </>
  );
};

export default MasonryPage;
