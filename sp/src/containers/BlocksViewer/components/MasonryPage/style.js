import styled from 'styled-components/macro';

const stylePost = {
  container: {
    minHeight: '320px',
    marginBottom: '22px',
    maxWidth: '365px',
    justifyContent: 'flex-end',
  },
  textWrap: { margin: 0, textAlign: 'left' },
  textTitle: {
    fontSize: '30px',
    fontFamily: "'MullerBold', sans-serif",
    lineHeight: '1.2',
    textAlign: 'left',
  },
  textSubTitle: {
    textAlign: 'left',
    minHeight: 'initial',
    zIndex: '6',
    position: 'relative',
    fontSize: '19px',
    lineHeight: '1.26',
  },
};

const styleQuote = {
  container: {
    minHeight: '320px',
    margin: '0',
    marginBottom: '22px',
    maxWidth: '365px',
  },
  textTitle: {
    fontSize: '27px',
    textAlign: 'left',
    lineHeight: '1.07',
  },
};

const styleFact = {
  container: {
    maxWidth: '365px',
    minHeight: '320px',
    height: 'auto',
    marginBottom: '22px',
  },
  textTitle: {
    fontSize: '36px',
    lineHeight: 1.06,
  },
};

const styleNews = {
  container: {
    marginLeft: '0',
    marginRight: '0',
    maxWidth: '365px',
    width: '365px',
    minHeight: '320px',
    height: 'auto',
    marginBottom: '22px',
  },
  textTitle: {
    // fontSize: '36px',
    // lineHeight: 1.06,
  },
};

const masonryMobileLayout = {
  left: '50% !important',
  transform: 'translateX(-50%) !important',
};

const Title = styled.h1`
  font-size: 68px;
  color: #fff;
  font-family: 'MullerBold';
  margin-bottom: 50px;
  margin-top: 45px;
  margin-right: 30px;
  line-height: 1;
  max-width: 640px;
  @media (max-width: 767px) {
    max-width: 365px;
    margin: ${({ withDescription }) =>
      withDescription ? '45px 0 50px' : '45px auto 50px'};
    font-size: 32px;
  }
`;

const TagTitle = styled.h1`
  height: 29px;
  font-family: 'MullerBold';
  font-size: 28px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.32;
  letter-spacing: normal;
  text-align: left;
  color: #ffffff;
  margin: 49px 0 25px 0;
  height: 100%;
  @media (max-width: 767px) {
    font-size: 19.7px;
    margin: 20px 0 24px 0px;
    line-height: 1.16;
  }
`;

export {
  masonryMobileLayout,
  styleQuote,
  styleNews,
  stylePost,
  styleFact,
  TagTitle,
  // ImgWrap,
  // ImgElem,
  Title,
};
