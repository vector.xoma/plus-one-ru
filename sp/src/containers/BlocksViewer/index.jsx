import React from 'react';

import RowBlock from 'components/Block/components/RowBlock';
import MasonryPage from './components/MasonryPage';
import useMetaTags from 'utils/hooks/useMetaTags';
import styled from 'styled-components/macro';

const BlocksViewerContainer = styled.div`
  color: #fff;
  margin: 0 auto;
  max-width: 1140px;
  @media (max-width: 767px) {
    padding: 0 10px;
  }
`;

const BlocksViewer = ({
  rowsOfBlocks = [],
  blockList = [],
  masonry = false,
  incrementPage = _ => _,
  isMainPage = false,
  isPlotPage = false,
  rubricValues,
  staticTitle,
  rubric,
  meta,
}) => {
  useMetaTags({ meta, isMainPage });

  return (
    <>
      <BlocksViewerContainer>
        {masonry && !isPlotPage ? (
          <MasonryPage
            tagHeader={meta && meta.header}
            incrementPage={incrementPage}
            rubricValues={rubricValues}
            staticTitle={staticTitle}
            isPlotPage={isPlotPage}
            blocks={blockList}
            rubric={rubric}
          />
        ) : (
          rowsOfBlocks.map(({ rowContent = [{}], rowType, type }, i) => (
            <RowBlock
              groupName={rowContent[0].name}
              content={rowContent}
              rowType={rowType}
              typePost={type}
              isMainPage
              key={i}
            />
          ))
        )}
      </BlocksViewerContainer>
    </>
  );
};

export default BlocksViewer;
