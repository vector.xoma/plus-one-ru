import React, { useState } from 'react';
import { Icon, Dropdown } from 'antd';
import defaultImg from 'img/default_img.jpeg';
import { getExt, imgFormats } from './constants';
import {
  FolderImg,
  ElementWrap,
  ElementName,
  ArrowContainer,
  ListFileImg,
  ListFolderImg,
  ListElementWrap,
  ListArrowContainer,
  ListElementData,
  TableElementWrap,
  TableElementData,
  BlockImg,
} from './styles';
import './style.scss';
import IconGroup from './components/IconGroup';
import ContextMenu from './components/ContextMenu';

const Element = ({
  onClick,
  type,
  fullFileName,
  fileName,
  mode,
  humanFileTime,
  humanFileSize,
  position,
  setData,
  extension,
  setIsAnyModals,
}) => {
  const [contextModal, setContextModal] = useState(false);
  const preparedFileName = fileName && fileName.replace(`.${extension}`, '');
  const matchImg = extension && imgFormats.includes(extension.toLowerCase());
  if (mode === 'list') {
    if (type === 'arrow') {
      return (
        <ListElementWrap onClick={onClick}>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <ListArrowContainer>
              <Icon type="arrow-left" />
            </ListArrowContainer>
            <span style={{ marginLeft: 25 }}>Назад</span>
          </div>
        </ListElementWrap>
      );
    }

    return (
      <Dropdown
        overlay={
          <ContextMenu
            onClick={onClick}
            type={type}
            fileName={fileName}
            fullFileName={fullFileName}
            humanFileTime={humanFileTime}
            setContextModal={setContextModal}
            contextModal={contextModal}
          />
        }
        trigger={['contextMenu']}
      >
        <ListElementWrap onClick={onClick}>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            {type === 'file' ? (
              <ListFileImg src={matchImg ? fullFileName : defaultImg} />
            ) : (
              <ListFolderImg />
            )}
            <span style={{ marginLeft: 15 }}>{preparedFileName}</span>
          </div>
          <ListElementData width="380px">
            <div className="ext">{type === 'file' && getExt(fileName)}</div>
            <div className="date">{humanFileTime}</div>
            <div className="size">
              {(type === 'file' && humanFileSize) || 'no data'}
            </div>

            <IconGroup
              setIsAnyModals={setIsAnyModals}
              extension={extension}
              setData={setData}
              path={position}
              type={type}
              fullFileName={fullFileName}
              preparedFileName={preparedFileName}
              fileName={fileName}
              position="flex-start"
              wrapperWidth="90px"
            />
          </ListElementData>
        </ListElementWrap>
      </Dropdown>
    );
  }

  if (mode === 'table') {
    if (type === 'arrow') {
      return (
        <TableElementWrap onClick={onClick}>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <ListArrowContainer>
              <Icon type="arrow-left" />
            </ListArrowContainer>
            <span style={{ marginLeft: 25 }}>Назад</span>
          </div>
        </TableElementWrap>
      );
    }

    return (
      <Dropdown
        overlay={
          <ContextMenu
            onClick={onClick}
            type={type}
            fileName={fileName}
            fullFileName={fullFileName}
            humanFileTime={humanFileTime}
            setContextModal={setContextModal}
            contextModal={contextModal}
          />
        }
        trigger={['contextMenu']}
      >
        <TableElementWrap onClick={onClick} title={preparedFileName}>
          <TableElementData style={{ width: '80%' }}>
            {type === 'file' ? (
              <ListFileImg src={matchImg ? fullFileName : defaultImg} />
            ) : (
              <ListFolderImg />
            )}
            <div className="name" style={{ marginLeft: 15, width: 190 }}>
              {preparedFileName}
            </div>
          </TableElementData>
          <IconGroup
            setIsAnyModals={setIsAnyModals}
            extension={extension}
            setData={setData}
            path={position}
            type={type}
            fullFileName={fullFileName}
            preparedFileName={preparedFileName}
            fileName={fileName}
            position="flex-end"
          />
        </TableElementWrap>
      </Dropdown>
    );
  }

  if (type === 'arrow') {
    return (
      <ElementWrap onClick={onClick}>
        <ArrowContainer>
          <Icon type="arrow-left" />
        </ArrowContainer>
      </ElementWrap>
    );
  }

  return (
    <Dropdown
      overlay={
        <ContextMenu
          onClick={onClick}
          type={type}
          fileName={fileName}
          fullFileName={fullFileName}
          humanFileTime={humanFileTime}
          setContextModal={setContextModal}
          contextModal={contextModal}
        />
      }
      trigger={['contextMenu']}
    >
      <ElementWrap onClick={onClick} title={preparedFileName}>
        {type === 'file' ? (
          <BlockImg src={matchImg ? fullFileName : defaultImg} />
        ) : (
          <FolderImg />
        )}
        <div
          className="decription"
          style={{ width: '100%', height: 25, position: 'relative' }}
        >
          <ElementName
            className="hover_animation"
            style={{
              position: 'absolute',
              overflow: 'hidden',
              textOverflow: 'ellipsis',
              whiteSpace: 'nowrap',
              width: '100%',
            }}
          >
            {preparedFileName}
          </ElementName>
          <IconGroup
            setIsAnyModals={setIsAnyModals}
            extension={extension}
            setData={setData}
            path={position}
            type={type}
            fullFileName={fullFileName}
            preparedFileName={preparedFileName}
            fileName={fileName}
          />
        </div>
      </ElementWrap>
    </Dropdown>
  );
};

export default Element;
