import React, { useState, useEffect } from 'react';
import { Upload, Icon, Input, Breadcrumb, Dropdown, Menu, Button } from 'antd';
import { Files } from 'connectors/query/Files';

import { menuRows, showMods } from './constants';
import {
  StyledBreadcrumb,
  BreadcrumbWrupper,
  AddFileIcon,
  AddFolderIcon,
  ElementsCounter,
  CarretIcon,
} from './styles';

const FileManagerHeader = ({
  setVisibleFolderModal,
  setFilteredFiles,
  setShowMode,
  setPosition,
  setData,
  setSort,
  showMode,
  sort,
  position,
  currentFiles,
}) => {
  const [folderCounter, setFolderCounter] = useState(0);
  const [fileCounter, setFileCounter] = useState(0);

  const filterFile = e => {
    const filterFile = f =>
      f.fileName.toLowerCase().includes(e.target.value.toLowerCase());
    setFilteredFiles(currentFiles.filter(filterFile));
  };

  useEffect(() => {
    let files = 0;
    let folders = 0;

    currentFiles.forEach(f => {
      if (f.type === 'folder') folders += 1;
      if (f.type === 'file') files += 1;
    });

    setFileCounter(files);
    setFolderCounter(folders);
  }, [currentFiles]);

  const onSortChange = type => {
    setSort({ direction: sort.type === type && !sort.direction, type });
  };

  const menu = (
    <Menu>
      <strong style={{ marginLeft: 5 }}>Сортировка</strong>
      {Object.keys(menuRows).map((mr, i) => (
        <Menu.Item key={i} onClick={() => onSortChange(mr)}>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            {sort.type === mr && sort.direction && (
              <CarretIcon type="caret-up" />
            )}
            {sort.type === mr && !sort.direction && (
              <CarretIcon type="caret-down" />
            )}
            {sort.type !== mr && <div style={{ width: '20px' }} />}
            {menuRows[mr]}
          </div>
        </Menu.Item>
      ))}
    </Menu>
  );

  return (
    <>
      <div style={{ display: 'flex' }}>
        <Upload
          name="file"
          multiple
          showUploadList={false}
          beforeUpload={file => {
            const formData = new FormData();
            formData.append(`files`, file);
            formData.append(`dir`, position.join('/'));
            Files.PostFile(formData)
              .then(result => {
                setData(result);
                setVisibleFolderModal(false);
              })
              .catch(err => {
                throw err;
              });
          }}
        >
          <AddFileIcon type="upload" title="Загрузить файл" />
        </Upload>

        <AddFolderIcon
          type="folder-add"
          title="Создать папку"
          onClick={() => setVisibleFolderModal(true)}
        />

        <Input
          addonBefore="Фильтр"
          placeholder="Фильтр"
          onChange={filterFile}
          style={{ width: 200 }}
        />
      </div>

      <BreadcrumbWrupper>
        <div style={{ display: 'flex' }}>
          <Breadcrumb>
            <StyledBreadcrumb onClick={() => setPosition([])}>
              Home
            </StyledBreadcrumb>
            {position &&
              position.length > 0 &&
              position.map((p, i) => (
                <StyledBreadcrumb
                  key={i}
                  onClick={() => setPosition(position.slice(0, i + 1))}
                >
                  {p}
                </StyledBreadcrumb>
              ))}
          </Breadcrumb>

          <ElementsCounter>
            (Файлов: {fileCounter}, Папок: {folderCounter})
          </ElementsCounter>
        </div>

        <div>
          {Object.keys(showMods).map((sm, i) => (
            <Button
              key={i}
              style={{
                color: showMode === sm ? 'white' : 'black',
                backgroundColor: showMode === sm ? 'black' : 'white',
                marginRight: 5,
              }}
              onClick={() => setShowMode(sm)}
            >
              {showMods[sm]}
            </Button>
          ))}
        </div>

        <Dropdown overlay={menu} placement="bottomLeft">
          <Button>
            <Icon type="sort-ascending" />
          </Button>
        </Dropdown>
      </BreadcrumbWrupper>
    </>
  );
};

export default FileManagerHeader;
