import React from 'react';
import { Icon, Menu, Button, Row, Col } from 'antd';
import ImgModal from '../ImgModal';

const ContextMenu = ({
  type,
  fileName,
  fullFileName,
  humanFileTime,
  setContextModal,
  contextModal,
  onClick = _ => _,
}) => {
  return (
    <>
      <Menu>
        {type !== 'folder' && (
          <Menu.Item
            style={{ maxWidth: 200, width: 200 }}
            key="1"
            onClick={onClick}
          >
            Выбрать
          </Menu.Item>
        )}
        <Menu.Item
          style={{ maxWidth: 200, width: 200 }}
          key="2"
          onClick={() => setContextModal(true)}
        >
          <Icon type="link" /> Показать URL
        </Menu.Item>

        <Menu.Item
          style={{ maxWidth: 200, width: 200, cursor: 'auto' }}
          key="3"
        >
          СВОЙСТВА ФАЙЛА
        </Menu.Item>
        <Menu.Item
          style={{ maxWidth: 200, width: 200, cursor: 'auto' }}
          key="4"
        >
          <Icon type="profile" /> {fileName}
        </Menu.Item>
        <Menu.Item
          style={{ maxWidth: 200, width: 200, cursor: 'auto' }}
          key="5"
        >
          <Icon type="info-circle" /> РАЗРЕШЕНИЕ
        </Menu.Item>
        <Menu.Item
          style={{ maxWidth: 200, width: 200, cursor: 'auto' }}
          key="6"
        >
          <Icon type="calendar" /> {humanFileTime}
        </Menu.Item>
      </Menu>

      <ImgModal
        visible={contextModal}
        title={'URL'}
        zIndex={9999999}
        centered
        footer={[
          <Button
            key="submit"
            type="primary"
            onClick={e => {
              e.stopPropagation();
              setContextModal(false);
            }}
          >
            OK
          </Button>,
        ]}
        onCancel={e => {
          setContextModal(false);
        }}
      >
        <Row justify="space-between" type="flex">
          <Col span={17}>
            <p>{fullFileName}</p>
          </Col>
          <Col span={7}>
            <Button
              type="primary"
              ghost
              onClick={() => {
                navigator.clipboard.writeText(fullFileName);
              }}
            >
              <Icon type="copy" />
              Скопировать
            </Button>
          </Col>
        </Row>
      </ImgModal>
    </>
  );
};

export default ContextMenu;
