import React, { useState, useEffect } from 'react';
import { Icon, Input } from 'antd';

import styled from 'styled-components/macro';
import { Files } from 'connectors/query/Files';
import ImgModal from '../ImgModal';

import { FileImg } from '../../styles';

const Wrapper = styled.div`
  display: flex;
  justify-content: ${({ position }) => position};
  width: ${({ wrapperWidth = '100%' }) => wrapperWidth};
`;
const IconWrapper = styled.div`
  &:first-child {
    margin-left: 0px;
  }
  margin-left: 10px;
`;

const titleModal = {
  folder: 'Вы уверены, что хотите удалить эту папку и все файлы в ней?',
  file: 'Вы уверены, что хотите удалить этот файл?',
};

const IconGroup = ({
  type,
  fullFileName,
  fileName,
  position = 'center',
  wrapperWidth,
  path,
  setData,
  preparedFileName,
  extension,
  setIsAnyModals,
}) => {
  const [modalVisible, setModalVisible] = useState({
    edit: false,
    del: false,
    watch: false,
  });
  const [currentName, setCurrentName] = useState();

  useEffect(() => {
    setCurrentName(preparedFileName);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [fileName]);

  useEffect(() => {
    const match = Object.keys(modalVisible).find(
      modalName => modalVisible[modalName],
    );
    if (match) setIsAnyModals(true);
    if (!match) setIsAnyModals(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalVisible]);

  return (
    <Wrapper position={position} wrapperWidth={wrapperWidth}>
      {type === 'file' && (
        <IconWrapper
          onClick={e => {
            e.stopPropagation();
          }}
        >
          <a
            href={fullFileName}
            download={fileName}
            target="_blank"
            style={{ color: '#000' }}
            rel="noopener noreferrer"
          >
            <Icon type="download" title="Загрузить" />
          </a>
        </IconWrapper>
      )}
      {type === 'file' && (
        <IconWrapper
          onClick={e => {
            e.stopPropagation();
            setModalVisible({ ...modalVisible, watch: true });
          }}
        >
          <Icon type="eye" theme="filled" title="Просмотр" />
        </IconWrapper>
      )}
      <IconWrapper
        onClick={e => {
          e.stopPropagation();
          setModalVisible({ ...modalVisible, edit: true });
        }}
      >
        {<Icon type="edit" title="Переименовать" theme="filled" />}
      </IconWrapper>
      <IconWrapper
        onClick={e => {
          e.stopPropagation();
          setModalVisible({ ...modalVisible, del: true });
        }}
      >
        {
          <Icon
            type="delete"
            theme="filled"
            title="Удалить"
            onClick={e => {
              e.stopPropagation();
              setModalVisible({ ...modalVisible, del: true });
            }}
          />
        }
      </IconWrapper>
      {/* Модалки для иконок под фото */}
      <ImgModal
        visible={modalVisible.watch}
        onCancel={e => {
          setModalVisible({ ...modalVisible, watch: false });
        }}
        footer={null}
      >
        <FileImg src={fullFileName} />
      </ImgModal>
      <ImgModal
        visible={modalVisible.edit}
        onCancel={e => {
          setModalVisible({ ...modalVisible, edit: false });
          setCurrentName(preparedFileName);
        }}
        onOk={() => {
          setModalVisible({ ...modalVisible, edit: false });
          Files.getFolder({
            command: 'rename',
            new_name: `${currentName}.${extension}`,
            old_name: fileName,
            parent_folder: `/${path.join('/')}`,
          })
            .then(result => {
              setData(result);
            })
            .catch(console.error);
          setCurrentName(preparedFileName);
        }}
      >
        <Input
          placeholder="Введите имя файла"
          onClick={e => {
            e.stopPropagation();
          }}
          value={currentName}
          onChange={e => {
            setCurrentName(e.target.value);
          }}
        />
      </ImgModal>
      <ImgModal
        visible={modalVisible.del}
        title={type === 'folder' ? titleModal.folder : titleModal.file}
        onCancel={() => {
          setModalVisible({ ...modalVisible, del: false });
        }}
        onOk={() => {
          Files.getFolder({
            command: 'delete',
            name: fileName,
            parent_folder: `/${path.join('/')}`,
            type,
          })
            .then(result => {
              setData(result);
            })
            .catch(console.error);
          setModalVisible({ ...modalVisible, del: false });
        }}
      ></ImgModal>
    </Wrapper>
  );
};

export default IconGroup;
