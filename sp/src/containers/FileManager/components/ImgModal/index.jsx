import React from 'react';
import { Modal } from 'antd';

const ImgModal = ({
  visible = false,
  title = '',
  children,
  footer,
  zIndex = 1100,
  closable = false,
  onCancel = _ => _,
  onOk = _ => _,
  ...props
}) => {
  return (
    <Modal
      visible={visible}
      title={title}
      zIndex={zIndex}
      onClick={e => {
        e.stopPropagation();
      }}
      onCancel={e => {
        e.stopPropagation();
        onCancel();
      }}
      onOk={e => {
        e.stopPropagation();
        onOk();
      }}
      footer={footer}
      closable={closable}
      centered
      {...props}
    >
      {children}
    </Modal>
  );
};

export default ImgModal;
