import React from 'react';
import { Icon } from 'antd';

export const getExt = fileName => {
  const fileSplitted = fileName.split('.');
  return fileSplitted[fileSplitted.length - 1];
};

export const defaultSort = (a, b, direction) => {
  if (direction) {
    return a < b ? -1 : 1;
  } else {
    return a > b ? -1 : 1;
  }
};

export const nameSort = (a = '', b = '', direction) => {
  if (direction) {
    return a.toLowerCase() < b.toLowerCase() ? -1 : 1;
  } else {
    return a.toLowerCase() > b.toLowerCase() ? -1 : 1;
  }
};

export const typeSort = (a, b, direction) => {
  if (direction) {
    return getExt(a) < getExt(b) ? -1 : 1;
  } else {
    return getExt(a) > getExt(b) ? -1 : 1;
  }
};

export const menuRows = {
  fileName: 'Имя файла',
  fileTime: 'Дата',
  fileSize: 'Размер',
  extension: 'Тип',
};

export const showMods = {
  tile: <Icon type="appstore" theme="filled" />,
  list: <Icon type="menu" />,
  table: <Icon type="table" />,
};

export const imgFormats = ['png', 'jpeg', 'jpg', 'webp', 'svg', 'gif'];
