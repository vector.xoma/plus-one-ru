import React, { useState, useEffect } from 'react';
import { Modal, Input } from 'antd';
import { Files } from 'connectors/query/Files';
import Element from './Element';
import FileManagerHeader from './FileManagerHeader';

import { defaultSort, nameSort } from './constants';
import {
  ListElementWrap,
  FileManagerWrap,
  FileManagerListWrap,
  ListElementData,
} from './styles';

const FileManager = ({ setPicUrl, hideModal, visible }) => {
  const [position, setPosition] = useState([]);
  const [currentFiles, setCurrentFiles] = useState([]);
  const [filteredFiles, setFilteredFiles] = useState([]);
  const [visibleFolderModal, setVisibleFolderModal] = useState(false);
  const [folderName, setFolderName] = useState('');
  const [sort, setSort] = useState({ direction: false, type: '' });
  const [showMode, setShowMode] = useState('tile');
  const [isAnyModals, setIsAnyModals] = useState(false);

  useEffect(() => {
    if (currentFiles && sort.type) {
      const sortByfolder = (a, b) => (a.type > b.type ? -1 : 1);
      const sorts = {
        fileName: (a, b) =>
          nameSort(a[sort.type], b[sort.type], sort.direction),
        fileType: (a, b) => defaultSort(a.type, b.type, sort.direction),
        extension: (a, b) =>
          nameSort(a.extension || '', b.extension || '', sort.direction),
      };

      const sortFunc =
        sorts[sort.type] ||
        ((a, b) => defaultSort(a[sort.type], b[sort.type], sort.direction));

      const sortedFiles = [...currentFiles].sort(sortFunc).sort(sortByfolder);
      setFilteredFiles(sortedFiles);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sort, currentFiles]);

  useEffect(() => {
    async function getGalleries() {
      const result = await Files.getFolder({ dir: position.join('/') });
      setCurrentFiles(result);
      setSort({ direction: false, type: 'fileTime' });
    }
    getGalleries();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [position]);

  const backDirectory = () => {
    const newPos = [...position];
    newPos.pop();
    setPosition(newPos);
  };

  const clickEvent = element => {
    const events = {
      folder: () => setPosition([...position, element.fileName]),
      file: () => {
        if (!isAnyModals) {
          setPicUrl(element.fullFileName);
          hideModal();
        }
      },
    };
    return events[element.type]();
  };

  return (
    <Modal
      visible={visible}
      onCancel={hideModal}
      footer={null}
      zIndex={1100}
      style={{ top: 20 }}
      bodyStyle={{
        background: '#ececec',
        padding: '20px 30px',
      }}
      width={'80%'}
    >
      <FileManagerHeader
        position={position}
        currentFiles={filteredFiles}
        sort={sort}
        showMode={showMode}
        setShowMode={setShowMode}
        setData={setCurrentFiles}
        setSort={setSort}
        setPosition={setPosition}
        setFilteredFiles={setFilteredFiles}
        setVisibleFolderModal={setVisibleFolderModal}
      />
      {(showMode === 'tile' || showMode === 'table') && (
        <FileManagerWrap>
          {position.length > 0 && (
            <Element onClick={backDirectory} type="arrow" mode={showMode} />
          )}
          {filteredFiles.map((f, i) => (
            <Element
              key={i}
              onClick={() => {
                if (!isAnyModals) {
                  clickEvent(f);
                }
              }}
              mode={showMode}
              position={position}
              setData={setCurrentFiles}
              setIsAnyModals={setIsAnyModals}
              {...f}
            />
          ))}
        </FileManagerWrap>
      )}

      {showMode === 'list' && (
        <FileManagerListWrap>
          <ListElementWrap>
            <span style={{ marginLeft: 15 }}>Имя</span>
            <ListElementData style={{ width: 380 }}>
              <div className="ext">Тип</div>
              <div className="date">Дата</div>
              <div className="size">Размер</div>
              <div className="act">Действие</div>
            </ListElementData>
          </ListElementWrap>
          {position.length > 0 && (
            <Element onClick={backDirectory} type="arrow" mode={showMode} />
          )}
          {filteredFiles.map((f, i) => (
            <Element
              key={i}
              onClick={() => clickEvent(f)}
              mode={showMode}
              position={position}
              setData={setCurrentFiles}
              setIsAnyModals={setIsAnyModals}
              {...f}
            />
          ))}
        </FileManagerListWrap>
      )}
      <Modal
        visible={visibleFolderModal}
        title="Введите имя папки:"
        zIndex={1100}
        bodyStyle={{
          background: '#ececec',
          padding: '20px 30px',
        }}
        centered
        width={'70%'}
        onOk={() => {
          Files.getFolder({
            command: 'create_folder',
            folder_name: folderName,
            parent_folder: position.join('/'),
          })
            .then(result => {
              setCurrentFiles(result);
              setVisibleFolderModal(false);
            })
            .catch(console.error);
        }}
        onCancel={() => setVisibleFolderModal(false)}
      >
        <Input
          placeholder="Имя папки"
          value={folderName}
          onChange={e => setFolderName(e.target.value)}
        ></Input>
      </Modal>
    </Modal>
  );
};

export default FileManager;
