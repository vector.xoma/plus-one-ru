import styled, { css } from 'styled-components/macro';
import { Icon, Breadcrumb } from 'antd';

export const TileImgCSS = css`
  width: 100%;
  height: 100%;
  object-fit: cover;
  display: block;
  min-height: 94px;
`;

export const FolderImg = styled.img.attrs({
  src: require('img/folder.png'),
  alt: 'FolderImg',
})`
  ${TileImgCSS}
`;
export const BlockImg = styled.img`
  max-width: 122px;
  max-height: 91px;
  ${TileImgCSS}
`;

export const FileImg = styled.img`
  ${TileImgCSS}
`;
export const ElementWrap = styled.div`
  margin: 5px;
  background-color: #dddddd;
  width: 125px;
  min-height: 100px;
  height: 100%;
  cursor: pointer;
  transition: all 0.3s;

  & :hover {
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
    .hover_animation {
      transform: translateY(-25px);
      transition: transform 0.4s;
    }
  }
  .hover_animation {
    transition: transform 0.4s;
  }
`;
export const ElementName = styled.div`
  height: 25px;
  font-size: 12px;
  background-color: #fff;
  color: #000;
  text-align: center;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  padding: 3px;
`;
export const ArrowContainer = styled.div`
  font-size: 42px;
  text-align: center;
  margin-top: 15px;
`;

export const ListFileImg = styled.img`
  width: 45px;
  height: 38px;
`;
export const ListFolderImg = styled.img.attrs({
  src: require('img/folder.png'),
  alt: 'FolderImg',
})`
  width: 50px;
  height: 50px;
`;
export const ListElementWrap = styled.div`
  display: flex;
  justify-content: space-between;
  cursor: pointer;
  align-items: center;
  width: 100%;
  height: 50px;
  border-bottom: 1px solid gray;
  transition: all 0.3s;

  & :hover {
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
  }
`;
export const ListArrowContainer = styled.div`
  font-size: 20px;
  text-align: center;
  margin-left: 20px;
`;
export const ListElementData = styled.div`
  display: flex;
  justify-content: flex-end;
  min-width: ${({ width = '195px' }) => width};

  .ext {
    min-width: 50px;
  }
  .date {
    min-width: 85px;
  }
  .size {
    min-width: 80px;
  }
  .act {
    min-width: 90px;
  }
`;

export const FileManagerWrap = styled.div`
  display: flex;
  flex-wrap: wrap;
`;
export const FileManagerListWrap = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
`;

export const StyledBreadcrumb = styled(Breadcrumb.Item)`
  cursor: pointer;
`;
export const BreadcrumbWrupper = styled.div`
  display: flex;
  margin-left: 5px;
  margin-bottom: 5px;
  justify-content: space-between;
`;
export const AddFileIcon = styled(Icon)`
  font-size: 30px;
  cursor: pointer;
  margin-right: 20px;
  user-select: none;
  margin-left: 5;
`;
export const AddFolderIcon = styled(Icon)`
  font-size: 30px;
  cursor: pointer;
  margin-right: 20px;
  user-select: none;
`;
export const ElementsCounter = styled.span`
  margin-left: 20px;
  margin-top: 3px;
`;
export const CarretIcon = styled(Icon)`
  margin-right: 6px;
`;

export const TableElementWrap = styled.div`
  display: flex;
  align-items: center;
  width: 360px;
  height: 40px;
  border-bottom: 1px solid gray;
  transition: all 0.3s;
  cursor: pointer;
  justify-content: space-between;
  margin: 0 2px;
  padding-right: 10px;

  & :hover {
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
  }
`;
export const TableElementData = styled.div`
  display: flex;
  align-items: center;

  .name {
    white-space: nowrap;
    width: 255px;
    overflow: hidden;
    text-overflow: ellipsis;
  }
`;
