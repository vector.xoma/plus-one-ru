import React, { useState, useRef } from 'react';
import { Button, Modal } from 'antd';

import Text from 'components/Text';
import Show from 'pages/Show';
import styled from 'styled-components/macro';
import useScroll from 'hooks/useScroll';

const InputDataWrap = styled.div`
  padding: 5px;
  /* width: 50%; */
  background: cornsilk;
  /* min-height: 650px; */
  position: relative;
  order: 2;
`;

const InputDataContent = styled.div`
  display: flex;
  flex-direction: column;

  padding: 10px;
`;

const InputData = ({ data, updateField, areaName, editorWrapper }) => {
  const update = fieldData => updateField(fieldData);
  const [visible, setVisible] = useState(false);
  const [fixedContainer, setFixedContainer] = useState(false);
  const editorContainer = useRef(null);
  const scrollAction = () => {
    if (editorContainer.current) {
      setFixedContainer(
        parseInt(editorContainer.current.getBoundingClientRect().top, 10) < 30,
      );
    }
  };
  useScroll(scrollAction);

  const fixEditorStyle = {
    position: 'fixed',
    width: editorWrapper ? (editorWrapper.offsetWidth - 10) / 2 : 'inherit',
    top: '20px',
  };

  return (
    <InputDataWrap ref={editorContainer}>
      <div
      // style={{
      //   position: 'absolute',
      //   ...(fixedContainer && fixEditorStyle),
      // }}
      >
        Редактор:
        <Button onClick={() => setVisible(true)}>Полный экран</Button>
        <Modal
          visible={visible}
          onCancel={() => setVisible(false)}
          footer={null}
          width={'100%'}
          bodyStyle={{ padding: 0 }}
        >
          <Show data={data} />
        </Modal>
        <InputDataContent>
          {data && (
            <Text.Editor data={data} updateField={update} areaName={areaName} />
          )}
        </InputDataContent>
      </div>
    </InputDataWrap>
  );
};

export default InputData;
