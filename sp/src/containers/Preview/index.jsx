import React from 'react';
import ReactDOMServer from 'react-dom/server';
import styled, { ThemeProvider } from 'styled-components/macro';
import { BrowserRouter } from 'react-router-dom';

import Text from 'components/Text';

const PreviewContainer = styled.div`
  background-color: ${({ isAboutPage, platformPost, theme }) => {
    if (theme.body_color !== '') {
        return theme.body_color;
    }
    if (platformPost === '1') {
        return '#dbdbdb';
    }
    return isAboutPage ? '#212121' : '#eaeaea';
}};
  height: 100%;
`;

const darkTheme = { backgroundColor: '#000', color: '#fff' };
const lightTheme = { backgroundColor: '#fff', color: '#000' };

const Preview = ({
                     data,
                     showTitle,
                     isAboutPage,
                     isarticlepage,
                     zoom,
                     isAdminPage,
                     platformPost,
                     lead,
                     areaName, // нужно редактору админки. для указания имени скрытого textarea для получения бэков фидов
                     donateProps,
                 }) => {
    const theme = isAboutPage ? darkTheme : lightTheme;

    return (
        <PreviewContainer
            isAboutPage={isAboutPage}
            isarticlepage={isarticlepage}
            platformPost={platformPost}
        >
            {showTitle && (
                <div
                    style={{
                        marginBottom: '10px',
                    }}
                >
                    Предпросмотр:
                    <textarea
                        name={`${areaName}_html_for_feed`}
                        style={{ display: 'none' }}
                        value={ReactDOMServer.renderToStaticMarkup(
                            <BrowserRouter>
                                <ThemeProvider theme={theme}>
                                    <Text
                                        data={data}
                                        zoom={zoom}
                                        isarticlepage={isarticlepage}
                                        isAdminPage={isAdminPage}
                                        lead={lead}
                                        donateProps={donateProps}
                                    />
                                </ThemeProvider>
                            </BrowserRouter>,
                        )}
                    />
                </div>
            )}

            <div>
                <ThemeProvider theme={theme}>
                    <Text
                        data={data}
                        zoom={zoom}
                        isarticlepage={isarticlepage}
                        isAdminPage={isAdminPage}
                        lead={lead}
                        donateProps={donateProps}
                    />
                </ThemeProvider>
            </div>
        </PreviewContainer>
    );
};

export default React.memo(Preview);
