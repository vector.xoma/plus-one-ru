import React, { useContext, useState, createContext, useEffect } from 'react';
import { Components } from 'connectors/query/Components';

const ComponentsContetx = createContext({
  components: [],
  updateComponent: _ => _,
});

function ComponentsProvider({ children }) {
  const [components, setComponents] = useState([]);

  useEffect(() => {
    async function getComponents() {
      const result = await Components.getList();
      setComponents(result);
    }
    getComponents();
  }, []);

  useEffect(() => {}, []);

  return (
    <ComponentsContetx.Provider value={{ components, setComponents }}>
      {children}
    </ComponentsContetx.Provider>
  );
}

const useComponents = () => {
  const context = useContext(ComponentsContetx);
  return context;
};

export { ComponentsProvider, useComponents };
