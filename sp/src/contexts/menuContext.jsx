import React, { useContext, useState, createContext, useEffect } from 'react';
import { Menu } from 'connectors/query/Menu';

const MenuContext = createContext({ data: [] });

function MenuProvider({ children }) {
  const [data, setData] = useState([]);

  useEffect(() => {
    async function getMenuData() {
      const result = await Menu.getList();
      setData(result);
    }
    getMenuData();
  }, []);

  return (
    <MenuContext.Provider value={{ data, setData }}>
      {children}
    </MenuContext.Provider>
  );
}

const useMenuData = () => {
  const context = useContext(MenuContext);
  return context;
};

export { MenuProvider, useMenuData };
