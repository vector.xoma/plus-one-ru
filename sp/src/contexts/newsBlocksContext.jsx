import React, { useContext, useState, createContext, useEffect } from 'react';
import { Blocks } from 'connectors/query/Blocks';
import _get from 'lodash/get';

const NewsBlocksContext = createContext({ News: [] });

async function getNews({ setNews, page, news }) {
  const result = await Blocks.getNews(page);
  const prepared = _get(result, 'items', []);
  const meta = _get(result, 'meta', {});
  if (!prepared) return;
  if (page === 1) {
    setNews({ data: prepared, meta });
    return;
  }
  news && news.data && setNews({ data: [...news.data, ...prepared], meta });
  return;
}

function NewsBlocksProvider({ children }) {
  const [news, setNews] = useState({});
  const [page, setPage] = useState(1);

  useEffect(() => {
    getNews({ setNews, page, news });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  return (
    <NewsBlocksContext.Provider
      value={{ news, setPage, page, setNews, getNews }}
    >
      {children}
    </NewsBlocksContext.Provider>
  );
}

const useNewsBlocks = () => {
  const context = useContext(NewsBlocksContext);
  return context;
};

export { NewsBlocksProvider, useNewsBlocks };
