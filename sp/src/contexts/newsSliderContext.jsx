import React, { useContext, useState, createContext, useEffect } from 'react';
import { News } from 'connectors/query/newsSlider';

const NewsSliderContext = createContext({ data: [] });

async function getSliderData(setData) {
  const result = await News.getList();
  setData(result);
}

function NewsSliderProvider({ children }) {
  const [data, setData] = useState([]);
  useEffect(() => {
    if (!data || !data.length) getSliderData(setData);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <NewsSliderContext.Provider value={{ data, setData, getSliderData }}>
      {children}
    </NewsSliderContext.Provider>
  );
}

const useNewsSliderData = () => {
  const context = useContext(NewsSliderContext);
  return context;
};

export { NewsSliderProvider, useNewsSliderData };
