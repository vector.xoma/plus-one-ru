import React, { useContext, useState, createContext, useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { Blocks } from 'connectors/query/Blocks';

const RowsBlocksContext = createContext({ rows: [] });

async function getRows(setRows = () => {}) {
  const result = await Blocks.getRows();
  setRows(result);
}

function RowsBlocksProvider({ children }) {
  const { pathname } = useLocation();
  const [rows, setRows] = useState([]);

  useEffect(() => {
    (pathname === '/' || pathname.includes('confirm-subscribe')) &&
      !rows.length &&
      getRows(setRows);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pathname]);

  return (
    <RowsBlocksContext.Provider value={{ rows, setRows, getRows }}>
      {children}
    </RowsBlocksContext.Provider>
  );
}

const useRowsBlocks = () => {
  const context = useContext(RowsBlocksContext);
  return context;
};

export { RowsBlocksProvider, useRowsBlocks };
