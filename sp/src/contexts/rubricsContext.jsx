import React, { useContext, useState, createContext, useEffect } from 'react';
import _get from 'lodash/get';
import { useLocation } from 'react-router-dom';
import { Blocks } from 'connectors/query/Blocks';

const RubricContext = createContext({ rows: [] });

const rubrics = ['economy', 'ecology', 'society', 'platform'];

async function getRows({
  activeRubricSetter,
  nextStep = false,
  currentRubric,
  rubricsMatch,
  rubricsData,
  page,
}) {
  const { items, meta, ...values } = await Blocks.getList(currentRubric, page);
  if (rubricsMatch) {
    if (nextStep) {
      if (!items.length) return;
      activeRubricSetter[currentRubric]({
        data: [...rubricsData[currentRubric].data, ...items],
        meta,
        rubricValues: { ...values },
      });
      return;
    }
    activeRubricSetter[currentRubric]({
      data: [...items],
      meta,
      rubricValues: { ...values },
    });
  }
}

function RubricProvider({ children }) {
  const { pathname } = useLocation();
  const [economy, setEconomy] = useState({ data: [] });
  const [ecology, setEcology] = useState({ data: [] });
  const [society, setSociety] = useState({ data: [] });
  const [platform, setPlatform] = useState({ data: [] });
  const [page, setPage] = useState(1);

  const rubricsData = {
    economy,
    ecology,
    society,
    platform,
  };

  const activeRubricSetter = {
    economy: setEconomy,
    ecology: setEcology,
    society: setSociety,
    platform: setPlatform,
  };

  const currentRubric = pathname.slice(1).replace('/', '');
  // TODO: отстреливается один лишний запрос
  // Кейс: зашли в рубрику, пролистали пару страниц
  //  перешли в др рубрику
  useEffect(() => setPage(1), [currentRubric]);

  useEffect(() => {
    const rubricsMatch = rubrics.find(r => r === currentRubric);

    if (rubricsMatch && !rubricsData[currentRubric].data.length && page === 1) {
      getRows({
        activeRubricSetter,
        currentRubric,
        rubricsMatch,
        rubricsData,
        page,
      });
    }
    if (rubricsMatch && page !== 1) {
      getRows({
        activeRubricSetter,
        nextStep: true,
        currentRubric,
        rubricsMatch,
        rubricsData,
        page,
      });
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentRubric, page]);

  return (
    <RubricContext.Provider
      value={{
        economy,
        ecology,
        society,
        platform,
        setPage,
        page,
        getRows,
        activeRubricSetter,
        currentRubric,
        rubrics,
        rubricsData,
      }}
    >
      {children}
    </RubricContext.Provider>
  );
}

const useRubric = () => {
  const context = useContext(RubricContext);
  return context;
};

export { RubricProvider, useRubric };
