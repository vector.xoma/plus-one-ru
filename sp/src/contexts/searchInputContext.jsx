import React, { useContext, useState, createContext, useEffect } from 'react';

const SearchInputContext = createContext({
  activeAnimationHeader: false,
});

function SearchInputProvider({ children }) {
  const [activeAnimationHeader, setActiveAnimationHeader] = useState(false);

  return (
    <SearchInputContext.Provider
      value={{ activeAnimationHeader, setActiveAnimationHeader }}
    >
      {children}
    </SearchInputContext.Provider>
  );
}

const useSearchInput = () => {
  const context = useContext(SearchInputContext);
  return context;
};

export { SearchInputProvider, useSearchInput };
