export const metaTags = {
  title: '+1 — Проект об устойчивом развитии',
  description:
    'Все, что вы хотели знать об изменениях в экономике, обществе и экосистеме, но боялись спросить.',
  image: 'https://plus-one.ru/og-plus-one.ru.png',
  url: 'https://plus-one.ru/?v2',
};
