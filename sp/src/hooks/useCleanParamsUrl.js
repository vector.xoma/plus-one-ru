import { useEffect } from 'react';
const useCleanParamsUrl = (arrParams = []) => {
  useEffect(() => {
    for (let i = 0; i < arrParams.length; i++) {
      if (window.location.search.includes(arrParams[i])) {
        window.location = `${window.location.origin}${window.location.pathname}`;
      }
    }
  }, []);
};

export default useCleanParamsUrl;
