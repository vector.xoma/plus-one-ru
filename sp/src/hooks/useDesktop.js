import { useState, useEffect } from 'react';

const useDesktop = () => {
  const [isDesktop, setIsDesktop] = useState(window.outerWidth >= 767);

  useEffect(() => {
    if (window.outerWidth <= 0) {
      setIsDesktop(true);
    }
    const resizeHandler = () => setIsDesktop(window.outerWidth >= 767);
    window.addEventListener('resize', resizeHandler);
    return () => window.removeEventListener('resize', resizeHandler);
  }, []);

  return isDesktop;
};

export default useDesktop;
