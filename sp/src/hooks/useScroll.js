import { useEffect } from 'react';

const useScroll = action => {
  useEffect(() => {
    document.addEventListener('scroll', action);
    return () => document.removeEventListener('scroll', action);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
};

export default useScroll;
