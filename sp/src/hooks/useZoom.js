import { useState, useEffect } from 'react';

const useZoom = ref => {
  const [zoom, setZoom] = useState(`1`);
  const resizeHandler = () => {
    setZoom(`${(ref.current.getBoundingClientRect().width - 16) / 1140}`);
  };

  useEffect(() => {
    window.addEventListener('load', resizeHandler);
    window.addEventListener('resize', resizeHandler);
    return () => {
      window.removeEventListener('resize', resizeHandler);
      window.removeEventListener('load', resizeHandler);
    };
  }, []);

  return zoom;
};

export default useZoom;
