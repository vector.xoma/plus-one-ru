import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import AppEditor from './AppEditor';
import AppUserFront from './AppUserFront';
import * as serviceWorker from './serviceWorker';

const root = document.getElementById('root');
// Админка
// root &&
// ReactDOM.render(<AppEditor globalVar="TestVar" areaName="name" />, root);

// основная страница пользователя
root &&
  ReactDOM.render(<AppUserFront globalVar="TestVar" areaName="name" />, root);

/* ------------------------- Редактор ------------------------------ */
// // О проекте
// const about = document.getElementById('reactEditor_about');
// about &&
//   ReactDOM.render(<AppEditor globalVar="EditorAbout" areaName="body" />, about);

// // Записи в блогах
// const blogpost_header = document.getElementById('reactEditor_blogpost_header');
// blogpost_header &&
//   ReactDOM.render(
//     <AppEditor globalVar="EditorBlogpostHeader" areaName="header" />,
//     blogpost_header,
//   );

// const blogpost_lead = document.getElementById('reactEditor_blogpost_lead');
// blogpost_lead &&
//   ReactDOM.render(
//     <AppEditor globalVar="EditorBlogpostLead" areaName="lead" />,
//     blogpost_lead,
//   );

// const blogpost_body = document.getElementById('reactEditor_blogpost_body');
// blogpost_body &&
//   ReactDOM.render(
//     <AppEditor globalVar="EditorBlogpostBody" areaName="body" />,
//     blogpost_body,
//   );

// // ------------------------------------

// const citatepost_header = document.getElementById(
//   'reactEditor_citatepost_header',
// );
// citatepost_header &&
//   ReactDOM.render(
//     <AppEditor globalVar="EditorCitatepostHeader" areaName="header" />,
//     citatepost_header,
//   );
// const citatepost_lead = document.getElementById('reactEditor_citatepost_lead');
// citatepost_lead &&
//   ReactDOM.render(
//     <AppEditor globalVar="EditorCitatepostLead" areaName="lead" />,
//     citatepost_lead,
//   );
// const citatepost_body = document.getElementById('reactEditor_citatepost_body');
// citatepost_body &&
//   ReactDOM.render(
//     <AppEditor globalVar="EditorCitatepostBody" areaName="body" />,
//     citatepost_body,
//   );

// const factpost_header = document.getElementById('reactEditor_factpost_header');
// factpost_header &&
//   ReactDOM.render(
//     <AppEditor globalVar="EditorFactpostHeader" areaName="header" />,
//     factpost_header,
//   );
// const factpost_lead = document.getElementById('reactEditor_factpost_lead');
// factpost_lead &&
//   ReactDOM.render(
//     <AppEditor globalVar="EditorFactpostLead" areaName="lead" />,
//     factpost_lead,
//   );
// const factpost_body = document.getElementById('reactEditor_factpost_body');
// factpost_body &&
//   ReactDOM.render(
//     <AppEditor globalVar="EditorFactpostBody" areaName="body" />,
//     factpost_body,
//   );

// const diypost_header = document.getElementById('reactEditor_diypost_header');
// diypost_header &&
//   ReactDOM.render(
//     <AppEditor globalVar="EditorDiypostHeader" areaName="header" />,
//     diypost_header,
//   );
// const diypost_lead = document.getElementById('reactEditor_diypost_lead');
// diypost_lead &&
//   ReactDOM.render(
//     <AppEditor globalVar="EditorDiypostLead" areaName="lead" />,
//     diypost_lead,
//   );
// const diypost_body = document.getElementById('reactEditor_diypost_body');
// diypost_body &&
//   ReactDOM.render(
//     <AppEditor globalVar="EditorDiypostBody" areaName="body" />,
//     diypost_body,
//   );

// const videopost_lead = document.getElementById('reactEditor_videoPost_lead');
// videopost_lead &&
//   ReactDOM.render(
//     <AppEditor globalVar="EditorVideoPostLead" areaName="lead" />,
//     videopost_lead,
//   );

// const writerblog_lead = document.getElementById('reactEditor_writerblog_lead');
// writerblog_lead &&
//   ReactDOM.render(
//     <AppEditor globalVar="EditorWriterBlog" areaName="lead" />,
//     writerblog_lead,
//   );

// const headerText = document.getElementById('reactEditor_headerText');
// headerText &&
//   ReactDOM.render(
//     <AppEditor globalVar="EditorHeaderText" areaName="description" />,
//     headerText,
//   );

// const conference_header = document.getElementById(
//   'reactEditor_conference_header',
// );
// conference_header &&
//   ReactDOM.render(
//     <AppEditor globalVar="EditorConferenceHeader" areaName="header" />,
//     conference_header,
//   );

// const event_lead = document.getElementById('reactEditor_event_lead');
// event_lead &&
//   ReactDOM.render(
//     <AppEditor globalVar="EditorEventLead" areaName="lead" />,
//     event_lead,
//   );
// const event_body = document.getElementById('reactEditor_event_body');
// event_body &&
//   ReactDOM.render(
//     <AppEditor globalVar="EditorEventBody" areaName="body" />,
//     event_body,
//   );

// const newspost_header = document.getElementById('reactEditor_newspost_header');
// newspost_header &&
//   ReactDOM.render(
//     <AppEditor globalVar="EditorNewspostHeader" areaName="header" />,
//     newspost_header,
//   );
// const newspost_lead = document.getElementById('reactEditor_newspost_lead');
// newspost_lead &&
//   ReactDOM.render(
//     <AppEditor globalVar="EditorNewspostLead" areaName="lead" />,
//     newspost_lead,
//   );
// const newspost_body = document.getElementById('reactEditor_newspost_body');
// newspost_body &&
//   ReactDOM.render(
//     <AppEditor globalVar="EditorNewspostBody" areaName="body" />,
//     newspost_body,
//   );

// const specProj_header = document.getElementById('reactEditor_specProj_header');
// specProj_header &&
//   ReactDOM.render(
//     <AppEditor globalVar="EditorSpecProjHeader" areaName="header" />,
//     specProj_header,
//   );

// const plot_lead = document.getElementById('reactEditor_plot_lead');
// plot_lead &&
//   ReactDOM.render(
//     <AppEditor globalVar="EditorPlotLead" areaName="lead" />,
//     plot_lead,
//   );

serviceWorker.unregister();
