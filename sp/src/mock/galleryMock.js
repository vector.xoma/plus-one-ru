const galleryMock = [
  {
    id: "445",
    gallery_id: "39",
    filename:
      "https://i.pinimg.com/564x/9d/e0/c2/9de0c2f8649ccc55b7ef71289ac7f9f6.jpg",
    image_name: null,
    image_author: null
  },
  {
    id: "446",
    gallery_id: "39",
    filename:
      "https://kartinki-dlya-srisovki.ru/wp-content/uploads/2018/06/klassnye-kartinki-dlya-srisovki-4.jpg",
    image_name: "name",
    image_author: "BBBBBBBB"
  },
  {
    id: "447",
    gallery_id: "39",
    filename:
      "https://bipbap.ru/wp-content/uploads/2019/10/skvishi-43-800x800-640x640.jpg",
    image_name: "dsadsadsa",
    image_author: "AAAAAAAAAAAAAAAAAAAAAAAA"
  },
  {
    id: "448",
    gallery_id: "39",
    filename:
      "https://kopilpremudrosti.ru/wp-content/uploads/2018/12/1-risunok.jpg",
    image_name: "Фотографии из архивов волонтеров",
    image_author: "author"
  }
];

export default galleryMock;
