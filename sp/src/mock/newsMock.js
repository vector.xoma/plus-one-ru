const newsMock = [
  [
    {
      title: " НОВОСТЬ ",
      url: "https://www.google.com/"
    }
  ],
  [
    {
      title: "   Тестовая новость без иллюстрации для PLUS1-587 ",
      url: "news/2019/11/25/vstavka-embed-c-twitter"
    },
    {
      title: "   Впервые в истории женщины вышли в космос без мужчин  ",
      url: "news/2019/11/25/vstavka-embed-c-twitter"
    },
    {
      title:
        " 500 центров амбулаторной онкологической помощи откроется в России к 2024 году  ",
      url: "news/2019/11/25/vstavka-embed-c-twitter"
    }
  ],
  [
    {
      title:
        "  Ежедневное воздействие синего света от телефонов и компьютеров сокращает жизнь ",
      url: "news/2019/11/25/vstavka-embed-c-twitter"
    },
    {
      title: "   Глобальное потепление обойдется России в $2 млрд ежегодно   ",
      url: "news/2019/11/25/vstavka-embed-c-twitter"
    },
    {
      title:
        "   Только три крупных бренда одежды борются с изменением климата  ",
      url: "news/2019/11/25/vstavka-embed-c-twitter"
    }
  ],
  [
    {
      title: "  В России хотят запретить пластиковые пакеты  ",
      url: "news/2019/11/25/vstavka-embed-c-twitter"
    },
    {
      title: "   Россияне сдали на переработку 35 тонн батареек   ",
      url: "news/2019/11/25/vstavka-embed-c-twitter"
    },
    {
      title: " Вставка embed   asd ",
      url: "news/2019/11/25/vstavka-embed-c-twitter"
    }
  ],
  [
    {
      title:
        " «Кавычки», — тире, «Кавычки „внутри кавычек“ работают»? А неразрывный на пробел?  ",
      url: "news/2019/11/25/vstavka-embed-c-twitter"
    },
    {
      title:
        " Вставка embed    asdsadsaasds adsa  asdsadsaasds adsa  asdsadsaasds adsa ",
      url: "news/2019/11/25/vstavka-embed-c-twitter"
    },
    {
      title: "   Эрмитаж перешел на раздельный сбор мусора  ",
      url: "news/2019/11/25/vstavka-embed-c-twitter"
    }
  ],
  [
    {
      title:
        " «Кавычки», — тире, «Кавычки „внутри кавычек“ работают»? А неразрывный на пробел?  ",
      url: "news/2019/11/25/vstavka-embed-c-twitter"
    },
    {
      title:
        " Вставка embed    asdsadsaasds adsa  asdsadsaasds adsa  asdsadsaasds adsa ",
      url: "news/2019/11/25/vstavka-embed-c-twitter"
    },
    {
      title: "   Эрмитаж перешел на раздельный сбор мусора  ",
      url: "news/2019/11/25/vstavka-embed-c-twitter"
    }
  ],
  [
    {
      title:
        " «Кавычки», — тире, «Кавычки „внутри кавычек“ работают»? А неразрывный на пробел?  ",
      url: "news/2019/11/25/vstavka-embed-c-twitter"
    },
    {
      title:
        " Вставка embed    asdsadsaasds adsa  asdsadsaasds adsa  asdsadsaasds adsa ",
      url: "news/2019/11/25/vstavka-embed-c-twitter"
    },
    {
      title: "   Эрмитаж перешел на раздельный сбор мусора  ",
      url: "news/2019/11/25/vstavka-embed-c-twitter"
    }
  ],
  [
    {
      title:
        " «Кавычки», — тире, «Кавычки „внутри кавычек“ работают»? А неразрывный на пробел?  ",
      url: "news/2019/11/25/vstavka-embed-c-twitter"
    },
    {
      title:
        " Вставка embed    asdsadsaasds adsa  asdsadsaasds adsa  asdsadsaasds adsa ",
      url: "news/2019/11/25/vstavka-embed-c-twitter"
    },
    {
      title: "   Эрмитаж перешел на раздельный сбор мусора  ",
      url: "news/2019/11/25/vstavka-embed-c-twitter"
    }
  ],
  [
    {
      title:
        " «Кавычки», — тире, «Кавычки „внутри кавычек“ работают»? А неразрывный на пробел?  ",
      url: "news/2019/11/25/vstavka-embed-c-twitter"
    },
    {
      title:
        " Вставка embed    asdsadsaasds adsa  asdsadsaasds adsa  asdsadsaasds adsa ",
      url: "news/2019/11/25/vstavka-embed-c-twitter"
    },
    {
      title: "   Эрмитаж перешел на раздельный сбор мусора  ",
      url: "news/2019/11/25/vstavka-embed-c-twitter"
    }
  ]
];
export default newsMock;
