import React, { useEffect, useState } from 'react';
import useMetaTags from 'utils/hooks/useMetaTags';

import _get from 'lodash/get';
import logo from 'img/plusOneLogo_white.svg';
import { About } from 'connectors/query/About';
import Preview from 'containers/Preview';
import BlackPagesOuter from 'containers/BlackPagesOuter';
import useDesktop from 'hooks/useDesktop';
import useCleanParamsUrl from 'hooks/useCleanParamsUrl';
import { TitleLogoWrap, TitleLogoImg, MainContainer } from './style';

const AboutPage = () => {
  const [pageData, setPageData] = useState({});
  const { meta } = pageData;
  const isDesktop = useDesktop();
  useMetaTags({ meta });
  //  useCleanParamsUrl(['?fbclid=', '?utm_']);
  useEffect(() => {
    window.scrollTo(0, 0);
    async function getNews() {
      const result = await About.getPage();
      setPageData(result);
    }
    getNews();
  }, []);

  return (
    <BlackPagesOuter isAboutPage>
      {isDesktop && (
        <TitleLogoWrap>
          <TitleLogoImg src={logo} />
        </TitleLogoWrap>
      )}
      <MainContainer>
        <Preview
          isAboutPage
          data={{ text: `${_get(pageData, 'content.body', '')}` }}
        />
      </MainContainer>
    </BlackPagesOuter>
  );
};

export default AboutPage;
