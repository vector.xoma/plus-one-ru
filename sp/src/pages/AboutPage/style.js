import styled from 'styled-components/macro';

const TitleLogoWrap = styled.div`
  max-width: 890px;
  min-height: 490px;
  margin: 0 auto 100px;
`;

const TitleLogoImg = styled.img`
  width: 100%;
  height: 100%;
  display: block;
  object-fit: cover;
`;
const MainContainer = styled.div`
  width: 100%;
  @media (max-width: 767px) {
    padding: 0 10px;
  }
`;

export { TitleLogoWrap, TitleLogoImg, MainContainer };
