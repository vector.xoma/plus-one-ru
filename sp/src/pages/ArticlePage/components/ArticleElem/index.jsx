import React, { useState, useEffect, useContext } from 'react';
import _get from 'lodash/get';
import { useLocation } from 'react-router-dom';
import { ThemeContext } from 'styled-components/macro';

import Preview from 'containers/Preview';

import Banner from 'components/Banner';
import TagsArticlePage from '../TagsArticlePage';
import ShareBlock from '../ShareBlock';
import RealtedPublish from '../RealtedPublish';
import WidgetSlider from '../WidgetSlider';
import DonateForm from 'components/DonateForm/';
import SubscribeForm from 'components/Block/components/SubscribeForm';

import {
  ArticleName,
  MainContainer,
  ContentWrap,
  PlatformHeader,
  Devider,
} from './style';

const ArticleElem = ({
  pageData = {},
  widgetData,
  blockAnnouncements = [],
  countBlockAnnounce,
}) => {
  const themeContext = useContext(ThemeContext);
  const {
    fund_resource_id: resourceId,
    header_donate_form: donateHeader,
    show_fund,
    shop_article_id: shopId,
    fullUrl,
  } = pageData;
  const location = useLocation();
  const [isNews, setIsNews] = useState(false);
  const [leadText, setLeadText] = useState({ text: '' });
  const [bodyText, setBodyText] = useState({ text: '' });
  const [newsImage, setNewsImage] = useState({ text: '' });
  const [newsAuthor, setNewsAuthor] = useState({ text: '' });
  const [relatedPosts, setRelatedPosts] = useState([]);
  useEffect(() => {
    setIsNews(location.pathname.includes('/news/'));
  }, [location]);

  useEffect(() => {
    setLeadText({ text: `${_get(pageData, 'lead', '')}` });
    setBodyText({ text: `${_get(pageData, 'body', '')}` });
    setNewsImage({ text: `${_get(pageData, 'mainImage', '')}` });
    setNewsAuthor({ text: `${_get(pageData, 'authorSign', '')}` });
    setRelatedPosts(_get(pageData, 'relatedPosts', []));
  }, [pageData]);

  return (
    <div className="article-js">
      <MainContainer>
        {!pageData.firstArticle && (
          <Banner
            elemId={pageData.id}
            bannerStyle={{
              backgroundColor: 'transparent',
              marginBottom: '54px',
            }}
            p2Desctop="ghgl"
            p2Mobile="ghgm"
          />
        )}
        {pageData.platformPost === '1' && (
          <PlatformHeader>МАТЕРИАЛ УЧАСТНИКА ПЛАТФОРМЫ +1</PlatformHeader>
        )}
        <ContentWrap platformPost={pageData.platformPost}>
          <TagsArticlePage data={pageData} />
          <ArticleName
            themeContext={themeContext}
            dangerouslySetInnerHTML={{
              __html: _get(pageData, 'name', ''),
            }}
          />
          <Preview
            isarticlepage={1}
            data={leadText}
            platformPost={pageData.platformPost}
            lead
            donateProps={{
              donateHeader,
              show_fund,
              resourceId,
              shopId,
            }}
          />
          {isNews && <Preview isarticlepage={1} data={newsImage} />}
          <Preview
            isarticlepage={1}
            data={bodyText}
            platformPost={pageData.platformPost}
            donateProps={{
              donateHeader,
              show_fund,
              resourceId,
              shopId,
            }}
          />
          {isNews && <Preview isarticlepage={1} data={newsAuthor} />}
          <ShareBlock shareUrl={fullUrl} />
        </ContentWrap>
        {relatedPosts && !!relatedPosts.length ? (
          <RealtedPublish posts={relatedPosts} />
        ) : (
          <RealtedPublish
            posts={
              blockAnnouncements[countBlockAnnounce] &&
              blockAnnouncements[countBlockAnnounce].items
            }
          />
        )}
        {/* TODO: недоделан */}
        {show_fund === '1' ? (
          <DonateForm data={{ header: donateHeader, resourceId, shopId }} />
        ) : (
          <SubscribeForm />
        )}
        <WidgetSlider widgetData={widgetData} />
      </MainContainer>
      <Devider />
    </div>
  );
};

export default React.memo(ArticleElem);
