import styled from 'styled-components/macro';

const ArticleName = styled.div`
  max-width: 755px;
  margin: 0 auto;
  font-size: 54px;
  font-family: 'MullerBold';
  margin-bottom: 35px;
  line-height: 1.09;
  color: ${({ theme }) => {
    return theme.font_color && theme.font_color !== ''
      ? theme.font_color
      : '#000';
  }};
  sub,
  sup {
    font-size: 75%;
  }

  @media (max-width: 767px) {
    font-size: 24px;
  }
`;

const MainContainer = styled.div`
  width: 100%;
  margin: 0 auto;
  background-color: ${({ theme }) => theme.body_color};
  /* max-width: 1140px; */
  @media (max-width: 767px) {
    padding: 0 10px;
  }
`;
const ContentWrap = styled.div`
  ${({ platformPost }) =>
    platformPost === '1' && {
      background: '#dbdbdb',
      maxWidth: '825px',
      margin: '0 auto',
      padding: '31px 60px 85px 51px',
      borderRadius: '5px',
      marginBottom: '56px',
    }}
`;

const PlatformHeader = styled.div`
  text-transform: uppercase;
  color: #000;
  font-size: 14px;
  line-height: 17px;
  margin-bottom: 16px;
  margin-top: 38px;
  text-align: center;
  font-family: 'MullerMedium';
`;

const Devider = styled.div`
  max-width: 755px;
  height: 2px;
  margin: 40px auto;
  background-color: #d6d6d6;
  @media (max-width: 767px) {
    max-width: 300px;
  }
`;

export { ArticleName, MainContainer, ContentWrap, PlatformHeader, Devider };
