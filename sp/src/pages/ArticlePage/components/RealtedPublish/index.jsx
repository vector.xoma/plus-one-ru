import React from 'react';
import RowBlock from 'components/Block/components/RowBlock';

function RealtedPublish({ posts }) {
  return (
    <RowBlock
      content={posts}
      rowType={'1_3'}
      isMainPage
      typePost={'news'}
      articlePageRow
    />
  );
}

export default RealtedPublish;
