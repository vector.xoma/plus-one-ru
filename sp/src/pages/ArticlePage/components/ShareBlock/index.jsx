import React, { useEffect } from 'react';
import styled from 'styled-components/macro';

const ShareBlockWrap = styled.div`
  max-width: 755px;
  margin: 0 auto;
  padding-bottom: 84px;
`;

const ShareBlock = ({ shareUrl, shareTitle }) => {
  useEffect(() => {
    const script = document.createElement('script');
    const script2 = document.createElement('script');

    script.src = 'https://yastatic.net/es5-shims/0.0.2/es5-shims.min.js';
    script2.src = 'https://yastatic.net/share2/share.js';

    if (document.getElementsByClassName('ya-share2')[0]) {
      document.getElementsByClassName('ya-share2')[0].appendChild(script);
      document.getElementsByClassName('ya-share2')[0].appendChild(script2);
    }
  }, []);

  return (
    <ShareBlockWrap>
      <div
        className="ya-share2"
        data-services="vkontakte,facebook,odnoklassniki,twitter,evernote,viber,whatsapp,skype,telegram"
        data-url={`${window.location.origin}/${shareUrl}`}
        data-title={shareTitle}
      ></div>
    </ShareBlockWrap>
  );
};

export default ShareBlock;
