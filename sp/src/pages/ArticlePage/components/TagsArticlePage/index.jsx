import React from 'react';
import _get from 'lodash/get';
import {
  TagWrap,
  TagHeaderTopic,
  TagHeaderDate,
  TagNameWrap,
  TagName,
  TagHeader,
  LinkTo,
  TagPartner,
} from './style';

const TagsArticlePage = ({ data = {} }) => {
  const { specProjectUrl, specProjectName, postDateStr } = data;
  const specUrl = specProjectUrl && new URL(specProjectUrl);
  //TODO: нужно что бы бэк отдавал относительные и абслоютные ссылки
  const RouteLink = ({ children }) => {
    return specUrl.href.includes('https://plus-one') ? (
      <LinkTo to={`${specUrl.pathname}` || ''}>{children}</LinkTo>
    ) : (
      <a target="_blank" rel="nofollow noopener" href={specUrl.href || ''}>
        {children}
      </a>
    );
  };
  return (
    <TagWrap>
      {data && (
        <TagHeader>
          {data && (
            <>
              {specProjectUrl && (
                <RouteLink to={`${specUrl.pathname}` || ''}>
                  <TagHeaderTopic margin={specProjectName ? '22px' : '0'}>
                    {specProjectName}
                  </TagHeaderTopic>
                </RouteLink>
              )}
              <TagHeaderDate>{postDateStr}</TagHeaderDate>
            </>
          )}
        </TagHeader>
      )}
      <TagNameWrap>
        {data.is_partner_material === '1' && (
          <TagPartner>
            <TagName>Партнерский материал</TagName>
          </TagPartner>
        )}
        {data && (
          <LinkTo to={`/${_get(data, 'tags.url', '')}`}>
            <TagName bgColor={`#${_get(data, 'tags.color')}`}>
              {_get(data, 'tags.name')}
            </TagName>
          </LinkTo>
        )}
        {data &&
          data.postTags &&
          data.postTags.map((el, i) => {
            return (
              <LinkTo to={`/${_get(el, 'url', '')}`} key={i}>
                <TagName bgColor="#fff">{el.name}</TagName>
              </LinkTo>
            );
          })}
      </TagNameWrap>
    </TagWrap>
  );
};
export default TagsArticlePage;
