import styled from 'styled-components/macro';
import { Link } from 'react-router-dom';

export const TagWrap = styled.div`
  max-width: 755px;
  margin: 0 auto;
  padding: 26px 0 44px 0;
`;

export const LinkTo = styled(Link)`
  line-height: 1.4;
`;

export const TagHeader = styled.div`
  display: flex;
  margin-bottom: 16px;
`;

export const TagHeaderTopic = styled.span`
  margin-right: ${({ margin }) => margin};
  font-family: 'MullerRegular', sans-serif;
  font-size: 12px;
  font-weight: 500;
  line-height: 1.4;
  letter-spacing: 0.24px;
  text-align: left;
  color: ${({ theme }) => (theme.font_color ? theme.font_color : '#000')};
  text-transform: uppercase;
  display: block;
`;

export const TagHeaderDate = styled(TagHeaderTopic)`
  margin-right: 0;
`;

export const TagNameWrap = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

export const TagName = styled.div`
  padding: 4px 7px 2px 10px;
  margin: 5px 6px 5px 0;
  border-radius: 5px;
  font-family: 'MullerRegular', sans-serif;
  font-size: 12px;
  line-height: 1.4;
  letter-spacing: 0.24px;
  text-align: left;
  text-transform: uppercase;
  color: #000;
  background-color: ${({ bgColor }) => bgColor};
`;
export const TagPartner = styled.div`
  ${TagName} {
    box-sizing: border-box;
    border: 1px solid;
    border-color: ${({ theme }) => {
      return theme.border_color && theme.border_color !== ''
        ? theme.border_color
        : '#000';
    }};
    padding: 3px 7px 1px 10px;
    line-height: 1.4;
    color: ${({ theme }) => {
      return theme.font_color && theme.font_color !== ''
        ? theme.font_color
        : '#000';
    }};
  }
`;
