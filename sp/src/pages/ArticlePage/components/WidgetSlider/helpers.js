const nameAttr = 'data-widget-name';

const createNewScript = ({ data, count }) => {
  var newScript = document.createElement('script');
  newScript.setAttribute(nameAttr, data[count].name);
  newScript.type = 'text/javascript';
  newScript.async = true;
  newScript.src = data[count] && data[count].src;
  newScript.charset = 'utf-8';
  document.head.appendChild(newScript);
  newScript.onload = () => {
    if (data[count] && data[count].fn) {
      /*eslint-disable no-eval */
      eval(data[count].fn);
    }
  };
};

export { createNewScript };
