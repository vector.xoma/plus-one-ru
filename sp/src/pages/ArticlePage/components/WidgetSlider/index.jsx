import React, { useEffect } from 'react';
import { WidgetContainer } from './style';
import { createNewScript } from './helpers';

const WidgetSlider = ({ widgetData: { count, data } }) => {
  useEffect(() => {
    if (!data[count]) return;
    createNewScript({ data, count });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);
  return (
    <WidgetContainer>
      {data[count] && (
        <div dangerouslySetInnerHTML={{ __html: data[count].root }} />
      )}
    </WidgetContainer>
  );
};

export default React.memo(WidgetSlider);
