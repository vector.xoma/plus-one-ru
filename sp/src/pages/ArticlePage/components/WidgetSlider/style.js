import styled from 'styled-components/macro';

const WidgetContainer = styled.div`
  max-width: 1140px;
  margin: 0 auto;
  .widget-wrap {
    margin: 20px 0;
  }
`;
export { WidgetContainer };
