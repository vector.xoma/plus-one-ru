function getCoords(elem, scrollY) {
  let box = elem && elem.getBoundingClientRect();
  return {
    top: box.top + scrollY,
  };
}

const onSetPosition = ({
  scrollY,
  setPositionArticle,
  articleWrap,
  cbFn = el => setPositionArticle([getCoords(el, scrollY)]),
}) => {
  articleWrap &&
    articleWrap.current &&
    Array.prototype.map.call(
      articleWrap.current.querySelectorAll('.article-js'),
      cbFn,
    );
};

/*
 ** /<script.*><\/script>/gm - тип <script src="..."><\/script>
 ** (?<=src=)".*? - поиск занчения в  src
 ** <script.*>.*<\/script> - все типы скриптов
 ** (?<=script.*>) ?\(.*(?=<\/script>) - для функции внутри script
 */
const preparedWidgetCode = ({ widgetCode, name }) => {
  let root = widgetCode;
  let src;
  // находим скрипты с src, обрабатываем и удаляем из строки
  if (/<script.*><\/script>/gm.test(widgetCode)) {
    // Выдергиваем script Из строки
    const scriptSrc = widgetCode.match(/<script.*><\/script>/gm);
    // ответ в виде - src="//smi2.ru/data/js/94748.js"
    src = scriptSrc[0].match(/src="([^"]*)"/gm)[0];
    // Очищаем от ковычек и от src=
    if (src) src = src.replace(/["]/g, '').replace(/src=/g, '');
    // очищаем строку от скрипта чтобы оставался только div
    root = root.replace(/<script.*><\/script>/gm, '');
  }
  // обрабатывем скрипты содержащие anon. fn. внутри и удаляем из строки
  const fn = root.match(/\(function.*()\)/gm);
  if (fn) {
    root = root.replace(/<script.*>.*<\/script>/gm, '');
  }
  // на выходе root должен содержать div структуру
  return { root, src, fn: fn && fn[0], name };
};

export { getCoords, onSetPosition, preparedWidgetCode };
