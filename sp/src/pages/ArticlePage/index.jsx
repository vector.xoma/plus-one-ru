import React, { useState, useEffect, useRef } from 'react';
import { useParams, useHistory } from 'react-router-dom';

import _get from 'lodash/get';
import _isEmpty from 'lodash/isEmpty';
import InfiniteScroll from 'react-infinite-scroll-component';
import styled, { ThemeProvider } from 'styled-components/macro';

import { Article } from 'connectors/query/Article';
import HeaderComponent from 'components/HeaderComponent/HeaderComponent';
import HeaderBottom from 'components/HeaderBottom';
import HeaderFixed from 'components/HeaderFixed';
import useScroll from 'hooks/useScroll';
import useCleanParamsUrl from 'hooks/useCleanParamsUrl';
import ArticleElem from './components/ArticleElem';
import DropDown from 'components/DropDown';
import useMetaTags from 'utils/hooks/useMetaTags';
import Banner from 'components/Banner';
import { useSearchInput } from 'contexts/searchInputContext';
import { useLocation } from 'react-router-dom';

import { Widget } from 'connectors/query/Widget';
import { getCoords, onSetPosition, preparedWidgetCode } from './helpers';

const articlePageStyles = {
  btn: { background: '#000' },
  logos: true,
};
const animationstyles = {
  btn: { opacity: '0' },
  headerWrap: { background: 'transparent', boxShadow: 'none' },
  input: { transform: 'translateY(80px)' },
};
const Wrap = styled.div`
  height: auto;
  background: #eaeaea;
  min-height: 100vh;
`;

const ArticlePage = () => {
  const { activeAnimationHeader, setActiveAnimationHeader } = useSearchInput();
  const [positionArticle, setPositionArticle] = useState([]);
  const [fullListArticle, setFullListArticle] = useState([]);
  const [counterArticle, setCounterArticle] = useState(0);
  const [scrollY, setScrollY] = useState(window.scrollY);
  const [fixedHeader, setFixedHeader] = useState(false);
  const [numbArticle, setNumbArticle] = useState(0);
  const [widgetData, setWidgetData] = useState([]);
  const [headerRef, setHeaderRef] = useState(null);
  const [visible, setVisible] = useState(false);
  const [data, setData] = useState([]);
  const [countBlockAnnounce, setCountBlockAnnounce] = useState(0);

  // когда рубрика одна и таже, но меняется статья не происходит редирект
  // forceReload для корректной работы роутинга
  const [forceReload, setForceReload] = useState(false);
  const { articleName } = useParams();

  const articleWrap = useRef(null);
  const bannerWrap = useRef(null);
  const history = useHistory();
  useMetaTags({ meta: data[numbArticle] && data[numbArticle].meta });
  //  useCleanParamsUrl(['?fbclid=', '?utm_']);

  const { pathname, search } = useLocation();
  const fullPathname = `${pathname.replace('/article', '')}${search}`;

  const isStableDev = pathname.includes('устойчивое-развитие');
  const news = fullPathname.includes('news');

  useEffect(() => {
    onSetPosition({
      scrollY,
      setPositionArticle,
      articleWrap,
      cbFn: el =>
        setPositionArticle([...positionArticle, getCoords(el, scrollY)]),
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);

  // логика изменения урла при скролле
  useEffect(() => {
    let currentArticleNumb = 0;
    for (let i = 0; i < positionArticle.length; i++) {
      const top = positionArticle[i].top;
      if (top < scrollY) currentArticleNumb = i;
      if (top > scrollY) break;
    }
    if (numbArticle !== currentArticleNumb && data[currentArticleNumb]) {
      setNumbArticle(currentArticleNumb);
      setForceReload(true);
      history.replace(`/${data[currentArticleNumb].fullUrl}`, {});
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [scrollY, positionArticle]);

  const scrollAction = () => {
    setScrollY(window.scrollY);
  };
  useScroll(scrollAction);

  const isHeaderFixedVisible = () => {
    if (headerRef && bannerWrap.current) {
      return (
        scrollY >=
        parseInt(getComputedStyle(headerRef).height) +
          parseInt(getComputedStyle(bannerWrap.current).height)
      );
    }
  };

  useEffect(() => {
    setFixedHeader(isHeaderFixedVisible());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [scrollY]);

  useEffect(() => {
    async function getNews() {
      const getArticle = async () => {
        const loc = window.location.pathname.split('/');
        const str = loc[loc.length - 1];
        if (isStableDev) {
          return await Article.getStableDevelopment(str);
        }

        return news
          ? await Article.getNews(fullPathname)
          : await Article.getPage(fullPathname);
      };

      const dataPage = await getArticle();

      // когда статья уже просколина и какая то ссылка ведет на неё же
      // то скролл неверно устанавливался
      await window.scrollTo(0, 0);

      const item = _get(dataPage, 'item', []);
      const meta = _get(dataPage, 'meta', []);
      const blockAnnouncements = _get(dataPage, 'blockAnnouncements', []);

      // если страница не вернула статью
      if (!item || _isEmpty(item)) {
        history.push('/404');
      }
      const nextPost = _get(dataPage, 'urlNextPost', []);

      setData([
        {
          ...item,
          firstArticle: true,
          meta: { ...meta },
          blockAnnouncements,
        },
      ]);
      setFullListArticle(nextPost);
      setNumbArticle(0);
      setPositionArticle([]);
      onSetPosition({
        scrollY,
        setPositionArticle,
        articleWrap,
      });
    }
    setForceReload(false);
    if (!forceReload) getNews();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [articleName, pathname]);

  const getNextPage = async () => {
    // Не запускать при первой инициализации

    if (_isEmpty(data)) return;
    if (fullListArticle.length < counterArticle - 1) return;
    const { item, meta = [] } = await Article.getNextPage(
      fullListArticle[counterArticle],
    );

    setCounterArticle(counterArticle + 1);

    // часто возвращает одну и ту же статью.
    if (!item) return;
    if (data.find(article => article.id === item.id)) return;

    setData([...data, { ...item, meta: { ...meta } }]);
  };

  // Счётчик для Блоков анонсов , setState не подходит

  const getAnnounceIndex = i => {
    let counter = 0;
    if (data.length > 0 && data[0].blockAnnouncements) {
      for (let j = 0; j < i; j++) {
        if (!data[j].relatedPosts || data[j].relatedPosts.length === 0) {
          counter++;
        }
      }
    }
    return counter % data[0].blockAnnouncements.length;
  };

  useEffect(() => {
    const getWidget = async () => {
      const result = await Widget.getWidget();
      const preparedWidget = result.map(
        ({ widget_code: widgetCode = '', widget_name: name }) => {
          return preparedWidgetCode({ widgetCode, name });
        },
      );
      setWidgetData(preparedWidget);
    };
    getWidget();

    // Очищать данные при клике назад.
    // Если произошел переход на связанную статью
    // Далее кликнули кнопку назад в браузере
    // Данные в data будут уже лежать
    // стянутся данные связанной статьи а не старой
    const cleanData = () => setData([]);
    window.addEventListener('popstate', cleanData, false);
    return () => window.removeEventListener('popstate', cleanData, false);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <Wrap isarticlepage={1}>
        {data[0] && data[0].firstArticle && (
          <Banner
            bannerStyle={{ backgroundColor: '#EAEAEA' }}
            elemId={data[0].id}
            ref={bannerWrap}
            p2Desctop="ghgl"
            p2Mobile="ghgm"
          />
        )}
        <HeaderFixed
          setActiveAnimationHeader={setActiveAnimationHeader}
          activeAnimationHeader={activeAnimationHeader}
          animationstyles={animationstyles}
          setFixedHeader={setFixedHeader}
          setVisible={setVisible}
          visible={fixedHeader}
        />
        <DropDown
          isHeaderFixedOpen={fixedHeader}
          setVisible={setVisible}
          visible={visible}
        />
        <div className="header-wrap">
          <HeaderBottom
            articlePageStyles={articlePageStyles}
            setHeaderRef={setHeaderRef}
            setVisible={setVisible}
            visible={visible}
            isarticlepage={1}
            activeAnimationHeader={activeAnimationHeader}
            setActiveAnimationHeader={setActiveAnimationHeader}
          />
        </div>
        <InfiniteScroll
          style={{ overflowY: 'hidden ' }}
          scrollThreshold={'2200px'}
          dataLength={data.length}
          next={getNextPage}
          endMessage={''}
          hasMore={true}
        >
          <div className="article-wrap-js" ref={articleWrap}>
            {data.map((article, i) => {
              const customColorTheme = {
                body_color: article.body_color,
                border_color: article.border_color,
                font_color: article.font_color,
              };
              return (
                <ThemeProvider
                  theme={customColorTheme}
                  key={article.id ? article.id : i}
                >
                  <ArticleElem
                    countBlockAnnounce={getAnnounceIndex(i)}
                    blockAnnouncements={
                      data[0].blockAnnouncements && data[0].blockAnnouncements
                    }
                    pageData={article}
                    widgetData={{
                      data: widgetData,
                      count: i % widgetData.length,
                    }}
                  />
                </ThemeProvider>
              );
            })}
          </div>
        </InfiniteScroll>
      </Wrap>
    </>
  );
};

export default React.memo(ArticlePage);
