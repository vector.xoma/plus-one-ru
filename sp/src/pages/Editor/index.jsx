import React, { useState, useEffect, useRef } from 'react';
import InputData from 'containers/InputData';
import Preview from 'containers/Preview';
import styled from 'styled-components/macro';

import useZoom from 'hooks/useZoom';
// import FileManager from "containers/FileManager";

// import "./styles.scss";

const EditorWorkPlace = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
`;

const EditorPreviewPlace = styled.div`
  /* width: 50%; */
  padding: 5px;
  background-color: #eaeaea;
  overflow-y: auto;
  order: 1;
  ${({ areaName }) => {
    if (areaName === 'header') {
      return { height: '200px' };
    }
    return areaName === 'body'
      ? { height: '600px', minHeight: '300px', resize: 'vertical' }
      : { height: '400px' };
  }}
`;
const PreviewWrap = styled.div`
  height: 100%;
`;

const Editor = ({ updateData, serverData = null, areaName }) => {
  const [data, setData] = useState(serverData || {});
  const editorWrapper = useRef(null);

  useEffect(() => {
    updateData(data);
  }, [data, updateData]);

  const updateField = fieldData => setData(fieldData);

  const previewRef = useRef(null);

  const zoom = useZoom(previewRef);

  // return <FileManager />;

  return (
    <div className="editor_wrapper" ref={editorWrapper}>
      <EditorWorkPlace areaName={areaName}>
        <InputData
          data={data}
          updateField={updateField}
          areaName={areaName}
          editorWrapper={editorWrapper.current}
        />
        <EditorPreviewPlace
          className="editor_preview-place"
          ref={previewRef}
          areaName={areaName}
          // zoom={zoom}
        >
          {/* <div style={{ transform: `scale(${zoom})` }}> */}
          <div>
            <PreviewWrap zoom={zoom}>
              <Preview
                data={data}
                showTitle
                zoom={zoom}
                areaName={areaName}
                isAdminPage
                lead={areaName === 'lead'}
              />
            </PreviewWrap>
          </div>
        </EditorPreviewPlace>
      </EditorWorkPlace>
    </div>
  );
};

export default Editor;
