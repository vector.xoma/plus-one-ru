import React, { useEffect, useState } from 'react';
import { useRowsBlocks } from 'contexts/rowsBlocksContext';
import _isEmpty from 'lodash/isEmpty';

import Preview from 'containers/Preview';
import BlocksViewer from 'containers/BlocksViewer';
import BlackPagesOuter from 'containers/BlackPagesOuter';
import NewsSlider from 'components/NewsSlider';
import VideoPost from 'components/VideoPost';
import useMetaTags from 'utils/hooks/useMetaTags';
import useCleanParamsUrl from 'hooks/useCleanParamsUrl';
import SubscribeConfirmMessage from 'components/Block/components/SubscribeConfirmMessage';
import { useParams } from 'react-router-dom';

// TODO: Пока не известно что приходит с бэка
// const frames = [
//   'https://www.youtube.com/embed/0LAdAV-jHhw',
//   'https://www.youtube.com/embed/kE04ATMQyso',
//   'https://www.youtube.com/embed/zWvukq0Wa4M?list=TLPQMjQwMzIwMjBggxMqW-lEHg',
// ];
const videoData = {
  img: 'https://klike.net/uploads/posts/2019-03/1551516106_1.jpg',
  frames: [
    {
      frame:
        '<iframe src="https://player.vimeo.com/video/119038131?color=ffffff&portrait=0" width="640" height="272" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>',
      name: 'Vimeo Patrouille Suisse and Airbus A320 Swiss Lauberhorn 2015',
      currentTime: 600,
      description: '',
    },
    {
      frame:
        '<iframe  width="640" height="360" src="https://play.live.dfw.ru/stream/id1038/6900.html" frameborder="0" border="0" allowfullscreen webkitallowfullscreen mozallowfullscreen scrolling="no"></iframe>',
      name: 'Плеер Росконгресс',
      currentTime: 300,
      description: '',
    },
    {
      frame:
        '<iframe width="560" height="315" src="https://www.youtube.com/embed/dvDyK8QFDYU?start=60" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
      name: 'Youtube: «А такие кавычки работают?»',
      currentTime: 0,
      description: '',
    },
  ],
};

const MainPage = ({ data = { text: '' }, confirmSubscribe = false }) => {
  const { rows: blocksList, setRows, getRows } = useRowsBlocks();
  useMetaTags();
  //  useCleanParamsUrl(['?fbclid=', '?utm_']);
  useEffect(() => {
    window.scrollTo(0, 0);
  });
  const { hashConfirmSubscribe } = useParams();

  // // Необходимо стягивать данные, когда бек обновился.
  // контекст стягивает данные единожды
  // В идеале бы использовать webSocket
  useEffect(() => {
    // данные ранее были стянуты
    if (!_isEmpty(blocksList)) getRows(setRows);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <BlackPagesOuter isMainPage>
        {confirmSubscribe ? <SubscribeConfirmMessage /> : null}
        {/* TODO: Доделать когда будет ясно как должно работать */}
        {/* <VideoPost posts={videoData.frames} img={videoData.img} /> */}
        <NewsSlider />
        {blocksList ? (
          <BlocksViewer rowsOfBlocks={blocksList} isMainPage />
        ) : (
          <Preview data={data} />
        )}
      </BlackPagesOuter>
    </>
  );
};

export default MainPage;
