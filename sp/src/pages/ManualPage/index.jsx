import React, { useState, useEffect } from 'react';
import useMetaTags from 'utils/hooks/useMetaTags';

import BlocksViewer from 'containers/BlocksViewer';
import BlackPagesOuter from 'containers/BlackPagesOuter';
import { Manual } from 'connectors/query/Manual';

const ManualPage = () => {
  const [data, setData] = useState({});
  const { items, meta, writerInfo } = data;
  useEffect(() => {
    async function getData() {
      const { items, meta, ...values } = await Manual.getList();
      setData({ items, meta, writerInfo: { ...values } });
    }
    getData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  useMetaTags({ meta });

  return (
    <BlackPagesOuter isMainPage>
      <BlocksViewer rubricValues={writerInfo} blockList={items} masonry />
    </BlackPagesOuter>
  );
};

export default ManualPage;
