import React, { useEffect } from 'react';
import _isEmpty from 'lodash/isEmpty';

import { useNewsBlocks } from 'contexts/newsBlocksContext';
import useCleanParamsUrl from 'hooks/useCleanParamsUrl';
import BlocksViewer from 'containers/BlocksViewer';
import BlackPagesOuter from 'containers/BlackPagesOuter';

const RubricBlocksList = () => {
  const {
    news: { data, meta },
    page,
    setPage,
    setNews,
    getNews,
  } = useNewsBlocks();
  //  useCleanParamsUrl(['?fbclid=', '?utm_']);

  const incrementPage = () => setPage(page + 1);

  // // Необходимо стягивать данные, когда бек обновился.
  // контекст стягивает данные единожды
  // В идеале бы использовать webSocket
  useEffect(() => {
    // данные ранее были стянуты
    if (!_isEmpty(data)) {
      getNews({
        setNews,
        page,
        data,
      });
    }
    return () => setPage(1);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <BlackPagesOuter isMainPage>
      <BlocksViewer
        incrementPage={incrementPage}
        blockList={data}
        meta={meta}
        staticTitle={'Новости'}
        masonry
      />
    </BlackPagesOuter>
  );
};

export default RubricBlocksList;
