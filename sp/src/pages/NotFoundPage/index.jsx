import React, { useState, useEffect } from 'react';

import {
  ErrorPageContainer,
  ErrorTextWrap,
  ErrorText,
  LogoWrap,
  LogoImg,
} from './style';
import logo from 'img/plus_one_logo.svg';
import BlackPagesOuter from 'containers/BlackPagesOuter';

const NotFoundPage = () => {
  const [animate, setAnimate] = useState(false);
  useEffect(() => setAnimate(true), [animate]);
  return (
    <BlackPagesOuter isErrorPage>
      <ErrorPageContainer>
        <LogoWrap to="/">
          <LogoImg src={logo} />
        </LogoWrap>
        <ErrorTextWrap>
          <ErrorText animate={animate}>404</ErrorText>
        </ErrorTextWrap>
      </ErrorPageContainer>
    </BlackPagesOuter>
  );
};

export default NotFoundPage;
