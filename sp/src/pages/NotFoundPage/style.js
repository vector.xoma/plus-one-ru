import styled, { keyframes } from 'styled-components/macro';
import { Link } from 'react-router-dom';

const shakeAnimation = keyframes`
  from,
  to {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }

  10%,
  30%,
  50%,
  70%,
  90% {
    -webkit-transform: translate3d(-10px, 0, 0);
    transform: translate3d(-10px, 0, 0);
  }

  20%,
  40%,
  60%,
  80% {
    -webkit-transform: translate3d(10px, 0, 0);
    transform: translate3d(10px, 0, 0);
  }
`;

const ErrorPageContainer = styled.div`
  max-width: 100%;
  text-align: center;
`;

const LogoWrap = styled(Link)`
  width: 120px;
  height: 63px;
  margin: 34px auto 40px auto;
  display: block;
`;

const LogoImg = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
  display: block;
  @media (max-width: 767px) {
    margin-bottom: 0;
  }
`;
const ErrorTextWrap = styled.div`
  text-align: center;
  min-height: 530px;
  display: flex;
  justify-content: center;
  align-items: center;
  @media (max-width: 767px) {
    min-height: 200px;
  }
`;

const ErrorText = styled.p`
  margin: auto;
  font-size: 21vw;
  font-family: 'CSTMXprmntl02-Bold';
  color: #fff;
  display: block;
  margin: auto;
  line-height: normal;
  font-family: 'CSTMXprmntl02-Bold', sans-serif;
  color: #fff;
  animation-name: ${({ animate }) => (animate ? shakeAnimation : 'none')};
  animation-duration: 1s;
  animation-iteration-count: 1;

  text-align: center;
  @media (max-width: 767px) {
    font-size: 110px;
  }
`;

export { ErrorPageContainer, LogoWrap, LogoImg, ErrorTextWrap, ErrorText };
