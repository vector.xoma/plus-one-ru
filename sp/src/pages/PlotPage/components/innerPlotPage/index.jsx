import React, { useState, useEffect } from 'react';
import useMetaTags from 'utils/hooks/useMetaTags';

import BlocksViewer from 'containers/BlocksViewer';
import BlackPagesOuter from 'containers/BlackPagesOuter';
import { Plot } from 'connectors/query/Plot';

const InnerPlotPage = ({ match: { params: { url } = {} } = {} }) => {
  const [data, setData] = useState({});
  const { items, meta, writerInfo } = data;
  useEffect(() => {
    async function getPlotData(url) {
      const result = await Plot.getInnerPlot(url);
      setData(result);
    }
    getPlotData(url);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  useMetaTags({ meta });
  return (
    <BlackPagesOuter isMainPage>
      <BlocksViewer rubricValues={writerInfo} blockList={items} masonry />
    </BlackPagesOuter>
  );
};

export default InnerPlotPage;
