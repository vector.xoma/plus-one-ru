import React, { useState, useEffect } from 'react';
import styled, { ThemeProvider } from 'styled-components/macro';
import useMetaTags from 'utils/hooks/useMetaTags';
import useCleanParamsUrl from 'hooks/useCleanParamsUrl';

import BlackPagesOuter from 'containers/BlackPagesOuter';
import { Plot } from 'connectors/query/Plot';
import InfiniteScroll from 'react-infinite-scroll-component';
import { Title } from 'containers/BlocksViewer/components/MasonryPage/style';
import Group from 'components/Block/components/Group';
import { CountersTitle } from '../../containers/BlocksViewer/components/CountersPage/style';
import TitleBlock from '../../components/TitleBlock';
import useDesktop from '../../hooks/useDesktop';

const Container = styled.div`
  width: 1140px;
  max-width: 100%;
  margin: 0 auto;
  @media (max-width: 767px) {
    padding: 0 10px;
    width: 100%;
  }
`;

const PlotPage = ({ titleBlock }) => {
  const [data, setData] = useState({ items: [], writerInfo: {} });
  const { items, meta, writerInfo } = data;
  const isDesktop = useDesktop();
  useEffect(() => {
    async function getPlotData() {
      const { items, meta, ...values } = await Plot.getPlot();
      setData({ items, meta, writerInfo: { ...values } });
    }
    getPlotData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  useMetaTags({ meta });
  //  useCleanParamsUrl(['?fbclid=', '?utm_']);
  const theme = { color: '#fff' };
  return (
    <BlackPagesOuter isMainPage>
      <ThemeProvider theme={theme}>
        <Container>
          {!titleBlock ? (
            <Title>Сюжеты</Title>
          ) : (
            <TitleBlock
              info={writerInfo}
              src={isDesktop ? writerInfo.imageDesktop : writerInfo.imageMobile}
            />
          )}
          <InfiniteScroll
            dataLength={[]}
            next={() => {}}
            hasMore={true}
            style={{
              overflowY: 'hidden ',
            }}
            endMessage={''}
          >
            {items.map(({ postList, name, id, url } = {}) => (
              <Group data={postList} groupName={name} key={id} url={url} />
            ))}
          </InfiniteScroll>
        </Container>
      </ThemeProvider>
    </BlackPagesOuter>
  );
};

export default PlotPage;
