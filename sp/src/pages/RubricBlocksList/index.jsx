import React, { useEffect } from 'react';
import _isEmpty from 'lodash/isEmpty';

import { useRubric } from 'contexts/rubricsContext';
import BlocksViewer from 'containers/BlocksViewer';
import BlackPagesOuter from 'containers/BlackPagesOuter';

const RubricBlocksList = ({ rubric }) => {
  const {
    page,
    setPage,
    currentRubric,
    getRows,
    ...rubricsLists
  } = useRubric();
  const {
    meta,
    data: list,
    activeRubricSetter,
    rubrics,
    rubricsData,
    rubricValues = {},
  } = rubricsLists[rubric];
  const incrementPage = () => setPage(page + 1);
  // TODO: передалать
  // Необходимо стягивать данные, когда бек обновился.
  // контекст стягивает данные единожды
  // В идеале бы использовать webSocket
  useEffect(() => {
    // данные ранее были стянуты
    if (!_isEmpty(list)) {
      getRows({
        activeRubricSetter,
        currentRubric,
        rubrics,
        rubricsData,
        page,
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <BlackPagesOuter isRubricPage>
      <BlocksViewer
        incrementPage={incrementPage}
        rubricValues={rubricValues}
        blockList={list}
        rubric={rubric}
        meta={meta}
        masonry
      />
    </BlackPagesOuter>
  );
};

export default RubricBlocksList;
