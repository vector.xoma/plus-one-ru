import React, { useState, useEffect } from 'react';
import BlocksViewer from 'containers/BlocksViewer';
import BlackPagesOuter from 'containers/BlackPagesOuter';
import { Search } from 'connectors/query/Search';
import { useLocation } from 'react-router-dom';
import _get from 'lodash/get';

const SearchResultList = () => {
  const { search } = useLocation();
  const [searchList, setSearchList] = useState([]);
  const [fullLegth, setFullLegth] = useState([]);
  const [page, setPage] = useState(1);
  useEffect(() => {
    // при инифити скролле
    async function addList() {
      const result = await Search.getSearchResult(search.substr(1), page);
      if (page !== 1) {
        setSearchList([...searchList, ..._get(result, 'items', [])]);
        setFullLegth(_get(result, 'countItems', ''));
        return;
      }
    }
    addList();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  useEffect(() => {
    // для обычного поиска
    async function getList() {
      const result = await Search.getSearchResult(search.substr(1), page);
      setSearchList(_get(result, 'items', []));
      setFullLegth(_get(result, 'countItems', ''));
    }
    getList();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [search]);

  const incrementPage = () => setPage(page + 1);

  return (
    <BlackPagesOuter
      searchTitle={decodeURI(search).substr(1)}
      listLength={fullLegth}
      setPage={setPage}
      isSearchPage
    >
      <BlocksViewer
        incrementPage={incrementPage}
        blockList={searchList}
        masonry
      />
    </BlackPagesOuter>
  );
};

export default SearchResultList;
