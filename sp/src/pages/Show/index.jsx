import React from 'react';

import Preview from 'containers/Preview';
import HeaderBottom from "../../components/HeaderBottom";
import BlackPagesOuter from "../../containers/BlackPagesOuter";

// страница "Полный экран" для редактора
const Show = ({ data }) => {
  let isAboutPage = !!document.getElementById('reactEditor_about');
  let content = isAboutPage ?
      (<BlackPagesOuter isAboutPage={isAboutPage}>
          <Preview data={data} modal={1}/>
      </BlackPagesOuter>)
      :
      (
        <>
          <HeaderBottom isarticlepage={1}/>
          <Preview data={data} isarticlepage={'1'} modal={1}/>
        </>);

  return (
      <>
          {content}
      </>
  );
};

export default Show;
