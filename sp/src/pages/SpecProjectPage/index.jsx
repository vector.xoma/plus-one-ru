import React, { useState, useEffect } from 'react';
import { SpecProject } from 'connectors/query/SpecProject';
import BlackPagesOuter from 'containers/BlackPagesOuter';
import Post from 'components/Block/components/Post';
import styled from 'styled-components/macro';
import useDesktop from 'hooks/useDesktop';
import useCleanParamsUrl from 'hooks/useCleanParamsUrl';
import useMetaTags from 'utils/hooks/useMetaTags';

const MainContainer = styled.div`
  max-width: 1140px;
  margin: 0 auto;
  @media (max-width: 767px) {
    padding: 0 10px;
  }
`;

const Title = styled.h1`
  font-size: 68px;
  color: #fff;
  font-family: 'MullerBold';
  margin-bottom: 50px;
  margin-top: 45px;
  @media only screen and (max-width: 480px) {
    font-size: 32px;
  }
`;

const SpecProjectPage = () => {
  const [articles, setArticles] = useState({ items: [] });
  const isDesktop = useDesktop();

  useMetaTags({ meta: articles.meta });
  //  useCleanParamsUrl(['?fbclid=', '?utm_']);

  useEffect(() => {
    const getSpecProjects = async () => {
      const { items, meta } = await SpecProject.get();
      // данные items приходят в формате {{},{}}
      setArticles({ items: Object.values(items), meta });
    };
    getSpecProjects();
  }, []);
  const block = isDesktop ? '1_1' : '1_3';

  return (
    <>
      {/* <useMetaTags /> */}
      <BlackPagesOuter isRubricPage>
        <MainContainer>
          <Title>Спецпроекты +1</Title>
          {articles.items.map(data => (
            <Post
              data={{ ...data, block }}
              key={data.id}
              isSpecProjectPage
              image={data.image_1_1}
              imageMobile={data.image_1_3 ? data.image_1_3 : data.image_1_1}
            />
          ))}
        </MainContainer>
      </BlackPagesOuter>
    </>
  );
};

export default SpecProjectPage;
