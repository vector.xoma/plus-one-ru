import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import _get from 'lodash/get';

import BlocksViewer from 'containers/BlocksViewer';
import BlackPagesOuter from 'containers/BlackPagesOuter';
import { Blocks } from 'connectors/query/Blocks';

const TagBlocksList = ({ rubric }) => {
  const [data, setData] = useState({ list: [] });
  const [page, setPage] = useState(1);
  const { pathName } = useParams();
  const fullPathname = window.location.pathname;

  const incrementPage = () => {
    // обработка кейса, когда на короткой странице масонри (из одной страницы)
    // улетает сразу пачка запросов. приводило к тому, что данные с 1й страницы
    // не успевали подгрузиться и тут же улетал 2й запрос. в итоге данные с 1й
    // странцы затирались пустым массивом из 2й страницы
    if (page === 1 && _get(data, 'list.length', 0) === 0) return;
    setPage(page + 1);
  };

  useEffect(() => {
    if (fullPathname.includes('author')) {
      if (page === 1) {
        Blocks.getTagList(`author/${pathName}`, page).then(res => {
          const list = _get(res, 'posts', []);
          const meta = _get(res, 'meta', {});
          const writerInfo = _get(res, 'writerInfo', {});
          setData({ list, meta, writerInfo });
        });
      }
      if (page !== 1) {
        Blocks.getTagList(`${rubric}/${pathName}`, page).then(res => {
          const posts = _get(res, 'posts', []) || [];
          const meta = _get(res, 'meta', {});
          setData({
            list: [...data.list, ...posts],
            writerInfo: data.writerInfo,
            meta,
          });
        });
      }
    }

    if (fullPathname.includes('tag')) {
      if (page === 1) {
        Blocks.getTagList(`tag/${rubric}/${pathName}`).then(res => {
          const list = _get(res, 'posts', []);
          const meta = _get(res, 'meta', {});
          setData({ list, meta });
        });
      }
      if (page !== 1) {
        Blocks.getTagList(`tag/${rubric}/${pathName}`, page).then(res => {
          const posts = _get(res, 'posts', null);
          const meta = _get(res, 'meta', {});
          posts && setData({ list: [...data.list, ...posts], meta });
        });
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [rubric, page]);

  return (
    <BlackPagesOuter isMainPage>
      <BlocksViewer
        rubricValues={data.writerInfo}
        incrementPage={incrementPage}
        blockList={data.list}
        meta={data.meta}
        masonry
      />
    </BlackPagesOuter>
  );
};

export default TagBlocksList;
