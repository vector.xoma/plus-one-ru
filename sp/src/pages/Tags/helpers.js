import { StableDevelopment } from 'connectors/query/StableDevelopment';
import { Participants } from 'connectors/query/Participants';
import _get from 'lodash/get';
// TODO: если не нужно - удалить
const translate = {
  tag: 'Тэги',
  leaders: 'Лидеры',
};
// TODO: если не нужно - удалить
const path = {
  tag: 'tags',
  leaders: 'authors',
};

const getData = async ({ isGrowth, isParticipants, setData }) => {
  let res;
  if (isGrowth) {
    res = await StableDevelopment.getList();
  }
  if (isParticipants) {
    res = await Participants.getList();
  }
  const { items, meta, ...values } = res;

  setData({ items, meta, writerInfo: { ...values } });
  return;
};

export { getData };
