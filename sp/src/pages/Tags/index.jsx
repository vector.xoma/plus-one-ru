import React, { useState, useEffect } from 'react';
import _get from 'lodash/get';

import CountersPage from 'containers/BlocksViewer/components/CountersPage';
import BlackPagesOuter from 'containers/BlackPagesOuter';
// TODO: если не нужно - удалить
import { Tags } from 'connectors/query/Tags';

import { getData } from './helpers';
import useDesktop from '../../hooks/useDesktop';

const TagsPage = ({
  type,
  rubric,
  isGrowth = false,
  showCount = true,
  titleBlock,
  isParticipants,
}) => {
  const [data, setData] = useState({});
  const { items, meta, writerInfo = {} } = data;
  const isDesktop = useDesktop();
  useEffect(() => {
    if (isGrowth || isParticipants) {
      getData({ isGrowth, isParticipants, setData });
      return;
    }
    // TODO: если не нужно - удалить
    // Tags.getList(type, rubric).then(res => {
    //   const list = _get(res, `${path[type]}`, []);
    //   const meta = _get(res, 'meta', {});
    //   setData({ list, meta });
    // });
    // }, [type, rubric, isGrowth]);
  }, []);
  const src = isDesktop ? writerInfo.imageDesktop : writerInfo.imageMobile;
  return (
    <BlackPagesOuter isMainPage>
      <CountersPage
        writerInfo={writerInfo}
        titleBlock={titleBlock}
        showCount={showCount}
        textColor="#fff"
        rubric={rubric}
        data={items}
        meta={meta}
        src={src}
      />
    </BlackPagesOuter>
  );
};

export default TagsPage;
