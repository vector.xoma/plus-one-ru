import React from 'react';

import Tags from 'pages/Tags';
import RubricBlocksList from 'pages/RubricBlocksList';
import SearchResultList from 'pages/SearchResultList';
import TagBlocksList from 'pages/TagBlocksList';
import NewsPage from 'pages/NewsPage';
import MainPage from 'pages/MainPage';
import AboutPage from 'pages/AboutPage';
import ArticlePage from 'pages/ArticlePage';
import SpecProjectPage from 'pages/SpecProjectPage';
import NotFoundPage from 'pages/NotFoundPage';
import PlotPage from 'pages/PlotPage';
import InnerPlotPage from 'pages/PlotPage/components/innerPlotPage';
import ManualPage from 'pages/ManualPage';

const routes = [];

// Static
routes.push(
  {
    path: '/search',
    component: SearchResultList,
  },
  {
    path: '/special-projects',
    component: SpecProjectPage,
  },
  {
    path: '/authors/platform',
    render: props => <Tags {...props} type="leaders" rubric="platform" />,
    exact: true,
  },
  {
    path: '/',
    component: MainPage,
    exact: true,
  },
  {
    path: '/news',
    component: NewsPage,
    exact: true,
  },
  {
    path: '/about',
    component: AboutPage,
    exact: true,
  },
  {
    path: '/story',
    render: props => <PlotPage titleBlock />,
    exact: true,
  },
  {
    path: '/story/:url',
    component: InnerPlotPage,
  },
  {
    path: `/${decodeURI('устойчивое-развитие')}`,
    render: props => (
      <Tags
        isGrowth
        showCount={false}
        titleBlock
        rubric="устойчивое-развитие"
      />
    ),
    exact: true,
  },
  {
    path: `/author/:pathName`,
    render: props => <TagBlocksList {...props} rubric="author" />,
    exact: true,
  },
  {
    path: '/author',
    render: props => <Tags titleBlock isParticipants />,
    exact: true,
  },
  {
    path: '/manual',
    component: ManualPage,
    exact: true,
  },
  {
    path: '/confirm-subscribe/:hashConfirmSubscribe',
    render: prors => <MainPage confirmSubscribe />,
    exact: true,
  },

  // Article Rubric Types
  {
    path: [
      '/ecology/:articleName',
      '/society/:articleName',
      '/economy/:articleName',
      '/platform/:articleName',
      '/news/:articleName',
      `/${decodeURI('устойчивое-развитие')}/:articleName`,
      `/manual/:aricleName`,
    ],
    component: ArticlePage,
  },
);
// TODO:Уточнить актуальны ли страницы leaders,tag если нет то убрать
// + убрать логику градиентов в списках в компонентн Tags
// Rubric Types
['ecology', 'society', 'economy', 'platform'].forEach(type => {
  routes.push(
    {
      path: `/${type}`,
      render: props => <RubricBlocksList {...props} rubric={type} />,
      exact: true,
    },
    {
      path: `/leaders/${type}`,
      render: props => <Tags {...props} type="leaders" rubric={type} />,
      exact: true,
    },
    {
      path: `/leaders/${type}/:pathName`,
      render: props => <TagBlocksList {...props} rubric={type} />,
      exact: true,
    },
    {
      path: `/tag/${type}`,
      render: props => <Tags {...props} type="tag" rubric={type} />,
      exact: true,
    },
    {
      path: `/tag/${type}/:pathName`,
      render: props => <TagBlocksList {...props} rubric={type} />,
      exact: true,
    },
  );
});

// Static
routes.push({
  component: NotFoundPage,
});

export { routes };
