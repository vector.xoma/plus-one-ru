const proxy = require('http-proxy-middleware');

module.exports = function (app) {
  app.use(
    proxy('https://dev.plus-one.ru/api', {
      changeOrigin: true,
      secure: false,
    }),
    proxy('https://dev.plus-one.ru/files', {
      changeOrigin: true,
      secure: false,
    }),
    proxy('https://dev.plus-one.ru/adminv2/api/v1/galleries', {
      changeOrigin: true,
      secure: false,
    }),
  );
};
