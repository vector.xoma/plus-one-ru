let isBootstrapped = false;
const _ = 'dataLayer';
window[_] = window[_] || [];

const GTM_ID = 'GTM-W7L3VJ7';

// Updated this whenever you change GTM settings!
// Otherwise client browser will get old cached version!
const CUSTOM_HASH = 'K65BJH7xSzUuT9CV';

function bootstrap() {
  isBootstrapped = true;

  window[_].push({
    'gtm.start': new Date().getTime(),
    event: 'gtm.js',
  });

  const GTMScriptTag = document.createElement('script');
  GTMScriptTag.async = true;
  GTMScriptTag.src = [
    'https://www.googletagmanager.com/gtm.js?id=',
    GTM_ID,
    '&h=',
    CUSTOM_HASH,
  ].join('');

  document.getElementsByTagName('head')[0].appendChild(GTMScriptTag);
}

export function pageView(url, title) {
  !isBootstrapped && bootstrap();

  window[_].push({
    event: 'Pageview',
    pagePath: url,
    pageTitle: title,
  });
}
