import { useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { hat } from 'utils/index';
import { pageView } from 'utils/gtm';
import _debounce from 'lodash/debounce';

import { defaultMetaData } from '../defaultMetaData';

const pageViewDebounced = _debounce(pageView, 500);

const useMetaTags = ({ meta, isMainPage = false } = {}) => {
  const { pathname, search } = useLocation();
  const metaData = meta || defaultMetaData;

  useEffect(() => {
    if (!isMainPage) hat(metaData);

    const defaultMetaForNonRoot = pathname !== '/' && !Boolean(meta);
    const noTitle = metaData.title == null;

    if (!defaultMetaForNonRoot && !noTitle) {
      pageViewDebounced(pathname, metaData.title);
    }

    if (pathname === '/search') {
      pageViewDebounced(
        `${pathname}${search}`,
        `${decodeURI(search).substring(1)} — Проект +1`,
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [metaData]);
};
export default useMetaTags;
