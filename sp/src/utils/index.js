export const clearTextSpaces = text =>
  text.replace(/\n\n/g, '\n').replace(/\s{2,}/g, ' ');

export const splitBySelection = (startText = '', area = {}) => ({
  start: startText.slice(0, area.selectionStart),
  middle: startText.slice(area.selectionStart, area.selectionEnd),
  end: startText.slice(area.selectionEnd),
});

export const hat = metaData => {
  if (Object.keys(metaData).length === 0) return;
  document.getElementsByTagName('title').item(0).innerText = metaData.title;

  Array.from(document.getElementsByTagName('link')).forEach(link => {
    switch (link.rel) {
      case 'image_src':
        link.href = metaData.ogImage;
        break;
      case 'canonical':
        link.href = `${window.location.origin}${window.location.pathname}`;
        break;
      default:
        break;
    }
  });

  Array.from(document.getElementsByTagName('meta')).forEach(meta => {
    switch (meta.name) {
      case 'title':
        meta.content = metaData.title;
        break;
      case 'item-image':
        meta.content = metaData.ogImage;
        break;
      case 'description':
        meta.content = metaData.description;
        break;
      case 'keywords':
        meta.content = metaData.keywords;
        break;
      case 'twitter:card':
        meta.content = metaData.twitterCard;
        break;
      case 'twitter:title':
        meta.content = metaData.twitterTitle;
        break;
      case 'twitter:description':
        meta.content = metaData.twitterDescription;
        break;
      case 'twitter:url':
        meta.content = metaData.twitterUrl;
        break;
      case 'twitter:image':
        meta.content = metaData.twitterImage;
        break;
      case 'twitter:image:alt':
        meta.content = metaData.twitterImageAlt;
        break;
      case 'twitter:site':
        meta.content = metaData.twitterSite;
        break;
      case 'vk:image':
        meta.content = metaData.vkImage;
        break;
      default:
        break;
    }

    switch (meta.getAttribute('property')) {
      case 'og:image':
        meta.content = metaData.ogImage;
        break;
      case 'og:title':
        meta.content = metaData.ogTitle;
        break;
      case 'og:type':
        meta.content = metaData.ogType;
        break;
      case 'og:description':
        meta.content = metaData.ogDescription;
        break;
      case 'og:site_name':
        meta.content = metaData.ogSiteName;
        break;
      case 'og:url':
        meta.content = metaData.ogUrl;
        break;
      default:
        break;
    }
  });
};
