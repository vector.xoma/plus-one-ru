jQuery(document).ready(function ($) {
	initBgImage();
	var mac = navigator.platform.match(/(Mac|iPhone|iPod|iPad)/i) ? true : false;
	if (mac) $('body').addClass('mac');
});

$(window).load(function () {
	if ($('.fotorama').size()) initGallery();
});

function initGallery() {
	$('.fotorama').each(function (index, el) {
		var _this = $(this),
			_parent = _this.closest('.fotorama-holder');
		var fotorama = $(this).data('fotorama');
		_this.on('fotorama:showend ', function (e, fotorama) {
			$('.numbering', _parent).html((fotorama.activeIndex + 1) + '/' + fotorama.size);
			var _cotent = $('.fotorama__active .fotorama__html div p', _parent).html();
			$('.info p', _parent).html(_cotent);
		}).fotorama({
			nav: 'thumbs',
			// width: 792,
			// height: 450,
			width: '100%',
			height: '100%',
			fit: 'cover',
			loop: true,
			margin: 0,
			arrows: true,
			arrows: false,
			thumbwidth: 72,
			thumbheight: 55,
			thumbmargin: 3,
			allowfullscreen: true
		});
		$('.numbering', _parent).html((fotorama.activeIndex + 1) + '/' + fotorama.size);
		var _cotent = $('.fotorama__active .fotorama__html div p', _parent).html();
		$('.info p', _parent).html(_cotent);
	});

	$('.fotorama__wrap').addClass('fotorama__wrap--no-controls');

	$('.fotorama-holder .next').click(function (event) {
		event.preventDefault();
		var fotorama = $(this).closest('.fotorama-holder').find('.fotorama').data('fotorama');
		fotorama.show('>');
	});
	$('.fotorama-holder .prev').click(function (event) {
		event.preventDefault();
		var fotorama = $(this).closest('.fotorama-holder').find('.fotorama').data('fotorama');
		fotorama.show('<');
	});
}

function initBgImage() {
	$('.bg').each(function (index, el) {
		var _src = $('> img', this).attr('src');
		$('> img', this).hide();
		$(this).css('background-image', 'url(' + _src + ')');
	});
}

// left: 37, up: 38, right: 39, down: 40,
// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
var keys = {
	37: 1,
	38: 1,
	39: 1,
	40: 1
};

function preventDefault(e) {
	e = e || window.event;
	if (e.preventDefault)
		e.preventDefault();
	e.returnValue = false;
}

function preventDefaultForScrollKeys(e) {
	if (keys[e.keyCode]) {
		preventDefault(e);
		return false;
	}
}

function disableScroll() {
	if (window.addEventListener) // older FF
		window.addEventListener('DOMMouseScroll', preventDefault, false);
	window.onwheel = preventDefault; // modern standard
	window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
	window.ontouchmove = preventDefault; // mobile
	document.onkeydown = preventDefaultForScrollKeys;
}

function enableScroll() {
	if (window.removeEventListener)
		window.removeEventListener('DOMMouseScroll', preventDefault, false);
	window.onmousewheel = document.onmousewheel = null;
	window.onwheel = null;
	window.ontouchmove = null;
	document.onkeydown = null;
}